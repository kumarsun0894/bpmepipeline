# -*- coding: utf-8 -*-
"""
Created on Fri Nov 13 15:51:54 2020

@author: SUN
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 11:14:52 2020
@author: Sun Kumar
"""

from LogSetup import setup_logging
setup_logging()
import logging
logger = logging.getLogger('BPMEngine.2.02')
#directory =os.path.split(Path(PureWindowsPath(os.path.abspath(__file__))))[0]
#os.chdir(Path(PureWindowsPath(directory)))



import warnings
warnings.filterwarnings("ignore")
import ast
import time
#         from LogSetup import setup_logging
#         setup_logging()

from BPME_Repository.PendingReports import PendingReports
from BPME_Repository.PALRefApi import PALRefApi
from BPME_Repository.LoadReferenceData import LoadReferenceData
from BPME_Repository.isComplexPropulsion import isComplexPropulsion
from BPME_Transform.ReferenceCurves import ReferenceCurves
from BPME_Repository.VesselFilter import VesselFilter
from BPME_Core.RefDashboards import RefDashboards
from BPME_Repository.LoadServiceData import LoadServiceData
from BPME_Repository.PALNoonApi import PALNoonApi
from BPME_Repository.PALHPApi import PALHPApi
from BPME_Repository.PALMEApi import PALMEApi
from BPME_Repository.PALAEApi import PALAEApi
from BPME_Repository.NDLRefApi import NDLRefApi
from BPME_Repository.SaveNoonInput import SaveNoonInput
from BPME_Repository.SaveHPInput import SaveHPInput
from BPME_Repository.SaveMEInput import SaveMEInput
from BPME_Repository.SaveAEInput import SaveAEInput
from BPME_Repository.NoonOutputApi import NoonOutputApi
from BPME_Repository.HPOutputApi import HPOutputApi
from BPME_Repository.MEOutputApi import MEOutputApi
from BPME_Repository.AEOutputApi import AEOutputApi
from BPME_Repository.SaveRefApI import SaveRefApI
from BPME_Repository.ReferenceCompare import ReferenceCompare
from BPMEngineSingleVessel import BPMEngineSingleVessel
from BPME_Repository.SaveSRQApi import SaveSRQApi
from config import mariHeaders,mariclientKey,mariclientCrt,mariBaseUrl
from config import maripendingUrl,mariGetRef,mariGetNoon,mariGetHP,mariGetME,mariGetAE
from config import naviBaseUrl,getReference,saveNoon,saveNoonKPI,saveHP,saveHPKPI
from config import saveSRQurl,saveME,saveMEKPI,saveAE,saveAEKPI
from config import saveReference,UnprocessedOnly
from config import vessel,fromDate,toDate,mariNoonOut,mariHPOut,mariMEOut,mariRefOut,mariAEOut
from BPME_Repository.PALNoonOutput import PALNoonOutput
from BPME_Repository.PALHPOutput import PALHPOutput
from BPME_Repository.PALMEOutput import PALMEOutput
from BPME_Repository.PALAEOutput import PALAEOutput
from BPME_Repository.PALRefOutput import PALRefOutput

try:

    start_time = time.time()
    BPMEngineVersion='2.02'
    headers = ast.literal_eval(mariHeaders)

    SRQ={}
    SRQ['BPMEVersion']=BPMEngineVersion
    SRQST1=[]

    #Invoking Prelimenary ApI for  fetching IMO and LogType
    records_df1,SRQST1 = PendingReports(mariBaseUrl,maripendingUrl,headers,mariclientCrt,mariclientKey,start_time,SRQST1)
    # import pandas as pd
    # records_df1 = pd.DataFrame(columns=['imo', 'logtype','rawref','naviRef','RDSraw','RDS','RefOut','nME','EngineIndex','nAE','AEEngineIndex'])
    # records_df1  = records_df1.append({'imo': int(9665683) ,'logtype': 'Noon'}, ignore_index=True)
    # records_df1  = records_df1.append({'imo': int(9665683) ,'logtype': 'HP'}, ignore_index=True)
    # records_df1  = records_df1.append({'imo': int(9665683) ,'logtype': 'ME','EngineIndex': int(1)}, ignore_index=True)
    #Applying Vessel Filter
    if vessel == '':
        pass
    else:
        records_df1 = VesselFilter(records_df1,vessel)



    #Invoking all ServiceData APIs for each row of records_df
    RecordData =records_df1.groupby(records_df1.imo)
    #for i,v in records_df.iterrows():
    for rd in RecordData:
        SRQST = SRQST1.copy()
        records_df=rd[1]
        ref_imo = records_df.imo.unique()
        try:
            

            #Invoking PAL Reference ApI for all IMOs
            records_df,SRQST  = PALRefApi(mariBaseUrl,mariGetRef,headers,mariclientCrt,mariclientKey,records_df,start_time,SRQST)
            #Loading Reference Data and Extracting NME from Reference Data for all IMOs
            records_df,SRQST = LoadReferenceData(records_df,start_time,SRQST)
            # Check if complex propulsion chain exist and go to next vessel
            records_df_filter = records_df.drop_duplicates(subset = ["imo"])
            records_df_filter.reset_index(drop=True,inplace=True)
    
            icp,SRQST=isComplexPropulsion(records_df_filter['RDSraw'][0],start_time,SRQST)
            if icp:
                print('Complex propulsion chain!')
            # Define reference curves
            records_df_filter,SRQST= ReferenceCurves(records_df_filter,start_time,SRQST)
            #RefDashboards(RDS)
            records_df_filter,SRQST=RefDashboards(records_df_filter,start_time,SRQST)
            #Invoking NDL Reference ApI for List of IMOspip
            records_df_filter,SRQST = NDLRefApi(naviBaseUrl,getReference,records_df_filter,start_time,SRQST)#incase of error use mariapps_reference only
            #Reference Comparison
            if SRQST[-1]['Remarks'] =='complete':
                mismatched_imo,SRQST = ReferenceCompare(records_df_filter,start_time,SRQST)
                #Saving Reference Output in Navidium Server/input also in case of change
                SRQST=SaveRefApI(naviBaseUrl,saveReference,mismatched_imo,records_df_filter,start_time,SRQST)
            elif SRQST[-1]['Remarks'] =='failed':
                mismatched_imo = records_df_filter.imo.tolist()
                #Saving Reference Output in Navidium Server/input also in case of change
                SRQST=SaveRefApI(naviBaseUrl,saveReference,mismatched_imo,records_df_filter,start_time,SRQST)
    
    #         else:
    #             SRQST=SaveRefApI(naviBaseUrl,saveReference,mismatched_imo,records_df_filter,start_time,SRQST)
            #Passing Data to MariApps
            SRQST = PALRefOutput(mariBaseUrl,mariRefOut,headers,mariclientCrt,mariclientKey,records_df_filter,start_time,SRQST)
            records_df1 = records_df.merge(records_df_filter,on='imo',how='right')
            records_df2 = records_df1.drop(['logtype_y','nME_y','EngineIndex_y','nAE_y','AEEngineIndex_y','naviRef_x','RDS_x','RefOut_x','rawref_x','RDSraw_x'],axis=1)
            records_df2.rename(columns = {'logtype_x':'logtype', 'nME_x':'nME','nAE_x':'nAE','AEEngineIndex_x':'AEEngineIndex',
                                      'RDS_y':'RDS','RefOut_y':'RefOut','EngineIndex_x':'EngineIndex','rawref_y':'rawref','naviRef_y':'naviRef','RDSraw_y':'RDSraw'}, inplace = True)
    
            #records_df=records_df.iloc[0]
            for i , records_df in records_df2.iterrows():
                try:
                    #Processing for Noon Service Data
                    if records_df['logtype'] =='Noon':
                        SRQSTN=SRQST.copy()
                        SRQN=SRQ.copy()
                        SRQN['logtype']='Noon'
                        #Calling Mariapps API for Fetching Noon Service Data
                        ServiceDataRaw,SRQSTN = PALNoonApi(mariBaseUrl,mariGetNoon,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,start_time,SRQSTN)
                        #Loading Service Data into Dataframe For processing
                        ServiceData,SRQSTN = LoadServiceData(ServiceDataRaw,records_df['logtype'],start_time,SRQSTN,records_df['imo'])
                        print("Total Number of Noon LogID to process-{}".format(len(ServiceData)))
                        if len(ServiceData)!=0:
                            ServiceData =ServiceData.groupby(ServiceData.LogID)
                            #Passing Each LogID(row) into BPMEngineSingleVessel
                            for data in ServiceData:
                                SRQSTNSD=SRQSTN.copy()
                                #Saving Noon Input into Navidium Database by Calling Navidium API
                                SRQSTNSD=SaveNoonInput(naviBaseUrl,saveNoon,records_df['imo'],data[1],start_time,SRQSTNSD)
                                #Calling BPMEngine Single Vessel
                                NoonOutput,NoonDiagnostics,NoonOutputExt,srqlist=BPMEngineSingleVessel(records_df['logtype'],records_df['EngineIndex'],BPMEngineVersion,records_df['RDS'],data[1],SRQN,start_time,SRQSTNSD)
                                #Saving Noon Output into Navidium Database by Calling Navidim API
                                SRQSTND=NoonOutputApi(naviBaseUrl,saveNoonKPI,records_df['imo'],NoonOutput,NoonDiagnostics,NoonOutputExt,start_time)
                                srqlist['SRQST'].append(SRQSTND)
                                #Passing Noon Api to Mariapps
                                srqlist['SRQST'].append(PALNoonOutput(mariBaseUrl,mariNoonOut,headers,mariclientCrt,mariclientKey,NoonOutput,NoonDiagnostics,start_time))
                                #Saving Service Queue Request by Calling Navidium's Service Queue Request API
                                SaveSRQApi(naviBaseUrl,saveSRQurl,srqlist,start_time)
    
                        else:
                            print("No Noon Log to process for imo {}".format(records_df['imo']))
                    #Processing for HP Service Data
                    elif records_df['logtype'] =='HP':
                        SRQSTH=SRQST.copy()
                        SRQH=SRQ.copy()
                        SRQH['logtype']='HP'
                        #Calling Mariapps API for Fetching HP Service Data
                        ServiceDataRaw,SRQSTH = PALHPApi(mariBaseUrl,mariGetHP,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,start_time,SRQSTH)
                        #Loading Service Data into Dataframe For processing
                        ServiceData,SRQSTH = LoadServiceData(ServiceDataRaw,records_df['logtype'],start_time,SRQSTH,records_df['imo'])
                        print("Total Number of HP LogID to process-{}".format(len(ServiceData)))
                        if len(ServiceData)!=0:
                            ServiceData =ServiceData.groupby(ServiceData.LogID)
                            #Passing Each LogID(row) into BPMEngineSingleVessel
                            for data in ServiceData:
                                SRQSTHSD=SRQSTH.copy()
                                #Saving HP Input into Navidium Database by Calling Navidium API
                                SRQSTHSD=SaveHPInput(naviBaseUrl,saveHP,records_df['imo'],data[1],start_time,SRQSTHSD)
                                #Calling BPMEngine Single Vessel
                                HPOutput,HPDiagnostics,HPOutputExt,srqlist=BPMEngineSingleVessel(records_df['logtype'],records_df['EngineIndex'],BPMEngineVersion,records_df['RDS'],data[1],SRQH,start_time,SRQSTHSD)
                                #Saving HP Output into Navidium Database by Calling Navidim API
                                SRQSTHD=HPOutputApi(naviBaseUrl,saveHPKPI,records_df['imo'],HPOutput,HPDiagnostics,start_time)
                                srqlist['SRQST'].append(SRQSTHD)
                                #Passing HP Output to Mariapps
                                srqlist['SRQST'].append(PALHPOutput(mariBaseUrl,mariHPOut,headers,mariclientCrt,mariclientKey,HPOutput,HPDiagnostics,start_time))
                                #Saving Service Queue Request by Calling Navidium's Service Queue Request API
                                SaveSRQApi(naviBaseUrl,saveSRQurl,srqlist,start_time)
                        else:
                            print("No HP Log to process for imo {}".format(records_df['imo']))
    
                    # #Processing for ME Service Data
                    elif records_df['logtype'] =='ME':
                        SRQSTM=SRQST.copy()
                        SRQM=SRQ.copy()
                        SRQM['logtype']='ME'
                        for EngineNo in range(0,int(records_df['nME'])):
                            #Calling Mariapps API for Fetching ME Service Data
                            ServiceDataRaw,SRQSTM = PALMEApi(mariBaseUrl,mariGetME,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,EngineNo+1,start_time,SRQSTM)
                            #Loading Service Data into Dataframe For processing
                            ServiceData,SRQSTM = LoadServiceData(ServiceDataRaw,records_df['logtype'],start_time,SRQSTM,records_df['imo'])
                            print("Total Number of ME LogID to process-{}".format(len(ServiceData)))
                            if len(ServiceData)!=0:
                                ServiceData =ServiceData.groupby(ServiceData.LogID)
                                #Passing Each LogID(row) into BPMEngineSingleVessel
                                for data in ServiceData:
                                    SRQSTMSD=SRQSTM.copy()
                                    #Saving ME Input into Navidium Database by Calling Navidium API
                                    SRQSTMSD=SaveMEInput(naviBaseUrl,saveME,records_df['imo'],data[1],EngineNo+1,start_time,SRQSTMSD)
                                    #Calling BPMEngine Single Vessel
                                    MEOutput,MEDiagnostics,MEOutputExt,srqlist=BPMEngineSingleVessel(records_df['logtype'],EngineNo+1,BPMEngineVersion,records_df['RDS'],data[1],SRQM,start_time,SRQSTMSD)
                                    #Saving ME Output into Navidium Database by Calling Navidim API
                                    SRQSTMD=MEOutputApi(naviBaseUrl,saveMEKPI,records_df['imo'],MEOutput,MEDiagnostics,EngineNo+1,start_time)
                                    srqlist['SRQST'].append(SRQSTMD)
                                    #Pass ME output to PAL
                                    srqlist['SRQST'].append(PALMEOutput(mariBaseUrl,mariMEOut,headers,mariclientCrt,mariclientKey,EngineNo+1,MEOutput,MEDiagnostics,start_time))
                                    #Saving Service Queue Request by Calling Navidium's Service Queue Request API
                                    SaveSRQApi(naviBaseUrl,saveSRQurl,srqlist,start_time)
                            else:
                                print("No ME Log to process for imo {}".format(records_df['imo']))
    
                    # elif records_df['logtype'] =='AE':
                    #     SRQSTA=SRQST.copy()
                    #     SRQA=SRQ.copy()
                    #     SRQA['logtype']='AE'
                    #     for EngineNo in range(0,int(records_df['nAE'])):
                    #         #Calling Mariapps API for Fetching AE Service Data
                    #         ServiceDataRaw,SRQSTA = PALAEApi(mariBaseUrl,mariGetAE,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,EngineNo+1,start_time,SRQSTA)
                    #         #Loading Service Data into Dataframe For processing
                    #         ServiceData,SRQSTA = LoadServiceData(ServiceDataRaw,records_df['logtype'],start_time,SRQSTA,records_df['imo'])
                    #         print("Total Number of AE LogID to process-{}".format(len(ServiceData)))
                    #         if len(ServiceData)!=0:
                    #             ServiceData =ServiceData.groupby(ServiceData.LogID)
                    #             #Passing Each LogID(row) into BPMEngineSingleVessel
                    #             for data in ServiceData:
                    #                 SRQSTASD=SRQSTA.copy()
                    #                 #Saving AE Input into Navidium Database by Calling Navidium API
                    #                 SRQSTASD=SaveAEInput(naviBaseUrl,saveAE,records_df['imo'],data[1],EngineNo+1,start_time,SRQSTASD)
                    #                 #Calling BPMEngine Single Vessel
                    #                 AEOutput,AEDiagnostics,AEOutputExt,srqlist=BPMEngineSingleVessel(records_df['logtype'],EngineNo+1,BPMEngineVersion,records_df['RDS'],data[1],SRQA,start_time,SRQSTASD)
                    #                 #Saving AE Output into Navidium Database by Calling Navidim API
                    #                 SRQSTAD=AEOutputApi(naviBaseUrl,saveAEKPI,records_df['imo'],AEOutput,AEDiagnostics,EngineNo+1,start_time)
                    #                 srqlist['SRQST'].append(SRQSTAD)
                    #                 #Pass AE output to PAL
                    #                 srqlist['SRQST'].append(PALAEOutput(mariBaseUrl,mariAEOut,headers,mariclientCrt,mariclientKey,EngineNo+1,AEOutput,AEDiagnostics,start_time))
                    #                 #Saving Service Queue Request by Calling Navidium's Service Queue Request API
                    #                 SaveSRQApi(naviBaseUrl,saveSRQurl,srqlist,start_time)
                    #         else:
                    #             print("No AE Log to process for imo {}".format(records_df['imo']))
    
    
    
    
    
    
    
                except:
                    
                    logger.error("Error in BPMEngine for imo {} and logtype {}".format(records_df['imo'],records_df['logtype']),exc_info=True)
                    pass

        except Exception as er:
            #logger.error("Error in BPMEngine {} ".format(er),exc_info=True,stack_info =True)
            logger.error("Error in BPMEngine {} for imo-{} ".format(er,ref_imo),exc_info=True)
            pass
except Exception as err:
    logger.error("Error in BPMEngine {} ".format(err),exc_info=True)

else:
    print("BPM Engine 2.02  Execution Completed Successfully")


