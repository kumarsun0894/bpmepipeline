# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 17:27:21 2020

@author: Sun Kumar
"""
import logging
import time
logger = logging.getLogger('BPMEngine.2.02')
import requests


def SaveAEInput(naviBaseUrl,saveAE,imo,ServiceDataRaw,EngineIndex,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        ServiceDataRaw = ServiceDataRaw.to_csv(index = False)
        saveAEurl=naviBaseUrl+saveAE
        r=requests.post(saveAEurl,json={'IMO':int(imo),'LogData':ServiceDataRaw,'AENumber':int(EngineIndex)})
        if r.status_code==200:
            print("AE Input ServiceData Successfully Dumped into Database")
        elif r.status_code!=200:
            logger.error("SaveAEInput Connection Error {}".format(r.status_code), exc_info=True,stack_info =True)
            raise Exception
    except Exception as err:

        logger.error("Error in SaveAEInput {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return(SRQST)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(SRQST)
