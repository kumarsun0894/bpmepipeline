# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 16:29:52 2020

@author: Subhin Antony
"""
import io
import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')
import numpy as np
import  pandas as pd



def LoadServiceData(ServiceData,LogType,start_time,SRQST,imo):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=time.time()-start_time
    SRQSTD['Stage']=__name__
    try:

        inputdata=pd.read_csv(io.StringIO(ServiceData))
        inputdata['tstart']=pd.to_datetime(inputdata['DateStart_UTC']).dt.strftime('%d/%m/%Y')+ ' ' + inputdata['TimeStart_UTC']
        inputdata['tend']=pd.to_datetime(inputdata['DateEnd_UTC']).dt.strftime('%d/%m/%Y')+ ' ' + inputdata['TimeEnd_UTC']
        inputdata['tstart']=pd.to_datetime(inputdata['tstart'])
        inputdata['tend'] = pd.to_datetime(inputdata['tend'])
        # inputdata['tstart']=pd.to_datetime(inputdata['DateStart_UTC'] + ' ' + inputdata['TimeStart_UTC'],format="%d/%m/%Y %H:%M:%S")
        # inputdata['tend']=pd.to_datetime(inputdata['DateEnd_UTC'] + ' ' + inputdata['TimeEnd_UTC'],format="%d/%m/%Y %H:%M:%S")
        inputdata['Dt']=(inputdata.tend-inputdata.tstart).astype('timedelta64[s]')
        inputdata['Dt']=inputdata.Dt/3600
        inputdata['LogID']=inputdata['LogID'].astype('str')
        inputdata['LogID']=inputdata['LogID'].str.rstrip('_')




#         coll=[]
#         for col in inputdata.columns:
#             if inputdata[col].dtype == np.float64:
#                 coll.append(col)
#
#         inputdata=inputdata.T
#         ServiceDataStruct = dict(zip(inputdata.index, inputdata.values))
#         for ky in coll:
#             ServiceDataStruct[ky]=ServiceDataStruct[ky].astype('float64')
#
#
#
#
#         ServiceDataStruct['LogID']=ServiceDataStruct['LogID'].astype('int64')
    except Exception as err:

        logger.error("Error in LoadServiceData {}".format(err),exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return({},SRQST)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        if inputdata.empty == False:
            return(inputdata,SRQST)
        else:
            logger.error("No Pending Records to Perform Operation Further for imo {} and logtype {} ".format(imo,LogType))
            return({},SRQST)



