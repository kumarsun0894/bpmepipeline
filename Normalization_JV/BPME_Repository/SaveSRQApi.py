# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 11:14:52 2020
@author: Sun Kumar
"""
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')
import requests
from requests.auth import HTTPBasicAuth

def SaveSRQApi(naviBaseUrl,saveSRQurl,srq,start_time,AuthUser,AuthPW):
    try:
        srqApi = naviBaseUrl+saveSRQurl
        r=requests.post(srqApi,json=srq,auth=HTTPBasicAuth(AuthUser, AuthPW))
        if r.status_code==200:
            print("ServiceQueueRequest Successfully Saved in Database")
        elif r.status_code!=200:
            logger.error("{} {} for URL-{}".format(r.text,r.status_code,srqApi), exc_info=True,stack_info =True)
            raise Exception

    except Exception as err:

        logger.error("Error in SaveSRQApi{}".format(err), exc_info=True,stack_info =True)
