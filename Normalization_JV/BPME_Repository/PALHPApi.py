# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 15:51:04 2020

@author: Sun Kumar
"""

import requests

import time
import logging

logger = logging.getLogger('BPMEngine.2.02')

def PALHPApi(mariBaseUrl,mariGetHP,fromDate,toDate,UnprocessedOnly,headers,clientCrt,clientKey,records_df,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        url = mariBaseUrl+mariGetHP+'?IMO={}&FromDate={}&ToDate={}&UnProcessedOnly={}'.format(records_df['imo'],fromDate,toDate,UnprocessedOnly)
        #url = mariBaseUrl+mariGetHP+'?IMO={}&ToDate={}&UnProcessedOnly={}'.format(records_df['imo'],toDate,UnprocessedOnly)
        r = requests.get(url, headers=headers, cert=(clientCrt,clientKey))

        if r.status_code == 200:
            HPServiceRaw =r.text
        elif r.status_code != 200:
            #logger.error("PALHPApi Connection Error {}".format(r.status_code), exc_info=True,stack_info =True)
            logger.error("{} {} for URL-{}".format(r.text,r.status_code,url), exc_info=True,stack_info =True)
            raise Exception

    except Exception as err:

        logger.error("Error in PALHPApi {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return('',SRQST)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(HPServiceRaw,SRQST)





