# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 12:46:18 2020

@author: Sun Kumar
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 15:34:58 2020

@author: Sun Kumar
"""

import requests
import time
import logging
logger = logging.getLogger('BPMEngine.2.02')



def PALRefApi(mariBaseUrl,mariGetRef,headers,clientCrt,clientKey,records_df,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=time.time()-start_time
    SRQSTD['Stage']=__name__
    try:
        imoList = records_df.imo.unique().tolist()
        for imo in imoList:
            refurl = mariBaseUrl+mariGetRef+'?'+'IMO={}'.format(imo)
            r = requests.get(refurl, headers=headers, cert=(clientCrt, clientKey))
            if r.status_code == 200:
                for  i,x in records_df.iterrows():
                    if x['imo'] == imo:
                        x['rawref'] = r.text
            elif r.status_code != 200:
                logger.error("PAL Ref API Connection Error {} with Response Text {}".format(r.status_code,r.text), exc_info=True)
                for  i,x in records_df.iterrows():
                    if x['imo'] == imo:
                        x['rawref'] ="empty"
        records_df_filtered = records_df[records_df['rawref'] != "empty"]

    except Exception:
        logger.error("PAL Ref API call Error", exc_info=True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        
        return({},SRQST)
    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        if records_df_filtered.empty == False:
            return(records_df_filtered,SRQST)
        else:
            logger.error("No Pending Reference Data Records to Perform Operation Further")
            return({},SRQST)
        # return(records_df_filtered,SRQST)





