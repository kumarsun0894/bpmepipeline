# -*- coding: utf-8 -*-
"""
Created on Sun Jun 28 22:22:50 2020

@author: SubhinAntony
"""
import numpy as np
import time
import logging 
# create logger
logger = logging.getLogger('Normalization_JV.BPMEngine')
def dataframetodic(inputdata,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        coll=[]
        for col in inputdata.columns:
            if inputdata[col].dtype == np.float64:
                coll.append(col)
            
        inputdata=inputdata.T
        ServiceDataStruct = dict(zip(inputdata.index, inputdata.values))
        for ky in coll:
            ServiceDataStruct[ky]=ServiceDataStruct[ky].astype('float64')
        ServiceDataStruct['LogID']=ServiceDataStruct['LogID'].astype('float64').astype('int64')
        # return(ServiceDataStruct,SRQSTD)
    except Exception as err:
        raise
        logger.error("{}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time              
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return({},SRQST)
        
    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time               
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(ServiceDataStruct,SRQST)
