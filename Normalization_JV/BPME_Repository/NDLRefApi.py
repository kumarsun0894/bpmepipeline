# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 23:10:07 2020

@author: hadoop
"""
import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')
import requests
from requests.auth import HTTPBasicAuth
def NDLRefApi(naviBaseUrl,getReference,records_df,start_time,SRQST,AuthUser,AuthPW):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=time.time()-start_time
    SRQSTD['Stage']=__name__
    try:
        imoList = records_df['imo'].astype(int).to_list()
        naviref=naviBaseUrl+getReference
        
        r=requests.get(naviref,json=imoList,auth=HTTPBasicAuth(AuthUser, AuthPW))
        if r.status_code==200:
            navirefout = r.json()
            navirefout = navirefout['result']['referenceData']
            for i in range(0,len(navirefout)):
                for x,v in records_df.iterrows():
                    if int(v['imo']) ==navirefout[i]['imo']:
                        v['naviRef'] = navirefout[i]['data']
        elif r.status_code!=200:
            #logger.error("Navidium Ref API Connection Error {}".format(r.status_code), exc_info=True,stack_info =True)
            logger.error("{} {} for URL-{}".format(r.text,r.status_code,naviref), exc_info=True,stack_info =True)
            raise Exception

        # return records_df
    except Exception as err:
        logger.error("Error in NDLRefApi {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return(records_df,SRQST)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(records_df,SRQST)
