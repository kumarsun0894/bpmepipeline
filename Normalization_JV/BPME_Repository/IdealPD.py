# -*- coding: utf-8 -*-

import numpy as np
from BPME_Common import IdealST as IST
from BPME_Common import WaterLineLength as WLL
from BPME_Common import WaterPlaneArea as WPA
from BPME_Common import BowSectionArea as BSA
from BPME_Common import TransomArea as TA
from BPME_Common import RudderSurfaceArea as RSA
from BPME_Common import CenterOfBuoyancy as CB
from BPME_Common import MidshipSectionArea as MSA
from BPME_Common import WettedSurfaceArea as WSA
from BPME_Common import IdealDeliveredPowerHoltrop as IDPH
from BPME_Common import IdealDeliveredPower as IDP

def IdealPD(U,Tm,Tsw,Sasw,RDS):
    t=Tm*np.ones(U.shape)
    TaH=t
    TfH=t
    TmH=(TaH+TfH)/2
    DISPVH=IST.IdealST(RDS.Th,RDS.Vh,TmH,0)
    [LwlH,xLwl0_5H]=WLL.WaterLineLength(TaH,TfH,RDS['Lbp'],RDS['HullProfile'].T)   
    [_,CwpH]=WPA.WaterPlaneArea(TmH,LwlH,RDS['B'],RDS['Th'],RDS['Cwph'])
    [AbH,hbH]=BSA.BowSectionArea(TfH,RDS['BowSection'].T,RDS['BulbousBowVessel'])
    AtrH=TA.TransomArea(TaH,TfH,RDS['Lbp'],RDS['B'],RDS['HullProfile'].T,RDS['TransomSection'].T,RDS['TransomSternVessel'])
    ArH=RSA.RudderSurfaceArea(TaH,RDS['Lbp'],RDS['RudderProfile'].T)
    [_,lcbH]=CB.CenterOfBuoyancy(TmH,LwlH,xLwl0_5H,RDS['Th'],RDS['LCBh'])
    CbH=DISPVH/(LwlH*RDS['B']*TmH)
    [_,CmH]=MSA.MidshipSectionArea(TmH,RDS['B'],RDS['Th'],RDS['Cmh'])#midship section area under water in [m2]
    WSAH=WSA.WettedSurfaceArea(TmH,LwlH,DISPVH,CmH,CwpH,AbH,RDS['Th'],RDS['WSAh'],RDS['B'])#wetted surface area in [m2]
#    vswH=SeawaterViscosity(Tsw*np.ones(TmH.shape),Sasw*np.ones(TmH.shape))
#    rhoswH=SeawaterDensity(Tsw*np.ones(TmH.shape),Sasw*np.ones(TmH.shape))
    PDHid=IDPH.IdealDeliveredPowerHoltrop(U,TmH,TfH,LwlH,DISPVH,WSAH,CbH,CmH,CwpH,lcbH,rhoswH,vswH,ArH,AbH,hbH,AtrH,RDS)
    PD=IDP.IdealDeliveredPower(U,t,PDHid,RDS['Tref'],RDS['Pref'],RDS['Uref'],RDS['Tb'],RDS['Ts'])
    
    return PD
                                       