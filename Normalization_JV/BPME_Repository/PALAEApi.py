# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 15:51:36 2020

@author: Sun Kumar
"""

import requests
import time

import logging

logger = logging.getLogger('BPMEngine.2.02')

def PALAEApi(mariBaseUrl,mariGetAE,fromDate,toDate,UnprocessedOnly,headers,clientCrt,clientKey,records_df,EngineNo,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        url = mariBaseUrl+mariGetAE+'?IMO={}&FromDate={}&ToDate={}&NoOfEngines={}&EngineNo={}&Equipment={}&UnProcessedOnly={}'.format(records_df['imo'],fromDate,toDate,records_df['nAE'],EngineNo,'AE',UnprocessedOnly)
        r = requests.get(url, headers=headers, cert=(clientCrt,clientKey))

        if r.status_code == 200:
            AEServiceRaw =r.text
        elif r.status_code != 200:
            logger.error("PALAEApi Connection Error {}".format(r.status_code), exc_info=True,stack_info =True)
            raise Exception
    except Exception as err:

        logger.error("Error in PALAEApi {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return('',SRQST)
    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(AEServiceRaw,SRQST)




