# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 22:58:52 2020

@author: Sun Kumar
"""
import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')
import requests

def AEOutputApi(naviBaseUrl,saveAEKPI,imo,AEOutput,AEDiagnostics,EngineNo,start_time):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        AEout=naviBaseUrl+saveAEKPI
        r=requests.post(MEout,json={'IMO':int(imo),'Output':AEOutput,'Diagnostics':AEDiagnostics,'AENumber':EngineNo})
        if r.status_code==200:
            print("AE Ouput Successfully Saved in Database")
        elif r.status_code!=200:
            logger.error("AEOutputApi Connection Error {}".format(r.status_code), exc_info=True,stack_info =True)
            logger.error("{} {} for URL-{}".format(r.text,r.status_code,AEout), exc_info=True,stack_info =True)
            raise Exception
    except Exception as err:

        logger.error("Error in AEOutputApi {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        return(SRQSTD)
    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        return(SRQSTD)
