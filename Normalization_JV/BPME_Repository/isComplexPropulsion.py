# -*- coding: utf-8 -*-
"""
Created on Tue May 12 15:32:37 2020

@author: SubhinAntony

% isComplexPropulsion
% Description:  This function checks if a vessel has complex propulsion
%			   configuration.
%
% Input:		ReferenceDataRawStruct (structure)
%
% Output:	   isCP	0 or 1 (logical)


"""
import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')
def isComplexPropulsion(ReferenceDataRawStruct,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=time.time()-start_time
    SRQSTD['Stage']=__name__
    try:
    	isCP=((ReferenceDataRawStruct['nME']>1) or\
    		  ('4' in ReferenceDataRawStruct['CategoryME']) or\
    			  (ReferenceDataRawStruct['nProp']>1) or\
    				  ('CPP' in ReferenceDataRawStruct['TypeProp']) or\
    					  ('Yes'in ReferenceDataRawStruct['SG']) or\
    						  ('Yes' in ReferenceDataRawStruct['RG']) or\
    							  ('Yes' in ReferenceDataRawStruct['PMotor']))
    	if isCP:
    		Number_of_MEs= ReferenceDataRawStruct['nME']
    		ME_Stroke_Type=ReferenceDataRawStruct['CategoryME']
    		Number_of_Propellers= ReferenceDataRawStruct['nProp']
    		Propeller_Type= ReferenceDataRawStruct['TypeProp']
    		Shaft_Generator= ReferenceDataRawStruct['SG']
    		Reduction_Gear= ReferenceDataRawStruct['RG']
    		Propulsion_Motor= ReferenceDataRawStruct['PMotor']
    		ComplexPropulsion=dict(Number_of_MEs=Number_of_MEs,ME_Stroke_Type=ME_Stroke_Type,Number_of_Propellers=Number_of_Propellers,Propeller_Type=Propeller_Type,Shaft_Generator=Shaft_Generator,Reduction_Gear=Reduction_Gear,Propulsion_Motor=Propulsion_Motor)
    except Exception as err:

        logger.error("isComplexPropulsion Error {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        #SRQST.append(SRQSTD)
        return({},SRQST)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        #SRQST.append(SRQSTD)
        return(isCP,SRQST)


# 	return(isCP)




