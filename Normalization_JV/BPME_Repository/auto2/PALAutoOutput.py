# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 16:45:44 2020

@author: Sun Kumar
"""
import time
import requests
import logging
import io
import pandas as pd

logger = logging.getLogger('BPMEngine.2.02')

def PALAutoOutput(mariBaseUrl,mariAutoOut,headers,mariclientCrt,mariclientKey,AutoOutput,AutoDiagnostics,start_time):
	SRQSTD={}
	SRQSTD['StepTimeLapse']=(time.time()-start_time)
	SRQSTD['Stage']=__name__
	try:
		Output=pd.read_csv(io.StringIO(AutoOutput))
		
		
		DateTimeStart = pd.to_datetime(Output.DateTimeStart,format="%Y-%m-%d %H:%M:%S")
		DateTimeStart = DateTimeStart[0].to_pydatetime()
		DateTimeEnd = pd.to_datetime(Output.DateTimeEnd,format="%Y-%m-%d %H:%M:%S")
		DateTimeEnd = DateTimeEnd[0].to_pydatetime()
		AutoOutput=Output.to_csv(index = False)
		
		noonouturl=mariBaseUrl+mariAutoOut
		if len(Output.IMO)>1:
			LogID = Output.LogID.tolist()
			IMO = Output.IMO[0].astype(str)
			r=requests.post(noonouturl,json={'IMO':IMO,'NoonLogOutput':AutoOutput,'NoonLogDiagnostics':AutoDiagnostics},headers=headers,cert=(mariclientCrt, mariclientKey))
			if r.status_code==200:
				print("Auto Output passed to PAL")
			elif r.status_code!=200:
				logger.error("{} {} for URL-{} and IMO-{}".format(r.text,r.status_code,noonouturl,IMO), exc_info=True,stack_info =True)
				raise Exception
		else:
			LogID = int(Output.LogID)
			IMO = str(int(Output.IMO))
			r=requests.post(noonouturl,data={'IMO':IMO,'LogId':LogID,'StartDate':DateTimeStart,'AutoLogOutput':AutoOutput,'AutoLogDiagnostics':AutoDiagnostics},headers=headers,cert=(mariclientCrt, mariclientKey))
			if r.status_code==200:
				print("Auto Output passed to PAL")
			elif r.status_code!=200:
				logger.error("{} {} for URL-{} and IMO-{}".format(r.text,r.status_code,noonouturl,IMO), exc_info=True,stack_info =True)
				raise Exception
	except Exception as err:
		logger.error("Error in Passing  Auto Data to PAL {}".format(err), exc_info=True,stack_info =True)
		SRQSTD['TotalTimeLapse']=time.time()-start_time
		SRQSTD['Remarks']="failed"
		return(SRQSTD)
	else:
		SRQSTD['TotalTimeLapse']=time.time()-start_time
		SRQSTD['Remarks']="complete"
		return(SRQSTD)
