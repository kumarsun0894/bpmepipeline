# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 17:06:06 2020

@author: hadoop
"""
import time
import requests
import logging
import pandas as pd
import io

logger = logging.getLogger('BPMEngine.2.02')

def PALMEOutput(mariBaseUrl,mariMEOut,headers,mariclientCrt,mariclientKey,EngineNo,MEOutput,MEDiagnostics,start_time):
	SRQSTD={}
	SRQSTD['StepTimeLapse']=(time.time()-start_time)
	SRQSTD['Stage']=__name__
	try:
		Output=pd.read_csv(io.StringIO(MEOutput))

		DateTimeStart = pd.to_datetime(Output.DateTimeStart,format="%Y-%m-%d %H:%M:%S")
		DateTimeStart = DateTimeStart[0].to_pydatetime()
		DateTimeEnd = pd.to_datetime(Output.DateTimeEnd,format="%Y-%m-%d %H:%M:%S")
		DateTimeEnd = DateTimeEnd[0].to_pydatetime()
		MEOutput=Output.to_csv(index = False)
		meouturl=mariBaseUrl+mariMEOut
		if len(Output.IMO)>1:
			IMO = Output.IMO[0].astype(str)
			r=requests.post(meouturl,json={'IMO':IMO,'EngineNo':EngineNo,'MELogOutput':MEOutput,'MELogDiagnostics':MEDiagnostics},headers=headers,cert=(mariclientCrt, mariclientKey))
			if r.status_code==200:
				print("ME Output passed to PAL")
			elif r.status_code!=200:
				logger.error("{} {} for URL-{} and IMO-{}".format(r.text,r.status_code,meouturl,IMO), exc_info=True,stack_info =True)
				raise Exception
		else:
			LogID = int(Output.LogID)
			IMO = str(int(Output.IMO))
			r=requests.post(meouturl,data={'IMO':IMO,'LogId':LogID,'StartDate':DateTimeStart,'EndDate':DateTimeEnd,'EngineNo':EngineNo,'MELogOutput':MEOutput,'MELogDiagnostics':MEDiagnostics},headers=headers,cert=(mariclientCrt, mariclientKey))
			if r.status_code==200:
				print("ME Output passed to PAL")
			elif r.status_code!=200:
				logger.error("{} {} for URL-{} and IMO-{}".format(r.text,r.status_code,meouturl,IMOs), exc_info=True,stack_info =True)
				raise Exception
	except Exception as err:
		logger.error("Error in Passing  ME Data to PAL {}".format(err), exc_info=True,stack_info =True)
		SRQSTD['TotalTimeLapse']=time.time()-start_time
		SRQSTD['Remarks']="failed"
		return(SRQSTD)
	else:
		SRQSTD['TotalTimeLapse']=time.time()-start_time
		SRQSTD['Remarks']="complete"
		return(SRQSTD)
