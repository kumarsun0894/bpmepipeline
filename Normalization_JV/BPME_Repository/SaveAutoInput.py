# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 16:51:42 2020

@author: Sun Kumar
"""
import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')
import requests
from requests.auth import HTTPBasicAuth

def SaveAutoInput(naviBaseUrl,saveAuto,imo,ServiceDataRaw,start_time,SRQST,AuthUser,AuthPW):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        ServiceDataRaw = ServiceDataRaw.to_csv(index = False)
        savenoonurl=naviBaseUrl+saveAuto
        r=requests.post(savenoonurl,json={'IMO':int(imo),'LogData':ServiceDataRaw},auth=HTTPBasicAuth(AuthUser, AuthPW))
        if r.status_code==200:
            print("Auto Input ServiceData Successfully Dumped into Database")
        elif r.status_code!=200:
            logger.error("{} {} for URL-{}".format(r.text,r.status_code,savenoonurl), exc_info=True,stack_info =True)
            raise Exception

    except Exception as err:

        logger.error("Error in SaveAutoInput {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return(SRQST)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(SRQST)
