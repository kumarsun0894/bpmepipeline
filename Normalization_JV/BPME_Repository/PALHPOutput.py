# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 17:05:47 2020

@author: hadoop
"""
import time
import requests
import logging
import io
import pandas as pd
import datetime
logger = logging.getLogger('BPMEngine.2.02')

def PALHPOutput(mariBaseUrl,mariHPOut,headers,mariclientCrt,mariclientKey,HPOutput,HPDiagnostics,start_time):
	SRQSTD={}
	SRQSTD['StepTimeLapse']=(time.time()-start_time)
	SRQSTD['Stage']=__name__
	try:
		Output=pd.read_csv(io.StringIO(HPOutput))
		
		DateTimeStart = pd.to_datetime(Output.DateTimeStart,format="%Y-%m-%d %H:%M:%S")
		DateTimeStart = DateTimeStart[0].to_pydatetime()
		DateTimeEnd = pd.to_datetime(Output.DateTimeEnd,format="%Y-%m-%d %H:%M:%S")
		DateTimeEnd = DateTimeEnd[0].to_pydatetime()
		HPOutput=Output.to_csv(index = False)
		hpouturl=mariBaseUrl+mariHPOut
		if len(Output.IMO)>1:
			IMO = Output.IMO[0].astype(str)
			r=requests.post(hpouturl,json={'IMO':IMO,'HPLogOutput':HPOutput,'HPLogDiagnostics':HPDiagnostics},headers=headers,cert=(mariclientCrt, mariclientKey))
			if r.status_code==200:
				print("HP Output passed to PAL")
			elif r.status_code!=200:
				logger.error("{} {} for URL-{} and IMO-{}".format(r.text,r.status_code,hpouturl,IMO), exc_info=True,stack_info =True)
				raise Exception
		else:
			LogID = int(Output.LogID)
			IMO = str(int(Output.IMO))
			r=requests.post(hpouturl,data={'IMO':IMO,'LogId':LogID,'StartDate':DateTimeStart,'EndDate':DateTimeEnd,'HPLogOutput':HPOutput,'HPLogDiagnostics':HPDiagnostics},headers=headers,cert=(mariclientCrt, mariclientKey))
			if r.status_code==200:
				print("HP Output passed to PAL")
			elif r.status_code!=200:
				#logger.error("HP Output not passed to PAL Error code {}".format(r.status_code), exc_info=True,stack_info =True)
				logger.error("{} {} for URL-{} and IMO-{}".format(r.text,r.status_code,hpouturl,IMO), exc_info=True,stack_info =True)
				raise Exception
	except Exception as err:
		logger.error("Error in Passing  HP Data to PAL {}".format(err), exc_info=True,stack_info =True)
		SRQSTD['TotalTimeLapse']=time.time()-start_time
		SRQSTD['Remarks']="failed"
		return(SRQSTD)
	else:
		SRQSTD['TotalTimeLapse']=time.time()-start_time
		SRQSTD['Remarks']="complete"
		return(SRQSTD)
