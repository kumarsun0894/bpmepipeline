# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 17:27:21 2020

@author: Sun Kumar
"""
import logging
import time
logger = logging.getLogger('BPMEngine.2.02')
import requests
from requests.auth import HTTPBasicAuth


def SaveMEInput(naviBaseUrl,saveME,imo,ServiceDataRaw,EngineIndex,start_time,SRQST,AuthUser,AuthPW):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        ServiceDataRaw = ServiceDataRaw.to_csv(index = False)
        saveMEurl=naviBaseUrl+saveME
        r=requests.post(saveMEurl,json={'IMO':int(imo),'LogData':ServiceDataRaw,'MENumber':int(EngineIndex)},auth=HTTPBasicAuth(AuthUser, AuthPW))
        if r.status_code==200:
            print("ME Input ServiceData Successfully Dumped into Database")
        elif r.status_code!=200:
            logger.error("{} {} for URL-{}".format(r.text,r.status_code,saveMEurl), exc_info=True,stack_info =True)
            raise Exception
    except Exception as err:

        logger.error("Error in SaveMEInput {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return(SRQST)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(SRQST)
