# -*- coding: utf-8 -*-
"""
Created on Tue Jun 30 14:05:12 2020

@author: 	Sun Kumar
"""

def VesselFilter(records_df,vessel_filter):
	#9841938,9841940,9665683,9439864,9665657,9665671,9665669,9676735,9328493,9676709,9676711,9760770,9266229,9278624,9590230
	imolist = vessel_filter.split(',')
	records_df = records_df[records_df['imo'].isin(imolist)]
	if records_df.empty == True:
		raise Exception("Sorry No matching IMO to process")
	else:
		records_df.reset_index(drop=True, inplace=True)
		return records_df
	