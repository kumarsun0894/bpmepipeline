# -*- coding: utf-8 -*-
"""
Created on Wed May 27 11:25:24 2020

@author: Subhin  Antony
"""
import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')
import numpy as np
def ReferenceCompare(df,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=time.time()-start_time
    SRQSTD['Stage']=__name__
    try:
        df2 = df.copy()
        df2=df2.drop(columns=['logtype','nME','EngineIndex','nAE','AEEngineIndex','RDSraw','RDS','RefOut','LogCount'])
        df2.columns=['imo', 'rawrefm','rawrefn']
        df2.drop_duplicates(keep='first',inplace = True)
        df2['Match'] = np.where(df2['rawrefm'] == df2['rawrefn'] , 'True', 'False')
        dfcp=df2.copy()
        dfcp=dfcp[dfcp.Match=='False']
        dfcp=dfcp.drop(columns=['Match','rawrefm'])
        dfcp.columns=['imo', 'rawref']

        reflist_update=dfcp['imo'].tolist()
        # return(reflist_update)
    except Exception as err:
        # raise
        logger.error("{}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return([],SRQST)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(reflist_update,SRQST)
