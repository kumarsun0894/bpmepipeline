# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 13:12:23 2020

@author: SubhinAntony
"""
import logging 
# create logger
logging.basicConfig(filename ="C:\\Users\\SubhinAntony\\Downloads\\Normalization_JV\\output\\prb.log",level=logging.DEBUG)
logger=logging.getLogger(__name__)

import BPMEngineSingleVessel as besv
from datetime import datetime
VesselObjectID = 683546
# VesselObjectID=864997
EngineIndex = ''
# EngineIndex=2
# LogType='ME'
LogType = 'Noon'
# LogType='HP'
# LogType='Auto'
# BPMEngineNoonVersion = 1.28
BPMEngineVersion = 1.28
print(VesselObjectID,LogType,EngineIndex)
ref,ServiceData=1,2
SRQ={}
# SRQ['ID']=111
SRQ['CreatedDate']=datetime.now()
SRQ['CreatedDate']=SRQ['CreatedDate'].strftime("%d/%m/%Y %H:%M:%S")
SRQ['LogId']=LogType
SRQ['BPMEVersion']=BPMEngineVersion
SRQ['SRQStatus']='Queued'
SRQST={}
# SRQST['ID']=101
# SRQST['SRQId']=SRQ['ID']
SRQST['CreatedDate']=datetime.now()
SRQST['CreatedDate']=SRQST['CreatedDate'].strftime("%d/%m/%Y %H:%M:%S")
SRQST['Stage']=[]
SRQST['Remarks']=[]
SRQST['StepTimeLapse']=[]
SRQST['TotalTimeLapse']=[]
try:
    Output,RefOutput,Diagnostics,ExOutput,SRQ,SRQST=besv.BPMEngineSingleVessel(LogType,EngineIndex,BPMEngineVersion,ref,ServiceData,SRQ,SRQST)
except Exception as err:
    logger.error(err)
    raise
finally:
    
    print('done')
    
    