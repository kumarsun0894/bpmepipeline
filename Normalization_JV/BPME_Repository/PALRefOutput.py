# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 20:35:02 2020

@author: Sun Kumar
"""
import requests
import pandas as pd
import time
import logging


# create logger
logger = logging.getLogger('BPMEngine.2.02')

def PALRefOutput(mariBaseUrl,mariRefOut,headers,mariclientCrt,mariclientKey,records_df,start_time,SRQST):
	SRQSTD={}
	SRQSTD['StepTimeLapse']=time.time()-start_time
	SRQSTD['Stage']=__name__
	try:
		marirefurl=mariBaseUrl+mariRefOut
		for i,v in records_df.iterrows():
			RefDashTable=pd.DataFrame.from_dict(v['RefOut'])
			RefOutput=RefDashTable.to_csv(index = False)
			#RefOutput=RefOutput.replace('\r\n','')
			data={'IMO':str(v['imo']),'ReferenceOutput':RefOutput}

			r=requests.post(marirefurl,json = data,headers=headers,cert=(mariclientCrt, mariclientKey))
			if r.status_code==200:
				print("Reference Output passed to PAL")
			elif r.status_code!=200:
				logger.error("{} {} for URL-{} and IMO-{}".format(r.text,r.status_code,marirefurl,v['imo']), exc_info=True,stack_info =True)
				raise Exception
	#
		# return(SRQSTD)
	except Exception as err:
		logger.error("Error in passing reference data to PAL{}".format(err), exc_info=True,stack_info =True)
		SRQSTD['TotalTimeLapse']=time.time()-start_time
		SRQSTD['Remarks']="failed"
		SRQST.append(SRQSTD)
		return(SRQST)

	else:
		SRQSTD['TotalTimeLapse']=time.time()-start_time
		SRQSTD['Remarks']="complete"
		SRQST.append(SRQSTD)
		return(SRQST)
