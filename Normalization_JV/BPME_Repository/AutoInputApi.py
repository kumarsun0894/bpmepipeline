# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 15:50:33 2020

@author: Sun Kumar
"""
import requests
from requests.auth import HTTPBasicAuth
import time
import logging

logger = logging.getLogger('BPMEngine.2.02')

def AutoInputApi(naviBaseUrl,getAuto,AutoFromDate,AutoToDate,records_df,start_time,SRQST,AuthUser,AuthPW):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__



    try:
        url=naviBaseUrl+getAuto

        r = requests.get(url,json={'IMO':records_df['imo']},auth=HTTPBasicAuth(AuthUser, AuthPW))

        if r.status_code == 200:
            AutoServiceRawJson = r.json()
            AutoServiceRaw=AutoServiceRawJson['result']['autologs']
        elif r.status_code != 200:
            logger.error("AutoInputApi Connection Error {}".format(r.status_code), exc_info=True,stack_info =True)
            raise Exception

    except Exception as err:

        logger.error("Error in AutoInputApi {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return('',SRQST)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(AutoServiceRaw,SRQST)






