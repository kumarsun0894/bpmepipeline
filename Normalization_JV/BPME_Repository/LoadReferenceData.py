# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 13:59:04 2020

@author: Sun Kumar
"""
import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')
import numpy as np
def LoadReferenceData(records_df,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=time.time()-start_time
    SRQSTD['Stage']=__name__
    try:
        num_int = ['IMONoVessel']
        num=['IMONoVessel','Loa','Lbp','B','D','Td','Tb','Ts','Ud',\
         'CapacityCargoVessel','DWT','nCHVessel','nCGVessel','Ds','Vs','Dd','Vd',\
             'Ad','Zad','nISBRG','etaS','etaB','rhoswh','TrialIndexMT','nProp','dProp',\
                 'pitchProp','zProp','AeAoProp','TaConSTrial','TfConSTrial','VConSTrial',\
                     'TaSTrial','TfSTrial','VSTrial','nME','nMEST','nAE','nAEST','nBL','nFM',\
                         'nSPM','nSThruster','g','Zaid','kyy','ksiP','ksiN','ksiU','LCVMDOref',\
                             'LCVHFOref','Ck2','CorAllowance','MCRidAE','rhoswSTrial','RHmarginAE','SCOCminMANelME']
        stg=['NameVessel','NoonVerModel','HPVerModel','MEVerModel',\
                  'HPLogConfig','MELogConfig','AELogConfig','NoonLogConfig','HullNoVessel',\
                  'BuildYardVessel','YearBuildVessel','UltimateOwnerVessel','TypeVessel',\
                  'TypeSTAWINDVessel','SuperStructureSTAWINDVessel','TypeCGVessel',\
                  'BulbousBowVessel','Cstern','TransomSternVessel','AutoLoggingVessel',\
                  'vesselsensd','vesselwp','DrawingURLVessel','PowerTypeMT','TypeProp',\
                  'HullNoSTrial','PowerTypeSTrial','WeatherCorSTrial','SPM','SThruster',\
                  'GCU','SG','RG','STurbine','GTurbine','PMotor','AEVerModel']
        str_list=['MakerME','BuilderME','ModelME','SerNoME','CategoryME','TypeME',\
                      'PistConfME','ModeME','DualFuelME','VEGBPME','LIWATME','IARME','MBTME',\
                      'CBTME','AVME','TVME','TCCOSME','MakerTCME','ModelTCME','TypeGovME',\
                      'ModelGovME','TypeLBME','MakerLBME','ModelLBME','CoolantJKTME',\
                      'LibcodeME','HullNoMEST','SerNoMEST','FuelModeMEST','PowerTypeMEST',\
                      'ModeMEST','MakerAE','PistConfAE','ModelAE','DualFuelAE','ECNTRAE',\
                      'LibcodeAE','HullNoAEST','FuelModeAEST','MakerBL','ModelBL','FluidFM',\
                      'EquipmentFM','LocationFM','TypeFM','MakerFM','ModelFM','InterfaceFM',\
                      'ECNTRSPM','LocationSThruster','LoadTypeAEST','RegLBME']
        arr=['BowSection','HullProfile','RudderProfile','TransomSection','Th',\
                 'Dh','LCBh','TaMT','TfMT','VMT','U0MT','P0MT','N0MT','etaD0MT',\
                 'UCorSTrial','PCorSTrial','NCorSTrial','etaeffME','ncME','dPISTME',\
                 'sPISTME','PMCRME','PNCRME','NMCRME','NNCRME','nTCME','NmaxTCME',\
                 'TmaxTCME','muTCME','dTCcompME','SCOCidME','NoME','LCVFOMEST','rhoFOMEST',\
                 'MCRMEST','NMEST','PMEST','NTCMEST','FPIMEST','TFOPPinMEST','FOCMEST',\
                 'SFOCMEST','SFOCisoMEST','TairTCinMEST','pambairMEST','TscavMEST',\
                 'pscavMEST','pindMEST','peffMEST','pmaxMEST','pcompMEST','TegEVoutMEST',\
                 'pegRMEST','TegTCinMEST','pegTCinMEST','TegTCoutMEST','pegTCoutMEST',\
                 'TcwACinMEST','TcwACoutMEST','TairACinMEST','TairACoutMEST','dpairACMEST',\
                 'dpairAFMEST','AuxBlowMEST','MCRNOxMEST','NOxisoMEST','ncAE','dPISTAE',\
                 'sPISTAE','PengnomAE','PgennomAE','NnomAE','PFAE','nTCAE','NoAE',\
                 'LCVFOAEST','rhoFOAEST','MCRAEST','NAEST','PengAEST','PgenAEST','NTCAEST',\
                 'FPIAEST','FOCAEST','SFOCAEST','SFOCisoAEST','TairTCinAEST','pambairAEST',\
                 'pmaxAEST','TegEVoutAEST','TegTCinAEST','TegTCoutAEST','TscavAEST',\
                 'SteamCapacityBL','FaccuracyFM','PnomSThruster','F1iso','F2iso','Kiso',\
                 'TCNomogram','UtrialMT', 'PtrialMT', 'NtrialMT', 'LCVGOMEST', 'TGOinMEST', \
                 'pGOinMEST', 'GOCMEST', 'SGOCMEST', 'SGOCisoMEST', 'LCVGOAEST', 'TFOPPinAEST', \
                 'TGOinAEST', 'pGOinAEST', 'GOCAEST', 'SGOCAEST', 'SGOCisoAEST', 'TcwACinAEST', \
                 'TairACinAEST', 'pscavAEST','Cwph','Cmh','WSAh','dtunSThruster','etagenAEST',\
                 'PgennomAAE','PFnomAE','SCOCbME','SCOCminME','SCOCmaxME','BNLBME','ACCLBME',\
                 'TmCP','UCP','FOCMECP','FwiCP','SSNCP','LCVFOMECP','pcompAEST','TcwACoutAEST','dpairACAEST','dpairAFAEST']

        # with open('input/'+path,'r') as f:

        for i,v in records_df.iterrows():
            dct = {}
            ref = v['rawref']
            ref = ref.split('\n')
            for line in ref:
                line = line.replace('\r','')
                line = line.replace('\n','')

                ar = line.rstrip(';').split('=')

                if ar[0] in stg:
                    dct[ar[0]]=ar[1].rstrip('"').lstrip('"').replace('[','').replace(']','')
                elif ar[0] in num:
                    dct[ar[0]]=float(ar[1].replace('[','').replace(']',''))
                elif ar[0] in num_int:
                    dct[ar[0]]=int(ar[1].replace('[','').replace(']',''))


                elif ar[0] in str_list:
                    if ar[1]=='[]':
                        dct[ar[0]]=np.array([])

                    else:
                        dct[ar[0]]=np.array([i.rstrip('"').lstrip('"') for i in ar[1].replace('[','').replace(']','').split(",")])

                elif ar[0] in arr:
                    ar_tmp=[i for i in ar[1].replace('[','').replace(']','').replace(';',',;,').split(",")]
                    index=[i for i ,X in enumerate(ar_tmp) if X==';']
                    arrr=[ float(i) for i in ar_tmp if i !=';' ]
                    if len(index)>=1:
                        dct[ar[0]]=np.reshape(arrr,(int(len(index)+int(1)),index[0]))
                    elif len(index)==0:
                        dct[ar[0]]=np.array(arrr)
            v['RDSraw'] = dct
            if v['logtype']=='ME':
                EngineIndex = []
                v['nME'] = int(dct['nME'])
                if v['nME'] ==1:
                    EngineIndex.append(1)
                    v['EngineIndex'] = EngineIndex

                elif v['nME'] ==2:
                    EngineIndex.extend([1,2])
                    v['EngineIndex'] = EngineIndex

            if v['logtype']=='AE':
                v['nAE'] = int(dct['nAE'])
                v['AEEngineIndex'] = (np.arange(v['nAE'])+1).tolist()




        # return records_df
    except Exception as err:
        logger.error("{}".format(err),exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return({},SRQST)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(records_df,SRQST)



