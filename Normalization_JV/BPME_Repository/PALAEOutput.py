# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 17:06:06 2020

@author: hadoop
"""
import time
import requests
import logging
import pandas as pd
import io

logger = logging.getLogger('BPMEngine.2.02')

def PALAEOutput(mariBaseUrl,mariAEOut,headers,mariclientCrt,mariclientKey,EngineNo,AEOutput,AEDiagnostics,start_time):
	SRQSTD={}
	SRQSTD['StepTimeLapse']=(time.time()-start_time)
	SRQSTD['Stage']=__name__
	try:
		Output=pd.read_csv(io.StringIO(AEOutput))
		LogID = int(Output.LogID)
		IMO = str(int(Output.IMO))
		DateTimeStart = pd.to_datetime(Output.DateTimeStart,format="%Y-%m-%d %H:%M:%S")
		DateTimeStart = DateTimeStart[0].to_pydatetime()
		DateTimeEnd = pd.to_datetime(Output.DateTimeEnd,format="%Y-%m-%d %H:%M:%S")
		DateTimeEnd = DateTimeEnd[0].to_pydatetime()
		AEOutput=Output.to_csv(index = False)
		aeouturl=mariBaseUrl+mariAEOut
		r=requests.post(aeouturl,data={'IMO':IMO,'LogId':LogID,'StartDate':DateTimeStart,'EndDate':DateTimeEnd,'EngineNo':EngineNo,'AELogOutput':AEOutput,'AELogDiagnostics':AEDiagnostics},headers=headers,cert=(mariclientCrt, mariclientKey))
		if r.status_code==200:
			print("AE Output passed to PAL")
		elif r.status_code!=200:
			logger.error("AE Output not passed to PAL Error code {}".format(r.status_code), exc_info=True,stack_info =True)
			raise Exception
	except Exception as err:
		logger.error("Error in Passing  AE Data to PAL {}".format(err), exc_info=True,stack_info =True)
		SRQSTD['TotalTimeLapse']=time.time()-start_time
		SRQSTD['Remarks']="failed"
		return(SRQSTD)
	else:
		SRQSTD['TotalTimeLapse']=time.time()-start_time
		SRQSTD['Remarks']="complete"
		return(SRQSTD)
