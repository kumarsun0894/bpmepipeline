# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 22:58:05 2020

@author: Sun Kumar
"""
import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')
import requests
from requests.auth import HTTPBasicAuth
def NoonOutputApi(naviBaseUrl,saveNoonKPI,imo,NoonOutput,NoonDiagnostics,NoonOutputExt,start_time,AuthUser,AuthPW):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:

        noonout=naviBaseUrl+saveNoonKPI
        r=requests.post(noonout,json={'IMO':int(imo),'Output':NoonOutput,'Diagnostics':NoonDiagnostics,'Extensions':NoonOutputExt},auth=HTTPBasicAuth(AuthUser, AuthPW))
        if r.status_code==200:
            print("Noon Ouput Successfully Saved in Database")
        elif r.status_code!=200:
            #logger.error("NoonOutputApi Connection Error {}".format(r.status_code), exc_info=True,stack_info =True)
            logger.error("{} {} for URL-{} for IMO-{}".format(r.text,r.status_code,noonout,imo), exc_info=True,stack_info =True)
            raise Exception
    except Exception as err:

        logger.error("Error in NoonOutputApi {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"

        return(SRQSTD)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"

        return(SRQSTD)
