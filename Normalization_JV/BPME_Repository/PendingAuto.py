# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 12:45:12 2020

@author: Sun Kumar
"""

#Function for Fetching pending IMOs,logtype
import requests
from requests.auth import HTTPBasicAuth
import pandas as pd
import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')

def PendingAuto(naviBaseUrl,pendingAuto,start_time,SRQST,AuthUser,AuthPW):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        AutoRecords = pd.DataFrame(columns=['imo', 'logtype','rawref','RDSraw','RDS','RefOut','EngineIndex'])
        pendurl = naviBaseUrl+pendingAuto
        response = requests.get(pendurl,auth=HTTPBasicAuth(AuthUser, AuthPW))
        if response.status_code == 200:
            pending_reports = response.json()



            #########################

            for i in pending_reports['result']:
                if i['autologCount']>0:
                    AutoRecords=AutoRecords.append({'imo': i["imo"],'logtype': 'Auto'}, ignore_index=True)






        elif response.status_code != 200:
            logger.error("{} {} for URL-{}".format(response.text,response.status_code,pendurl), exc_info=True,stack_info =True)
            raise Exception


        # return records_df
    except Exception as err:
        logger.error("Error in Fetching Pending Record {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return({},SRQST)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        if AutoRecords.empty == False:
            return(AutoRecords,SRQST)
        else:
            logger.error("No Pending Records to Perform Operation Further")
            return({},SRQST)
