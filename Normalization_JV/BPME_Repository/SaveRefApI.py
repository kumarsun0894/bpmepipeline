# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 19:04:33 2020

@author: Sun Kumar
"""
import requests
from requests.auth import HTTPBasicAuth
import pandas as pd
import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')
def SaveRefApI(naviBaseUrl,saveReference,mismatched_imo,records_df,start_time,SRQST,AuthUser,AuthPW):
	SRQSTD={}
	SRQSTD['StepTimeLapse']=time.time()-start_time
	SRQSTD['Stage']=__name__
	try:
		saverefurl=naviBaseUrl+saveReference
		for i,v in records_df.iterrows():
			RefDashTable=pd.DataFrame.from_dict(v['RefOut'])
			RefOutput=RefDashTable.to_csv(index = False)
			for i in mismatched_imo:
				if i==v['imo']:

					r=requests.post(saverefurl,json={'IMO':int(i),'ReferenceInput':v['rawref'],'ReferenceCurve':RefOutput},auth=HTTPBasicAuth(AuthUser, AuthPW))
					if r.status_code==200:
						print("Reference Input and Output Saved in Database")
					elif r.status_code!=200:
						logger.error("{} {} for URL-{}".format(r.text,r.status_code,saverefurl), exc_info=True,stack_info =True)
						raise Exception

	#
			if len(mismatched_imo)==0:


				r=requests.post(saverefurl,json={'IMO':int(v['imo']),'ReferenceCurve':RefOutput},auth=HTTPBasicAuth(AuthUser, AuthPW))
				if r.status_code==200:
					print("Reference Output Saved in Database")
				elif r.status_code!=200:
					logger.error("SaveRefApI Connection Error {}".format(r.status_code,r.text), exc_info=True)
					raise Exception
	#
		# return(SRQSTD)
	except Exception as err:
		logger.error("Error in SaveRefApI {}".format(err), exc_info=True)
		SRQSTD['TotalTimeLapse']=time.time()-start_time
		SRQSTD['Remarks']="failed"
		SRQST.append(SRQSTD)
		return(SRQST)

	else:
		SRQSTD['TotalTimeLapse']=time.time()-start_time
		SRQSTD['Remarks']="complete"
		SRQST.append(SRQSTD)
		return(SRQST)
