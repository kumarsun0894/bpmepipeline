# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 12:45:12 2020

@author: Sun Kumar
"""

#Function for Fetching pending IMOs,logtype
import requests
import pandas as pd
import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')

def PendingReports(mariBaseUrl,maripendingUrl,headers,clientCrt,clientKey,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        records_df = pd.DataFrame(columns=['imo', 'logtype','rawref','naviRef','RDSraw','RDS','RefOut','nME','EngineIndex','nAE','AEEngineIndex','LogCount'])
        pendurl = mariBaseUrl+maripendingUrl
        response = requests.get(pendurl, headers=headers, cert=(clientCrt, clientKey))
        if response.status_code == 200:
            pending_reports = response.json()
            #records = {}

            #########################

            for i in pending_reports:

                if (i['hpModelled'] == True and i['hpCount']>0) or (i['noonModelled'] == True and i['noonCount']>0) or (i['meModelled'] == True and i['meCount']>0) or (i['aeModelled'] == True and i['aeCount']>0):
                    if i['hpModelled'] == True and i['hpCount']>0:
                        df1  = records_df.append({'imo': i["imo"],'logtype': 'HP','LogCount':i['hpCount']}, ignore_index=True)
                        records_df = records_df.append(df1,ignore_index =True)
                        pass
                    if i['noonModelled'] == True and i['noonCount']>0:
                        df2 = records_df.append({'imo': i["imo"],'logtype': 'Noon','LogCount':i['noonCount']}, ignore_index=True)
                        records_df = records_df.append(df2,ignore_index =True)
                        pass
                    if i['meModelled'] == True and i['meCount']>0:
                        df3 = records_df.append({'imo': i["imo"],'logtype': 'ME','LogCount':i['meCount']}, ignore_index=True)
                        records_df = records_df.append(df3,ignore_index =True)
                        pass
                    if i['aeModelled'] == True and i['aeCount']>0:
                        df4 = records_df.append({'imo': i["imo"],'logtype': 'AE','LogCount':i['aeCount']}, ignore_index=True)
                        records_df = records_df.append(df4,ignore_index =True)
                        pass
                records_df.drop_duplicates(inplace=True)


        elif response.status_code != 200:
            logger.error("{} {} for URL-{}".format(response.text,response.status_code,pendurl), exc_info=True,stack_info =True)
            raise Exception


        # return records_df
    except Exception as err:
        logger.error("Error in Fetching Pending Record {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return({},SRQST)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        records_df = records_df[records_df['logtype'].isin(['Noon','ME','HP'])]
        records_df.reset_index(drop=True, inplace=True)
        if records_df.empty == False:
            return(records_df,SRQST)
        else:
            # records_dftemp = pd.DataFrame(columns=['imo', 'logtype','rawref','naviRef','RDSraw','RDS','RefOut','nME','EngineIndex','nAE','AEEngineIndex','LogCount'])
            # records_dftemp  = records_dftemp.append({'imo': int(9665683) ,'logtype': 'Noon','LogCount':int(1)}, ignore_index=True)

            # return(records_dftemp,SRQST)

            logger.error("No Pending Records to Perform Operation Further")
            return({},SRQST)
