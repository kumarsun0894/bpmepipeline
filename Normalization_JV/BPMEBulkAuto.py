# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 10:58:49 2020
@author: Subhin Antony 
"""
import pyodbc
from LogSetup import setup_logging
setup_logging()
import logging
logger = logging.getLogger('BPMEngine.2.02')
#directory =os.path.split(Path(PureWindowsPath(os.path.abspath(__file__))))[0]
#os.chdir(Path(PureWindowsPath(directory)))


import numpy as np
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import ast
import time
#         from LogSetup import setup_logging
#         setup_logging()

from Bulk_Api.PendingReports_Bulk import PendingReports_Bulk
from Bulk_Api.PendingReports_BulkAuto import PendingReports_BulkAuto
from BPME_Repository.PALRefApi import PALRefApi
from BPME_Repository.LoadReferenceData import LoadReferenceData
from BPME_Repository.isComplexPropulsion import isComplexPropulsion
from BPME_Transform.ReferenceCurves import ReferenceCurves
from BPME_Repository.VesselFilter import VesselFilter
from BPME_Core.RefDashboards import RefDashboards
from BPME_Repository.LoadServiceData import LoadServiceData
from BPME_Repository.PALAutoApi import PALAutoApi

from BPME_Repository.AutoOutputApi import AutoOutputApi

from BPME_Repository.SaveRefApI import SaveRefApI

#from BPMEngineSingleVessel import BPMEngineSingleVessel
from BulkBPMEngineSingleVessel  import BPMEngineSingleVessel

from config import mariHeaders,mariclientKey,mariclientCrt,mariBaseUrl
from config import mariGetRef
from config import naviBaseUrl,getReference,saveNoon,saveNoonKPI,saveHP,saveHPKPI
from config import saveSRQurl,saveME,saveMEKPI,saveAE,saveAEKPI
from config import saveReference
from config import vessel,fromDate,mariNoonOut,mariHPOut,mariMEOut,mariRefOut,mariAEOut
from BPME_Repository.PALAutoOutput import PALAutoOutput

from BPME_Repository.PALRefOutput import PALRefOutput
from Bulk_Api.NDLSaveHistoricalSRQHeader import NDLSaveHistoricalSRQHeader
from Bulk_Api.NDLGetDataSyncInProgressHSRQ import NDLGetDataSyncInProgressHSRQ
from Bulk_Api.NDLSaveBulkLogInput import NDLSaveBulkLogInput
from Bulk_Api.NDLUpdateHistoricalSRQLogStatus import NDLUpdateHistoricalSRQLogStatus
from Bulk_Api.NDLUpdateBulkSRQHeaderStatus import NDLUpdateBulkSRQHeaderStatus
from config import SaveHistoricalSRQHeader,GetDataSyncInProgressHSRQ,SaveHSRQLogs,UpdateHistoricalSRQLogStatus,UpdateBulkSRQHeaderStatus,UnprocessedOnly,mariGetAuto,saveAutoKPI,mariautopendingUrl,auto_vessel,mariAutoOut
from datetime import datetime
from config import AuthUser,AuthPW
from config import NaviHeaders as navihd
from config import DataPath
from config import mail_sender,sender_pw,em_list,cc_list
from ErrorMail.mkdirr import mkdirr
from ErrorMail.email_df import email_df
from pathlib import Path
from config import  melog,noonlog,hplog,autolog
current_time = datetime.now()
toDate=current_time.strftime('%m/%d/%Y')
def BPMEBulkAuto():
    try:
    
    
        start_time = time.time()
        BPMEngineVersion='2.02'
        headers = ast.literal_eval(mariHeaders)
        NaviHeaders = ast.literal_eval(navihd)
        
        er_file,main_fld=mkdirr(DataPath)
    
        refdfOutput= main_fld+r"\Refdata\rfdf.csv"
        if Path(refdfOutput).is_file():
            rf_df=pd.read_csv(refdfOutput)
        else:
            rf_df=pd.DataFrame(columns = ['imo','ref','ser'])
        rf_df.to_csv(refdfOutput, index=False)
    
        SRQ={}
        SRQ['BPMEVersion']=BPMEngineVersion
        SRQST1=[]
    
        #Invoking Prelimenary ApI for  fetching IMO and LogType
        records_df1,SRQST1 = PendingReports_BulkAuto(mariBaseUrl,mariautopendingUrl,headers,mariclientCrt,mariclientKey,start_time,SRQST1)
          #Applying Vessel Filter
        if auto_vessel == '':
            pass
        else:
            records_df1 = VesselFilter(records_df1,auto_vessel)
    
         
          # NDLSaveHistoricalSRQHeader to update header
        SRQST1=NDLSaveHistoricalSRQHeader(naviBaseUrl,NaviHeaders,SaveHistoricalSRQHeader,records_df1,start_time,SRQST1)
        # GetDataSyncInProgressHSRQ to get records to process
        records_df1,SRQST1=NDLGetDataSyncInProgressHSRQ(naviBaseUrl,GetDataSyncInProgressHSRQ,start_time,SRQST1,AuthUser,AuthPW)
        records_df1=records_df1[records_df1.logtype=="Auto"]
        #Invoking all ServiceData APIs for each row of records_df
        #RecordData =records_df1.groupby(records_df1.imo)
        RecordData =records_df1.groupby(['imo','groupID'])
        #for i,v in records_df.iterrows():
        for rd in RecordData:
            SRQST = SRQST1.copy()
            records_df=rd[1]
            ref_imo = records_df.imo.unique()
            grp_id = records_df.groupID.unique()
            
            
            try:
                
                #Invoking PAL Reference ApI for all IMOs
                records_df1,SRQST  = PALRefApi(mariBaseUrl,mariGetRef,headers,mariclientCrt,mariclientKey,records_df,start_time,SRQST)
                #Loading Reference Data and Extracting NME from Reference Data for all IMOs
                records_df,SRQST = LoadReferenceData(records_df,start_time,SRQST)
                # Check if complex propulsion chain exist and go to next vessel
                
                records_df_filter = records_df.drop_duplicates(subset = ["imo"])
                records_df_filter.reset_index(drop=True,inplace=True)
                if isinstance(records_df, pd.DataFrame):
                    ref=records_df_filter['rawref'][0]
                else:
                    ref='empty'
        
                icp,SRQST=isComplexPropulsion(records_df_filter['RDSraw'][0],start_time,SRQST)
                if icp:
                    print('Complex propulsion chain!')
                # Define reference curves
                records_df_filter,SRQST= ReferenceCurves(records_df_filter,start_time,SRQST)
                #RefDashboards(RDS)
                records_df_filter,SRQST=RefDashboards(records_df_filter,start_time,SRQST)
                mismatched_imo=records_df_filter['imo'].tolist()
    
                SRQST=SaveRefApI(naviBaseUrl,saveReference,mismatched_imo,records_df_filter,start_time,SRQST,AuthUser,AuthPW)
                #Passing Data to MariApps
                ###ucs
                SRQST = PALRefOutput(mariBaseUrl,mariRefOut,headers,mariclientCrt,mariclientKey,records_df_filter,start_time,SRQST)
                if not(ref=="empty"):
                    ref_stat=-1
                    for stat in SRQST:
                        if stat['Stage'] in ['BPME_Transform.ReferenceCurves','BPME_Core.RefDashboards']:
                            if stat['Remarks']=="failed":
                                ref_stat=0
                if not(ref=="empty") and (ref_stat==0):
                    rf_df  = rf_df.append({'imo': int(ref_imo) ,'ref': ref}, ignore_index=True)
                    refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                    rf_df.to_csv(refdfOutput, index=False)
                ###ucE
                records_df1 = records_df.merge(records_df_filter,on='imo',how='right')
                records_df2 = records_df1.drop(['logtype_y','nME_y','EngineIndex_y','nAE_y','AEEngineIndex_y','naviRef_x','RDS_x','RefOut_x','rawref_x','RDSraw_x','LogCount_x', 'groupID_x'],axis=1)
                records_df2.rename(columns = {'logtype_x':'logtype', 'nME_x':'nME','nAE_x':'nAE','AEEngineIndex_x':'AEEngineIndex',
                                           'RDS_y':'RDS','RefOut_y':'RefOut','EngineIndex_x':'EngineIndex','rawref_y':'rawref','naviRef_y':'naviRef','RDSraw_y':'RDSraw','LogCount_y':'LogCount', 'groupID_y':'groupID'}, inplace = True)
        
                #records_df=records_df.iloc[0]
                NumLog=[]
        
                df_len=records_df2.shape[0]
                for i , records_df in records_df2.iterrows():
                
        
                    try:
                        #Processing for Noon Service Data
                        if records_df['logtype'] =='Auto':
                            autodfOutput= main_fld+r"\Auto\autodf.csv"
                            if Path(autodfOutput).is_file():
                                auto_df=pd.read_csv(autodfOutput)
                            else:
                                auto_df=pd.DataFrame(columns = ['imo','ref','ser'])
                            auto_df.to_csv(autodfOutput, index=False)
                            autoimo=records_df['imo']
                            autoref=records_df['rawref']
                            
                            EngineIndex="NULL"
                            SRQSTN=SRQST.copy()
                            SRQN=SRQ.copy()
                            SRQN['logtype']='Auto'
                            #Calling Mariapps API for Fetching Noon Service Data
                            ServiceDataRaw,SRQSTN = PALAutoApi(mariBaseUrl,mariGetAuto,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,start_time,SRQSTN)
                            autoser=ServiceDataRaw
                            #Loading Service Data into Dataframe For processing
                            ServiceData,SRQSTN = LoadServiceData(ServiceDataRaw,records_df['logtype'],start_time,SRQSTN,records_df['imo'])
                            print("Total Number of Auto LogID for IMO-{} to process-{} ".format(records_df['imo'],len(ServiceData)))
                             
                            if len(ServiceData)!=0:
                                
                                ChunkSize=int(ServiceData.shape[0]/500)+1
                                ChunkDf=np.array_split(ServiceData, ChunkSize)
                                SRQSTN=NDLSaveBulkLogInput(naviBaseUrl,SaveHSRQLogs,records_df,ServiceData,EngineIndex,start_time,SRQSTN,AuthUser,AuthPW)
        
                                #ServiceData =ServiceData.groupby(ServiceData.LogID)
                                #Passing Each LogID(row) into BPMEngineSingleVessel
                                for data in ChunkDf:
                                       
                                    data=data.reset_index(drop=True)
                                    SRQSTNSD=SRQSTN.copy()
                                     #Saving Noon Input into Navidium Database by Calling Navidium API
    
                                    #Calling BPMEngine Single Vessel
                                    NoonHeader=[]
                                    AutoOutput,AutoDiagnostics,AutoOutputExt,SRQSTND=BPMEngineSingleVessel(records_df['logtype'],records_df['EngineIndex'],BPMEngineVersion,records_df['RDS'],data,SRQN,start_time,SRQSTNSD)
                                    emailst=SRQSTND.copy()
                                    NoonHeader.append(SRQSTND)
                                    #Saving Noon Output into Navidium Database by Calling Navidim API
                                    #SRQSTND=NoonOutputApi(naviBaseUrl,saveNoonKPI,records_df['imo'],NoonOutput,NoonDiagnostics,NoonOutputExt,start_time)
                                    SRQSTND=AutoOutputApi(naviBaseUrl,saveAutoKPI,records_df['imo'],AutoOutput,AutoDiagnostics,AutoOutputExt,start_time,AuthUser,AuthPW)
                                    NoonHeader.append(SRQSTND)
                                    #Passing Noon Api to Mariapps
                                    NoonHeader.append(PALAutoOutput(mariBaseUrl,mariAutoOut,headers,mariclientCrt,mariclientKey,AutoOutput,AutoDiagnostics,start_time))
                                    if records_df['logtype']=='Auto':
                                        if not(autoser==''):
                                            auto_stat=-1
                                            if emailst['Remarks']=="failed":
                                                auto_stat=0
                                        if not(autoser=='') and (auto_stat==0):
                                            auto_df=pd.read_csv(autodfOutput)
                                            auto_df  = auto_df.append({'imo': int(autoimo) ,'ref': autoref,'ser': autoser}, ignore_index=True)
                                            # refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                                            auto_df.to_csv(autodfOutput, index=False)
                                    #Saving Service Queue Request by Calling Navidium's Service Queue Request API
                                if (NoonHeader[-1]['Remarks']=="complete")and (NoonHeader[-2]['Remarks']=="complete") and (NoonHeader[-3]['Remarks']=="complete"):
                                    UHSRQST=NDLUpdateHistoricalSRQLogStatus(naviBaseUrl,UpdateHistoricalSRQLogStatus,records_df,start_time,SRQST,"Completed",AuthUser,AuthPW)
                                    NumLog.append(int(1))
                                else:
                                    UHSRQST=NDLUpdateHistoricalSRQLogStatus(naviBaseUrl,UpdateHistoricalSRQLogStatus,records_df,start_time,SRQST,"Failed",AuthUser,AuthPW)
                                    NumLog.append(int(0))
        
        
                            else:
                                print("No Auto Log to process for imo {}".format(records_df['imo']))
                                
                
                        
            
                    except:
                        logger.error("Error in BPMEngine for imo {} and logtype {}".format(records_df['imo'],records_df['logtype']),exc_info=True,stack_info =True)
                        UHSRQST=NDLUpdateHistoricalSRQLogStatus(naviBaseUrl,UpdateHistoricalSRQLogStatus,records_df,start_time,SRQST,"Failed",AuthUser,AuthPW)
                        NumLog.append(int(0))
                        if records_df['logtype']=='Auto':
                            if not(autoser==''):
                                auto_stat=-1
                                
                                if emailst['Remarks']=="failed":
                                    auto_stat=0
                            if not(autoser=='') and (auto_stat==0):
                                auto_df=pd.read_csv(autodfOutput)
                                auto_df  = auto_df.append({'imo': int(autoimo) ,'ref': autoref,'ser': autoser}, ignore_index=True)
                                # refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                                auto_df.to_csv(autodfOutput, index=False)
                        pass
                if sum(NumLog)==df_len:
                    UpdateBulkSRQST=NDLUpdateBulkSRQHeaderStatus(naviBaseUrl,UpdateBulkSRQHeaderStatus,records_df['imo'],records_df['groupID'],start_time,SRQST,"completed",AuthUser,AuthPW)
                else:
                    UpdateBulkSRQST=NDLUpdateBulkSRQHeaderStatus(naviBaseUrl,UpdateBulkSRQHeaderStatus,records_df['imo'],records_df['groupID'],start_time,SRQST,"failed",AuthUser,AuthPW)
                    
            except Exception as er:
                logger.error("Error in BPMEngine {} for imo-{} ".format(er,ref_imo),exc_info=True,stack_info =True)
                UpdateBulkSRQST=NDLUpdateBulkSRQHeaderStatus(naviBaseUrl,UpdateBulkSRQHeaderStatus,ref_imo,grp_id,start_time,SRQST,"failed",AuthUser,AuthPW)
                pass
                
    except Exception as err:
    
        logger.error("Error in Auto BulkBPMEngine {} ".format(err),exc_info=True,stack_info =True)
    else:
        try:
            
            em_listt=ast.literal_eval(em_list)
            cc_listt=ast.literal_eval(cc_list)
            email_df(main_fld, DataPath,em_listt,cc_listt,mail_sender,sender_pw,noonlog,hplog,melog,autolog)
        except Exception as err:
            logger.error("Error in sending mail {} ".format(err),exc_info=True)
            print('email failed')
        else:
            print(" Auto BULK BPM Engine 2.02  Execution Completed Successfully")
    
        

if __name__ == '__main__':
    BPMEBulkAuto()
    


