import logging
import time
from pythonjsonlogger import jsonlogger
import socket
import json

RESERVED_FORMATTER_KEY_WORDS = ['asctime','name','msg','message','args','levelname','levelno','pathname','filename','module','exc_info','exc_text','stack_info','lineno','funcName','created','msecs','relativeCreated','thread','threadName','processName','process']
CUSTOM_FORMATTER_KEY_WORDS = ['machinename','extra_data']

class CustomFormatter(logging.Formatter):
    '''
    This class adds the data from extra field into a 
    dictionary variable 'extra_data
    '''
    converter = time.gmtime
    def format(self, record):
        #print("custom")
        know_keywords = RESERVED_FORMATTER_KEY_WORDS + CUSTOM_FORMATTER_KEY_WORDS
        extra = {k:v for k,v in record.__dict__.items()
             if k not in know_keywords}
        if len(json.dumps(extra))>0:
            record.__dict__['extra_data'] = json.dumps(extra)
            # string += " ,'extra_data': " + str(extra)
        else :
            record.__dict__['extra_data'] = None
        #print('extra string')
        #print(record.__dict__['extra_data'])
        return super().format(record)

class UTCFormatter(logging.Formatter):
    '''
    This class changes the time format to UTC and also adds
    the data from extra field as a json to the extra keyword
    '''
    #print("UTCFormatter")
    converter = time.gmtime
    def format(self, record):
        know_keywords = RESERVED_FORMATTER_KEY_WORDS + CUSTOM_FORMATTER_KEY_WORDS
        extra = {k:v for k,v in record.__dict__.items()
             if k not in know_keywords}
        if len(json.dumps(extra))>0:
            record.__dict__['extra_data'] = json.dumps(extra)
            # string += " ,'extra_data': " + str(extra)
        else :
            record.__dict__['extra_data'] = None
        print('extra string')
        print(record.__dict__['extra_data'])
        return super().format(record)

class UTCJSONFormatter(jsonlogger.JsonFormatter):
    converter = time.gmtime

class JSONFormatter(jsonlogger.JsonFormatter):
    pass

class CustomFilter(logging.Filter):
    '''
    This class acts as a context filter for logs. This filter
    adds the machine ID to the log.
    '''
    def filter(self, record):
        record.machinename = None
        try:
            record.machinename = socket.gethostname()
        except:
            record.machinename = None
        return True
    
class DBHandler(logging.Handler):

    """
    A handler class which logs into database for each logging event.
    """

    def __init__(self, driver, servernetwork, database, userid,
                 password, appname, spname='dbo.InsertIntoLog'):
        """
        Initialize the handler.
        """
        logging.Handler.__init__(self)
        if isinstance(driver, str):
            self.driver = driver
        else:
            self.driver = None
        if isinstance(servernetwork, str):
            self.servernetwork = servernetwork
        else:
            self.servernetwork = None
        if isinstance(database, str):
            self.database = database
        else:
            self.database = None
        if isinstance(userid, str):
            self.userid = userid
        else:
            self.userid = None
        if isinstance(password, str):
            self.password = password
        else:
            self.password = None
        if isinstance(appname, str):
            self.appname = appname
        else:
            self.appname = None
        if isinstance(spname, str) and spname.strip():
            self.spname = spname
        else:
            self.spname = 'LOG.InsertIntoLog'

    def emit(self, record):
        """
        Emit a record.
        """
        pyodbc_session = None
        try:
            import pyodbc
            # formatting record
            self.format(record)
            pyodbc_conn = pyodbc.connect(f'Driver={self.driver};\
                                                                     Server={self.servernetwork};\
                                                                     Database={self.database};\
                                                                     UID={self.userid};\
                                                                     PWD={self.password};\
                                                                     MARS_Connection=Yes;')
            pyodbc_session = pyodbc_conn.cursor()
            if self.spname:
                params =[self.appname,record.__dict__['asctime'],record.__dict__['name'],record.__dict__['levelname'],record.__dict__['pathname'],record.__dict__['lineno'],record.__dict__['message'],record.__dict__['exc_text'],record.__dict__['machinename'],record.__dict__['extra_data']]
                # print(""" EXEC """ + self.spname + """ @Application = ?, @LoggedOnUTC = ?,@Logger = ?,@LoggerLevel = ?,@PathName = ?,@Module = ?,@FunctionName = ?,@LineNumber = ?,@Message = ?, @Trace = ? """)
                pyodbc_session.execute(""" EXEC """ + self.spname + """ @Application = ?, @LoggedOnUTC = ?,@ExceptionType = ?,@Class = ?,@TargetSite = ?,@LineNumber = ?,@Message = ?,@StackTrace = ?,@MachineName = ?,@ExtraData = ?""", params)
                pyodbc_session.commit()
        except ImportError:
            print("Make sure the following packages are included : pyodbc")
        except pyodbc.Error as err:
            print(err)
        except Exception as e:
            print(e)
            self.handleError(record)
        finally:
            if pyodbc_session : 
                pyodbc_session.close()
    
