# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 11:32:07 2020

@author: Subhin Antony

% ISOCorrections
% Description:  This function applies the generalized version of ISO
%               corrections as suggested by MAN manual, section 706-06.
%
% Input:        A       [°C or bar] measured variable to be corrected (vector)
%               T1      [°C]        air temperature at TC inlet (vector)
%               T2      [°C]        cooling water temperature at AC inlet (vector)
%               F1      [-]         constant related to the air temperatureat TC inlet (scalar)
%               F2      [-]         constant related to the cooling water temperature at AC inlet (scalar)
%               K       [K or bar]  constant related to the absolute conditions (scalar)
%
% Output:       Aiso
%
% Note 1:       The current method is a simplified application of the ISO
%               correction and is suggested only for 4 variables, i.e. A
%               can be: Texh, pscav, pcomp and pmax.
%
% Note 2:       According to MAN performance sheets (excel based), there
%               are more advanced alogriothms to perfrom the ISO
%               corrections. Additionally many more variables (not only 4)
%               are corrected.
%
% See also:     ReferenceCurves
"""
import numpy as np
def ISOCorrections(A,T1,T2,F1,F2,K):
    Aiso=A+((K+A)*(F1*(T1-25)+F2*(T2-25)))
    Aiso=Aiso.astype(float)
    Aiso[np.isnan(Aiso)]=-1
    Aiso[Aiso<0]=np.nan

    return(Aiso) 
