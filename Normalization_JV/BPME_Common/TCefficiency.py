# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 15:48:31 2020

@author: Subhin Antony

% TCefficiency
% Description:  The TC efficiency is calculated based on the guidelines
%               included in MAN manuals.
% 
% Input:        pambair   [mbar]    ambient air pressure (vector)
%               dpairAC   [mmWC]    air pressure drop across AC (vector)
%               TairTCin  [°C]      air temperature at TC inlet (vector)
%               NTC       [RPM]     TC rotational speed (vector)
%               pscav     [bar]     scavenge air pressure (vector)
%               pegR      [bar]     exhaust gas pressure at receiver (vector)
%               pegTCout  [mmWC]    exhaust gas pressure at TC outlet (vector)
%               TegTCin   [°C]      exhaust gas temperature at TC inlet (vector)
%               dTCcomp   [m]       compressor diameter (scalar)
%               muTC      [-]       compressor slip factor (scalar)
%
% Output:       etaTCtot  [-]       total TC efficiency (vector)
%               etaTCcomp [-]       compressor efficiency (vector)
%               etaTCturb [-]       turbine efficiency (vector)
%
% See also:     ReferenceCurves, MEManipulateServiceData

"""
import numpy as np
def TCefficiency(pambair,dpairAC,TairTCin,NTC,pscav,pegR,pegTCout,TegTCin,dTCcomp,muTC):

	
	
	
	
	
	#Estimated ratio of air mass flow through compressor over exhaust gas mass
	#flow through turbine as per MAN manuals.
	mA_mX=0.9817						#[-]
	
	#Units conversion
	T1=TairTCin+273.15				 #[K]
	T2=TegTCin+273.15				  #[K]
	p1=pambair/1000				  #[bar]
	p2=dpairAC*9.8067*1e-5			 #[bar]
	p3=pegTCout*9.8067*1e-5		   #[bar]
	
	#Intermediate calculations
	R1=(p1+pscav+p2)/p1   #compressor pressure ratio %???????should be added the dpairAF (airfilter) here?????
	R2=(p1+p3)/(p1+pegR)  #turbine pressure ratio
	UTC=np.pi*dTCcomp*NTC
	
   #Total efficiency calculation
#	etaTCtot=0.9265*mA_mX*T1*(R1**0.286-1)/(T2*(1-R2**0.265))
	etaTCtot = 0.9265*mA_mX*T1*np.divide((R1**0.286-1),(T2.astype('float64')*(1-R2**0.265)),out=np.full_like((R1**0.286-1),float("nan")),where = (T2.astype('float64')*(1-R2**0.265))!=0)
	#Compressor efficiency calcualtion
#	etaTCcomp=3628800*T1*(R1**0.286-1)/(muTC*UTC**2)
	etaTCcomp=np.divide((3628800*T1*(R1**0.286-1)),(muTC*UTC.astype('float64')**2),out= np.full_like(3628800*T1*(R1**0.286-1),float("nan")),where =(muTC*UTC.astype('float64')**2)!=0)

	#Turbine efficiency calculation
#	etaTCturb=etaTCtot/etaTCcomp
	etaTCturb=np.divide(etaTCtot,etaTCcomp,out=np.full_like(etaTCtot,float("nan")),where=etaTCcomp!=0)


	return etaTCtot,etaTCcomp,etaTCturb
	
   
	