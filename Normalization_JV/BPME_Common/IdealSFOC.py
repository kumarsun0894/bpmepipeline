# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 17:12:28 2020

@author: SunKumar

% IdealSFOC
% Description:  This function calculates the ideal SFOC for a given MCR.
%
% Input:        mcr       [%]       MCR percentage in service (vector)
%               SFOCisoST [g/kWh]   SFOCiso from shop test (vector)
%               MCRST     [%]       MCR percentage from shop test (vector)
%
% Output:       SFOCisoid [g/kWh]   ideal SFOC for in service mcr (vector)
%
% See also:     IdealDeliveredPower, IdealFOC, IdealPropulsiveEfficiency,
%               IdealRPM, IdealSpeed


"""
import numpy as np
#from scipy.interpolate import pchip
from BPME_Common.pchipwrap import pchipwrap as pchip

def SFOCHighExtrapolation(mcr,SFOCST,MCRST):
    #First point to define linear curve between x1, usually x1=75
    idx1 = SFOCST.tolist().index(min(SFOCST))
    SFOCST1=min(SFOCST)
    MCRST1=MCRST[idx1]
    #Second point to define linear curve x2=100
    MCRST2=max(MCRST)
    SFOCST2=SFOCST[MCRST==MCRST2]
    #If the minimum value is at 100% MCR, then the extrapolation will be a horizontal line
    if MCRST1==MCRST2:
        lamda=0
    else:
        lamda=(SFOCST2-SFOCST1)/(MCRST2-MCRST1)
    beta=SFOCST2-lamda*MCRST2
    SFOCH=lamda*mcr+beta
    return SFOCH

def SFOCLowExtrapolation(mcr,SFOCST,MCRST):
    #Normalized (percentage) distance definition
    dnorm=0.05

#Normalize data
    MCRnorm=MCRST/np.nanmax(MCRST)
    SFOCnorm=SFOCST/np.nanmax(SFOCST)

#Define normalized interpolation data
    mcrnorm=np.arange(MCRnorm[0],MCRnorm[-1],0.001)
    sfocn=pchip(MCRnorm,SFOCnorm)#SFOCnorm
    sfocnorm = sfocn(mcrnorm)

#Find normalized offset
    for i in range(0,len(mcrnorm)):
           Dnorm=np.sqrt((mcrnorm[i]-mcrnorm[0])**2+(sfocnorm[i]-sfocnorm[0])**2)
           if Dnorm>dnorm:
               Onorm=mcrnorm[i]-MCRnorm[0]
               break

    offset=Onorm*np.max(MCRST)
    #Lower end MCR points definitions
    MCRST1=MCRST[0]
    MCRST2=MCRST[0]+offset
    
    #Lower end power points definitions
    SFOCST1=SFOCST[0]
    SFOCST2x=pchip(MCRST,SFOCST)
    SFOCST2 = SFOCST2x(MCRST2)
    
    #Find b coeeficient for smooth continuation towards lower end
    b0=np.log(SFOCST2/SFOCST1)/np.log(MCRST1/MCRST2)
    
    #Lower end coefficients definitions
    p=np.polyfit(np.array([10,MCRST1]),np.array([0.106,b0]),1)
    b=np.polyval(p,mcr)
    a=SFOCST1*MCRST1**b
    
    #Lower end extrapolation function
    SFOCL=a/mcr**b
    return SFOCL

def IdealSFOCisoScalar(mcr,SFOCisoST,MCRST):


#np.any(np.bitwise_and(mcr<MCRST[0],mcr>=1))
    if mcr<MCRST[0] and mcr>=1:
        SSFOCisoid=SFOCLowExtrapolation(mcr,SFOCisoST,MCRST)
            
    elif mcr>=MCRST[0] and mcr<=MCRST[-1]:
        SSFOC=pchip(MCRST,SFOCisoST)
        SSFOCisoid = SSFOC(mcr)
                
    elif (mcr>MCRST[-1]) and (mcr<=110):
        SSFOCisoid=SFOCHighExtrapolation(mcr,SFOCisoST,MCRST)
    elif mcr>110:
        SSFOCisoid=float('NaN')
        print('Ideal SFOCiso error: MCR too high!')
                    
    else:
        SSFOCisoid=float('NaN')
        print('Ideal SFOCiso error!')
    
    
    return SSFOCisoid

def IdealSFOC(mcr,SFOCisoST,MCRST):
    if mcr.ndim==1:
        mcr=mcr.reshape(1,-1).T

    if np.isnan(mcr.astype('float64')).all() == True:
        
        SFOCisoid= float('NaN')
        
        print('MCR is empty! (IdealSFOC)')
        return SFOCisoid
        
    #if only one SFOC point then consider horizontal line
    elif SFOCisoST[np.argwhere(np.isnan(SFOCisoST)==False)].size == 1:
        SFOCisoid = (SFOCisoST[np.argwhere(np.isnan(SFOCisoST)==False)]*np.ones(mcr.shape))
        return SFOCisoid
    
    elif np.all(np.isnan(SFOCisoST)):
        SFOCisoid=np.empty(mcr.shape)
        SFOCisoid[:]=np.nan
        return SFOCisoid
    #Memory pre-allocation
    SFOCisoid=np.empty(mcr.shape)
    SFOCisoid[:]=np.nan
    
    
    #SFOCisoid = []
    for i in range(0,len(mcr)):
        SFOCisoid[i]=IdealSFOCisoScalar(mcr[i],SFOCisoST,MCRST)
        #SFOCisoid.append(sfocisoid)
        
    
    
    # SFOCisoid=np.array(SFOCisoid).reshape(1,-1).T
    
    return SFOCisoid





