# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 12:11:23 2020

@author: SunKumar

% BowHalfAngle
% Description:  This function calculates the half angle of entrance which
%               is used in Holtrop method
%
% Input:        Cb      [-]     block coefficient for each draught (vector)
%
% Output:       ie      [deg]   estimated half angle of entrance at the waterline (svector)
%
% Note 1:       The function used before was proposed in Holtrop 1982 paper
%               but due to its sensitivity it was decided to change to a
%               simpler and more robust influenced by Molland et al. and
%               enhanced by Nicholas Kouvaras.
%
% Note 2:       The original function by Holtrop 1982 was:
%               ie=1+89*exp(-(Lwl/B).^0.80856.*(1-Cwp).^0.30484.*(1-Cp-0.0225*lcb).^0.6367.*(Lr/B).^0.34574.*(100*V./Lwl.^3).^0.16302)
%               However, we observed a sensitivity on this term:
%               (1-Cp-0.0225*lcb).^0.6367
%               if this term becomes negative, then it generates complex
%               angles, therefore we added the absolute funtion:
%               ie=1+89*exp(-(Lwl/B).^0.80856.*(1-Cwp).^0.30484.*abs(1-Cp-0.0225*lcb).^0.6367.*(Lr/B).^0.34574.*(100*V./Lwl.^3).^0.16302)
%               However, this may lead to inaccuracies and by that time 
%               it was necessary to be further investigated.%
%
% See also:     ResistanceHoltrop

"""

import numpy as np
#from scipy.interpolate import pchip
import scipy.interpolate as ipchip
def BowHalfAngle(Cb):
	
	# %Table from Molland et al, 2011. It is extended with assumed values for
	# %Cb=0 and Cb=1 for a simple extrapolation rules by Nicholas Kouvaras
	IEE=np.array([0,8,10,20,35,90])
	CBB=np.array([0,0.55,0.6,0.7,0.8,1])
	pc = ipchip.PchipInterpolator(CBB,IEE)
	ie = pc(Cb)
	# the below "manipulation" was due to the old Holtrop's method. This may be
	# obsolete and need to be removed.
	# add a diagnostic for complex and out of range numbers generation due to invalid input (e.g. huge displacement)
	ie[np.iscomplex(ie) == True] = 0 # %filter complex numbers
	ie[ie<0]=0 #%filter for negative numbers
	ie[ie>90]=90 #%filter for angles larger than 90°
	return ie


    