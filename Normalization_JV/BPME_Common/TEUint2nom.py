# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 00:58:46 2020

@author: SubhinAntony

% TEUint2nom
% Description:  This function translates the values stored in variable TEU
%               from integers: 0, 1 or 2 to strings: 'NO', 'A', 'FULL'.
%
% Input:        TEU     [-]     containers quantity on main deck (vector)
%
% Output:       teu     [-]     containers quantity on main deck (cell array)
%
% See also:     ManipulateServiceData, STAWIND
"""
import numpy as np

def TEUint2nom(TEU):
    teu=np.empty(np.shape(TEU),dtype=object)
    teu[:] = np.nan
    for i in range(0,len(TEU)):
        if TEU[i]==0:
            teu[i]='NO'
        elif TEU[i]==1:
            teu[i]='A'
        elif TEU[i]==2:
            teu[i]='FULL'
        else:
            teu[i]=np.nan
            print('error: not acceptable value for TEU variable')
    return teu

#TEU = np.array([0, 2, 1, 1, 1, 1, 1, 1, 1, 1])
#rslt = TEUint2nom(TEU)
#print(rslt)