# -*- coding: utf-8 -*-
'''

Created on Mon Mar 30 13:44:41 2020

@author: Subhin Antony

NOTE1   : The formulas were extracted from an excel: ASTM_Tables.xls
          by Oleg Boriskin
NOTE    : The formula which calculates the corrected density based on
          the VCF is: rhoFOcor=(rhoFOME-1.1)*VCF
NOTE3   : The density in vacuum subtracted by 1.1kg/m3 is known as
          density in air.
'''
import numpy as np

def VolumeCorrectionFactor(rhoFO,TFO):
    K0=186.9696
    K1=0.486218
    deltaT=TFO-15
    # a15=K0/rhoFO**2+K1/rhoFO
    g=np.divide(K0,rhoFO**2, out=np.zeros_like(rhoFO), where=rhoFO!=0)
    # g=np.divide(K0,rhoFO,out=np.zeros_like(K0),where=(rhoFO)!=0)
    f=np.divide(K1,rhoFO,out=np.zeros_like(rhoFO),where=rhoFO!=0)
    a15=g+f
    gk=-a15*deltaT*(1+0.8*a15*deltaT)
    VCF=np.exp(gk.astype('float64'))
    
    return VCF




#
#rhoFO= np.array([[1010.],
#       [1000.],
#       [1007.],
#       [1007.],
#       [1007.],
#       [1007.],
#       [1009.],
#       [1009.],
#       [ 840.],
#       [1009.]])
#TFO=np.array([[ 98.],
#       [ 98.],
#       [ 95.],
#       [ 96.],
#       [ 97.],
#       [ 96.],
#       [ 95.],
#       [ 96.],
#       [ 24.],
#       [ 96.]])
#Vf = VolumeCorrectionFactor(rhoFO,TFO)
#print(Vf)