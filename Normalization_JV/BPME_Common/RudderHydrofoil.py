# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 13:32:49 2020

@author: akhil.surendran
"""
import numpy as np
#from shapely.geometry import Polygon

#from sympy import polygon,Point

def RudderHydrofoil(RudderProfile):
        xr=RudderProfile[:,0]
        zr=RudderProfile[:,1]
        xrmin=min(xr)
        xrmax=max(xr)
        zrmin=min(zr)
        zrmax=max(zr)
        
        xmin_idx=np.flatnonzero(xr<(xrmax-xrmin)/2+xrmin)
        xmax_idx=np.flatnonzero(xr>(xrmax-xrmin)/2+xrmin)
        zmin_idx=np.flatnonzero(zr<(zrmax-zrmin)/2+zrmin)  
        zmax_idx=np.flatnonzero(zr>(zrmax-zrmin)/2+zrmin)
        
        Xcor=np.array(min(xr[zmin_idx]))
        Xcor = np.append(Xcor,max(xr[zmin_idx]))
        Xcor=np.append(Xcor,max(xr[zmax_idx]))
        Xcor=np.append(Xcor,min(xr[zmax_idx]))
        Zcor=np.array(min(zr[xmin_idx]))
        Zcor=np.append(Zcor,min(zr[xmax_idx]))
        Zcor=np.append(Zcor,max(zr[xmax_idx]))
        Zcor=np.append(Zcor,max(zr[xmin_idx]))

        ch1=Xcor[1]-Xcor[0];
        ch2=Xcor[2]-Xcor[3];
        ch=np.mean([ch1,ch2])
        
        th=0.15*ch;
        naca15=np.array([[1.0000 ,0.00158]
              ,[ 0.9500,0.01008]
              ,[ 0.9000,0.01810]
              ,[ 0.8000,0.03279]
              ,[ 0.7000,0.04580]
              ,[ 0.6000,0.05704]
              ,[ 0.5000,0.06617]
              ,[ 0.4000,0.07254]
              ,[ 0.3000,0.07502]
              ,[ 0.2500,0.07427]
              ,[ 0.2000,0.07172]
              ,[ 0.1500,0.06682]
              ,[ 0.1000,0.05853]
              ,[ 0.0750,0.05250]
              ,[ 0.0500,0.04443]
              ,[ 0.0250,0.03268]
              ,[ 0.0125,0.02367]
              ,[ 0.0000,0.00000]
              ,[ 0.0125,-0.0236]
              ,[ 0.0250,-0.0326]
              ,[ 0.0500,-0.0444]
              ,[ 0.0750,-0.0525]
              ,[ 0.1000,-0.0585]
              ,[ 0.1500,-0.0668]
              ,[ 0.2000,-0.0717]
              ,[ 0.2500,-0.0742]
              ,[ 0.3000,-0.0750]
              ,[ 0.4000,-0.0725]
              ,[ 0.5000,-0.0661]
              ,[ 0.6000,-0.0570]
              ,[ 0.7000,-0.0458]
              ,[ 0.8000,-0.0327]
              ,[ 0.9000,-0.0181]
              ,[ 0.9500,-0.0100]
              ,[1.0000, -0.00158]])

        col1= ch*naca15[:,0]
        col2= ch*naca15[:,1]
        x=zip(col1,col2)
        #Afoil = Polygon(x).area
        Afoil = 0.5*np.abs(np.dot(col1,np.roll(col2,1))-np.dot(col2,np.roll(col1,1)))
        
        t= [th,ch,Afoil]
#        print(t)
        return t
#    RudderProfile=np.array([[-4.04,-3.97,0.95,1.43,2.55,-4.04],[12.60,0.20,0.20,0.63,11.83,12.60]])
#    RudderProfileT = np.transpose(RudderProfile)
#    t = RudderHydrofoil(RudderProfileT)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
