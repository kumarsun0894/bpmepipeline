# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 12:05:38 2020

@author: Subhin Antony

% This function is present only in BPME V 2.xx. For finding the index of an array.

"""
import numpy as np

def find(li,s,pos):
    matched_index=[]
    
    for i in range(0,len(li)):
        if s==li[i]:
            matched_index.append(i)
    
            
    if len(matched_index)>0:
        if pos =='first':
            
            return(np.array(matched_index[0]))
        elif pos =='last':
            
                
            return(np.array(matched_index[-1]))
        else:
            a=[]
            return(np.array(a))
            
            
        
    else:
        a=np.array([])
        return(a)
        
