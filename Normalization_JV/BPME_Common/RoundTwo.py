# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 09:55:34 2020

@author: SubhinAntony
"""
import numpy as np
import pandas as pd
def RoundTwo(arr):
    NewArr= arr+0.001
    RodArr= np.around(NewArr,2)
    return(RodArr)
    


if __name__=="__main__":
    RoundTwo(np.array([1.065,float('Nan')]))