# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 11:57:49 2020

@author: SubhinAntony

% WaterPlaneArea
% Description:  This function calculates the waterplane area based on the
%               waterplane area coefficient from hydrostatic tables. If
%               hydrostatic tables are unavailable, then an approximation
%               is used (Cwp=0.82) based on Papanikolaou, p.40.
%
% Input:        Tm      [m]     actual mean draught (vector)
%               Lwl     [m]     waterline length (vector)
%               B       [m]     ship breadth (scalar)
%               Th      [m]     mean draught from hydrostatic tables (vector)
%               Cwph    [-]     waterplane area coefficient from hydrostatic tables (vector)
%
% Output:       WPA     [m2]    actual waterplane area (vector)
%               Cwp     [-]     actual waterplane area coefficient (vector)
%
% See also:     ManipulateServiceData


"""
import numpy as np
import pandas as pd
from scipy.interpolate import pchip
def WaterPlaneArea(Tm,Lwl,B,Th,Cwph):
    if ~(np.any(np.isnan(Cwph))):
        
        #sort
        dataset = pd.DataFrame({'Th':Th,'Cwph':Cwph},columns=['Th', 'Cwph'])
        dataset = dataset.sort_values('Th')
        Th=np.array(dataset['Th']).astype('float64')
        Cwph=np.array(dataset['Cwph']).astype('float64')
        
        pcfn=pchip(Th,Cwph)
        Cwp=pcfn(Tm)
    elif np.any(np.isnan(Cwph)):
        Cwp=0.82*np.ones(np.size(Tm)) #see Papanikolaou, p.40
    else:
        print('Cwp error!')
        
        Cwp=np.nan
    WPA=Cwp*Lwl*B
    return(WPA,Cwp)
    
        
        
    