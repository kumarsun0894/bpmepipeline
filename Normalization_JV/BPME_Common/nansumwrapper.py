# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 01:09:07 2020

@author: SubhinAntony

% This is present only in BPME V 2.xx. For finding the nansum() equivalent for MatLab function.
"""
import pandas as pd
import numpy as np
def nansumwrapper(vr, **kwargs):
    #print(vr)
    
    #a=a.astype('float64')
    #print(pd.isnull(vr).all(**kwargs))
    if np.any(pd.isnull(vr).all(**kwargs)):
        index=pd.isnull(vr).all(**kwargs)
        sm=np.nansum(vr, **kwargs)
        sm[index]=float('NaN')
        #print(sm)
        return(sm)
    else:
        #print(np.nansum(vr, **kwargs))
        return np.nansum(vr, **kwargs)