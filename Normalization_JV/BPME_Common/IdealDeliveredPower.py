# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 11:55:09 2020

@author: Sun Kumar


"""

import numpy as np
import pandas as pd
# from BPME_Repository import BPMEngineSingleVessel as BSV
from BPME_Common import LastElement as LE 
#from scipy.interpolate import pchip
from BPME_Common.pchipwrap import pchipwrap as pchip


def PowerSpeedLowExtrapolation(U,Pm,Um,ExtrapolationMethod):


    #Normalized (percentage) distance definition
    dnorm=0.05

    #Normalize data
    Unorm=Um / max(Um)
    

    Pnorm=Pm / np.max(Pm)

    #Define normalized interpolation data
    unorm=np.arange(Unorm[0],Unorm[-1],0.001)
    pche=pchip(Unorm,Pnorm)
    pnorm = pche(unorm)

    #Find normalized offset
    for i in range(0,len(unorm)):
        Dnorm=np.sqrt((unorm[i] - unorm[0]) ** 2 + (pnorm[i] - pnorm[0]) ** 2)
        if Dnorm > dnorm:
            Onorm=unorm[i] - Unorm[0]

            break
    
    #offset=np.dot(Onorm,max(Um))
    offset=Onorm*max(Um)
    
    #Ustep=0.01;
    
    if ExtrapolationMethod == 'A':
        #Lower end speed points definitions
        Um1=Um[0]

        Um2=Um[0] + offset

        Pm1=Pm[0]

        splk=pchip(Um,Pm)
        Pm2 = splk(Um2)

        b=np.log(Pm2 / Pm1) / np.log(Um2 / Um1)

        a=Pm1 / Um1 ** b

        PL=np.dot(a,U ** b)

       
    # else:
    #     if ExtrapolationMethod == 'L':
    #         #Lower end speed range definition
    #         UR=arange(Um(1),Um(1) + offset,Ustep)

    #         PR=pchip(Um,Pm,UR)

    #         c0=concat([1,1])

    #         F=lambda c=None,u=None: dot(c(1),u ** c(2))

    #         c=lsqcurvefit(F,c0,UR,PR)

    #         PL=F(c,U)

    else:
        print('Power-Speed low extrapolation method error!')
        print('')
        PL=float('nan')

    return PL
    
#if __name__ == '__main__':
#    pass
#    
#    

def PowerSpeedHighExtrapolation(U,Pm,Um,ExtrapolationMethod):
    #Normalized (percentage) distance definition
    dnorm=0.05
    #Normalize data
    Unorm=Um / max(Um)
    Pnorm=Pm / max(Pm)
    #Define normalized interpolation data
    unorm=np.arange(Unorm[0],Unorm[-1],0.001)
    spl=pchip(Unorm,Pnorm)
    pnorm=spl(unorm)
    #Find normalized offset
    for i in range(len(unorm)-1,-1,-1):
        Dnorm=np.sqrt((unorm[-1] - unorm[i]) ** 2 + (pnorm[-1] - pnorm[i]) ** 2)
        
        if Dnorm > dnorm:
            Onorm=Unorm[-1] - unorm[i]
            break
    offset=np.dot(Onorm,max(Um))
    
    #Ustep=0.01;
    if ExtrapolationMethod == 'A':
        #High end speed points definitions
        Um1=Um[-1] - offset
        Um2=Um[-1]
        spln=pchip(Um,Pm)
        Pm1 = spln(Um1)
        Pm2=Pm[-1]
        b=(np.dot(Pm1,Um2 ** 3) - np.dot(Pm2,Um1 ** 3)) / (Um2 ** 3 - Um1 ** 3)
        a=(Pm1 - b) / Um1 ** 3
        PH=np.dot(a,U ** 3) + b
        
    # else:
    #     if ExtrapolationMethod == 'L':
    #         #High end speed range definition
    #         UR=arange(Um(end()) - offset,Um(end()),Ustep)
    #         PR=pchip(Um,Pm,UR)
    #         c0=concat([1,1])
    #         F=lambda c=None,u=None: dot(c(1),u ** 3) + c(2)
    #         c=lsqcurvefit(F,c0,UR,PR)
    #         PH=F(c,U)
            
    else:
        print('Power-Speed high extrapolation method error!')
        print('')
        PH=float('nan')
    
    return PH
    
#if __name__ == '__main__':
#    pass


def IdealDeliveredPowerScalar(U,T,PDHid,Tref,Pref,Uref,Tb,Ts):
    

  

    
    #Check if T is within aceeptable range
    if  pd.isnull(T):#T.size ==0 or
        print('T is unavailable!')
        PPDid=float('nan')

        return PPDid
    
    elif (T < Tb - 0.01) or T > (Ts *1.042):
        print('T is out of [Tb,Ts] range!')
        PPDid=float('nan')
        return PPDid
    
    #Standard approach
    if T >= np.min(Tref) and T <= np.max(Tref):
        #Identify Uref size
         #imax=Uref.size


            
        try:
             imax,j = Uref.shape
        except:
             imax = 1
             # pass
        
        last=LE.LastElement(Uref).astype(int)+1
        #last = [8]

#         print("Tref.shape",Tref.shape)
#         print("Tref",Tref)
        PD=np.full(Tref.shape,np.nan)
#         PD = np.array([[float("NaN")]])
        for i in range(0,imax):
            if U < Uref[i,0]:



#                 print("PD",PD)
#                 print("PD",PD.shape)
            
                PD[i]=PowerSpeedLowExtrapolation(U,Pref[i,0:int(last[i])],Uref[i,0:int(last[i])],'A')

            else:
                if U >= Uref[i,0] and U <= Uref[i,last[i]-1]:
                    
                    pch=pchip(Uref[i,0:int(last[i])],Pref[i,0:int(last[i])])
                    PD[i] = pch(U)

                else:
                    if U > Uref[i,last[i]-1]:
                        PD[i]=PowerSpeedHighExtrapolation(U,Pref[i,0:int(last[i])],Uref[i,0:int(last[i])],'A')

                    else:
                        print('IdealDeliveredPower error!')
        if imax > 1:
            for i in range(0,imax - 1):
                if T >= Tref[i] and T <= Tref[i + 1]:
                    PPDid=np.multiply((T - Tref[i]),(PD[i + 1] - PD[i])) / (Tref[i + 1] - Tref[i]) + PD[i]

        else:
            PPDid=PD
            
        #Holtrop approach
    else:
        if T < min(Tref) or T > max(Tref):
            print('T is out of model test range! Holtrop method will be used!')
            PPDid=PDHid

    
    #Negative values filtering (which generates complex numbers in some calculations)
    if float(PPDid) < 0:
        PPDid=float('nan')

    
    return PPDid
    
#if __name__ == '__main__':
#    pass

def IdealDeliveredPower(U,T,PDHid,Tref,Pref,Uref,Tb,Ts):
   
    
#     print("U",U)
#     print("U.shape",U.shape)

    
#     print("T",T)
#     print("PDHid",PDHid)
#     print("Tref",Tref)
#     print("Pref",Pref)
#     print("Uref",Uref)
#     print("Tb",Tb)
#     print("Ts",Ts)
    if Uref.ndim==1:
        Uref=Uref.reshape(1,-1)
    if Pref.ndim==1:
        Pref=Pref.reshape(1,-1)
#    if Tref.ndim ==1:
#        Tref = Tref.reshape(1,-1)
#     print("TREF",Tref)
#     print("TREF.SHAAPE",Tref.shape)
    
    #Memory pre-allocation
    PDid=np.full(U.shape,np.nan)

    
    for i in range(0,len(U)):
        
        PDid[i]=IdealDeliveredPowerScalar(U[i],T[i],PDHid[i],Tref,Pref,Uref,Tb,Ts)
    return PDid
    
#if __name__ == '__main__':
#    pass
#U=np.array([11.43,19.04,21.96,22.64,23.32])
#TaSTrial=9.43
#TfSTrial=5.58
#T = 7.45
#T = np.full(U.shape,T)
#Tb=7.45
#Ts=13.90
#PDHid = np.full(U.shape,np.nan)
#Tref = np.array([7.45  , 12.  , 13.9])
#     #Uref=  np.array([17.00,18.00,19.00,20.00,21.00,22.00,23.00,24.00])
#     #Pref = np.array([8384,9885,11599,13558,15861,18626,21945,25774])
#
#Uref=  np.array([[17.00,18.00,19.00,20.00,21.00,22.00,23.00,24.00],
#                   [16.00,18.00,20.00,21.00,21.50,22.00,23.00,24.00],
#                [16.00,18.00,20.00,21.00,21.50,22.00,23.00,float('NaN')]])
#     
#Pref = np.array([[8384,9885,11599,13558,15861,18626,21945,25774],
#                   [7555,10529,14497,17135,18682,20408,24354,28759],
#                   [8867,13039,18760,22426,24531,26874,32471,float('NaN')]])
#  
#PDid = IdealDeliveredPower(U,T,PDHid,Tref,Pref,Uref,Tb,Ts)