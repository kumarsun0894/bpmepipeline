    # -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 12:02:30 2020

@author: Subhin Antony

% TransomArea
% Description:
%
% Note3:A new formula is used to estimate the transom by using the end
%       points easily identified in the GA plan, i.e.: ymin=0,
%       ymax=half-breadth at deck, zmin and zmax from hull profile.
%       The formula used is z=a*y^5+b.
%
% Note: The order (currently 6th) can be fine-tuned later.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Shiplab estimations
% -------------------
% Atr=0.95*(Ta-Ta*0.9225)*B*0.89*TRE
%
% Note1: TRE is a boolean variable to indicate the existence or absence of
%        a transom stern.
%
% Note2: The formula above, after algebraic manipulation yields:
%        Atr=0.065526*Ta*B
%
% Source: http://shiplab.hials.org/app/holtrop/

"""
from BPME_Common import intersections as inn
import numpy as np
import pandas as pd
from scipy.interpolate import pchip
#import scipy.interpolate as ipchip

     
def TransomArea(Ta,Tf,Lbp,B,HullProfile,TransomSection,TRE):
        
        #Check if transom exists
    if 'No' in TRE: 
        ztr_actual=np.NaN
        ytr_actual=np.NaN
        Atr=np.zeros(np.size(Tf))
    elif 'Yes' in TRE:
        #Define the transom curve
        #Check if 2 points only of the Transom are provided and make estimate based on a 5th power curve
        if np.all(~(np.isnan(TransomSection))) and len(TransomSection[:,0])==2:
            order=5
            
            a=(np.max(TransomSection[:,1])-np.min(TransomSection[:,1]))/np.max(TransomSection[:,0])**order
            b=np.min(TransomSection[:,1])
            ytrt=np.linspace(0,np.max(TransomSection[:,0]),10)
            ytr=np.transpose(ytrt)
            ztr=a*ytr**order+b
        #Check if a full Transom Section is provided
        elif np.all(~(np.isnan(TransomSection))) and len(TransomSection[:,0])>2:
            ytr=TransomSection[:,0]
            ztr=TransomSection[:,1]
        else:
            ytr=np.nan
            ztr=np.nan
        # sort
        if not(isinstance(ztr, float)):
            if np.any(~(ztr==np.sort(ztr))):
                dataset = pd.DataFrame({'ztr': ztr, 'ytr': ytr}, columns=['ztr', 'ytr'])
                dataset=dataset.sort_values('ztr')
                ztr=np.array(dataset['ztr']).astype('float64')
                ytr=np.array(dataset['ytr']).astype('float64')
        #Check if Hull Profile and Transom Section are provided
        if np.all(~(np.isnan((HullProfile)))) and np.all(~(pd.isnull(ytr))):
            x=np.array([np.min(HullProfile[:,0])-1,np.max(HullProfile[:,0])+1])*np.ones((len(Ta),2))
            # z=(Tf-Ta)/Lbp*x+Ta
            z=(((Tf-Ta)/Lbp)*x)+Ta
            # z=np.transpose(z)
            #Define valid logs
            bb=~(pd.isnull(Ta)) & ~(pd.isnull(Tf)) & (Ta<np.max(HullProfile[:,1])) & (Tf<np.max(HullProfile[:,1]))
            bb=bb.ravel()
            isValid=[i for i ,X in enumerate(bb) if X==True]
            #Memory pre-allocation
            Ttr=np.empty(np.size(Ta))
            Ttr[:]=np.nan
            Atr=np.empty(np.size(Ta))
            Atr[:]=np.nan
            ytr_actual=np.empty((len(Ta),len(ytr)+2))
            ytr_actual[:]=np.nan
            ztr_actual=np.empty((len(Ta),len(ytr)+2))
            ztr_actual[:]=np.nan
            for i in isValid:
                temp,zwl=inn.intersection(x[i,:],z[i,:],HullProfile[:,0],HullProfile[:,1])
                Ttr=zwl[0]
                if Ttr>np.min(ztr) and Ttr<np.max(ztr):
                    pchip_1=pchip(ztr,ytr)                                    ##
                    ytrwl=pchip_1(Ttr)
                    ztrtemp=np.concatenate((ztr[ztr<=Ttr].ravel(),Ttr.ravel(),Ttr.ravel()),axis=None)
                    ztrtemp=np.transpose(ztrtemp)
                    
                    ytrtemp=np.concatenate((ytr[ztr<=Ttr].ravel(),ytrwl.ravel(),np.zeros(1)),axis=None)
                    
                    ytrtemp=np.transpose(ytrtemp)
                    ztr_actual[i,0:len(ztrtemp)]=ztrtemp
                    ytr_actual[i,0:len(ytrtemp)]=ytrtemp
                    Atr[i]=2*0.5*np.abs(np.dot(ytrtemp,np.roll(ztrtemp,1))-np.dot(ztrtemp,np.roll(ytrtemp,1)))
                elif Ttr<=np.min(ztr):
                    Atr[i]=0
                elif Ttr>=np.max(ztr):
                    ztr_actual[i,0:len(ztr)]=np.transpose(ztr)
                    ytr_actual[i,0:len(ytr)]=np.transpose(ytr)
                    Atr[i]=2*0.5*np.abs(np.dot(ytr,np.roll(ztr,1))-np.dot(ztr,np.roll(ytr,1)))
                else:
                    print('Atr error!')
        else:
            #Use estimation algorithm by Shiplab
            Atr=0.065526*Ta*B
            ztr_actual=np.nan
            ytr_actual=np.nan
    else:
        print('Atr error!')
        Atr=np.zeros(np.size(Ta))
        ztr_actual=np.nan
        ytr_actual=np.nan
    return(Atr,ytr_actual,ztr_actual)
    
                    
      
