# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 17:02:37 2020

@author: Sun Kumar

 	% Csternnom2int
 	% Description:  This function translates the values stored in variable
 	%               STERN from letters: P,V,N and U to integers: -25,-10,0,10
 	%               (see Holtrop 1984 definitions).
 	%
 	% Input:        STERN   [-]     stern type (string)
 	%
 	% Output:       stern     [-]   stern type value (integer)
 	%
 	% See also:     ResistanceHoltrop
"""

def Csternnom2int(STERN):
	
	

# 	% Csternnom2int
# 	% Description:  This function translates the values stored in variable
# 	%               STERN from letters: P,V,N and U to integers: -25,-10,0,10
# 	%               (see Holtrop 1984 definitions).
# 	%
# 	% Input:        STERN   [-]     stern type (string)
# 	%
# 	% Output:       stern     [-]   stern type value (integer)
# 	%
# 	% See also:     ResistanceHoltrop
	

	
# 	%Map stern type
	if STERN =='P':
	    stern=-25
	elif STERN =='V':
	    stern = -10
	elif STERN =='N':
	    stern=0
	elif STERN=='U':
	    stern=10
	else:
	    stern=float('NaN')
	    print('Error: Stern type is not defined!')
	return stern