# -*- coding: utf-8 -*-
import numpy as np
from BPME_Common import ResistanceHoltrop as RHT
from BPME_Common import CalibrationHoltrop as CHT
def IdealDeliveredPowerHoltrop(STW,Tm,Tf,Lwl,V,S,Cb,Cm,Cwp,lcb,rhosw,vsw,Ar,Ab,hb,Atr,RDS):
    if np.all(np.isnan(RDS['HullProfile'])):
        print("Holtrop data are not entered, thus PDHid cannot be estimated!")
        PDHid=np.empty(np.shape(STW))
        PDHid[:] = np.nan
        return PDHid
    Ar = Ar.reshape(1,np.size(Ar))
    RTHoltrop,temp,temp0,temp1,temp2,temp3,temp4,temp5,temp6,temp7,temp8,temp9=RHT.ResistanceHoltrop(STW,Tm,Tf,Lwl,V,S,Cb,Cm,Cwp,lcb,rhosw,vsw,Ar,Ab,hb,Atr,RDS)
    PDHoltrop=STW*0.514444*RTHoltrop/1000/0.7
    Pratio=CHT.CalibrationHoltrop(STW,Tm,
                          RDS['Tref'],
                          RDS['PratioHoltropref'],
                          RDS['Uref'],
                          RDS['Tb'],
                          RDS['Ts'])
    PDHid=PDHoltrop*Pratio
    
    return PDHid
   

