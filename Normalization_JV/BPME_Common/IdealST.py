# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 13:43:15 2020

@author: SunKumar
@Delta: SunKumar

% IdealST
% Description:  This function implements a generalized rule for ME and AE
%               shop test data to calculate the ideal yid (dependent
%               variable) for a given x (independent variable). Currently, 
%               the couples x-y are defined in MCModelingME.m (for MEs).
%
% Input:        XST     [any]   ST independent variable (vector)
%               YST     [any]   ST dependent variable (vector)
%               x       [any]   in service value for the independent variable (vector)
%               c       [any]   expected intersection of the curve with the y-axis
%
% Output:       yid     [any]   ideal y for in service x (vector)
%
% See also:     MCModelingME, IdealSFOC
"""


import numpy as np
import pandas as pd
from scipy.interpolate import pchip


def YYLowExtrapolation(XST,YST,x,c):
    try:
        #Normalized (percentage) distance definition
        dnorm=0.05
        
        #Normalize data
        Xnorm=XST/np.nanmax(XST)
        Ynorm=YST/np.nanmax(YST)
        #Xnorm = Xnorm[~np.isnan(Xnorm)]
        #Ynorm = Ynorm[~np.isnan(Ynorm)]
        #Define normalized interpolation data
        xnorm=np.arange(Xnorm[0],Xnorm[-1],0.001)
        #sort
        if not(isinstance(Xnorm, float)):
            if np.any(~(Xnorm==np.sort(Xnorm))):
                df = pd.DataFrame({'Xnorm':Xnorm,'Ynorm':Ynorm},columns=['Xnorm','Ynorm'])
                df = df.sort_values('Xnorm')
                Xnorm=np.array(df['Xnorm'])
                Ynorm=np.array(df['Ynorm'])
        yn=pchip(Xnorm,Ynorm)
        ynorm = yn(xnorm)
        
        #Find normalized offset
        for i in range(0,len(xnorm)):
            Dnorm=np.sqrt((xnorm[i]-xnorm[0])**2+(ynorm[i]-ynorm[0])**2)
            if Dnorm>dnorm:
                Onorm=xnorm[i]-Xnorm[0]
                break
            
        
        
        offset=Onorm*np.max(XST) #round(max(max(Um))*0.05*100)/100;
        
        #Lower end speed points definitions
        XST1=XST[0]
        XST2=XST[0]+offset
        
        #Lower end power points definitions
        YST1=YST[0]
        
        #sort
        if not(isinstance(XST, float)):
            if np.any(~(XST==np.sort(XST))):
                df = pd.DataFrame({'XST':XST,'YST':YST},columns=['XST','YST'])
                df = df.sort_values('XST')
                XST = np.array(df['XST'])
                YST = np.array(df['YST'])
        YS=pchip(XST,YST)
        YST2 = YS(XST2)
        
        #Lower end coefficients definitions
        b=np.log((YST2-c)/(YST1-c))/np.log(XST2/XST1)
        a=(YST1-c)/XST1**b
        
        #Lower end extrapolation function
        YL=a*x**b+c
        #Safety correction to prevent from anomalies
        if np.abs(YL-c)>0.1 and b<0.1:
            YL=[]
            b=0.5
            a=(YST1-c)/XST1**b
            YL=a*x**b+c
    except Exception as e:
        raise
    else:
        return YL
    
def YYHighExtrapolation(XST,YST,x):



#Normalized (percentage) distance definition
    dnorm=0.05

#Normalize data
    Xnorm=XST/np.nanmax(XST)
    Ynorm=YST/np.nanmax(YST)

#Define normalized interpolation data
    xnorm=np.arange(Xnorm[0],Xnorm[-1],0.001)
    yn=pchip(Xnorm,Ynorm)
    ynorm = yn(xnorm)

#Find normalized offset
    for i in reversed(range(0,len(xnorm))):
        Dnorm=np.sqrt((xnorm[-1]-xnorm[i])**2+(ynorm[-1]-ynorm[i])**2)
        if Dnorm>dnorm:
            Onorm=Xnorm[-1]-xnorm[i]
            break
    
    
    offset=Onorm*np.max(XST)
    
    #High end x points definitions
    XST1=XST[-1]-offset
    XST2=XST[-1]
    
    #High end y points definitions
    YST1A=pchip(XST,YST)
    YST1 = YST1A(XST1)
    YST2=YST[-1]
    
    #Higher end coefficients definitions
    a=(YST2-YST1)/(XST2-XST1)
    c=YST2-a*XST2
    
    #Higher end extrapolation function
    YH=a*x+c

# % %Optimization Approach
# % %High end x range definition
# % Xstep=0.01;
# % XR=XST(end)-offset:Xstep:XST(end);
# % 
# % %High end y range definition
# % YR=pchip(XST,YST,XR);
# % 
# % %Initial coefficients guess
# % cc0=[1 1 1];
# % 
# % %Optimization algorithm anonymous function definition
# % F=@(cc,xx) cc(1)*xx.^cc(2)+cc(3);
# % 
# % %Optimization algorithm (least squares method) execution
# % cc=mylsqcurvefit(F,cc0,XR,YR);
# % 
# % %High end extrapolation function
# % YH=F(cc,x);

    return YH





def IdealSTScalar(XST,YST,x,c):
    
    if XST.ndim>1:
        XST=XST.ravel()
    if YST.ndim>1:
        YST=YST.ravel()
        



    if x<XST[0]:
        yyid=YYLowExtrapolation(XST,YST,x,c)
            
    elif x>=XST[0] and x<=XST[-1]:
        if not(isinstance(XST, float)):
            if np.any(~(XST==np.sort(XST))):
                df = pd.DataFrame({'XST':XST,'YST':YST},columns=['XST','YST'])
                df = df.sort_values('XST')
                XST = np.array(df['XST'])
                YST = np.array(df['YST'])
        yy=pchip(XST,YST)
        yyid = yy(x)
                
    elif x>XST[-1] and x<=XST[-1]*1.1:
        yyid=YYHighExtrapolation(XST,YST,x)
        
        
    elif x>XST[-1]*1.1:
        yyid=float('NaN')
        print('Ideal ST curve error: x-axis value too high!')
                    
    else:
        yyid=float('NaN')
        print('Ideal ST curve error!')
        
    return yyid

def IdealST(XST,YST,x,c):

    if x.ndim ==1:
        x = x.reshape(1,-1).T

    if (np.isnan(XST).all()) or (np.isnan(YST).all()):
        yid=np.full(x.shape,np.nan)
        return yid.ravel()
        
    elif np.all(YST==0):
        yid=np.zeros((x.shape))
        return yid.ravel()
    
    elif x.size == 0:
        yid =np.array([])
        return yid
    
    
    #imax: # of logs, jmax # of cylinders
    #print(np.shape(x))
    [imax,jmax]=x.shape
#    yid=[]
    yid= np.full(x.shape,np.nan) #np.full(x.shape,np.nan) 
    XST = XST[~np.isnan(XST)] # AC
    YST = YST[~np.isnan(YST)] # AC
    if len(XST) != len(YST):
        yid=np.full(x.shape,np.nan)
        return yid.ravel()
                    
    for i in range(0,imax):
        for j in range(0,jmax):
            yid[i,j]=IdealSTScalar(XST,YST,x[i,j],c)
            
    if jmax<=1:
        yid = yid.ravel()
    
    return yid


    
