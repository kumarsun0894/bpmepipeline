# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 23:16:19 2020

@author: Subhin Antony
@Delta:SunKumar

% SFOCiso
% Description:  This function performes the SFOC ISO correction.
%
% Input:        SFOC        [g/kWh] actual SFOC (vector)
%               LCVFO       [MJ/kg] actual LCV (vector)
%               LCVMDOref   [MJ/kg] reference LCV (scalar)
%               TltcwACin   [°C]    cooling water temperature at AC inlet (vector)
%               TairTCin    [°C]    air temperature at TC inlet (vector)
%               pambair     [mbar]  ambient air pressure (vector)
%
% Output:       SFOCisocor  [g/kWh] ISO corrected SFOC (vector)
%
% See also:     BPMEngineSingleVessel, ManipulateServiceData,
%               HPOutputFiltering, RefDashboards

"""

def SFOCiso(SFOC,LCVFO,LCVMDOref,TltcwACin,TairTCin,pambair):
    SFOCisocor=SFOC*(LCVFO/LCVMDOref-0.0006*(TltcwACin-25)-0.0002*(TairTCin-25)+0.00002*(pambair-1000))
    return(SFOCisocor) 
