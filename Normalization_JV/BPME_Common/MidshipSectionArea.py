# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 11:15:11 2020

@author: SubhinAntony

% MidshipSectionArea
% Description:  This function calculates the midship section area based on
%               the midship section coefficient from hydrostatic tables. If
%               hydrostatic tables are unavailable, then an approximation
%               is used (Cm=0.97) based on Tupper, 2004, p.37.
%
% Input:        Tm      [m]     actual mean draught (vector)
%               B       [m]     ship breadth (scalar)
%               Th      [m]     mean draught from hydrostatic tables (vector)
%               Cmh     [-]     midship section coefficient from hydrostatic tables (vector)
%
% Output:       Amid    [m2]    actual midship section area (vector)
%               Cm      [-]     actual midship section coefficient (vector)
%
% See also:     SHALLOW, STATOTAL, ManipulateServiceData

"""
import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline
def MidshipSectionArea(Tm,B,Th,Cmh):
    if ~(np.any(np.isnan(Cmh))):
        spln=InterpolatedUnivariateSpline(Th,Cmh)
        Cm=spln(Tm.astype('float64'))
    elif np.any(np.isnan(Cmh)):
        Cm=0.97*np.ones(np.size(Tm)) #see Tupper, 2004, p.37
    else:
        print('Cm error!')
        Cm=np.nan
    Amid=Cm*Tm*B
    return(Amid,Cm)
        
        
        