# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 11:55:09 2020

@author: akhil.surendran
% SeawaterViscosity
% Description:  Analytical expression of viscosity as function of
%               temperature and salinity as a parametric 5th order
%               polynomial which arose based on curve fitting. Data from
%               ITTC 7.5-02-01-03, Appendices A and B.
%
% Input:        tw      [°C]    water temperature (vector)
%               Sa      [g/kg]  water salinity (vector)
%
% Output:       vw      [m2/s]  sea water viscosity (vector)
%
% Note 1:       According to ITTC 7.5-02-01-03: In general, the water
%               properties are a function of temperature, pressure and
%               absolute salinity. In this procedure, data are provided at
%               standard pressure of 0.101325MPa.
%
% Note 2:       The idea is that we define two reference curves:
%               - vfw for Sa=0g/kg
%               - vsw for Sa=35.16504g/kg
%               and we generate a linear relationship:
%               vw(tw,Sa)=lamda(tw)*Sa+beta(tw)
%               where:
%               lamda(tw)=(vsw-vfw)/35.16504
%               beta(tw)=vfw
%
% See also:     STATOTAL, ManipulateServiceData
"""
def SeawaterViscosity(tw,Sa):
	   
	vfw=-2.042E-15*tw**5+3.823E-13*tw**4-3.164E-11*tw**3+1.626E-09*tw**2-6.199E-08*tw+1.792E-06

	vsw=-2.041E-15*tw**5+3.823E-13*tw**4-3.169E-11*tw**3+1.634E-09*tw**2-6.289E-08*tw+1.854E-06

	vw=Sa*(vsw-vfw)/35.16504+vfw
        
	return(vw)

        