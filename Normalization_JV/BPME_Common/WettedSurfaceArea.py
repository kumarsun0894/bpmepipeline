# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 11:16:00 2020

@author: SubhinAntony
@Delta:SunKumar

% WettedSurfaceArea
% Description:  This function calculates the wetted surface area based on
%               data from hydrostatic tables. If hydrostatic tables are
%               unavailable, then an approximation is used based on
%               Holtrop's method.
%
% Input:        Tm      [m]     actual mean draught (vector)
%               Lwl     [m]     waterline length (vector)
%               V       [m3]    actual volumetric displacement (vector)
%               Cm      [-]     midship section coefficient (vector)
%               Cwp     [-]     waterplane area coefficient (vector)
%               Ab      [m2]    bulbous bow transverse sectional area (vector)
%               Th      [m]     mean draught from hydrostatic tables (vector)
%               WSAh    [m2]    wetted surface area for Th (vector)
%               B       [m]     ship breadth (scalar)
%
% Output:       WSA     [m2]    wetted surface area (vector)
%
% Note:         To achieve better results, Denny's method must be enhanced
%               with two coeficients (c1 and c2), where they are calculated
%               based on "fminsearch" optimization function (see
%               WettedSurfaceEstimator.m function). Other wetted surface
%               estimation methods are defined in Holtrop's papaers and can
%               be evaluated and used here.
%
% See also:     MainpulateServiceData, STATOTAL, TEMP
"""
import numpy as np
from BPME_Common import IdealST as idst

from scipy.interpolate import InterpolatedUnivariateSpline
def WettedSurfaceArea(Tm,Lwl,V,Cm,Cwp,Ab,Th,WSAh,B):
    if Th.ndim==1:
        Th=Th.reshape(1,-1)
        Th=np.transpose(Th)
    if Lwl.ndim==1:
        Lwl=Lwl.reshape(1,-1)
        Lwl=np.transpose(Lwl)
    if Tm.ndim==1:
        Tm=Tm.reshape(1,-1)
        Tm=np.transpose(Tm)
    if V.ndim==1:
        V=V.reshape(1,-1)
        V=np.transpose(V)
    if Cm.ndim==1:
        Cm=Cm.reshape(1,-1)
        Cm=np.transpose(Cm)
    if Cwp.ndim==1:
        Cwp=Cwp.reshape(1,-1)
        Cwp=np.transpose(Cwp)
    if Ab.ndim==1:
        Ab=Ab.reshape(1,-1)
        Ab=np.transpose(Ab)
    if WSAh.ndim==1:
        WSAh=WSAh.reshape(1,-1)
        WSAh=np.transpose(WSAh)
    if ~(np.any(np.isnan(WSAh))):
        WSA=idst.IdealST(Th.ravel(),WSAh.ravel(),Tm.astype('float64'),0)   
        
    elif np.any(np.isnan(WSAh)):
        #Block coefficient at actual loading conditions
        Cb=V/(Lwl*B*Tm)
        #Holtrop's formula
        WSA=Lwl*(2*Tm + B)*np.sqrt(Cm)*(0.453+0.4425*Cb-0.2862*Cm-0.003467*B/Tm+0.3696*Cwp)+2.38*Ab/Cb
    else:
        print('WSA error!')
        print('')
        WSA=np.nan
    return WSA
                
        
    