# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 13:44:41 2020

@author: Subhin Antony
"""
from BPME_Common import intersections as inn
import numpy as np
import pandas as pd

def WaterLineLength(Ta,Tf,Lbp,HullProfile):
    if np.all(~(np.isnan((HullProfile)))):
        x=[np.min(HullProfile[:,0])-1,np.max(HullProfile[:,0])+1]*np.ones((len(Ta),2))
        z=(((Tf-Ta)/Lbp)*x)+Ta
        # z=np.transpose(z)
        # x=np.transpose(x)
		
        # print(z)
        #Define valid logs
        b=[Ta[i]+Tf[i] for i in range(0,len(Ta))]
        
        isValid=[i for i ,X in enumerate(b) if ~(pd.isnull(X))]
        
        
        
        
        #Memory pre-allocation
        xwl=np.empty((len(Ta),2))
        xwl[:]=np.nan
        
        zwl=np.empty((len(Ta),2))
        zwl[:]=np.nan
        
        Lwl=np.empty(np.size(Ta))
        Lwl[:]=np.nan
        for i in isValid:
            xx,zz=inn.intersection(x[i,:],z[i,:],HullProfile[:,0],HullProfile[:,1])
            if (~(np.any(np.isnan(xx))) and xx.size!=0) and (~(np.any((np.isnan(zz)))) and zz.size!=0):
                xwl[i,:]=np.array([np.min(xx),np.max(xx)])
                zwl[i,:]=np.array([np.min(zz),np.max(zz)])
                A=np.array([xwl[i,0],zwl[i,0]])
                B=np.array([xwl[i,1],zwl[i,1]])
                Lwl[i]=np.linalg.norm(B-A)
            else:
                Lwl[i]=np.nan
        
        xLwl0_5=np.mean(xwl,axis = 1)
        zLwl0_5=np.mean(zwl,axis = 1)
    else:
        Lwl=Lbp*np.ones(np.size(Ta))
        xLwl0_5=Lwl/2
        zLwl0_5=(Ta+Tf)/2
            
    return(Lwl,xLwl0_5,zLwl0_5)       


#if __name__ == '__main__':
#    Ta=np.array([9.30,9.30,9.30,9.30,9.30,9.30,9.30,9.30])
#    Tf=np.array([5.6,5.6,5.6,5.6,5.6,5.6,5.6,5.6])
#    Lbp=244   
#    HullProfile=np.array([[-4.11,-4.11,2.53,7.51,9.42,10.34,10.48,9.86,8.88,7.36,7.36,8.95,11.09,12.70,15.96,17.36,238.00,240.53,243.81,247.02,248.65,250.39,250.96,251.09,251.04,250.72,250.15,249.34,248.24,245.83,244.75,244.00,244.00,251.14],[22.13,12.71,11.81,11.15,10.37,9.19,7.71,6.56,5.75,5.42,3.74,2.73,1.69,1.07,0.23,0.00,0.00,0.33,1.05,2.46,3.67,5.86,7.30,8.11,8.84,9.65,10.20,10.71,10.94,10.98,11.18,11.76,14.51,26.34]])
#    HullProfile = np.transpose(HullProfile)
#    Lwl,xLwl0_5,zLwl0_5=WaterLineLength(Ta,Tf,Lbp,HullProfile)
#    print(Lwl,xLwl0_5,zLwl0_5)  