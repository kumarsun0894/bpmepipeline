# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 18:00:49 2020

@author: SunKumar
"""
import numpy as np
from BPME_Common import IdealSpeed as IS

def IdealRPMHoltrop(Pcor,NMEST,PeffMEST,etaT,etaeff,factorN2,ReferenceDataStruct):

	#If Holtrop data are not entered, skip this function
	if np.isnan(ReferenceDataStruct['HullProfile']).all():
		print("Holtrop data are not entered, thus NHid cannot be estimated!")
		NHid=np.full(Pcor.shape,np.nan)
		return(NHid)
	
	
	# The Holtrop method must be applied here.
	# An idea is to use the B-series Kt, Kq curves and assuming that it is
	# always designed at the maximum etao. Then the RPM can be calculated.
	# I think thats this optimization problem is being used in Politis' Fortran
	# code "GRID".
	
	PdMEST=PeffMEST*etaT*etaeff #shop test power corresponding to delivered power in [kW]
	
	NHid=IS.IdealSpeed(Pcor.astype(float),np.ones(Pcor.shape),np.full(Pcor.shape,np.nan),np.array([1],dtype = float),PdMEST,NMEST,1,1)*factorN2
	return NHid


