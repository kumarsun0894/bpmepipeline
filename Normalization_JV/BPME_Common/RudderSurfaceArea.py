# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 10:50:59 2020

@author: akhil.surendran
"""
import numpy as np
from BPME_Common import RudderHydrofoil as rh
from BPME_Common import intersections as ins
def RudderSurfaceArea(Ta,Lbp,RudderProfile):
    if np.all(~np.isnan(RudderProfile)):        
          xr=RudderProfile[:,0]
          zr=RudderProfile[:,1]
          #print("zr=",zr)
          [thr,ch,Af]=rh.RudderHydrofoil(RudderProfile)
          Ar= np.empty(np.shape(Ta))
          Ar[:] = np.nan
          xr_actual=np.empty((len(Ta),len(xr)+2))
          xr_actual[:] = np.nan
         
          zr_actual=np.empty((len(Ta),len(xr)+2))
          zr_actual[:] = np.nan
          
          for i in range(0,len(Ta)):
              if Ta[i]>min(zr) and Ta[i]<max(zr):
                    x1= np.array([min(xr)-1,max(xr)+1])
                    
                    y1 = np.array([Ta[i],Ta[i]])
                    x2= xr
                    y2= zr  
                    
                    
                    [xrwl,_]=ins.intersection(x1,y1,x2,y2)
                    if len(xrwl) == 1:
                        y1 = y1-0.001
                    [xrwl,_]=ins.intersection(x1,y1,x2,y2)
                    
                        
                    zrtemp = np.array([])
                    zrtemp = np.append(zrtemp,Ta[i])
                    zrtemp = np.append(zrtemp,zr[zr<=Ta[i]])
                    zrtemp= np.append(zrtemp,Ta[i])
                    zrtemp = zrtemp.T
                    
                    xrtemp=np.array([])
                    xrtemp = np.append(xrtemp,xrwl[0])
                    xrtemp = np.append(xrtemp,xr[zr<=Ta[i]])
                    xrtemp= np.append(xrtemp,xrwl[1])
                    xrtemp = xrtemp.T
                    
                    zr_actual[i,0:len(zrtemp)]=zrtemp
                    xr_actual[i,0:len(xrtemp)]=xrtemp
                    #print('xr_actual= ', xr_actual)
                    A = 2*(0.5*np.abs(np.dot(xrtemp,np.roll(zrtemp,1))-np.dot(zrtemp,np.roll(xrtemp,1))))
                    Ar[i]=A+Af
                    #print(Ar)
              elif Ta[i]<=min(zr):
                    Ar[i]=0
              elif Ta[i]>=max(zr):
                    zr_actual[i,0:len(zr)]=zr.T
                    xr_actual[i,0:len(xr)]=xr.T
                    A = 2*(0.5*np.abs(np.dot(xr,np.roll(zr,1))-np.dot(zr,np.roll(xr,1))))
                    Ar[i]=A+Af
              i= i+1
#              
    else:
        Ar=2*Ta*Lbp*0.023;
        thr=np.nan
        zr_actual = np.nan
        xr_actual=np.nan
    return (Ar,thr,xr_actual,zr_actual)
#    
