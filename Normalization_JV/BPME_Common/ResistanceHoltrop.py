# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 12:00:05 2020

@author: SunKumar


"""

import pandas as pd 
import  numpy as np
from BPME_Common import Csternnom2int as CS   
from BPME_Common import BowHalfAngle as BHA

def ResistanceHoltrop(STW,Tm,Tf,Lwl,V,S,Cb,Cm,Cwp,lcb,rhosw,vsw,Ar,Ab,hb,Atr,ReferenceDataStruct):
   
###########################################################################
# Data preparation                                                        #
###########################################################################
    B=ReferenceDataStruct['B']
    g=ReferenceDataStruct['g']
    U=(STW*0.514444)
    U[U==0] = np.nan
    
    Cstern=CS.Csternnom2int(ReferenceDataStruct['Cstern'])
    # Cstern=0
###########################################################################
# Bow thruster tunnel diameter definition                                 #
###########################################################################
#Check if side thruster exists
    if ReferenceDataStruct['SThruster']  =='Yes':
        
        #Check if number of side thrusters is filled
        #if (np.isnan(ReferenceDataStruct['nSThruster']) == False) and (bool(ReferenceDataStruct['nSThruster']) == True):
        if not(pd.isnull(ReferenceDataStruct['nSThruster'])):
            #find the indices which correpond to bow thrusters
            bowindex=[inx for inx, e in enumerate(ReferenceDataStruct['LocationSThruster'].tolist()) if e == 'Bow']
            #bowindex= ReferenceDataStruct['LocationSThruster'].index('Bow')
            if len(bowindex)>0:
                #define the tunnel diameter for all bow thrusters
                #dbtemp=np.full(bowindex,np.nan)
                dbtemp=np.empty(np.size(bowindex))
                dbtemp[:]=np.nan
                for i in range(0,len(bowindex)):
                    dbtemp[i]=ReferenceDataStruct['dtunSThruster'][bowindex[i]]
                    if np.isnan(dbtemp[i]) or (dbtemp[i].size == 0):
                        dbtemp[i]=0
                db=np.sum(dbtemp)
            else:
                db=0
        else:
            db=0
    else:
        db=0
    
###########################################################################
# Sappb options:
# --------------
# 1. Define the 10 elements as 10 different variables in Reference Data
#    Entry.
    
    # 2. Define a 10-element vector where the sequence is:
#    a. Rudder behind stern
#    b. Twin-screw balance rudders
#    c. Shaft brackets
#    d. Skeg
#    e. Strut bossings
#    f. Hull bossings
#    g. Shafts
#    h. Stabilizer fins
#    i. Dome
#    j. Bilge keels
    
    # 3. It is not defined in Reference Data Entry and an algorith estimates it
#    (either always 0 or statistical values based on the vessel type, size,
#    etc.)
    
    # Note1: Assume 0 for now.
#        
# Note2: All apendages assumed to have constant area except rudder.
###########################################################################
    Sappb=np.zeros((Tf.size,10))
    
 ###########################################################################
# An algorithm must be developed to calculate the Cbto coefficient based on
# crietria given in 1982 paper.
    
    # A sensitivity analysis must happen to evaluate if an average value can be
# used universaly.
    
    # Range:            0.003 to 0.012
# 1982 paper notes: For openings in the cylindrical part of a bulbous bow
#                   the lower figures should be used.
###########################################################################
    Cbto=0.008
    
###########################################################################
# 1+k2 Table
# See Holtrop 1982
    
    # A sensitivity analysis must happen to evaluate if an average value can be
# used universaly.
    
    # Column 1: 1+k2 average values for various appendages
#           It can be a vector in Reference Constants Definition in PAL\PM.
    
    # Column 3: appendage area in m2
#           New variables must be introduced (see point above) in reference
#           data entry and filled by user.
###########################################################################
    OnePlusk2=np.array([1.75,1.4,2.8,3,1.75,3,2,3,2.8,2.7,1.4])
###########################################################################
# Caculated variables                                                     #
###########################################################################
    
    #Prismatic coefficient [-]
    Cp=Cb / Cm
    #Length of the run [m]
    Lr=np.multiply(Lwl,(1 - Cp + np.multiply((0.06*Cp),lcb) / ((4*Cp) - 1)))
    #add a diagnostic for negative numbers generation due to invalid input (e.g. wrong lcb estimation)
    Lr[Lr < 1]=1
    #Half angle of entrance [deg]
    ie=BHA.BowHalfAngle(Cb)
    #Reynolds number [-]
    Rn=np.multiply(U,Lwl) / vsw
    #Appendage surface area calculation [m2]

    Ar=np.transpose(Ar)

    c=np.concatenate((Ar,Sappb),axis=1)
    Sapp =np.sum(c,axis=1)
    #Equivalent appendages form factor
    OnePlusk2eq=np.sum((OnePlusk2*c),1) / Sapp
    #OnePlusk2eq=np.sum(OnePlusk2*c,2) / Sapp
    #Froude number [-]
    Fn=U / np.sqrt(np.dot(g,Lwl))
    #Bow emergence measure
#    pb=(0.56*np.sqrt(Ab)) / (Tf - (1.5*hb))
    
    pb=np.divide((0.56*np.sqrt(Ab)).astype('float64'), (Tf - (1.5*hb)).astype('float64'), out=np.full_like((0.56*np.sqrt(Ab)).astype('float64'),float('nan')), where=(Tf - (1.5*hb)).astype('float64')!=0)

    #Bow immersion Froude number [-]
    #Fni=U / np.sqrt(np.dot(g,(Tf - hb - (0.25*np.sqrt(Ab)))) + (0.15*(U ** 2)))
    Fni=U/np.sqrt((g*(Tf-hb-0.25*np.sqrt(Ab.astype('float64')))+0.15*U**2).astype('float64'))
    #Transom immersion Froude number [-]
#    Fnt=U / np.sqrt(np.dot(2*g,Atr) / (B + np.dot(B,Cwp)))
    gAtr = 2*g*Atr 
    if len(gAtr[gAtr==0]) >0:
          gAtr[gAtr==0]=1
    Fnt=U/np.sqrt((gAtr/(B+B*Cwp)).astype('float64'))

###########################################################################
# Coefficients                                                            #
###########################################################################
    
    #Frictional resistance coefficient calculation based on the 1957 ITTC line
    Cf=0.075 / (np.log10(Rn.astype('float64')) - 2) ** 2
    #Stern shape coefficient calculation
    c14=1 + (0.011*Cstern)
    #c7 calculation
    c7=np.full(Tf.shape,np.nan)
    for i in range(0,len(Tf)):
        if B / Lwl[i] <= 0.11:
            c7[i]=np.dot(0.229577,(B / Lwl[i]) ** 0.33333)
        elif B / Lwl[i] > 0.11 and B / Lwl[i] <= 0.25:
            c7[i]=B / Lwl[i]
        elif B / Lwl[i] > 0.25:
            c7[i]=0.5 - (0.0625*Lwl[i]) / B
        else:
            c7[i]=float("NaN")
            print('c7 error!')
    
    #c1 calculation 2223105*c7.^3.78613.*(Tm/B).^1.07961.*(90-ie).^-1.37565
    #np.multiply(np.multiply(np.dot(2223105,c7 ** 3.78613),(Tm / B) ** 1.07961),(90 - ie) ** - 1.37565)
    c1=2223105*c7**3.78613*(Tm/B)**1.07961*(90-ie)**-1.37565
    #c3 calculation
    #np.dot(0.56,Ab ** 1.5) / (np.multiply(np.dot(B,Tm),(np.dot(0.31,np.sqrt(Ab)) + Tf - hb)))
#    c3=0.56*Ab**1.5/(B*Tm*(0.31*np.sqrt(Ab)+Tf-hb))
    c3= np.divide((0.56*Ab**1.5).astype('float64'), (B*Tm*(0.31*np.sqrt(Ab)+Tf-hb)).astype('float64'), out=np.full_like((0.56*Ab**1.5).astype('float64'),float('nan')), where=(B*Tm*(0.31*np.sqrt(Ab)+Tf-hb)).astype('float64')!=0)
    #c2 calculation 
    #exp(-1.89*sqrt(c3))
    c2=np.exp(- 1.89*np.sqrt(c3.astype('float64')))
    #c5 calculation
    #1 - np.dot(0.8,Atr) / (np.multiply(np.dot(B,Tm),Cm))
#    c5= 1-0.8*Atr/(B*Tm*Cm)
    c5= 1-np.divide((0.8*Atr).astype('float64'), (B*Tm*Cm).astype('float64'), out=np.full_like((0.8*Atr).astype('float64'),float('nan')), where=(B*Tm*Cm).astype('float64')!=0)

    #c15 calculation
    c15=np.full(Tf.shape,np.nan)
    # c15=np.empty(Tf.shape)
    # c15[:]=np.nan
    for i in range(0,len(Tf)):
        if Lwl[i] ** 3 / V[i] <= 512:
            
            c15[i]=- 1.69385
        elif Lwl[i] ** 3 / V[i] > 512 and Lwl[i] ** 3 / V[i]<= 1726.91:
            c15[i]=- 1.69385 + (Lwl[i] / V[i] ** (1 / 3) - 8) / 2.36
        elif Lwl[i] ** 3 / V[i] > 1726.91:
            c15[i]=0
        else:
            c15[i]=float("NaN")
            print('c15 error!')
    
    #c16 calculation
    c16=np.full(Tf.shape,np.nan)
    for i in range(0,len(Tf)):
        if Cp[i]<= 0.8:
            #c16[i]=np.dot(8.07981,Cp[i]) - np.dot(13.8673,Cp[i] ** 2) + np.dot(6.984388,Cp[i] ** 3)
            c16[i] = 8.07981*Cp[i]-13.8673*Cp[i]**2+6.984388*Cp[i]**3
        elif Cp[i] > 0.8:
            c16[i]=1.73014 - np.dot(0.7067,Cp[i])
        else:
            c16[i]=float("NaN")
            print('c16 error!')
    
    #c17 calculation
    #c17=6919.3*Cm**(-1.3346)*(V/Lwl**3)**2.00977*(Lwl/B-2)**1.40692
    c17=6919.3*Cm**(-1.3346)*(np.divide(V, Lwl**3, out=np.full_like(V,float('nan')), where=Lwl**3!=0))**2.00977*(Lwl/B-2)**1.40692
    #d calculation
    d=- 0.9
    #lamda calculation
    lamda=np.full(Tf.shape,np.nan)
    for i in range(0,len(Tf)):
        if Lwl[i] / B <= 12:
            lamda[i]=np.dot(1.446,Cp[i]) - np.dot(0.03,Lwl[i]) / B
        elif Lwl[i] / B > 12:
            lamda[i]=np.dot(1.446,Cp[i]) - 0.36
        else:
            lamda[i]=float("NaN")
            print('lamda error!')
    
    #m1 calculation
#    m1=0.0140407*Lwl/Tm-1.75254*V**(1/3)/Lwl-4.79323*B/Lwl-c16
    #m1=0.0140407*Lwl.astype('float64')/Tm.astype('float64')-1.75254*V**(1/3)/Lwl.astype('float64')-4.79323*B/Lwl.astype('float64')-c16
    m1=0.0140407*Lwl.astype('float64')/Tm.astype('float64')-1.75254*np.divide(V**(1/3), Lwl.astype('float64'), out=np.full_like(V**(1/3),float('nan')), where=Lwl.astype('float64')!=0)-4.79323*np.divide(B, Lwl.astype('float64'), out=np.full_like(Lwl.astype('float64'),float('nan')), where=Lwl.astype('float64')!=0)-c16

    #m3 calculation
    m3=-7.2035*(B/Lwl)**0.326869*(Tm/B)**0.605375
    #m4 calculation
    m4=c15*0.4*np.exp(-0.034*Fn.astype('float64')**-3.29)
    #c6 calculation
    c6=np.full(Tf.shape,np.nan)
    for i in range(0,len(Tf)):
        if Fnt[i] < 5:
            c6[i]=0.2*(1-0.2*Fnt[i])
        elif Fnt[i] >= 5:
            c6[i]=0
        else:
            c6[i]=float("NaN")
            print('c6 error!')
    
    #c4 calculation
    c4=np.full(Tf.shape,np.nan)
    for i in range(0,len(Tf)):
        if Tf[i] / Lwl[i] <= 0.04:
            c4[i]=Tf[i] / Lwl[i]
        elif Tf[i] / Lwl[i] > 0.04:
            c4[i]=0.04
        else:
            c4[i]=float("NaN")
            print('c4 error!')
    
    #Ca calculation
    #Ca=np.dot(0.006,(Lwl + 100) ** (- 0.16)) - 0.00205 + np.multiply(np.dot(np.multiply(np.dot(0.003,np.sqrt(Lwl / 7.5)),Cb ** 4.0),c2),(0.04 - c4))
    Ca=0.006*(Lwl+100)**(-0.16)-0.00205+0.003*np.sqrt(Lwl/7.5)*Cb**4*c2*(0.04-c4)
# C:\BPM_Ver2\BPM_Engine_Gen1\Source\Library\ResistanceHoltrop.m:263
    ###########################################################################
# Note: In addition, Ca might be increased to calculate e.g. the effect of
#       a larger hull roughness than standard. To this end the ITTC-1978
#       formulation can be used from which the increase of Ca can be
#       derived for roughness values higher than the standard figure of 
#       ks=150um (mean apparent amplitude):
#       increase Ca=(0.105*ks^(1/3)-0.005579)/Lwl^(1/3)
#       In these formulae Lwl and ks are given in metres.
###########################################################################
    
###########################################################################
# Resistance                                                              #
###########################################################################
    
    #Frictional resistance calculation
    #Rf=np.multiply(np.dot(np.multiply(np.dot(0.5,rhosw),U ** 2.0),S),Cf)
    Rf = .5*rhosw*U**2*S*Cf
    #Form factor calculation
    #OnePlusk1=0.93 + np.multiply(np.multiply(np.multiply(np.multiply(np.multiply(np.dot(0.487118,c14),(B / Lwl) ** 1.06806),(Tm / Lwl) ** 0.46106),(Lwl / Lr) ** 0.121563),(Lwl ** 3.0 / V) ** 0.36486),(1 - Cp) ** - 0.604247)
    #OnePlusk1=0.93+0.487118*c14* (B/Lwl)**1.06806* (Tm/Lwl)**0.46106* (Lwl/Lr)**0.121563* (Lwl**3/V)**0.36486*(1-Cp)**-0.604247
    OnePlusk1=0.93+0.487118*c14* np.divide(B, Lwl.astype('float64'), out=np.full_like(Lwl.astype('float64'),float('nan')), where=Lwl.astype('float64')!=0)**1.06806* np.divide(Tm, Lwl.astype('float64'), out=np.full_like(Tm,float('nan')), where=Lwl.astype('float64')!=0)**0.46106* (Lwl/Lr)**0.121563* (Lwl**3/V)**0.36486*(1-Cp)**-0.604247
    #add a diagnostic for complex numbers generation due to invalid input (e.g. huge displacement)
    OnePlusk1[[i for i,val in enumerate(OnePlusk1) if type(val) is complex]]= float("NaN")
#    OnePlusk1[np.iscomplex(OnePlusk1) == True] = float("NaN")
    
    #Appendage resistance calculation
    #Rapp=np.multiply(np.multiply(np.dot(np.multiply(np.dot(0.5,rhosw),U ** 2.0),Sapp),OnePlusk2eq),Cf) + np.dot(np.dot(np.dot(np.multiply(rhosw,U ** 2.0),np.pi),db ** 2),Cbto)
    Rapp =0.5*rhosw*U**2*Sapp*OnePlusk2eq*Cf+rhosw*U**2*np.pi*db**2*Cbto
    #Wave resistance calculation
    Rw=np.full(Tf.shape,np.nan)
    for i in range(0,len(Tf)):
        if Fn[i] <= 0.4:
            #Rw[i]=np.dot(np.dot(np.dot(np.dot(np.dot(np.dot(c1[i],c2[i]),c5[i]),V[i]),rhosw[i]),g),np.exp(np.dot(m1[i],Fn[i] ** d) + np.dot(m4[i],np.cos(np.dot(lamda[i],Fn[i] ** (- 2))))))
            Rw[i]=c1[i]*c2[i]*c5[i]*V[i]*rhosw[i]*g*np.exp(m1[i]*Fn[i]**d+m4[i]*np.cos(lamda[i]*Fn[i]**(-2)))
        elif Fn[i] > 0.4 and Fn[i] < 0.55:
            Rwa040=c1[i] *c2[i]*c5[i]*V[i]*rhosw[i]*g*np.exp(m1[i]*0.40**d+c15[i]*0.4*np.exp(-0.034*0.40**-3.29)*np.cos(lamda[i]*0.40**(-2)))
            Rwb055=c17[i]*c2[i]*c5[i]*V[i]*rhosw[i]*g*np.exp(m3[i]*0.55**d+c15[i]*0.4*np.exp(-0.034*0.55**-3.29)*np.cos(lamda[i]*0.55**(-2)))
            Rw[i]=Rwa040+(10*Fn[i]-4)*(Rwb055-Rwa040)/1.5
        elif Fn[i] >= 0.55:
            Rw[i]=c17[i]*c2[i]*c5[i]*V[i]*rhosw[i]*g*np.exp(m3[i]*Fn[i]**d+m4[i]*np.cos(lamda[i]*Fn[i]**(-2)))
        else:
            Rw[i]=float("NaN")
            print('Rw error!')
    
    #Bulbous bow resistance calculation
    #Rb=np.dot(np.multiply(np.dot(np.multiply(np.dot(0.11,np.exp(np.dot(-3,pb**(-2)))),Fni**3.0),Ab**1.5),rhosw),g)/(1+Fni**2)
    Rb= 0.11*np.exp(-3*pb.astype('float64')**(-2))*Fni**3*Ab**1.5*rhosw*g/(1+Fni**2)
    

    #Transom resistance calculation
    #Rtr=np.multiply(np.dot(np.multiply(np.dot(0.5,rhosw),U ** 2.0),Atr),c6)
    Rtr =0.5*rhosw*U**2*Atr*c6
    #Model-ship correlation resistance calculation
    #Ra=np.multiply(np.dot(np.multiply(np.dot(0.5,rhosw),U ** 2.0),S),Ca)
    Ra=0.5*rhosw*U**2*S*Ca
    #Total resistance calculation
    Rtotal=np.multiply(Rf,OnePlusk1) + Rapp + Rw + Rb + Rtr + Ra
    return Rtotal,Rf,Rapp,Rw,Rb,Rtr,Ra,OnePlusk1,OnePlusk2eq,Sapp,Cf,Ca

