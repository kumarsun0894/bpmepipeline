# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 15:48:31 2020

@author: Subhin Antony

"""
import numpy as np
from BPME_Common import LastElement as LE
from scipy.interpolate import pchip

def CalibrationHoltrop(U,T,Tref,PratioHoltropref,Uref,Tb,Ts):
    Pratio=np.empty(np.shape(U))
    Pratio[:] = np.nan
    if np.all(np.isnan(PratioHoltropref)):
        return Pratio
    for i in range(0,len(U)):
        Pratio[i]=CalibrationHoltropScalar(U[i],T[i],Tref,PratioHoltropref,Uref,Tb,Ts)
    return   Pratio  



def CalibrationHoltropScalar(U,T,Tref,Pratioref,Uref,Tb,Ts):
     if np.isnan(T) or np.size(T)==0:
         print("T is unavailable!")
         PPratio=np.nan
         return
     elif (T<Tb-0.01) or  (T>Ts*1.042):
         print("T is out of [Tb,Ts] range!") #diagnostic
         PPratio=np.nan
         return
     [imax,_]=np.shape(Uref)
     last=LE.LastElement(Uref)
     if T>=min(Tref) and T<=max(Tref):
         Pratio=np.empty(np.shape(Tref))
         Pratio[:] = np.nan
         for i in range(0,imax):
             if U<Uref[i,1]:
                 Pratio[i]=Pratioref[i,1]
             elif U>=Uref[i,1] and U<=Uref[i,int(last[i])]:
                 pchipfn =  pchip(Uref[i,0:int(last[i])+1],Pratioref[i,0:int(last[i])+1])
                 Pratio[i]=pchipfn(U)    
             elif U>Uref[i,int(last[i])]:
                 Pratio[i]=Pratioref[i,int(last[i])] 
             else:
                 print('Holtrop calibration error!')   
             
         if imax>1:
               for i in range(0,imax-1):
                   if T>=Tref[i] and T<=Tref[i+1]:
                    PPratio=(T-Tref[i])*(Pratio[i+1]-Pratio[i])/(Tref[i+1]-Tref[i])+Pratio[i]
         else:
                PPratio=Pratio

     elif T<min(Tref):
         print("T is out of model test range! Holtrop power ratio will be extrapolated!") #diagnostic
         if U<Uref[0,0]:
             PPratio=Pratioref[0,0]
         elif U>=Uref[0,0] and U<=Uref[0,int(last[0])]:
             pc=pchip(Uref[0,0:int(last[0])+1],Pratioref[0,0:int(last[0])+1])
             PPratio = pc(U)
         elif U>Uref[0,int(last[0])]:
            PPratio=Pratioref[0,int(last[0])]
         else:
            PPratio=np.nan
            print('Holtrop calibration error!')
     elif T>max(Tref):
         print("T is out of model test range! Holtrop power ratio will be extrapolated!") #diagnostic
         if U<Uref[imax-1,0]:
             PPratio=Pratioref[imax-1,0]

         elif U>=Uref[imax-1,0] and U<=Uref[imax-1,int(last[imax-1])]:
            pchip_1=pchip(Uref[imax-1,0:int(last[imax-1])+1],Pratioref[imax-1,0:int(last[imax-1])+1])
            PPratio = pchip_1(U)

         elif U>Uref[imax-1,int(last[imax-1])]:
            PPratio=Pratioref[imax-1,int(last[imax-1])]
         else:
            PPratio=np.nan
            print('Holtrop calibration error!')
     return PPratio   
                       
                 