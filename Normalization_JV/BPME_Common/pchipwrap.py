# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 13:01:48 2020

@author: SubhinAntony
"""

from scipy.interpolate import pchip
import numpy as np
import pandas as pd
def pchipwrap(ar_one,ar_two):
    dic={'ar_1':ar_one,'ar_2':ar_two}
    df=pd.DataFrame(dic)
    df.dropna(subset = ["ar_1","ar_2"], inplace=True)
    df = df.drop_duplicates(subset=["ar_1"])
    pch=pchip(np.array(df.ar_1),np.array(df.ar_2))
    return(pch)
    


#aa=np.array([1,float('nan'),3,4])
#bb=np.array([1,2,3,4])
#
#gg=pchipwrap(aa,bb)
