# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 18:06:48 2020

@author: hadoop
"""
import numpy as np
from BPME_Common import IdealSFOC as isf




def IdealFOC(PDid,FOCMEST,PeffMEST,LCVFOMEST,LCVref,etaT,etaeffME):
    '''
    Calculate MCR as the maximum value of the shop test vector
    This may not be 100% correct in some exceptional case. Currently there is
    a validation in ModelCreator for this. However, for a more robust result,
    we may add PMCRME as an additional input argument in this function.
    '''
    PMCRME=np.max(PeffMEST)
    #Calculate ideal effective power from the ideal delivered power
    Peffid=PDid/(etaT*etaeffME)
    #Calculate the ideal ME load as MCR percentage
    mcrid=Peffid/PMCRME*100
    #Calculate the LCV corrected SFOC from the ME shop test 
    SFOCLCVcorMEST=1000*FOCMEST/PeffMEST*LCVFOMEST/LCVref
    #Calculate the ME load vector as the MCR percentage from the shop test
    MCRMEST=PeffMEST/PMCRME*100
    #Find the ideal LCV corrected SFOC
    SFOCid=isf.IdealSFOC(mcrid,SFOCLCVcorMEST,MCRMEST)
    #Convert SFOCid in g/kWh to FOCid in MT/day
    if (Peffid.ndim==1) and isinstance(SFOCid, np.ndarray):
        FOCid=24*SFOCid.ravel()*Peffid/10**6
    else:
        FOCid=24*SFOCid*Peffid/10**6
    return(FOCid)
    


    



   
