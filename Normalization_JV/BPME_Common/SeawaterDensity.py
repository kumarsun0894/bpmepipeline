# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 11:11:58 2020

@author: akhil.surendran
@Delta:SunKumar

% SeawaterDensity
% Description:  Analytical expression of density as function of temperature
%               and salinity as a parametric 4th order polynomial which
%               arose based on curve fitting. Data from ITTC 7.5-02-01-03,
%               Appendices A and B.
%
% Input:        tw      [°C]    water temperature (vector)
%               Sa      [g/kg]  water salinity (vector)
%
% Output:       rhow    [kg/m3] sea water density (vector)
%
% Note 1:       According to ITTC 7.5-02-01-03: In general, the water
%               properties are a function of temperature, pressure and
%               absolute salinity. In this procedure, data are provided at
%               standard pressure of 0.101325MPa.
%
% Note 2:       The idea is that we define two reference curves:
%               - rhofw for Sa=0g/kg
%               - rhosw for Sa=35.16504g/kg
%               and we generate a linear relationship:
%               rhow(tw,Sa)=lamda(tw)*Sa+beta(tw)
%               where:
%               lamda(tw)=(rhosw-rhofw)/35.16504
%               beta(tw)=rhofw
%
% See also:     STATOTAL

"""


def SeawaterDensity(tw,Sa):
	
	rhofw=-3.4339454E-07*tw**4+6.7267845E-05*tw**3-8.5149115E-03*tw**2+6.4176148E-02*tw+9.9984790E+02

	rhosw=-9.1017501E-08*tw**4+3.3793635E-05*tw**3-6.0827778E-03*tw**2-5.8572317E-02*tw+1.0281606E+03

	rhow=Sa*(rhosw-rhofw)/35.16504+rhofw
	   
	return(rhow)