# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 15:48:31 2020

@author: SunKumar

"""
import numpy as np
import pandas as pd 
#from scipy.interpolate import pchip
from BPME_Common.pchipwrap import pchipwrap as pchip
from BPME_Common import LastElement as LE


def SpeedPowerLowExtrapolation(P=None,Pm=None,Um=None,ExtrapolationMethod=None,*args,**kwargs):
    #Normalized (percentage) distance definition
    dnorm=0.05
    #Normalize data
    Unorm=Um / max(Um)
    Pnorm=Pm / max(Pm)
    #Define normalized interpolation data
    unorm=np.arange(Unorm[0],Unorm[-1],0.001)
    sple=pchip(Unorm,Pnorm)
    pnorm = sple(unorm)
    #Find normalized offset
    for i in range(0,len(unorm)):
        Dnorm=np.sqrt((unorm[i] - unorm[0]) ** 2 + (pnorm[i] - pnorm[0]) ** 2)
        if Dnorm > dnorm:
            Onorm=unorm[i] - Unorm[0]

            break
    
    offset=np.dot(Onorm,max(Um))
    
    #Ustep=0.01;
    
    if ExtrapolationMethod == 'A':
        #Lower end speed points definitions
        Um1=Um[0]
        Um2=Um[0] + offset
        Pm1=Pm[0]
        splk=pchip(Um,Pm)
        Pm2 = splk(Um2)

        b=np.log(Pm2 / Pm1) / np.log(Um2 / Um1)

        a=Pm1 / Um1 ** b
        UL=(P / a) ** (1 / b)
    else:
        print('Speed-Power low extrapolation method error!')
        print('')
        UL=float('nan')

    
    return UL
    
if __name__ == '__main__':
    pass
    


def SpeedPowerHighExtrapolation(P,Pm,Um,ExtrapolationMethod):

    



    
    #Normalized (percentage) distance definition
    dnorm=0.05
    #Normalize data
    Unorm=Um / max(Um)
    Pnorm=Pm / max(Pm)
    #Define normalized interpolation data
    unorm=np.arange(Unorm[0],Unorm[-1],0.001)
    chip=pchip(Unorm,Pnorm)
    pnorm=chip(unorm)
    #Find normalized offset
    for i in range(len(unorm)-1,-1,-1):
        
        
        Dnorm=np.sqrt((unorm[-1] - unorm[i]) ** 2 + (pnorm[-1] - pnorm[i]) ** 2)
        if Dnorm > dnorm:
            Onorm=Unorm[-1] - unorm[i]
            break
    
    offset=np.dot(Onorm,max(Um))
    
    #Ustep=0.01;
    
    if ExtrapolationMethod == 'A':
        #High end speed points definitions
        Um1=Um[-1] - offset
        Um2=Um[-1]
        pc=pchip(Um,Pm)
        Pm1 = pc(Um1)
        Pm2=Pm[-1]
        b=(np.dot(Pm1,Um2 ** 3) - np.dot(Pm2,Um1 ** 3)) / (Um2 ** 3 - Um1 ** 3)
        a=(Pm1 - b) / Um1 ** 3
        UH=((P - b) / a) ** (1 / 3)
    else:
        print('Speed-Power high extrapolation method error!')
        print('')
        UH=float('nan')
 
    
    return UH
    
if __name__ == '__main__':
    pass

def IdealSpeedScalar(P,T,UHid,Tref,Pref,Uref,Tb,Ts):



    
    #Check if T is within aceeptable range
    if T.size == 0 or pd.isnull(T):
        print('T is unavailable!')
        UUid=float('nan')
        return UUid
    elif T < Tb - 0.01 or T > Ts *1.042: #Ttrfr=Tfr+Ttr-Ts=Ts*(1+1/48)+Ts*(1+1/48)-Ts=Ts*1.042
        
        print('T is out of [Tb,Ts] range!')
        UUid=float('nan')
        return UUid
    
    #Standard approach
    if (T >= min(Tref)) and (T <= max(Tref)):
        if (Uref.size) !=0:
            
            
            #Identify Uref size
            try:
                imax,j = Uref.shape
            except:
                imax = 1
                # pass
        
            last=LE.LastElement(Uref).astype(int)+1
            U=np.full(Tref.shape,np.nan)
            for i in range(0,imax):
                if P < Pref[i,0]:
                    U[i]=SpeedPowerLowExtrapolation(P,Pref[i,0:last[i]],Uref[i,0:last[i]],'A')
                elif P >= Pref[i,0] and P <= Pref[i,last[i]-1]:
                    pc=pchip(Pref[i,0:last[i]],Uref[i,0:last[i]])
                    U[i]=pc(P)
                elif P > Pref[i,last[i]-1]:
                    U[i]=SpeedPowerHighExtrapolation(P,Pref[i,0:last[i]],Uref[i,0:last[i]],'A')
                else:
                    print('IdealSpeed error!')
                #Find speed for P and T
            if imax > 1:
                for i in range(0,imax - 1):
                    if T >= Tref[i] and T <= Tref[i + 1]:
                        UUid=np.multiply((T - Tref[i]),(U[i + 1] - U[i])) / (Tref[i + 1] - Tref[i]) + U[i]
            else:
                UUid=U
                            
                    
        elif Uref.size == 0:
            print('Uref is empty!')
            print('')
            UUid=float('NaN')
        else:
            print('Uid error!')
            print('')
            UUid=float('NaN')
        
    #Holtrop approach
    else:
        if T < min(Tref) or T > max(Tref):
            print('T is out of model test range! Holtrop method will be used!')
            UUid=UHid
            
    #Negative values filtering (which generates complex numbers in some calculations)
    
    if UUid < 0:
        UUid=float('NaN')
    
    return UUid
    
if __name__ == '__main__':
    pass




def IdealSpeed(Pcor,T,UHid,Tref,Pref,Uref,Tb,Ts):
    Pcor,T,UHid=Pcor.astype('float64'),T.astype('float64'),UHid.astype('float64')
    if Pcor.ndim==0:
        Pcor=Pcor.reshape(1,)

    
    if T.ndim==0:
        T=T.reshape(1,)         

    

    
    try:
        i,j = Pref.shape
    except:
        Pref = np.reshape(Pref,(1, Pref.size))
    try:
        k,l = Uref.shape
    except:
        Uref = np.reshape(Uref,(1,Uref.size))


    
    #Memory pre-allocation
    Uid=np.full(Pcor.shape,np.nan)
    for i in range(0,len(Pcor)):

        Uid[i]=IdealSpeedScalar(Pcor[i],T[i],UHid[i],Tref,Pref,Uref,Tb,Ts)
    
    return Uid

    
    

