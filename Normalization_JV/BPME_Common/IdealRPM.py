"""
Created on Wed Apr  1 10:55:45 2020

@author:SunKumar
"""



import numpy as np

# from BPME_Repository import BPMEngineSingleVessel as BSV
from BPME_Common import LastElement as LE 
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import pchip

def RPMSpeedLowExtrapolation(U,Nm,Um,ExtrapolationMethod):



    
    #Normalized (percentage) distance definition
	dnorm=0.05
    #Normalize data
	Unorm=Um / max(Um)
	Nnorm=Nm / max(Nm)
    #Define normalized interpolation data
	unorm=np.arange(Unorm[0],Unorm[-1],0.001)
	sple=pchip(Unorm,Nnorm)
	nnorm = sple(unorm)
    #Find normalized offset
	for i in range(0,len(unorm)):
		Dnorm=np.sqrt((unorm[i] - unorm[0]) ** 2 + (nnorm[i] - nnorm[0]) ** 2)
		if Dnorm > dnorm:
			Onorm=unorm[i] - Unorm[0]
			break
    
	offset=np.dot(Onorm,max(Um))
    
    #Ustep=0.01;
    
	if ExtrapolationMethod == 'A':
        #Lower end speed points definitions
		Um1=Um[0]
		Um2=Um[0] + offset
		Nm1=Nm[0]
		splk=pchip(Um,Nm)
		Nm2 = splk(Um2)
		b=np.log(Nm2 / Nm1) / np.log(Um2 / Um1)
		a=Nm1 / Um1 ** b
		NL=np.dot(a,U ** b)

	else:
		print('RPM-Speed low extrapolation method error!')
		print('')
		NL=float('NaN')
    
	return NL
    
if __name__ == '__main__':
    pass

def RPMSpeedHighExtrapolation(U,Nm,Um,ExtrapolationMethod):



###########################################################################
    
    #Normalized (percentage) distance definition
	dnorm=0.05
    #Normalize data
	Unorm=Um / max(Um)
	Nnorm=Nm / max(Nm)

    #Define normalized interpolation data
	unorm=np.arange(Unorm[0],Unorm[-1],0.001)

	spl=pchip(Unorm,Nnorm)
	nnorm = spl(unorm)
    #Find normalized offset
	for i in range(len(unorm)-1,-1,-1):
		Dnorm=np.sqrt((unorm[-1] - unorm[i]) ** 2 + (nnorm[-1] - nnorm[i]) ** 2)
		if Dnorm > dnorm:
			Onorm=Unorm[-1] - unorm[i]
			break
    
	offset=np.dot(Onorm,max(Um))
    
    #Ustep=0.01;
    
	if ExtrapolationMethod == 'A':
        #High end speed points definitions
		Um1=Um[-1] - offset
		Um2=Um[-1]
		spln=pchip(Um,Nm)
		Nm1 = spln(Um1)
		
		Nm2=Nm[-1]
		b=(np.dot(Nm1,Um2 ** 2) - np.dot(Nm2,Um1 ** 2)) / (Um2 ** 2 - Um1 ** 2)
		a=(Nm1 - b) / Um1 ** 2
		NH=np.dot(a,U ** 2) + b

	else:
		print('RPM-Speed high extrapolation method error!')
		print('')
		NH=float('NaN')
    
	return NH
    


    
    

def IdealRPMScalar(U,T,NHid,Tref,Nref,Uref,Tb,Ts):



    
    #Check if T is within aceeptable range
	if T.size == 0 or np.isnan(T):
		print('T is unavailable!')
		NNid=float('NaN')

		return NNid
	else:
		if (T < Tb - 0.01) or (T > Ts *1.042):
			print('T is out of [Tb,Ts] range!')
			NNid=float('NaN')
			return NNid
    
    #Standard approach
	if T >= min(Tref) and T <= max(Tref):
        #Identify Uref size
 		#imax=Uref.size
		try:
			imax,j = Uref.shape
		except:
 			imax = 1
 			# pass
		
		last=LE.LastElement(Uref).astype(int)+1
		#last = [8]

		
		N=np.full(Tref.shape,np.nan)
		for i in range(0,imax):
			if U < Uref[i,0]:
				N[i]=RPMSpeedLowExtrapolation(U,Nref[i,0:last[i]],Uref[i,0:last[i]],'A')
			elif U >= Uref[i,0] and U <= Uref[i,last[i]-1]:
				spline=pchip(Uref[i,0:last[i]],Nref[i,0:last[i]])
				N[i] = spline(U)
			elif U > Uref[i,last[i]-1]:
				N[i]=RPMSpeedHighExtrapolation(U,Nref[i,0:last[i]],Uref[i,0:last[i]],'A')
			else:
				print('IdealRPM error!') #diagnostic
        #Find the RPM for U and T
		if imax > 1:
			for i in range(0,imax - 1):
				if T >= Tref[i] and T <= Tref[i + 1]:
					NNid=np.multiply((T - Tref[i]),(N[i + 1] - N[i])) / (Tref[i + 1] - Tref[i]) + N[i]
		else:
			NNid=N
        #Holtrop approach
	else:
		if T < min(Tref) or T > max(Tref):
			print('T is out of model test range! Holtrop method will be used!')
			NNid=NHid
    
	return NNid
    
if __name__ == '__main__':
    pass
    
def IdealRPM(U,T,NHid,Tref,Nref,Uref,Tb,Ts):
	try:
		i,j = Nref.shape
	except:
		Nref = np.reshape(Nref,(1, Nref.size))
	try:
		k,l = Uref.shape
	except:
		Uref = np.reshape(Uref,(1,Uref.size))


    
    #Memory pre-allocation
	Nid=np.full(U.shape,np.nan)

	for i in range(0,len(U)):
		Nid[i]=IdealRPMScalar(U[i],T[i],NHid[i],Tref,Nref,Uref,Tb,Ts)

    
	return Nid
    

   

    
    

    