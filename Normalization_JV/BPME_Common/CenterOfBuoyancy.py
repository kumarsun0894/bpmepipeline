# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 10:55:45 2020

@author: SubhinAntony
"""
import numpy as np
import pandas as pd
from scipy.interpolate import pchip
def CenterOfBuoyancy(Tm,Lwl,xLwl0_5,Th,LCBh):
    if ~(np.any(pd.isnull(LCBh))):
        #sort
        if np.any(~(Th==np.sort(Th))):
            df = pd.DataFrame({'Th':Th,'LCBh':LCBh},columns=['Th','LCBh'])
            df=df.sort_values('Th')
            Th=np.array(df['Th']).astype('float64')
            LCBh=np.array(df['LCBh']).astype('float64')
        
        pcfn=pchip(Th,LCBh)
        LCB=pcfn(Tm.astype('float64')) #a high and low extrapolation rule must be applied
    elif np.any(pd.isnull(LCBh)):
        LCB=Lwl/2
    else:
        print('LCB error!')
        LCB=np.nan
    if LCB.ndim==1:
        LCB=LCB.reshape(1,-1)
        LCB=np.transpose(LCB)
    if xLwl0_5.ndim==1:
        xLwl0_5=xLwl0_5.reshape(1,-1)
        xLwl0_5=np.transpose(xLwl0_5)
    lcb=(LCB-xLwl0_5)/xLwl0_5*100
    return LCB,lcb
        