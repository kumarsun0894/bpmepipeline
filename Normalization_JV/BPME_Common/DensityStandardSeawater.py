# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 11:11:58 2020

@author: akhil.surendran
"""


def DensityStandardSeawater(tw):
        rhow=-9.1017501E-08*tw**4+3.3793635E-05*tw**3-6.0827778E-03*tw**2-5.8572317E-02*tw+1.0281606E+03
        return(rhow)