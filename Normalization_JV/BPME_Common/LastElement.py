# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 00:58:46 2020

@author: SubhinAntony

% LastElement
% Description:  This function takes a matrix with possible NaNs at the end
%               of each row and returns the index of the last not-NaN
%               element per row.
%
% Input:        matrix       [-]    a matrix with possible NaNs at the end of each row (matrix)
%
% Output:       last         [%]    indices for thelast not-NaN element per row (vector)
%
% See also:     STNormalization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
"""
import numpy as np
import pandas as pd

def LastElement(matrix):
    if matrix.ndim==1:
        matrix=matrix.reshape(1,-1)
    #Identify matrix size
    [imax,jmax]=np.shape(matrix)
    #Memory pre-allocation
    last=np.empty((1,imax))
    last[:]=np.nan
    #Define last element in the matrix that is not NaN
    for i in range(0,imax):
        for j in range(0,jmax):
            if not(pd.isnull(matrix[i,j])):
                last[0][i]=j
    return(last.ravel())
                
    
   
            
        
                

