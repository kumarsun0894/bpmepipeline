# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 11:55:09 2020

@author: akhil.surendran
"""
def ViscosityStandardSeawater(tw):
        vw=-2.041E-15*tw**5+3.823E-13*tw**4-3.169E-11*tw**3+1.634E-09*tw**2-6.289E-08*tw+1.854E-06
        return(vw)

        