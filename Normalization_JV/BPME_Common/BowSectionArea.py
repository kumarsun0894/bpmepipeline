# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 14:31:53 2020

@author: Subhin Antony
@Delta:hadoop
"""
import numpy as np
#from scipy.interpolate import pchip

"""
Created on Thu Mar 26 11:11:58 2020

@author: akhil.surendran

% BowSectionArea
% Description:  This function calculates the underwater cross section area
%               along with the centroid of the bulbous bow at the fore
%               perpendicular. This info is requested for the resistance
%               estimation method (Holtrop & Mennen, 1982).
%
% Input:        Tf
%               BowSection
%               BOE is a boolean variable to indicate the existence or absence of a bulbous bow.
%
% Output:       Ab
%               hb
%               yb_actual
%               zb_actual
%
% Note1:        If drawings are missing an estimation algorithm is used as
%               proposed by (Shiplab)
%               Ab=pi*(Tf/2)^2*BOE/7.7
%               hb=Tf/2
%
% Note2:        The formula above, after algebraic manipulation yields:
%               Ab=0.102*Tf
%
% Note3:        A new formula (winter 2020) is used to estimate the bow
%               section itself with great results. The assumption is that
%               the section is defined fully by is upper and lower z-axis
%               points which can be identified from the GA plan. Based on
%               these two values, e.g. 0.5m and 7.5m, an ellispoid formula
%               is used to define an estimated bow section:
%               center: (yc , zc) = (0 , min(z)+r)
%               theta = [-pi/2:pi/2]
%               r = (max(z) - min(z)) / 2
%               yb = r/2 * cos(th) + yc
%               zb = r   * sin(th) + zc
%
% References:   - Holtrop, J., & Mennen, G. G. J. (1982). An approximate
%                 power prediction method. International Shipbuilding
%                 Progress, 29(335), 166-170.
%               - http://shiplab.hials.org/app/holtrop/
%
% See also:     ResistanceHoltrop

"""



#import scipy.interpolate as ipchip
import pandas as pd
from scipy.interpolate import pchip

from shapely.geometry import Polygon



def BowSectionArea(Tf,BowSection,BOE):

    #Check if bulbous bow exists
    if "Yes" in BOE:
        #Define the bow section curve
        #Check if 2 points only of the Bow Section are provided and make estimate based on an ellipsoid shape
        if np.all(~(np.isnan((BowSection)))) and len(BowSection[:,0])==2:
            th=np.transpose(np.arange(-np.pi/2,((np.pi/2)+(np.pi/10)),np.pi/10))                           ##
            r=(np.max(BowSection[:,1])-np.min(BowSection[:,1]))/2
            yb=r/2*np.cos(th)
            zb=r*np.sin(th)+np.min(BowSection[:,1])+r
        #Check if a full Bow Section is provided 
        elif np.all(~(np.isnan((BowSection)))) and  len(BowSection[:,0])>2:
            yb=BowSection[:,0]
            zb=BowSection[:,1]
        else:
            yb=np.nan
            zb=np.nan
        # sort
        if not(isinstance(zb, float)):
            if np.any(~(zb==np.sort(zb))):
                dataset = pd.DataFrame({'zb': zb, 'yb': yb}, columns=['zb', 'yb'])
                dataset=dataset.sort_values('zb')
                zb=np.array(dataset['zb']).astype('float64')
                yb=np.array(dataset['yb']).astype('float64')
        
        #Check if Bow Section is provided
        if ~(np.any((np.isnan(yb)))):
            #Memory pre-allocation
            Ab=np.empty(np.size(Tf))
            Ab[:]=np.nan
            hb=np.empty(np.size(Tf))
            hb[:]=np.nan
            yb_actual=np.empty((len(Tf),len(yb)+2))
            yb_actual[:]=np.nan
            
            zb_actual=np.empty((len(Tf),len(yb)+2))
            zb_actual[:]=np.nan
            
            for i in range(0,len(Tf)):
                if Tf[i]>np.min(zb) and Tf[i]<np.max(zb):
                    pchip_1=pchip(zb,yb)                                    ##
                    ybwl=pchip_1(Tf[i])
                    
                    zbtemp=np.concatenate((zb[zb<Tf[i]].ravel(),Tf[i].ravel(),Tf[i].ravel()),axis=None)
                    
                    zbtemp=np.transpose(zbtemp)
                   
                    ybtemp=np.concatenate((yb[zb<Tf[i]].ravel(),ybwl.ravel(),np.zeros(1)),axis=None)
                   
                   
                    ybtemp=np.transpose(ybtemp)
                    
                    
                    
                    zb_actual[i,0:len(zbtemp)]=zbtemp
                    yb_actual[i,0:len(ybtemp)]=ybtemp
                    
                    coordinate = []
                    for j in range(ybtemp.size):
                        coordinate.append((ybtemp[j], zbtemp[j]))
                    poly = Polygon(coordinate)
                    Ab[i]=2*poly.area                                            ##
                    cent=poly.centroid
                    cent=np.array(cent)
                    hb[i]=cent[1]
                    
                elif Tf[i]<=np.min(zb):
                    zb_actual[i,:]=np.nan
                    yb_actual[i,:]=np.nan
                    Ab[i]=0
                    hb[i]=0
                elif Tf[i]>=np.max(zb):
                    zb_actual[i,0:len(zb)]=np.transpose(zb)
                    yb_actual[i,0:len(yb)]=np.transpose(yb)
                    
                    
                    coordinate = []
                    for k in range(yb.size):
                        coordinate.append((yb[k], zb[k]))
                    poly = Polygon(coordinate)
                    Ab[i]=2*poly.area                                            ##
                    cent=poly.centroid
                    cent=np.array(cent)
                    hb[i]=cent[1]
                    
                    
                else:
                    print('Ab error!')
        
            
        else:
            #Use estimation algorithm by Shiplab
            Ab=0.102*Tf**2
            hb=Tf/2
            zb_actual=np.nan
            yb_actual=np.nan
            
    else:
        zb_actual=np.nan
        yb_actual=np.nan
        hb=np.zeros(np.size(Tf))
        Ab=np.zeros(np.size(Tf))
    return(Ab,hb,yb_actual,zb_actual)
        
                

            
# Tf=np.array([5.6,5.6,5.6,5.6,5.6,5.6,5.6,5.6])  
# BowSection=np.array([[0.00,0.00],[1.16,11.76]]) 
# BowSection = np.transpose(BowSection)   
# BOE="Yes"    
