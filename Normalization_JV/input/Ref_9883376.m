NameVessel="Gloria Confidence";
NoonVerModel="Y";
HPVerModel="Y";
MEVerModel="Y";
AEVerModel="Y";
AutoVerModel=[NaN];
HPLogConfig="Y";
MELogConfig="Y";
AELogConfig="Y";
NoonLogConfig="Y";
ExNames=[NaN];
NotesModel=[NaN];
lastprocessedNoon=[NaN];
lastprocessedHP=[NaN];
lastprocessedME=[NaN];
lastprocessedAE=[NaN];
HullNoVessel="DY4073";
IMONoVessel=9883376;
BuildYardVessel="NEW DAYANG SHIPBUILDING CO";
YearBuildVessel="2019";
UltimateOwnerVessel="Sumec Shipping";
TypeVessel="BULK";
TypeSTAWINDVessel="BC";
SuperStructureSTAWINDVessel="CRA";
Loa=199.99;
Lbp=193.74;
B=32.26;
D=18.50;
Td=11.30;
Tb=6.00;
Ts=13.30;
Ud=14.25;
CapacityCargoVessel=77491;
DWT=63102;
nCHVessel=5;
TypeCGVessel="CRA";
nCGVessel=[NaN];
CapacityCGVessel=[NaN];
Ds=74922;
Vs=72808;
Dd=62528;
Vd=60739;
Ad=620;
Zad=31.30;
BulbousBowVessel=[NaN];
BowSection=[NaN];
Cstern="N";
TransomSternVessel=[NaN];
HullProfile=[NaN];
RudderProfile=[NaN];
TransomSection=[NaN];
nISBRG=1;
etaS=0.990;
etaB=1.000;
AutoLoggingVessel=[NaN];
vesselsensd=[NaN];
vesselwp="Meteo";
vesselpp=[NaN];
DrawingURLVessel="https://bsmcloud.sharepoint.com/:f:/s/FPC-ShipDrawings/EvO46tlIFNVJuIYQLyeS8u4BRXJLh-JPhKhd0I2nDzoWTg?e=dJUUtt";
rhoswh=1025.0;
Th=[2.00,4.00,6.00,8.00,10.00,12.00,14.00];
Dh=[9702.6,20264.3,31273.5,42696.7,54573.5,66854.1,79273.6];
WSAh=[5376.50,6265.00,7144.60,8048.30,8996.80,9887.50,10712.90];
Cmh=[0.982,0.991,0.994,0.995,0.996,0.997,0.997];
Cwph=[0.800,0.841,0.873,0.906,0.943,0.965,0.970];
LCBh=[106.89,106.37,105.58,104.42,102.97,101.43,100.12];
TaMT=[7.40,11.30];
TfMT=[4.60,11.30];
VMT=[30030.00,60774.00];
PowerTypeMT="D";
U0MT=[12.00,12.50,13.00,13.50,14.00,14.50,15.00,15.50,16.00,16.50;12.00,12.50,13.00,13.50,14.00,14.50,15.00,15.50,16.00,16.50];
P0MT=[2696,3096,3585,4141,4768,5492,6372,7424,8648,10108;3185,3631,4136,4707,5365,6128,7021,8133,9468,10889];
N0MT=[67.77,70.88,74.23,77.77,81.44,85.35,89.51,93.89,98.62,103.51;72.30,75.50,78.80,82.20,85.70,89.40,93.40,97.70,102.40,107.26];
etaD0MT=[NaN];
U15MT=[NaN];
P15MT=[NaN];
N15MT=[NaN];
etaD15MT=[NaN];
TrialIndexMT=1;
UtrialMT=[NaN];
PtrialMT=[NaN];
NtrialMT=[NaN];
nProp=1;
TypeProp="FPP";
dProp=6.900;
pitchProp=5.353;
zProp=4;
AeAoProp=0.438;
HullNoSTrial="DY4073";
PowerTypeSTrial="S";
TaConSTrial=7.40;
TfConSTrial=4.60;
VConSTrial=30030.00;
TaSTrial=7.50;
TfSTrial=4.43;
VSTrial=30644.00;
UCorSTrial=[14.57,15.07,15.29];
PCorSTrial=[5383,6119,6511];
NCorSTrial=[83.90,87.30,89.10];
WeatherCorSTrial="Yes";
TEUSTrial=[NaN];
WSASTrial=[NaN];
TswSTrial=[NaN];
rhoswSTrial=[NaN];
TairSTrial=[NaN];
pairSTrial=[NaN];
azbowSTrial=[NaN];
SOGSTrial=[NaN];
PSTrial=[NaN];
NPropSTrial=[NaN];
HeadSTrial=[NaN];
UwirSTrial=[NaN];
psiwirSTrial=[NaN];
HwvSTrial=[NaN];
TwvSTrial=[NaN];
psiwvrSTrial=[NaN];
HslSTrial=[NaN];
TslSTrial=[NaN];
psislrSTrial=[NaN];
hswSTrial=[NaN];
nME=1;
MakerME=["MAN"];
BuilderME=["CSE"];
ModelME=["5S60ME-C8.1"];
SerNoME=["CSB075"];
CategoryME=["2"];
TypeME=["ME"];
PistConfME=["INLINE"];
ModeME=[NaN];
etaeffME=[0.990];
ncME=[5];
dPISTME=[0.600];
sPISTME=[2.400];
PMCRME=[8300.0];
PNCRME=[6640];
NMCRME=[91.00];
NNCRME=[84.50];
DualFuelME=["No"];
VEGBPME=["Yes"];
LIWATME=["No"];
IARME=["No"];
MBTME=["No"];
CBTME=["No"];
AVME=["Yes"];
TVME=["No"];
TCCOSME=["No"];
nTCME=[1];
MakerTCME=["MAN"];
ModelTCME=["TCA55-26"];
NmaxTCME=[NaN];
TmaxTCME=[500];
muTCME=[NaN];
dTCcompME=[NaN];
TypeGovME=[NaN];
ModelGovME=[NaN];
TypeLBME=["E"];
MakerLBME=["MAN"];
ModelLBME=["ALPHA"];
PulseLBME=["NA"];
HighPISTME=["NA"];
RegLBME=[NaN];
BNLBME=[100];
ACCLBME=[0.30];
SCOCminME=[0.60];
SCOCmaxME=[2.21];
SCOCbME=[NaN];
SCOCidME=[NaN];
CoolantJKTME=[NaN];
CoolantFOVVME=[NaN];
CoolantLOCoolerME=[NaN];
CoolantACME=[NaN];
CoolantTCME=[NaN];
nSPME=[NaN];
nACTRME=[NaN];
MakerDiagToolME=[NaN];
ModelDiagToolME=[NaN];
TypeDiagToolME=[NaN];
InstallationDiagToolME=[NaN];
LibcodeME=[NaN];
ModeMESim=[NaN];
MCRMESim=[NaN];
NMESim=[NaN];
PMESim=[NaN];
SFOCisoMESim=[NaN];
TegEVoutMESim=[NaN];
mdotegMESim=[NaN];
mdotSteamProductionMESim=[NaN];
nMEST=1;
HullNoMEST=["DY4073"];
SerNoMEST=["CBS075"];
NoME=[1];
FuelModeMEST=["F"];
PowerTypeMEST=["E"];
ModeMEST=[NaN];
LCVFOMEST=[43.2];
rhoFOMEST=[836.7];
LCVGOMEST=[NaN];
rhoGOMEST=[NaN];
MCRMEST=[25.0,50.0,75.0,80.0,100.0,110.0];
NMEST=[57.10,72.30,82.70,84.50,91.10,93.80];
PMEST=[2064,4157,6226,6646,8299,9126];
NTCMEST=[6988,11655,14610,15044,16760,17440];
FPIMEST=[44.0,65.0,84.0,87.0,100.0,106.0];
TFOPPinMEST=[30,30,32,30,30,31];
TGOinMEST=[NaN];
pGOinMEST=[NaN];
FOCMEST=[372.8,726.4,1033.6,1104.0,1419.2,1616.8];
SFOCMEST=[180.62,174.74,166.01,166.11,171.00,177.16];
SFOCisoMEST=[181.50,175.60,167.00,166.90,171.70,177.80];
GOCMEST=[NaN];
SGOCMEST=[NaN];
SGOCisoMEST=[NaN];
TairTCinMEST=[30,31,29,29,31,33];
pambairMEST=[1013,1013,1013,1013,1013,1013];
TscavMEST=[35,32,33,35,37,38];
pscavMEST=[0.33,0.95,1.70,1.86,2.48,2.75];
pindMEST=[7.4,11.2,14.3,14.9,17.1,18.2];
peffMEST=[6.4,10.2,13.3,13.9,16.1,17.2];
pmaxMEST=[91.0,119.7,149.8,154.2,159.8,160.3];
pcompMEST=[55.7,86.2,107.0,111.3,127.8,137.5];
TegEVoutMEST=[242,284,310,318,355,373];
pegRMEST=[0.25,0.85,1.55,1.70,2.34,2.57];
TegTCinMEST=[290,330,358,370,400,420];
pegTCinMEST=[0.24,0.81,1.48,1.62,2.23,2.45];
TegTCoutMEST=[250,250,240,230,245,250];
pegTCoutMEST=[60.0,180.0,390.0,430.0,590.0,630.0];
TcwACinMEST=[25,26,30,30,31,31];
TcwACoutMEST=[26,28,36,36,39,39];
TairACinMEST=[60,100,150,150,180,190];
TairACoutMEST=[26,27,31,33,35,36];
dpairACMEST=[30.0,45.0,65.0,75.0,85.0,95.0];
dpairAFMEST=[25.0,75.0,115.0,125.0,180.0,200.0];
AuxBlowMEST=[1,0,0,0,0,0];
MCRNOxMEST=[NaN];
NOxisoMEST=[NaN];
nAE=3;
MakerAE=["Yanmar","Yanmar","Yanmar"];
BuilderAE=[NaN,NaN,NaN];
PistConfAE=["INLINE","INLINE","INLINE"];
ModelAE=["6EY18ALW","6EY18ALW","6EY18ALW"];
SerNoAE=["9655","9656","9658"];
ncAE=[6,6,6];
dPISTAE=[0.180,0.180,0.180];
sPISTAE=[0.280,0.280,0.280];
PengnomAE=[800.0,800.0,800.0];
PgennomAAE=[937.5,937.5,937.5];
NnomAE=[900,900,900];
PFnomAE=[0.800,0.800,0.800];
DualFuelAE=["No","No","No"];
ECNTRAE=["No","No","No"];
nTCAE=[1,1,1];
MakerTCAE=[NaN;NaN;NaN];
ModelTCAE=[NaN;NaN;NaN];
nTCinletAE=[NaN,NaN,NaN];
NmaxTCAE=[NaN;NaN;NaN];
TmaxTCAE=[NaN;NaN;NaN];
muTCAE=[NaN;NaN;NaN];
dTCcompAE=[NaN;NaN;NaN];
CoolantJKTAE=[NaN,NaN,NaN];
CoolantFOVVAE=[NaN,NaN,NaN];
CoolantLOCoolerAE=[NaN,NaN,NaN];
CoolantACAE=[NaN,NaN,NaN];
CoolantTCAE=[NaN,NaN,NaN];
MakerDiagToolAE=[NaN,NaN,NaN];
ModelDiagToolAE=[NaN,NaN,NaN];
TypeDiagToolAE=[NaN,NaN,NaN];
InstallationDiagToolAE=[NaN,NaN,NaN];
LibcodeAE=["651.01","651.02","651.03"];
nAEST=3;
HullNoAEST=["DY4073","DY4073","DY4073"];
SerNoAEST=["9655","9655","9655"];
NoAE=[1,2,3];
FuelModeAEST=["F","F","F"];
LCVFOAEST=[42.590,42.590,42.590];
rhoFOAEST=[862.7,862.7,862.7];
LCVGOAEST=[NaN,NaN,NaN];
rhoGOAEST=[NaN,NaN,NaN];
LoadTypeAEST=["G","G","G"];
MCRAEST=[25.0,50.0,75.0,100.0,110.0;25.0,50.0,75.0,100.0,110.0;25.0,50.0,75.0,100.0,110.0];
etagenAEST=[0.960;0.960;0.960];
NAEST=[900.00,900.00,900.00,900.00,900.00;900.00,900.00,900.00,900.00,900.00;900.00,900.00,900.00,900.00,900.00];
PengAEST=[200.0,400.0,600.0,800.0,880.0;200.0,400.0,600.0,800.0,880.0;200.0,400.0,600.0,800.0,880.0];
PgenAEST=[187.5,375.0,562.5,750.0,825.0;187.5,375.0,562.5,750.0,825.0;187.5,375.0,562.5,750.0,825.0];
NTCAEST=[NaN;NaN;NaN];
FPIAEST=[22.5,25.5,28.5,31.8,33.5;22.5,25.5,28.5,31.8,33.5;22.5,25.5,28.5,31.8,33.5];
TFOPPinAEST=[NaN;NaN;NaN];
TGOinAEST=[NaN;NaN;NaN];
pGOinAEST=[NaN;NaN;NaN];
FOCAEST=[46.8,79.9,115.5,153.4,171.6;46.8,79.9,115.5,153.4,171.6;46.8,79.9,115.5,153.4,171.6];
SFOCAEST=[NaN,NaN,NaN,193.930,NaN;NaN,NaN,NaN,193.930,NaN;NaN,NaN,NaN,193.930,NaN];
SFOCisoAEST=[NaN;NaN;NaN];
GOCAEST=[NaN;NaN;NaN];
SGOCAEST=[NaN;NaN;NaN];
SGOCisoAEST=[NaN;NaN;NaN];
TairTCinAEST=[32,32,31,31,30;32,32,31,31,30;32,32,31,31,30];
pambairAEST=[1007,1007,1007,1007,1007;1007,1007,1007,1007,1007;1007,1007,1007,1007,1007];
pmaxAEST=[66.0,108.0,148.0,178.0,186.0;66.0,108.0,148.0,178.0,186.0;66.0,108.0,148.0,178.0,186.0];
pcompAEST=[NaN;NaN;NaN];
TegEVoutAEST=[316,352,370,431,465;316,352,370,431,465;316,352,370,431,465];
TegTCinAEST=[438,477,480,530,564;438,477,480,530,564;438,477,480,530,564];
TegTCoutAEST=[357,344,308,326,350;357,344,308,326,350;357,344,308,326,350];
TcwACinAEST=[37,37,36,36,36;37,37,36,36,36;37,37,36,36,36];
TcwACoutAEST=[37,37,37,38,38;37,37,37,38,38;37,37,37,38,38];
TairACinAEST=[78,145,198,234,247;78,145,198,234,247;78,145,198,234,247];
TscavAEST=[46,47,47,49,52;46,47,47,49,52;46,47,47,49,52];
pscavAEST=[0.48,1.40,2.50,3.45,3.79;0.48,1.40,2.50,3.45,3.79;0.48,1.40,2.50,3.45,3.79];
dpairACAEST=[NaN;NaN;NaN];
dpairAFAEST=[NaN;NaN;NaN];
MCRNOxAEST=[NaN;NaN;NaN];
NOxisoAEST=[NaN;NaN;NaN];
nBL=1;
FluidBL=["S"];
MakerBL=[NaN];
ModelBL=[NaN];
DualFuelBL=["No"];
SteamCapacityBL=[NaN];
nFM=2;
FluidFM=["FO","FO"];
EquipmentFM_new=["ME1","AE1"];
EquipmentFM=["ME1","AE1"];
LocationFM=["In","In"];
TypeFM=["V","V"];
MakerFM=[NaN,NaN];
ModelFM=[NaN,NaN];
FaccuracyFM=[NaN,NaN];
InterfaceFM=[NaN,NaN];
MakerSL=[NaN];
ModelSL=[NaN];
UaccuracySL=[NaN];
SPM="No";
nSPM=[NaN];
ECNTRSPM=[NaN];
LocationSPM=[NaN];
MakerSPM=[NaN];
ModelSPM=[NaN];
QaccuracySPM=[NaN];
NaccuracySPM=[NaN];
InterfaceSPM=[NaN];
SThruster="No";
nSThruster=[NaN];
LocationSThruster=[NaN];
dSThruster=[NaN];
pitchSThruster=[NaN];
dtunSThruster=[NaN];
VnomSThruster=[NaN];
AnomSThruster=[NaN];
PnomSThruster=[NaN];
NnomSThruster=[NaN];
GCU="No";
nGCU=[NaN];
MakerGCU=[NaN];
ModelGCU=[NaN];
CapacityGCU=[NaN];
SG="No";
nSG=[NaN];
ECNTRSG=[NaN];
MakerSG=[NaN];
ModelSG=[NaN];
PnomSG=[NaN];
NnomSG=[NaN];
etaSG=[NaN];
RG="No";
nRG=[NaN];
MakerRG=[NaN];
ModelRG=[NaN];
RatioRG=[NaN];
etaRG=[NaN];
STurbine="No";
nSTurbine=[NaN];
MakerSTurbine=[NaN];
ModelSTurbine=[NaN];
PnomSTurbine=[NaN];
NnomSTurbine=[NaN];
DualFuelSTurbine=[NaN];
GTurbine="No";
nGTurbine=[NaN];
MakerGTurbine=[NaN];
ModelGTurbine=[NaN];
PnomGTurbine=[NaN];
NnomGTurbine=[NaN];
DualFuelGTurbine=[NaN];
PMotor="No";
nPMotor=[NaN];
ECNTRPMotor=[NaN];
MakerPMotor=[NaN];
ModelPMotor=[NaN];
TypePMotor=[NaN];
PnomPMotor=[NaN];
NnomPMotor=[NaN];
IGG="No";
nIGG=[NaN];
MakerIGG=[NaN];
ModelIGG=[NaN];
CapacityIGG=[NaN];
INC="Yes";
nINC=[NaN];
MakerINC=[NaN];
ModelINC=[NaN];
EG="Yes";
nEG=[NaN];
TmCP=[NaN];
FwiCP=[NaN];
SSNCP=[NaN];
UCP=[NaN];
FOCMECP=[NaN];
LCVFOMECP=[NaN];
g=9.8067;
Zaid=10;
kyy=0.25;
ksiP=0;
ksiN=0.2;
ksiU=-0.33;
LCVMDOref=42.7;
LCVHFOref=40.6;
Ck2=1.309;
TCNomogram=[-0.00126,1.063;-0.000964,1.939];
CorAllowance=50;
MCRidAE=80;
RHmarginAE=1;
F1iso=[-0.002446,0.002856,0.002954,0.002198];
F2iso=[-0.00059,-0.00222,-0.00153,-0.00081];
Kiso=[273,1,1,1];
Cpcomp=[0.000226,-0.002954,0.00153,-7.02E-05];
Cpmax=[0.000165,-0.002198,0.00081,-5.28E-05];
Cpscav=[0.00022,-0.002856,0.00222,-6.79E-05];
CTegTCin=[-0.000169,0.002466,0.00059,6.13E-05];
CTegTCout=[-2.8E-05,0.00316,0.00018,9.74E-05];
CSFOC=[-5.3E-05,0.000707,0.000413,1.68E-05];
pambairiso=1000;
TairTCiniso=25;
SCOCminMANelME=0.6;
