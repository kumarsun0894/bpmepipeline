NameVessel="Marvel Hawk";
NoonVerModel="Y";
HPVerModel="Y";
MEVerModel="Y";
AEVerModel=[NaN];
AutoVerModel=[NaN];
HPLogConfig="Y";
MELogConfig="Y";
AELogConfig="Y";
NoonLogConfig="Y";
ExNames=[NaN];
NotesModel="Tb was changed from 9.6m to 9.1m based on realistic data from the vessel's operational profile.";
lastprocessedNoon=[NaN];
lastprocessedHP=[NaN];
lastprocessedME=[NaN];
lastprocessedAE=[NaN];
HullNoVessel="SN2149";
IMONoVessel=9760770;
BuildYardVessel="SAMSUNG HEAVY INDUSTRIES CO.,LTD";
YearBuildVessel="2018";
UltimateOwnerVessel="Bernhard Schulte GmbH & Co. KG";
TypeVessel="GAST";
TypeSTAWINDVessel="GC";
SuperStructureSTAWINDVessel="PED";
Loa=293.30;
Lbp=285.00;
B=45.80;
D=26.20;
Td=11.35;
Tb=9.10;
Ts=12.50;
Ud=19.67;
CapacityCargoVessel=174267;
DWT=89432;
nCHVessel=4;
TypeCGVessel="PP";
nCGVessel=[NaN];
CapacityCGVessel=[NaN];
Ds=129297;
Vs=125294;
Dd=115986;
Vd=112323;
Ad=1668;
Zad=51.80;
BulbousBowVessel="Yes";
BowSection=[0.00,0.00;3.26,11.22];
Cstern="N";
TransomSternVessel="Yes";
HullProfile=[-5.10,-5.10,6.99,8.20,8.62,8.25,7.37,6.20,4.58,4.56,5.77,9.94,13.65,18.38,23.65,27.17,273.65,279.95,283.90,286.45,288.20,288.20,287.07,285.55,285.00,285.00,286.83;20.61,12.72,9.58,8.79,7.70,6.54,5.80,5.36,5.41,4.05,3.90,2.28,1.42,0.60,0.18,0.05,0.03,0.95,2.44,4.52,7.22,9.18,10.23,10.65,10.97,16.61,24.52];
RudderProfile=[-3.90,2.46,1.18,-3.21,-3.90;11.84,10.27,0.49,0.49,11.84];
TransomSection=[0.00,14.29;12.11,19.82];
nISBRG=2;
etaS=0.990;
etaB=1.000;
AutoLoggingVessel="No";
vesselsensd=[NaN];
vesselwp="Meteo";
vesselpp=[NaN];
DrawingURLVessel="https://bsmcloud.sharepoint.com/sites/FPC-ShipDrawings/Shared%20Documents/Forms/AllItems.aspx?RootFolder=%2Fsites%2FFPC%2DShipDrawings%2FShared%20Documents%2FVessel%20Drawings%2FBSM%20%28UK%29%2F9760770%20%2D%20Marvel%20Hawk&FolderCTID=0x012000444889A43C820240A52D85B51C50F42D&View=%7BA7082555%2D3FF2%2D4418%2DA4AA%2D81FA86A8EF19%7D";
rhoswh=1025.0;
Th=[2.98,3.98,4.98,5.98,6.98,7.98,8.98,9.98,10.98,11.98,12.98];
Dh=[26611.7,36572.3,46770.2,57142.7,67692.8,78415.7,89315.3,100407.6,111698.5,123193.9,134863.1];
WSAh=[10459.10,11260.30,11991.80,12719.90,13429.80,14124.00,14837.30,15566.60,16277.50,16988.40,17625.30];
Cmh=[0.982,0.986,0.989,0.991,0.992,0.993,0.994,0.995,0.995,0.995,0.996];
Cwph=[0.732,0.753,0.768,0.781,0.795,0.806,0.821,0.835,0.851,0.866,0.876];
LCBh=[141.63,141.10,140.69,140.35,140.00,139.57,139.02,138.39,137.66,136.87,136.17];
TaMT=[9.60,11.35,12.00];
TfMT=[9.60,11.35,12.00];
VMT=[93645.10,116032.40,123533.00];
PowerTypeMT="B";
U0MT=[12.00,14.00,16.00,17.00,17.50,18.00,19.00,19.50,20.00,21.00,22.00;12.00,14.00,16.00,17.00,17.50,18.00,19.00,19.50,20.00,21.00,22.00;12.00,14.00,16.00,17.00,17.50,18.00,19.00,19.50,20.00,21.00,NaN];
P0MT=[4433,6811,9858,11642,12632,13685,16008,17359,18913,22630,27517;4268,6658,9811,11710,12754,13892,16501,18043,19853,23991,29106;4282,6716,9899,11897,13034,14274,17137,18738,20551,24856,NaN];
N0MT=[51.97,60.43,68.82,72.99,75.12,77.27,81.58,83.80,86.16,91.19,96.71;52.17,60.72,69.31,73.59,75.75,77.96,82.49,84.88,87.40,92.55,98.23;52.55,61.15,69.73,74.12,76.35,78.66,83.34,85.76,88.23,93.42,NaN];
etaD0MT=[0.777,0.785,0.792,0.797,0.798,0.799,0.802,0.802,0.803,0.801,0.796;0.791,0.793,0.794,0.795,0.796,0.796,0.796,0.796,0.794,0.789,0.780;0.787,0.788,0.790,0.790,0.790,0.790,0.790,0.790,0.789,0.784,NaN];
U15MT=[NaN];
P15MT=[NaN];
N15MT=[NaN];
etaD15MT=[NaN];
TrialIndexMT=1;
UtrialMT=[NaN];
PtrialMT=[NaN];
NtrialMT=[NaN];
nProp=2;
TypeProp="FPP";
dProp=7.300;
pitchProp=7.088;
zProp=5;
AeAoProp=0.540;
HullNoSTrial="SN2149";
PowerTypeSTrial="B";
TaConSTrial=9.60;
TfConSTrial=9.60;
VConSTrial=93645.10;
TaSTrial=9.66;
TfSTrial=9.57;
VSTrial=94007.83;
UCorSTrial=[19.09,19.77,20.49];
PCorSTrial=[15700,18525,22166];
NCorSTrial=[82.50,86.30,91.30];
WeatherCorSTrial="Yes";
TEUSTrial=[NaN];
WSASTrial=[NaN];
TswSTrial=[NaN];
rhoswSTrial=[NaN];
TairSTrial=[NaN];
pairSTrial=[NaN];
azbowSTrial=[NaN];
SOGSTrial=[NaN];
PSTrial=[NaN];
NPropSTrial=[NaN];
HeadSTrial=[NaN];
UwirSTrial=[NaN];
psiwirSTrial=[NaN];
HwvSTrial=[NaN];
TwvSTrial=[NaN];
psiwvrSTrial=[NaN];
HslSTrial=[NaN];
TslSTrial=[NaN];
psislrSTrial=[NaN];
hswSTrial=[NaN];
nME=2;
MakerME=["Wartsila","Wartsila"];
BuilderME=["Doosan","Doosan"];
ModelME=["W6X62DF","W6X62DF"];
SerNoME=["DWL0105329","DWL0105330"];
CategoryME=["2","2"];
TypeME=["WX","WX"];
PistConfME=["INLINE","INLINE"];
ModeME=["TIER III";"TIER III"];
etaeffME=[0.990,0.990];
ncME=[6,6];
dPISTME=[0.620,0.620];
sPISTME=[2.658,2.658];
PMCRME=[12540.0,12540.0];
PNCRME=[11286,11286];
NMCRME=[90.30,90.30];
NNCRME=[87.20,87.20];
DualFuelME=["Yes","Yes"];
VEGBPME=["Yes","Yes"];
LIWATME=["Yes","Yes"];
IARME=["Yes","Yes"];
MBTME=["Yes","Yes"];
CBTME=["No","No"];
AVME=["No","Yes"];
TVME=["No","Yes"];
TCCOSME=["No","No"];
nTCME=[1,1];
MakerTCME=["ABB";"ABB"];
ModelTCME=["A175-L35";"A175-L35"];
NmaxTCME=[15780;15780];
TmaxTCME=[550;550];
muTCME=[NaN;NaN];
dTCcompME=[NaN;NaN];
TypeGovME=[NaN,NaN];
ModelGovME=[NaN,NaN];
TypeLBME=["E","E"];
MakerLBME=["Wartsila","Wartsila"];
ModelLBME=["Other","Other"];
PulseLBME=["Yes","Yes"];
HighPISTME=[NaN,NaN];
RegLBME=[NaN,NaN];
BNLBME=[NaN,NaN];
ACCLBME=[NaN,NaN];
SCOCminME=[0.50,NaN];
SCOCmaxME=[3.00,NaN];
SCOCbME=[0.60,0.60];
SCOCidME=[0.85,0.85];
CoolantJKTME=["FW","FW"];
CoolantFOVVME=["FW","FW"];
CoolantLOCoolerME=["FW","FW"];
CoolantACME=["FW","FW"];
CoolantTCME=["LO","LO"];
nSPME=[NaN,NaN];
nACTRME=[NaN,NaN];
MakerDiagToolME=["WOIS monitor","WOIS monitor"];
ModelDiagToolME=["MS 2440","MS 2440"];
TypeDiagToolME=[NaN,NaN];
InstallationDiagToolME=["Permanent","Permanent"];
LibcodeME=["601.01","601.02"];
ModeMESim=[NaN];
MCRMESim=[NaN];
NMESim=[NaN];
PMESim=[NaN];
SFOCisoMESim=[NaN];
TegEVoutMESim=[NaN];
mdotegMESim=[NaN];
mdotSteamProductionMESim=[NaN];
nMEST=4;
HullNoMEST=["SN2149","SN2149","SN2149","SN2149"];
SerNoMEST=["DWL0105329","DWL0105329","DWL0105329","DWL0105329"];
NoME=[1,1,2,2];
FuelModeMEST=["F","G","F","G"];
PowerTypeMEST=["E","E","E","E"];
ModeMEST=["2","3","2","3"];
LCVFOMEST=[41.6,41.6,41.6,41.6];
rhoFOMEST=[936.7,936.7,936.7,936.7];
LCVGOMEST=[NaN,49.5,NaN,49.5];
rhoGOMEST=[NaN,NaN,NaN,NaN];
MCRMEST=[25.0,50.0,75.0,100.0;25.0,50.0,75.0,100.0;25.0,50.0,75.0,100.0;25.0,50.0,75.0,100.0];
NMEST=[56.90,71.70,82.00,90.30;56.90,71.70,82.00,90.30;56.90,71.70,82.00,90.30;56.90,71.70,82.00,90.30];
PMEST=[3135,6270,9405,12540;3135,6270,9405,12540;3135,6270,9405,12540;3135,6270,9405,12540];
NTCMEST=[8249,12067,14120,15198;6900,10876,12814,14532;8249,12067,14120,15198;6900,10876,12814,14532];
FPIMEST=[34.3,59.4,85.0,100.8;48.2,55.7,52.7,48.5;34.3,59.4,85.0,100.8;48.2,55.7,52.7,48.5];
TFOPPinMEST=[41,42,42,42;40,41,42,44;41,42,42,42;40,41,42,44];
TGOinMEST=[NaN;NaN;NaN;NaN];
pGOinMEST=[NaN,NaN,NaN,NaN;4.2,5.7,7.7,10.5;NaN,NaN,NaN,NaN;4.2,5.7,7.7,10.5];
FOCMEST=[620.7,1213.9,1842.4,2430.3;6.1,7.6,7.6,7.5;620.7,1213.9,1842.4,2430.3;6.1,7.6,7.6,7.5];
SFOCMEST=[198.00,193.60,195.90,193.80;2.30,1.44,0.96,0.71;198.00,193.60,195.90,193.80;2.30,1.44,0.96,0.71];
SFOCisoMEST=[190.70,186.80,188.70,186.10;NaN,NaN,NaN,NaN;190.70,186.80,188.70,186.10;NaN,NaN,NaN,NaN];
GOCMEST=[NaN,NaN,NaN,NaN;492.7,967.5,1394.1,1821.3;NaN,NaN,NaN,NaN;492.7,967.5,1394.1,1821.3];
SGOCMEST=[NaN,NaN,NaN,NaN;157.2,154.3,148.2,145.2;NaN,NaN,NaN,NaN;157.2,154.3,148.2,145.2];
SGOCisoMEST=[NaN;NaN;NaN;NaN];
TairTCinMEST=[32,32,32,33;31,32,33,34;32,32,32,33;31,32,33,34];
pambairMEST=[1001,1001,1001,1001;1006,1006,1006,1006;1001,1001,1001,1001;1006,1006,1006,1006];
TscavMEST=[38,36,40,45;37,35,37,44;38,36,40,45;37,35,37,44];
pscavMEST=[0.82,2.12,3.33,3.97;0.55,1.59,2.56,3.58;0.82,2.12,3.33,3.97;0.55,1.59,2.56,3.58];
pindMEST=[7.9,11.9,15.3,18.3;7.9,11.9,15.3,18.3;7.9,11.9,15.3,18.3;7.9,11.9,15.3,18.3];
peffMEST=[6.9,10.9,14.3,17.3;6.9,10.9,14.3,17.3;6.9,10.9,14.3,17.3;6.9,10.9,14.3,17.3];
pmaxMEST=[58.6,75.7,95.7,121.5;81.4,123.2,173.1,190.1;58.6,75.7,95.7,121.5;81.4,123.2,173.1,190.1];
pcompMEST=[42.4,66.1,89.8,101.1;38.1,60.4,80.3,86.6;42.4,66.1,89.8,101.1;38.1,60.4,80.3,86.6];
TegEVoutMEST=[244,323,392,458;253,297,347,403;244,323,392,458;253,297,347,403];
pegRMEST=[0.80,2.13,3.35,4.00;0.50,1.60,2.54,3.59;0.80,2.13,3.35,4.00;0.50,1.60,2.54,3.59];
TegTCinMEST=[280,371,437,498;296,334,379,443;280,371,437,498;296,334,379,443];
pegTCinMEST=[0.76,2.03,3.19,3.81;0.48,1.52,2.42,3.42;0.76,2.03,3.19,3.81;0.48,1.52,2.42,3.42];
TegTCoutMEST=[200,220,240,285;240,215,220,245;200,220,240,285;240,215,220,245];
pegTCoutMEST=[5.0,15.0,100.0,300.0;30.0,110.0,200.0,300.0;5.0,15.0,100.0,300.0;30.0,110.0,200.0,300.0];
TcwACinMEST=[29,30,31,33;28,30,32,35;29,30,31,33;28,30,32,35];
TcwACoutMEST=[29,31,34,37;28,31,34,39;29,31,34,37;28,31,34,39];
TairACinMEST=[90,160,215,250;76,137,182,233;90,160,215,250;76,137,182,233];
TairACoutMEST=[34,35,40,44;32,33,37,44;34,35,40,44;32,33,37,44];
dpairACMEST=[85.0,105.0,125.0,130.0;65.0,105.0,120.0,135.0;85.0,105.0,125.0,130.0;65.0,105.0,120.0,135.0];
dpairAFMEST=[10.0,40.0,71.0,85.0;10.0,30.0,50.0,77.0;10.0,40.0,71.0,85.0;10.0,30.0,50.0,77.0];
AuxBlowMEST=[1,0,0,0;1,0,0,0;1,0,0,0;1,0,0,0];
MCRNOxMEST=[25.0,50.0,75.0,100.0;25.0,50.0,75.0,100.0;25.0,50.0,75.0,100.0;25.0,50.0,75.0,100.0];
NOxisoMEST=[19.800,12.900,12.200,14.300;0.460,0.100,0.150,2.780;19.800,12.900,12.200,14.300;0.460,0.100,0.150,2.780];
nAE=4;
MakerAE=["Wartsila","Wartsila","Wartsila","Wartsila"];
BuilderAE=["Wartsila","Wartsila","Wartsila","Wartsila"];
PistConfAE=["INLINE","INLINE","INLINE","INLINE"];
ModelAE=["W6L34DF","W6L34DF","W6L34DF","W6L34DF"];
SerNoAE=["PAAE298819","PAAE298820","PAAE298821","PAAE298822"];
ncAE=[6,6,6,6];
dPISTAE=[0.340,0.340,0.340,0.340];
sPISTAE=[0.400,0.400,0.400,0.400];
PengnomAE=[2880.0,2880.0,2880.0,2880.0];
PgennomAAE=[3437.5,3437.5,3437.5,3437.5];
NnomAE=[720,720,720,720];
PFnomAE=[0.800,0.800,0.800,0.800];
DualFuelAE=["Yes","Yes","Yes","Yes"];
ECNTRAE=["No","No","No","No"];
nTCAE=[1,1,1,1];
MakerTCAE=[NaN;NaN;NaN;NaN];
ModelTCAE=[NaN;NaN;NaN;NaN];
nTCinletAE=[NaN,NaN,NaN,NaN];
NmaxTCAE=[NaN;NaN;NaN;NaN];
TmaxTCAE=[NaN;NaN;NaN;NaN];
muTCAE=[NaN;NaN;NaN;NaN];
dTCcompAE=[NaN;NaN;NaN;NaN];
CoolantJKTAE=[NaN,NaN,NaN,NaN];
CoolantFOVVAE=[NaN,NaN,NaN,NaN];
CoolantLOCoolerAE=[NaN,NaN,NaN,NaN];
CoolantACAE=[NaN,NaN,NaN,NaN];
CoolantTCAE=[NaN,NaN,NaN,NaN];
MakerDiagToolAE=["WOIS monitor","WOIS monitor","WOIS monitor","WOIS monitor"];
ModelDiagToolAE=["MS 2440","MS 2440","MS 2440","MS 2440"];
TypeDiagToolAE=[NaN,NaN,NaN,NaN];
InstallationDiagToolAE=[NaN,NaN,NaN,NaN];
LibcodeAE=["651.01","651.02","651.03","651.04"];
nAEST=4;
HullNoAEST=["SN2149",NaN,NaN,NaN];
SerNoAEST=["PAAE298819",NaN,NaN,NaN];
NoAE=[1,2,3,4];
FuelModeAEST=["F","F","F","F"];
LCVFOAEST=[NaN,NaN,NaN,NaN];
rhoFOAEST=[NaN,NaN,NaN,NaN];
LCVGOAEST=[NaN,NaN,NaN,NaN];
rhoGOAEST=[NaN,NaN,NaN,NaN];
LoadTypeAEST=[NaN,NaN,NaN,NaN];
MCRAEST=[25.0,50.0,75.0,100.0;NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN];
etagenAEST=[NaN;NaN;NaN;NaN];
NAEST=[720.00,720.00,720.00,720.00;NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN];
PengAEST=[NaN;NaN;NaN;NaN];
PgenAEST=[NaN;NaN;NaN;NaN];
NTCAEST=[NaN;NaN;NaN;NaN];
FPIAEST=[NaN;NaN;NaN;NaN];
TFOPPinAEST=[34,33,32,29;NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN];
TGOinAEST=[NaN;NaN;NaN;NaN];
pGOinAEST=[NaN;NaN;NaN;NaN];
FOCAEST=[175.0,278.0,367.0,461.0;NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN];
SFOCAEST=[241.110,192.130,170.600,160.010;NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN];
SFOCisoAEST=[NaN;NaN;NaN;NaN];
GOCAEST=[NaN;NaN;NaN;NaN];
SGOCAEST=[NaN;NaN;NaN;NaN];
SGOCisoAEST=[NaN;NaN;NaN;NaN];
TairTCinAEST=[NaN;NaN;NaN;NaN];
pambairAEST=[NaN;NaN;NaN;NaN];
pmaxAEST=[NaN;NaN;NaN;NaN];
pcompAEST=[NaN;NaN;NaN;NaN];
TegEVoutAEST=[375,377,392,371;NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN];
TegTCinAEST=[NaN;NaN;NaN;NaN];
TegTCoutAEST=[NaN;NaN;NaN;NaN];
TcwACinAEST=[NaN;NaN;NaN;NaN];
TcwACoutAEST=[NaN;NaN;NaN;NaN];
TairACinAEST=[NaN;NaN;NaN;NaN];
TscavAEST=[NaN;NaN;NaN;NaN];
pscavAEST=[NaN;NaN;NaN;NaN];
dpairACAEST=[NaN;NaN;NaN;NaN];
dpairAFAEST=[NaN;NaN;NaN;NaN];
MCRNOxAEST=[NaN;NaN;NaN;NaN];
NOxisoAEST=[NaN;NaN;NaN;NaN];
nBL=2;
FluidBL=["S","S"];
MakerBL=[NaN,NaN];
ModelBL=[NaN,NaN];
DualFuelBL=["No","No"];
SteamCapacityBL=[NaN,NaN];
nFM=6;
FluidFM=["FO","GAS","FO","GAS","FO","GAS"];
EquipmentFM_new=["ME1","ME1","ME2","ME2","AE1","AE1"];
EquipmentFM=["ME1","ME1","ME2","ME2","AE1","AE1"];
LocationFM=["In","In","In","In","In","In"];
TypeFM=["V","M","V","M","V","M"];
MakerFM=[NaN,NaN,NaN,NaN,NaN,NaN];
ModelFM=[NaN,NaN,NaN,NaN,NaN,NaN];
FaccuracyFM=[NaN,NaN,NaN,NaN,NaN,NaN];
InterfaceFM=[NaN,NaN,NaN,NaN,NaN,NaN];
MakerSL="FURUNO";
ModelSL="DS-60";
UaccuracySL=[NaN];
SPM="Yes";
nSPM=1;
ECNTRSPM=["Yes"];
LocationSPM=[NaN];
MakerSPM=["SPECS VISION"];
ModelSPM=["TPM - II"];
QaccuracySPM=[NaN];
NaccuracySPM=[NaN];
InterfaceSPM=[NaN];
SThruster="No";
nSThruster=[NaN];
LocationSThruster=[NaN];
dSThruster=[NaN];
pitchSThruster=[NaN];
dtunSThruster=[NaN];
VnomSThruster=[NaN];
AnomSThruster=[NaN];
PnomSThruster=[NaN];
NnomSThruster=[NaN];
GCU="Yes";
nGCU=1;
MakerGCU=["Alfalaval"];
ModelGCU=[NaN];
CapacityGCU=[NaN];
SG="No";
nSG=[NaN];
ECNTRSG=[NaN];
MakerSG=[NaN];
ModelSG=[NaN];
PnomSG=[NaN];
NnomSG=[NaN];
etaSG=[NaN];
RG="No";
nRG=[NaN];
MakerRG=[NaN];
ModelRG=[NaN];
RatioRG=[NaN];
etaRG=[NaN];
STurbine="No";
nSTurbine=[NaN];
MakerSTurbine=[NaN];
ModelSTurbine=[NaN];
PnomSTurbine=[NaN];
NnomSTurbine=[NaN];
DualFuelSTurbine=[NaN];
GTurbine="No";
nGTurbine=[NaN];
MakerGTurbine=[NaN];
ModelGTurbine=[NaN];
PnomGTurbine=[NaN];
NnomGTurbine=[NaN];
DualFuelGTurbine=[NaN];
PMotor="No";
nPMotor=[NaN];
ECNTRPMotor=[NaN];
MakerPMotor=[NaN];
ModelPMotor=[NaN];
TypePMotor=[NaN];
PnomPMotor=[NaN];
NnomPMotor=[NaN];
IGG="Yes";
nIGG=[NaN];
MakerIGG=[NaN];
ModelIGG=[NaN];
CapacityIGG=[NaN];
INC="Yes";
nINC=1;
MakerINC=["HMMCO"];
ModelINC=["MAXI T 150SL WS"];
EG="Yes";
nEG=[NaN];
TmCP=[9.30,11.50];
FwiCP=5.00;
SSNCP=4.00;
UCP=[12.00,12.50,13.00,13.50,14.00,14.50,15.00,15.50,16.00,16.50,17.00,17.50,18.00,18.50,19.00,19.50;12.00,12.50,13.00,13.50,14.00,14.50,15.00,15.50,16.00,16.50,17.00,17.50,18.00,18.50,19.00,19.50];
FOCMECP=[43.00,46.00,47.80,51.80,55.80,59.90,64.00,68.10,72.30,77.00,81.70,87.20,92.60,99.10,102.80,110.60;42.00,45.00,46.90,50.60,54.40,58.60,62.80,67.30,71.90,77.30,82.80,89.30,95.90,104.10,109.40,119.90];
LCVFOMECP=42.70;
g=9.8067;
Zaid=10;
kyy=0.25;
ksiP=0;
ksiN=0.2;
ksiU=-0.33;
LCVMDOref=42.7;
LCVHFOref=40.6;
Ck2=1.309;
TCNomogram=[-0.00126,1.063;-0.000964,1.939];
CorAllowance=50;
MCRidAE=80;
RHmarginAE=1;
F1iso=[-0.002446,0.002856,0.002954,0.002198];
F2iso=[-0.00059,-0.00222,-0.00153,-0.00081];
Kiso=[273,1,1,1];
Cpcomp=[0.000226,-0.002954,0.00153,-7.02E-05];
Cpmax=[0.000165,-0.002198,0.00081,-5.28E-05];
Cpscav=[0.00022,-0.002856,0.00222,-6.79E-05];
CTegTCin=[-0.000169,0.002466,0.00059,6.13E-05];
CTegTCout=[-2.8E-05,0.00316,0.00018,9.74E-05];
CSFOC=[-5.3E-05,0.000707,0.000413,1.68E-05];
pambairiso=1000;
TairTCiniso=25;
SCOCminMANelME=0.6;
