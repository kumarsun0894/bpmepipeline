NameVessel="RDO Fortune";
NoonVerModel="Y";
HPVerModel="Y";
MEVerModel="Y";
AEVerModel="Y";
AutoVerModel=[NaN];
HPLogConfig="Y";
MELogConfig="Y";
AELogConfig="Y";
NoonLogConfig="Y";
ExNames=[NaN];
NotesModel=[NaN];
lastprocessedNoon=[NaN];
lastprocessedHP=[NaN];
lastprocessedME=[NaN];
lastprocessedAE=[NaN];
HullNoVessel="S590";
IMONoVessel=9623673;
BuildYardVessel="HYUNDAI SAMHO HEAVY INDUSTRIES CO., LTD";
YearBuildVessel=[NaN];
UltimateOwnerVessel="D. Oltmann Reederei GmbH & Co. KG";
TypeVessel="CONT";
TypeSTAWINDVessel="CC";
SuperStructureSTAWINDVessel="LB";
Loa=256.06;
Lbp=242.00;
B=37.40;
D=22.10;
Td=12.00;
Tb=7.06;
Ts=13.50;
Ud=21.92;
CapacityCargoVessel=5000;
DWT=51423;
nCHVessel=7;
TypeCGVessel=[NaN];
nCGVessel=[NaN];
CapacityCGVessel=[NaN];
Ds=81831;
Vs=79546;
Dd=70266;
Vd=68288;
Ad=1245;
Zad=43.45;
BulbousBowVessel="Yes";
BowSection=[NaN];
Cstern="N";
TransomSternVessel=[NaN];
HullProfile=[NaN];
RudderProfile=[NaN];
TransomSection=[NaN];
nISBRG=1;
etaS=0.990;
etaB=1.000;
AutoLoggingVessel="No";
vesselsensd=[NaN];
vesselwp="Meteo";
vesselpp=[NaN];
DrawingURLVessel="https://bsmcloud.sharepoint.com/:f:/s/FPC-ShipDrawings/ElWOPrzn2kZJm7wpPNF0DycBnnZGrz_jVSXrkkkMAEpJ5w?e=jpC8Ji";
rhoswh=1025.0;
Th=[3.00,4.70,6.85,9.00,11.00,12.00,13.00,14.00];
Dh=[13005.0,22214.0,34931.0,48875.0,62917.0,70266.0,77895.0,85821.0];
WSAh=[5527.30,6523.90,7782.80,9122.20,10394.80,11113.50,11829.60,12437.60];
Cmh=[0.924,0.950,0.966,0.974,0.979,0.981,0.982,0.983];
Cwph=[NaN];
LCBh=[NaN];
TaMT=[9.24,12.00,13.50];
TfMT=[4.88,12.00,13.50];
VMT=[35361.70,68394.70,79657.60];
PowerTypeMT="D";
U0MT=[12.50,13.50,14.50,15.50,16.50,17.50,18.50,19.50,20.50,21.50,22.50,23.50,24.50;12.00,13.00,14.00,15.00,16.00,17.00,18.00,19.00,20.00,21.00,22.00,23.00,24.00;11.50,12.50,13.50,14.50,15.50,16.50,17.50,18.50,19.50,20.50,21.50,22.50,23.50];
P0MT=[3332,4234,5264,6393,7679,9186,10935,12897,15164,17871,20941,24482,28505;3743,4697,5835,7196,8695,10233,11866,13760,15972,18650,22384,26622,31236;3553,4561,5737,7084,8585,10232,12140,14478,17195,20308,24613,29901,36191];
N0MT=[41.89,45.41,48.90,52.24,55.59,58.90,62.28,65.80,69.36,73.00,76.90,81.09,85.37;42.52,45.97,49.51,53.11,56.56,59.86,63.08,66.37,69.79,73.44,77.67,81.94,86.17;42.01,45.73,49.44,53.13,56.72,60.22,63.78,67.61,71.53,75.47,80.04,84.90,89.95];
etaD0MT=[0.758,0.764,0.770,0.773,0.776,0.776,0.776,0.780,0.782,0.783,0.784,0.787,0.788;0.741,0.754,0.763,0.765,0.763,0.760,0.758,0.760,0.761,0.764,0.755,0.754,0.763;0.746,0.740,0.737,0.735,0.736,0.743,0.749,0.746,0.743,0.744,0.732,0.732,0.721];
U15MT=[NaN];
P15MT=[NaN];
N15MT=[NaN];
etaD15MT=[NaN];
TrialIndexMT=1;
UtrialMT=[NaN];
PtrialMT=[NaN];
NtrialMT=[NaN];
nProp=1;
TypeProp="FPP";
dProp=8.800;
pitchProp=9.579;
zProp=[NaN];
AeAoProp=[NaN];
HullNoSTrial="S589";
PowerTypeSTrial="S";
TaConSTrial=9.24;
TfConSTrial=4.88;
VConSTrial=35361.70;
TaSTrial=8.67;
TfSTrial=5.57;
VSTrial=35560.00;
UCorSTrial=[14.50,15.58,16.26,16.99,18.07,19.71,21.12,21.99,22.28,22.75,23.65,23.89];
PCorSTrial=[5243,6479,7908,9271,10680,13916,16883,20140,21732,22936,25741,26513];
NCorSTrial=[48.51,52.21,55.49,58.43,61.20,65.79,69.97,73.60,75.30,76.86,79.67,80.97];
WeatherCorSTrial="Yes";
TEUSTrial=[NaN];
WSASTrial=[NaN];
TswSTrial=[NaN];
rhoswSTrial=[NaN];
TairSTrial=[NaN];
pairSTrial=[NaN];
azbowSTrial=[NaN];
SOGSTrial=[NaN];
PSTrial=[NaN];
NPropSTrial=[NaN];
HeadSTrial=[NaN];
UwirSTrial=[NaN];
psiwirSTrial=[NaN];
HwvSTrial=[NaN];
TwvSTrial=[NaN];
psiwvrSTrial=[NaN];
HslSTrial=[NaN];
TslSTrial=[NaN];
psislrSTrial=[NaN];
hswSTrial=[NaN];
nME=1;
MakerME=["MAN"];
BuilderME=[NaN];
ModelME=["6S80ME-C9"];
SerNoME=["AA4993"];
CategoryME=["2"];
TypeME=["ME"];
PistConfME=["INLINE"];
ModeME=[NaN];
etaeffME=[0.990];
ncME=[6];
dPISTME=[0.800];
sPISTME=[3.450];
PMCRME=[27060.0];
PNCRME=[24354];
NMCRME=[78.00];
NNCRME=[NaN];
DualFuelME=["No"];
VEGBPME=["Yes"];
LIWATME=["No"];
IARME=["No"];
MBTME=["No"];
CBTME=["No"];
AVME=["No"];
TVME=["No"];
TCCOSME=["No"];
nTCME=[2];
MakerTCME=[NaN];
ModelTCME=[NaN];
NmaxTCME=[NaN];
TmaxTCME=[NaN];
muTCME=[NaN];
dTCcompME=[NaN];
TypeGovME=[NaN];
ModelGovME=[NaN];
TypeLBME=["E"];
MakerLBME=[NaN];
ModelLBME=["ALPHA"];
PulseLBME=["NA"];
HighPISTME=["NA"];
RegLBME=[NaN];
BNLBME=[70];
ACCLBME=[NaN];
SCOCminME=[0.60];
SCOCmaxME=[2.21];
SCOCbME=[NaN];
SCOCidME=[1.00];
CoolantJKTME=[NaN];
CoolantFOVVME=[NaN];
CoolantLOCoolerME=[NaN];
CoolantACME=[NaN];
CoolantTCME=[NaN];
nSPME=[NaN];
nACTRME=[NaN];
MakerDiagToolME=[NaN];
ModelDiagToolME=[NaN];
TypeDiagToolME=[NaN];
InstallationDiagToolME=[NaN];
LibcodeME=["601.01"];
ModeMESim=[NaN];
MCRMESim=[NaN];
NMESim=[NaN];
PMESim=[NaN];
SFOCisoMESim=[NaN];
TegEVoutMESim=[NaN];
mdotegMESim=[NaN];
mdotSteamProductionMESim=[NaN];
nMEST=1;
HullNoMEST=["S590"];
SerNoMEST=["AA4994"];
NoME=[1];
FuelModeMEST=["F"];
PowerTypeMEST=["E"];
ModeMEST=["2"];
LCVFOMEST=[41.8];
rhoFOMEST=[915.3];
LCVGOMEST=[NaN];
rhoGOMEST=[NaN];
MCRMEST=[25.0,50.0,75.0,90.0,100.0,110.0];
NMEST=[49.10,61.90,70.90,75.30,78.00,80.50];
PMEST=[6765,13530,20295,24354,27060,29766];
NTCMEST=[7207,11465,14199,14525,15296,16133];
FPIMEST=[43.0,64.0,84.0,94.0,100.0,107.0];
TFOPPinMEST=[39,40,40,40,40,40];
TGOinMEST=[NaN];
pGOinMEST=[NaN];
FOCMEST=[1255.0,2350.0,3515.0,4335.0,4880.0,5445.0];
SFOCMEST=[185.65,173.71,173.12,178.02,180.34,182.97];
SFOCisoMEST=[181.39,169.09,168.58,173.41,175.85,178.53];
GOCMEST=[NaN];
SGOCMEST=[NaN];
SGOCisoMEST=[NaN];
TairTCinMEST=[27,27,28,28,28,28];
pambairMEST=[1012,1012,1013,1013,1013,1012];
TscavMEST=[36,36,39,41,42,42];
pscavMEST=[0.62,1.59,2.72,2.89,3.82,3.75];
pindMEST=[8.9,13.6,17.5,19.7,21.0,22.3];
peffMEST=[7.9,12.6,16.5,18.7,20.0,21.3];
pmaxMEST=[100.0,135.0,150.0,170.0,171.0,170.0];
pcompMEST=[60.0,92.0,110.0,130.0,140.0,156.0];
TegEVoutMEST=[231,291,353,392,436,486];
pegRMEST=[0.52,1.43,2.56,2.72,3.13,3.60];
TegTCinMEST=[291,363,420,475,517,575];
pegTCinMEST=[0.40,1.27,2.37,2.49,2.89,3.35];
TegTCoutMEST=[229,232,234,275,300,336];
pegTCoutMEST=[10.0,83.0,168.0,168.0,248.0,205.0];
TcwACinMEST=[21,26,24,26,28,27];
TcwACoutMEST=[23,30,38,42,45,48];
TairACinMEST=[83,91,190,204,220,238];
TairACoutMEST=[23,27,31,35,36,38];
dpairACMEST=[85.0,137.0,168.0,183.0,198.0,210.0];
dpairAFMEST=[0.0,0.0,0.0,0.0,0.0,0.0];
AuxBlowMEST=[1,0,0,0,0,0];
MCRNOxMEST=[NaN];
NOxisoMEST=[NaN];
nAE=4;
MakerAE=["Hyundai Himsen","Hyundai Himsen","Hyundai Himsen","Hyundai Himsen"];
BuilderAE=[NaN,NaN,NaN,NaN];
PistConfAE=["INLINE","INLINE","INLINE","INLINE"];
ModelAE=["H25/33","H25/33","H25/33","H25/33"];
SerNoAE=["BA4670-1","BA4670-1","BA4670-1","BA4670-1"];
ncAE=[9,9,9,9];
dPISTAE=[0.250,0.250,0.250,0.250];
sPISTAE=[0.330,0.330,0.330,0.330];
PengnomAE=[2095.0,2095.0,2095.0,2095.0];
PgennomAAE=[1990.0,1990.0,1990.0,1990.0];
NnomAE=[720,720,720,720];
PFnomAE=[0.750,0.750,0.750,0.750];
DualFuelAE=["No","No","No","No"];
ECNTRAE=["No","No","No","No"];
nTCAE=[1,1,1,1];
MakerTCAE=["KBB";"KBB";"KBB";"KBB"];
ModelTCAE=["ST6";"ST6";"ST6";"ST6"];
nTCinletAE=[NaN,NaN,NaN,NaN];
NmaxTCAE=[35500;35500;35500;35500];
TmaxTCAE=[650;650;650;650];
muTCAE=[NaN;NaN;NaN;NaN];
dTCcompAE=[NaN;NaN;NaN;NaN];
CoolantJKTAE=[NaN,NaN,NaN,NaN];
CoolantFOVVAE=[NaN,NaN,NaN,NaN];
CoolantLOCoolerAE=[NaN,NaN,NaN,NaN];
CoolantACAE=[NaN,NaN,NaN,NaN];
CoolantTCAE=[NaN,NaN,NaN,NaN];
MakerDiagToolAE=[NaN,NaN,NaN,NaN];
ModelDiagToolAE=[NaN,NaN,NaN,NaN];
TypeDiagToolAE=[NaN,NaN,NaN,NaN];
InstallationDiagToolAE=[NaN,NaN,NaN,NaN];
LibcodeAE=["651.01","651.02","651.03","651.04"];
nAEST=4;
HullNoAEST=["S589","S589","S589","S589"];
SerNoAEST=["BA4670-1","BA4670-1","BA4670-1","BA4670-1"];
NoAE=[1,2,3,4];
FuelModeAEST=["F","F","F","F"];
LCVFOAEST=[41.881,41.881,41.881,41.881];
rhoFOAEST=[908.4,908.4,908.4,908.4];
LCVGOAEST=[NaN,NaN,NaN,NaN];
rhoGOAEST=[NaN,NaN,NaN,NaN];
LoadTypeAEST=["E","E","E","E"];
MCRAEST=[25.0,50.0,75.0,100.0;25.0,50.0,75.0,100.0;25.0,50.0,75.0,100.0;25.0,50.0,75.0,100.0];
etagenAEST=[0.928,0.955,0.963,0.966;0.928,0.955,0.963,0.966;0.928,0.955,0.963,0.966;0.928,0.955,0.963,0.966];
NAEST=[720.00,720.00,720.00,720.00;720.00,720.00,720.00,720.00;720.00,720.00,720.00,720.00;720.00,720.00,720.00,720.00];
PengAEST=[523.8,1047.5,1571.3,2095.0;523.8,1047.5,1571.3,2095.0;523.8,1047.5,1571.3,2095.0;523.8,1047.5,1571.3,2095.0];
PgenAEST=[486.1,1000.5,1512.8,2023.6;486.1,1000.5,1512.8,2023.6;486.1,1000.5,1512.8,2023.6;486.1,1000.5,1512.8,2023.6];
NTCAEST=[13800,21600,26700,30300;13800,21600,26700,30300;13800,21600,26700,30300;13800,21600,26700,30300];
FPIAEST=[13.0,18.2,24.1,30.5;13.0,18.2,24.1,30.5;13.0,18.2,24.1,30.5;13.0,18.2,24.1,30.5];
TFOPPinAEST=[31,32,33,34;31,32,33,34;31,32,33,34;31,32,33,34];
TGOinAEST=[NaN;NaN;NaN;NaN];
pGOinAEST=[NaN;NaN;NaN;NaN];
FOCAEST=[NaN,NaN,NaN,402.8;NaN,NaN,NaN,402.8;NaN,NaN,NaN,402.8;NaN,NaN,NaN,402.8];
SFOCAEST=[NaN,NaN,NaN,192.270;NaN,NaN,NaN,192.270;NaN,NaN,NaN,192.270;NaN,NaN,NaN,192.270];
SFOCisoAEST=[NaN,NaN,NaN,187.670;NaN,NaN,NaN,187.670;NaN,NaN,NaN,187.670;NaN,NaN,NaN,187.670];
GOCAEST=[NaN;NaN;NaN;NaN];
SGOCAEST=[NaN;NaN;NaN;NaN];
SGOCisoAEST=[NaN;NaN;NaN;NaN];
TairTCinAEST=[24,24,25,26;24,24,25,26;24,24,25,26;24,24,25,26];
pambairAEST=[1019,1019,1019,1019;1019,1019,1019,1019;1019,1019,1019,1019;1019,1019,1019,1019];
pmaxAEST=[84.0,124.0,161.0,191.0;84.0,124.0,161.0,191.0;84.0,124.0,161.0,191.0;84.0,124.0,161.0,191.0];
pcompAEST=[NaN;NaN;NaN;NaN];
TegEVoutAEST=[330,323,330,354;330,323,330,354;330,323,330,354;330,323,330,354];
TegTCinAEST=[355,420,439,470;355,420,439,470;355,420,439,470;355,420,439,470];
TegTCoutAEST=[292,322,296,290;292,322,296,290;292,322,296,290;292,322,296,290];
TcwACinAEST=[32,32,33,35;32,32,33,35;32,32,33,35;32,32,33,35];
TcwACoutAEST=[33,35,38,44;33,35,38,44;33,35,38,44;33,35,38,44];
TairACinAEST=[NaN;NaN;NaN;NaN];
TscavAEST=[36,37,39,42;36,37,39,42;36,37,39,42;36,37,39,42];
pscavAEST=[0.39,1.18,2.06,3.33;0.39,1.18,2.06,3.33;0.39,1.18,2.06,3.33;0.39,1.18,2.06,3.33];
dpairACAEST=[NaN;NaN;NaN;NaN];
dpairAFAEST=[NaN;NaN;NaN;NaN];
MCRNOxAEST=[10.0,25.0,50.0,75.0,100.0;10.0,25.0,50.0,75.0,100.0;10.0,25.0,50.0,75.0,100.0;10.0,25.0,50.0,75.0,100.0];
NOxisoAEST=[11.850,11.640,7.950,9.190,9.360;11.850,11.640,7.950,9.190,9.360;11.850,11.640,7.950,9.190,9.360;11.850,11.640,7.950,9.190,9.360];
nBL=1;
FluidBL=["S"];
MakerBL=["CONTOIL"];
ModelBL=["VZO 25 FL"];
DualFuelBL=["No"];
SteamCapacityBL=[NaN];
nFM=3;
FluidFM=["FO","FO","FO"];
EquipmentFM_new=["ME1","AE1,AE2,AE3,AE4","BL1"];
EquipmentFM=["ME1","AE1","BL1"];
LocationFM=["In","In","In"];
TypeFM=["M","M","V"];
MakerFM=[NaN,NaN,NaN];
ModelFM=[NaN,NaN,NaN];
FaccuracyFM=[NaN,NaN,NaN];
InterfaceFM=[NaN,NaN,NaN];
MakerSL="JRC";
ModelSL="JLN-550 DOPLER SONAR";
UaccuracySL=[NaN];
SPM="Yes";
nSPM=1;
ECNTRSPM=["Yes"];
LocationSPM=["0","1"];
MakerSPM=["KYMA"];
ModelSPM=["KPM 100"];
QaccuracySPM=[NaN];
NaccuracySPM=[NaN];
InterfaceSPM=[NaN];
SThruster="Yes";
nSThruster=1;
LocationSThruster=["Bow"];
dSThruster=[NaN];
pitchSThruster=[2.280];
dtunSThruster=[NaN];
VnomSThruster=[NaN];
AnomSThruster=[NaN];
PnomSThruster=[NaN];
NnomSThruster=[NaN];
GCU="No";
nGCU=[NaN];
MakerGCU=[NaN];
ModelGCU=[NaN];
CapacityGCU=[NaN];
SG="No";
nSG=[NaN];
ECNTRSG=[NaN];
MakerSG=[NaN];
ModelSG=[NaN];
PnomSG=[NaN];
NnomSG=[NaN];
etaSG=[NaN];
RG="No";
nRG=[NaN];
MakerRG=[NaN];
ModelRG=[NaN];
RatioRG=[NaN];
etaRG=[NaN];
STurbine="No";
nSTurbine=[NaN];
MakerSTurbine=[NaN];
ModelSTurbine=[NaN];
PnomSTurbine=[NaN];
NnomSTurbine=[NaN];
DualFuelSTurbine=[NaN];
GTurbine="No";
nGTurbine=[NaN];
MakerGTurbine=[NaN];
ModelGTurbine=[NaN];
PnomGTurbine=[NaN];
NnomGTurbine=[NaN];
DualFuelGTurbine=[NaN];
PMotor="No";
nPMotor=[NaN];
ECNTRPMotor=[NaN];
MakerPMotor=[NaN];
ModelPMotor=[NaN];
TypePMotor=[NaN];
PnomPMotor=[NaN];
NnomPMotor=[NaN];
IGG="No";
nIGG=[NaN];
MakerIGG=[NaN];
ModelIGG=[NaN];
CapacityIGG=[NaN];
INC="Yes";
nINC=[NaN];
MakerINC=[NaN];
ModelINC=[NaN];
EG="Yes";
nEG=[NaN];
TmCP=[NaN,12.00];
FwiCP=4;
SSNCP=3;
UCP=[NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN;10.00,11.00,12.00,13.00,14.00,15.00,16.00,17.00,18.00,19.00,20.00,21.00];
FOCMECP=[NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN;13.20,16.00,19.00,22.50,26.60,31.50,37.40,44.20,52.40,62.10,72.50,86.00];
LCVFOMECP=40.60;
g=9.8067;
Zaid=10;
kyy=0.25;
ksiP=0;
ksiN=0.2;
ksiU=-0.33;
LCVMDOref=42.7;
LCVHFOref=40.6;
Ck2=1.309;
TCNomogram=[-0.00126,1.063;-0.000964,1.939];
CorAllowance=50;
MCRidAE=80;
RHmarginAE=1;
F1iso=[-0.002446,0.002856,0.002954,0.002198];
F2iso=[-0.00059,-0.00222,-0.00153,-0.00081];
Kiso=[273,1,1,1];
Cpcomp=[0.000226,-0.002954,0.00153,-7.02E-05];
Cpmax=[0.000165,-0.002198,0.00081,-5.28E-05];
Cpscav=[0.00022,-0.002856,0.00222,-6.79E-05];
CTegTCin=[-0.000169,0.002466,0.00059,6.13E-05];
CTegTCout=[-2.8E-05,0.00316,0.00018,9.74E-05];
CSFOC=[-5.3E-05,0.000707,0.000413,1.68E-05];
pambairiso=1000;
TairTCiniso=25;
SCOCminMANelME=0.6;
