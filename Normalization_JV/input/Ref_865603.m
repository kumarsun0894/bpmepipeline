NameVessel="Aristos I";
NoonVerModel="Y";
HPVerModel="Y";
MEVerModel="Y";
AEVerModel=[NaN];
AutoVerModel=[NaN];
HPLogConfig="Y";
MELogConfig="Y";
AELogConfig="Y";
NoonLogConfig="Y";
ExNames=[NaN];
NotesModel=[NaN];
lastprocessedNoon="In progress";
lastprocessedHP="In progress";
lastprocessedME="In progress";
lastprocessedAE=[NaN];
HullNoVessel="3105";
IMONoVessel=9862891;
BuildYardVessel="Hyundai Heavy Industries";
YearBuildVessel="2020";
UltimateOwnerVessel="Capital Gas Shipmanagement Corporation";
TypeVessel="GAST";
TypeSTAWINDVessel="GC";
SuperStructureSTAWINDVessel="PED";
Loa=299.06;
Lbp=291.00;
B=46.40;
D=26.50;
Td=11.50;
Tb=9.25;
Ts=12.50;
Ud=19.50;
CapacityCargoVessel=174000;
DWT=127721;
nCHVessel=4;
TypeCGVessel="PP";
nCGVessel=8;
CapacityCGVessel=[1850.00,1850.00,1850.00,1850.00,1850.00,1850.00,1850.00,1850.00];
Ds=127721;
Vs=124606;
Dd=116250;
Vd=113415;
Ad=1496;
Zad=55.20;
BulbousBowVessel="Yes";
BowSection=[0.00,0.00;4.71,11.98];
Cstern="N";
TransomSternVessel="Yes";
HullProfile=[-3.53,-3.53,6.69,8.15,9.00,9.37,8.46,7.00,5.50,5.50,16.06,21.48,26.83,34.13,268.56,272.73,274.96,280.24,285.53,291.01,293.14,294.97,295.18,293.14,294.57,291.72,290.91,290.81,294.97;21.42,11.32,9.77,9.33,8.54,7.38,5.93,5.20,5.03,3.87,1.52,0.71,0.14,0.00,0.00,0.38,1.34,1.43,2.65,4.59,5.76,7.55,9.31,10.72,10.23,11.11,11.82,15.12,27.78];
RudderProfile=[-3.18,-3.08,0.00,0.72,1.01,1.98,-3.18;9.90,2.25,2.25,2.45,3.00,9.90,9.90];
TransomSection=[0.00,15.85;11.32,21.42];
nISBRG=2;
etaS=0.990;
etaB=1.000;
AutoLoggingVessel="Yes";
vesselsensd=[NaN];
vesselwp="Meteo";
vesselpp=[NaN];
DrawingURLVessel="https://bsmcloud.sharepoint.com/:f:/r/sites/FPC-ShipDrawings/Shared%20Documents/Vessel%20Drawings/BSM%20(Hellas)/9862891%20-%20Aristos%20I?csf=1&web=1&e=GybfKq";
rhoswh=1025.0;
Th=[7.00,7.50,8.00,8.50,9.00,9.50,9.60,10.00,10.50,11.00,11.50,12.00,12.50,13.00,13.50,14.00];
Dh=[66561.0,71903.0,77286.0,82705.0,88157.0,93646.0,94784.0,99173.0,104742.0,110358.0,116023.0,121734.0,127490.0,133292.0,139139.0,145028.0];
WSAh=[13098.90,13449.80,13725.70,14105.70,14456.30,14814.00,14884.50,15160.60,15505.20,15851.30,16198.70,16544.60,16900.90,17225.20,17557.70,17885.10];
Cmh=[0.979,0.980,0.981,0.983,0.984,0.984,0.985,0.985,0.986,0.987,0.987,0.988,0.988,0.989,0.989,0.989];
Cwph=[0.769,0.774,0.779,0.783,0.788,0.794,0.796,0.800,0.806,0.813,0.820,0.827,0.834,0.841,0.847,0.853];
LCBh=[144.82,144.62,144.40,144.15,143.87,143.57,143.51,143.24,142.89,142.52,142.14,141.76,141.37,140.99,140.61,140.25];
TaMT=[9.25,11.50,12.50];
TfMT=[9.25,11.50,12.50];
VMT=[88678.50,113122.90,124294.30];
PowerTypeMT="B";
U0MT=[12.00,13.00,14.00,15.00,16.00,17.00,18.00,19.00,20.00,21.00,22.00;12.00,13.00,14.00,15.00,16.00,17.00,18.00,19.00,20.00,21.00,22.00;12.00,13.00,14.00,15.00,16.00,17.00,18.00,19.00,20.00,21.00,22.00];
P0MT=[4215,5379,6696,8124,9683,11438,13412,15637,18229,21283,24881;4070,5172,6447,7897,9534,11373,13422,15732,18538,21930,25885;4411,5576,6928,8486,10262,12265,14535,17132,20161,23845,28477];
N0MT=[44.75,48.55,52.25,55.80,59.26,62.72,66.22,69.77,73.44,77.31,81.47;44.51,48.20,51.88,55.55,59.22,62.90,66.54,70.17,74.08,78.20,82.31;45.29,49.01,52.73,56.47,60.22,63.97,67.74,71.58,75.51,79.59,84.37];
etaD0MT=[0.810,0.814,0.819,0.826,0.834,0.838,0.842,0.845,0.846,0.848,0.854;0.869,0.869,0.868,0.867,0.867,0.867,0.867,0.866,0.866,0.865,0.862;0.828,0.827,0.826,0.826,0.826,0.826,0.825,0.823,0.819,0.815,0.820];
U15MT=[NaN];
P15MT=[NaN];
N15MT=[NaN];
etaD15MT=[NaN];
TrialIndexMT=1;
UtrialMT=[NaN];
PtrialMT=[NaN];
NtrialMT=[NaN];
nProp=2;
TypeProp="FPP";
dProp=8.400;
pitchProp=7.535;
zProp=3;
AeAoProp=0.403;
HullNoSTrial="3105";
PowerTypeSTrial="S";
TaConSTrial=9.25;
TfConSTrial=9.25;
VConSTrial=88678.00;
TaSTrial=9.23;
TfSTrial=9.29;
VSTrial=89274.10;
UCorSTrial=[19.08,20.10,20.53,21.41];
PCorSTrial=[15933,18533,19847,22908];
NCorSTrial=[72.70,76.68,78.40,81.92];
WeatherCorSTrial="Yes";
TEUSTrial=[NaN];
WSASTrial=[NaN];
TswSTrial=[NaN];
rhoswSTrial=[NaN];
TairSTrial=[NaN];
pairSTrial=[NaN];
azbowSTrial=[NaN];
SOGSTrial=[NaN];
PSTrial=[NaN];
NPropSTrial=[NaN];
HeadSTrial=[NaN];
UwirSTrial=[NaN];
psiwirSTrial=[NaN];
HwvSTrial=[NaN];
TwvSTrial=[NaN];
psiwvrSTrial=[NaN];
HslSTrial=[NaN];
TslSTrial=[NaN];
psislrSTrial=[NaN];
hswSTrial=[NaN];
nME=2;
MakerME=["WinGD","WinGD"];
BuilderME=["Hyundai Heavy Industries Co. Ltd.","Hyundai Heavy Industries Co. Ltd."];
ModelME=["5X72DF","5X72DF"];
SerNoME=["KAA006924","KAA006925"];
CategoryME=["2","2"];
TypeME=["WX","WX"];
PistConfME=["INLINE","INLINE"];
ModeME=[NaN;NaN];
etaeffME=[0.990,0.990];
ncME=[5,5];
dPISTME=[0.720,0.720];
sPISTME=[3.086,3.086];
PMCRME=[12112.0,12112.0];
PNCRME=[10295,10295];
NMCRME=[77.10,77.10];
NNCRME=[73.00,73.00];
DualFuelME=["Yes","Yes"];
VEGBPME=["Yes","Yes"];
LIWATME=["Yes","Yes"];
IARME=["No","No"];
MBTME=["Yes","Yes"];
CBTME=["Yes","Yes"];
AVME=["Yes","Yes"];
TVME=["No","No"];
TCCOSME=["No","No"];
nTCME=[1,1];
MakerTCME=["ABB";"ABB"];
ModelTCME=["A-175L";"A-175L"];
NmaxTCME=[NaN;NaN];
TmaxTCME=[NaN;NaN];
muTCME=[NaN;NaN];
dTCcompME=[NaN;NaN];
TypeGovME=[NaN,NaN];
ModelGovME=[NaN,NaN];
TypeLBME=["E","E"];
MakerLBME=["Win-GD","Win-GD"];
ModelLBME=[NaN,NaN];
PulseLBME=["Yes","Yes"];
HighPISTME=[NaN,NaN];
RegLBME=[NaN,NaN];
BNLBME=[NaN,NaN];
ACCLBME=[NaN,NaN];
SCOCminME=[NaN,NaN];
SCOCmaxME=[NaN,NaN];
SCOCbME=[NaN,NaN];
SCOCidME=[NaN,NaN];
CoolantJKTME=["FW","FW"];
CoolantFOVVME=["LO","LO"];
CoolantLOCoolerME=["FW","FW"];
CoolantACME=["FW","FW"];
CoolantTCME=[NaN,NaN];
nSPME=[NaN,NaN];
nACTRME=[1,1];
MakerDiagToolME=["WinGD","WinGD"];
ModelDiagToolME=[NaN,NaN];
TypeDiagToolME=[NaN,NaN];
InstallationDiagToolME=["Permanent","Permanent"];
LibcodeME=[NaN,NaN];
ModeMESim=[NaN];
MCRMESim=[NaN];
NMESim=[NaN];
PMESim=[NaN];
SFOCisoMESim=[NaN];
TegEVoutMESim=[NaN];
mdotegMESim=[NaN];
mdotSteamProductionMESim=[NaN];
nMEST=4;
HullNoMEST=["3105","3105","3105","3105"];
SerNoMEST=["KAA006924","KAA006924","KAA006924","KAA006924"];
NoME=[1,1,2,2];
FuelModeMEST=["F","G","F","G"];
PowerTypeMEST=["E","E","E","E"];
ModeMEST=["2","3","2","3"];
LCVFOMEST=[42.3,42.3,42.3,42.3];
rhoFOMEST=[876.8,876.8,876.8,876.8];
LCVGOMEST=[NaN,49.5,NaN,49.5];
rhoGOMEST=[NaN,NaN,NaN,NaN];
MCRMEST=[25.0,50.0,75.0,85.0,100.0,100.0;25.0,50.0,75.0,85.0,100.0,100.0;25.0,50.0,75.0,85.0,100.0,100.0;25.0,50.0,75.0,85.0,100.0,100.0];
NMEST=[48.60,61.20,70.00,73.00,77.10,77.10;48.60,61.20,70.00,73.00,77.10,77.10;48.60,61.20,70.00,73.00,77.10,77.10;48.60,61.20,70.00,73.00,77.10,77.10];
PMEST=[3029,6058,9086,10298,12115,12115;3029,6058,9086,10298,12115,12115;3029,6058,9086,10298,12115,12115;3029,6058,9086,10298,12115,12115];
NTCMEST=[8142,11013,12839,13388,14213,14236;6473,10063,11973,12531,13336,13320;8142,11013,12839,13388,14213,14236;6473,10063,11973,12531,13336,13320];
FPIMEST=[39.5,58.6,79.2,86.6,99.8,100.0;38.7,59.4,78.7,85.5,99.3,98.6;39.5,58.6,79.2,86.6,99.8,100.0;38.7,59.4,78.7,85.5,99.3,98.6];
TFOPPinMEST=[40,40,40,41,41,41;41,41,41,41,42,41;40,40,40,41,41,41;41,41,41,41,42,41];
TGOinMEST=[NaN;NaN;NaN;NaN];
pGOinMEST=[NaN;NaN;NaN;NaN];
FOCMEST=[588.6,1137.0,1656.3,1868.0,2288.1,NaN;7.3,9.1,9.1,9.3,8.5,NaN;588.6,1137.0,1656.3,1868.0,2288.1,NaN;7.3,9.1,9.1,9.3,8.5,NaN];
SFOCMEST=[194.37,187.74,182.33,181.45,188.92,NaN;2.40,1.50,1.00,0.90,0.70,NaN;194.37,187.74,182.33,181.45,188.92,NaN;2.40,1.50,1.00,0.90,0.70,NaN];
SFOCisoMEST=[193.08,186.79,181.32,179.91,187.70,NaN;NaN,NaN,NaN,NaN,NaN,NaN;193.08,186.79,181.32,179.91,187.70,NaN;NaN,NaN,NaN,NaN,NaN,NaN];
GOCMEST=[NaN,NaN,NaN,NaN,NaN,NaN;488.3,920.2,1340.2,1500.4,1787.0,NaN;NaN,NaN,NaN,NaN,NaN,NaN;488.3,920.2,1340.2,1500.4,1787.0,NaN];
SGOCMEST=[NaN,NaN,NaN,NaN,NaN,NaN;161.2,151.9,147.5,145.7,147.5,NaN;NaN,NaN,NaN,NaN,NaN,NaN;161.2,151.9,147.5,145.7,147.5,NaN];
SGOCisoMEST=[NaN,NaN,NaN,NaN,NaN,NaN;159.4,150.1,145.8,144.0,145.8,NaN;NaN,NaN,NaN,NaN,NaN,NaN;159.4,150.1,145.8,144.0,145.8,NaN];
TairTCinMEST=[21,21,23,22,23,25;21,20,22,21,22,22;21,21,23,22,23,25;21,20,22,21,22,22];
pambairMEST=[1017,1017,1016,1016,1016,1015;1022,1022,1021,1021,1021,1021;1017,1017,1016,1016,1016,1015;1022,1022,1021,1021,1021,1021];
TscavMEST=[24,21,21,24,22,22;23,19,19,19,19,19;24,21,21,24,22,22;23,19,19,19,19,19];
pscavMEST=[0.88,1.82,2.85,3.18,3.64,3.65;0.54,1.45,2.38,2.78,3.20,3.14;0.88,1.82,2.85,3.18,3.64,3.65;0.54,1.45,2.38,2.78,3.20,3.14];
pindMEST=[7.0,10.5,13.5,14.6,16.2,16.2;7.0,10.5,13.5,14.6,16.2,16.2;7.0,10.5,13.5,14.6,16.2,16.2;7.0,10.5,13.5,14.6,16.2,16.2];
peffMEST=[6.0,9.5,12.5,13.6,15.2,15.2;6.0,9.5,12.5,13.6,15.2,15.2;6.0,9.5,12.5,13.6,15.2,15.2;6.0,9.5,12.5,13.6,15.2,15.2];
pmaxMEST=[65.6,91.3,113.7,125.5,129.6,129.3;70.5,104.9,135.8,146.3,183.3,180.1;65.6,91.3,113.7,125.5,129.6,129.3;70.5,104.9,135.8,146.3,183.3,180.1];
pcompMEST=[55.5,75.4,92.5,98.0,105.4,105.2;39.6,61.6,78.3,83.0,84.5,83.8;55.5,75.4,92.5,98.0,105.4,105.2;39.6,61.6,78.3,83.0,84.5,83.8];
TegEVoutMEST=[200,259,304,324,363,364;200,241,273,292,316,314;200,259,304,324,363,364;200,241,273,292,316,314];
pegRMEST=[0.76,1.64,2.61,2.92,3.35,3.36;0.44,1.29,2.17,2.54,2.93,2.88;0.76,1.64,2.61,2.92,3.35,3.36;0.44,1.29,2.17,2.54,2.93,2.88];
TegTCinMEST=[262,325,369,385,428,429;260,304,334,350,378,371;262,325,369,385,428,429;260,304,334,350,378,371];
pegTCinMEST=[0.80,1.71,2.68,3.03,3.53,3.53;0.48,1.37,2.27,2.59,3.03,3.01;0.80,1.71,2.68,3.03,3.53,3.53;0.48,1.37,2.27,2.59,3.03,3.01];
TegTCoutMEST=[180,190,199,204,233,226;199,195,188,193,195,195;180,190,199,204,233,226;199,195,188,193,195,195];
pegTCoutMEST=[40.0,130.0,220.0,270.0,330.0,320.0;10.0,100.0,190.0,230.0,280.0,280.0;40.0,130.0,220.0,270.0,330.0,320.0;10.0,100.0,190.0,230.0,280.0,280.0];
TcwACinMEST=[24,25,24,27,24,24;23,24,24,23,23,23;24,25,24,27,24,24;23,24,24,23,23,23];
TcwACoutMEST=[24,26,27,29,28,28;23,24,25,25,26,26;24,26,27,29,28,28;23,24,25,25,26,26];
TairACinMEST=[75,125,167,181,200,202;55,104,147,165,180,181;75,125,167,181,200,202;55,104,147,165,180,181];
TairACoutMEST=[27,24,24,27,25,25;26,22,22,22,22,22;27,24,24,27,25,25;26,22,22,22,22,22];
dpairACMEST=[35.0,43.0,55.0,58.0,65.0,65.0;27.0,40.0,45.0,50.0,57.0,60.0;35.0,43.0,55.0,58.0,65.0,65.0;27.0,40.0,45.0,50.0,57.0,60.0];
dpairAFMEST=[15.0,32.0,70.0,65.0,77.0,70.0;11.0,27.0,47.0,56.0,66.0,66.0;15.0,32.0,70.0,65.0,77.0,70.0;11.0,27.0,47.0,56.0,66.0,66.0];
AuxBlowMEST=[NaN;NaN;NaN;NaN];
MCRNOxMEST=[NaN;NaN;NaN;NaN];
NOxisoMEST=[NaN;NaN;NaN;NaN];
nAE=4;
MakerAE=["Hyundai Himsen","Hyundai Himsen","Hyundai Himsen","Hyundai Himsen"];
BuilderAE=[NaN,NaN,NaN,NaN];
PistConfAE=["INLINE","INLINE","INLINE","INLINE"];
ModelAE=[NaN,NaN,NaN,NaN];
SerNoAE=[NaN,NaN,NaN,NaN];
ncAE=[8,6,6,8];
dPISTAE=[NaN,NaN,NaN,NaN];
sPISTAE=[NaN,NaN,NaN,NaN];
PengnomAE=[3840.0,2880.0,2880.0,3840.0];
PgennomAAE=[3680.0,2760.0,2760.0,3680.0];
NnomAE=[NaN,NaN,NaN,NaN];
PFnomAE=[0.800,0.800,0.800,0.800];
DualFuelAE=["Yes","Yes","Yes","Yes"];
ECNTRAE=["Yes","Yes","Yes","Yes"];
nTCAE=[1,1,1,1];
MakerTCAE=[NaN;NaN;NaN;NaN];
ModelTCAE=[NaN;NaN;NaN;NaN];
nTCinletAE=[NaN,NaN,NaN,NaN];
NmaxTCAE=[NaN;NaN;NaN;NaN];
TmaxTCAE=[NaN;NaN;NaN;NaN];
muTCAE=[NaN;NaN;NaN;NaN];
dTCcompAE=[NaN;NaN;NaN;NaN];
CoolantJKTAE=[NaN,NaN,NaN,NaN];
CoolantFOVVAE=[NaN,NaN,NaN,NaN];
CoolantLOCoolerAE=[NaN,NaN,NaN,NaN];
CoolantACAE=[NaN,NaN,NaN,NaN];
CoolantTCAE=[NaN,NaN,NaN,NaN];
MakerDiagToolAE=[NaN,NaN,NaN,NaN];
ModelDiagToolAE=[NaN,NaN,NaN,NaN];
TypeDiagToolAE=[NaN,NaN,NaN,NaN];
InstallationDiagToolAE=[NaN,NaN,NaN,NaN];
LibcodeAE=[NaN,NaN,NaN,NaN];
nAEST=8;
HullNoAEST=["3105","3105","3105","3105",NaN,NaN,"3105","3105"];
SerNoAEST=["KBA006848-1","KBA006848-1","KBA006849-1","KBA006849-1",NaN,NaN,"KBA006848-1","KBA006848-1"];
NoAE=[1,1,2,2,3,3,4,4];
FuelModeAEST=["F","G","F","G","F","G","F","G"];
LCVFOAEST=[42.296,42.296,42.296,42.296,NaN,NaN,42.296,42.296];
rhoFOAEST=[887.3,887.3,887.3,887.3,NaN,NaN,887.3,887.3];
LCVGOAEST=[NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN];
rhoGOAEST=[NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN];
LoadTypeAEST=["E","E","E","E",NaN,NaN,"E","E"];
MCRAEST=[25.0,50.0,75.0,100.0,100.0;25.0,50.0,75.0,100.0,100.0;25.0,50.0,75.0,100.0,100.0;25.0,50.0,75.0,100.0,100.0;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;25.0,50.0,75.0,100.0,100.0;25.0,50.0,75.0,100.0,100.0];
etagenAEST=[0.960,0.934,0.952,0.960,0.960;0.876,0.934,0.952,0.960,0.960;0.880,0.936,0.953,0.960,0.960;0.880,0.936,0.953,0.960,0.960;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;0.960,0.934,0.952,0.960,0.960;0.876,0.934,0.952,0.960,0.960];
NAEST=[720.00,720.00,720.00,720.00,720.00;720.00,720.00,720.00,720.00,720.00;720.00,720.00,720.00,720.00,720.00;720.00,720.00,720.00,720.00,720.00;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;720.00,720.00,720.00,720.00,720.00;720.00,720.00,720.00,720.00,720.00];
PengAEST=[960.0,1920.0,2880.0,3840.0,3840.0;NaN,NaN,NaN,NaN,NaN;720.0,1440.0,2160.0,2880.0,2880.0;720.0,1440.0,2160.0,2880.0,2880.0;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;960.0,1920.0,2880.0,3840.0,3840.0;NaN,NaN,NaN,NaN,NaN];
PgenAEST=[NaN,NaN,NaN,NaN,NaN;960.0,1920.0,2880.0,3840.0,3840.0;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;960.0,1920.0,2880.0,3840.0,3840.0];
NTCAEST=[16230,21330,26190,29820,29850;15630,19950,20580,27420,27420;19200,24570,29340,32790,32880;16920,22800,26770,29850,29670;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;16230,21330,26190,29820,29850;15630,19950,20580,27420,27420];
FPIAEST=[10.5,13.0,26.5,35.9,35.9;NaN,NaN,NaN,NaN,NaN;10.2,18.3,26.8,35.8,35.8;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;10.5,13.0,26.5,35.9,35.9;NaN,NaN,NaN,NaN,NaN];
TFOPPinAEST=[36,38,39,40,40;24,25,25,25,26;32,35,35,37,37;24,25,25,25,25;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;36,38,39,40,40;24,25,25,25,26];
TGOinAEST=[NaN,NaN,NaN,NaN,NaN;18.0,20.0,18.0,20.0,20.0;NaN,NaN,NaN,NaN,NaN;19.0,19.8,19.8,20.0,20.0;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;18.0,20.0,18.0,20.0,20.0];
pGOinAEST=[NaN,NaN,NaN,NaN,NaN;1.6,2.3,4.5,4.2,4.2;NaN,NaN,NaN,NaN,NaN;1.7,2.6,3.3,4.2,4.2;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;1.6,2.3,4.5,4.2,4.2];
FOCAEST=[210.0,373.2,537.6,720.0,720.0;11.5,10.6,8.0,9.0,9.0;158.0,280.0,412.8,545.6,545.6;4.7,4.9,5.3,4.9,4.9;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;210.0,373.2,537.6,720.0,720.0;11.5,10.6,8.0,9.0,9.0];
SFOCAEST=[218.750,194.375,186.667,187.500,187.500;NaN,NaN,NaN,NaN,NaN;220.000,195.000,181.110,189.440,189.440;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;218.750,194.375,186.667,187.500,187.500;NaN,NaN,NaN,NaN,NaN];
SFOCisoAEST=[215.097,190.720,184.795,184.370,184.370;NaN,NaN,NaN,NaN,NaN;216.298,191.567,189.158,186.257,186.257;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;215.097,190.720,184.795,184.370,184.370;NaN,NaN,NaN,NaN,NaN];
GOCAEST=[NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN];
SGOCAEST=[NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN];
SGOCisoAEST=[NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN];
TairTCinAEST=[24,24,24,25,25;23,23,23,23,23;24,24,24,24,24;23,23,23,23,23;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;24,24,24,25,25;23,23,23,23,23];
pambairAEST=[1017,1017,1016,1017,1017;1020,1020,1020,1020,1020;1018,1018,1018,1018,1018;1018,1018,1018,1028,1028;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;1017,1017,1016,1017,1017;1020,1020,1020,1020,1020];
pmaxAEST=[73.3,92.6,124.6,151.3,151.3;62.0,75.6,77.2,155.0,155.0;74.7,90.5,122.0,147.7,147.2;61.0,71.0,112.0,151.0,151.0;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;73.3,92.6,124.6,151.3,151.3;62.0,75.6,77.2,155.0,155.0];
pcompAEST=[NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN];
TegEVoutAEST=[332,441,470,524,525;330,473,475,479,479;308,426,472,535,536;330,462,485,493,492;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;332,441,470,524,525;330,473,475,479,479];
TegTCinAEST=[376,500,510,542,543;392,556,560,555,553;344,466,491,536,538;392,542,565,565,564;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;376,500,510,542,543;392,556,560,555,553];
TegTCoutAEST=[307,384,352,341,341;328,451,409,381,378;261,335,319,321,324;325,425,418,389,388;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;307,384,352,341,341;328,451,409,381,378];
TcwACinAEST=[43,42,40,38,38;46,46,44,45,45;40,40,40,39,38;43,44,47,45,45;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;43,42,40,38,38;46,46,44,45,45];
TcwACoutAEST=[43,42,41,39,40;47,47,45,46,46;41,41,41,40,40;43,45,48,46,46;NaN,NaN,NaN,NaN,NaN;NaN,NaN,NaN,NaN,NaN;43,42,41,39,40;47,47,45,46,46];
TairACinAEST=[NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN];
TscavAEST=[NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN];
pscavAEST=[NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN];
dpairACAEST=[NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN];
dpairAFAEST=[NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN];
MCRNOxAEST=[NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN];
NOxisoAEST=[NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN];
nBL=2;
FluidBL=["S","S"];
MakerBL=["Aalborg","Aalborg"];
ModelBL=["OS-TCI","OS-TCI"];
DualFuelBL=["No","No"];
SteamCapacityBL=[7500,7500];
nFM=10;
FluidFM=["FO","GAS","FO","GAS","GAS","GAS","GAS","GAS","FO","FO"];
EquipmentFM_new=["ME1","ME1","ME2","ME2","AE1","AE2","AE3","AE4","AE1,AE2","AE3,AE4"];
EquipmentFM=["ME1","ME1","ME2","ME2","AE1","AE2","AE3","AE4","AE2","AE3"];
LocationFM=["In","In","In","In","In","In","In","In","In","In"];
TypeFM=["M","M","M","M","M","M","M","M","M","M"];
MakerFM=["Emerson","Emerson","Emerson","Emerson","Emerson","Emerson","Emerson","Emerson","Emerson","Emerson"];
ModelFM=["Micro Motion Coriolis","Micro Motion Coriolis","Micro Motion Coriolis","Micro Motion Coriolis","Micro Motion Coriolis","Micro Motion Coriolis","Micro Motion Coriolis","Micro Motion Coriolis","Micro Motion Coriolis","Micro Motion Coriolis"];
FaccuracyFM=[0.20,0.27,0.20,0.27,NaN,NaN,NaN,NaN,NaN,NaN];
InterfaceFM=[NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN,NaN];
MakerSL="Furuno";
ModelSL="DS-60";
UaccuracySL=2.00;
SPM="Yes";
nSPM=2;
ECNTRSPM=["Yes","Yes"];
LocationSPM=["0","1"];
MakerSPM=["Kyma","Kyma"];
ModelSPM=["KPM","KPM"];
QaccuracySPM=[0.50,0.50];
NaccuracySPM=[0.10,0.10];
InterfaceSPM=["Digital","Digital"];
SThruster="No";
nSThruster=[NaN];
LocationSThruster=[NaN];
dSThruster=[NaN];
pitchSThruster=[NaN];
dtunSThruster=[NaN];
VnomSThruster=[NaN];
AnomSThruster=[NaN];
PnomSThruster=[NaN];
NnomSThruster=[NaN];
GCU="Yes";
nGCU=1;
MakerGCU=["Alfa Laval"];
ModelGCU=["TLA-8.5-S3"];
CapacityGCU=[2570];
SG="No";
nSG=[NaN];
ECNTRSG=[NaN];
MakerSG=[NaN];
ModelSG=[NaN];
PnomSG=[NaN];
NnomSG=[NaN];
etaSG=[NaN];
RG="No";
nRG=[NaN];
MakerRG=[NaN];
ModelRG=[NaN];
RatioRG=[NaN];
etaRG=[NaN];
STurbine="No";
nSTurbine=[NaN];
MakerSTurbine=[NaN];
ModelSTurbine=[NaN];
PnomSTurbine=[NaN];
NnomSTurbine=[NaN];
DualFuelSTurbine=[NaN];
GTurbine="No";
nGTurbine=[NaN];
MakerGTurbine=[NaN];
ModelGTurbine=[NaN];
PnomGTurbine=[NaN];
NnomGTurbine=[NaN];
DualFuelGTurbine=[NaN];
PMotor="No";
nPMotor=[NaN];
ECNTRPMotor=[NaN];
MakerPMotor=[NaN];
ModelPMotor=[NaN];
TypePMotor=[NaN];
PnomPMotor=[NaN];
NnomPMotor=[NaN];
IGG="Yes";
nIGG=1;
MakerIGG=["Alfa Laval"];
ModelIGG=["Smit LNG"];
CapacityIGG=[16000];
INC="Yes";
nINC=1;
MakerINC=["Maxi"];
ModelINC=["1200SL WS"];
EG="Yes";
nEG=1;
TmCP=[NaN];
FwiCP=[NaN];
SSNCP=[NaN];
UCP=[NaN];
FOCMECP=[NaN];
LCVFOMECP=[NaN];
g=9.8067;
Zaid=10;
kyy=0.25;
ksiP=0;
ksiN=0.2;
ksiU=-0.33;
LCVMDOref=42.7;
LCVHFOref=40.6;
Ck2=1.309;
TCNomogram=[-0.00126,1.063;-0.000964,1.939];
CorAllowance=50;
MCRidAE=80;
RHmarginAE=1;
F1iso=[-0.002446,0.002856,0.002954,0.002198];
F2iso=[-0.00059,-0.00222,-0.00153,-0.00081];
Kiso=[273,1,1,1];
Cpcomp=[0.000226,-0.002954,0.00153,-7.02E-05];
Cpmax=[0.000165,-0.002198,0.00081,-5.28E-05];
Cpscav=[0.00022,-0.002856,0.00222,-6.79E-05];
CTegTCin=[-0.000169,0.002466,0.00059,6.13E-05];
CTegTCout=[-2.8E-05,0.00316,0.00018,9.74E-05];
CSFOC=[-5.3E-05,0.000707,0.000413,1.68E-05];
pambairiso=1000;
TairTCiniso=25;
SCOCminMANelME=0.6;
