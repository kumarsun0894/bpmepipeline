NameVessel="Flex Ranger";
NoonVerModel="Y";
HPVerModel="Y";
MEVerModel="Y";
AEVerModel=[NaN];
AutoVerModel=[NaN];
HPLogConfig="Y";
MELogConfig="Y";
AELogConfig="Y";
NoonLogConfig="Y";
ExNames=[NaN];
NotesModel=[NaN];
lastprocessedNoon=[NaN];
lastprocessedHP=[NaN];
lastprocessedME=[NaN];
lastprocessedAE=[NaN];
HullNoVessel="SN2107";
IMONoVessel=9709025;
BuildYardVessel="Samsung Heavy Ind.";
YearBuildVessel="2018";
UltimateOwnerVessel="Frontline Management AS";
TypeVessel="GAST";
TypeSTAWINDVessel="GC";
SuperStructureSTAWINDVessel="PED";
Loa=293.16;
Lbp=285.00;
B=45.80;
D=26.20;
Td=11.50;
Tb=9.22;
Ts=12.00;
Ud=19.50;
CapacityCargoVessel=174100;
DWT=82301;
nCHVessel=4;
TypeCGVessel="PP";
nCGVessel=8;
CapacityCGVessel=[NaN];
Ds=117147;
Vs=114290;
Dd=117878;
Vd=115003;
Ad=1564;
Zad=52.15;
BulbousBowVessel="Yes";
BowSection=[285.00,285.00;3.29,11.05];
Cstern="N";
TransomSternVessel="Yes";
HullProfile=[-5.02,-5.02,11.43,12.35,11.88,10.67,7.27,4.60,4.60,5.94,8.41,11.15,14.51,20.45,27.66,30.68,40.33,270.74,274.93,282.05,285.79,287.57,288.14,287.19,285.70,285.06,285.06,287.95;20.55,12.35,8.17,7.35,6.43,5.89,5.23,5.23,3.77,3.64,2.95,2.09,1.27,0.57,0.48,0.32,0.00,0.00,0.28,1.74,3.74,5.89,8.49,9.82,10.20,10.83,12.54,26.69];
RudderProfile=[-4.86,-4.13,-3.27,0.98,2.03,2.35,-4.86;12.06,8.83,0.70,0.70,8.64,10.32,12.06];
TransomSection=[0.00,14.30;12.28,20.52];
nISBRG=2;
etaS=0.990;
etaB=1.000;
AutoLoggingVessel=[NaN];
vesselsensd=[NaN];
vesselwp="Meteo";
vesselpp=[NaN];
DrawingURLVessel="https://bsmcloud.sharepoint.com/sites/FPC-ShipDrawings/Shared%20Documents/Forms/AllItems.aspx?RootFolder=%2Fsites%2FFPC%2DShipDrawings%2FShared%20Documents%2FVessel%20Drawings%2FBSM%20%28UK%29%2F9709037%20%2D%20Flex%20Rainbow&FolderCTID=0x012000444889A43C820240A52D85B51C50F42D&View=%7BA7082555%2D3FF2%2D4418%2DA4AA%2D81FA86A8EF19%7D";
rhoswh=1025.0;
Th=[4.00,4.50,5.00,5.50,6.00,6.50,7.00,7.50,8.00,8.50,9.00,9.50,10.00,10.50,11.00,11.50,12.00,12.50,13.00];
Dh=[36523.6,41590.1,46712.3,51878.3,57081.4,62335.7,67635.7,72976.3,78359.6,83787.1,89260.4,94783.2,100352.9,105975.2,111646.7,117369.6,123143.4,128963.9,134814.3];
WSAh=[11275.60,11664.70,12040.60,12410.20,12757.50,13115.60,13474.50,13816.80,14162.30,14520.00,14881.70,15235.60,15605.00,15965.00,16319.60,16674.20,17025.10,17356.90,17664.50];
Cmh=[0.986,0.988,0.989,0.990,0.991,0.992,0.992,0.993,0.993,0.994,0.994,0.994,0.995,0.995,0.995,0.995,0.996,0.996,0.996];
Cwph=[0.751,0.760,0.768,0.774,0.781,0.788,0.795,0.801,0.807,0.814,0.820,0.828,0.836,0.843,0.851,0.858,0.866,0.872,0.876];
LCBh=[140.93,140.70,140.53,140.37,140.23,140.06,139.87,139.67,139.45,139.21,138.94,138.64,138.31,137.96,137.60,137.21,136.82,136.42,136.04];
TaMT=[9.60,11.50,12.00];
TfMT=[9.60,11.50,12.00];
VMT=[93647.00,114584.00,120216.00];
PowerTypeMT="D";
U0MT=[12.00,14.00,16.00,17.00,17.50,18.00,19.00,19.50,20.00,21.00,22.00;12.00,14.00,16.00,17.00,17.50,18.00,19.00,19.50,20.00,21.00,22.00;12.00,14.00,16.00,17.00,17.50,18.00,19.00,19.50,20.00,21.00,NaN];
P0MT=[4028,6249,9095,10795,11727,12716,14898,16196,17644,21111,25657;4076,6327,9324,11154,12173,13287,15829,17307,18910,22813,27567;4024,6322,9398,11308,12392,13553,16245,17838,19571,23565,NaN];
N0MT=[41.80,48.56,55.27,58.63,60.31,62.00,65.44,67.24,69.08,73.01,77.45;42.32,49.20,56.15,59.66,61.41,63.20,66.95,68.87,70.87,75.07,79.44;42.52,49.50,56.51,60.07,61.88,63.71,67.50,69.53,71.59,75.82,NaN];
etaD0MT=[NaN];
U15MT=[NaN];
P15MT=[NaN];
N15MT=[NaN];
etaD15MT=[NaN];
TrialIndexMT=1;
UtrialMT=[NaN];
PtrialMT=[NaN];
NtrialMT=[NaN];
nProp=2;
TypeProp="FPP";
dProp=8.400;
pitchProp=8.370;
zProp=4;
AeAoProp=0.429;
HullNoSTrial="SN2108";
PowerTypeSTrial="D";
TaConSTrial=9.60;
TfConSTrial=9.60;
VConSTrial=93647.00;
TaSTrial=9.67;
TfSTrial=9.65;
VSTrial=93890.50;
UCorSTrial=[18.72,19.95,19.88,20.41,21.61];
PCorSTrial=[15281,17856,17858,20117,23137];
NCorSTrial=[67.05,70.70,70.70,73.60,77.17];
WeatherCorSTrial="Yes";
TEUSTrial=[NaN];
WSASTrial=[NaN];
TswSTrial=[NaN];
rhoswSTrial=[NaN];
TairSTrial=[NaN];
pairSTrial=[NaN];
azbowSTrial=[NaN];
SOGSTrial=[NaN];
PSTrial=[NaN];
NPropSTrial=[NaN];
HeadSTrial=[NaN];
UwirSTrial=[NaN];
psiwirSTrial=[NaN];
HwvSTrial=[NaN];
TwvSTrial=[NaN];
psiwvrSTrial=[NaN];
HslSTrial=[NaN];
TslSTrial=[NaN];
psislrSTrial=[NaN];
hswSTrial=[NaN];
nME=2;
MakerME=["MAN","MAN"];
BuilderME=[NaN,NaN];
ModelME=["5G70ME-C9.5-GI","5G70ME-C9.5-GI"];
SerNoME=[NaN,NaN];
CategoryME=["2","2"];
TypeME=["ME","ME"];
PistConfME=["INLINE","INLINE"];
ModeME=["Tier lll";"Tier lll"];
etaeffME=[0.990,0.990];
ncME=[5,5];
dPISTME=[0.700,0.700];
sPISTME=[3.256,3.256];
PMCRME=[12222.0,12222.0];
PNCRME=[10710,10710];
NMCRME=[74.00,74.00];
NNCRME=[70.80,70.80];
DualFuelME=["Yes","Yes"];
VEGBPME=["Yes","Yes"];
LIWATME=["No","No"];
IARME=["Yes","Yes"];
MBTME=["Yes","Yes"];
CBTME=["No","No"];
AVME=["Yes","Yes"];
TVME=["Yes","Yes"];
TCCOSME=["No","No"];
nTCME=[1,1];
MakerTCME=[NaN;NaN];
ModelTCME=["A270-L";"A270-L"];
NmaxTCME=[18060;18060];
TmaxTCME=[550;550];
muTCME=[NaN;NaN];
dTCcompME=[NaN;NaN];
TypeGovME=[NaN,NaN];
ModelGovME=[NaN,NaN];
TypeLBME=["E","E"];
MakerLBME=["MAN","MAN"];
ModelLBME=["ALPHA","ALPHA"];
PulseLBME=["NA","NA"];
HighPISTME=["NA","NA"];
RegLBME=[NaN,NaN];
BNLBME=[100,100];
ACCLBME=[0.40,0.40];
SCOCminME=[0.60,NaN];
SCOCmaxME=[2.21,NaN];
SCOCbME=[NaN,NaN];
SCOCidME=[0.65,0.65];
CoolantJKTME=["FW","FW"];
CoolantFOVVME=[NaN,NaN];
CoolantLOCoolerME=[NaN,NaN];
CoolantACME=[NaN,NaN];
CoolantTCME=[NaN,NaN];
nSPME=[NaN,NaN];
nACTRME=[NaN,NaN];
MakerDiagToolME=["PMI","PMI"];
ModelDiagToolME=[NaN,NaN];
TypeDiagToolME=[NaN,NaN];
InstallationDiagToolME=["Permanent","Permanent"];
LibcodeME=["601.01","601.02"];
ModeMESim=[NaN];
MCRMESim=[NaN];
NMESim=[NaN];
PMESim=[NaN];
SFOCisoMESim=[NaN];
TegEVoutMESim=[NaN];
mdotegMESim=[NaN];
mdotSteamProductionMESim=[NaN];
nMEST=4;
HullNoMEST=["SN2108","SN2108","SN2108","SN2108"];
SerNoMEST=["DML0105900","DML0105900","DML0105900","DML0105900"];
NoME=[1,1,2,2];
FuelModeMEST=["F","G","F","G"];
PowerTypeMEST=["E","E","E","E"];
ModeMEST=["3","3","3","3"];
LCVFOMEST=[41.5,41.5,41.5,41.5];
rhoFOMEST=[943.0,943.0,943.0,943.0];
LCVGOMEST=[NaN,49.3,NaN,49.3];
rhoGOMEST=[NaN,NaN,NaN,NaN];
MCRMEST=[25.0,50.0,75.0,87.6,100.0;25.0,50.0,75.0,87.6,100.0;25.0,50.0,75.0,87.6,100.0;25.0,50.0,75.0,87.6,100.0];
NMEST=[46.60,58.70,67.20,70.80,74.00;46.60,58.70,67.20,70.80,74.00;46.60,58.70,67.20,70.80,74.00;46.60,58.70,67.20,70.80,74.00];
PMEST=[3056,6111,9167,10706,12222;3056,6111,9167,10706,12222;3056,6111,9167,10706,12222;3056,6111,9167,10706,12222];
NTCMEST=[6286,10771,13855,14597,15741;7027,10973,13670,14556,15767;6286,10771,13855,14597,15741;7027,10973,13670,14556,15767];
FPIMEST=[43.0,65.0,84.0,92.0,100.0;43.0,65.0,85.0,91.0,100.0;43.0,65.0,84.0,92.0,100.0;43.0,65.0,85.0,91.0,100.0];
TFOPPinMEST=[27,28,29,30,30;32,33,34,34,35;27,28,29,30,30;32,33,34,34,35];
TGOinMEST=[NaN;NaN;NaN;NaN];
pGOinMEST=[NaN;NaN;NaN;NaN];
FOCMEST=[544.0,1011.0,1527.0,1806.0,2091.0;31.7,50.3,62.3,62.2,67.8;544.0,1011.0,1527.0,1806.0,2091.0;31.7,50.3,62.3,62.2,67.8];
SFOCMEST=[178.10,165.40,166.60,168.70,171.10;10.40,8.20,6.80,5.80,5.50;178.10,165.40,166.60,168.70,171.10;10.40,8.20,6.80,5.80,5.50];
SFOCisoMEST=[174.40,162.10,163.10,164.50,166.90;10.10,8.00,6.60,5.70,5.40;174.40,162.10,163.10,164.50,166.90;10.10,8.00,6.60,5.70,5.40];
GOCMEST=[NaN,NaN,NaN,NaN,NaN;410.5,819.8,1202.6,1437.7,1705.0;NaN,NaN,NaN,NaN,NaN;410.5,819.8,1202.6,1437.7,1705.0];
SGOCMEST=[NaN,NaN,NaN,NaN,NaN;134.4,134.2,131.2,134.3,139.5;NaN,NaN,NaN,NaN,NaN;134.4,134.2,131.2,134.3,139.5];
SGOCisoMEST=[NaN,NaN,NaN,NaN,NaN;132.8,132.6,129.7,132.4,137.8;NaN,NaN,NaN,NaN,NaN;132.8,132.6,129.7,132.4,137.8];
TairTCinMEST=[16,17,18,19,20;22,22,22,23,23;16,17,18,19,20;22,22,22,23,23];
pambairMEST=[1015,1015,1015,1014,1015;1015,1015,1015,1015,1015;1015,1015,1015,1014,1015;1015,1015,1015,1015,1015];
TscavMEST=[27,23,26,30,34;34,29,30,34,38;27,23,26,30,34;34,29,30,34,38];
pscavMEST=[0.33,1.01,2.02,2.29,2.80;0.38,1.04,1.89,2.25,2.77;0.33,1.01,2.02,2.29,2.80;0.38,1.04,1.89,2.25,2.77];
pindMEST=[6.0,9.7,12.9,14.4,15.8;6.0,9.6,12.7,14.3,15.6;6.0,9.7,12.9,14.4,15.8;6.0,9.6,12.7,14.3,15.6];
peffMEST=[5.0,8.7,11.9,13.4,14.8;5.0,8.6,11.7,13.3,14.6;5.0,8.7,11.9,13.4,14.8;5.0,8.6,11.7,13.3,14.6];
pmaxMEST=[108.4,145.8,166.1,184.9,172.2;119.5,155.8,184.0,184.9,173.6;108.4,145.8,166.1,184.9,172.2;119.5,155.8,184.0,184.9,173.6];
pcompMEST=[68.8,105.1,126.4,155.3,148.9;79.0,115.9,144.2,153.6,148.8;68.8,105.1,126.4,155.3,148.9;79.0,115.9,144.2,153.6,148.8];
TegEVoutMEST=[166,219,260,280,327;184,217,247,277,325;166,219,260,280,327;184,217,247,277,325];
pegRMEST=[0.28,0.90,1.90,2.20,2.70;0.36,0.98,1.81,2.16,2.66;0.28,0.90,1.90,2.20,2.70;0.36,0.98,1.81,2.16,2.66];
TegTCinMEST=[230,300,340,360,405;245,305,335,360,395;230,300,340,360,405;245,305,335,360,395];
pegTCinMEST=[0.25,0.89,1.86,2.14,2.64;0.33,0.94,1.75,2.10,2.60;0.25,0.89,1.86,2.14,2.64;0.33,0.94,1.75,2.10,2.60];
TegTCoutMEST=[170,200,190,200,222;195,200,190,200,223;170,200,190,200,222;195,200,190,200,223];
pegTCoutMEST=[10.0,80.0,170.0,260.0,300.0;5.0,70.0,200.0,240.0,300.0;10.0,80.0,170.0,260.0,300.0;5.0,70.0,200.0,240.0,300.0];
TcwACinMEST=[17,17,18,20,22;22,22,22,23,24;17,17,18,20,22;22,22,22,23,24];
TcwACoutMEST=[17,20,26,29,34;22,25,29,33,38;17,20,26,29,34;22,25,29,33,38];
TairACinMEST=[35,90,145,165,185;50,98,145,165,190;35,90,145,165,185;50,98,145,165,190];
TairACoutMEST=[17,17,21,25,30;22,23,25,29,33;17,17,21,25,30;22,23,25,29,33];
dpairACMEST=[40.0,75.0,105.0,110.0,120.0;50.0,75.0,100.0,115.0,120.0;40.0,75.0,105.0,110.0,120.0;50.0,75.0,100.0,115.0,120.0];
dpairAFMEST=[10.0,29.0,49.0,78.0,88.0;10.0,30.0,55.0,72.0,90.0;10.0,29.0,49.0,78.0,88.0;10.0,30.0,55.0,72.0,90.0];
AuxBlowMEST=[1,0,0,0,0;1,0,0,0,0;1,0,0,0,0;1,0,0,0,0];
MCRNOxMEST=[NaN;NaN;NaN;NaN];
NOxisoMEST=[NaN;NaN;NaN;NaN];
nAE=4;
MakerAE=["Wartsila","Wartsila","Wartsila","Wartsila"];
BuilderAE=[NaN,NaN,NaN,NaN];
PistConfAE=["INLINE","INLINE","INLINE","INLINE"];
ModelAE=["6L34DF","8L34DF","8L34DF","6L34DF"];
SerNoAE=[NaN,NaN,NaN,NaN];
ncAE=[6,8,8,6];
dPISTAE=[NaN,NaN,NaN,NaN];
sPISTAE=[NaN,NaN,NaN,NaN];
PengnomAE=[2880.0,3840.0,3840.0,2880.0];
PgennomAAE=[3460.0,4610.0,4610.0,3460.0];
NnomAE=[NaN,NaN,NaN,NaN];
PFnomAE=[0.800,0.800,0.800,0.800];
DualFuelAE=["Yes","Yes","Yes","Yes"];
ECNTRAE=["Yes","Yes","Yes","Yes"];
nTCAE=[1,1,1,1];
MakerTCAE=[NaN;NaN;NaN;NaN];
ModelTCAE=[NaN;NaN;NaN;NaN];
nTCinletAE=[NaN,NaN,NaN,NaN];
NmaxTCAE=[NaN;NaN;NaN;NaN];
TmaxTCAE=[NaN;NaN;NaN;NaN];
muTCAE=[NaN;NaN;NaN;NaN];
dTCcompAE=[NaN;NaN;NaN;NaN];
CoolantJKTAE=[NaN,NaN,NaN,NaN];
CoolantFOVVAE=[NaN,NaN,NaN,NaN];
CoolantLOCoolerAE=[NaN,NaN,NaN,NaN];
CoolantACAE=[NaN,NaN,NaN,NaN];
CoolantTCAE=[NaN,NaN,NaN,NaN];
MakerDiagToolAE=[NaN,NaN,NaN,NaN];
ModelDiagToolAE=[NaN,NaN,NaN,NaN];
TypeDiagToolAE=[NaN,NaN,NaN,NaN];
InstallationDiagToolAE=[NaN,NaN,NaN,NaN];
LibcodeAE=["651.01","651.03","651.03","651.01"];
nAEST=4;
HullNoAEST=[NaN,NaN,NaN,NaN];
SerNoAEST=[NaN,NaN,NaN,NaN];
NoAE=[1,2,3,4];
FuelModeAEST=["F","F","F","F"];
LCVFOAEST=[NaN,NaN,NaN,NaN];
rhoFOAEST=[NaN,NaN,NaN,NaN];
LCVGOAEST=[NaN,NaN,NaN,NaN];
rhoGOAEST=[NaN,NaN,NaN,NaN];
LoadTypeAEST=[NaN,NaN,NaN,NaN];
MCRAEST=[NaN;NaN;NaN;NaN];
etagenAEST=[NaN;NaN;NaN;NaN];
NAEST=[NaN;NaN;NaN;NaN];
PengAEST=[NaN;NaN;NaN;NaN];
PgenAEST=[NaN;NaN;NaN;NaN];
NTCAEST=[NaN;NaN;NaN;NaN];
FPIAEST=[NaN;NaN;NaN;NaN];
TFOPPinAEST=[NaN;NaN;NaN;NaN];
TGOinAEST=[NaN;NaN;NaN;NaN];
pGOinAEST=[NaN;NaN;NaN;NaN];
FOCAEST=[NaN;NaN;NaN;NaN];
SFOCAEST=[NaN;NaN;NaN;NaN];
SFOCisoAEST=[NaN;NaN;NaN;NaN];
GOCAEST=[NaN;NaN;NaN;NaN];
SGOCAEST=[NaN;NaN;NaN;NaN];
SGOCisoAEST=[NaN;NaN;NaN;NaN];
TairTCinAEST=[NaN;NaN;NaN;NaN];
pambairAEST=[NaN;NaN;NaN;NaN];
pmaxAEST=[NaN;NaN;NaN;NaN];
pcompAEST=[NaN;NaN;NaN;NaN];
TegEVoutAEST=[NaN;NaN;NaN;NaN];
TegTCinAEST=[NaN;NaN;NaN;NaN];
TegTCoutAEST=[NaN;NaN;NaN;NaN];
TcwACinAEST=[NaN;NaN;NaN;NaN];
TcwACoutAEST=[NaN;NaN;NaN;NaN];
TairACinAEST=[NaN;NaN;NaN;NaN];
TscavAEST=[NaN;NaN;NaN;NaN];
pscavAEST=[NaN;NaN;NaN;NaN];
dpairACAEST=[NaN;NaN;NaN;NaN];
dpairAFAEST=[NaN;NaN;NaN;NaN];
MCRNOxAEST=[NaN;NaN;NaN;NaN];
NOxisoAEST=[NaN;NaN;NaN;NaN];
nBL=2;
FluidBL=["S","S"];
MakerBL=[NaN,NaN];
ModelBL=[NaN,NaN];
DualFuelBL=["No","No"];
SteamCapacityBL=[NaN,NaN];
nFM=6;
FluidFM=["FO","GAS","FO","GAS","FO","GAS"];
EquipmentFM_new=["ME1","ME1","ME2","ME2","AE1","AE1"];
EquipmentFM=["ME1","ME1","ME2","ME2","AE1","AE1"];
LocationFM=["In","In","In","In","In","In"];
TypeFM=["V","M","V","M","V","M"];
MakerFM=[NaN,NaN,NaN,NaN,NaN,NaN];
ModelFM=[NaN,NaN,NaN,NaN,NaN,NaN];
FaccuracyFM=[NaN,NaN,NaN,NaN,NaN,NaN];
InterfaceFM=[NaN,NaN,NaN,NaN,NaN,NaN];
MakerSL="Furuno";
ModelSL="DS-60";
UaccuracySL=0.50;
SPM="Yes";
nSPM=1;
ECNTRSPM=["Yes"];
LocationSPM=["1","2"];
MakerSPM=["KYMA"];
ModelSPM=["KYMA Ship Performance"];
QaccuracySPM=[NaN];
NaccuracySPM=[NaN];
InterfaceSPM=[NaN];
SThruster=[NaN];
nSThruster=[NaN];
LocationSThruster=[NaN];
dSThruster=[NaN];
pitchSThruster=[NaN];
dtunSThruster=[NaN];
VnomSThruster=[NaN];
AnomSThruster=[NaN];
PnomSThruster=[NaN];
NnomSThruster=[NaN];
GCU="Yes";
nGCU=1;
MakerGCU=[NaN];
ModelGCU=[NaN];
CapacityGCU=[NaN];
SG="No";
nSG=[NaN];
ECNTRSG=[NaN];
MakerSG=[NaN];
ModelSG=[NaN];
PnomSG=[NaN];
NnomSG=[NaN];
etaSG=[NaN];
RG="No";
nRG=[NaN];
MakerRG=[NaN];
ModelRG=[NaN];
RatioRG=[NaN];
etaRG=[NaN];
STurbine="No";
nSTurbine=[NaN];
MakerSTurbine=[NaN];
ModelSTurbine=[NaN];
PnomSTurbine=[NaN];
NnomSTurbine=[NaN];
DualFuelSTurbine=[NaN];
GTurbine="No";
nGTurbine=[NaN];
MakerGTurbine=[NaN];
ModelGTurbine=[NaN];
PnomGTurbine=[NaN];
NnomGTurbine=[NaN];
DualFuelGTurbine=[NaN];
PMotor="No";
nPMotor=[NaN];
ECNTRPMotor=[NaN];
MakerPMotor=[NaN];
ModelPMotor=[NaN];
TypePMotor=[NaN];
PnomPMotor=[NaN];
NnomPMotor=[NaN];
IGG="Yes";
nIGG=1;
MakerIGG=[NaN];
ModelIGG=[NaN];
CapacityIGG=[NaN];
INC="Yes";
nINC=1;
MakerINC=[NaN];
ModelINC=[NaN];
EG="Yes";
nEG=[NaN];
TmCP=[NaN];
FwiCP=[NaN];
SSNCP=[NaN];
UCP=[NaN];
FOCMECP=[NaN];
LCVFOMECP=[NaN];
g=9.8067;
Zaid=10;
kyy=0.25;
ksiP=0;
ksiN=0.2;
ksiU=-0.33;
LCVMDOref=42.7;
LCVHFOref=40.6;
Ck2=1.309;
TCNomogram=[-0.00126,1.063;-0.000964,1.939];
CorAllowance=50;
MCRidAE=80;
RHmarginAE=1;
F1iso=[-0.002446,0.002856,0.002954,0.002198];
F2iso=[-0.00059,-0.00222,-0.00153,-0.00081];
Kiso=[273,1,1,1];
Cpcomp=[0.000226,-0.002954,0.00153,-7.02E-05];
Cpmax=[0.000165,-0.002198,0.00081,-5.28E-05];
Cpscav=[0.00022,-0.002856,0.00222,-6.79E-05];
CTegTCin=[-0.000169,0.002466,0.00059,6.13E-05];
CTegTCout=[-2.8E-05,0.00316,0.00018,9.74E-05];
CSFOC=[-5.3E-05,0.000707,0.000413,1.68E-05];
pambairiso=1000;
TairTCiniso=25;
SCOCminMANelME=0.6;
