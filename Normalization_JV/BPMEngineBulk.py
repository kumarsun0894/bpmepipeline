# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 10:58:49 2020
@author: Subhin Antony 
"""
import pyodbc
import io
from LogSetup import setup_logging
setup_logging()
import logging
logger = logging.getLogger('BPMEngine.2.02')
#directory =os.path.split(Path(PureWindowsPath(os.path.abspath(__file__))))[0]
#os.chdir(Path(PureWindowsPath(directory)))


import numpy as np
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import ast
import time

#         from LogSetup import setup_logging
#         setup_logging()

from Bulk_Api.PendingReports_Bulk import PendingReports_Bulk
from BPME_Repository.PALRefApi import PALRefApi
from BPME_Repository.LoadReferenceData import LoadReferenceData
from BPME_Repository.isComplexPropulsion import isComplexPropulsion
from BPME_Transform.ReferenceCurves import ReferenceCurves
from BPME_Repository.VesselFilter import VesselFilter
from BPME_Core.RefDashboards import RefDashboards
from BPME_Repository.LoadServiceData import LoadServiceData
from BPME_Repository.PALNoonApi import PALNoonApi
from BPME_Repository.PALHPApi import PALHPApi
from BPME_Repository.PALMEApi import PALMEApi
from BPME_Repository.PALAEApi import PALAEApi
from BPME_Repository.NDLRefApi import NDLRefApi
from BPME_Repository.SaveNoonInput import SaveNoonInput
from BPME_Repository.SaveHPInput import SaveHPInput
from BPME_Repository.SaveMEInput import SaveMEInput
from BPME_Repository.SaveAEInput import SaveAEInput
from BPME_Repository.NoonOutputApi import NoonOutputApi
from BPME_Repository.HPOutputApi import HPOutputApi
from BPME_Repository.MEOutputApi import MEOutputApi
from BPME_Repository.AEOutputApi import AEOutputApi
from BPME_Repository.SaveRefApI import SaveRefApI

#from BPMEngineSingleVessel import BPMEngineSingleVessel
from BulkBPMEngineSingleVessel  import BPMEngineSingleVessel

from config import mariHeaders,mariclientKey,mariclientCrt,mariBaseUrl,NaviHeaders
from config import maripendingUrl,mariGetRef,mariGetNoon,mariGetHP,mariGetME,mariGetAE
from config import naviBaseUrl,getReference,saveNoon,saveNoonKPI,saveHP,saveHPKPI
from config import saveSRQurl,saveME,saveMEKPI,saveAE,saveAEKPI
from config import saveReference
from config import vessel,fromDate,mariNoonOut,mariHPOut,mariMEOut,mariRefOut,mariAEOut
from BPME_Repository.PALNoonOutput import PALNoonOutput
from BPME_Repository.PALHPOutput import PALHPOutput
from BPME_Repository.PALMEOutput import PALMEOutput
from BPME_Repository.PALAEOutput import PALAEOutput
from BPME_Repository.PALRefOutput import PALRefOutput
from Bulk_Api.NDLSaveHistoricalSRQHeader import NDLSaveHistoricalSRQHeader
from Bulk_Api.NDLGetDataSyncInProgressHSRQ import NDLGetDataSyncInProgressHSRQ
from Bulk_Api.NDLSaveBulkLogInput import NDLSaveBulkLogInput
from Bulk_Api.NDLUpdateHistoricalSRQLogStatus import NDLUpdateHistoricalSRQLogStatus
from Bulk_Api.NDLUpdateBulkSRQHeaderStatus import NDLUpdateBulkSRQHeaderStatus
from config import SaveHistoricalSRQHeader,GetDataSyncInProgressHSRQ,SaveHSRQLogs,UpdateHistoricalSRQLogStatus,UpdateBulkSRQHeaderStatus,UnprocessedOnly
from datetime import datetime
from config import AuthUser,AuthPW
from config import DataPath
from config import mail_sender,sender_pw,em_list,cc_list
from ErrorMail.mkdirr import mkdirr
from ErrorMail.email_df import email_df
from pathlib import Path
from config import  melog,noonlog,hplog

current_time = datetime.now()
toDate=current_time.strftime('%m/%d/%Y')

try:


    start_time = time.time()
    BPMEngineVersion='2.02'
    headers = ast.literal_eval(mariHeaders)
    NaviHeaders= ast.literal_eval(NaviHeaders)
    er_file,main_fld=mkdirr(DataPath)
    
    refdfOutput= main_fld+r"\Refdata\rfdf.csv"
    if Path(refdfOutput).is_file():
        rf_df=pd.read_csv(refdfOutput)
    else:
        rf_df=pd.DataFrame(columns = ['imo','ref','ser'])
    rf_df.to_csv(refdfOutput, index=False)

    SRQ={}
    SRQ['BPMEVersion']=BPMEngineVersion
    SRQST1=[]

    #Invoking Prelimenary ApI for  fetching IMO and LogType
    records_df1,SRQST1 = PendingReports_Bulk(mariBaseUrl,maripendingUrl,headers,mariclientCrt,mariclientKey,start_time,SRQST1)
      #Applying Vessel Filter
    if vessel == '':
        pass
    else:
        records_df1 = VesselFilter(records_df1,vessel)

     
    # NDLSaveHistoricalSRQHeader to update header
    SRQST1=NDLSaveHistoricalSRQHeader(naviBaseUrl,NaviHeaders,SaveHistoricalSRQHeader,records_df1,start_time,SRQST1)
    # GetDataSyncInProgressHSRQ to get records to process
    records_df1,SRQST1=NDLGetDataSyncInProgressHSRQ(naviBaseUrl,GetDataSyncInProgressHSRQ,start_time,SRQST1,AuthUser,AuthPW)
    #Invoking all ServiceData APIs for each row of records_df
    #RecordData =records_df1.groupby(records_df1.imo)
    RecordData =records_df1.groupby(['imo','groupID'])
    #for i,v in records_df.iterrows():
    for rd in RecordData:
        SRQST = SRQST1.copy()
        records_df=rd[1]
        ref_imo = records_df.imo.unique()
        grp_id = records_df.groupID.unique()
        
        
        try:
            
            #Invoking PAL Reference ApI for all IMOs
            records_df1,SRQST  = PALRefApi(mariBaseUrl,mariGetRef,headers,mariclientCrt,mariclientKey,records_df,start_time,SRQST)
            #Loading Reference Data and Extracting NME from Reference Data for all IMOs
            records_df,SRQST = LoadReferenceData(records_df1,start_time,SRQST)
            # Check if complex propulsion chain exist and go to next vessel
            
            records_df_filter = records_df.drop_duplicates(subset = ["imo"])
            records_df_filter.reset_index(drop=True,inplace=True)
            if isinstance(records_df, pd.DataFrame):
                ref=records_df_filter['rawref'][0]
            else:
                ref='empty'
    
            icp,SRQST=isComplexPropulsion(records_df_filter['RDSraw'][0],start_time,SRQST)
            if icp:
                print('Complex propulsion chain!')
            # Define reference curves
            records_df_filter,SRQST= ReferenceCurves(records_df_filter,start_time,SRQST)
            #RefDashboards(RDS)
            records_df_filter,SRQST=RefDashboards(records_df_filter,start_time,SRQST)
            mismatched_imo=records_df_filter['imo'].tolist()

            SRQST=SaveRefApI(naviBaseUrl,saveReference,mismatched_imo,records_df_filter,start_time,SRQST,AuthUser,AuthPW)
            #Passing Data to MariApps
            ###ucs
            SRQST = PALRefOutput(mariBaseUrl,mariRefOut,headers,mariclientCrt,mariclientKey,records_df_filter,start_time,SRQST)
            if not(ref=="empty"):
                ref_stat=-1
                for stat in SRQST:
                    if stat['Stage'] in ['BPME_Transform.ReferenceCurves','BPME_Core.RefDashboards']:
                        if stat['Remarks']=="failed":
                            ref_stat=0
            if not(ref=="empty") and (ref_stat==0):
                rf_df  = rf_df.append({'imo': int(ref_imo) ,'ref': ref}, ignore_index=True)
                refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                rf_df.to_csv(refdfOutput, index=False)
            ###ucE
            records_df1 = records_df.merge(records_df_filter,on='imo',how='right')
            records_df2 = records_df1.drop(['logtype_y','nME_y','EngineIndex_y','nAE_y','AEEngineIndex_y','naviRef_x','RDS_x','RefOut_x','rawref_x','RDSraw_x','LogCount_x', 'groupID_x'],axis=1)
            records_df2.rename(columns = {'logtype_x':'logtype', 'nME_x':'nME','nAE_x':'nAE','AEEngineIndex_x':'AEEngineIndex',
                                       'RDS_y':'RDS','RefOut_y':'RefOut','EngineIndex_x':'EngineIndex','rawref_y':'rawref','naviRef_y':'naviRef','RDSraw_y':'RDSraw','LogCount_y':'LogCount', 'groupID_y':'groupID'}, inplace = True)
    
            #records_df=records_df.iloc[0]
            NumLog=[]
    
            df_len=records_df2.shape[0]
            for i , records_df in records_df2.iterrows():
            
    
                try:
                    #Processing for Noon Service Data
                    if records_df['logtype'] =='Noon':
                        noondfOutput= main_fld+r"\Noon\noondf.csv"
                        if Path(noondfOutput).is_file():
                            noon_df=pd.read_csv(noondfOutput)
                        else:
                            noon_df=pd.DataFrame(columns = ['imo','ref','ser'])
                        noon_df.to_csv(noondfOutput, index=False)
                        noonimo=records_df['imo']
                        noonref=records_df['rawref']
                        EngineIndex="NULL"
                        SRQSTN=SRQST.copy()
                        SRQN=SRQ.copy()
                        SRQN['logtype']='Noon'
                        #Calling Mariapps API for Fetching Noon Service Data
                        ServiceDataRaw,SRQSTN = PALNoonApi(mariBaseUrl,mariGetNoon,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,start_time,SRQSTN)
                        noonser=ServiceDataRaw
                        #Loading Service Data into Dataframe For processing
                        ServiceData,SRQSTN = LoadServiceData(ServiceDataRaw,records_df['logtype'],start_time,SRQSTN,records_df['imo'])
                        print("Total Number of Noon LogID for IMO-{} to process-{} ".format(records_df['imo'],len(ServiceData)))
                         
                        if len(ServiceData)!=0:
                            
                            ChunkSize=int(ServiceData.shape[0]/500)+1
                            ChunkDf=np.array_split(ServiceData, ChunkSize)
                            SRQSTN=NDLSaveBulkLogInput(naviBaseUrl,SaveHSRQLogs,records_df,ServiceData,EngineIndex,start_time,SRQSTN,AuthUser,AuthPW)
    
                            #ServiceData =ServiceData.groupby(ServiceData.LogID)
                            #Passing Each LogID(row) into BPMEngineSingleVessel
                            for data in ChunkDf:
                                   
                                data=data.reset_index(drop=True)
                                SRQSTNSD=SRQSTN.copy()
                                 #Saving Noon Input into Navidium Database by Calling Navidium API

                                #Calling BPMEngine Single Vessel
                                NoonHeader=[]
                                NoonOutput,NoonDiagnostics,NoonOutputExt,SRQSTND=BPMEngineSingleVessel(records_df['logtype'],records_df['EngineIndex'],BPMEngineVersion,records_df['RDS'],data,SRQN,start_time,SRQSTNSD)
                                emailst=SRQSTND.copy()
                                NoonHeader.append(SRQSTND)
                                #Saving Noon Output into Navidium Database by Calling Navidim API
                                SRQSTND=NoonOutputApi(naviBaseUrl,saveNoonKPI,records_df['imo'],NoonOutput,NoonDiagnostics,NoonOutputExt,start_time,AuthUser,AuthPW)
                                NoonHeader.append(SRQSTND)
                                NoonOutput=pd.read_csv(io.StringIO(NoonOutput))
                                for colm in NoonOutput.columns:
                                
                                    if colm in ['SCOCME_set_1','SCOCME_set_2','SCOCME_set_3','SCOCME_set_4','SCOCME_set_5','SCOCME_set_6','SCOCME_set_7']:
                                        del NoonOutput[colm]
                                NoonOutput=NoonOutput.to_csv(index = False)
                                #Passing Noon Api to Mariapps
                                NoonHeader.append(PALNoonOutput(mariBaseUrl,mariNoonOut,headers,mariclientCrt,mariclientKey,NoonOutput,NoonDiagnostics,start_time))
                                if records_df['logtype']=='Noon':
                                    if not(noonser==''):
                                        noon_stat=-1
                                        if emailst['Remarks']=="failed":
                                            noon_stat=0
                                    if not(noonser=='') and (noon_stat==0):
                                        noon_df=pd.read_csv(noondfOutput)
                                        noon_df  = noon_df.append({'imo': int(noonimo) ,'ref': noonref,'ser': noonser}, ignore_index=True)
                                        # refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                                        noon_df.to_csv(noondfOutput, index=False)
                                #Saving Service Queue Request by Calling Navidium's Service Queue Request API
                            if (NoonHeader[-1]['Remarks']=="complete")and (NoonHeader[-2]['Remarks']=="complete") and (NoonHeader[-3]['Remarks']=="complete"):
                                UHSRQST=NDLUpdateHistoricalSRQLogStatus(naviBaseUrl,UpdateHistoricalSRQLogStatus,records_df,start_time,SRQST,"Completed",AuthUser,AuthPW)
                                NumLog.append(int(1))
                            else:
                                UHSRQST=NDLUpdateHistoricalSRQLogStatus(naviBaseUrl,UpdateHistoricalSRQLogStatus,records_df,start_time,SRQST,"Failed",AuthUser,AuthPW)
                                NumLog.append(int(0))
    
    
                        else:
                            print("No Noon Log to process for imo {}".format(records_df['imo']))
                            
            
                    #Processing for HP Service Data
                    elif records_df['logtype'] =='HP':
                        hpdfOutput= main_fld+r"\HP\hpdf.csv"
                        if Path(hpdfOutput).is_file():
                            hp_df=pd.read_csv(hpdfOutput)
                        else:
                            hp_df=pd.DataFrame(columns = ['imo','ref','ser'])
                        hp_df.to_csv(hpdfOutput, index=False)
                        hpimo=records_df['imo']
                        hpref=records_df['rawref']
                        EngineIndex = "NULL"
                        SRQSTH=SRQST.copy()
                        SRQH=SRQ.copy()
                        SRQH['logtype']='HP'
                        #Calling Mariapps API for Fetching HP Service Data
                        ServiceDataRaw,SRQSTH = PALHPApi(mariBaseUrl,mariGetHP,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,start_time,SRQSTH)
                        hpser=ServiceDataRaw
                        #Loading Service Data into Dataframe For processing
                        ServiceData,SRQSTH = LoadServiceData(ServiceDataRaw,records_df['logtype'],start_time,SRQSTH,records_df['imo'])
                        print("Total Number of HP LogID to process-{}".format(len(ServiceData)))
                        if len(ServiceData)!=0:
                            ChunkSize=int(ServiceData.shape[0]/500)+1
                            ChunkDf=np.array_split(ServiceData, ChunkSize)
                            # ServiceData =ServiceData.groupby(ServiceData.LogID)
                            #Passing Each LogID(row) into BPMEngineSingleVessel
                            SRQSTH=NDLSaveBulkLogInput(naviBaseUrl,SaveHSRQLogs,records_df,ServiceData,EngineIndex,start_time,SRQSTH,AuthUser,AuthPW)
                            for data in ChunkDf:
                                data=data.reset_index(drop=True)
                                SRQSTHSD=SRQSTH.copy()
                                #Saving HP Input into Navidium Database by Calling Navidium API
                                #SRQSTHSD=SaveHPInput(naviBaseUrl,saveHP,records_df['imo'],data[1],start_time,SRQSTHSD)
                                
                                 #Calling BPMEngine Single Vessel
                                HPHeader=[]
                                HPOutput,HPDiagnostics,HPOutputExt,SRQSTHD=BPMEngineSingleVessel(records_df['logtype'],records_df['EngineIndex'],BPMEngineVersion,records_df['RDS'],data,SRQH,start_time,SRQSTHSD)
                                emailst=SRQSTHD.copy()
                                HPHeader.append(SRQSTHD)
                                #Saving HP Output into Navidium Database by Calling Navidim API
                                SRQSTHD=HPOutputApi(naviBaseUrl,saveHPKPI,records_df['imo'],HPOutput,HPDiagnostics,start_time,AuthUser,AuthPW)
                                HPHeader.append(SRQSTHD)
                                #Passing HP Output to Mariapps
                                HPHeader.append(PALHPOutput(mariBaseUrl,mariHPOut,headers,mariclientCrt,mariclientKey,HPOutput,HPDiagnostics,start_time))
                                if records_df['logtype']=='HP':
                                    if not(hpser==''):
                                        hp_stat=-1
                                        
                                        if emailst['Remarks']=="failed":
                                            hp_stat=0
                                    if not(hpser=='') and (hp_stat==0):
                                        
                                        hp_df=pd.read_csv(hpdfOutput)
                                        hp_df  = hp_df.append({'imo': int(hpimo) ,'ref': hpref,'ser': hpser}, ignore_index=True)
                                        # refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                                        hp_df.to_csv(hpdfOutput, index=False)
                                #Saving Service Queue Request by Calling Navidium's Service Queue Request API
                            if (HPHeader[-1]['Remarks']=="complete")and (HPHeader[-2]['Remarks']=="complete") and (HPHeader[-3]['Remarks']=="complete"):
                                UHSRQST=NDLUpdateHistoricalSRQLogStatus(naviBaseUrl,UpdateHistoricalSRQLogStatus,records_df,start_time,SRQST,"Completed",AuthUser,AuthPW)
                                NumLog.append(int(1))
                            else:
                                UHSRQST=NDLUpdateHistoricalSRQLogStatus(naviBaseUrl,UpdateHistoricalSRQLogStatus,records_df,start_time,SRQST,"Failed",AuthUser,AuthPW)
                                NumLog.append(int(0))
                        else:
                            print("No HP Log to process for imo {}".format(records_df['imo']))
    
                    #Processing for ME Service Data
                    
                    elif records_df['logtype'] =='ME':
                        medfOutput= main_fld+r"\ME\medf.csv"
                        if Path(medfOutput).is_file():
                            me_df=pd.read_csv(medfOutput)
                        else:
                            me_df=pd.DataFrame(columns = ['imo','ref','ser'])
                        me_df.to_csv(medfOutput, index=False)
                        meimo=records_df['imo']
                        meref=records_df['rawref']
                        mestat=0
                        SRQSTM=SRQST.copy()
                        SRQM=SRQ.copy()
                        SRQM['logtype']='ME'
                        for EngineNo in range(0,int(records_df['nME'])):
                            #Calling Mariapps API for Fetching ME Service Data
                            ServiceDataRaw,SRQSTM = PALMEApi(mariBaseUrl,mariGetME,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,EngineNo+1,start_time,SRQSTM)
                            meser=ServiceDataRaw
                            #Loading Service Data into Dataframe For processing
                            ServiceData,SRQSTM = LoadServiceData(ServiceDataRaw,records_df['logtype'],start_time,SRQSTM,records_df['imo'])
                            print("Total Number of ME LogID to process-{}".format(len(ServiceData)))
                            if len(ServiceData)!=0:
                                ChunkSize=int(ServiceData.shape[0]/500)+1
                                ChunkDf=np.array_split(ServiceData, ChunkSize)
                                SRQSTM=NDLSaveBulkLogInput(naviBaseUrl,SaveHSRQLogs,records_df,ServiceData,EngineNo+1,start_time,SRQSTM,AuthUser,AuthPW)
                                #Passing Each LogID(row) into BPMEngineSingleVessel
                                for data in ChunkDf:
                                    SRQSTMSD=SRQSTM.copy()
                                    #Saving ME Input into Navidium Database by Calling Navidium API
                                    #SRQSTMSD=SaveMEInput(naviBaseUrl,saveME,records_df['imo'],data[1],EngineNo+1,start_time,SRQSTMSD)
                                    
                                    #Calling BPMEngine Single Vessel
                                    MEHeader=[]
                                    MEOutput,MEDiagnostics,MEOutputExt,SRQSTMD=BPMEngineSingleVessel(records_df['logtype'],EngineNo+1,BPMEngineVersion,records_df['RDS'],data,SRQM,start_time,SRQSTMSD)
                                    emailst=SRQSTMD.copy()
                                    MEHeader.append(SRQSTMD)
                                    #Saving ME Output into Navidium Database by Calling Navidim API
                                    SRQSTMD=MEOutputApi(naviBaseUrl,saveMEKPI,records_df['imo'],MEOutput,MEDiagnostics,EngineNo+1,start_time,AuthUser,AuthPW)
                                    MEHeader.append(SRQSTMD)
                                    #Pass ME output to PAL
                                    MEHeader.append(PALMEOutput(mariBaseUrl,mariMEOut,headers,mariclientCrt,mariclientKey,EngineNo+1,MEOutput,MEDiagnostics,start_time))
                                    #Saving Service Queue Request by Calling Navidium's Service Queue Request API
                                    if records_df['logtype']=='ME':
                                        if not(meser==''):
                                            me_stat=-1
                                            
                                            if emailst['Remarks']=="failed":
                                                me_stat=0
                                        if not(meser=='') and (me_stat==0):
                                            me_df=pd.read_csv(medfOutput)
                                            me_df  = me_df.append({'imo': int(meimo) ,'ref': meref,'ser': meser}, ignore_index=True)
                                            # refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                                            me_df.to_csv(medfOutput, index=False)
                                if (MEHeader[-1]['Remarks']=="complete")and (MEHeader[-2]['Remarks']=="complete") and (MEHeader[-3]['Remarks']=="complete"):
                                    UHSRQST=NDLUpdateHistoricalSRQLogStatus(naviBaseUrl,UpdateHistoricalSRQLogStatus,records_df,start_time,SRQST,"Completed",AuthUser,AuthPW)
                                    mestat=mestat+1
                                    #NumLog.append(int(1))
                                else:
                                    UHSRQST=NDLUpdateHistoricalSRQLogStatus(naviBaseUrl,UpdateHistoricalSRQLogStatus,records_df,start_time,SRQST,"Failed",AuthUser,AuthPW)
                                    #NumLog.append(int(0))
                        
                            else:
                                print("No ME Log to process for imo {}".format(records_df['imo']))
                                #NumLog.append(int(0))
                        if records_df['nME']==mestat:
                            NumLog.append(int(1))
                        else:
                            NumLog.append(int(0))
                            
                            
                        
    #                 elif records_df['logtype'] =='AE':
    #                     SRQSTA=SRQST.copy()
    #                     SRQA=SRQ.copy()
    #                     SRQA['logtype']='AE'
    #                     for EngineNo in range(0,int(records_df['nAE'])):
    #                         #Calling Mariapps API for Fetching AE Service Data
    #                         ServiceDataRaw,SRQSTA = PALAEApi(mariBaseUrl,mariGetME,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,EngineNo+1,start_time,SRQSTA)
    #                         #Loading Service Data into Dataframe For processing
    #                         ServiceData,SRQSTA = LoadServiceData(ServiceDataRaw,records_df['logtype'],start_time,SRQSTA,records_df['imo'])
    #                         print("Total Number of AE LogID to process-{}".format(len(ServiceData)))
    #                         if len(ServiceData)!=0:
    #                             ChunkSize=int(ServiceData.shape[0]/500)+1
    #                             ChunkDf=np.array_split(ServiceData, ChunkSize)
    
    #                             #Passing Each LogID(row) into BPMEngineSingleVessel
    #                             for data in ChunkDf:
    #                                 SRQSTASD=SRQSTA.copy()
    #                                 #Saving AE Input into Navidium Database by Calling Navidium API
    #                                 #SRQSTASD=SaveAEInput(naviBaseUrl,saveME,records_df['imo'],data[1],start_time,SRQSTASD)
    #                                 SRQSTASD=NDLSaveBulkLogInput(naviBaseUrl,SaveHSRQLogs,records_df,data,EngineNo+1,start_time,SRQSTASD)
    #                                 #Calling BPMEngine Single Vessel
    #                                 AEHeader=[]
    #                                 AEOutput,AEDiagnostics,AEOutputExt,SRQSTAD=BPMEngineSingleVessel(records_df['logtype'],EngineNo+1,BPMEngineVersion,records_df['RDS'],data[1],SRQM,start_time,SRQSTASD)
    #                                 AEHeader.append(SRQSTAD)
    #                                 #Saving AE Output into Navidium Database by Calling Navidim API
    #                                 SRQSTAD=AEOutputApi(naviBaseUrl,saveMEKPI,records_df['imo'],AEOutput,AEDiagnostics,EngineNo+1,start_time)
    #                                 AEHeader.append(SRQSTAD)
    #                                 #Pass AE output to PAL
    #                                 AEHeader.append(PALAEOutput(mariBaseUrl,mariMEOut,headers,mariclientCrt,mariclientKey,EngineNo+1,AEOutput,AEDiagnostics,start_time))
    #                                 #Saving Service Queue Request by Calling Navidium's Service Queue Request API
    #                                 if (AEHeader[-1]['Remarks']=="complete")or (AEHeader[-2]['Remarks']=="complete") or (AEHeader[-3]['Remarks']=="complete"):
    #                                     UHSRQST=NDLUpdateHistoricalSRQLogStatus(naviBaseUrl,UpdateHistoricalSRQLogStatus,records_df,start_time,SRQST,"Completed")
    #                                     NumLog.append(int(1))
    #                                 else:
    #                                     UHSRQST=NDLUpdateHistoricalSRQLogStatus(naviBaseUrl,UpdateHistoricalSRQLogStatus,records_df,start_time,SRQST,"Failed")
    #                                     NumLog.append(int(0))
    #                         else:
    #                             print("No AE Log to process for imo {}".format(records_df['imo']))
        
                except:
                    logger.error("Error in BPMEngine for imo {} and logtype {}".format(records_df['imo'],records_df['logtype']),exc_info=True,stack_info =True)
                    UHSRQST=NDLUpdateHistoricalSRQLogStatus(naviBaseUrl,UpdateHistoricalSRQLogStatus,records_df,start_time,SRQST,"Failed",AuthUser,AuthPW)
                    NumLog.append(int(0))
                    if records_df['logtype']=='Noon':
                        if not(noonser==''):
                            noon_stat=-1
                            
                            if emailst['Remarks']=="failed":
                                noon_stat=0
                        if not(noonser=='') and (noon_stat==0):
                            noon_df=pd.read_csv(noondfOutput)
                            noon_df  = noon_df.append({'imo': int(noonimo) ,'ref': noonref,'ser': noonser}, ignore_index=True)
                            # refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                            noon_df.to_csv(noondfOutput, index=False)
                    if records_df['logtype']=='HP':
                        if not(hpser==''):
                            hp_stat=-1
                            
                            if emailst['Remarks']=="failed":
                                hp_stat=0
                        if not(hpser=='') and (hp_stat==0):
                            hp_df=pd.read_csv(hpdfOutput)
                            hp_df  = hp_df.append({'imo': int(hpimo) ,'ref': hpref,'ser': hpser}, ignore_index=True)
                            # refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                            hp_df.to_csv(hpdfOutput, index=False)
                    if records_df['logtype']=='ME':
                        if not(meser==''):
                            me_stat=-1
                            
                            if emailst['Remarks']=="failed":
                                me_stat=0
                        if not(meser=='') and (me_stat==0):
                            me_df=pd.read_csv(medfOutput)
                            me_df  = me_df.append({'imo': int(meimo) ,'ref': meref,'ser': meser}, ignore_index=True)
                            # refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                            me_df.to_csv(medfOutput, index=False)
                    pass
            if (sum(NumLog)==df_len) and (df_len>0):
                UpdateBulkSRQST=NDLUpdateBulkSRQHeaderStatus(naviBaseUrl,UpdateBulkSRQHeaderStatus,records_df['imo'],records_df['groupID'],start_time,SRQST,"completed",AuthUser,AuthPW)
            else:
                UpdateBulkSRQST=NDLUpdateBulkSRQHeaderStatus(naviBaseUrl,UpdateBulkSRQHeaderStatus,records_df['imo'],records_df['groupID'],start_time,SRQST,"failed",AuthUser,AuthPW)
                
        except Exception as er:
            logger.error("Error in BPMEngine {} for imo-{} ".format(er,ref_imo),exc_info=True,stack_info =True)
            UpdateBulkSRQST=NDLUpdateBulkSRQHeaderStatus(naviBaseUrl,UpdateBulkSRQHeaderStatus,ref_imo,grp_id,start_time,SRQST,"failed",AuthUser,AuthPW)
            pass
            
except Exception as err:

    logger.error("Error in BPMEngine {} ".format(err),exc_info=True,stack_info =True)
else:
    try:
        
        em_list=ast.literal_eval(em_list)
        cc_list=ast.literal_eval(cc_list)
        email_df(main_fld, DataPath,em_list,cc_list,mail_sender,sender_pw,noonlog,hplog,melog)
    except Exception as err:
        logger.error("Error in sending mail {} ".format(err),exc_info=True)
        print('email failed')
    else:
        print("BPM Engine 2.02  Execution Completed Successfully")
    


