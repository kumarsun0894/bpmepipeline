import json
import logging.config
import os
"""
@author: Padma Kurup
"""

from config import log_dir
def setup_logging(default_path='log_config.txt', default_level=logging.INFO):
	# change the path variable to the location where log_config.txt is stored
	script_dir = log_dir+"\\log_config.txt"

	if (os.path.exists(script_dir)):
		with open(script_dir, 'rt') as f:
			config = json.load(f)
		logging.config.dictConfig(config)
	else:
		logging.basicConfig(level=default_level)

