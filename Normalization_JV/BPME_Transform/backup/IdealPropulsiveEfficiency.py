# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 12:04:41 2020

@author: akhil.surendran
"""
import numpy as np
import pandas as pd
from BPME_Common import LastElement as LE
from scipy.interpolate import pchip

def IdealPropulsiveEfficiency(U,T,etaDHid,Tref,etaDref,Uref,Tb,Ts):
    etaDid= np.empty(np.shape(U))

    for i  in range(0,len(U)):
        etaDid[i]=IdealPropulsiveEfficiencyScalar(U[i],T[i],etaDHid[i],Tref,etaDref,Uref,Tb,Ts)
    return  etaDid   


def IdealPropulsiveEfficiencyScalar(U,T,etaDHid,Tref,etaDref,Uref,Tb,Ts):
    if np.all(pd.isnull(T)):
        print("T is unavailable!") #diagnostic
        eetaDid=np.nan
        return
    elif T<Tb-0.01 or T>Ts+0.01:
        print("T is out of [Tb,Ts] range!") #diagnostic
        eetaDid=np.nan
        return
   
    if T>=min(Tref) and T<=max(Tref):
        [imax,_]=np.shape(Uref)
        last=LE.LastElement(Uref)
        etaD= np.empty(np.shape(Tref))
        etaD[:] = np.nan
        if np.any(~np.isnan(etaDref)):
#            etaD= np.empty(np.shape(Tref))
#            etaD[:] = np.nan
            for i in range(0,imax):
                if U<Uref[i,0]:
                    etaD[i]=np.mean(etaDref[i,0:int(round(last[i]/2))])
                elif U>=Uref[i,0] and U<=Uref[i,int(last[i])]:
                    pchipfn = pchip(Uref[i,0:int(last[i])],etaDref[i,0:int(last[i])])
                    etaD[i]= pchipfn(U)
                elif U>Uref[i,int(last[i])]:
                    etaD[i]=np.mean(etaDref[i,int(round(last[i]/2)):int(last[i])])
                else:
                    print('IdealPropulsiveEfficiency error!') #diagnostic
            if imax>1:
                 for i in range(0,imax-1):
                     if T>=Tref[i] and T<=Tref[i+1]:
                         eetaDid=(T-Tref[i])*(etaD[i+1]-etaD[i])/(Tref[i+1]-Tref[i])+etaD[i]
            else:
                 eetaDid=etaD
                 
        elif np.any(np.isnan(etaDref)):
            eetaDid=0.7 # ISO19030
        else:
            print('etaDid error!')
            eetaDid=np.nan
            
    elif T<min(Tref) or T>max(Tref):
        print("T is out of model test range! Holtrop method will be used!")
        eetaDid=etaDHid
        
    return eetaDid
           
       

#U =np.array([12.5, np.nan, np.nan,np.nan, 11.2, 10.8, 10.5, 10.3, np.nan, np.nan]) 
#T = np.array([11.35, 11.35, np.nan, np.nan, 10.7, 11.21, 11.21, 10.7, 11.35, np.nan])#,11.025,11.025,np.nan,10.81,10.81,10.645])    
#etaDHid = np.array([1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7])#,1.7,1.7,1.7]) 
#Tref = np.array([ 7.45,12,13.9])
#etaDref = np.array([[0.819,0.821,0.822,0.823,0.822,0.818,0.814,0.81],
#                    [0.776,0.78,0.782,0.781,0.78,0.778,0.774,0.771],
#                    [0.776,0.776,0.774,0.771,0.769,0.767,0.76,np.nan]])
#Uref = np.array([[ 1,18,19,20,21,22,23,24],
#                 [16,18,20,21,21.5,22,23,24],
#                 [16,18,20,21,21.5,22,23,np.nan]])
#Ts=  13.9
#Tb=  7.45
#rslt = IdealPropulsiveEfficiency(U,T,etaDHid,Tref,etaDref,Uref,Tb,Ts) 
#print(rslt)      
#    
#                