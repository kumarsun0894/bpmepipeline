# -*- coding: utf-8 -*- 
"""
Created on Fri Apr 10 12:36:21 2020

@author: Subhin Antony

% ManipulateFuelData
% Description:  This function 
%
% Input:        SDS
%               LogType
%
% Output:       SDS
%
% See also:     BPMEngineSingleVessel

"""

import pandas as pd
from ExceptionHandler import ExceptionHandler
from BPME_Common import nansumwrapper as nsw
from BPME_Common import VolumeCorrectionFactor as vcf
from BPME_Transform import LCVEstimator as lcv
import numpy as np
import sys
import time 
import logging 
# create logger
logger = logging.getLogger('BPMEngine.2.02')
def ManipulateFuelData(IMO,SDS,RDS,DS,LogType,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        #Check LogType
        if 'HP' in LogType or 'ME' in LogType :
            
            SDS['rhoFOcorME']=(SDS['rhoFOME_an']-1.1)*vcf.VolumeCorrectionFactor(SDS['rhoFOME_an'],SDS['TFOFMinME_avg']) #FO density for temperature in [kg/m3] (new correction)
            #Define mass FO consumption based on either a mass or a volumetric flow meter measurement
            SDS['mFOCME_dCnt'][pd.isnull(SDS['mFOCME_dCnt'])]=SDS['VFOCME_dCnt'][pd.isnull(SDS['mFOCME_dCnt'])]*SDS['rhoFOcorME'][pd.isnull(SDS['mFOCME_dCnt'])]
    
            #LCV estimation
            SDS['LCVFOME_an'][pd.isnull(SDS['LCVFOME_an'])]=lcv.LCVEstimator(SDS['FOGME'][pd.isnull(SDS['LCVFOME_an'])])
        
            #Define variable if not exported in csv
            if 'LCVGOME_an' not in SDS:
                SDS['LCVGOME_an']=np.empty(SDS['pambairER'].shape)
                SDS['LCVGOME_an'][:] = np.nan
        
            #Estimate LCV for DF engines
            if 'Yes' in RDS['DualFuelME']:
                SDS['LCVGOME_an'][pd.isnull(SDS['LCVGOME_an'])]=48 #MJ/kg        
    
            #Indicate logs with missing FO temperature when only VFOC was provided but not mFOC
            DS['MissingFOTemperatureME']=np.zeros(SDS['mFOCME_dCnt'].shape)
            TFOMEcheck=np.flatnonzero(np.logical_and(np.logical_and(pd.isnull(SDS['TFOFMinME_avg']),pd.isnull(SDS['mFOCME_dCnt'])) , ~pd.isnull(SDS['VFOCME_dCnt'])))
            if len(TFOMEcheck)>0:
                DS['MissingFOTemperatureME'][TFOMEcheck]=1
                print('FO temperature error!')
        
        
            #Indicate logs with missing FOC
            DS['MissingFOCME']=np.zeros(SDS['mFOCME_dCnt'].shape)
            FOCMEcheck=np.flatnonzero(pd.isnull(SDS['mFOCME_dCnt']))
            if len(FOCMEcheck)>0:
                DS['MissingFOCME'][FOCMEcheck]=1
                print('FOC is missing!')
            # return[SDS,DS]
    
        if 'AE' in LogType:
            #Calculated corrected FO density
            #Density in vacuum subtracted by 1.1kg/m3 is known as density in air, see:
            #http://www.chemicaltankerguide.com/cargo-calculation.html
            #http://www.bunkering.co.kr/bunker_spec/density_1.htm
            SDS['rhoFOcorAE']=(SDS['rhoFOAE_an']-1.1)*vcf.VolumeCorrectionFactor(SDS['rhoFOAE_an'],SDS['TFOFMinAE_avg'])
            
            #Define mass FO consumption based on either a mass or a volumetric flow meter measurement
            SDS['mFOCAE_dCnt'][pd.isnull(SDS['mFOCAE_dCnt'])]=SDS['VFOCAE_dCnt'][pd.isnull(SDS['mFOCAE_dCnt'])]*SDS['rhoFOcorAE'][pd.isnull(SDS['mFOCAE_dCnt'])]
            
            #LCV estimation
            SDS['LCVFOAE_an'][pd.isnull(SDS['LCVFOAE_an'])]=lcv.LCVEstimator(SDS['FOGAE'][pd.isnull(SDS['LCVFOAE_an'])])
            
            #Define variable if not exported in csv
            if 'LCVGOAE_an' not in SDS:
                SDS['LCVGOAE_an']=np.empty(SDS['pambairER'].shape)
                SDS['LCVGOAE_an'][:] = np.nan
            #Estimate LCV for DF engines
            if 'Yes' in RDS['DualFuelAE']:
                SDS['LCVGOAE_an'][pd.isnull(SDS['LCVGOAE_an'])]=48 #MJ/kg
                
            #Indicate logs with missing FO temperature when only VFOC was provided but not mFOC
            DS['MissingFOTemperatureAE']=np.zeros(SDS['mFOCAE_dCnt'].shape)
            TFOAEcheck=np.flatnonzero(np.logical_and(np.logical_and(pd.isnull(SDS['TFOFMinAE_avg']),pd.isnull(SDS['mFOCAE_dCnt'])) ,~pd.isnull(SDS['VFOCAE_dCnt'])))
            if len(TFOAEcheck)>0:
                DS['MissingFOTemperatureAE'][TFOAEcheck]=1
                print('AE FO temperature error!')
                
            #Indicate logs with missing FOC
            DS['MissingFOCAE']=np.zeros(SDS['mFOCAE_dCnt'].shape)
            FOCAEcheck=np.flatnonzero(pd.isnull(SDS['mFOCAE_dCnt']))
            if len(FOCAEcheck)>0:
                DS['MissingFOCAE'][FOCAEcheck]=1
                print('AE FOC is missing!')
    
            # return[SDS,DS]
    
        if 'Auto' in LogType:
            #Calculated corrected FO density
            #Density in vacuum subtracted by 1.1kg/m3 is known as density in air, see:
            #http://www.chemicaltankerguide.com/cargo-calculation.html
            #http://www.bunkering.co.kr/bunker_spec/density_1.htm
            SDS['rhoFOcorME']=(SDS['rhoFOME_an']-1.1)*vcf.VolumeCorrectionFactor(SDS['rhoFOME_an'],SDS['TFOFMinME_avg_auto']) #FO density for temperature in [kg/m3] (new correction)
    
            #Define mass FO consumption based on either a mass or a volumetric flow meter measurement
            SDS['mFOCME_dCnt_auto'][pd.isnull(SDS['mFOCME_dCnt_auto'])]=SDS['VFOCME_dCnt_auto'][pd.isnull(SDS['mFOCME_dCnt_auto'])]*SDS['rhoFOcorME'][pd.isnull(SDS['mFOCME_dCnt_auto'])]
        
            # return[SDS,DS]
    
    
        if 'Noon' in LogType:
            if np.any(SDS['nFOGME']>0):
                #In case of multiple fuel grades used within a log duration (changeover), the data must be checked
                #Check if NaNs are properly distributed in ME FO properties arrays
                for k in range(0,int(len(SDS['nFOGME']))):
                    if SDS['nFOGME'][k]<np.max(SDS['nFOGME']):
                        for l in range(int(SDS['nFOGME'][k]),int(np.max(SDS['nFOGME']))):
                        
                            #FOG check
                            if ~(pd.isnull(SDS['FOGgME'][k,l])):
                                SDS['FOGgME'][k,l]=np.nan
                                print('FOG nan error!')
                        
                            #V check
                            if ~(pd.isnull(SDS['VFOCgME'][k,l])):
                                SDS['VFOCgME'][k,l]=np.NaN
                                print('VFOC nan error!')
                       
                            #m check
                            if ~(pd.isnull(SDS['mFOCgME'][k,l])):
                                SDS['mFOCgME'][k,l]=np.nan
                                print('mFOC nan error!')
                        
                            #T check
                            if ~(pd.isnull(SDS['TFOFMingME'][k,l])):
                                SDS['TFOFMingME'][k,l]=np.nan
                                print('TFOFMin nan error!')
                        
                            #v check
                            if ~(pd.isnull(SDS['vFOgME'][k,l])):
                                SDS['vFOgME'][k,l]=np.nan
                                print('vFO nan error!')
                        
                            #rho check
                            if ~(pd.isnull(SDS['rhoFOgME'][k,l])):
                                SDS['rhoFOgME'][k,l]=np.nan
                                print('rho nan error!')
                       
                            #LCV check
                            if ~(pd.isnull(SDS['LCVFOgME'][k,l])):
                                SDS['LCVFOgME'][k,l]=np.nan
                                print('LCV nan error!')
                       
                            #sul check
                            if ~(pd.isnull(SDS['SulFOgME'][k,l])):
                                SDS['SulFOgME'][k,l]=np.nan
                                print('sul nan error!')
                        
         
                #Estimate missing FO properties
                for k in range(0,int(len(SDS['nFOGME']))):
                    for l in range(0,int(SDS['nFOGME'][k])):
        
                        #FOG estimation
                        if pd.isnull(SDS['FOGgME'][k,l]):
                            #FOGgME(k,l)=NaNmust define estimation rule
                            print('FOG unavailable!')
                    
                        #V estimation
                        if pd.isnull(SDS['VFOCgME'][k,l]):
                            #VFOCgME(k,l)=NaNmust define estimation rule
                            print('VFOC unavailable!')
                    
                        #m estimation
                        if pd.isnull(SDS['mFOCgME'][k,l]):
                            #mFOCgME(k,l)=NaNmust define estimation rule
                            print('mFOC unavailable!')
                    
                        #T estimation
                        if pd.isnull(SDS['TFOFMingME'][k,l]):
                            #TFOFMingME(k,l)=NaNmust define estimation rule
                            print('TFOFMin unavailable!')
                   
                        #v estimation
                        if pd.isnull(SDS['vFOgME'][k,l]):
                            #vFOgME(k,l)=NaNmust define estimation rule
                            print('vFO unavailable!')
                   
                        #rho estimation
                        if pd.isnull(SDS['rhoFOgME'][k,l]):
                            #rhoFOgME(k,l)=NaNmust define estimation rule
                            print('rho unavailable!')
                   
                        #LCV estimation
                        #RMA-1 RMB-2 RMD-3 RME-4 RMG-5 RMK-6 DMA-7 DMB-8 DMX-9 DMZ-10 LNG-11 LPG PROPANE-12 LPG BUTANE-13
                        if pd.isnull(SDS['LCVFOgME'][k,l]):
                            SDS['LCVFOgME'][k,l]=lcv.LCVEstimator(np.array(SDS['FOGgME'][k,l]))
                        
                        #print('LCV unavailable!')
                   
                        #sul estimation
                        if pd.isnull(SDS['SulFOgME'][k,l]):
                            #sulFOgME(k,l)=NaNmust define estimation rule
                            print('sul unavailable!')
                 
                    #Calculated corrected FO density
                    #Density in vacuum subtracted by 1.1kg/m3 is known as density in air, see:
                    #http://www.chemicaltankerguide.com/cargo-calculation.html
                    #http://www.bunkering.co.kr/bunker_spec/density_1.htm
                    SDS['rhoFOcorgME']=(SDS['rhoFOgME']-1.1)*vcf.VolumeCorrectionFactor(SDS['rhoFOgME'],SDS['TFOFMingME']) #FO density for temperature in [kg/m3] (new correction)
                    #Define mass FO consumption based on either a mass or a volumetric flow meter measurement
                    SDS['mFOCgME'][pd.isnull(SDS['mFOCgME'])]=SDS['VFOCgME'][pd.isnull(SDS['mFOCgME'])]*SDS['rhoFOcorgME'][pd.isnull(SDS['mFOCgME'])]       
            
            #end---
            if 'LCVGOME_an' not in SDS:
                SDS['LCVGOME_an']=np.empty(SDS['pambairER'].shape)
                SDS['LCVGOME_an'][:] = np.nan
            #Estimate LCV for DF engines
            if 'Yes' in RDS['DualFuelME']:
                SDS['LCVGOME_an'][pd.isnull(SDS['LCVGOME_an'])]=48 #MJ/kg        
        
            
            #Calculate the FO properties weighted average for the logs where nFOG>1
            if np.max(SDS['nFOGME'])>=1:
                SDS['mFOCME']=nsw.nansumwrapper(SDS['mFOCgME'],axis=1)
                SDS['VFOCME']=nsw.nansumwrapper(SDS['VFOCgME'],axis=1)
               
                SDS['TFOFMinME']=np.divide(nsw.nansumwrapper(SDS['mFOCgME']*SDS['TFOFMingME'],axis=1),nsw.nansumwrapper(SDS['mFOCgME'],axis=1), out=np.zeros_like(nsw.nansumwrapper(SDS['mFOCgME']*SDS['TFOFMingME'],axis=1)), where=nsw.nansumwrapper(SDS['mFOCgME'],axis=1)!=0)
                SDS['vFOME']=np.divide(nsw.nansumwrapper(SDS['mFOCgME']*SDS['vFOgME'],axis=1),nsw.nansumwrapper(SDS['mFOCgME'],axis=1), out=np.zeros_like(nsw.nansumwrapper(SDS['mFOCgME']*SDS['vFOgME'],axis=1)), where=nsw.nansumwrapper(SDS['mFOCgME'],axis=1)!=0)
                SDS['rhoFOME']=np.divide(nsw.nansumwrapper(SDS['mFOCgME']*SDS['rhoFOgME'],axis=1),nsw.nansumwrapper(SDS['mFOCgME'],axis=1), out=np.zeros_like(nsw.nansumwrapper(SDS['mFOCgME']*SDS['rhoFOgME'],axis=1)), where=nsw.nansumwrapper(SDS['mFOCgME'],axis=1)!=0)
                SDS['LCVFOME']=np.divide(nsw.nansumwrapper(SDS['mFOCgME']*SDS['LCVFOgME'],axis=1),nsw.nansumwrapper(SDS['mFOCgME'],axis=1), out=np.zeros_like(nsw.nansumwrapper(SDS['mFOCgME']*SDS['LCVFOgME'],axis=1)), where=nsw.nansumwrapper(SDS['mFOCgME'],axis=1)!=0)
                SDS['SulFOME']=np.divide(nsw.nansumwrapper(SDS['mFOCgME']*SDS['SulFOgME'],axis=1),nsw.nansumwrapper(SDS['mFOCgME'],axis=1), out=np.zeros_like(nsw.nansumwrapper(SDS['mFOCgME']*SDS['SulFOgME'],axis=1)), where=nsw.nansumwrapper(SDS['mFOCgME'],axis=1)!=0)
            else:
                #This is to cover noon at port only logs. In that case there is no
                #FOC info.
                SDS['mFOCME']=np.empty(np.shape(SDS['nFOGME']))
                SDS['mFOCME'][:]=np.nan
                SDS['VFOCME']=np.empty(np.shape(SDS['nFOGME']))
                SDS['VFOCME'][:]=np.nan
                SDS['TFOFMinME']=np.empty(np.shape(SDS['nFOGME']))
                SDS['TFOFMinME'][:]=np.nan
                SDS['vFOME']=np.empty(np.shape(SDS['nFOGME']))
                SDS['vFOME'][:]=np.nan
                SDS['rhoFOME']=np.empty(np.shape(SDS['nFOGME']))
                SDS['rhoFOME'][:]=np.nan
                SDS['LCVFOME']=np.empty(np.shape(SDS['nFOGME']))
                SDS['LCVFOME'][:]=np.nan
                SDS['SulFOME']=np.empty(np.shape(SDS['nFOGME']))
                SDS['SulFOME'][:]=np.nan
            
        ##########################################################################################    
            
            if np.any(SDS['nFOGME']>0):
            #Remove per change-over info for ME
                del SDS['mFOCgME']
                del SDS['VFOCgME']
                del SDS['TFOFMingME']
                del SDS['vFOgME']
                del SDS['rhoFOgME']
                del SDS['LCVFOgME']
                del SDS['SulFOgME']
        
        
            #Indicate logs with missing FO temperature when only VFOC was provided but not mFOC
            DS['MissingFOTemperatureME']=np.zeros(SDS['mFOCME'].shape)
            TFOMEcheck=((pd.isnull(SDS['TFOFMinME'])) &(pd.isnull(SDS['mFOCME'])) & (~pd.isnull(SDS['VFOCME'])))
    #       TFOMEcheck=np.flatnonzero(np.logical_and(np.logical_and(pd.isnull(SDS['TFOFMinME']),pd.isnull(SDS['mFOCME'])) ,~pd.isnull(SDS['VFOCME'])))
            if np.any(TFOMEcheck):
                DS['MissingFOTemperatureME'][TFOMEcheck]=1
                print('FO temperature error!')
        
            #Indicate logs with missing FOC
            DS['MissingFOCME']=np.zeros(SDS['mFOCME'].shape)
            FOCMEcheck=np.flatnonzero(pd.isnull(SDS['mFOCME']))
            if len(FOCMEcheck):
                DS['MissingFOCME'][FOCMEcheck]=1
                print('FOC is missing!')
        
        
            #Calculate the AE FO properties weighted average for the logs where nFOG>1
            if np.max(SDS['nFOGAE'])>=1:
                SDS['mFOCAE']=nsw.nansumwrapper(SDS['mFOCgAE'],axis=1)
                #SDS.VFOCAE(:,i)=nansum(SDS.VFOCgAE(:,i,:),3);
                SDS['TFOFMinAE']=np.divide(nsw.nansumwrapper(SDS['mFOCgAE']*SDS['TFOFMingAE'],axis=1),nsw.nansumwrapper(SDS['mFOCgAE'],axis=1),out=np.zeros_like(nsw.nansumwrapper(SDS['mFOCgAE']*SDS['TFOFMingAE'],axis=1)),where=nsw.nansumwrapper(SDS['mFOCgAE'],axis=1)!=0)
                SDS['vFOAE']=np.divide(nsw.nansumwrapper(SDS['mFOCgAE']*SDS['vFOgAE'],axis=1),nsw.nansumwrapper(SDS['mFOCgAE'],axis=1),out=np.zeros_like(nsw.nansumwrapper(SDS['mFOCgAE']*SDS['vFOgAE'],axis=1)),where = nsw.nansumwrapper(SDS['mFOCgAE'],axis=1)!=0)
                SDS['rhoFOAE']=np.divide(nsw.nansumwrapper(SDS['mFOCgAE']*SDS['rhoFOgAE'],axis=1),nsw.nansumwrapper(SDS['mFOCgAE'],axis=1),out = np.zeros_like(nsw.nansumwrapper(SDS['mFOCgAE']*SDS['rhoFOgAE'],axis=1)),where=nsw.nansumwrapper(SDS['mFOCgAE'],axis=1 )!=0)
                SDS['LCVFOAE']=np.divide(nsw.nansumwrapper(SDS['mFOCgAE']*SDS['LCVFOgAE'],axis=1),nsw.nansumwrapper(SDS['mFOCgAE'],axis=1),out = np.zeros_like(nsw.nansumwrapper(SDS['mFOCgAE']*SDS['LCVFOgAE'],axis=1)),where =nsw.nansumwrapper(SDS['mFOCgAE'],axis=1)!=0)
                SDS['SulFOAE']=np.divide(nsw.nansumwrapper(SDS['mFOCgAE']*SDS['SulFOgAE'],axis=1),nsw.nansumwrapper(SDS['mFOCgAE'],axis=1),out = np.zeros_like(nsw.nansumwrapper(SDS['mFOCgAE']*SDS['SulFOgAE'],axis=1)),where=nsw.nansumwrapper(SDS['mFOCgAE'],axis=1)!=0)
                
        
            if np.any(SDS['nFOGAE']>0):
                #Remove per change-over info for AE
                del SDS['mFOCgAE']
                del SDS['VFOCgAE']
                del SDS['TFOFMingAE']
                del SDS['vFOgAE']
                del SDS['rhoFOgAE']
                del SDS['LCVFOgAE']
                del SDS['SulFOgAE']
           
            # SRQST['TotalTimeLapse'].append(time.time()-start_time)
            # SRQST['Remarks'].append("complete")    
            # return(SDS,DS,SRQST)
    except Exception as err:
        exp = "Error in ManipulateFuelData {} in line No. {} for IMO {}".format(err,sys.exc_info()[-1].tb_lineno,IMO)
        logger.error(exp,exc_info=True)
        ExceptionHandler(LogType,exp)
        SRQSTD['TotalTimeLapse']=time.time()-start_time              
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return({},{},SRQST)
        
    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time               
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(SDS,DS,SRQST)
    
