# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 17:22:46 2020

@author: Subhin Antony
@Delta:SunKumar

% ClylinderConstants
% Description:  This function estimates the cylinder constants as as
%               suggested empirically by MAN (see "20 21 - ME MANUAL
%               TE2528_VOL.1.pdf" page 169). Those constants are related
%               throught the following forumlas for the indicated and
%               effective pressures:
%
%               peff=Peff/(k2*N*nc)
%               pind=peff+k1
%
%               Where:  Peff    effective power in kW
%                       N       engine speed in RPM
%                       nc      number of cylinders
%                       k1,k2   cyliner constants
%
%               The cylinder constants have the following values for MAN
%               engines:
%
%               k1=1.7bar for S26MC
%               k1=1.2bar for S/L35MC
%               k1=1.0bar for all other models
%               k2=Ck2*dPISTME.^2.*sPISTME
%
% Input:        Ck2     coefficient as defined by MAN with a value of 1.309 (scalar)
%               dPISTME piston bore in m (scalar)
%               sPISTME piston stroke in m (scalar)
%               ModelME engine model (string)
%
% Output:       k1, k2
%
% Note:         R&D is pending to find relevant values for WinGD and other
%               makers' engines. Therefore currently all engines use the
%               MAN values.      
%
% See also:     ReferenceCurves

"""

def CylinderConstants(Ck2,dPISTME,sPISTME,ModelME):
# ClylinderConstants
# Description:  This function estimates the cylinder constants as as
#               suggested empirically by MAN (see "20 21 - ME MANUAL
#               TE2528_VOL.1.pdf" page 169). Those constants are related
#               throught the following forumlas for the indicated and
#               effective pressures:
#
#               peff=Peff/(k2*N*nc)
#               pind=peffk1
#
#               Where:  Peff    effective power in kW
#                       N       engine speed in RPM
#                       nc      number of cylinders
#                       k1,k2   cyliner constants
#
#               The cylinder constants have the following values for MAN
#               engines:
#
#               k1=1.7bar for S26MC
#               k1=1.2bar for S/L35MC
#               k1=1.0bar for all other models
#               k2=Ck2*dPISTME.^2.*sPISTME
#
# Input:        Ck2     coefficient as defined by MAN with a value of 1.309 (scalar)
#               dPISTME piston bore in m (scalar)
#               sPISTME piston stroke in m (scalar)
#               ModelME engine model (string)
#
# Output:       k1, k2
#
# Note:         R&D is pending to find relevant values for WinGD and other
#               makers' engines. Therefore currently all engines use the
#               MAN values.      
#
# See also:     ReferenceCurves



#Estimate mean effective pressure based on MAN formula
    k2=Ck2*dPISTME**2*sPISTME
    if all(elem == 'S26MC' for elem in ModelME):
        #     if ModelME.all()=='S26MC':
        k1=1.7
    elif all(elem == 'S35MC' for elem in ModelME) or all(elem == 'L35MC' for elem in ModelME):
        #     elif ModelME.all()=='S35MC' or ModelME.all() =='L35MC':
        k1=1.2
    else:
        k1=1
    return(k1,k2)