# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 10:08:41 2020

@author: akhil.surendran

% MissingServiceDataEstimator
% Description:  This function is estimating missing service data. 
%
% Input:        ServiceDataStruct
%               ReferenceDataStruct
%               LogType
%
% Output:       ServiceDataStruct
%
% See also:     BPMEngineSingleVessel

"""
import sys
import numpy as np
import pandas as pd 
from ExceptionHandler import ExceptionHandler
from BPME_Common import SeawaterDensity as SWD
from BPME_Transform import WaveHeight2Knots as wh
from BPME_Common import IdealST as IST
import time
import logging 
# create logger
logger = logging.getLogger('BPMEngine.2.02')
def MissingServiceDataEstimator(IMO,LogType,SDS,RDS,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        #Mean draught caclulation in [m]
        if 'Tf' in SDS:
             SDS['Tm']=(SDS['Tf']+SDS['Ta'])/2
        
        #Estimate missing data
        #for j in range(0,len(SDS['pambairER'])):
          
        #Define missing ship headings
        if 'HEAD' in SDS:
            SDS['HEAD'][pd.isnull(SDS['HEAD'])]=0 #deg
          
            #Define missing swell heights
        if  'Hsl' in SDS:
            SDS['Hsl'][pd.isnull(SDS['Hsl'])]=0 #m
          
            #Define missing swell periods
        if 'Tsl' in SDS:
            SDS['Tsl'][pd.isnull(SDS['Tsl'])]=0 #s
          
            #Define missing swell direction
        if 'psislt' in SDS:
            SDS['psislt'][pd.isnull(SDS['psislt'])]=np.degrees(np.arccos((np.cos(np.radians((SDS['HEAD'][pd.isnull(SDS['psislt'])]+90).astype('float64')))))) #deg
          
            #Define missing wave direction
        if  'psiwvt' in SDS:
            #SDS['psiwvt'][pd.isnull(SDS['psiwvt'])]=np.degrees(np.arccos(np.cos(np.radians(SDS['HEAD'][pd.isnull(SDS['psiwvt'])]+90))))#deg
            SDS['psiwvt'][pd.isnull(SDS['psiwvt'])] = np.degrees(np.arccos(np.cos(np.radians((SDS['HEAD'][pd.isnull(SDS['psiwvt'])]+90).astype('float64')))))
            
            #Define missing wind direction
        if 'psiwit' in SDS :
            #SDS['psiwit'][pd.isnull(SDS['psiwit'])]=np.degrees(np.arccos(np.cos(np.radians(SDS['HEAD'][pd.isnull(SDS['psiwit'])]+80)))) #deg   
            SDS['psiwit'][pd.isnull(SDS['psiwit'])] = np.degrees(np.arccos(np.cos(np.radians((SDS['HEAD'][pd.isnull(SDS['psiwit'])]+80).astype('float64')))))
          
            #Define missing air temperature
        if 'Tair'in SDS :
            SDS['Tair'][pd.isnull(SDS['Tair'])]=20 #°C
          
            #Define missing air pressure
        if 'pair' in SDS:
            SDS['pair'][pd.isnull(SDS['pair'])]=1013 #mbar   
          
            #Define missing containers on deck
        if 'TEUonDeck' in SDS:
            SDS['TEUonDeck'][np.logical_or(np.logical_or(pd.isnull(SDS['TEUonDeck']),SDS['TEUonDeck']<0),SDS['TEUonDeck']>2)]=1 #average   
          
            #Define missing pitch # heave motions
        if 'azbow' in SDS:
            SDS['azbow'][pd.isnull(SDS['azbow'])]=0 #No      
          
            #Define missing sea water temperatures
        if 'Tsw' in SDS :
            SDS['Tsw'][pd.isnull(SDS['Tsw'])]=15 #°C   
          
            #Define missing ER ambient air pressure
        if 'pambairER' in SDS:
            SDS['pambairER'][pd.isnull(SDS['pambairER'])]=1013 #mbar
          
            #Define missing ER ambient air temperature    
        if 'TambairER' in SDS:
            SDS['TambairER'][pd.isnull(SDS['TambairER'])]=30 #[°C]
        
        #Define missing cooling water temperature at AC inlet
        #if SDS['TltcwACinME'].ndim==1:
    #       if 'TltcwACinME' in SDS:
    #           SDS['TltcwACinME'][pd.isnull(SDS['TltcwACinME'])]=25 #°C
              
    #       if SDS['TltcwACinME'].ndim==2:
    #          if 'TltcwACinME' in SDS.keys() and pd.isnull(SDS['TltcwACinME'][j][0]):
    #              SDS['TltcwACinME'][j][0]=25 #°C
            
        if 'TairTCinME' in SDS:
            SDS['TairTCinME'][pd.isnull(SDS['TairTCinME'])]=30 #[°C]
       
        if 'TltcwACinME' in SDS:
            SDS['TltcwACinME'][pd.isnull(SDS['TltcwACinME'])]=25
        
        if 'Sasw_wp' in SDS:
            SDS['Sasw_wp'][np.logical_or(pd.isnull(SDS['Sasw_wp']),SDS['Sasw_wp']<0,SDS['Sasw_wp']>50)]=35.16504
        if 'hsw' in SDS and 'hsw_wp' in SDS:  
            SDS['hsw_wp'][SDS['hsw_wp']<0] = np.nan
            SDS['hsw'][np.logical_or(pd.isnull(SDS['hsw']),SDS['hsw']<0)] =SDS['hsw_wp'][np.logical_or(pd.isnull(SDS['hsw']),SDS['hsw']<0)]
        if 'Tsw' in SDS and 'Sasw_wp' in SDS: 
            # if not 'rhosw'  in SDS:
            #     ar= np.empty((len(SDS['pambairER']),1))
            #     ar[:] = np.nan
            #     SDS['rhosw'] = ar
            SDS['rhosw']=SWD.SeawaterDensity(SDS['Tsw'],SDS['Sasw_wp'])
            
        if 'DISP' in SDS:
            SDS['DISP'][pd.isnull(SDS['DISP'])] = IST.IdealST(RDS['Th'],RDS['Vh'],SDS['Tm'][pd.isnull(SDS['DISP'])],0)*SDS['rhosw'][pd.isnull(SDS['DISP'])]/1000
            
        if 'Uwit' in SDS and 'Hwv' in SDS:
             SDS['Uwit'][pd.isnull(SDS['Uwit'])] = wh.WaveHeight2Knots(SDS['Hwv'][pd.isnull(SDS['Uwit'])],SDS['Tm'][pd.isnull(SDS['Uwit'])],RDS['Td'],RDS['B'],RDS['Zaid'],RDS['Zad'],RDS['Ad'])
        # SRQST['TotalTimeLapse'].append(time.time()-start_time)
        # SRQST['Remarks'].append("complete")
        # return(SDS,SRQST)
    except Exception as err:
        exp = "Error in MissingServiceDataEstimator {} in line No. {} for IMO {}".format(err,sys.exc_info()[-1].tb_lineno,IMO)
        logger.error(exp,exc_info=True)
        ExceptionHandler(LogType,exp)
        SRQSTD['TotalTimeLapse']=time.time()-start_time              
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return({},SRQST)
        
    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time               
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(SDS,SRQST)
       
  #############################################################################################      
    #Calculated sea water density based on sea water temperature
#    if 'Tsw' in  SDS:
#        if not 'rhosw'  in SDS:
#            ar= np.empty((len(SDS['pambairER']),1))
#            ar[:] = np.nan
#            SDS['rhosw'] = ar
#            
#        SDS['rhosw'][j][0]=SWD.DensityStandardSeawater(SDS['Tsw'][j]) #[kg/m3]
#          
#        #Define missing displacements
#     if 'DISP' in SDS and pd.isnull(SDS['DISP'][j]) and ~pd.isnull(SDS['Tm'][j]):
#        spln = InterpolatedUnivariateSpline(ReferenceDataStruct['Th'],ReferenceDataStruct['Vh'])
#        spval = spln(SDS['Tm'][j])
#        SDS['DISP'][j]=spval*SDS['rhosw'][j]/1000
#
#    for j in range(0,len(SDS['pambairER'])):
#      if 'Uwit' in SDS and pd.isnull(SDS['Uwit'][j]):
#        if LogType=='HP':
#            #legacy variable
#            if  not pd.isnull(SDS['WIND'][j]):
#                SDS['Uwit'][j]=bk.Beaufort2Knots(SDS['WIND'][j],SDS['Tm'][j],ReferenceDataStruct['Td'],ReferenceDataStruct['B'],ReferenceDataStruct['Zaid'],ReferenceDataStruct['Zad'],ReferenceDataStruct['Ad'])
#            elif not pd.isnull(SDS['Hwv'][j]):
#                SDS['Uwit'][j]=wh.WaveHeight2Knots(SDS['Hwv'][j],SDS['Tm'][j],ReferenceDataStruct['Td'],ReferenceDataStruct['B'],ReferenceDataStruct['Zaid'],ReferenceDataStruct['Zad'],ReferenceDataStruct['Ad'])
#            else:
#                SDS['Uwit'][j]=np.nan;
#            
#        elif LogType=='Noon':
#            if not pd.isnull(SDS['Hwv'][j]):
#                SDS['Uwit'][j]=wh.WaveHeight2Knots(SDS['Hwv'][j],SDS['Tm'][j],ReferenceDataStruct['Td'],ReferenceDataStruct['B'],ReferenceDataStruct['Zaid'],ReferenceDataStruct['Zad'],ReferenceDataStruct['Ad'])
#            else:
#                SDS['Uwit'][j]=np.nan
        







    