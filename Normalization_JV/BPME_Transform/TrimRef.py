# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 14:17:48 2020

@author: Subhin Antony

% TrimRef
% Description:  This function calculates the reference trim as a function
%               of the mean draught.
%
% Input:        Tm      [m]     mean drught (vector)
%               Tam     [m]     model test aft draught (vector)
%               Tfm     [m]     model test fore draught (vector)
%
% Output:       tref    [m]     reference trim at Tm (vector)
%
% Note:         Trim sign convention: (-): by stern, (+): by bow
%
% See also:     ManipulateServiceData

"""
import numpy as np
    
def  TrimRef(Tm,Tam,Tfm):
    tref=np.empty(np.shape(Tm))
    tref[:] = np.nan
    for i in  range(0,len(Tm)):
        tref[i]=TrimRefScalar(Tm[i],Tam,Tfm)    
    return tref    

def TrimRefScalar(Tm,Tam,Tfm):
    imax=len(Tam)
    Tref=(Tam+Tfm)/2  #model test mean draughts in [m]
    trimref=Tfm-Tam   #model test trim in [m]
    if Tm<Tref[0] or Tm>Tref[imax-1] or np.size(Tm) ==0 or np.isnan(Tm.astype('float64')) :
      print("T is out of range!")
      return
    if imax>1:
        for i in range(0,imax-1):
            if Tm>=Tref[i] and Tm<=Tref[i+1]:
               tr=(Tm-Tref[i])*(trimref[i+1]-trimref[i])/(Tref[i+1]-Tref[i])+trimref[i]
    else:
        tr=trimref
    return tr    


#Tf= np.array([11.35, 11.35, np.nan, np.nan, 10.7, 11.21, 11.21, 10.7, 11.35, np.nan])
#Ta = np.array([11.45, 11.45, np.nan, np.nan, 11.35, 10.91, 10.91, 11.35, 10.7, np.nan])
#Tm=(Tf+Ta)/2

#Tm= np.array([11.4 ,11.4 ,np.nan,np.nan,11.025,11.06,11.06,11.025,11.025,np.nan])
#Tam = np.array([9.30,12.00,13.90])
#Tfm = np.array([5.60,12.00,13.90])
#rslt = TrimRef(Tm,Tam,Tfm)
#print(rslt)

