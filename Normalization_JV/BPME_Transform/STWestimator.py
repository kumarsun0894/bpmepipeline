# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 14:17:48 2020

@author: Subhin Antony
"""
import numpy as np
# import math
def STWestimator(SOG,HEAD,Ucut,psicut):
    #Change current direction convention
    psicut_tr=psicut+180

    #Restrict the range within 0-359°
    psicut_tr[psicut_tr>359]=psicut_tr[psicut_tr>359]-360

    #Calculate current factor
    CF=-Ucut*np.cos((np.pi/180) * HEAD.astype('float64') - (np.pi/180) * psicut_tr.astype('float64'))

    #Estimate STW based on crew reported data
    STWest=SOG-CF
    
    return (STWest,CF)

 