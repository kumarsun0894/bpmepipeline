# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 18:13:03 2020

@author: akhil.surendran
"""
import numpy as np
from scipy.interpolate import pchip

def WaveHeight2Knots(Hw,Tm,Td,B,Zaid,Zad,Ad):
    #Beaufort tables from ISO15016
    #BftTable=np.array([0,1,2,3,4,5,6,7,8,9,10,11,12])
    UwTable=np.array([0 ,2,5,8.5,13.5,19,24.5,30.5,37,44,51.5,59,64])# true wind at 10m height [kn]
    HwTable=np.array([0,0.1,0.2,0.6,1,2,3,4,5.5,7,9,11.5,14])
    
    #Geometric calculations
    dT=Td-Tm
    A=Ad+dT*B
    
    #Anemometer heights correction to current loading conditions
    #As per ISO 19030-2:2016 we assume negligible effect of trim
    Za=Zad+dT
    Zaref=(Ad*(Zaid+dT)+0.5*B*dT**2)/A
    pcfn = pchip(HwTable,UwTable)
    Uwtref=pcfn(Hw)
    Uwt=Uwtref*(Za/Zaref)**(1/7)
    return Uwt
    
  
#Hw = np.array([1])
#Tm = np.array([11.4])
#Td = np.array([12])
#B = np.array([37.3])
#Zaid = np.array([10])
#Zad = np.array([46])
#Ad = np.array([1400]) 
#r = WaveHeight2Knots(Hw,Tm,Td,B,Zaid,Zad,Ad) 
#print(r)

