# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 23:43:23 2020

@author: Subhin Antony

% STNormalizationSingleEngine
% Description:  This function calcualtes the average values of a repetitive
%			   engine load and ignores loads higher than 100%. The
%			   returned, "normalized" vector has reduced length. It is
%			   applicable for all vector variables included in shop test,
%			   e.g. pscav, N, Peff, NTC, etc.
%
% Input:		ST		  [-]	 raw shop test variable (vector)
%			   MCRST	   [%]	 raw shop test engine loads (vector)
%
% Output:	   STnorm	  [-]	 normalized shot test variable (vector)
%			   MCRSTnorm   [%]	 normalized shop test engine loads (vector)
%
% Note:		 MCRST must be monotonically increasing and the ST values
%			   should correspond to the MCRST order.
%
% Example:	  If the input vectors are:
%			   - pscavST=[0.59,1.50,2.60,3.15,3.25,3.56]
%			   - MCRST=[25,50,75,100,100,110]
%
%			   The output vectors will be:
%			   - pscavSTnorm=[0.59,1.50,2.60,3.15]
%			   - MCRSTnorm=[25,50,75,100]
%
% See also:	 STNormalization, ReferenceCurves

"""
from BPME_Common import LastElement as le
import numpy as np
from scipy.interpolate import pchip
from BPME_Transform import accumarray as ac
import pandas as pd 

def STNormalizationSingleEngine(ST,MCRST):
	if (len(ST)>1) and (len(ST)!=len(MCRST)):
		print('MEST or AEST variable and MCR have different length! ST Normalization function!')
		MCRSTnorm=np.nan
		STnorm=np.nan
		return(STnorm,MCRSTnorm)
	if np.all(pd.isnull(ST)) and (len(ST)!=len(MCRST)):
		ST=np.empty(MCRST.shape)
		ST[:]=np.nan
	#Remove double entries
	#MCRSTtemp,temp,ic=np.unique(MCRST)
	MCRSTtemp, temp, ic= np.unique(MCRST,return_index=True,return_inverse=True, axis=0)
	STtemp=ac.accum(ic,ST,func=lambda x: np.nanmean(x))
	
	#STtemp=accumarray(ic,ST,[],@nanmean)'
	
	#if values at 100% load are missing
	if not(np.any(MCRSTtemp==100)) and (np.min(MCRSTtemp)<100) and (np.max(MCRSTtemp)>100):
		#An extra input should be added to flag if the value at 100% should be
		#interpolated or not based on the variable nature. E.g. pamb should not
		#be interpolated but the value at 110% generator should be used. This
		#exception is applicable for only a few variables, therefore is not
		#urgent. But whenever judged necassary this should be updated.
	
		#Find value at 100% load
		if np.count_nonzero(~(np.isnan(STtemp)))>1:
			if np.any(np.isnan(STtemp)) or np.any(np.isnan(MCRSTtemp)):
				k  =pd.DataFrame({'STtemp':STtemp,'MCRSTtemp':MCRSTtemp})
				k.dropna(inplace=True)
			
				pchip_1=pchip(k.MCRSTtemp,k.STtemp)								  
				ST100=pchip_1(np.array([100]))
			else:
				pchip_1=pchip(MCRSTtemp,STtemp)								  
				ST100=pchip_1(np.array([100]))
				
		else:
			ST100=np.array(['NaN']).astype('float64')
		
		#Discard values greater than 100% load
		STtemp=STtemp[MCRSTtemp<100]
		MCRSTtemp=MCRSTtemp[MCRSTtemp<100]
		
		
		# STtemp[MCRSTtemp>100]=np.nan
		# STtemp = STtemp[np.logical_not(np.isnan(STtemp))]
		# MCRSTtemp[MCRSTtemp>100]=np.nan
		# MCRSTtemp = MCRSTtemp[np.logical_not(np.isnan(MCRSTtemp))]
		
		#Add value of 100% load as the last element
		MCRSTnorm=np.concatenate((MCRSTtemp,np.array([100])))
		STnorm=np.concatenate((STtemp,ST100))
	#if values at 100% load exist   
	elif (np.any(MCRSTtemp==100)) and  (np.max(MCRSTtemp)>100):
		#Discard values greater than 100% load
		STtemp[MCRSTtemp>100]=np.nan
		MCRSTtemp[MCRSTtemp>100]=np.nan
		
		MCRSTnorm=MCRSTtemp
		STnorm=STtemp
	else:
		MCRSTnorm=MCRSTtemp
		STnorm=STtemp
	return(STnorm,MCRSTnorm)
		
		
	
			
		
	
