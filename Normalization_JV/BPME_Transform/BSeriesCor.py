# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 12:04:41 2020

@author: akhil.surendran

% BSeriesCor
% Description:  This fumction makes corrections on Kt and Kq coefficients
%               according to ITTC (1978) as suggested by (Holtrop & Mennen,
%               1982).
%
% Input:        J       [-]     advance coefficient (vector)
%               P       [m]     propeller pitch (scalar)
%               D       [m]     propeller diameter (scalar)
%               AeAo    [-]     expanded blade area ratio (scalar)
%               z       [-]     number of propeller blades (scalar)
%
% Output:       Kt      [-]     corrected thrust coefficient (vector)
%               Kq      [-]     corrected torque coefficient (vector)
%               etao    [-]     corrected open-water propeller efficiency (vector)
%
% Note1:        The propeller blade surface roughness, kp=0.00003 in [m] is
%               for new propellers. This may be a propeller roughness
%               "tuning" factor or used reversely as an indicator for
%               propeller roughness. Further investigation is needed.
%
% Note2:        kp may be defiend in constants definition page.
%
% References:   - 1978 ITTC Performance Prediction Method
%               - Holtrop, J., & Mennen, G. G. J. (1982). An approximate
%                 power prediction method. International Shipbuilding
%                 Progress, 29(335), 166-170.
%
% See also:     BSeries, KTCoefficients, KQCoefficients


"""
#This fumction makes corrections on Kt and Kq coefficients
#according to ITTC (1978) as suggested by (Holtrop & Mennen,1982).

import numpy as np
from BPME_Transform import BSeries as BS
def BSeriesCor(J,P,D,AeAo,z):
    #Calculate pitch ratio [-]
    P_D=P/D
    #Calculate uncorrected coefficients based on B-Series [-]
    [KtB,KqB,temp]=BS.BSeries(J,P_D,AeAo,z)
    #Propeller blade surface roughness [m]
    kp=0.00003

    #Calculate the chord length at 75# prop radius
    c075=2.073*AeAo*D/z

    #Calculate the thickness-chordlength ratio
    tc075=(0.0185-0.00125*z)*D/c075

    #Calculate the difference in drag coefficient of the profile section
    DCd=(2+4*tc075)*(0.003605-(1.89+1.62*np.log(c075/kp))**(-2.5))

    #Apply correction formula for Kt [-]
    Kt=KtB+DCd*0.3*P*c075*z/D**2

    #Apply correction formula for Kq [-]
    Kq=KqB-DCd*0.25*c075*z/D

    #Calculate etao based on its definition [-]
    etao=J/(2*np.pi)*Kt/Kq
    
    return (Kt,Kq,etao)
