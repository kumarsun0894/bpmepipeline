# -*- coding: utf-8 -*-
"""
Created on Fri Oct  9 16:55:57 2020

@author: SUN
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 12:12:40 2020

@author: Subhin Antony

 ReferenceCurves
 Description:  This function generates the reference curves.

 Input:        RDS

 Output:       RDS

 See also:     BPMEngineSingleVessel


 Disclaimer:
 -----------
 BSM retains the intellectual rights of all developments/process
 enhancements for these changes and amendments, which will be ongoing.
 Sharing of this information outside of the BSM Group should have prior
 written permission from the authors and BSM Corporate Fleet Management.

 History:
 --------
 06/05/2020 version 1.26
 29/03/2020 version 1.25
 20/12/2019 version 1.24
 16/12/2019 version 1.23
 06/12/2019 version 1.22
 09/09/2019 version 1.21
 15/08/2019 version 1.20
 15/07/2019 version 1.19
 18/04/2019 version 1.18
 08/04/2019 version 1.17
 04/12/2018 version 1.16
 07/11/2018 version 1.15
 07/11/2018 version 1.14
 05/11/2018 version 1.13
 17/07/2018 version 1.12
 10/07/2018 version 1.11
 23/05/2018 version 1.10
 03/05/2018 version 1.09
 25/04/2018 version 1.08
 13/10/2017 version 1.07
 06/10/2017 version 1.06
 07/09/2017 version 1.05
 31/07/2017 version 1.04
 28/07/2017 version 1.03
 28/07/2017 version 1.02
 12/06/2017 version 1.01
 06/06/2017 version 1.00

"""
import sys
from BPME_Common.pchipwrap import pchipwrap as pchip
# from scipy.interpolate import pchip
import numpy as np
import math
# from BPME_Repository import BPMEngineSingleVessel as bpmsv
from ExceptionHandler import ExceptionHandler
from BPME_Transform import AeAoKeller as ae
from BPME_Transform import STNormalization as st
from BPME_Transform import EffectivePower as ep
from BPME_Common import SFOCiso as sf
from BPME_Common import ISOCorrections as ic
from BPME_Common import TCefficiency as te
from BPME_Transform import CylinderConstants as cc
from BPME_Common import find as f
from BPME_Common import RoundTwo as rt
from BPME_Common import IdealDeliveredPower as idp
from BPME_Common import IdealSpeed as IS
from BPME_Common import IdealRPM as ir
from BPME_Common import LastElement as LE
from BPME_Transform import ReferenceCalibrationFactorHoltrop as rch
from BPME_Common import SeawaterDensity as swd
from BPME_Transform import BSeriesCor as bsc
from BPME_Transform import etagenCalculator as etgc
import time
import logging

# create logger
logger = logging.getLogger('BPMEngine.2.02')


# def ReferenceCurves(RDS,LogType):

# RDS=bpmsv.RDS
# LogType = 'Noon'

def ReferenceCurves(records_df, start_time, SRQST):
    SRQSTD = {}
    SRQSTD['StepTimeLapse'] = time.time() - start_time
    SRQSTD['Stage'] = __name__
    rc_status = -1
    try:
        for i, v in records_df.iterrows():
            imo = v.imo
            try:

                RDS = v['RDSraw']

                if RDS['U0MT'].ndim == 1:
                    RDS['U0MT'] = RDS['U0MT'].reshape(1, -1)
                if RDS['P0MT'].ndim == 1:
                    RDS['P0MT'] = RDS['P0MT'].reshape(1, -1)
                if RDS['N0MT'].ndim == 1:
                    RDS['N0MT'] = RDS['N0MT'].reshape(1, -1)

                # Estimate Missing Reference Data

                # Estimate etaB

                if math.isnan(RDS['etaB']):
                    RDS['etaB'] = 1
                    print('etaB is estimated!')

                # Estimate etaS
                if math.isnan(RDS['etaS']):
                    RDS['etaS'] = 0.99
                    print('etaS is estimated!')

                # Estimate etaeffME
                if np.any(np.isnan(RDS['etaeffME'])):
                    RDS['etaeffME'] = np.ones(int(RDS['nME'])) * 0.99
                    print('etaeffME is estimated!')

                # Estimate PowerTypeMEST
                # This rule needs modification but also to fix the m-file export format to not mix NaNs in a string vector.
                if 'NaN' in RDS['PowerTypeMEST']:
                    RDS['PowerTypeMEST'] = np.array(['E' for i in range(0, int(RDS['nME']))])
                    print('PowerTypeMEST is estimated!')

                # #Estimate AuxBlowMEST
                # if any(np.isnan(RDS['AuxBlowMEST'])):
                #     AuxBlowMESTtemp=np.zeros(len(RDS['MCRMEST']))
                #     index=[i for i ,X in enumerate(RDS['MCRMEST']) if X<=30]
                #     for i in index:
                #         AuxBlowMESTtemp[i]=1
                #     index_AuxBlowMEST=[i for i ,X in enumerate(RDS['AuxBlowMEST']) if not(math.isnan(X))]
                #     for i in index_AuxBlowMEST:
                #         AuxBlowMESTtemp[i]=RDS['AuxBlowMEST'][i]
                #     RDS[AuxBlowMEST]=AuxBlowMESTtemp;
                #     print('AuxBlowMEST is estimated!')

                # Estimate AuxBlowMEST
                if np.any(np.isnan(RDS['AuxBlowMEST'])):
                    AuxBlowMESTtemp = np.zeros(RDS['MCRMEST'].shape)
                    AuxBlowMESTtemp[RDS['MCRMEST'] <= 30] = 1
                    AuxBlowMESTtemp[np.flatnonzero(~(np.isnan(RDS['AuxBlowMEST'])))] = RDS['AuxBlowMEST'][
                        np.flatnonzero(~(np.isnan(RDS['AuxBlowMEST'])))]
                    # AuxBlowMESTtemp[~(np.isnan(RDS['AuxBlowMEST']))]=RDS['AuxBlowMEST'][~(np.isnan(RDS['AuxBlowMEST']))]
                    RDS['AuxBlowMEST'] = AuxBlowMESTtemp
                    print('AuxBlowMEST is estimated!')

                # Estimate muTCME

                if np.any(np.isnan(RDS['muTCME'])):
                    if RDS['nME'] == 1:
                        muTCMEtemp = np.ones(int(np.max(RDS['nTCME']))) * 0.7
                    else:
                        muTCMEtemp = np.ones((int(RDS['nME']), int(np.max(RDS['nTCME'])))) * 0.7

                    # suppose all engines have the same number of TCs
                    muTCMEtemp[np.flatnonzero(~(np.isnan(RDS['muTCME'])))] = RDS['muTCME'][
                        np.flatnonzero(~(np.isnan(RDS['muTCME'])))]
                    RDS['muTCME'] = muTCMEtemp
                    print('muTCME is estimated!')

                # a=np.array([1,2,float('NaN')])
                # a[np.isnan(a)] =  np.array([1])
                # print(type(np.isnan(a)))
                # import numpy as np
                # a=np.ones((2,2))*7
                # print(a)
                # b=np.array([[3,float('NaN')],[4,float('NaN')]])
                # print([np.isnan(b)])
                # print(np.isnan(b))
                # a[~(np.isnan(b))]=b[~(np.isnan(b))]
                # print(a)

                # a={
                #    'c' : np.ones((2,2)),
                #    'b' : np.array([[3,float('NaN')],[4,float('NaN')]])
                #    }

                # g=a['c'][~(np.isnan(a['b']))]
                # print(g.shape)

                # Estimate dTCcompME
                if np.any(np.isnan(RDS['dTCcompME'])):
                    if RDS['nME'] == 1:
                        RDS['dTCcompME'] = np.empty(int(np.max(RDS['nTCME'])))
                        RDS['dTCcompME'][:] = np.nan
                    else:
                        RDS['dTCcompME'] = np.empty((int(RDS['nME']), int(np.max(RDS['nTCME']))))
                        RDS['dTCcompME'][:] = np.nan
                    # suppose all engines have the same number of TCs
                    print('dTCcompME is missing!')

                # import numpy as np
                # hh= np.empty((2,2))
                # hh[:]=np.NaN
                # print(hh)
                # print(type(hh[0][0]))

                # Estimate missing AeAoProp
                if math.isnan(RDS['AeAoProp']):
                    # In case of nME>1 the sum of MCR is used.
                    RDS['AeAoProp'] = ae.AeAoKeller(np.sum(RDS['PMCRME']), RDS['nProp'], RDS['dProp'], RDS['zProp'],
                                                    RDS['Ud'], RDS['Td'])
                    print('AeAoProp is estimated!')
                # Estimate Cstern
                if RDS['Cstern'] == 'NaN':
                    RDS['Cstern'] = "N"
                    print('Cstern is estimated!')
                # Estimate rhoswSTrial
                if math.isnan(RDS['rhoswSTrial']):
                    RDS['rhoswSTrial'] = swd.SeawaterDensity(15, 35.16504)
                    print('rhoswSTrial is estimated!')
                # Estimate generator efficiency if AE modelling is not verified
                if not ('Y' == RDS['AEVerModel']):
                    RDS['etagenAE'] = 0.96 * np.ones(np.shape(RDS['PengnomAE']))
                if RDS['PengnomAE'].ndim == 1:
                    RDS['PengnomAE'] = RDS['PengnomAE'].reshape(1, -1)

                # ME Shop Test Data

                # Normalize ME shop test data
                # This step is to average multiple entries at 100% and to remove 110% data
                MCRMESTraw = RDS['MCRMEST']
                # RDS['MCRMEST']=np.array([])
                [RDS['NMEST'], RDS['MCRMEST']] = st.STNormalization(RDS['NMEST'], MCRMESTraw)
                [RDS['PMEST'], temp] = st.STNormalization(RDS['PMEST'], MCRMESTraw)
                [RDS['pambairMEST'], temp] = st.STNormalization(RDS['pambairMEST'], MCRMESTraw)
                [RDS['NTCMEST'], temp] = st.STNormalization(RDS['NTCMEST'], MCRMESTraw)
                [RDS['TscavMEST'], temp] = st.STNormalization(RDS['TscavMEST'], MCRMESTraw)
                [RDS['FOCMEST'], temp] = st.STNormalization(RDS['FOCMEST'], MCRMESTraw)
                [RDS['SFOCMEST'], temp] = st.STNormalization(RDS['SFOCMEST'], MCRMESTraw)
                [RDS['SFOCisoMEST'], temp] = st.STNormalization(RDS['SFOCisoMEST'], MCRMESTraw)
                [RDS['peffMEST'], temp] = st.STNormalization(RDS['peffMEST'], MCRMESTraw)
                [RDS['FPIMEST'], temp] = st.STNormalization(RDS['FPIMEST'], MCRMESTraw)
                [RDS['TFOPPinMEST'], temp] = st.STNormalization(RDS['TFOPPinMEST'], MCRMESTraw)
                [RDS['pindMEST'], temp] = st.STNormalization(RDS['pindMEST'], MCRMESTraw)
                [RDS['pmaxMEST'], temp] = st.STNormalization(RDS['pmaxMEST'], MCRMESTraw)
                [RDS['pcompMEST'], temp] = st.STNormalization(RDS['pcompMEST'], MCRMESTraw)
                [RDS['pscavMEST'], temp] = st.STNormalization(RDS['pscavMEST'], MCRMESTraw)
                [RDS['TcwACinMEST'], temp] = st.STNormalization(RDS['TcwACinMEST'], MCRMESTraw)
                [RDS['TcwACoutMEST'], temp] = st.STNormalization(RDS['TcwACoutMEST'], MCRMESTraw)
                [RDS['TairACinMEST'], temp] = st.STNormalization(RDS['TairACinMEST'], MCRMESTraw)
                [RDS['TairACoutMEST'], temp] = st.STNormalization(RDS['TairACoutMEST'], MCRMESTraw)
                [RDS['dpairACMEST'], temp] = st.STNormalization(RDS['dpairACMEST'], MCRMESTraw)
                [RDS['dpairAFMEST'], temp] = st.STNormalization(RDS['dpairAFMEST'], MCRMESTraw)
                [RDS['TairTCinMEST'], temp] = st.STNormalization(RDS['TairTCinMEST'], MCRMESTraw)
                [RDS['TegEVoutMEST'], temp] = st.STNormalization(RDS['TegEVoutMEST'], MCRMESTraw)
                [RDS['TegTCinMEST'], temp] = st.STNormalization(RDS['TegTCinMEST'], MCRMESTraw)
                [RDS['TegTCoutMEST'], temp] = st.STNormalization(RDS['TegTCoutMEST'], MCRMESTraw)
                [RDS['pegRMEST'], temp] = st.STNormalization(RDS['pegRMEST'], MCRMESTraw)
                [RDS['pegTCinMEST'], temp] = st.STNormalization(RDS['pegTCinMEST'], MCRMESTraw)
                [RDS['pegTCoutMEST'], temp] = st.STNormalization(RDS['pegTCoutMEST'], MCRMESTraw)
                [RDS['AuxBlowMEST'], temp] = st.STNormalization(RDS['AuxBlowMEST'], MCRMESTraw)

                # Calculate effective power in kW
                RDS['PeffMEST'] = ep.EffectivePower(RDS['PMEST'], RDS['etaeffME'], RDS['PowerTypeMEST'], RDS['nMEST'],
                                                    RDS['NoME'])

                # Calculate SFOC and SFOCiso in g/kWh
                RDS['SFOCcalcMEST'] = 1000 * RDS['FOCMEST'] / RDS['PeffMEST']
                rds = []
                for i in range(0, int(RDS['nMEST'])):
                    if RDS['SFOCcalcMEST'].ndim == 1:
                        RDS['SFOCcalcMEST'] = RDS['SFOCcalcMEST'].reshape(1, -1)
                    if RDS['TcwACinMEST'].ndim == 1:
                        RDS['TcwACinMEST'] = RDS['TcwACinMEST'].reshape(1, -1)
                    if RDS['TairTCinMEST'].ndim == 1:
                        RDS['TairTCinMEST'] = RDS['TairTCinMEST'].reshape(1, -1)
                    if RDS['pambairMEST'].ndim == 1:
                        RDS['pambairMEST'] = RDS['pambairMEST'].reshape(1, -1)

                    k = sf.SFOCiso(RDS['SFOCcalcMEST'][i, :], \
                                   RDS['LCVFOMEST'][i], \
                                   RDS['LCVMDOref'], \
                                   RDS['TcwACinMEST'][i, :], \
                                   RDS['TairTCinMEST'][i, :], \
                                   RDS['pambairMEST'][i, :])
                    rds.append(k.ravel())
                RDS['SFOCisocalcMEST'] = np.array(rds)

                if np.any(np.isnan(RDS['SFOCisocalcMEST'])):
                    if RDS['SFOCisoMEST'].ndim == 1:
                        RDS['SFOCisoMEST'] = RDS['SFOCisoMEST'].reshape(1, -1)
                    RDS['SFOCisocalcMEST'] = RDS['SFOCisoMEST']
                    print('SFOCiso for ME Shop Test cannot be calculated!')

                # Correct 4 reference variables for ISO conditions as per MAN manual, section 706-06
                RDS['TegEVoutisocalcMEST'] = ic.ISOCorrections(RDS['TegEVoutMEST'], RDS['TairTCinMEST'],
                                                               RDS['TcwACinMEST'], RDS['F1iso'][0], RDS['F2iso'][0],
                                                               RDS['Kiso'][0])
                RDS['pscavisocalcMEST'] = ic.ISOCorrections(RDS['pscavMEST'], RDS['TairTCinMEST'], RDS['TcwACinMEST'],
                                                            RDS['F1iso'][1], RDS['F2iso'][1], RDS['Kiso'][1])
                RDS['pcompisocalcMEST'] = ic.ISOCorrections(RDS['pcompMEST'], RDS['TairTCinMEST'], RDS['TcwACinMEST'],
                                                            RDS['F1iso'][2], RDS['F2iso'][2], RDS['Kiso'][2])
                RDS['pmaxisocalcMEST'] = ic.ISOCorrections(RDS['pmaxMEST'], RDS['TairTCinMEST'], RDS['TcwACinMEST'],
                                                           RDS['F1iso'][3], RDS['F2iso'][3], RDS['Kiso'][3])

                # Calculate absolute pressures
                RDS['pscavabsisocalcMEST'] = RDS['pscavisocalcMEST'] + RDS['pambairMEST'] / 1000;
                RDS['pcompabsisocalcMEST'] = RDS['pcompisocalcMEST'] + RDS['pambairMEST'] / 1000;
                RDS['pmaxabsisocalcMEST'] = RDS['pmaxisocalcMEST'] + RDS['pambairMEST'] / 1000;

                # Calculate pressure rise in bar
                RDS['pmax_pcompisoMEST'] = RDS['pmaxabsisocalcMEST'] - RDS['pcompabsisocalcMEST']

                # Calculate pressure ratio in
                RDS['pcomp_pscavabsisoMEST'] = RDS['pcompabsisocalcMEST'] / RDS['pscavabsisocalcMEST']

                # Calculate temperature differences in C
                RDS['dTaircwACMEST'] = RDS['TairACoutMEST'] - RDS['TcwACinMEST']
                RDS['dTcwACMEST'] = RDS['TcwACoutMEST'] - RDS['TcwACinMEST']

                # Calculate TC efficiencies

                if RDS['pambairMEST'].ndim == 1:
                    RDS['pambairMEST'] = RDS['pambairMEST'].reshape(1, -1)
                if RDS['dpairACMEST'].ndim == 1:
                    RDS['dpairACMEST'] = RDS['dpairACMEST'].reshape(1, -1)
                if RDS['TairTCinMEST'].ndim == 1:
                    RDS['TairTCinMEST'] = RDS['TairTCinMEST'].reshape(1, -1)
                if RDS['NTCMEST'].ndim == 1:
                    RDS['NTCMEST'] = RDS['NTCMEST'].reshape(1, -1)
                if RDS['pscavMEST'].ndim == 1:
                    RDS['pscavMEST'] = RDS['pscavMEST'].reshape(1, -1)
                if RDS['pegRMEST'].ndim == 1:
                    RDS['pegRMEST'] = RDS['pegRMEST'].reshape(1, -1)
                if RDS['pegTCoutMEST'].ndim == 1:
                    RDS['pegTCoutMEST'] = RDS['pegTCoutMEST'].reshape(1, -1)
                if RDS['TegTCinMEST'].ndim == 1:
                    RDS['TegTCinMEST'] = RDS['TegTCinMEST'].reshape(1, -1)
                if RDS['dTCcompME'].ndim == 1:
                    RDS['dTCcompME'] = RDS['dTCcompME'].reshape(1, -1)
                if RDS['muTCME'].ndim == 1:
                    RDS['muTCME'] = RDS['muTCME'].reshape(1, -1)

                etatm = []
                etacm = []
                etatum = []
                for i in range(0, int(RDS['nMEST'])):
                    j = int(RDS['NoME'][i]) - int(1)
                    RDS_etaTCtotcalcMEST, RDS_etaTCcompcalcMEST, RDS_etaTCturbcalcMEST = te.TCefficiency(
                        RDS['pambairMEST'][i, :], RDS['dpairACMEST'][i, :], RDS['TairTCinMEST'][i, :],
                        RDS['NTCMEST'][i, :], RDS['pscavMEST'][i, :], RDS['pegRMEST'][i, :], RDS['pegTCoutMEST'][i, :],
                        RDS['TegTCinMEST'][i, :], RDS['dTCcompME'][j, 0], RDS['muTCME'][j, 0])
                    etatm.append(RDS_etaTCtotcalcMEST.ravel())
                    etacm.append(RDS_etaTCcompcalcMEST.ravel())
                    etatum.append(RDS_etaTCturbcalcMEST.ravel())
                RDS['etaTCtotcalcMEST'] = np.array(etatm)
                RDS['etaTCcompcalcMEST'] = np.array(etacm)
                RDS['etaTCturbcalcMEST'] = np.array(etatum)

                # Define cylinder constants
                RDS['k1'], RDS['k2'] = cc.CylinderConstants(RDS['Ck2'], RDS['dPISTME'], RDS['sPISTME'], RDS['ModelME'])

                # Dual Fuel Engines

                if 'Yes' in RDS['DualFuelME']:
                    # Define representative engine No for each applicable fuel mode
                    RDS['RepMEFO'] = f.find(RDS['FuelModeMEST'], 'F', 'first')
                    RDS['RepMEGO'] = f.find(RDS['FuelModeMEST'], 'G', 'first')

                    [RDS['TGOinMEST'], temp] = st.STNormalization(RDS['TGOinMEST'], MCRMESTraw)
                    [RDS['pGOinMEST'], temp] = st.STNormalization(RDS['pGOinMEST'], MCRMESTraw)
                    [RDS['GOCMEST'], temp] = st.STNormalization(RDS['GOCMEST'], MCRMESTraw)
                    [RDS['SGOCMEST'], temp] = st.STNormalization(RDS['SGOCMEST'], MCRMESTraw)
                    [RDS['SGOCisoMEST'], temp] = st.STNormalization(RDS['SGOCisoMEST'], MCRMESTraw)

                    RDS['SGOCcalcMEST'] = 1000 * RDS['GOCMEST'] / RDS['PeffMEST']
                    if RDS['SGOCcalcMEST'].ndim == 1:
                        RDS['SGOCcalcMEST'] = RDS['SGOCcalcMEST'].reshape(1, -1)
                    if RDS['TcwACinMEST'].ndim == 1:
                        RDS['TcwACinMEST'] = RDS['TcwACinMEST'].reshape(1, -1)
                    if RDS['TairTCinMEST'].ndim == 1:
                        RDS['TairTCinMEST'] = RDS['TairTCinMEST'].reshape(1, -1)
                    if RDS['pambairMEST'].ndim == 1:
                        RDS['pambairMEST'] = RDS['pambairMEST'].reshape(1, -1)
                    sgoc = []
                    for i in range(0, int(RDS['nMEST'])):
                        sfo = sf.SFOCiso(RDS['SGOCcalcMEST'][i, :], \
                                         RDS['LCVGOMEST'][i], \
                                         RDS['LCVMDOref'], \
                                         RDS['TcwACinMEST'][i, :], \
                                         RDS['TairTCinMEST'][i, :], \
                                         RDS['pambairMEST'][i, :])
                        sgoc.append(sfo.ravel())
                    RDS['SGOCisocalcMEST'] = np.array(sgoc)
                    for i in range(0, int(RDS['nMEST'])):
                        if np.any(np.isnan(RDS['SGOCisocalcMEST'][i, :])):
                            RDS['SGOCisocalcMEST'][i, :] = RDS['SGOCisoMEST'][i, :]
                            print('SGOCiso for ME Shop Test cannot be calculated!')

                # AE Shop Test Data
                # Calculate generator efficiency for all AE loads and initialize the generator load if AE is not modelled
                RDS['etagenAEST'], RDS['MCRgenAEST'] = etgc.etagenCalculator(RDS['etagenAEST'], RDS['MCRAEST'])

                # Calculate real generator power in kW
                RDS['PgennomRAE'] = RDS['PgennomAAE'] * RDS['PFnomAE']
                if RDS['PgennomRAE'].ndim == 1:
                    RDS['PgennomRAE'] = RDS['PgennomRAE'].reshape(1, -1)

                if 'Y' in RDS['AEVerModel']:  # removed all log type
                    # LoadTypeAEST must be identical accross engines
                    LTAE = np.unique(RDS['LoadTypeAEST'])
                    # Define engine power and generator real power for all loads in kW
                    if LTAE == 'E':
                        if RDS['PengnomAE'].ndim == 1:
                            RDS['PengAEST'] = RDS['PengnomAE'].reshape(1, -1).T * RDS['MCRAEST'] / 100
                        else:
                            RDS['PengAEST'] = RDS['PengnomAE'].T * RDS['MCRAEST'] / 100
                        RDS['PgenRAEST'] = RDS['PengAEST'] * RDS['etagenAEST']
                        MCRAESTraw = RDS['MCRAEST']
                    elif LTAE == 'G':
                        RDS['PgenRAEST'] = (RDS['PgennomRAE'].T) * RDS['MCRAEST'] / 100
                        RDS['PengAEST'] = RDS['PgenRAEST'] / RDS['etagenAEST']
                        MCRAESTraw = RDS['PengAEST'] / (RDS['PengnomAE'].T) * 100
                    # Handle missing FOC info
                    for i in range(0, int(RDS['nAEST'])):
                        if (np.count_nonzero(~np.isnan(RDS['FOCAEST'][i, :])) > 0) and np.count_nonzero(
                                ~np.isnan(RDS['FOCAEST'][i, :])) < len(MCRAESTraw[i, :]):
                            pchip_1 = pchip(np.concatenate((np.array([0]), MCRAESTraw[i, :])),
                                            np.concatenate((np.array([0]), RDS['FOCAEST'][i, :])))
                            RDS['FOCAEST'][i, :] = pchip_1(MCRAESTraw[i, :])
                    # Handle missing SFOC info
                    if np.all(np.size(RDS['SFOCAEST']) == np.size(MCRAESTraw)):
                        RDS['SFOCAEST'][np.isnan(RDS['SFOCAEST'])] = 1000 * RDS['FOCAEST'][np.isnan(RDS['SFOCAEST'])] / \
                                                                     RDS['PengAEST'][np.isnan(RDS['SFOCAEST'])]
                    elif np.all(np.isnan(RDS['SFOCAEST'])):
                        RDS['SFOCAEST'] = 1000 * RDS['FOCAEST'] / RDS['PengAEST']
                    # Handle missing SFOCiso info
                    if np.all(np.size(RDS['SFOCisoAEST']) == np.size(MCRAESTraw)) and np.all(
                            np.size(RDS['SFOCisoAEST']) == np.size(RDS['TairTCinAEST'])) and np.all(
                            np.size(RDS['SFOCisoAEST']) == np.size(RDS['pambairAEST'])):
                        for i in range(0, int(RDS['nAEST'])):
                            RDS['SFOCisoAEST'][i, np.isnan(RDS['SFOCisoAEST'][i, :])] = sf.SFOCiso(
                                RDS['SFOCAEST'][i, np.isnan(RDS['SFOCisoAEST'][i, :])], RDS['LCVFOAEST'][i],
                                RDS['LCVMDOref'], RDS['TcwACinAEST'][i, np.isnan(RDS['SFOCisoAEST'][i, :])],
                                RDS['TairTCinAEST'][i, np.isnan(RDS['SFOCisoAEST'][i, :])],
                                RDS['pambairAEST'][i, np.isnan(RDS['SFOCisoAEST'][i, :])])

                    # Normalize AE shop test data
                    # MCRAESTraw=RDS['MCRAEST']
                    [RDS['NAEST'], RDS['MCRAEST']] = st.STNormalization(RDS['NAEST'], MCRAESTraw)
                    [RDS['etagenAEST'], temp] = st.STNormalization(RDS['etagenAEST'], MCRAESTraw)
                    [RDS['PengAEST'], temp] = st.STNormalization(RDS['PengAEST'], MCRAESTraw)
                    [RDS['PgenRAEST'], temp] = st.STNormalization(RDS['PgenRAEST'], MCRAESTraw)
                    [RDS['FPIAEST'], temp] = st.STNormalization(RDS['FPIAEST'], MCRAESTraw)
                    [RDS['TFOPPinAEST'], temp] = st.STNormalization(RDS['TFOPPinAEST'], MCRAESTraw)
                    [RDS['pambairAEST'], temp] = st.STNormalization(RDS['pambairAEST'], MCRAESTraw)
                    [RDS['NTCAEST'], temp] = st.STNormalization(RDS['NTCAEST'], MCRAESTraw)
                    [RDS['TairACinAEST'], temp] = st.STNormalization(RDS['TairACinAEST'], MCRAESTraw)
                    [RDS['TscavAEST'], temp] = st.STNormalization(RDS['TscavAEST'], MCRAESTraw)
                    [RDS['FOCAEST'], temp] = st.STNormalization(RDS['FOCAEST'], MCRAESTraw)
                    [RDS['SFOCAEST'], temp] = st.STNormalization(RDS['SFOCAEST'], MCRAESTraw)
                    [RDS['SFOCisoAEST'], temp] = st.STNormalization(RDS['SFOCisoAEST'], MCRAESTraw)
                    [RDS['pmaxAEST'], temp] = st.STNormalization(RDS['pmaxAEST'], MCRAESTraw)
                    [RDS['pcompAEST'], temp] = st.STNormalization(RDS['pcompAEST'], MCRAESTraw)
                    [RDS['pscavAEST'], temp] = st.STNormalization(RDS['pscavAEST'], MCRAESTraw)
                    [RDS['TcwACinAEST'], temp] = st.STNormalization(RDS['TcwACinAEST'], MCRAESTraw)
                    [RDS['TcwACoutAEST'], temp] = st.STNormalization(RDS['TcwACoutAEST'], MCRAESTraw)
                    [RDS['TairTCinAEST'], temp] = st.STNormalization(RDS['TairTCinAEST'], MCRAESTraw)
                    [RDS['TegEVoutAEST'], temp] = st.STNormalization(RDS['TegEVoutAEST'], MCRAESTraw)
                    [RDS['TegTCinAEST'], temp] = st.STNormalization(RDS['TegTCinAEST'], MCRAESTraw)
                    [RDS['TegTCoutAEST'], temp] = st.STNormalization(RDS['TegTCoutAEST'], MCRAESTraw)
                    [RDS['dpairACAEST'], temp] = st.STNormalization(RDS['dpairACAEST'], MCRAESTraw)
                    [RDS['dpairAFAEST'], temp] = st.STNormalization(RDS['dpairAFAEST'], MCRAESTraw)
                    # Calculate generator percentage load
                    RDS['MCRgenAEST'] = []
                    RDS['MCRgenAEST'] = RDS['PgenRAEST'] / (RDS['PgennomRAE'].T) * 100

                    # Calculate SFOC and SFOCiso in g/kWh
                    RDS['SFOCcalcAEST'] = 1000 * RDS['FOCAEST'] / RDS['PengAEST']
                    SFAEST = []
                    for i in range(0, int(RDS['nAEST'])):
                        aest = sf.SFOCiso(RDS['SFOCcalcAEST'][i, :], \
                                          RDS['LCVFOAEST'][i], \
                                          RDS['LCVMDOref'], \
                                          RDS['TcwACinAEST'][i, :], \
                                          RDS['TairTCinAEST'][i, :], \
                                          RDS['pambairAEST'][i, :])
                        SFAEST.append(aest.ravel())
                    RDS['SFOCisocalcAEST'] = np.array(SFAEST)
                    if np.any(np.isnan(RDS['SFOCisocalcAEST'])):
                        RDS['SFOCisocalcAEST'] = RDS['SFOCisoAEST']
                        print('SFOCiso for AE Shop Test cannot be calculated!')
                    # Correct 4 reference variables for ISO conditions as per MAN manual, section 706-06
                    RDS['TegEVoutisocalcAEST'] = ic.ISOCorrections(RDS['TegEVoutAEST'], RDS['TairTCinAEST'],
                                                                   RDS['TcwACinAEST'], RDS['F1iso'][0], RDS['F2iso'][0],
                                                                   RDS['Kiso'][0])
                    RDS['pscavisocalcAEST'] = ic.ISOCorrections(RDS['pscavAEST'], RDS['TairTCinAEST'],
                                                                RDS['TcwACinAEST'], RDS['F1iso'][1], RDS['F2iso'][1],
                                                                RDS['Kiso'][1])
                    RDS['pmaxisocalcAEST'] = ic.ISOCorrections(RDS['pmaxAEST'], RDS['TairTCinAEST'], RDS['TcwACinAEST'],
                                                               RDS['F1iso'][3], RDS['F2iso'][3], RDS['Kiso'][3])
                    RDS['TegTCinisocalcAEST'] = RDS['TegTCinAEST']  # must add correction
                    RDS['TegTCoutisocalcAEST'] = RDS['TegTCoutAEST']  # must add correction

                    # Calculate absolute pressures
                    RDS['pscavabsisocalcAEST'] = RDS['pscavisocalcAEST'] + RDS['pambairAEST'] / 1000
                    RDS['pmaxabsisocalcAEST'] = RDS['pmaxisocalcAEST'] + RDS['pambairAEST'] / 1000
                    # Calculate generator efficiency
                    # RDS['etagenAE']=np.mean(RDS['PgenAEST']/RDS['PengAEST'],axis=1).T
                    # if np.all(np.isnan(RDS['etagenAE'])):
                    #     RDS['etagenAE']=0.96*np.ones(np.shape(RDS['PengnomAE']))

                    # Dual Fuel Engines
                    if ('Yes' in RDS['DualFuelAE']):
                        [RDS['TGOinAEST'], temp] = st.STNormalization(RDS['TGOinAEST'], MCRAESTraw)
                        [RDS['pGOinAEST'], temp] = st.STNormalization(RDS['pGOinAEST'], MCRAESTraw)
                        [RDS['GOCAEST'], temp] = st.STNormalization(RDS['GOCAEST'], MCRAESTraw)
                        [RDS['SGOCAEST'], temp] = st.STNormalization(RDS['SGOCAEST'], MCRAESTraw)
                        [RDS['SGOCisoAEST'], temp] = st.STNormalization(RDS['SGOCisoAEST'], MCRAESTraw)
                        RDS['SGOCcalcAEST'] = 1000 * RDS['GOCAEST'] / RDS['PengAEST']
                        SGAEST = []
                        for i in range(0, int(RDS['nAEST'])):
                            goaest = sf.SFOCiso(RDS['SGOCcalcAEST'][i, :], \
                                                RDS['LCVGOAEST'][i], \
                                                RDS['LCVMDOref'], \
                                                RDS['TcwACinAEST'][i, :], \
                                                RDS['TairTCinAEST'][i, :], \
                                                RDS['pambairAEST'][i, :])
                            SGAEST.append(goaest.ravel())
                        RDS['SGOCisocalcAEST'] = np.array(SGAEST)
                        for i in range(0, int(RDS['nAEST'])):
                            if np.any(np.isnan(RDS['SGOCisocalcAEST'][i, :])):
                                RDS['SGOCisocalcAEST'][i, :] = RDS['SGOCisoAEST'][i, :]
                                print('SGOCiso for AE Shop Test cannot be calculated!')

                ###############################################################
                ###############################################################
                ###############################################################

                # Model Test and Sea Trials Data

                # Define reference draughts as the model test mean draughts in [m]
                # RDS['Tref']=np.around((RDS['TaMT']+RDS['TfMT'])/2,decimals=2)
                RDS['Tref'] = rt.RoundTwo((RDS['TaMT'] + RDS['TfMT']) / 2)

                # Define transmition efficiency etaT=PD/PB in [-]
                RDS['etaT'] = RDS['etaS'] * RDS['etaB']

                # Define last element in Uref that is not NaN
                last = LE.LastElement(RDS['U0MT'])

                # Check if model tests are given in terms of "Delivered", "Shaft" or "Brake"
                # power and redefine it as DELIVERED
                if RDS['PowerTypeMT'] == 'D':
                    RDS['PD0MT'] = RDS['P0MT']

                elif RDS['PowerTypeMT'] == 'S':
                    RDS['PD0MT'] = RDS['P0MT'] * RDS['etaS']

                elif RDS['PowerTypeMT'] == 'B':
                    RDS['PD0MT'] = RDS['P0MT'] * RDS['etaT']

                else:
                    print('Please define model test PowerType and run again')
                    RDS['PD0MT'] = np.full(RDS['P0MT'].shape, np.nan)

                # Check if sea trials are given in terms of "Delivered", "Shaft" or "Brake"
                # power and redefine it as DELIVERED
                if RDS['PowerTypeSTrial'] == 'D':
                    RDS['PDCorSTrial'] = RDS['PCorSTrial']

                elif RDS['PowerTypeSTrial'] == 'S':
                    RDS['PDCorSTrial'] = RDS['PCorSTrial'] * RDS['etaS']

                elif RDS['PowerTypeSTrial'] == 'B':
                    RDS['PDCorSTrial'] = RDS['PCorSTrial'] * RDS['etaT']

                else:
                    print('Please define sea trials PowerType and run again')
                    RDS['PDCorSTrial'] = np.full(RDS['PCorSTrial'].shape, np.nan)

                # Define model test trial conditions
                # if TrialIndexMT is provided, then the model test trial condition is defined from
                # the reference draughts, displacement and 0% sea margin data
                # print("check-1",RDS['U0MT'][int(RDS['TrialIndexMT'])-1,0:int(last[int(RDS['TrialIndexMT'])]+1)])
                # print("check0",RDS['TrialIndexMT'])
                # print("check1",RDS['U0MT'][int(RDS['TrialIndexMT']),:int(last[int(RDS['TrialIndexMT'])])])
                # print("check",RDS['U0MT'][RDS['TrialIndexMT'][0:last(RDS['TrialIndexMT'])]])
                #     if RDS['U0MT'].ndim == 1:
                #         RDS['U0MT'] = RDS['U0MT'].reshape(1,-1).T
                #     if RDS['PD0MT'].ndim == 1:
                #         RDS['PD0MT'] = RDS['PD0MT'].reshape(1,-1).T
                #     if RDS['N0MT'].ndim == 1:
                #         RDS['N0MT'] = RDS['N0MT'].reshape(1,-1).T
                #     if RDS['TaMT'].ndim == 1:
                #         RDS['TaMT'] = RDS['TaMT'].reshape(1,-1).T
                #     if RDS['TfMT'].ndim == 1:
                #         RDS['TfMT'] = RDS['TfMT'].reshape(1,-1).T
                #     if RDS['VMT'].ndim == 1:
                #         RDS['VMT'] = RDS['VMT'].reshape(1,-1).T

                if (bool(RDS['TrialIndexMT']) == True) and (
                        (RDS['UtrialMT'].size == 0) or (np.isnan(RDS['UtrialMT']).any())) and (
                        (RDS['PtrialMT'].size == 0) or (np.isnan(RDS['PtrialMT']).any())) and (
                        ((RDS['NtrialMT']).size == 0) or (np.isnan(RDS['NtrialMT']).any())):

                    RDS['UtrialMT'] = RDS['U0MT'][int(RDS['TrialIndexMT']) - 1,
                                      0:int(last[int(RDS['TrialIndexMT'] - 1)] + 1)]
                    RDS['PtrialMT'] = RDS['PD0MT'][int(RDS['TrialIndexMT']) - 1,
                                      0:int(last[int(RDS['TrialIndexMT'] - 1)] + 1)]
                    RDS['NtrialMT'] = RDS['N0MT'][int(RDS['TrialIndexMT']) - 1,
                                      0:int(last[int(RDS['TrialIndexMT'] - 1)] + 1)]
                    RDS['TatrialMT'] = RDS['TaMT'][int(RDS['TrialIndexMT'] - 1)]
                    RDS['TftrialMT'] = RDS['TfMT'][int(RDS['TrialIndexMT'] - 1)]
                    RDS['TmtrialMT'] = (RDS['TatrialMT'] + RDS['TftrialMT']) / 2
                    RDS['TmtrialMT'] = np.array([RDS['TmtrialMT']])
                    RDS['VtrialMT'] = RDS['VMT'][int(RDS['TrialIndexMT'] - 1)]

                # if none of the above cases then warn user and terminate program
                else:

                    print('TrialIndexMT, UtrialMT and PtrialMT are empty in the same time')
                    print('Revise the input file and run again')
                    print('BPM Engine Error 1')

                # Calculate open-water propeller coefficients at sea trial
                # Calculate Kq from sea trials
                etaR = 1.02  # 0.9922-0.05908*AeAoProp+0.07424*(Cp-0.0225*lcb)
                etaD = 0.7  # etaR.*etao.*etaH;
                w = 0.2  # c9*c20.*Cv.*Lwl./Ta.*(0.050776+0.93405*c11.*Cv./(1-Cp1))+0.27915*c20*sqrt(B./(Lwl.*(1-Cp1)))+c19*c20;
                t = 0.2  # 0.25014*(B./Lwl).^0.28956.*(sqrt(B*Tm)./dProp).^0.2624./(1-Cp+0.0225*lcb).^0.01762+0.0015*Cstern;

                RDS['UaCorSTrial'] = RDS['UCorSTrial'] * (1 - w) * 0.514444
                RDS['JSTrial'] = RDS['UaCorSTrial'] / (RDS['NCorSTrial'] / 60 * RDS['dProp'])
                RDS['KqSTrial'] = 1000 * RDS['PDCorSTrial'] * etaR / (
                            RDS['rhoswSTrial'] * 2 * np.pi * (RDS['NCorSTrial'] / 60) ** 3 * RDS['dProp'] ** 5)

                RDS['RtSTrial'] = RDS['PDCorSTrial'] * etaD / (RDS['UCorSTrial'] * 0.51444)
                RDS['ThSTrial'] = RDS['RtSTrial'] / (1 - t)
                RDS['KtSTrial'] = 1000 * RDS['ThSTrial'] / (
                            RDS['rhoswSTrial'] * (RDS['NCorSTrial'] / 60) ** 2 * RDS['dProp'] ** 4)
                RDS['etaoSTrial'] = RDS['JSTrial'] / (2 * np.pi) * RDS['KtSTrial'] / RDS['KqSTrial']

                # Find correction factors between sea trials and model tests
                # print("TmtrialMT",RDS['TmtrialMT'])
                # print("TmtrialMT.shape",RDS['TmtrialMT'].shape)

                RDS['PtrialinterMT'] = idp.IdealDeliveredPower(RDS['UCorSTrial'],
                                                               RDS['TmtrialMT'] * np.ones(RDS['UCorSTrial'].shape),
                                                               np.full(RDS['UCorSTrial'].shape, np.nan),
                                                               RDS['TmtrialMT'],
                                                               RDS['PtrialMT'],
                                                               RDS['UtrialMT'],
                                                               RDS['Tb'],
                                                               RDS['Ts'])

                RDS['NtrialinterMT'] = ir.IdealRPM(RDS['UCorSTrial'],
                                                   RDS['TmtrialMT'] * np.ones(RDS['UCorSTrial'].shape),
                                                   np.full(RDS['UCorSTrial'].shape, np.nan),
                                                   RDS['TmtrialMT'],
                                                   RDS['NtrialMT'],
                                                   RDS['UtrialMT'],
                                                   RDS['Tb'],
                                                   RDS['Ts'])
                [RDS['Ktinter'], RDS['Kqinter'], RDS['etaointer']] = bsc.BSeriesCor(RDS['JSTrial'].T, RDS['pitchProp'],
                                                                                    RDS['dProp'], RDS['AeAoProp'],
                                                                                    RDS['zProp'])

                RDS['facp'] = RDS['PDCorSTrial'] / RDS['PtrialinterMT']
                RDS['facn'] = RDS['NCorSTrial'] / RDS['NtrialinterMT']
                RDS['fackq'] = RDS['KqSTrial'].T / RDS['Kqinter']
                RDS['fackt'] = RDS['KtSTrial'].T / RDS['Ktinter']
                RDS['factorP'] = np.mean(RDS['facp'])
                RDS['factorN'] = np.mean(RDS['facn'])
                RDS['factorKt'] = np.mean(RDS['fackt'])
                RDS['factorKq'] = np.mean(RDS['fackq'])

                # Define Hull Reference Experimental Data
                RDS['Uref'] = RDS['U0MT']
                RDS['Pref'] = np.round(RDS['P0MT'] * RDS['factorP'])
                RDS['Nref'] = RDS['N0MT'] * RDS['factorN']
                RDS['etaDref'] = RDS['etaD0MT']

                # Find correction factors between sea trials and ME shop tests
                RDS['PtrialinterMEST'] = idp.IdealDeliveredPower(RDS['NCorSTrial'],
                                                                 RDS['TmtrialMT'] * np.ones(RDS['UCorSTrial'].shape),
                                                                 np.full(RDS['UCorSTrial'].shape, np.nan),
                                                                 RDS['TmtrialMT'],
                                                                 RDS['PeffMEST'][0, :] * RDS['nME'] * RDS['etaeffME'][
                                                                     0] * RDS['etaT'],  # for multiple engines
                                                                 RDS['NMEST'][0, :],
                                                                 RDS['Tb'],
                                                                 RDS['Ts'])

                RDS['NtrialinterMEST'] = IS.IdealSpeed(RDS['PDCorSTrial'],
                                                       RDS['TmtrialMT'] * np.ones(RDS['PDCorSTrial'].shape),
                                                       np.full(RDS['UCorSTrial'].shape, np.nan),
                                                       RDS['TmtrialMT'],
                                                       RDS['PeffMEST'][0, :] * RDS['nME'] * RDS['etaeffME'][0] * RDS[
                                                           'etaT'],  # for multiple engines
                                                       RDS['NMEST'][0, :],
                                                       RDS['Tb'],
                                                       RDS['Ts'])

                RDS['facp2'] = RDS['PDCorSTrial'] / RDS['PtrialinterMEST']
                RDS['facn2'] = RDS['NCorSTrial'] / RDS['NtrialinterMEST']
                RDS['factorP2'] = np.mean(RDS['facp2'])
                RDS['factorN2'] = np.mean(RDS['facn2'])

                # Define Holtrop Method Calibration Factor
                RDS['PratioHoltropref'] = rch.ReferenceCalibrationFactorHoltrop(RDS)

                # Define speed range to correspond to 10-90% MCR range

                RDS['Uminref'] = np.floor(IS.IdealSpeed(np.sum(RDS['PMCRME']) * 0.1,
                                                        np.max(RDS['Tref']),
                                                        np.full(RDS['PMCRME'].shape, float('NaN')),
                                                        RDS['Tref'],
                                                        RDS['Pref'],
                                                        RDS['Uref'],
                                                        RDS['Tb'],
                                                        RDS['Ts']))

                RDS['Umaxref'] = np.ceil(IS.IdealSpeed(np.sum(RDS['PMCRME']) * 0.9,
                                                       np.min(RDS['Tref']),
                                                       np.full(RDS['PMCRME'].shape, float('NaN')),
                                                       RDS['Tref'],
                                                       RDS['Pref'],
                                                       RDS['Uref'],
                                                       RDS['Tb'],
                                                       RDS['Ts']))

                # Hydrostatics Data

                # Define hydrostatics total volume displacement based on total mass displacement
                RDS['Vh'] = 1000 * RDS['Dh'] / RDS['rhoswh']
                # SRQSTD['TotalTimeLapse'].append(time.time()-start_time)
                # SRQSTD['Remarks'].append("complete")
                # return(RDS,SRQSTD)
                v['RDS'] = RDS
            except Exception as eri:
                # raise
                expi = "Reference Curve Error-{} in lineNo. {} for Vessel {}".format(eri, sys.exc_info()[-1].tb_lineno,
                                                                                     imo)
                logger.error(expi, exc_info=True)
                ExceptionHandler("Ref", expi)

                v['RDS'] = "empty"
                rc_status = 0
                pass
            else:
                v['RDS'] = RDS
                pass
    except Exception as err:
        # raise
        expo = "{} in line No. {}".format(err, sys.exc_info()[-1].tb_lineno)
        logger.error(expo, exc_info=True)
        ExceptionHandler("Ref", expo)
        SRQSTD['TotalTimeLapse'] = time.time() - start_time
        SRQSTD['Remarks'] = "failed"
        SRQST.append(SRQSTD)
        return ({}, SRQST)

    else:
        if rc_status == 0:
            SRQSTD['TotalTimeLapse'] = time.time() - start_time
            SRQSTD['Remarks'] = "failed"
            SRQST.append(SRQSTD)
            return ({}, SRQST)
        else:
            SRQSTD['TotalTimeLapse'] = time.time() - start_time
            SRQSTD['Remarks'] = "complete"
            SRQST.append(SRQSTD)
            records_df = records_df[records_df['RDS'] != "empty"]
            return (records_df, SRQST)


# -*- coding: utf-8 -*-
"""
Created on Fri Oct  9 16:55:57 2020

@author: SUN
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 12:12:40 2020

@author: Subhin Antony

 ReferenceCurves
 Description:  This function generates the reference curves.

 Input:        RDS

 Output:       RDS

 See also:     BPMEngineSingleVessel


 Disclaimer:
 -----------
 BSM retains the intellectual rights of all developments/process
 enhancements for these changes and amendments, which will be ongoing.
 Sharing of this information outside of the BSM Group should have prior
 written permission from the authors and BSM Corporate Fleet Management.

 History:
 --------
 06/05/2020 version 1.26
 29/03/2020 version 1.25
 20/12/2019 version 1.24
 16/12/2019 version 1.23
 06/12/2019 version 1.22
 09/09/2019 version 1.21
 15/08/2019 version 1.20
 15/07/2019 version 1.19
 18/04/2019 version 1.18
 08/04/2019 version 1.17
 04/12/2018 version 1.16
 07/11/2018 version 1.15
 07/11/2018 version 1.14
 05/11/2018 version 1.13
 17/07/2018 version 1.12
 10/07/2018 version 1.11
 23/05/2018 version 1.10
 03/05/2018 version 1.09
 25/04/2018 version 1.08
 13/10/2017 version 1.07
 06/10/2017 version 1.06
 07/09/2017 version 1.05
 31/07/2017 version 1.04
 28/07/2017 version 1.03
 28/07/2017 version 1.02
 12/06/2017 version 1.01
 06/06/2017 version 1.00

"""
import sys
from BPME_Common.pchipwrap import pchipwrap as pchip
# from scipy.interpolate import pchip
import numpy as np
import math
# from BPME_Repository import BPMEngineSingleVessel as bpmsv
from ExceptionHandler import ExceptionHandler
from BPME_Transform import AeAoKeller as ae
from BPME_Transform import STNormalization as st
from BPME_Transform import EffectivePower as ep
from BPME_Common import SFOCiso as sf
from BPME_Common import ISOCorrections as ic
from BPME_Common import TCefficiency as te
from BPME_Transform import CylinderConstants as cc
from BPME_Common import find as f
from BPME_Common import RoundTwo as rt
from BPME_Common import IdealDeliveredPower as idp
from BPME_Common import IdealSpeed as IS
from BPME_Common import IdealRPM as ir
from BPME_Common import LastElement as LE
from BPME_Transform import ReferenceCalibrationFactorHoltrop as rch
from BPME_Common import SeawaterDensity as swd
from BPME_Transform import BSeriesCor as bsc
from BPME_Transform import etagenCalculator as etgc
import time
import logging

# create logger
logger = logging.getLogger('BPMEngine.2.02')


# def ReferenceCurves(RDS,LogType):

# RDS=bpmsv.RDS
# LogType = 'Noon'

def ReferenceCurves(records_df, start_time, SRQST):
    SRQSTD = {}
    SRQSTD['StepTimeLapse'] = time.time() - start_time
    SRQSTD['Stage'] = __name__
    rc_status = -1
    try:
        for i, v in records_df.iterrows():
            imo = v.imo
            try:

                RDS = v['RDSraw']

                if RDS['U0MT'].ndim == 1:
                    RDS['U0MT'] = RDS['U0MT'].reshape(1, -1)
                if RDS['P0MT'].ndim == 1:
                    RDS['P0MT'] = RDS['P0MT'].reshape(1, -1)
                if RDS['N0MT'].ndim == 1:
                    RDS['N0MT'] = RDS['N0MT'].reshape(1, -1)

                # Estimate Missing Reference Data

                # Estimate etaB

                if math.isnan(RDS['etaB']):
                    RDS['etaB'] = 1
                    print('etaB is estimated!')

                # Estimate etaS
                if math.isnan(RDS['etaS']):
                    RDS['etaS'] = 0.99
                    print('etaS is estimated!')

                # Estimate etaeffME
                if np.any(np.isnan(RDS['etaeffME'])):
                    RDS['etaeffME'] = np.ones(int(RDS['nME'])) * 0.99
                    print('etaeffME is estimated!')

                # Estimate PowerTypeMEST
                # This rule needs modification but also to fix the m-file export format to not mix NaNs in a string vector.
                if 'NaN' in RDS['PowerTypeMEST']:
                    RDS['PowerTypeMEST'] = np.array(['E' for i in range(0, int(RDS['nME']))])
                    print('PowerTypeMEST is estimated!')

                # #Estimate AuxBlowMEST
                # if any(np.isnan(RDS['AuxBlowMEST'])):
                #     AuxBlowMESTtemp=np.zeros(len(RDS['MCRMEST']))
                #     index=[i for i ,X in enumerate(RDS['MCRMEST']) if X<=30]
                #     for i in index:
                #         AuxBlowMESTtemp[i]=1
                #     index_AuxBlowMEST=[i for i ,X in enumerate(RDS['AuxBlowMEST']) if not(math.isnan(X))]
                #     for i in index_AuxBlowMEST:
                #         AuxBlowMESTtemp[i]=RDS['AuxBlowMEST'][i]
                #     RDS[AuxBlowMEST]=AuxBlowMESTtemp;
                #     print('AuxBlowMEST is estimated!')

                # Estimate AuxBlowMEST
                if np.any(np.isnan(RDS['AuxBlowMEST'])):
                    AuxBlowMESTtemp = np.zeros(RDS['MCRMEST'].shape)
                    AuxBlowMESTtemp[RDS['MCRMEST'] <= 30] = 1
                    AuxBlowMESTtemp[np.flatnonzero(~(np.isnan(RDS['AuxBlowMEST'])))] = RDS['AuxBlowMEST'][
                        np.flatnonzero(~(np.isnan(RDS['AuxBlowMEST'])))]
                    # AuxBlowMESTtemp[~(np.isnan(RDS['AuxBlowMEST']))]=RDS['AuxBlowMEST'][~(np.isnan(RDS['AuxBlowMEST']))]
                    RDS['AuxBlowMEST'] = AuxBlowMESTtemp
                    print('AuxBlowMEST is estimated!')

                # Estimate muTCME

                if np.any(np.isnan(RDS['muTCME'])):
                    if RDS['nME'] == 1:
                        muTCMEtemp = np.ones(int(np.max(RDS['nTCME']))) * 0.7
                    else:
                        muTCMEtemp = np.ones((int(RDS['nME']), int(np.max(RDS['nTCME'])))) * 0.7

                    # suppose all engines have the same number of TCs
                    muTCMEtemp[np.flatnonzero(~(np.isnan(RDS['muTCME'])))] = RDS['muTCME'][
                        np.flatnonzero(~(np.isnan(RDS['muTCME'])))]
                    RDS['muTCME'] = muTCMEtemp
                    print('muTCME is estimated!')

                # a=np.array([1,2,float('NaN')])
                # a[np.isnan(a)] =  np.array([1])
                # print(type(np.isnan(a)))
                # import numpy as np
                # a=np.ones((2,2))*7
                # print(a)
                # b=np.array([[3,float('NaN')],[4,float('NaN')]])
                # print([np.isnan(b)])
                # print(np.isnan(b))
                # a[~(np.isnan(b))]=b[~(np.isnan(b))]
                # print(a)

                # a={
                #    'c' : np.ones((2,2)),
                #    'b' : np.array([[3,float('NaN')],[4,float('NaN')]])
                #    }

                # g=a['c'][~(np.isnan(a['b']))]
                # print(g.shape)

                # Estimate dTCcompME
                if np.any(np.isnan(RDS['dTCcompME'])):
                    if RDS['nME'] == 1:
                        RDS['dTCcompME'] = np.empty(int(np.max(RDS['nTCME'])))
                        RDS['dTCcompME'][:] = np.nan
                    else:
                        RDS['dTCcompME'] = np.empty((int(RDS['nME']), int(np.max(RDS['nTCME']))))
                        RDS['dTCcompME'][:] = np.nan
                    # suppose all engines have the same number of TCs
                    print('dTCcompME is missing!')

                # import numpy as np
                # hh= np.empty((2,2))
                # hh[:]=np.NaN
                # print(hh)
                # print(type(hh[0][0]))

                # Estimate missing AeAoProp
                if math.isnan(RDS['AeAoProp']):
                    # In case of nME>1 the sum of MCR is used.
                    RDS['AeAoProp'] = ae.AeAoKeller(np.sum(RDS['PMCRME']), RDS['nProp'], RDS['dProp'], RDS['zProp'],
                                                    RDS['Ud'], RDS['Td'])
                    print('AeAoProp is estimated!')
                # Estimate Cstern
                if RDS['Cstern'] == 'NaN':
                    RDS['Cstern'] = "N"
                    print('Cstern is estimated!')
                # Estimate rhoswSTrial
                if math.isnan(RDS['rhoswSTrial']):
                    RDS['rhoswSTrial'] = swd.SeawaterDensity(15, 35.16504)
                    print('rhoswSTrial is estimated!')
                # Estimate generator efficiency if AE modelling is not verified
                if not ('Y' == RDS['AEVerModel']):
                    RDS['etagenAE'] = 0.96 * np.ones(np.shape(RDS['PengnomAE']))
                if RDS['PengnomAE'].ndim == 1:
                    RDS['PengnomAE'] = RDS['PengnomAE'].reshape(1, -1)

                # ME Shop Test Data

                # Normalize ME shop test data
                # This step is to average multiple entries at 100% and to remove 110% data
                MCRMESTraw = RDS['MCRMEST']
                # RDS['MCRMEST']=np.array([])
                [RDS['NMEST'], RDS['MCRMEST']] = st.STNormalization(RDS['NMEST'], MCRMESTraw)
                [RDS['PMEST'], temp] = st.STNormalization(RDS['PMEST'], MCRMESTraw)
                [RDS['pambairMEST'], temp] = st.STNormalization(RDS['pambairMEST'], MCRMESTraw)
                [RDS['NTCMEST'], temp] = st.STNormalization(RDS['NTCMEST'], MCRMESTraw)
                [RDS['TscavMEST'], temp] = st.STNormalization(RDS['TscavMEST'], MCRMESTraw)
                [RDS['FOCMEST'], temp] = st.STNormalization(RDS['FOCMEST'], MCRMESTraw)
                [RDS['SFOCMEST'], temp] = st.STNormalization(RDS['SFOCMEST'], MCRMESTraw)
                [RDS['SFOCisoMEST'], temp] = st.STNormalization(RDS['SFOCisoMEST'], MCRMESTraw)
                [RDS['peffMEST'], temp] = st.STNormalization(RDS['peffMEST'], MCRMESTraw)
                [RDS['FPIMEST'], temp] = st.STNormalization(RDS['FPIMEST'], MCRMESTraw)
                [RDS['TFOPPinMEST'], temp] = st.STNormalization(RDS['TFOPPinMEST'], MCRMESTraw)
                [RDS['pindMEST'], temp] = st.STNormalization(RDS['pindMEST'], MCRMESTraw)
                [RDS['pmaxMEST'], temp] = st.STNormalization(RDS['pmaxMEST'], MCRMESTraw)
                [RDS['pcompMEST'], temp] = st.STNormalization(RDS['pcompMEST'], MCRMESTraw)
                [RDS['pscavMEST'], temp] = st.STNormalization(RDS['pscavMEST'], MCRMESTraw)
                [RDS['TcwACinMEST'], temp] = st.STNormalization(RDS['TcwACinMEST'], MCRMESTraw)
                [RDS['TcwACoutMEST'], temp] = st.STNormalization(RDS['TcwACoutMEST'], MCRMESTraw)
                [RDS['TairACinMEST'], temp] = st.STNormalization(RDS['TairACinMEST'], MCRMESTraw)
                [RDS['TairACoutMEST'], temp] = st.STNormalization(RDS['TairACoutMEST'], MCRMESTraw)
                [RDS['dpairACMEST'], temp] = st.STNormalization(RDS['dpairACMEST'], MCRMESTraw)
                [RDS['dpairAFMEST'], temp] = st.STNormalization(RDS['dpairAFMEST'], MCRMESTraw)
                [RDS['TairTCinMEST'], temp] = st.STNormalization(RDS['TairTCinMEST'], MCRMESTraw)
                [RDS['TegEVoutMEST'], temp] = st.STNormalization(RDS['TegEVoutMEST'], MCRMESTraw)
                [RDS['TegTCinMEST'], temp] = st.STNormalization(RDS['TegTCinMEST'], MCRMESTraw)
                [RDS['TegTCoutMEST'], temp] = st.STNormalization(RDS['TegTCoutMEST'], MCRMESTraw)
                [RDS['pegRMEST'], temp] = st.STNormalization(RDS['pegRMEST'], MCRMESTraw)
                [RDS['pegTCinMEST'], temp] = st.STNormalization(RDS['pegTCinMEST'], MCRMESTraw)
                [RDS['pegTCoutMEST'], temp] = st.STNormalization(RDS['pegTCoutMEST'], MCRMESTraw)
                [RDS['AuxBlowMEST'], temp] = st.STNormalization(RDS['AuxBlowMEST'], MCRMESTraw)

                # Calculate effective power in kW
                RDS['PeffMEST'] = ep.EffectivePower(RDS['PMEST'], RDS['etaeffME'], RDS['PowerTypeMEST'], RDS['nMEST'],
                                                    RDS['NoME'])

                # Calculate SFOC and SFOCiso in g/kWh
                RDS['SFOCcalcMEST'] = 1000 * RDS['FOCMEST'] / RDS['PeffMEST']
                rds = []
                for i in range(0, int(RDS['nMEST'])):
                    if RDS['SFOCcalcMEST'].ndim == 1:
                        RDS['SFOCcalcMEST'] = RDS['SFOCcalcMEST'].reshape(1, -1)
                    if RDS['TcwACinMEST'].ndim == 1:
                        RDS['TcwACinMEST'] = RDS['TcwACinMEST'].reshape(1, -1)
                    if RDS['TairTCinMEST'].ndim == 1:
                        RDS['TairTCinMEST'] = RDS['TairTCinMEST'].reshape(1, -1)
                    if RDS['pambairMEST'].ndim == 1:
                        RDS['pambairMEST'] = RDS['pambairMEST'].reshape(1, -1)

                    k = sf.SFOCiso(RDS['SFOCcalcMEST'][i, :], \
                                   RDS['LCVFOMEST'][i], \
                                   RDS['LCVMDOref'], \
                                   RDS['TcwACinMEST'][i, :], \
                                   RDS['TairTCinMEST'][i, :], \
                                   RDS['pambairMEST'][i, :])
                    rds.append(k.ravel())
                RDS['SFOCisocalcMEST'] = np.array(rds)

                if np.any(np.isnan(RDS['SFOCisocalcMEST'])):
                    if RDS['SFOCisoMEST'].ndim == 1:
                        RDS['SFOCisoMEST'] = RDS['SFOCisoMEST'].reshape(1, -1)
                    RDS['SFOCisocalcMEST'] = RDS['SFOCisoMEST']
                    print('SFOCiso for ME Shop Test cannot be calculated!')

                # Correct 4 reference variables for ISO conditions as per MAN manual, section 706-06
                RDS['TegEVoutisocalcMEST'] = ic.ISOCorrections(RDS['TegEVoutMEST'], RDS['TairTCinMEST'],
                                                               RDS['TcwACinMEST'], RDS['F1iso'][0], RDS['F2iso'][0],
                                                               RDS['Kiso'][0])
                RDS['pscavisocalcMEST'] = ic.ISOCorrections(RDS['pscavMEST'], RDS['TairTCinMEST'], RDS['TcwACinMEST'],
                                                            RDS['F1iso'][1], RDS['F2iso'][1], RDS['Kiso'][1])
                RDS['pcompisocalcMEST'] = ic.ISOCorrections(RDS['pcompMEST'], RDS['TairTCinMEST'], RDS['TcwACinMEST'],
                                                            RDS['F1iso'][2], RDS['F2iso'][2], RDS['Kiso'][2])
                RDS['pmaxisocalcMEST'] = ic.ISOCorrections(RDS['pmaxMEST'], RDS['TairTCinMEST'], RDS['TcwACinMEST'],
                                                           RDS['F1iso'][3], RDS['F2iso'][3], RDS['Kiso'][3])

                # Calculate absolute pressures
                RDS['pscavabsisocalcMEST'] = RDS['pscavisocalcMEST'] + RDS['pambairMEST'] / 1000;
                RDS['pcompabsisocalcMEST'] = RDS['pcompisocalcMEST'] + RDS['pambairMEST'] / 1000;
                RDS['pmaxabsisocalcMEST'] = RDS['pmaxisocalcMEST'] + RDS['pambairMEST'] / 1000;

                # Calculate pressure rise in bar
                RDS['pmax_pcompisoMEST'] = RDS['pmaxabsisocalcMEST'] - RDS['pcompabsisocalcMEST']

                # Calculate pressure ratio in
                RDS['pcomp_pscavabsisoMEST'] = RDS['pcompabsisocalcMEST'] / RDS['pscavabsisocalcMEST']

                # Calculate temperature differences in C
                RDS['dTaircwACMEST'] = RDS['TairACoutMEST'] - RDS['TcwACinMEST']
                RDS['dTcwACMEST'] = RDS['TcwACoutMEST'] - RDS['TcwACinMEST']

                # Calculate TC efficiencies

                if RDS['pambairMEST'].ndim == 1:
                    RDS['pambairMEST'] = RDS['pambairMEST'].reshape(1, -1)
                if RDS['dpairACMEST'].ndim == 1:
                    RDS['dpairACMEST'] = RDS['dpairACMEST'].reshape(1, -1)
                if RDS['TairTCinMEST'].ndim == 1:
                    RDS['TairTCinMEST'] = RDS['TairTCinMEST'].reshape(1, -1)
                if RDS['NTCMEST'].ndim == 1:
                    RDS['NTCMEST'] = RDS['NTCMEST'].reshape(1, -1)
                if RDS['pscavMEST'].ndim == 1:
                    RDS['pscavMEST'] = RDS['pscavMEST'].reshape(1, -1)
                if RDS['pegRMEST'].ndim == 1:
                    RDS['pegRMEST'] = RDS['pegRMEST'].reshape(1, -1)
                if RDS['pegTCoutMEST'].ndim == 1:
                    RDS['pegTCoutMEST'] = RDS['pegTCoutMEST'].reshape(1, -1)
                if RDS['TegTCinMEST'].ndim == 1:
                    RDS['TegTCinMEST'] = RDS['TegTCinMEST'].reshape(1, -1)
                if RDS['dTCcompME'].ndim == 1:
                    RDS['dTCcompME'] = RDS['dTCcompME'].reshape(1, -1)
                if RDS['muTCME'].ndim == 1:
                    RDS['muTCME'] = RDS['muTCME'].reshape(1, -1)

                etatm = []
                etacm = []
                etatum = []
                for i in range(0, int(RDS['nMEST'])):
                    j = int(RDS['NoME'][i]) - int(1)
                    RDS_etaTCtotcalcMEST, RDS_etaTCcompcalcMEST, RDS_etaTCturbcalcMEST = te.TCefficiency(
                        RDS['pambairMEST'][i, :], RDS['dpairACMEST'][i, :], RDS['TairTCinMEST'][i, :],
                        RDS['NTCMEST'][i, :], RDS['pscavMEST'][i, :], RDS['pegRMEST'][i, :], RDS['pegTCoutMEST'][i, :],
                        RDS['TegTCinMEST'][i, :], RDS['dTCcompME'][j, 0], RDS['muTCME'][j, 0])
                    etatm.append(RDS_etaTCtotcalcMEST.ravel())
                    etacm.append(RDS_etaTCcompcalcMEST.ravel())
                    etatum.append(RDS_etaTCturbcalcMEST.ravel())
                RDS['etaTCtotcalcMEST'] = np.array(etatm)
                RDS['etaTCcompcalcMEST'] = np.array(etacm)
                RDS['etaTCturbcalcMEST'] = np.array(etatum)

                # Define cylinder constants
                RDS['k1'], RDS['k2'] = cc.CylinderConstants(RDS['Ck2'], RDS['dPISTME'], RDS['sPISTME'], RDS['ModelME'])

                # Dual Fuel Engines

                if 'Yes' in RDS['DualFuelME']:
                    # Define representative engine No for each applicable fuel mode
                    RDS['RepMEFO'] = f.find(RDS['FuelModeMEST'], 'F', 'first')
                    RDS['RepMEGO'] = f.find(RDS['FuelModeMEST'], 'G', 'first')

                    [RDS['TGOinMEST'], temp] = st.STNormalization(RDS['TGOinMEST'], MCRMESTraw)
                    [RDS['pGOinMEST'], temp] = st.STNormalization(RDS['pGOinMEST'], MCRMESTraw)
                    [RDS['GOCMEST'], temp] = st.STNormalization(RDS['GOCMEST'], MCRMESTraw)
                    [RDS['SGOCMEST'], temp] = st.STNormalization(RDS['SGOCMEST'], MCRMESTraw)
                    [RDS['SGOCisoMEST'], temp] = st.STNormalization(RDS['SGOCisoMEST'], MCRMESTraw)

                    RDS['SGOCcalcMEST'] = 1000 * RDS['GOCMEST'] / RDS['PeffMEST']
                    if RDS['SGOCcalcMEST'].ndim == 1:
                        RDS['SGOCcalcMEST'] = RDS['SGOCcalcMEST'].reshape(1, -1)
                    if RDS['TcwACinMEST'].ndim == 1:
                        RDS['TcwACinMEST'] = RDS['TcwACinMEST'].reshape(1, -1)
                    if RDS['TairTCinMEST'].ndim == 1:
                        RDS['TairTCinMEST'] = RDS['TairTCinMEST'].reshape(1, -1)
                    if RDS['pambairMEST'].ndim == 1:
                        RDS['pambairMEST'] = RDS['pambairMEST'].reshape(1, -1)
                    sgoc = []
                    for i in range(0, int(RDS['nMEST'])):
                        sfo = sf.SFOCiso(RDS['SGOCcalcMEST'][i, :], \
                                         RDS['LCVGOMEST'][i], \
                                         RDS['LCVMDOref'], \
                                         RDS['TcwACinMEST'][i, :], \
                                         RDS['TairTCinMEST'][i, :], \
                                         RDS['pambairMEST'][i, :])
                        sgoc.append(sfo.ravel())
                    RDS['SGOCisocalcMEST'] = np.array(sgoc)
                    for i in range(0, int(RDS['nMEST'])):
                        if np.any(np.isnan(RDS['SGOCisocalcMEST'][i, :])):
                            RDS['SGOCisocalcMEST'][i, :] = RDS['SGOCisoMEST'][i, :]
                            print('SGOCiso for ME Shop Test cannot be calculated!')

                # AE Shop Test Data
                # Calculate generator efficiency for all AE loads and initialize the generator load if AE is not modelled
                RDS['etagenAEST'], RDS['MCRgenAEST'] = etgc.etagenCalculator(RDS['etagenAEST'], RDS['MCRAEST'])

                # Calculate real generator power in kW
                RDS['PgennomRAE'] = RDS['PgennomAAE'] * RDS['PFnomAE']
                if RDS['PgennomRAE'].ndim == 1:
                    RDS['PgennomRAE'] = RDS['PgennomRAE'].reshape(1, -1)

                if 'Y' in RDS['AEVerModel']:  # removed all log type
                    # LoadTypeAEST must be identical accross engines
                    LTAE = np.unique(RDS['LoadTypeAEST'])
                    # Define engine power and generator real power for all loads in kW
                    if LTAE == 'E':
                        if RDS['PengnomAE'].ndim == 1:
                            RDS['PengAEST'] = RDS['PengnomAE'].reshape(1, -1).T * RDS['MCRAEST'] / 100
                        else:
                            RDS['PengAEST'] = RDS['PengnomAE'].T * RDS['MCRAEST'] / 100
                        RDS['PgenRAEST'] = RDS['PengAEST'] * RDS['etagenAEST']
                        MCRAESTraw = RDS['MCRAEST']
                    elif LTAE == 'G':
                        RDS['PgenRAEST'] = (RDS['PgennomRAE'].T) * RDS['MCRAEST'] / 100
                        RDS['PengAEST'] = RDS['PgenRAEST'] / RDS['etagenAEST']
                        MCRAESTraw = RDS['PengAEST'] / (RDS['PengnomAE'].T) * 100
                    # Handle missing FOC info
                    for i in range(0, int(RDS['nAEST'])):
                        if (np.count_nonzero(~np.isnan(RDS['FOCAEST'][i, :])) > 0) and np.count_nonzero(
                                ~np.isnan(RDS['FOCAEST'][i, :])) < len(MCRAESTraw[i, :]):
                            pchip_1 = pchip(np.concatenate((np.array([0]), MCRAESTraw[i, :])),
                                            np.concatenate((np.array([0]), RDS['FOCAEST'][i, :])))
                            RDS['FOCAEST'][i, :] = pchip_1(MCRAESTraw[i, :])
                    # Handle missing SFOC info
                    if np.all(np.size(RDS['SFOCAEST']) == np.size(MCRAESTraw)):
                        RDS['SFOCAEST'][np.isnan(RDS['SFOCAEST'])] = 1000 * RDS['FOCAEST'][np.isnan(RDS['SFOCAEST'])] / \
                                                                     RDS['PengAEST'][np.isnan(RDS['SFOCAEST'])]
                    elif np.all(np.isnan(RDS['SFOCAEST'])):
                        RDS['SFOCAEST'] = 1000 * RDS['FOCAEST'] / RDS['PengAEST']
                    # Handle missing SFOCiso info
                    if np.all(np.size(RDS['SFOCisoAEST']) == np.size(MCRAESTraw)) and np.all(
                            np.size(RDS['SFOCisoAEST']) == np.size(RDS['TairTCinAEST'])) and np.all(
                            np.size(RDS['SFOCisoAEST']) == np.size(RDS['pambairAEST'])):
                        for i in range(0, int(RDS['nAEST'])):
                            RDS['SFOCisoAEST'][i, np.isnan(RDS['SFOCisoAEST'][i, :])] = sf.SFOCiso(
                                RDS['SFOCAEST'][i, np.isnan(RDS['SFOCisoAEST'][i, :])], RDS['LCVFOAEST'][i],
                                RDS['LCVMDOref'], RDS['TcwACinAEST'][i, np.isnan(RDS['SFOCisoAEST'][i, :])],
                                RDS['TairTCinAEST'][i, np.isnan(RDS['SFOCisoAEST'][i, :])],
                                RDS['pambairAEST'][i, np.isnan(RDS['SFOCisoAEST'][i, :])])

                    # Normalize AE shop test data
                    # MCRAESTraw=RDS['MCRAEST']
                    [RDS['NAEST'], RDS['MCRAEST']] = st.STNormalization(RDS['NAEST'], MCRAESTraw)
                    [RDS['etagenAEST'], temp] = st.STNormalization(RDS['etagenAEST'], MCRAESTraw)
                    [RDS['PengAEST'], temp] = st.STNormalization(RDS['PengAEST'], MCRAESTraw)
                    [RDS['PgenRAEST'], temp] = st.STNormalization(RDS['PgenRAEST'], MCRAESTraw)
                    [RDS['FPIAEST'], temp] = st.STNormalization(RDS['FPIAEST'], MCRAESTraw)
                    [RDS['TFOPPinAEST'], temp] = st.STNormalization(RDS['TFOPPinAEST'], MCRAESTraw)
                    [RDS['pambairAEST'], temp] = st.STNormalization(RDS['pambairAEST'], MCRAESTraw)
                    [RDS['NTCAEST'], temp] = st.STNormalization(RDS['NTCAEST'], MCRAESTraw)
                    [RDS['TairACinAEST'], temp] = st.STNormalization(RDS['TairACinAEST'], MCRAESTraw)
                    [RDS['TscavAEST'], temp] = st.STNormalization(RDS['TscavAEST'], MCRAESTraw)
                    [RDS['FOCAEST'], temp] = st.STNormalization(RDS['FOCAEST'], MCRAESTraw)
                    [RDS['SFOCAEST'], temp] = st.STNormalization(RDS['SFOCAEST'], MCRAESTraw)
                    [RDS['SFOCisoAEST'], temp] = st.STNormalization(RDS['SFOCisoAEST'], MCRAESTraw)
                    [RDS['pmaxAEST'], temp] = st.STNormalization(RDS['pmaxAEST'], MCRAESTraw)
                    [RDS['pcompAEST'], temp] = st.STNormalization(RDS['pcompAEST'], MCRAESTraw)
                    [RDS['pscavAEST'], temp] = st.STNormalization(RDS['pscavAEST'], MCRAESTraw)
                    [RDS['TcwACinAEST'], temp] = st.STNormalization(RDS['TcwACinAEST'], MCRAESTraw)
                    [RDS['TcwACoutAEST'], temp] = st.STNormalization(RDS['TcwACoutAEST'], MCRAESTraw)
                    [RDS['TairTCinAEST'], temp] = st.STNormalization(RDS['TairTCinAEST'], MCRAESTraw)
                    [RDS['TegEVoutAEST'], temp] = st.STNormalization(RDS['TegEVoutAEST'], MCRAESTraw)
                    [RDS['TegTCinAEST'], temp] = st.STNormalization(RDS['TegTCinAEST'], MCRAESTraw)
                    [RDS['TegTCoutAEST'], temp] = st.STNormalization(RDS['TegTCoutAEST'], MCRAESTraw)
                    [RDS['dpairACAEST'], temp] = st.STNormalization(RDS['dpairACAEST'], MCRAESTraw)
                    [RDS['dpairAFAEST'], temp] = st.STNormalization(RDS['dpairAFAEST'], MCRAESTraw)
                    # Calculate generator percentage load
                    RDS['MCRgenAEST'] = []
                    RDS['MCRgenAEST'] = RDS['PgenRAEST'] / (RDS['PgennomRAE'].T) * 100

                    # Calculate SFOC and SFOCiso in g/kWh
                    RDS['SFOCcalcAEST'] = 1000 * RDS['FOCAEST'] / RDS['PengAEST']
                    SFAEST = []
                    for i in range(0, int(RDS['nAEST'])):
                        aest = sf.SFOCiso(RDS['SFOCcalcAEST'][i, :], \
                                          RDS['LCVFOAEST'][i], \
                                          RDS['LCVMDOref'], \
                                          RDS['TcwACinAEST'][i, :], \
                                          RDS['TairTCinAEST'][i, :], \
                                          RDS['pambairAEST'][i, :])
                        SFAEST.append(aest.ravel())
                    RDS['SFOCisocalcAEST'] = np.array(SFAEST)
                    if np.any(np.isnan(RDS['SFOCisocalcAEST'])):
                        RDS['SFOCisocalcAEST'] = RDS['SFOCisoAEST']
                        print('SFOCiso for AE Shop Test cannot be calculated!')
                    # Correct 4 reference variables for ISO conditions as per MAN manual, section 706-06
                    RDS['TegEVoutisocalcAEST'] = ic.ISOCorrections(RDS['TegEVoutAEST'], RDS['TairTCinAEST'],
                                                                   RDS['TcwACinAEST'], RDS['F1iso'][0], RDS['F2iso'][0],
                                                                   RDS['Kiso'][0])
                    RDS['pscavisocalcAEST'] = ic.ISOCorrections(RDS['pscavAEST'], RDS['TairTCinAEST'],
                                                                RDS['TcwACinAEST'], RDS['F1iso'][1], RDS['F2iso'][1],
                                                                RDS['Kiso'][1])
                    RDS['pmaxisocalcAEST'] = ic.ISOCorrections(RDS['pmaxAEST'], RDS['TairTCinAEST'], RDS['TcwACinAEST'],
                                                               RDS['F1iso'][3], RDS['F2iso'][3], RDS['Kiso'][3])
                    RDS['TegTCinisocalcAEST'] = RDS['TegTCinAEST']  # must add correction
                    RDS['TegTCoutisocalcAEST'] = RDS['TegTCoutAEST']  # must add correction

                    # Calculate absolute pressures
                    RDS['pscavabsisocalcAEST'] = RDS['pscavisocalcAEST'] + RDS['pambairAEST'] / 1000
                    RDS['pmaxabsisocalcAEST'] = RDS['pmaxisocalcAEST'] + RDS['pambairAEST'] / 1000
                    # Calculate generator efficiency
                    # RDS['etagenAE']=np.mean(RDS['PgenAEST']/RDS['PengAEST'],axis=1).T
                    # if np.all(np.isnan(RDS['etagenAE'])):
                    #     RDS['etagenAE']=0.96*np.ones(np.shape(RDS['PengnomAE']))

                    # Dual Fuel Engines
                    if ('Yes' in RDS['DualFuelAE']):
                        [RDS['TGOinAEST'], temp] = st.STNormalization(RDS['TGOinAEST'], MCRAESTraw)
                        [RDS['pGOinAEST'], temp] = st.STNormalization(RDS['pGOinAEST'], MCRAESTraw)
                        [RDS['GOCAEST'], temp] = st.STNormalization(RDS['GOCAEST'], MCRAESTraw)
                        [RDS['SGOCAEST'], temp] = st.STNormalization(RDS['SGOCAEST'], MCRAESTraw)
                        [RDS['SGOCisoAEST'], temp] = st.STNormalization(RDS['SGOCisoAEST'], MCRAESTraw)
                        RDS['SGOCcalcAEST'] = 1000 * RDS['GOCAEST'] / RDS['PengAEST']
                        SGAEST = []
                        for i in range(0, int(RDS['nAEST'])):
                            goaest = sf.SFOCiso(RDS['SGOCcalcAEST'][i, :], \
                                                RDS['LCVGOAEST'][i], \
                                                RDS['LCVMDOref'], \
                                                RDS['TcwACinAEST'][i, :], \
                                                RDS['TairTCinAEST'][i, :], \
                                                RDS['pambairAEST'][i, :])
                            SGAEST.append(goaest.ravel())
                        RDS['SGOCisocalcAEST'] = np.array(SGAEST)
                        for i in range(0, int(RDS['nAEST'])):
                            if np.any(np.isnan(RDS['SGOCisocalcAEST'][i, :])):
                                RDS['SGOCisocalcAEST'][i, :] = RDS['SGOCisoAEST'][i, :]
                                print('SGOCiso for AE Shop Test cannot be calculated!')

                ###############################################################
                ###############################################################
                ###############################################################

                # Model Test and Sea Trials Data

                # Define reference draughts as the model test mean draughts in [m]
                # RDS['Tref']=np.around((RDS['TaMT']+RDS['TfMT'])/2,decimals=2)
                RDS['Tref'] = rt.RoundTwo((RDS['TaMT'] + RDS['TfMT']) / 2)

                # Define transmition efficiency etaT=PD/PB in [-]
                RDS['etaT'] = RDS['etaS'] * RDS['etaB']

                # Define last element in Uref that is not NaN
                last = LE.LastElement(RDS['U0MT'])

                # Check if model tests are given in terms of "Delivered", "Shaft" or "Brake"
                # power and redefine it as DELIVERED
                if RDS['PowerTypeMT'] == 'D':
                    RDS['PD0MT'] = RDS['P0MT']

                elif RDS['PowerTypeMT'] == 'S':
                    RDS['PD0MT'] = RDS['P0MT'] * RDS['etaS']

                elif RDS['PowerTypeMT'] == 'B':
                    RDS['PD0MT'] = RDS['P0MT'] * RDS['etaT']

                else:
                    print('Please define model test PowerType and run again')
                    RDS['PD0MT'] = np.full(RDS['P0MT'].shape, np.nan)

                # Check if sea trials are given in terms of "Delivered", "Shaft" or "Brake"
                # power and redefine it as DELIVERED
                if RDS['PowerTypeSTrial'] == 'D':
                    RDS['PDCorSTrial'] = RDS['PCorSTrial']

                elif RDS['PowerTypeSTrial'] == 'S':
                    RDS['PDCorSTrial'] = RDS['PCorSTrial'] * RDS['etaS']

                elif RDS['PowerTypeSTrial'] == 'B':
                    RDS['PDCorSTrial'] = RDS['PCorSTrial'] * RDS['etaT']

                else:
                    print('Please define sea trials PowerType and run again')
                    RDS['PDCorSTrial'] = np.full(RDS['PCorSTrial'].shape, np.nan)

                # Define model test trial conditions
                # if TrialIndexMT is provided, then the model test trial condition is defined from
                # the reference draughts, displacement and 0% sea margin data
                # print("check-1",RDS['U0MT'][int(RDS['TrialIndexMT'])-1,0:int(last[int(RDS['TrialIndexMT'])]+1)])
                # print("check0",RDS['TrialIndexMT'])
                # print("check1",RDS['U0MT'][int(RDS['TrialIndexMT']),:int(last[int(RDS['TrialIndexMT'])])])
                # print("check",RDS['U0MT'][RDS['TrialIndexMT'][0:last(RDS['TrialIndexMT'])]])
                #     if RDS['U0MT'].ndim == 1:
                #         RDS['U0MT'] = RDS['U0MT'].reshape(1,-1).T
                #     if RDS['PD0MT'].ndim == 1:
                #         RDS['PD0MT'] = RDS['PD0MT'].reshape(1,-1).T
                #     if RDS['N0MT'].ndim == 1:
                #         RDS['N0MT'] = RDS['N0MT'].reshape(1,-1).T
                #     if RDS['TaMT'].ndim == 1:
                #         RDS['TaMT'] = RDS['TaMT'].reshape(1,-1).T
                #     if RDS['TfMT'].ndim == 1:
                #         RDS['TfMT'] = RDS['TfMT'].reshape(1,-1).T
                #     if RDS['VMT'].ndim == 1:
                #         RDS['VMT'] = RDS['VMT'].reshape(1,-1).T

                if (bool(RDS['TrialIndexMT']) == True) and (
                        (RDS['UtrialMT'].size == 0) or (np.isnan(RDS['UtrialMT']).any())) and (
                        (RDS['PtrialMT'].size == 0) or (np.isnan(RDS['PtrialMT']).any())) and (
                        ((RDS['NtrialMT']).size == 0) or (np.isnan(RDS['NtrialMT']).any())):

                    RDS['UtrialMT'] = RDS['U0MT'][int(RDS['TrialIndexMT']) - 1,
                                      0:int(last[int(RDS['TrialIndexMT'] - 1)] + 1)]
                    RDS['PtrialMT'] = RDS['PD0MT'][int(RDS['TrialIndexMT']) - 1,
                                      0:int(last[int(RDS['TrialIndexMT'] - 1)] + 1)]
                    RDS['NtrialMT'] = RDS['N0MT'][int(RDS['TrialIndexMT']) - 1,
                                      0:int(last[int(RDS['TrialIndexMT'] - 1)] + 1)]
                    RDS['TatrialMT'] = RDS['TaMT'][int(RDS['TrialIndexMT'] - 1)]
                    RDS['TftrialMT'] = RDS['TfMT'][int(RDS['TrialIndexMT'] - 1)]
                    RDS['TmtrialMT'] = (RDS['TatrialMT'] + RDS['TftrialMT']) / 2
                    RDS['TmtrialMT'] = np.array([RDS['TmtrialMT']])
                    RDS['VtrialMT'] = RDS['VMT'][int(RDS['TrialIndexMT'] - 1)]

                # if none of the above cases then warn user and terminate program
                else:

                    print('TrialIndexMT, UtrialMT and PtrialMT are empty in the same time')
                    print('Revise the input file and run again')
                    print('BPM Engine Error 1')

                # Calculate open-water propeller coefficients at sea trial
                # Calculate Kq from sea trials
                etaR = 1.02  # 0.9922-0.05908*AeAoProp+0.07424*(Cp-0.0225*lcb)
                etaD = 0.7  # etaR.*etao.*etaH;
                w = 0.2  # c9*c20.*Cv.*Lwl./Ta.*(0.050776+0.93405*c11.*Cv./(1-Cp1))+0.27915*c20*sqrt(B./(Lwl.*(1-Cp1)))+c19*c20;
                t = 0.2  # 0.25014*(B./Lwl).^0.28956.*(sqrt(B*Tm)./dProp).^0.2624./(1-Cp+0.0225*lcb).^0.01762+0.0015*Cstern;

                RDS['UaCorSTrial'] = RDS['UCorSTrial'] * (1 - w) * 0.514444
                RDS['JSTrial'] = RDS['UaCorSTrial'] / (RDS['NCorSTrial'] / 60 * RDS['dProp'])
                RDS['KqSTrial'] = 1000 * RDS['PDCorSTrial'] * etaR / (
                            RDS['rhoswSTrial'] * 2 * np.pi * (RDS['NCorSTrial'] / 60) ** 3 * RDS['dProp'] ** 5)

                RDS['RtSTrial'] = RDS['PDCorSTrial'] * etaD / (RDS['UCorSTrial'] * 0.51444)
                RDS['ThSTrial'] = RDS['RtSTrial'] / (1 - t)
                RDS['KtSTrial'] = 1000 * RDS['ThSTrial'] / (
                            RDS['rhoswSTrial'] * (RDS['NCorSTrial'] / 60) ** 2 * RDS['dProp'] ** 4)
                RDS['etaoSTrial'] = RDS['JSTrial'] / (2 * np.pi) * RDS['KtSTrial'] / RDS['KqSTrial']

                # Find correction factors between sea trials and model tests
                # print("TmtrialMT",RDS['TmtrialMT'])
                # print("TmtrialMT.shape",RDS['TmtrialMT'].shape)

                RDS['PtrialinterMT'] = idp.IdealDeliveredPower(RDS['UCorSTrial'],
                                                               RDS['TmtrialMT'] * np.ones(RDS['UCorSTrial'].shape),
                                                               np.full(RDS['UCorSTrial'].shape, np.nan),
                                                               RDS['TmtrialMT'],
                                                               RDS['PtrialMT'],
                                                               RDS['UtrialMT'],
                                                               RDS['Tb'],
                                                               RDS['Ts'])

                RDS['NtrialinterMT'] = ir.IdealRPM(RDS['UCorSTrial'],
                                                   RDS['TmtrialMT'] * np.ones(RDS['UCorSTrial'].shape),
                                                   np.full(RDS['UCorSTrial'].shape, np.nan),
                                                   RDS['TmtrialMT'],
                                                   RDS['NtrialMT'],
                                                   RDS['UtrialMT'],
                                                   RDS['Tb'],
                                                   RDS['Ts'])
                [RDS['Ktinter'], RDS['Kqinter'], RDS['etaointer']] = bsc.BSeriesCor(RDS['JSTrial'].T, RDS['pitchProp'],
                                                                                    RDS['dProp'], RDS['AeAoProp'],
                                                                                    RDS['zProp'])

                RDS['facp'] = RDS['PDCorSTrial'] / RDS['PtrialinterMT']
                RDS['facn'] = RDS['NCorSTrial'] / RDS['NtrialinterMT']
                RDS['fackq'] = RDS['KqSTrial'].T / RDS['Kqinter']
                RDS['fackt'] = RDS['KtSTrial'].T / RDS['Ktinter']
                RDS['factorP'] = np.mean(RDS['facp'])
                RDS['factorN'] = np.mean(RDS['facn'])
                RDS['factorKt'] = np.mean(RDS['fackt'])
                RDS['factorKq'] = np.mean(RDS['fackq'])

                # Define Hull Reference Experimental Data
                RDS['Uref'] = RDS['U0MT']
                RDS['Pref'] = np.round(RDS['P0MT'] * RDS['factorP'])
                RDS['Nref'] = RDS['N0MT'] * RDS['factorN']
                RDS['etaDref'] = RDS['etaD0MT']

                # Find correction factors between sea trials and ME shop tests
                RDS['PtrialinterMEST'] = idp.IdealDeliveredPower(RDS['NCorSTrial'],
                                                                 RDS['TmtrialMT'] * np.ones(RDS['UCorSTrial'].shape),
                                                                 np.full(RDS['UCorSTrial'].shape, np.nan),
                                                                 RDS['TmtrialMT'],
                                                                 RDS['PeffMEST'][0, :] * RDS['nME'] * RDS['etaeffME'][
                                                                     0] * RDS['etaT'],  # for multiple engines
                                                                 RDS['NMEST'][0, :],
                                                                 RDS['Tb'],
                                                                 RDS['Ts'])

                RDS['NtrialinterMEST'] = IS.IdealSpeed(RDS['PDCorSTrial'],
                                                       RDS['TmtrialMT'] * np.ones(RDS['PDCorSTrial'].shape),
                                                       np.full(RDS['UCorSTrial'].shape, np.nan),
                                                       RDS['TmtrialMT'],
                                                       RDS['PeffMEST'][0, :] * RDS['nME'] * RDS['etaeffME'][0] * RDS[
                                                           'etaT'],  # for multiple engines
                                                       RDS['NMEST'][0, :],
                                                       RDS['Tb'],
                                                       RDS['Ts'])

                RDS['facp2'] = RDS['PDCorSTrial'] / RDS['PtrialinterMEST']
                RDS['facn2'] = RDS['NCorSTrial'] / RDS['NtrialinterMEST']
                RDS['factorP2'] = np.mean(RDS['facp2'])
                RDS['factorN2'] = np.mean(RDS['facn2'])

                # Define Holtrop Method Calibration Factor
                RDS['PratioHoltropref'] = rch.ReferenceCalibrationFactorHoltrop(RDS)

                # Define speed range to correspond to 10-90% MCR range

                RDS['Uminref'] = np.floor(IS.IdealSpeed(np.sum(RDS['PMCRME']) * 0.1,
                                                        np.max(RDS['Tref']),
                                                        np.full(RDS['PMCRME'].shape, float('NaN')),
                                                        RDS['Tref'],
                                                        RDS['Pref'],
                                                        RDS['Uref'],
                                                        RDS['Tb'],
                                                        RDS['Ts']))

                RDS['Umaxref'] = np.ceil(IS.IdealSpeed(np.sum(RDS['PMCRME']) * 0.9,
                                                       np.min(RDS['Tref']),
                                                       np.full(RDS['PMCRME'].shape, float('NaN')),
                                                       RDS['Tref'],
                                                       RDS['Pref'],
                                                       RDS['Uref'],
                                                       RDS['Tb'],
                                                       RDS['Ts']))

                # Hydrostatics Data

                # Define hydrostatics total volume displacement based on total mass displacement
                RDS['Vh'] = 1000 * RDS['Dh'] / RDS['rhoswh']
                # SRQSTD['TotalTimeLapse'].append(time.time()-start_time)
                # SRQSTD['Remarks'].append("complete")
                # return(RDS,SRQSTD)
                v['RDS'] = RDS
            except Exception as eri:
                # raise
                expi = "Reference Curve Error-{} in lineNo. {} for Vessel {}".format(eri, sys.exc_info()[-1].tb_lineno,
                                                                                     imo)
                logger.error(expi, exc_info=True)
                ExceptionHandler("Ref", expi)

                v['RDS'] = "empty"
                rc_status = 0
                pass
            else:
                v['RDS'] = RDS
                pass
    except Exception as err:
        # raise
        expo = "{} in line No. {}".format(err, sys.exc_info()[-1].tb_lineno)
        logger.error(expo, exc_info=True)
        ExceptionHandler("Ref", expo)
        SRQSTD['TotalTimeLapse'] = time.time() - start_time
        SRQSTD['Remarks'] = "failed"
        SRQST.append(SRQSTD)
        return ({}, SRQST)

    else:
        if rc_status == 0:
            SRQSTD['TotalTimeLapse'] = time.time() - start_time
            SRQSTD['Remarks'] = "failed"
            SRQST.append(SRQSTD)
            return ({}, SRQST)
        else:
            SRQSTD['TotalTimeLapse'] = time.time() - start_time
            SRQSTD['Remarks'] = "complete"
            SRQST.append(SRQSTD)
            records_df = records_df[records_df['RDS'] != "empty"]
            return (records_df, SRQST)









