# -*- coding: utf-8 -*-
"""
Created on Sun Apr 12 10:37:30 2020

@author: Subhin Antony

% HandleServiceSymbols
% Description:  This function renames symbols making some assumptions.
%
% Input:        SDS (structure)
%               LogType (string)
%
% Output:       SDS (structure)
%
% See also:     BPMEngineSingleVessel

"""
import sys
import time
import logging
from ExceptionHandler import ExceptionHandler 
# create logger
logger = logging.getLogger('BPMEngine.2.02')
def HandleServiceSymbols(IMO,SDS,LogType,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
    
        #Assume that every analysis is based on instantaneous approach
        if 'HP' in LogType:
            SDS['Ta']=SDS['Ta_avg'] 
            del SDS['Ta_avg'] 
            SDS['Tf']=SDS['Tf_avg']
            del SDS['Tf_avg']          
            SDS['DISP']=SDS['DISP_avg']
            del SDS['DISP_avg']            
            SDS['HEAD']=SDS['HEAD_avg']
            del SDS['HEAD_avg']    
            SDS['DOG']=SDS['DOG_dCnt']
            del SDS['DOG_dCnt']
            SDS['DTW']=SDS['DTW_dCnt']
            del SDS['DTW_dCnt'] 
            SDS['azbow']=SDS['azbow_avg']
            del SDS['azbow_avg'] 
            SDS['TEUonDeck']=SDS['TEUonDeck_avg']
            del SDS['TEUonDeck_avg']
            SDS['PelSG']=SDS['PelSG_avg']
            del SDS['PelSG_avg']
            SDS['pitchCPP']=SDS['pitchCPP_avg']
            del SDS['pitchCPP_avg']
            SDS['Uwit']=SDS['Uwit_avg']
            del SDS['Uwit_avg']
            SDS['psiwit']=SDS['psiwit_avg']
            del SDS['psiwit_avg']
            SDS['Ucut']=SDS['Ucut_avg']
            del SDS['Ucut_avg']
            SDS['psicut']=SDS['psicut_avg']
            del SDS['psicut_avg']
            SDS['Hwv']=SDS['Hwv_avg']
            del SDS['Hwv_avg']
            SDS['Twv']=SDS['Twv_avg']
            del SDS['Twv_avg']
            SDS['psiwvt']=SDS['psiwvt_avg']
            del SDS['psiwvt_avg']
            SDS['Hsl']=SDS['Hsl_avg']
            del SDS['Hsl_avg']
            SDS['Tsl']=SDS['Tsl_avg']
            del SDS['Tsl_avg']
            SDS['psislt']=SDS['psislt_avg']
            del SDS['psislt_avg']
            SDS['hsw']=SDS['hsw_avg']
            del SDS['hsw_avg']
            SDS['Tsw']=SDS['Tsw_avg']
            del SDS['Tsw_avg']
            SDS['Tair']=SDS['Tair_avg']
            del SDS['Tair_avg']
            SDS['pair']=SDS['pair_avg']
            del SDS['pair_avg']
            SDS['Uwit_wp']=SDS['Uwit_avg_wp']
            del SDS['Uwit_avg_wp']
            
            SDS['psiwit_wp']=SDS['psiwit_avg_wp']
            del SDS['psiwit_avg_wp']
            
            SDS['Ucut_wp']=SDS['Ucut_avg_wp']
            del SDS['Ucut_avg_wp']
            
            SDS['psicut_wp']=SDS['psicut_avg_wp']
            del SDS['psicut_avg_wp']
            
            SDS['Hwv_wp']=SDS['Hwv_avg_wp']
            del SDS['Hwv_avg_wp']
            SDS['Twv_wp']=SDS['Twv_avg_wp']
            del SDS['Twv_avg_wp']
            
            SDS['psiwvt_wp']=SDS['psiwvt_avg_wp']
            del SDS['psiwvt_avg_wp']
            
            SDS['Hsl_wp']=SDS['Hsl_avg_wp']
            del SDS['Hsl_avg_wp']
            SDS['Tsl_wp']=SDS['Tsl_avg_wp']
            del SDS['Tsl_avg_wp']
            
            SDS['psislt_wp']=SDS['psislt_avg_wp']
            del SDS['psislt_avg_wp']
            
            SDS['hsw_wp']=SDS['hsw_avg_wp']
            del SDS['hsw_avg_wp']
            SDS['Tsw_wp']=SDS['Tsw_avg_wp']
            del SDS['Tsw_avg_wp']
            SDS['Tair_wp']=SDS['Tair_avg_wp']
            del SDS['Tair_avg_wp']
            SDS['pair_wp']=SDS['pair_avg_wp']
            del SDS['pair_avg_wp']
            
            SDS['Sasw_wp']=SDS['Sasw_avg_wp']
            del SDS['Sasw_avg_wp']
            
            SDS['DRME']=SDS['DRME_dCnt']
            del SDS['DRME_dCnt']
            SDS['DESME']=SDS['DESME_dCnt']
            del SDS['DESME_dCnt']
            SDS['PeffestME']=SDS['PeffestME_avg']
            del SDS['PeffestME_avg']
            SDS['VFOCME']=SDS['VFOCME_dCnt']
            del SDS['VFOCME_dCnt']
            SDS['mFOCME']=SDS['mFOCME_dCnt']
            del SDS['mFOCME_dCnt']
            SDS['TFOFMinME']=SDS['TFOFMinME_avg']
            del SDS['TFOFMinME_avg']
            SDS['vFOME']=SDS['vFOME_an']
            del SDS['vFOME_an']
            SDS['rhoFOME']=SDS['rhoFOME_an']
            del SDS['rhoFOME_an']
            SDS['LCVFOME']=SDS['LCVFOME_an']
            del SDS['LCVFOME_an']
            SDS['FPIME']=SDS['FPIME_avg']
            del SDS['FPIME_avg']   
            SDS['NTCME']=SDS['NTCME_avg']
            del SDS['NTCME_avg']    
            SDS['TscavME']=SDS['TscavME_avg']
            del SDS['TscavME_avg']
            SDS['TltcwACinME']=SDS['TltcwACinME_avg']    
            del SDS['TltcwACinME_avg']
            SDS['TambairER']=SDS['TambairER_avg']
            del SDS['TambairER_avg']
            SDS['pambairER']=SDS['pambairER_avg']
            del SDS['pambairER_avg']      
            SDS['mGOCME']=SDS['mGOCME_dCnt']     
            del SDS['mGOCME_dCnt']
            
            SDS['LCVGOME']=SDS['LCVGOME_an']    
            del SDS['LCVGOME_an']
            # return(SDS,SRQST)
    
       
        if 'Auto' in LogType:
            SDS['Ta']=SDS['Ta_avg']    
            del SDS['Ta_avg']      
            SDS['Tf']=SDS['Tf_avg']  
            del SDS['Tf_avg'] 
            SDS['DISP']=SDS['DISP_avg']
            del SDS['DISP_avg']
            SDS['TEUonDeck']=SDS['TEUonDeck_avg']
            del SDS['TEUonDeck_avg']
            #SDS.TambairER=SDS.TambairER_avg
            #SDS.pambairER=SDS.pambairER_avg
            SDS['TltcwACinME']=SDS['TltcwACinME_avg']
            del SDS['TltcwACinME_avg']
            SDS['rhoFOME']=SDS['rhoFOME_an']
            del SDS['rhoFOME_an']
            SDS['LCVFOME']=SDS['LCVFOME_an']
            del SDS['LCVFOME_an']
            SDS['HEAD']=SDS['HEAD_avg_auto']
            del SDS['HEAD_avg_auto']
            SDS['SOG']=SDS['SOG_avg_auto']
            del SDS['SOG_avg_auto']
            SDS['STW']=SDS['STW_avg_auto']
            del SDS['STW_avg_auto']
            SDS['NME']=SDS['NME_avg_auto']
            del SDS['NME_avg_auto']
            SDS['PSME']=SDS['PSME_avg_auto']
            del SDS['PSME_avg_auto']
            SDS['VFOCME']=SDS['VFOCME_dCnt_auto']
            del SDS['VFOCME_dCnt_auto']
            SDS['mFOCME']=SDS['mFOCME_dCnt_auto']
            del SDS['mFOCME_dCnt_auto']
            SDS['TFOFMinME']=SDS['TFOFMinME_avg_auto']
            del SDS['TFOFMinME_avg_auto']
            SDS['Uwit']=SDS['Uwit_avg_auto']
            del SDS['Uwit_avg_auto']
            SDS['psiwit']=SDS['psiwit_avg_auto']
            del SDS['psiwit_avg_auto']
            SDS['hsw']=SDS['hsw_avg_auto']
            del SDS['hsw_avg_auto']  
            
            SDS['Ucut']=SDS['Ucut_avg_wp']
            del SDS['Ucut_avg_wp']
            SDS['psicut']=SDS['psicut_avg_wp']
            del SDS['psicut_avg_wp']
            
            SDS['Hwv']=SDS['Hwv_avg_wp']
            del SDS['Hwv_avg_wp']
            SDS['Twv']=SDS['Twv_avg_wp']
            del SDS['Twv_avg_wp']
            SDS['psiwvt']=SDS['psiwvt_avg_wp']
            del SDS['psiwvt_avg_wp']
            SDS['Hsl']=SDS['Hsl_avg_wp']
            del SDS['Hsl_avg_wp']
            SDS['Tsl']=SDS['Tsl_avg_wp']
            del SDS['Tsl_avg_wp']
            SDS['psislt']=SDS['psislt_avg_wp']
            del SDS['psislt_avg_wp']
            SDS['Tsw']=SDS['Tsw_avg_wp']
            del SDS['Tsw_avg_wp']
            SDS['Tair']=SDS['Tair_avg_wp']
            del SDS['Tair_avg_wp']
            SDS['pair']=SDS['pair_avg_wp']
            del SDS['pair_avg_wp']
            
            SDS['Sasw_wp']=SDS['Sasw_avg_wp']
            del SDS['Sasw_avg_wp']
            SDS['hsw_wp']=SDS['hsw_avg_wp']
            del SDS['hsw_avg_wp']
            # return(SDS,SRQST)
    
        if 'Noon' in LogType:
            SDS['Ta']=SDS['Ta_avg']
            del SDS['Ta_avg']
            SDS['Tf']=SDS['Tf_avg']
            del SDS['Tf_avg']
            SDS['DISP']=SDS['DISP_avg']
            del SDS['DISP_avg']
            SDS['DOG']=SDS['DOG_dCnt']
            del SDS['DOG_dCnt']
            SDS['DTW']=SDS['DTW_dCnt']
            del SDS['DTW_dCnt']
            SDS['azbow']=SDS['azbow_avg']
            del SDS['azbow_avg']
            SDS['TEUonDeck']=SDS['TEUonDeck_avg']
            del SDS['TEUonDeck_avg']
            SDS['DRME']=SDS['DRME_dCnt']
            del SDS['DRME_dCnt']
            SDS['DESME']=SDS['DESME_dCnt']
            del SDS['DESME_dCnt']
            SDS['RHME']=SDS['RHME_dCnt']
            del SDS['RHME_dCnt']
            SDS['mGOCME']=SDS['mGOCME_dCnt']
            del SDS['mGOCME_dCnt']
            
            SDS['LCVGOME']=SDS['LCVGOME_an']
            del SDS['LCVGOME_an']
    #        SDS['PeffestME']=SDS['PeffestME_avg']
    #        del SDS['PeffestME_avg']
            SDS['PelAE']=SDS['PelAE_avg']
            del SDS['PelAE_avg']
            SDS['DEelAE']=SDS['DEelAE_dCnt']
            del SDS['DEelAE_dCnt']
            SDS['RHAE']=SDS['RHAE_dCnt']
            del SDS['RHAE_dCnt']
            SDS['mGOCAE']=SDS['mGOCAE_dCnt']
            del SDS['mGOCAE_dCnt']
            
            
            # return(SDS,SRQST)
    
        if 'ME' in LogType:
            SDS['NME']=SDS['NME_avg']
            del SDS['NME_avg']
            SDS['DESME']=SDS['DESME_dCnt']
            del SDS['DESME_dCnt']
            SDS['PeffestME']=SDS['PeffestME_avg']
            del SDS['PeffestME_avg']
            
            SDS['PindME']=SDS['PindME_avg']
            del SDS['PindME_avg']
            
            #SDS.VFOCME=SDS.VFOCME_dCnt
            SDS['mFOCME']=SDS['mFOCME_dCnt']
            del SDS['mFOCME_dCnt']
            SDS['TFOFMinME']=SDS['TFOFMinME_avg']
            del SDS['TFOFMinME_avg']
            SDS['vFOME']=SDS['vFOME_an']
            del SDS['vFOME_an']
            SDS['rhoFOME']=SDS['rhoFOME_an']
            del SDS['rhoFOME_an']
            SDS['LCVFOME']=SDS['LCVFOME_an']
            del SDS['LCVFOME_an']
            SDS['TscavME']=SDS['TscavME_avg']
            del SDS['TscavME_avg']
            SDS['pscavME']=SDS['pscavME_avg']
            del SDS['pscavME_avg']
            SDS['pegRME']=SDS['pegRME_avg']
            del SDS['pegRME_avg']
            #SDS.TambairER=SDS.TambairER_avg
            SDS['pambairER']=SDS['pambairER_avg']
            del SDS['pambairER_avg']
            
            SDS['AuxBlowME']=SDS['AuxBlowME_avg']
            del SDS['AuxBlowME_avg']
            SDS['mGOCME']=SDS['mGOCME_dCnt']
            del SDS['mGOCME_dCnt']
            SDS['LCVGOME']=SDS['LCVGOME_an']
            del SDS['LCVGOME_an']
            
            SDS['pindME']=SDS['pindME_avg']
            del SDS['pindME_avg']
            SDS['pmaxME']=SDS['pmaxME_avg']
            del SDS['pmaxME_avg']
            SDS['pcompME']=SDS['pcompME_avg'] 
            del SDS['pcompME_avg']
            SDS['TegEVoutME']=SDS['TegEVoutME_avg']
            del SDS['TegEVoutME_avg']
            SDS['FPIME']=SDS['FPIME_avg']
            del SDS['FPIME_avg']
            SDS['TFOPPinME']=SDS['TFOPPinME_avg']
            del SDS['TFOPPinME_avg']
        
            SDS['pindcylME']=SDS['pindcylME_avg']
            del SDS['pindcylME_avg']
            SDS['pmaxcylME']=SDS['pmaxcylME_avg']
            del SDS['pmaxcylME_avg']
            SDS['pcompcylME']=SDS['pcompcylME_avg']

        
            del SDS['pcompcylME_avg']
            SDS['TegEVoutcylME']=SDS['TegEVoutcylME_avg']
            del SDS['TegEVoutcylME_avg']
            SDS['FPIcylME']=SDS['FPIcylME_avg']
            del SDS['FPIcylME_avg']
            SDS['NTCME']=SDS['NTCME_avg']
            del SDS['NTCME_avg']
            SDS['dpairAFME']=SDS['dpairAFME_avg']
            del SDS['dpairAFME_avg']
            SDS['dpairACME']=SDS['dpairACME_avg']
            del SDS['dpairACME_avg']
            SDS['TairTCinME']=SDS['TairTCinME_avg']
            del SDS['TairTCinME_avg']
            SDS['TairACinME']=SDS['TairACinME_avg']
            del SDS['TairACinME_avg']
            SDS['TairACoutME']=SDS['TairACoutME_avg']
            del SDS['TairACoutME_avg']
            SDS['TltcwACinME']=SDS['TltcwACinME_avg']
            del SDS['TltcwACinME_avg']
            #SDS.TltcwACoutME=SDS.TltcwACoutME_avg
            SDS['TegTCinME']=SDS['TegTCinME_avg']
            del SDS['TegTCinME_avg']
            SDS['TegTCoutME']=SDS['TegTCoutME_avg'] 
            del SDS['TegTCoutME_avg']
            SDS['pegTCoutME']=SDS['pegTCoutME_avg']
            del SDS['pegTCoutME_avg']
            
            SDS['NTCTCME']=SDS['NTCTCME_avg']
            del SDS['NTCTCME_avg']
            SDS['dpairAFTCME']=SDS['dpairAFTCME_avg']
            del SDS['dpairAFTCME_avg']
            SDS['dpairACTCME']=SDS['dpairACTCME_avg']
            del SDS['dpairACTCME_avg']
            SDS['TairTCinTCME']=SDS['TairTCinTCME_avg']
            del SDS['TairTCinTCME_avg']
            SDS['TairACinTCME']=SDS['TairACinTCME_avg']
            del SDS['TairACinTCME_avg']
            SDS['TairACoutTCME']=SDS['TairACoutTCME_avg']
            del SDS['TairACoutTCME_avg']
            SDS['TltcwACinTCME']=SDS['TltcwACinTCME_avg']
            del SDS['TltcwACinTCME_avg']
            SDS['TltcwACoutTCME']=SDS['TltcwACoutTCME_avg']
            del SDS['TltcwACoutTCME_avg']
            SDS['TegTCinTCME']=SDS['TegTCinTCME_avg'] 
            del SDS['TegTCinTCME_avg']
            SDS['TegTCoutTCME']=SDS['TegTCoutTCME_avg']
            del SDS['TegTCoutTCME_avg']
            SDS['pegTCoutTCME']=SDS['pegTCoutTCME_avg'] 
            del SDS['pegTCoutTCME_avg']
            # return(SDS,SRQST)
    
        if 'AE' in LogType:
             SDS['pambairER']=SDS['pambairER_avg']
             del SDS['pambairER_avg']
             SDS['RHairER']=SDS['RHairER_avg']
             del SDS['RHairER_avg']
    #        SDS['RHAE_Cnt']=SDS['RHAE_Cnt']
    #        del SDS['RHAE_Cnt']
             SDS['PengAE']=SDS['PengAE_avg']
             del SDS['PengAE_avg'] 
             SDS['NAE']=SDS['NAE_avg']
             del SDS['NAE_avg']
             SDS['pcrkcAE']=SDS['pcrkcAE_avg']
             del SDS['pcrkcAE_avg']
             SDS['ThtcwinAE']=SDS['ThtcwinAE_avg']
             del SDS['ThtcwinAE_avg']
             SDS['phtcwinAE']=SDS['phtcwinAE_avg']
             del SDS['phtcwinAE_avg']
             SDS['pltcwinAE']=SDS['pltcwinAE_avg']
             del SDS['pltcwinAE_avg']
             SDS['TltcwACinAE']=SDS['TltcwACinAE_avg']
             del SDS['TltcwACinAE_avg']
             SDS['TltcwACoutAE']=SDS['TltcwACoutAE_avg']
             del SDS['TltcwACoutAE_avg']
             SDS['TltcwLOCinAE']=SDS['TltcwLOCinAE_avg']
             del SDS['TltcwLOCinAE_avg']
             SDS['TltcwLOCoutAE']=SDS['TltcwLOCoutAE_avg']
             del SDS['TltcwLOCoutAE_avg']
             SDS['TscavAE']=SDS['TscavAE_avg']
             del SDS['TscavAE_avg']
             SDS['pscavAE']=SDS['pscavAE_avg']
             del SDS['pscavAE_avg']
             SDS['GIAE']=SDS['GIAE_avg']
             del SDS['GIAE_avg']
             SDS['PelAE']=SDS['PelAE_avg']
             del SDS['PelAE_avg']
             SDS['DEelAE']=SDS['DEelAE_dCnt']
             del SDS['DEelAE_dCnt']
             SDS['Twdgph1AE']=SDS['Twdgph1AE_avg']
             del SDS['Twdgph1AE_avg']
             SDS['Twdgph2AE']=SDS['Twdgph2AE_avg']
             del SDS['Twdgph2AE_avg']
             SDS['Twdgph3AE']=SDS['Twdgph3AE_avg']
             del SDS['Twdgph3AE_avg']
             SDS['VLTALTAE']=SDS['VLTALTAE_avg']
             del SDS['VLTALTAE_avg']
             SDS['AMPALTAE']=SDS['AMPALTAE_avg']
             del SDS['AMPALTAE_avg']
             SDS['VFOCAE']=SDS['VFOCAE_dCnt']
             del SDS['VFOCAE_dCnt']
             SDS['mFOCAE']=SDS['mFOCAE_dCnt']
             del SDS['mFOCAE_dCnt']
             SDS['TFOFMinAE']=SDS['TFOFMinAE_avg']
             del SDS['TFOFMinAE_avg']
             SDS['vFOAE']=SDS['vFOAE_an']
             del SDS['vFOAE_an']
             SDS['rhoFOAE']=SDS['rhoFOAE_an']
             del SDS['rhoFOAE_an']
             SDS['LCVFOAE']=SDS['LCVFOAE_an']
             del SDS['LCVFOAE_an']
             SDS['SulFOAE']=SDS['SulFOAE_an']
             del SDS['SulFOAE_an']
             SDS['pFOFoutAE']=SDS['pFOFoutAE_avg']
             del SDS['pFOFoutAE_avg']
             SDS['TFOinAE']=SDS['TFOinAE_avg']
             del SDS['TFOinAE_avg']
             SDS['FOGAE']=SDS['FOGAE']
             del SDS['FOGAE']
             SDS['GOGAE']=SDS['GOGAE']
             del SDS['GOGAE']
             SDS['VGOCAE']=SDS['VGOCAE_dCnt']
             del SDS['VGOCAE_dCnt'] 
             SDS['mGOCAE']=SDS['mGOCAE_dCnt']
             del SDS['mGOCAE_dCnt']  
             SDS['TGOFMinAE']=SDS['TGOFMinAE_avg']
             del SDS['TGOFMinAE_avg']         
             SDS['TGOinAE']=SDS['TGOinAE_avg']
             del SDS['TGOinAE_avg']
             SDS['pGOinAE']=SDS['pGOinAE_avg']
             del SDS['pGOinAE_avg']
             SDS['rhoGOAE']=SDS['rhoGOAE_an']
             del SDS['rhoGOAE_an']
             SDS['LCVGOAE']=SDS['LCVGOAE_an']
             del SDS['LCVGOAE_an']
             SDS['pSOAE']=SDS['pSOAE_avg']
             del SDS['pSOAE_avg']
             SDS['TSOinAE']=SDS['TSOinAE_avg']
             del SDS['TSOinAE_avg']
             SDS['TSOoutAE']=SDS['TSOoutAE_avg']
             del SDS['TSOoutAE_avg']
             SDS['TSOLOFTinAE']=SDS['TSOLOFTinAE_avg']
             del SDS['TSOLOFTinAE_avg']
             SDS['TSOLOFToutAE']=SDS['TSOLOFToutAE_avg']
             del SDS['TSOLOFToutAE_avg']
             SDS['TSOLOCinAE']=SDS['TSOLOCinAE_avg']
             del SDS['TSOLOCinAE_avg']   
             SDS['TSOLOCoutAE']=SDS['TSOLOCoutAE_avg']
             del SDS['TSOLOCoutAE_avg']
             SDS['pindcylAE']=SDS['pindcylAE_avg']
             del SDS['pindcylAE_avg']
             SDS['pmaxcylAE']=SDS['pmaxcylAE_avg']
             del SDS['pmaxcylAE_avg']
             SDS['pcompcylAE']=SDS['pcompcylAE_avg']
             del SDS['pcompcylAE_avg']
             SDS['ThtcwoutcylAE']=SDS['ThtcwoutcylAE_avg']
             del SDS['ThtcwoutcylAE_avg']
             SDS['TegEVoutcylAE']=SDS['TegEVoutcylAE_avg']
             del SDS['TegEVoutcylAE_avg']
             SDS['FPIcylAE']=SDS['FPIcylAE_avg']
             del SDS['FPIcylAE_avg']
             SDS['NTCTCAE']=SDS['NTCTCAE_avg']
             del SDS['NTCTCAE_avg']
             SDS['dpairAFTCAE']=SDS['dpairAFTCAE_avg']
             del SDS['dpairAFTCAE_avg']
             SDS['dpairACTCAE']=SDS['dpairACTCAE_avg']
             del SDS['dpairACTCAE_avg']
             SDS['TairTCinTCAE']=SDS['TairTCinTCAE_avg']
             del SDS['TairTCinTCAE_avg']
             SDS['TairACoutTCAE']=SDS['TairACoutTCAE_avg']
             del SDS['TairACoutTCAE_avg']
             SDS['pegTCoutTCAE']=SDS['pegTCoutTCAE_avg']
             del SDS['pegTCoutTCAE_avg']
             SDS['TegTCoutTCAE']=SDS['TegTCoutTCAE_avg']
             del SDS['TegTCoutTCAE_avg']
            
             SDS['TegTCinTCAE']=SDS['TegTCinTCAE_avg']
             del SDS['TegTCinTCAE_avg']
             SDS['pSOTCinTCAE']=SDS['pSOTCinTCAE_avg']
             del SDS['pSOTCinTCAE_avg']
             SDS['pindAE']=SDS['pindAE_avg']
             del SDS['pindAE_avg']
             SDS['pmaxAE']=SDS['pmaxAE_avg']
             del SDS['pmaxAE_avg']
             SDS['pcompAE']=SDS['pcompAE_avg']
             del SDS['pcompAE_avg']
             SDS['ThtcwoutAE']=SDS['ThtcwoutAE_avg']
             del SDS['ThtcwoutAE_avg']
             SDS['TegEVoutAE']=SDS['TegEVoutAE_avg']
             del SDS['TegEVoutAE_avg']
             SDS['FPIAE']=SDS['FPIAE_avg']
             del SDS['FPIAE_avg']      
             SDS['NTCAE']=SDS['NTCAE_avg']
             del SDS['NTCAE_avg']
             SDS['dpairAFAE']=SDS['dpairAFAE_avg']
             del SDS['dpairAFAE_avg']
             SDS['dpairACAE']=SDS['dpairACAE_avg']
             del SDS['dpairACAE_avg']
             SDS['TairTCinAE']=SDS['TairTCinAE_avg']
             del SDS['TairTCinAE_avg']
             SDS['TairACoutAE']=SDS['TairACoutAE_avg']
             del SDS['TairACoutAE_avg']
             SDS['pegTCoutAE']=SDS['pegTCoutAE_avg']
             del SDS['pegTCoutAE_avg']
             SDS['TegTCinAE']=SDS['TegTCinAE_avg']
             del SDS['TegTCinAE_avg']
             SDS['TegTCoutAE']=SDS['TegTCoutAE_avg']
             del SDS['TegTCoutAE_avg']
             SDS['pSOTCinAE']=SDS['pSOTCinAE_avg']
             del SDS['pSOTCinAE_avg']
             # SRQST['TotalTimeLapse'].append(time.time()-start_time)
             # SRQST['Remarks'].append("complete")
             # return(SDS,SRQST)
    except Exception as err:
        exp = "Error in HandleServiceSymbols {} in line No. {} for IMO {}".format(err,sys.exc_info()[-1].tb_lineno,IMO)
        logger.error(exp,exc_info=True)
        ExceptionHandler(LogType,exp)
        SRQSTD['TotalTimeLapse']=time.time()-start_time              
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return({},SRQST)
        
    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time               
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(SDS,SRQST)

