
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 12:04:41 2020

@author: akhil.surendran

% PropImmersion
% Description:  This function calculates the propeller immersion according
%               to a standard formula provided in trim and stability
%               booklets. For proper operation im must be more than 100%.
%
% Input:        Ta      [m]     aft draught (vector)
%               Tf      [m]     for draught (vector)
%               Lbp     [m]     length between perpendiculars (scalar)
%               dProp   [m]     propeller diameter (scalar)
%               LProp   [m]     longitudinal position of propeller tip (scalar)
%               hProp   [m]     vertical lowest position of propeller tip (scalar)
%
% Output:       im      [%]     estimated bow length on the waterline (vector)
%
% Note:         LProp and hProp are currently not existed in the reference
%               variables list. Thus, they are enterred as empty ([]) input
%               arguments and the code below assume them to be 0. This must
%               cghange after LProp and hProp will be introduced in
%               reference variable list.
%
% See also:     ManipulateServiceData


"""
'''
NOTE:
LProp and hProp are currently not existed in the reference
variables list. Thus, they are enterred as empty ([]) input
arguments and the code below assume them to be 0. This must
cghange after LProp and hProp will be introduced in
reference variable list
'''

import numpy as np
def PropImmersion(Ta,Tf,Lbp,dProp,LProp,hProp):
    if np.isnan(LProp) or np.size(LProp)==0: 
       LProp=0;
    
    if np.isnan(hProp) or np.size(hProp)==0:
       hProp=0;
    
    im=(Ta+(Tf-Ta)/Lbp*LProp-hProp)/dProp*100
    return im



#Ta=np.array([11.45, 11.45, np.nan, np.nan, 11.35, 10.91, 10.91, 11.35, 10.7, np.nan])
#Tf=np.array([11.45, 11.45, np.nan, np.nan, 11.35, 10.91, 10.91, 11.35, 10.7, np.nan])
#Lbp=244.00
#dProp=8.700
#LProp=np.array([])
#hProp=np.array([])
#IM_r = PropImmersion(Ta,Tf,Lbp,dProp,LProp,hProp)
#print(IM_r)