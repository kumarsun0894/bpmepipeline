# -*- coding: utf-8 -*-
"""
Created on Mon Mar 23 23:45:11 2020

@author: Subhin Antony
"""
# AeAoKeller
# Description:  This function calculates the expanded area ration of a
#               propeller when not available from drawings.
#
# Input:        PMCR        ME MCR in kW (scalar)
#               nProp       number of propellers (scalar)
#               dProp       propeller diameter in m
#               zProp       number of propeller blades
#               Ud          design speed in kn
#               Td          design draught in m
#
# Output:       AeAoProp    expanded area ratio (scalar)
#
# See also:     ReferenceCurves

###########################################################################
# Disclaimer:
# -----------
# BSM retains the intellectual rights of all developments/process
# enhancements for these changes and amendments, which will be ongoing.
# Sharing of this information outside of the BSM Group should have prior
# written permission from the authors and BSM Corporate Fleet Management.
#
# History:
# --------
# 24/06/2020 version 1.01
# 24/09/2019 version 1.00
#
# Authors:
# --------
# Nicholas Kouvaras
###########################################################################
# from statistics import mean
def AeAoKeller(PMCR,nProp,dProp,zProp,Ud,Td):
    
    p0_pv=99047 #this value is for sea water at 15C in N/m2 (Holtrop,1982)
    EM=0.9        #engine margine
    SM=0.15       #sea margin
    etaTest=0.99   #transmition efficiency
    etaeffest=0.99 #engine effective power efficiency
    etaDest=0.7    #propulsive efficiency
    test=0.2       #thrust deduction
    rhosw=1026     #sea water density in kg/m3
    g=9.8067       #gravity acceleration in m/s2
    #Delivered power estimation @ design speed in W
    PDdest=PMCR*1000*EM*etaTest*etaeffest/(1+SM) #in W
    #Total resistance estimation @ design speed in N
    Rtdest=PDdest*etaDest/(Ud*0.51444) #in N
    #Thrust estimation @ design speed in N
    Thd=Rtdest/(1-test) #in N   #need to add mean   
    
    #Propeller shaft distance from waterline at design draught in m
    h=Td-dProp/2 
    #K calculation (see 1982 paper)
    if nProp==1:
        K=0.2
    elif nProp==2:
        K=0.05 #0 to 0.1
    else:
        K=float('NaN')
        print('K error!')
    #p0-pv (see 1982 paper)
    #Keller's formula
    AeAoProp=K+(1.3+0.3*zProp)*Thd/(dProp**2*(p0_pv+rhosw*g*h))
    return(float(AeAoProp))


# from statistics import mean
# a=mean([1,2,3])
# print(a)