# -*- coding: utf-8 -*-
"""
Created on Thu Apr  2 16:55:03 2020

@author: Subhin Antony
@Delta:SunKumar
"""
from BPME_Common import WaterLineLength as wll
from BPME_Common import WaterPlaneArea as wpa
from BPME_Common import BowSectionArea as bsa
from BPME_Common import TransomArea as ta
from BPME_Common import RudderSurfaceArea as rsa
from BPME_Common import CenterOfBuoyancy as cb
from BPME_Common import MidshipSectionArea as msa
from BPME_Common import WettedSurfaceArea as wsa
from BPME_Common import SeawaterViscosity as vss
from BPME_Common import SeawaterDensity as dss
from BPME_Common import ResistanceHoltrop as rh




import numpy as np
def ReferenceCalibrationFactorHoltrop(ReferenceDataStruct):
    #Identify Uref size 
    [imax,jmax] = (ReferenceDataStruct['Uref']).shape
    PDHoltrop=np.empty(np.shape(ReferenceDataStruct['Pref']))
    PDHoltrop[:]=np.nan
    for i in range(0,imax):
        #Construct "ServiceDataStruct"=Holtrop for model test conditions
        Ta=ReferenceDataStruct['TaMT'][i]*np.ones((jmax,1))
        Tf=ReferenceDataStruct['TfMT'][i]*np.ones((jmax,1))
        Tm=(Ta+Tf)/2
        V=ReferenceDataStruct['VMT'][i]*np.ones((jmax,1))
        STW=np.transpose(ReferenceDataStruct['Uref'][i,:])
        [Lwl,xLwl0_5,temp]=wll.WaterLineLength(Ta,Tf,ReferenceDataStruct['Lbp'],np.transpose(ReferenceDataStruct['HullProfile']))   
        [temp,Cwp]=wpa.WaterPlaneArea(Tm,Lwl,ReferenceDataStruct['B'],ReferenceDataStruct['Th'],ReferenceDataStruct['Cwph'])
        [Ab,hb,temp,tempp]=bsa.BowSectionArea(Tf,np.transpose(ReferenceDataStruct['BowSection']),ReferenceDataStruct['BulbousBowVessel'])
        Atr,temp,tempp=ta.TransomArea(Ta,Tf,ReferenceDataStruct['Lbp'],ReferenceDataStruct['B'],np.transpose(ReferenceDataStruct['HullProfile']),np.transpose(ReferenceDataStruct['TransomSection']),ReferenceDataStruct['TransomSternVessel'])
        Ar,temp,tempp,temppp=rsa.RudderSurfaceArea(Ta,ReferenceDataStruct['Lbp'],np.transpose(ReferenceDataStruct['RudderProfile']))
        [temp,lcb]=cb.CenterOfBuoyancy(Tm,Lwl,xLwl0_5,ReferenceDataStruct['Th'],ReferenceDataStruct['LCBh'])
        if V.ndim==1:
            V=V.reshape(1,-1)
            V=np.transpose(V)
        if Lwl.ndim==1:
            Lwl=Lwl.reshape(1,-1)
            Lwl=np.transpose(Lwl)
        if Tm.ndim==1:
            Tm=Tm.reshape(1,-1)
            Tm=np.transpose(Tm)
        Cb=V/(Lwl*ReferenceDataStruct['B']*Tm)                                                       #block coefficient in [-]
        [temp,Cm]=msa.MidshipSectionArea(Tm,ReferenceDataStruct['B'],ReferenceDataStruct['Th'],ReferenceDataStruct['Cmh'])                                       #midship section area under water in [m2]
        S=wsa.WettedSurfaceArea(Tm,Lwl,V,Cm,Cwp,Ab,ReferenceDataStruct['Th'],ReferenceDataStruct['WSAh'],ReferenceDataStruct['B']) #wetted surface area in [m2]
        vsw=vss.SeawaterViscosity(15*np.ones(np.size(Tm)),35.16504*np.ones(np.size(Tm)))
        rhosw=dss.SeawaterDensity(15*np.ones(np.size(Tm)),35.16504*np.ones(np.size(Tm)))
        if vsw.ndim==1:
            vsw=vsw.reshape(1,-1)
            vsw=np.transpose(vsw)
        if rhosw.ndim==1:
            rhosw=rhosw.reshape(1,-1)
            rhosw=np.transpose(rhosw)
        
        if STW.ndim==1:
            STW=STW.reshape(1,-1)
            STW=np.transpose(STW)
        if Cm.ndim==1:
            Cm=Cm.reshape(1,-1)
            Cm=np.transpose(Cm)
        if Cwp.ndim==1:
            Cwp=Cwp.reshape(1,-1)
            Cwp=np.transpose(Cwp)
        if Ab.ndim==1:
            Ab=Ab.reshape(1,-1)
            Ab=np.transpose(Ab)
        if hb.ndim==1:
            hb=hb.reshape(1,-1)
            hb=np.transpose(hb)
        if Atr.ndim==1:
            Atr=Atr.reshape(1,-1)
            Atr=np.transpose(Atr)
        #print(STW,Tm,Tf,Lwl,V,S,Cb,Cm,Cwp,lcb,rhosw,vsw,Ar,Ab,hb,Atr)   
        STW=STW.ravel()
        Tm=Tm.ravel()
        Tf=Tf.ravel()
        Lwl=Lwl.ravel()
        V=V.ravel()
        S=S.ravel()
        Cb=Cb.ravel()
        Cm=Cm.ravel()
        Cwp=Cwp.ravel()
        lcb=lcb.ravel()
        rhosw=rhosw.ravel()
        vsw=vsw.ravel()
        Ar=np.transpose(Ar)
        Ab=Ab.ravel()
        hb=hb.ravel()
        Atr=Atr.ravel()
        
        
        #Run Holtrop method to estimate total resistance
        RTHoltrop,temp,temp0,temp1,temp2,temp3,temp4,temp5,temp6,temp7,temp8,temp9=rh.ResistanceHoltrop(STW,Tm,Tf,Lwl,V,S,Cb,Cm,Cwp,lcb,rhosw,vsw,Ar,Ab,hb,Atr,ReferenceDataStruct)
    
        #Calculate and store the delivered power for each draught assuming etaD=0.7
        PDHoltrop[i,:]=np.transpose(STW*0.514444*RTHoltrop/1000/0.7)
        

    
    #Power ratio
    PratioHoltropref=ReferenceDataStruct['Pref']/PDHoltrop
    return(PratioHoltropref)

