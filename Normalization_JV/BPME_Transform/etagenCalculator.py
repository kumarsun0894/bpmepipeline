# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd

def etagenCalculator(etagennomAEST,MCRAEST):
    
    #Define number of shop tests
    #%For now we assume nAEST=nAE (DF cases are not considered here)
    [nAEST,_]=np.shape(MCRAEST)
    #Memory pre-allocation
    etagencalcAEST=np.empty(np.shape(MCRAEST))
    etagencalcAEST[:]= np.nan
    MCRgenAEST=MCRAEST
    
    #Estimated value
    etaest=0.96;

    for i in range(0,nAEST):
        if np.all(np.isnan(MCRAEST[i,:])) and len(MCRAEST[i,:])==1:
            etagencalcAEST[i,:]=etaest
            MCRgenAEST[i,:]=100
        #If etagen is not available set an estimated value corresponding to 100% load at PF=1
        elif np.all(np.isnan(etagennomAEST[i,:])) or np.any(etagennomAEST[i,:]>1) or np.any(etagennomAEST[i,:]<0.7):
            etagencalcAEST[i,:]=etagenEstimator(etaest,MCRAEST[i,:])
        
        #If only a single element is given as nominal value we assume by default that it corresponds to 100% load at PF=1
        elif ~np.all(np.isnan(etagennomAEST[i,:])) and len(etagennomAEST[i,:])==1:
            etagencalcAEST[i,:]=etagenEstimator(etagennomAEST[i],MCRAEST[i,:])
        
        #If etagen is given for every load as a vector we assume by default that it corresponds to PF=1 for all loads
        elif np.size(etagennomAEST[i,:])==np.size(MCRAEST[i,:]):
            etagencalcAEST[i,:]=etagennomAEST[i,:]
        else:
            etagencalcAEST[i,:]=np.nan;
            print('etagen calculation error!')
    return (etagencalcAEST,MCRgenAEST)

def etagenEstimator(etagennom,mcr):

    a=-0.000008;
    b=0.00135;
    c=etagennom-0.055;
    etagen=a*mcr**2+b*mcr+c
    return  etagen 
