# -*- coding: utf-8 -*-
import numpy as np

def CYLOrhoEstimator(GradeID):
    rhoData=np.array([900,930,913,930,934,940,934,940,900,np.nan,930,930,920,920,940,900,937,917,937,922,953,909,926,916,934,954,902,913,903,935,925,920,960,936,915,949,936,954,954,940,926,926,908,925,932,932,926,940,950,930,920,908])
    rho=rhoData[int(GradeID)]
    return rho




#                GradeID  BN	rho	Maker           Model	
# CYLOGradesInfo={1     30	900	"CASTROL"       "TLX XTRA 303 M/E"
#                 2     50	930	"CASTROL"       "CYLTECH 50S"
#                 3     40	913	"CASTROL"       "TLX PLUS 404"
#                 4     40	930	"CASTROL"       "CYLTECH 40SX"
#                 5     70	934	"CASTROL"       "CYLTECH 70"
#                 6     100	940	"CASTROL"       "CYLTECH 100"
#                 7     70	934	""              "ENERGOL CLO 50M"
#                 8     100	940	"CASTROL"       "CYLTECH 100"
#                 9     16	900	"CASTROL"       "CYLTECH ACT"
#                 10	NaN	NaN	"CHEVRON"       "CX MCL HBN 140"
#                 11	70	930	"CHEVRON"       "TARO SPECIAL 70"
#                 12	70	930	"CHEVRON"       "TARO SPECIAL HT 70"
#                 13	40	920	"CHEVRON"       "TARO SPECIAL HT LS 40"
#                 14	55	920	"CHEVRON"       "TARO SPECIAL HT 55 /L"
#                 15	100	940	"CHEVRON"       "TARO SPECIAL HT 100 /L"
#                 16	25	900	"CHEVRON"       "TARO SPECIAL HT LF /L"
#                 17	70	937	"EXXONMOBIL"	"MOBILGARD 570"
#                 18	40	917	"EXXONMOBIL"	"MOBILGARD 540 LS"
#                 19	70	937	"EXXONMOBIL"	"MOBILGARD 570 BULK"
#                 20	60	922	"EXXONMOBIL"	"MOBILGARD 560 VS"
#                 21	100	953	"EXXONMOBIL"	"MOBILGARD 5100"
#                 22	25	909	"EXXONMOBIL"	"MOBILGARD 525"
#                 23	55	926	"GULF MARINE"	"GULFSEA CYLCARE EHP 5055"
#                 24	40	916	"GULF MARINE"	"GULFSEA CYLCARE DCA 5040H"
#                 25	70	934	"GULF MARINE"	"GULFSEA CYLCARE DCA 5070H"
#                 26	100	954	"GULF MARINE"	"GULFSEA CYLCARE 50100"
#                 27	40	902	"GULF MARINE"	"GULFSEA CYLCARE ECA 50"
#                 28	40	913	"LUKOIL"        "NAVIGO MCL EXTRA (BN 40)"
#                 29	20	903	"LUKOIL"        "NAVIGO MCL ULTRA"
#                 30	70	935	"LUKOIL"        "NAVIGO 70 MCL"
#                 31	50	925	"LUKOIL"        "NAVIGO 50 MCL"
#                 32	40	920	"LUKOIL"        "NAVIGO 40 MCL"
#                 33	100	960	"LUKOIL"        "NAVIGO 100 MCL"
#                 34	70	936	" "             "MARINE C705"
#                 35	40	915	"SHELL"         "ALEXIA 40"
#                 36	100	949	"SHELL"         "ALEXIA 100"
#                 37	70	936	"SHELL"         "ALEXIA 50"
#                 38	100	954	"SHELL"         "ALEXIA X OIL (SAE 50, 100 BN)"
#                 39	100	954	"SHELL"         "ALEXIA S6"
#                 40	80	940	"SHELL"         "ALEXIA S5"
#                 41	60	926	"SHELL"         "ALEXIA S4 BULK"
#                 42	60	926	"SHELL"         "ALEXIA S4"
#                 43	25	908	"SHELL"         "ALEXIA S3"
#                 44	40	925	"SHELL"         "ALEXIA LS"
#                 45	70	932	"SHELL"         "ALEXIA OIL 50"
#                 46	70	932	"SHELL"         "ALEXIA 70"
#                 47	60	926	"SHELL"         "ALEXIA S4 BULK"
#                 48	70	940	"TOTAL"         "TALUSIA HR 70"
#                 49	100	950	"TOTAL"         "TALUSIA UNIVERSAL 100"
#                 50	57	930	"TOTAL"         "TALUSIA UNIVERSAL"
#                 51	40	920	"TOTAL"         "TALUSIA LS 40"
#                 52	25	908	"TOTAL"         "TALUSIA LS 25"