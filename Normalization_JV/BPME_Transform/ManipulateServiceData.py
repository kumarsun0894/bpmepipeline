# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 11:46:51 2020

@author: Subhin Antony
"""
import numpy as np
import pandas as pd
import sys

from BPME_Transform import TrimRef as TR
from BPME_Transform import PropImmersion as PI
from BPME_Common import WaterLineLength as WLL
from BPME_Common import WaterPlaneArea as WPA
from BPME_Common import BowSectionArea as BSA
from BPME_Common import TransomArea as TrA
from BPME_Common import RudderSurfaceArea as RSA
from BPME_Common import CenterOfBuoyancy as COB
from BPME_Common import WettedSurfaceArea as WSA
from BPME_Common import SeawaterViscosity as SWV
from BPME_Common import SeawaterDensity as SWD
from BPME_Common import MidshipSectionArea as MSA
from BPME_Common import TEUint2nom as T2N
from BPME_Transform import true2relative as T2R
from BPME_Transform import IdealPropulsiveEfficiency as IPE
from BPME_Transform import IdealPropulsiveEfficiencyHoltrop as IPEH
from BPME_Common import IdealDeliveredPowerHoltrop as IDPH
from BPME_Common import IdealDeliveredPower as IDP
from BPME_Common import ISOCorrections as iso
from BPME_Common import TCefficiency as tce
from BPME_Common import VolumeCorrectionFactor as vcf
from BPME_Common import nansumwrapper as nsw
from BPME_Common import IdealST as ist
from BPME_Transform import  STWestimator as stwe
import time
import logging 
# create logger
logger = logging.getLogger('BPMEngine.2.02')
from ExceptionHandler import ExceptionHandler

def ManipulateServiceData(IMO,SDS, RDS, DS, Eidx, LogType,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        
        SDS['Dt']=SDS['Dt'].reshape(1,-1).T
        if 'Noon' in LogType:
            minDuration = 12
            maxDuration = 36
        elif 'HP' in LogType:
            minDuration = 0.2
            maxDuration = 4
        elif 'ME' in LogType:
            minDuration = 0.2
            maxDuration = 6
            
        elif 'AE' in LogType:
            minDuration = 0.2
            maxDuration = 6
            
        elif 'Auto'in LogType:
            minDuration = 0.001
            maxDuration = 36
        # Discard reports with Dt<minDuration or Dt>maxDuration
        DS['LogDuration'] = np.zeros(np.size(SDS['pambairER'])).reshape(1,-1).T
        Dtcheck1 = np.array(SDS['Dt'] < minDuration)
        Dtcheck2 = np.array(SDS['Dt'] > maxDuration)
        Dtcheck = Dtcheck1 | Dtcheck2
        if sum(Dtcheck)[0] > 0:
            if SDS['LogValidity'].ndim ==1:
                SDS['LogValidity']=SDS['LogValidity'].reshape(1,-1).T
            SDS['LogValidity'][Dtcheck] =0
            DS['LogDuration'][Dtcheck] = 1
            print('Log duration too low or too high!')
        #Indicate reports with Dt<0
        DS['NegativeLogDuration']=np.zeros(SDS['pambairER'].shape)
        SDS['NegativeLogValidity']=np.zeros(SDS['pambairER'].shape)
        NegativeDtcheck=np.flatnonzero(SDS['Dt']<0).reshape(1,-1).T
        if sum(NegativeDtcheck)>0:
            SDS['Dt'][NegativeDtcheck]=0  #set it 0 to avoid complex numbers in calculations
            SDS['NegativeLogValidity'][NegativeDtcheck]=0
            DS['NegativeLogDuration'][NegativeDtcheck]=1
            print('Log duration<0!')   
        
        
        if 'Noon' in LogType or 'HP' in LogType or 'ME' in LogType or 'Auto' in LogType:
            #ME shaft power check
            if not('PSME' in SDS.keys()):
                SDS['PSME'] = np.empty(SDS['DESME'].shape)
                SDS['PSME'][:] = np.nan
            if SDS['PSME'].ndim==1:
                SDS['PSME']=SDS['PSME'].reshape(1,-1).T
            #ME energy counter check
            if not('DESME' in SDS.keys()):
                SDS['DESME'] = np.empty(SDS['PSME'].shape)
                SDS['DESME'][:] = np.nan
                #Define ME energy produced during log in [kWh]
            SDS['pambairER']=SDS['pambairER'].reshape(1,-1).T
            if SDS['DESME'].ndim == 1:
                SDS['DESME']= SDS['DESME'].reshape(1,-1).T
            
            if not 'RHME' in SDS.keys():
                if 'HP' in LogType or 'Auto' in LogType:
                    SDS['RHME']=np.tile(SDS['Dt'],(1,int(RDS['nME'])))
                elif 'ME' in LogType:
                    SDS['RHME']=SDS['Dt']
             #Define ME energy produced during log in [kWh]
            
            SDS['DESME'][pd.isnull(SDS['DESME'])]=SDS['PSME'][pd.isnull(SDS['DESME'])]*SDS['RHME'][pd.isnull(SDS['DESME'])]
    
            #Define AE electric energy produced during log in [kWh]
            if 'Noon' in LogType:
                SDS['DEelAE'][pd.isnull(SDS['DEelAE'])]=SDS['PelAE'][pd.isnull(SDS['DEelAE'])]*SDS['RHAE'][pd.isnull(SDS['DEelAE'])]
#                if  not 'PengAE' in SDS:
#                    SDS['PengAE']=np.empty(SDS['DEelAE'].shape)
#                    SDS['PengAE'][:] = np.nan
                #Assume values
                SDS['TairTCinAE']=np.tile(SDS['TambairER'].reshape(1,-1).T,(1,int(RDS['nAE'])))
                SDS['TltcwACinAE']=np.tile(np.nanmean(SDS['TltcwACinME'],axis= 1).reshape(1,-1).T,(1,int(RDS['nAE']))) #this is an estimation because now this variable is unavailable in Noon. This estimation to be removed once this variable become an entry on board.
        
        
    
            #ME indicated pressure check
            if not('pindME' in SDS.keys()):
                SDS['pindME'] = np.empty(np.shape(SDS['pambairER']))
                SDS['pindME'][:] = np.nan
            #MEFPI check
            if not('FPIME' in SDS.keys()):
                SDS['FPIME'] = np.empty(np.shape(SDS['pambairER']))
                SDS['FPIME'][:] = np.nan
            #ME NTC check
            if not('NTCME' in SDS.keys()):
                SDS['NTCME'] = np.empty(np.shape(SDS['pambairER']))
                SDS['NTCME'][:] = np.nan
            #ME Tscav check
            if not('TscavME' in SDS.keys()):
                SDS['TscavME'] = np.empty(np.shape(SDS['pambairER']))
                SDS['TscavME'][:] = np.nan
            #ME Peffest check
            if not('PeffestME' in SDS.keys()):
                SDS['PeffestME']=np.empty(np.shape(SDS['pambairER']))
                SDS['PeffestME'][:]=np.nan
           #ME BNCO check
            if not ('BNCOME' in SDS.keys()):
                SDS['BNCOME']=np.empty(np.shape(SDS['pambairER']))
                SDS['BNCOME'] = np.nan
    
    
            if 'Yes' in RDS['DualFuelME']:
                # Currently there is no function to calculate gas density
    #           SDS['rhoGOcor'] = np.empty(np.shape(SDS['pambairER']))
    #           SDS['rhoGOcor'][:] = np.nan
                #Define fuel and gas ratios
                SDS['mFOCME']=SDS['mFOCME'].reshape(1, -1).T
                SDS['mGOCME']=SDS['mGOCME'].reshape(1, -1).T
                mfg=np.concatenate((SDS['mFOCME'], SDS['mGOCME']), axis=1)
            
            
                SDS['FORatioME'] = np.divide(SDS['mFOCME'].astype('float64'), nsw.nansumwrapper(mfg, axis=1).reshape(1, -1).T.astype('float64'), out=np.full_like(SDS['mFOCME'].astype('float64'),float('nan')), where=nsw.nansumwrapper(mfg, axis=1).reshape(1, -1).T.astype('float64') != 0)
                SDS['GORatioME'] = np.divide(SDS['mGOCME'].astype('float64'), nsw.nansumwrapper(mfg, axis=1).reshape(1, -1).T.astype('float64'), out=np.full_like(SDS['mGOCME'].astype('float64'),float('nan')), where=nsw.nansumwrapper(mfg, axis=1).reshape(1, -1).T.astype('float64') != 0)
                #Memory pre-allocation
                SDS['FuelModeME'] = np.empty(np.shape(SDS['pambairER']))
                SDS['FuelModeME'][:] = np.nan
                #0=Gas, 1=FO, 2=Mixed
                SDS['FuelModeME'][(pd.isnull(SDS['FORatioME'])|(SDS['FORatioME']<=0.1)) & (SDS['GORatioME'] >= 0.9)] = 0
                SDS['FuelModeME'][SDS['FORatioME'] == 1] = 1
                SDS['FuelModeME'][(SDS['FORatioME']> 0.1) & (SDS['GORatioME'] < 0.9)] = 2
                #Define service data fuel mode indices
                SDS['FOModeME'] =np.flatnonzero( SDS['FuelModeME'] == 1).reshape(1,-1).T
                SDS['GOModeME'] =np.flatnonzero(SDS['FuelModeME'] == 0).reshape(1,-1).T
            
                DS['DFMixedModeME']=np.zeros(SDS['pambairER'].shape)
                DFMixedModeMEcheck=np.flatnonzero(SDS['FuelModeME']==2)
                if len(DFMixedModeMEcheck)>0:
                    SDS['LogValidity'][DFMixedModeMEcheck]=0
                    DS['DFMixedModeME'][DFMixedModeMEcheck]=1
                    print('Dual fuel in ME in mixed mode!')
            
    
        
         ######################################################################
             #NOON/HP/AUTO CALCULATIONS
         ########################################################################
        if(LogType == 'Noon' or LogType == 'HP' or LogType == 'Auto'):
            # Mean draught caclulation in [m] .reshape(1,-1).T
            SDS['Tm']=SDS['Tm'].reshape(1,-1).T
            SDS['Tf']=SDS['Tf'].reshape(1,-1).T
            SDS['Ta']=SDS['Ta'].reshape(1,-1).T
            
            SDS['Tm'] = (SDS['Tf']+SDS['Ta'])/2    
            
            # Trim calculation in [m]
            SDS['trim'] = SDS['Tf']-SDS['Ta']
            
            # Indicate reports with deltaTrim>0.3%Lbp
            trimref = TR.TrimRef(SDS['Tm'], RDS['TaMT'], RDS['TfMT'])    
            deltaTrim = SDS['trim']-trimref
            DS['TrimDeviation'] = np.zeros(np.shape(SDS['pambairER']))    
            dTrimcheck = np.flatnonzero(abs(deltaTrim) > 0.3/100*RDS['Lbp']).reshape(1,-1).T
            if len(dTrimcheck)>0:
                DS['TrimDeviation'][dTrimcheck] = 1
                print('Large trim deviation!')
    
            #Indicate reports with Propeller Immersion<100%
            im = PI.PropImmersion(SDS['Ta'], SDS['Tf'],RDS['Lbp'], RDS['dProp'], np.array([]), np.array([]))                      
            DS['PropImmersion'] = np.zeros(np.shape(SDS['pambairER']))    
            primcheck = np.flatnonzero(im <= 100).reshape(1,-1).T
            if len(primcheck)>0:
                DS['PropImmersion'][primcheck] = 1
                print('Propeller immersion < 100%!')
    
            #Indicate/Discard reports with Tm > Ts+0.01 or < Tb-0.01
            DS['DraughtOutOfRange'] = np.zeros(np.shape(SDS['pambairER']))
            draughtcheck = np.flatnonzero(np.logical_or(SDS['Tm'] > RDS['Ts']*1.042, SDS['Tm'] < RDS['Tb']-0.01)).reshape(1,-1).T
            if len(draughtcheck)>0:
                SDS['LogValidity'][draughtcheck] = 0
                DS['DraughtOutOfRange'][draughtcheck] = 1
                print('Draughts out of range!')
            
            
            #Indicate/Discard reports with missing Ta or Tf
            DS['MissingDraughts']=np.zeros(np.shape(SDS['pambairER']))
            MissingDraughtscheck=np.flatnonzero(pd.isnull(SDS['Tm']))
            if len(MissingDraughtscheck)>0:
                SDS['LogValidity'][MissingDraughtscheck]=0
                DS['MissingDraughts'][MissingDraughtscheck]=1
                print('Missing draughts!')
            
        
            #Indicate reports with discrepancy > 5# between estimated and reported displacement
            SDS['DISPest']=ist.IdealST(RDS['Th'],RDS['Vh'],SDS['Tm'],0)*SDS['rhosw']/1000
            DS['DisplacementDiscrepancy']=np.zeros(SDS['pambairER'].shape)
            displacementcheck=np.flatnonzero(abs((SDS['DISP']-SDS['DISPest'])/SDS['DISP']*100)>5)
            if len(displacementcheck)>0:
                DS['DisplacementDiscrepancy'][displacementcheck]=1
                print('Estimated and reported displacements have a difference > 5#!')
            
    
            if 'Noon' in LogType or 'Auto' in LogType:
            
                #Indicate logs with missing STW
                DS['MissingSTW']=np.zeros(np.shape(SDS['pambairER']))
                MissingSTWcheck=np.flatnonzero(pd.isnull(SDS['STW']))
                if len(MissingSTWcheck)>0:
                    DS['MissingSTW'][MissingSTWcheck]=1
                    print('Missing STW!')
            
            #Indicate logs with missing SOG
                DS['MissingSOG']=np.zeros(SDS['pambairER'].shape)
                MissingSOGcheck=np.flatnonzero(pd.isnull(SDS['SOG']))
                if len(MissingSOGcheck)>0:
                    DS['MissingSOG'][MissingSOGcheck]=1
                    print('Missing SOG!')
            #Speed over ground calculation in [kn]
            if 'SOG' not in SDS:
                # mean speed over ground during log
                SDS['DOG'] = SDS['DOG'].reshape(1,-1).T
                SDS['SOG'] = SDS['DOG'] /SDS['Dt']
            else:
                # SDS['DOG'] = SDS['DOG'].reshape(1,-1).T
                SDS['SOG'] = SDS['SOG'].reshape(1,-1).T        
            #Indicate/Discard reports with instantaneous from average SOG deviation > 2kn
            if LogType == 'Noon':
                DS['SOGdev'] = np.zeros(np.shape(SDS['pambairER']))  
                SDS['DOG'] = SDS['DOG'].reshape(1,-1).T
                SOGcheck = np.flatnonzero(abs(SDS['SOG']-SDS['DOG']/SDS['Dt']) > 2).reshape(1,-1).T
                if len(SOGcheck)>0:
                    SDS['LogValidity'][SOGcheck] = 0
                    DS['SOGdev'][SOGcheck] = 1
                    print('Instantaneous and average SOG have a difference > 2kn!')
            if 'STW' not in SDS:  # if instantaneous STW is not given
                #mean speed through water during log
                SDS['DTW'] = SDS['DTW'].reshape(1,-1).T
                SDS['STW'] = SDS['DTW'] / SDS['Dt']
            else:
                # SDS['DTW'] = SDS['DTW'].reshape(1,-1).T
                SDS['STW'] = SDS['STW'].reshape(1,-1).T
                
            #%Indicate/Discard reports with instantaneous from average STW deviation > 2kn
            if LogType == 'Noon':
                DS['STWdev'] = np.zeros(np.size(SDS['pambairER']))
                SDS['DTW'] = SDS['DTW'].reshape(1,-1).T
                STWcheck = np.flatnonzero(abs(SDS['STW']-SDS['DTW']/SDS['Dt']) > 2).reshape(1,-1).T
                if len(STWcheck)>0:
                    SDS['LogValidity'][STWcheck] = 0
                    DS['STWdev'][STWcheck] = 1
                    print('Instantaneous and average SOG have a difference > 2kn!')         
           
             #Indicate reports with estimated from reported STW discrepancy > 2#
            SDS['STWest'],temp=stwe.STWestimator(SDS['SOG'],SDS['HEAD'].reshape(1,-1).T,SDS['Ucut'].reshape(1,-1).T,SDS['psicut'].reshape(1,-1).T)
            DS['STWdiscrepancy']=np.zeros(SDS['pambairER'].shape)
            # STWdiscrepancycheck=np.flatnonzero(abs((SDS['STW']-SDS['STWest'])/SDS['STW'])>0.02).reshape(1,-1).T
            STWdiscrepancycheck=np.flatnonzero(abs(np.divide((SDS['STW']-SDS['STWest']),SDS['STW'],out=np.full_like((SDS['STW']-SDS['STWest']),np.nan),where =SDS['STW']!=0 ))>0.02).reshape(1,-1).T
            if len(STWdiscrepancycheck)>0:
                DS['STWdiscrepancy'][STWdiscrepancycheck]=1
                print('STW estimated from reported discrepancy>2#!')
            
            # ME rotational speed calculation in [RPM]
            if 'NME' not in SDS:  # if instantaneous NME is not given
    #            SDS['NME'] = SDS['DRME'] /(60*SDS['RHME'])
                 SDS['NME'] = np.divide(SDS['DRME'],(60*SDS['RHME']),out=np.full_like(SDS['DRME'],float('NaN')),where=SDS['RHME']!=0)
            #ME rotational speed estimation to fix wrong legacy variaable mapping from
            #propeller speed to ME speed. This must be fixed. This is introduced in
            #%25/10/2019 and is intendent to be only for a few days.
            if LogType == 'Noon':
                #SDS['NME'][pd.isnull(SDS['NME'])]=SDS['DRME'][pd.isnull(SDS['NME'])].astype('float64')/(60*SDS['RHME'][pd.isnull(SDS['NME'])]).astype('float64')
                SDS['NME'][pd.isnull(SDS['NME'])]=np.divide(SDS['DRME'][pd.isnull(SDS['NME'])].astype('float64'), (60*SDS['RHME'][pd.isnull(SDS['NME'])]).astype('float64'), out=np.full_like(SDS['DRME'][pd.isnull(SDS['NME'])].astype('float64'),float('nan')), where=(60*SDS['RHME'][pd.isnull(SDS['NME'])]).astype('float64')!=0)
                
            if LogType == 'Noon':
               #Define indices related to report type
               #NS: Noon report at sea
               #EOP: End of sea passage
               #FWE: Finish with engine
               #NNP: Noon report at port
               #STB: Standby departure
               #BOP: Beginning of sea passage
              SDS['NNSIndex'] = np.flatnonzero(SDS['Report_Type'] == 'NNS').reshape(1,-1).T
              SDS['EOPIndex'] = np.flatnonzero(SDS['Report_Type'] == 'EOP').reshape(1,-1).T
              SDS['FWEIndex'] = np.flatnonzero(SDS['Report_Type'] == 'FWE').reshape(1,-1).T
              SDS['NNPIndex'] = np.flatnonzero(SDS['Report_Type'] == 'NNP').reshape(1,-1).T
              SDS['STBIndex'] = np.flatnonzero(SDS['Report_Type'] == 'STB').reshape(1,-1).T
              SDS['BOPIndex'] = np.flatnonzero(SDS['Report_Type'] == 'BOP').reshape(1,-1).T
              #Accept only NNS and EOP
              SDS['LogValidity'][np.unique(np.concatenate((SDS['FWEIndex'], SDS['NNPIndex'], SDS['STBIndex'], SDS['BOPIndex'])))] = 0
              SDS['STW'] = SDS['STW'].reshape(1,-1).T
            #Discard reports with Dt~RHME
            if LogType == 'Noon':
                #Check discrepancy of running hours among MEs#This could generate a diagnostic
                #To be planned for the future
                if RDS['nME'] > 1:
                    DS['RunningHoursMEDis'] = np.zeros(np.shape(SDS['pambairER']))
                    RHMEdischeck = np.flatnonzero(np.logical_and(~pd.isnull(SDS['RHME'][:, 0]-SDS['RHME'][:, 1]), SDS['RHME'][:, 0]-SDS['RHME'][:, 1]) != 0).reshape(1,-1).T
                    if len(RHMEdischeck)>0:
                        SDS['LogValidity'][RHMEdischeck] = 0
                        DS['RunningHoursMEDis'][RHMEdischeck] = 1
                        print('Running hours vary accross MEs!')
                 
     #################################################################################
                
                DS['RunningHoursME'] = np.zeros(np.shape(SDS['pambairER']))
                RHMEcheck = np.flatnonzero(abs(SDS['Dt'].ravel()-SDS['RHME'][:, 0]) > 0.01).reshape(1,-1).T    
                if len(RHMEcheck)>0:
                    SDS['LogValidity'][RHMEcheck] = 0
                    DS['RunningHoursME'][RHMEcheck] = 1
                    print('Log duration is different than ME running hours!')
                    
                    
#             if(len(SDS['SOG'][SDS['SOG'] == 0]) > 0):
#                 SOGReplace = SDS['SOG']
#                 SOGReplace[SOGReplace == 0] = 1.
#             else:
#                 SOGReplace = SDS['SOG']
#             #Discard reports with dU>20%
#             deltaU = (SDS['SOG']-SDS['STW'])/SOGReplace*100
            deltaU = np.divide((SDS['SOG'].astype(float)-SDS['STW'].astype(float)),SDS['SOG'].astype(float),out=np.full_like(SDS['SOG'],float('NaN')),where=SDS['SOG']!=0)*100

            DS['dUdeviation'] = np.zeros(np.shape(SDS['pambairER']))
            dUcheck = np.flatnonzero(abs(deltaU) > 20).reshape(1,-1).T
            if len(dUcheck)>0:
                SDS['LogValidity'][dUcheck] = 0
    #            SDS['STW'][dUcheck] = np.nan
                DS['dUdeviation'][dUcheck] = 1
                print('Large SOG from STW deviation!')
            SDS['NProp'] = np.nanmean(SDS['NME'].astype('float64'), axis=1)
            SDS['NProp'] =SDS['NProp'].reshape(1,-1).T
            #Indicate reports with missing NProp
            DS['MissingNProp']=np.zeros(SDS['pambairER'].shape)
            MissingNPropcheck=np.flatnonzero(pd.isnull(SDS['NProp'])).reshape(1,-1).T
            if len(MissingNPropcheck)>0:
                SDS['LogValidity'][MissingNPropcheck]=0
                DS['MissingNProp'][MissingNPropcheck]=1
            print('Missing NProp!')
        
            #Heave and pitch motions check
            if 'azbow' not in SDS:
            #We may have to add a more realistic estimation of this variable
            #based on the wave height and/or wind speed, because this variable
            #(azbow) is the criterion for the wave correction method (STAWAVE 1
            #vs 2)
                SDS['azbow'] = np.zeros(np.shape(SDS['pambairER']))
            else:
                SDS['azbow'] =SDS['azbow'].reshape(1,-1).T
                
            #Estimate FO temperature at ME pump inlet
            #An assumption is made for the HP/Noon Logs that the temperature at
            #inlet flowmeter is the same as the FO pump inlet. This is not always true
            SDS['TFOPPinME'] = SDS['TFOFMinME']
            
            #Assume Value
            SDS['TambairER'] = SDS['TambairER'].reshape(1,-1).T
            SDS['TairTCinME'] = np.tile(SDS['TambairER'],(1,int(RDS['nME'])))
    
            #Indicate logs with missing Uwit
            DS['MissingWind'] = np.zeros(np.shape(SDS['pambairER']))
            Uwitcheck = np.flatnonzero(pd.isnull(SDS['Uwit'])).reshape(1,-1).T
            if len(Uwitcheck)>0:
                DS['MissingWind'][Uwitcheck] = 1
                print('Wind speed is missing!')   
            
            #HOLTROP DATA
            [SDS['Lwl'], SDS['xLwl0_5'], temp] = WLL.WaterLineLength(SDS['Ta'], SDS['Tf'],
                                                                     RDS['Lbp'], 
                                                                     np.transpose(RDS['HullProfile']))
                
                                                                                               
                                                                                               
            SDS['Lwl']= SDS['Lwl'].reshape(1,-1).T
            SDS['xLwl0_5']= SDS['xLwl0_5'].reshape(1,-1).T
            [SDS['WPA'], SDS['Cwp']] = WPA.WaterPlaneArea(SDS['Tm'].ravel(),
                                                          SDS['Lwl'].ravel(),
                                                          RDS['B'],
                                                          RDS['Th'],                              
                                                          RDS['Cwph'])                               
                                                                                        
                                                                                        
            SDS['WPA'] = SDS['WPA'].reshape(1,-1).T
            SDS['Cwp'] = SDS['Cwp'].reshape(1,-1).T
            
            [SDS['Ab'], SDS['hb'], temp1, temp2] = BSA.BowSectionArea(SDS['Tf'],
                                                                      RDS['BowSection'].T,                              
                                                                      RDS['BulbousBowVessel'])                              
            SDS['Ab'] = SDS['Ab'].reshape(1,-1).T
            SDS['hb'] = SDS['hb'].reshape(1,-1).T
            [SDS['Atr'], temp3, temp4] = TrA.TransomArea(SDS['Ta'], 
                                                        SDS['Tf'], 
                                                        RDS['Lbp'], 
                                                        RDS['B'], 
                                                        RDS['HullProfile'].T, 
                                                        RDS['TransomSection'].T, 
                                                        RDS['TransomSternVessel'])
            
            SDS['Atr'] = SDS['Atr'].reshape(1,-1).T
            
            [SDS['Ar'], temp5, temp6, temp7] = RSA.RudderSurfaceArea(SDS['Ta'],
                                                                                        RDS['Lbp'], 
                                                                                        RDS['RudderProfile'].T)
            
            
            
            [SDS['LCB'], SDS['lcb']] = COB.CenterOfBuoyancy(SDS['Tm'], 
                                                                                        SDS['Lwl'], 
                                                                                        SDS['xLwl0_5'],#.reshape(np.size(SDS['xLwl0_5']), 1),
                                                                                        RDS['Th'],
                                                                                        RDS['LCBh'])
            SDS['DISP'] =SDS['DISP'].reshape(1,-1).T.astype('float64')
            if SDS['rhosw'].ndim==1:
                SDS['rhosw']=SDS['rhosw'].reshape(1,-1).T
            
            SDS['DISPV'] = 1000*SDS['DISP'] /SDS['rhosw'] 
            
#            SDS['Cb'] = SDS['DISPV'] /(SDS['Lwl']*RDS['B'] * SDS['Tm'])  #block coefficient in [-]
            
            SDS['Cb'] = np.divide(SDS['DISPV'].astype('float64'), (SDS['Lwl']*RDS['B'] * SDS['Tm']).astype('float64'), out=np.full_like(SDS['DISPV'].astype('float64'),float('nan')), where=(SDS['Lwl']*RDS['B'] * SDS['Tm']).astype('float64')!=0)

            [SDS['Amid'], SDS['Cm']] = MSA.MidshipSectionArea(SDS['Tm'].ravel(),RDS['B'], RDS['Th'], RDS['Cmh'])  # midship section area under water in [m2]
            
            SDS['Amid'] = SDS['Amid'].reshape(1,-1).T
            SDS['Cm']=SDS['Cm'].reshape(1,-1).T
            SDS['WSA'] = WSA.WettedSurfaceArea(SDS['Tm'],
                                                             SDS['Lwl'], 
                                                             SDS['DISPV'],
                                                             SDS['Cm'], 
                                                             SDS['Cwp'], 
                                                             SDS['Ab'], 
                                                             RDS['Th'], 
                                                             RDS['WSAh'], 
                                                             RDS['B'])  # wetted surface area in [m2]
            if SDS['Tsw'].ndim==1:
                SDS['Tsw'] = SDS['Tsw'].reshape(1,-1).T  
            if SDS['Sasw_wp'].ndim==1:
                SDS['Sasw_wp']=SDS['Sasw_wp'].reshape(1,-1).T
                             
            SDS['vsw'] = SWV.SeawaterViscosity(SDS['Tsw'],SDS['Sasw_wp'])
            
            #Weather corrections preparation
            SDS['TEUonDeck'] = SDS['TEUonDeck'].reshape(1,-1).T    
            SDS['teu'] = T2N.TEUint2nom(SDS['TEUonDeck'])  # containers on main deck    
            SDS['DISPVref'] = SDS['DISPV'] #reference displacement is identical to actual which cancels the admiralty power correction
            
            SDS['HEAD'] = SDS['HEAD'].reshape(1,-1).T
            SDS['psiwvt'] = SDS['psiwvt'].reshape(1,-1).T
            SDS['psislt'] = SDS['psislt'].reshape(1,-1).T
            
            SDS['psiwvr'] = T2R.true2relative(SDS['HEAD'], SDS['psiwvt'])  #relative wave direction in [deg]
            SDS['psiwvr']=SDS['psiwvr'].reshape(1,-1).T
            SDS['psislr'] = T2R.true2relative(SDS['HEAD'], SDS['psislt'])  #relative swell direction in [deg]
            SDS['psislr']=SDS['psislr'].reshape(1,-1).T
            
            #Ideal curves
            #Must use Ucor instead of STW in the input argument for etaDid and
            #PDid. This can be implemented if we move the SHALLOW function here
            #from STATOTAL function.
            
            SDS['etaDHid'] = IPEH.IdealPropulsiveEfficiencyHoltrop(SDS['STW'].ravel(), RDS)
            SDS['etaDHid'] = SDS['etaDHid'].reshape(1,-1).T
            SDS['etaDid'] = IPE.IdealPropulsiveEfficiency(SDS['STW'].ravel(),
                                                            SDS['Tm'],
                                                            SDS['etaDHid'],
                                                            RDS['Tref'],
                                                            RDS['etaDref'],
                                                            RDS['Uref'],
                                                            RDS['Tb'],
                                                            RDS['Ts'])
            SDS['etaDid'] = SDS['etaDid'].reshape(1,-1).T
            #Ideal delivered power in [kW] from model test interpolation at current speed and draught
            SDS['PDHid'] = IDPH.IdealDeliveredPowerHoltrop(SDS['STW'].ravel(),
                                                            SDS['Tm'].ravel(),
                                                            SDS['Tf'].ravel(),
                                                            SDS['Lwl'].ravel(),
                                                            SDS['DISPV'].ravel(),
                                                            SDS['WSA'].ravel(),
                                                            SDS['Cb'].ravel(),
                                                            SDS['Cm'].ravel(),
                                                            SDS['Cwp'].ravel(),
                                                            SDS['lcb'].ravel(),
                                                            SDS['rhosw'].ravel(),
                                                            SDS['vsw'].ravel(),
                                                            SDS['Ar'].ravel(),
                                                            SDS['Ab'].ravel(),
                                                            SDS['hb'].ravel(),
                                                            SDS['Atr'].ravel(),
                                                            RDS)
            SDS['PDHid']= SDS['PDHid'].reshape(1,-1).T
            SDS['PDid'] = IDP.IdealDeliveredPower(SDS['STW'].ravel(),
                                                            SDS['Tm'].ravel(),
                                                            SDS['PDHid'].ravel(),
                                                            RDS['Tref'],
                                                            RDS['Pref'],
                                                            RDS['Uref'],
                                                            RDS['Tb'],
                                                            RDS['Ts'])
            SDS['PDid']= SDS['PDid'].reshape(1,-1).T
            
            if 'Noon' in LogType or 'HP' in LogType:
            
                #Current direction transformation
                #a conversion is necessary as per meteomatics meteorological
                #convention for directions:
                #https://api.meteomatics.com/FAQ.html#meteorological-convention-directions
                SDS['psicut_wp']=SDS['psicut_wp']+180
                SDS['psicut_wp'][SDS['psicut_wp']>359]=SDS['psicut_wp'][SDS['psicut_wp']>359]-360
                #Weather provider data calculation for WI
                SDS['psiwvr_wp']=T2R.true2relative(SDS['HEAD'].ravel(),SDS['psiwvt_wp'])
                SDS['psiwvr_wp']=SDS['psiwvr_wp'].reshape(1,-1).T
                SDS['psislr_wp']=T2R.true2relative(SDS['HEAD'].ravel(),SDS['psislt_wp'])
                SDS['psislr_wp']=SDS['psislr_wp'].reshape(1,-1).T
                if SDS['Tsw_wp'].ndim ==1:
                    SDS['Tsw_wp']=SDS['Tsw_wp'].reshape(1,-1).T
                SDS['rhosw_wp']=SWD.SeawaterDensity(SDS['Tsw_wp'],SDS['Sasw_wp']) 
                SDS['vsw_wp']=SWV.SeawaterViscosity(SDS['Tsw_wp'],SDS['Sasw_wp'])
                #Weather provider data filtering for -999 error values
                SDS['Uwit_wp'][SDS['Uwit_wp']<0]=np.nan
                SDS['psiwit_wp'][SDS['psiwit_wp']<0]=np.nan
                SDS['Ucut_wp'][SDS['Ucut_wp']<0]=np.nan
                SDS['psicut_wp'][SDS['psicut_wp']<0]=np.nan
                SDS['Tair_wp'][SDS['Tair_wp']<-50]=np.nan
                SDS['pair_wp'][SDS['pair_wp']<0]=np.nan
                SDS['Hwv_wp'][SDS['Hwv_wp']<0]=np.nan
                SDS['Twv_wp'][SDS['Twv_wp']<0]=np.nan
                SDS['psiwvr_wp'][SDS['psiwvr_wp']<-180]=np.nan
                SDS['Hsl_wp'][SDS['Hsl_wp']<0]=np.nan
                SDS['Tsl_wp'][SDS['Tsl_wp']<0]=np.nan
                SDS['psislr_wp'][SDS['psislr_wp']<-180]=np.nan
                SDS['rhosw_wp'][SDS['rhosw_wp']<0]=np.nan
                SDS['vsw_wp'][SDS['vsw_wp']<0]=np.nan
            
            elif 'Auto' in LogType:
                #Current direction transformation (this is needed for the weather provider data)
                #a conversion is necessary as per meteomatics meteorological convention for directions:
                #https://api.meteomatics.com/FAQ.html#meteorological-convention-directions
                SDS['psicut']=SDS['psicut']+180
                SDS['psicut'][SDS['psicut']>359]=SDS['psicut'][SDS['psicut']>359]-360
                #Weather provider (have been renamed in HandleServiceSymbols.m) data filtering for -999 error values
                SDS['Uwit'][SDS['Uwit']<0]=np.nan
                SDS['psiwit'][SDS['psiwit']<0]=np.nan
                SDS['Ucut'][SDS['Ucut']<0]=np.nan
                SDS['psicut'][SDS['psicut']<0]=np.nan
                SDS['Tair'][SDS['Tair']<-50]=np.nan
                SDS['pair'][SDS['pair']<0]=np.nan
                SDS['Hwv'][SDS['Hwv']<0]=np.nan
                SDS['Twv'][SDS['Twv']<0]=np.nan
                SDS['psiwvr'][SDS['psiwvr']<-180]=np.nan
                SDS['Hsl'][SDS['Hsl']<0]=np.nan
                SDS['Tsl'][SDS['Tsl']<0]=np.nan
                SDS['psislr'][SDS['psislr']<-180]=np.nan
                SDS['rhosw'][SDS['rhosw']<0]=np.nan
                SDS['vsw'][SDS['vsw']<0]=np.nan
    
        if LogType == 'Noon' or LogType == 'ME':
            if SDS['TairTCinME'].ndim==1:
                SDS['TairTCinME']=SDS['TairTCinME'].reshape(1,-1)
                SDS['TairTCinME'] = SDS['TairTCinME'].T
            if SDS['TltcwACinME'].ndim == 1:
                SDS['TltcwACinME'] = SDS['TltcwACinME'].reshape(1,-1)
                SDS['TltcwACinME'] = SDS['TltcwACinME'].T
            if SDS['pambairER'].ndim == 1:
                SDS['pambairER'] =SDS['pambairER'].reshape(1,-1)
                SDS['pambairER'] =SDS['pambairER'].T
            if SDS['pscavME'].ndim == 1:
                SDS['pscavME'] =SDS['pscavME'].reshape(1,-1)
                SDS['pscavME'] =SDS['pscavME'].T
            if SDS['TegEVoutME'].ndim == 1:
                SDS['TegEVoutME'] =SDS['TegEVoutME'].reshape(1,-1)
                SDS['TegEVoutME'] =SDS['TegEVoutME'].T
            if SDS['TegEVoutcylME'].ndim == 1:
                SDS['TegEVoutcylME'] =SDS['TegEVoutcylME'].reshape(1,-1)
                SDS['TegEVoutcylME'] =SDS['TegEVoutcylME'].T
                
            # Correct service variables for ISO conditions
            #SDS['TegEVoutisocylME'] need to rechecked because according to FPC their is a bug in this(dimension issue)
    
            SDS['pscavabsisoME'] = iso.ISOCorrections(SDS['pscavME'],SDS['TairTCinME'],SDS['TltcwACinME'],RDS['F1iso'][1],RDS['F2iso'][1],RDS['Kiso'][1])+SDS['pambairER']/1000
            SDS['TegEVoutisoME'] = iso.ISOCorrections(SDS['TegEVoutME'], SDS['TairTCinME'],
                                                                SDS['TltcwACinME'], RDS['F1iso'][0], RDS['F2iso'][0], RDS['Kiso'][0])
    
            # Calculate differences
            SDS['dTcwACTCME'] = SDS['TltcwACoutTCME'] - SDS['TltcwACinTCME']
            SDS['dTcwACME'] = np.mean(SDS['dTcwACTCME'],1)
    
        # ME calculations
    
        if LogType == 'ME':
            if SDS['pcompcylME'].ndim==1:
                SDS['pcompcylME']=SDS['pcompcylME'].reshape(1,-1)
                SDS['pcompcylME'] = SDS['pcompcylME'].T
                 
            if SDS['TairTCinME'].ndim ==1:
                SDS['TairTCinME'] = SDS['TairTCinME'].reshape(1,-1)
                SDS['TairTCinME'] = SDS['TairTCinME'].T
                 
            if SDS['TltcwACinME'].ndim==1:
                SDS['TltcwACinME'] = SDS['TltcwACinME'].reshape(1,-1)
                SDS['TltcwACinME'] = SDS['TltcwACinME'].T
            if SDS['pambairER'].ndim ==1:
                SDS['pambairER'] = SDS['pambairER'].reshape(1,-1)
                SDS['pambairER'] = SDS['pambairER'].T
            if SDS['pcompME'].ndim ==1:
                SDS['pcompME'] = SDS['pcompME'].reshape(1,-1)
                SDS['pcompME'] = SDS['pcompME'].T
                 
            if SDS['pmaxcylME'].ndim ==1:
                SDS['pmaxcylME']=SDS['pmaxcylME'].reshape(1,-1)
                SDS['pmaxcylME'] = SDS['pmaxcylME'].T
            if SDS['pmaxME'].ndim ==1:
                SDS['pmaxME'] =SDS['pmaxME'].reshape(1,-1)
                SDS['pmaxME'] = SDS['pmaxME'].T
            
            # Calculate differences
            SDS['dTaircwACTCME'] = SDS['TairACoutTCME'] - SDS['TltcwACinTCME']
            SDS['dTaircwACME'] = np.mean(SDS['dTaircwACTCME'],1)
            SDS['TegEVoutisocylME'] = iso.ISOCorrections(SDS['TegEVoutcylME'],SDS['TairTCinME'],
                                                                   SDS['TltcwACinME'], RDS['F1iso'][0], RDS['F2iso'][0], RDS['Kiso'][0])
            # Correct service variables for ISO conditions
            
            SDS['pcompabsisocylME'] = iso.ISOCorrections(SDS['pcompcylME'], SDS['TairTCinME'], SDS['TltcwACinME'],
                                                                   RDS['F1iso'][2], RDS['F2iso'][2], RDS['Kiso'][2])+SDS['pambairER']/1000
            SDS['pcompabsisoME'] = iso.ISOCorrections(SDS['pcompME'], SDS['TairTCinME'], SDS['TltcwACinME'],
                                                                RDS['F1iso'][2], RDS['F2iso'][2], RDS['Kiso'][2])+SDS['pambairER']/1000
    
            SDS['pmaxabsisocylME'] = iso.ISOCorrections(SDS['pmaxcylME'], SDS['TairTCinME'], SDS['TltcwACinME'],
                                                                  RDS['F1iso'][3], RDS['F2iso'][3], RDS['Kiso'][3])+SDS['pambairER']/1000
            SDS['pmaxabsisoME'] = iso.ISOCorrections(SDS['pmaxME'], SDS['TairTCinME'], SDS['TltcwACinME'],
                                                               RDS['F1iso'][3], RDS['F2iso'][3], RDS['Kiso'][3])+SDS['pambairER']/1000
    
            # Calculate pressure rise
            SDS['pmax_pcompisoME'] = SDS['pmaxabsisoME'] - SDS['pcompabsisoME']
    
            #Calculate pressure ratio
            SDS['pcomp_pscavabsisoME'] = SDS['pcompabsisoME'] / SDS['pscavabsisoME']
    
            eetaTCtotcalcTCME = []
            eetaTCcompcalcTCME= []
            eetaTCturbcalcTCME= []
            
            # Calculate TC efficiencies
            for i in range(0, RDS['nTCME'][Eidx-1].astype(int)):
                etaTCtotcalcTCME,etaTCcompcalcTCME, etaTCturbcalcTCME = tce.TCefficiency(SDS['pambairER'].ravel(), SDS['dpairACTCME'][:, i].ravel(),\
                                                                                SDS['TairTCinTCME'][:, i].ravel(), SDS['NTCTCME'][:, i].ravel(), SDS['pscavME'].ravel(),\
                                                                                    SDS['pegRME'].ravel(), SDS['pegTCoutTCME'][:, i].ravel(), SDS['TegTCinTCME'][:, i].ravel(),\
                                                                                        RDS['dTCcompME'][Eidx-1, i].ravel(), RDS['muTCME'][Eidx-1, i].ravel())
                eetaTCtotcalcTCME.append(etaTCtotcalcTCME)
                eetaTCcompcalcTCME.append(etaTCcompcalcTCME)
                eetaTCturbcalcTCME.append(etaTCturbcalcTCME)
            
                
            SDS['etaTCtotcalcTCME']  =    np.array(eetaTCtotcalcTCME).T
            SDS['etaTCcompcalcTCME']  =    np.array(eetaTCcompcalcTCME).T
            SDS['etaTCturbcalcTCME']  =    np.array(eetaTCturbcalcTCME).T
    
            SDS['etaTCtotcalcME']=np.mean(SDS['etaTCtotcalcTCME'],1)
            SDS['etaTCcompcalcME']=np.mean(SDS['etaTCcompcalcTCME'],1)
            SDS['etaTCturbcalcME']=np.mean(SDS['etaTCturbcalcTCME'],1)
            
            #Indicate logs with auxiliary blower on
            DS['AuxBlowerOn']=np.zeros(SDS['pambairER'].shape)
            AuxBlowerOncheck=np.flatnonzero(SDS['AuxBlowME']==1).reshape(1,-1).T
            if len(AuxBlowerOncheck)>0:
                DS['AuxBlowerOn'][AuxBlowerOncheck]=1
                print('Auxiliary blower is on!')
       
        ################################################################################
        # Noon/AE calculations 
        ################################################################################
        
        if 'Noon' in LogType or 'AE' in  LogType:
            #Correct service variables for ISO conditions
            SDS['TegEVoutisoAE']=iso.ISOCorrections(SDS['TegEVoutAE'],SDS['TairTCinAE'],SDS['TltcwACinAE'],RDS['F1iso'][0],RDS['F2iso'][0],RDS['Kiso'][0])
            #must add corrections
            SDS['TegTCinisoAE']=SDS['TegTCinAE']
            SDS['TegTCoutisoAE']=SDS['TegTCoutAE']
        
        if 'AE' in LogType:
            if 'RHAE' not in SDS:
                SDS['RHAE']=SDS['Dt'] #repmat(SDS.Dt,1,RDS.nME);
            #Must add new corrections and for more variables, e.g. NTC, TegTCin, TegTCout, etc.
            if SDS['TairTCinAE'].ndim ==1:
                SDS['TairTCinAE'] = SDS['TairTCinAE'].reshape(1,-1)
                SDS['TairTCinAE'] = SDS['TairTCinAE'].T
                 
            if SDS['TltcwACinAE'].ndim==1:
                SDS['TltcwACinAE'] = SDS['TltcwACinAE'].reshape(1,-1)
                SDS['TltcwACinAE'] = SDS['TltcwACinAE'].T
            if SDS['pmaxcylAE'].ndim ==1:
                SDS['pmaxcylAE']=SDS['pmaxcylAE'].reshape(1,-1)
                SDS['pmaxcylAE'] = SDS['pmaxcylAE'].T
            if SDS['pmaxAE'].ndim ==1:
                SDS['pmaxAE'] =SDS['pmaxAE'].reshape(1,-1)
                SDS['pmaxAE'] = SDS['pmaxAE'].T
            if SDS['pambairER'].ndim ==1:
                SDS['pambairER'] = SDS['pambairER'].reshape(1,-1)
                SDS['pambairER'] = SDS['pambairER'].T
            #Correct service variables for ISO conditions
            SDS['pscavabsisoAE']=iso.ISOCorrections(SDS['pscavAE'].reshape(1,-1).T,SDS['TairTCinAE'],SDS['TltcwACinAE'],RDS['F1iso'][1],RDS['F2iso'][1],RDS['Kiso'][1])+SDS['pambairER']/1000
            #SDS['TegEVoutisocylAE']=iso.ISOCorrections(SDS['TegEVoutcylAE'],SDS['TairTCinAE'],SDS['TltcwACinAE'],RDS['F1iso'][0],RDS['F2iso'][0],RDS['Kiso'][2])
            SDS['TegEVoutisocylAE']=iso.ISOCorrections(SDS['TegEVoutcylAE'],SDS['TairTCinAE'],SDS['TltcwACinAE'],RDS['F1iso'][0],RDS['F2iso'][0],RDS['Kiso'][0])
            SDS['pmaxabsisocylAE']=iso.ISOCorrections(SDS['pmaxcylAE'],SDS['TairTCinAE'],SDS['TltcwACinAE'],RDS['F1iso'][3],RDS['F2iso'][3],RDS['Kiso'][3])+SDS['pambairER']/1000
            SDS['pmaxabsisoAE']=iso.ISOCorrections(SDS['pmaxAE'],SDS['TairTCinAE'],SDS['TltcwACinAE'],RDS['F1iso'][3],RDS['F2iso'][3],RDS['Kiso'][3])+SDS['pambairER']/1000
        
            #Characterize  fuel mode for DF engines
            if 'Yes' in RDS['DualFuelAE']:
                SDS['mFOCAE']=SDS['mFOCAE'].reshape(1, -1).T
                SDS['mGOCAE']=SDS['mGOCAE'].reshape(1, -1).T
    
                #Define fuel and gas ratios
                mFmG = np.concatenate((SDS['mFOCAE'], SDS['mGOCAE']), axis=1)
                SDS['FORatioAE']=np.divide(SDS['mFOCAE'].astype("float64"),nsw.nansumwrapper(mFmG,axis = 1).reshape(1,-1).T.astype("float64"),out =np.full_like(SDS['mFOCAE'].astype("float64"),float('NaN')),where = nsw.nansumwrapper(mFmG,axis = 1).reshape(1,-1).T.astype("float64")!=0)
                SDS['GORatioAE']=np.divide(SDS['mGOCAE'].astype("float64"),nsw.nansumwrapper(mFmG,axis=1).reshape(1,-1).T.astype("float64"),out =np.full_like(SDS['mGOCAE'].astype("float64"),float('NaN')),where = nsw.nansumwrapper(mFmG,axis=1).reshape(1,-1).T.astype("float64")!=0)
    
                #Memory pre-allocation
                SDS['FuelModeAE']=np.empty(np.shape(SDS['pambairER']))
                SDS['FuelModeAE'][:] =np.nan
                #0=Gas, 1=FO, 2=Mixed
                SDS['FuelModeAE'][((pd.isnull(SDS['FORatioAE'])) | (SDS['FORatioAE']<=0.1)) & (SDS['GORatioAE']>=0.9)]=0
                SDS['FuelModeAE'][SDS['FORatioAE']==1]=1
                SDS['FuelModeAE'][(SDS['FORatioAE']>0.1) & (SDS['GORatioAE']<0.9)]=2
    
                #Define service data fuel mode indices
                SDS['FOModeAE']=np.flatnonzero(SDS['FuelModeAE']==1).reshape(1,-1).T
                SDS['GOModeAE']=np.flatnonzero(SDS['FuelModeAE']==0).reshape(1,-1).T
    
                #Discard in mixed mode (currently for all engines, later to be per engine)
                DS['DFMixedModeAE']=np.zeros(SDS['pambairER'].shape)
                DFMixedModeAEcheck=np.flatnonzero(SDS['FuelModeAE']==2).reshape(1,-1).T
                if len(DFMixedModeAEcheck)>0:
                    SDS['LogValidity'][DFMixedModeAEcheck]=0
                    DS['DFMixedModeAE'][DFMixedModeAEcheck]=1
                    print('Dual fuel in AE in mixed mode!')    
        #SRQST['TotalTimeLapse'].append(time.time()-start_time)
        #SRQST['Remarks'].append("complete")
        #return[SDS,DS,SRQST]
    except Exception as err:
        exp="Error in ManipulateServiceData {} in line No. {} for IMO {}".format(err,sys.exc_info()[-1].tb_lineno,IMO)
        logger.error(exp,exc_info=True)
        ExceptionHandler(LogType,exp)
        SRQSTD['TotalTimeLapse']=time.time()-start_time              
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return({},{},SRQST)
        
    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time               
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(SDS,DS,SRQST)

