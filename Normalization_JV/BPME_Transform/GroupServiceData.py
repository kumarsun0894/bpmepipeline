# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 14:17:48 2020

@author: Subhin Antony

% GroupServiceData
% Description:  This function groups service data by averaging them, e.g.
%               for 10 cylinder ME, the exhaust gas temperature is reported
%               per cylinder in noon log. These values are averaged to get
%               only 1 value for temperature instead of 10 values.
%
% Input:        SDS (structure)
%               RDS (structure)
%               LogType (string)
%
% Output:       SDS (structure)
%
% See also:     BPMEngineSingleVessel

"""
from ExceptionHandler import ExceptionHandler
import time
import logging 
# create logger
logger = logging.getLogger('BPMEngine.2.02')
import numpy as np
import sys

def GroupServiceData(IMO,SDS,RDS,Eidx,LogType,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        DS={}
        DS['LogID']=SDS['LogID']
        SDS['LogValidity']=np.ones(np.shape(SDS['LogID']))
        
        
        
        
        
        # HP log variables grouping  
        if LogType =='HP':
            #Group variables that are "per ME"
            DESME_dCnt=[]
            DRME_dCnt=[]
            FPIME_avg=[]
            TscavME_avg=[]
            PeffestME_avg=[]
            NTCME_avg=[]
            TltcwACinME_avg=[]
            
            for i in range(1,int(RDS['nME'])+1):
                DESME_dCnt.append(SDS['DESME_dCnt_{}'.format(i)])
                del SDS['DESME_dCnt_{}'.format(i)]
                DRME_dCnt.append(SDS['DRME_dCnt_{}'.format(i)])
                del SDS['DRME_dCnt_{}'.format(i)]
                FPIME_avg.append(SDS['FPIME_avg_{}'.format(i)])
                del SDS['FPIME_avg_{}'.format(i)]
                TscavME_avg.append(SDS['TscavME_avg_{}'.format(i)])
                del SDS['TscavME_avg_{}'.format(i)]
                PeffestME_avg.append(SDS['PeffestME_avg_{}'.format(i)])
                del SDS['PeffestME_avg_{}'.format(i)]
                NTCME_avg.append(SDS['NTCME_avg_{}'.format(i)])
                del SDS['NTCME_avg_{}'.format(i)]
                TltcwACinME_avg.append(SDS['TltcwACinME_avg_{}'.format(i)])
                del SDS['TltcwACinME_avg_{}'.format(i)]
            
            SDS['DESME_dCnt']=np.array(DESME_dCnt).T 
            SDS['DRME_dCnt']=np.array(DRME_dCnt).T
            SDS['FPIME_avg']=np.array(FPIME_avg).T
            SDS['TscavME_avg']=np.array(TscavME_avg).T
            SDS['PeffestME_avg']=np.array(PeffestME_avg).T
            SDS['NTCME_avg']=np.array(NTCME_avg).T
            SDS['TltcwACinME_avg']=np.array(TltcwACinME_avg).T
            
            
                
                
            #Group variables that are "per SG"
            
            if 'Yes' in RDS['SG']:
                PelSG_avg=[]
                for i in range(1,int(RDS['nSG'])+1):
                    PelSG_avg.append(SDS['PelSG_avg_{}'.format(i)])
                    del SDS['PelSG_avg_{}'.format(i)]
                SDS['PelSG_avg']=np.array(PelSG_avg).T
                
                    
                    
                
                    
                    
        # Auto log variables grouping 
        if 'Auto' in LogType:
            #Group variables that are "per ME"
            PSME_avg_auto=[]
            NME_avg_auto=[]
            TltcwACinME_avg=[]
            
            for i in range(1,int(RDS['nME'])+1):
                PSME_avg_auto.append(SDS['PSME_avg_auto_{}'.format(i)])
                del SDS['PSME_avg_auto_{}'.format(i)]
                NME_avg_auto.append(SDS['NME_avg_auto_{}'.format(i)])
                del SDS['NME_avg_auto_{}'.format(i)]
                TltcwACinME_avg.append(SDS['TltcwACinME_avg_{}'.format(i)])  
                del SDS['TltcwACinME_avg_{}'.format(i)]
            SDS['PSME_avg_auto']=np.array(PSME_avg_auto).T
            SDS['NME_avg_auto']=np.array(NME_avg_auto).T
            SDS['TltcwACinME_avg']=np.array(TltcwACinME_avg).T
            
            
                
        # Noon log variables grouping
        if 'Noon' in LogType:
            PSME=[]
            DESME_dCnt=[]
            RHME_dCnt=[]
            DRME_dCnt=[]
            NME=[]
            FPIME=[]
            SCOCME_set=[]
            TscavME=[]
            pscavME=[]
            ThtcwinME=[]
            pSOME=[]
            TSOinME=[]
            TSOoutME=[]
            pegRME=[]
            #Group variables that are "per ME"
            for i in range(1,int(RDS['nME'])+1):
                PSME.append(SDS['PSME_{}'.format(i)])
                del SDS['PSME_{}'.format(i)]
                DESME_dCnt.append(SDS['DESME_dCnt_{}'.format(i)])
                del SDS['DESME_dCnt_{}'.format(i)]
                RHME_dCnt.append(SDS['RHME_dCnt_{}'.format(i)])
                del SDS['RHME_dCnt_{}'.format(i)]
                DRME_dCnt.append(SDS['DRME_dCnt_{}'.format(i)])
                del SDS['DRME_dCnt_{}'.format(i)]
                NME.append(SDS['NME_{}'.format(i)])
                del SDS['NME_{}'.format(i)]
                FPIME.append(SDS['FPIME_{}'.format(i)])
                del SDS['FPIME_{}'.format(i)]
                SCOCME_set.append(SDS['SCOCME_set_{}'.format(i)])
                del SDS['SCOCME_set_{}'.format(i)] 
                TscavME.append(SDS['TscavME_{}'.format(i)])
                del SDS['TscavME_{}'.format(i)]
                pscavME.append(SDS['pscavME_{}'.format(i)])
                del SDS['pscavME_{}'.format(i)]
                ThtcwinME.append(SDS['ThtcwinME_{}'.format(i)])
                del SDS['ThtcwinME_{}'.format(i)]
                pSOME.append(SDS['pSOME_{}'.format(i)])
                del SDS['pSOME_{}'.format(i)]
                TSOinME.append(SDS['TSOinME_{}'.format(i)])
                del SDS['TSOinME_{}'.format(i)]
                TSOoutME.append(SDS['TSOoutME_{}'.format(i)])
                del SDS['TSOoutME_{}'.format(i)]
                pegRME.append(SDS['pegRME_{}'.format(i)])
                del SDS['pegRME_{}'.format(i)]
            SDS['PSME']=np.array(PSME).T
            SDS['RHME_dCnt']=np.array(RHME_dCnt).T
            SDS['DRME_dCnt']=np.array(DRME_dCnt).T
            SDS['DESME_dCnt']=np.array(DESME_dCnt).T
            SDS['NME']=np.array(NME).T
            SDS['FPIME']=np.array(FPIME).T
            SDS['SCOCME_set']=np.array(SCOCME_set).T
            SDS['TscavME']=np.array(TscavME).T
            SDS['pscavME']=np.array(pscavME).T
            SDS['ThtcwinME']=np.array(ThtcwinME).T
            SDS['pSOME']=np.array(pSOME).T
            SDS['TSOinME']=np.array(TSOinME).T
            SDS['TSOoutME']=np.array(TSOoutME).T
            SDS['pegRME']=np.array(pegRME).T
            #Group variables that are "per ME" and "per cylinder"                 ####
            
            
            # ThtcwoutcylME=[]
            # TegEVoutcylME=[]
            # for i in range(1,int(RDS['nME'])+1):
            #     ThtcwoutcylMEME=[]
            #     TegEVoutcylMEME=[]
                
            #     for j in range(1,int(RDS['ncME'][i-1])+1):
            #         ThtcwoutcylMEME.append(SDS['ThtcwoutcylME_{}_{}'.format(i,j)])
            #         del SDS['ThtcwoutcylME_{}_{}'.format(i,j)]
                    
            #         TegEVoutcylMEME.append(SDS['TegEVoutcylME_{}_{}'.format(i,j)])
            #         del SDS['TegEVoutcylME_{}_{}'.format(i,j)]
            #     ThtcwoutcylMEME=np.array(ThtcwoutcylMEME)
            #     ThtcwoutcylMEME=np.array(ThtcwoutcylMEME)
            #     ThtcwoutcylME.append(ThtcwoutcylMEME)
            #     TegEVoutcylME.append(TegEVoutcylMEME)
                
                    
            # SDS['ThtcwoutcylME']=np.array(ThtcwoutcylME).transpose(2,0,1)
            # SDS['TegEVoutcylME']=np.array(TegEVoutcylME).transpose(2,0,1)
            SDS['ThtcwoutcylME']=np.empty((int(len(SDS['pambairER'])),int(RDS['nME']),int(np.max(RDS['ncME']))))
            SDS['ThtcwoutcylME'].fill(np.nan)
            SDS['TegEVoutcylME']=np.empty((int(len(SDS['pambairER'])),int(RDS['nME']),int(np.max(RDS['ncME']))))
            SDS['TegEVoutcylME'].fill(np.nan)
            for i in range(1,int(RDS['nME'])+1):
                for j in range(1,int(RDS['ncME'][i-1])+1):
                     SDS['ThtcwoutcylME'][:,i-1,j-1]=SDS['ThtcwoutcylME_{}_{}'.format(i,j)]
                     del SDS['ThtcwoutcylME_{}_{}'.format(i,j)]
                     SDS['TegEVoutcylME'][:,i-1,j-1]=SDS['TegEVoutcylME_{}_{}'.format(i,j)]
                     del SDS['TegEVoutcylME_{}_{}'.format(i,j)]
                
            #Find average values across ME cylinders
            SDS['ThtcwoutME']=np.mean((SDS['ThtcwoutcylME']),axis=2)
            SDS['TegEVoutME']=np.mean((SDS['TegEVoutcylME']),axis=2)
            
            #Group variables that are "per ME" and "per TC"
            # NTCTCME=[]
            # dpairACTCME=[]
            # TltcwACinME=[]
            # TltcwACoutME=[]
            # #TairACoutME=[]
            # TegTCinME=[]
            # TegTCoutME=[]
            # pegTCoutME=[]
            # for i in range(1,int(RDS['nME'])+1):
            #     NTCTCMEME=[]
            #     dpairACTCMEME=[]
            #     TltcwACinMEME=[]
            #     TltcwACoutMEME=[]
            #     #TairACoutMEME=[]
            #     TegTCinMEME=[]
            #     TegTCoutMEME=[]
            #     pegTCoutMEME=[]
            #     for j in range(1,int(RDS['nTCME'][0])+1):
            #         NTCTCMEME.append(SDS['NTCTCME_{}_{}'.format(i,j)])
            #         del SDS['NTCTCME_{}_{}'.format(i,j)]
            #         dpairACTCMEME.append(SDS['dpairACTCME_{}_{}'.format(i,j)])
            #         del SDS['dpairACTCME_{}_{}'.format(i,j)]
            #         TltcwACinMEME.append(SDS['TltcwACinTCME_{}_{}'.format(i,j)])
            #         del SDS['TltcwACinTCME_{}_{}'.format(i,j)]
            #         TltcwACoutMEME.append(SDS['TltcwACoutTCME_{}_{}'.format(i,j)])
            #         del SDS['TltcwACoutTCME_{}_{}'.format(i,j)]
            #         #TairACoutMEME.append(SDS['TairACoutTCME_{}_{}'.format(i,j)])
            #         TegTCinMEME.append(SDS['TegTCinTCME_{}_{}'.format(i,j)])
            #         del SDS['TegTCinTCME_{}_{}'.format(i,j)]
            #         TegTCoutMEME.append(SDS['TegTCoutTCME_{}_{}'.format(i,j)])
            #         del SDS['TegTCoutTCME_{}_{}'.format(i,j)]
            #         pegTCoutMEME.append(SDS['pegTCoutTCME_{}_{}'.format(i,j)])
            #         del SDS['pegTCoutTCME_{}_{}'.format(i,j)]
                
                
            #     NTCTCME.append(np.array(NTCTCMEME))
            #     dpairACTCME.append(np.array(dpairACTCMEME))
            #     TltcwACinME.append(np.array(TltcwACinMEME))
            #     TltcwACoutME.append(np.array(TltcwACoutMEME))
            #     #TairACoutME.append(np.array(TairACoutMEME))
            #     TegTCinME.append(np.array(TegTCinMEME))
            #     TegTCoutME.append(np.array(TegTCoutMEME))
            #     pegTCoutME.append(np.array(pegTCoutMEME))
        
        
            # SDS['NTCTCME']=np.array(NTCTCME).transpose(2,0,1)
            # SDS['dpairACTCME']=np.array(dpairACTCME).transpose(2,0,1)
            # SDS['TltcwACinTCME']=np.array(TltcwACinME).transpose(2,0,1)
            # SDS['TltcwACoutTCME']=np.array(TltcwACoutME).transpose(2,0,1)
            # #SDS['TairACoutME']=np.array(TairACoutME).transpose(2,0,1)
            # SDS['TegTCinTCME']=np.array(TegTCinME).transpose(2,0,1)
            # SDS['TegTCoutTCME']=np.array(TegTCoutME).transpose(2,0,1)
            # SDS['pegTCoutTCME']=np.array(pegTCoutME).transpose(2,0,1)
            
            SDS['NTCTCME']=np.empty((int(len(SDS['pambairER'])),int(RDS['nME']),int(np.max(RDS['nTCME']))))
            SDS['NTCTCME'].fill(np.nan)
            SDS['dpairACTCME']=np.empty((int(len(SDS['pambairER'])),int(RDS['nME']),int(np.max(RDS['nTCME']))))
            SDS['dpairACTCME'].fill(np.nan)
            SDS['TltcwACinTCME']=np.empty((int(len(SDS['pambairER'])),int(RDS['nME']),int(np.max(RDS['nTCME']))))
            SDS['TltcwACinTCME'].fill(np.nan)
            SDS['TltcwACoutTCME']=np.empty((int(len(SDS['pambairER'])),int(RDS['nME']),int(np.max(RDS['nTCME']))))
            SDS['TltcwACoutTCME'].fill(np.nan)
            #SDS['TairACoutTCME']=np.empty((int(len(SDS['pambairER'])),int(RDS['nME']),int(np.max(RDS['nTCME']))))
            SDS['TegTCinTCME']=np.empty((int(len(SDS['pambairER'])),int(RDS['nME']),int(np.max(RDS['nTCME']))))
            SDS['TegTCinTCME'].fill(np.nan)
            SDS['TegTCoutTCME']=np.empty((int(len(SDS['pambairER'])),int(RDS['nME']),int(np.max(RDS['nTCME']))))
            SDS['TegTCoutTCME'].fill(np.nan)
            SDS['pegTCoutTCME']=np.empty((int(len(SDS['pambairER'])),int(RDS['nME']),int(np.max(RDS['nTCME']))))
            SDS['pegTCoutTCME'].fill(np.nan)
            for i in range(1,int(RDS['nME'])+1):
                for j in range(1,int(RDS['nTCME'][i-1])+1):
                    SDS['NTCTCME'][:,i-1,j-1]=SDS['NTCTCME_{}_{}'.format(i,j)]
                    del SDS['NTCTCME_{}_{}'.format(i,j)]
                    SDS['dpairACTCME'][:,i-1,j-1]=SDS['dpairACTCME_{}_{}'.format(i,j)]
                    del SDS['dpairACTCME_{}_{}'.format(i,j)]
                    SDS['TltcwACinTCME'][:,i-1,j-1]=SDS['TltcwACinTCME_{}_{}'.format(i,j)]
                    del SDS['TltcwACinTCME_{}_{}'.format(i,j)]
                    SDS['TltcwACoutTCME'][:,i-1,j-1]=SDS['TltcwACoutTCME_{}_{}'.format(i,j)]
                    del SDS['TltcwACoutTCME_{}_{}'.format(i,j)]
                    SDS['TegTCinTCME'][:,i-1,j-1]=SDS['TegTCinTCME_{}_{}'.format(i,j)]
                    del SDS['TegTCinTCME_{}_{}'.format(i,j)]
                    SDS['TegTCoutTCME'][:,i-1,j-1]=SDS['TegTCoutTCME_{}_{}'.format(i,j)]
                    del SDS['TegTCoutTCME_{}_{}'.format(i,j)]
                    SDS['pegTCoutTCME'][:,i-1,j-1]=SDS['pegTCoutTCME_{}_{}'.format(i,j)]
                    del SDS['pegTCoutTCME_{}_{}'.format(i,j)]
                    
                    
                    
            
            #Find average values across ME TCs
            SDS['NTCME']=np.mean(SDS['NTCTCME'],axis=2)
            SDS['dpairACME']=np.mean(SDS['dpairACTCME'],axis=2)
            SDS['TltcwACinME']=np.mean(SDS['TltcwACinTCME'],axis=2)
            # print(SDS['TltcwACinME'].shape)
            SDS['TltcwACoutME']=np.mean(SDS['TltcwACoutTCME'],axis=2)
            #SDS['TairACoutME']=np.mean(SDS['TairACoutTCME'],axis=2)
            SDS['TegTCinME']=np.mean(SDS['TegTCinTCME'],axis=2)
            SDS['TegTCoutME']=np.mean(SDS['TegTCoutTCME'],axis=2)
            SDS['pegTCoutME']=np.mean(SDS['pegTCoutTCME'],axis=2)
            
            #Group variables that are "per AE"
            SDS['PelAE_avg']=np.empty((int(len(SDS['pambairER'])),int(RDS['nAE'])))
            SDS['PelAE_avg'].fill(np.nan)
            SDS['DEelAE_dCnt']=np.empty((int(len(SDS['pambairER'])),int(RDS['nAE'])))
            SDS['DEelAE_dCnt'].fill(np.nan)
            SDS['RHAE_dCnt']=np.empty((int(len(SDS['pambairER'])),int(RDS['nAE'])))
            SDS['RHAE_dCnt'].fill(np.nan)
            SDS['ThtcwinAE']=np.empty((int(len(SDS['pambairER'])),int(RDS['nAE'])))
            SDS['ThtcwinAE'].fill(np.nan)
            SDS['TSOinAE']=np.empty((int(len(SDS['pambairER'])),int(RDS['nAE'])))
            SDS['TSOinAE'].fill(np.nan)
            SDS['TSOoutAE']=np.empty((int(len(SDS['pambairER'])),int(RDS['nAE'])))
            SDS['TSOoutAE'].fill(np.nan)
            SDS['NTCAE']=np.empty((int(len(SDS['pambairER'])),int(RDS['nAE'])))
            SDS['NTCAE'].fill(np.nan)
            SDS['TegTCinAE']=np.empty((int(len(SDS['pambairER'])),int(RDS['nAE'])))
            SDS['TegTCinAE'].fill(np.nan)
            SDS['TegTCoutAE']=np.empty((int(len(SDS['pambairER'])),int(RDS['nAE'])))
            SDS['TegTCoutAE'].fill(np.nan)
            for i in range(1,int(RDS['nAE'])+1):
                SDS['PelAE_avg'][:,i-1]=SDS['PelAE_avg_{}'.format(i)]
                del SDS['PelAE_avg_{}'.format(i)]
                SDS['DEelAE_dCnt'][:,i-1]=SDS['DEelAE_dCnt_{}'.format(i)]
                del SDS['DEelAE_dCnt_{}'.format(i)]
                SDS['RHAE_dCnt'][:,i-1]=SDS['RHAE_dCnt_{}'.format(i)]
                del SDS['RHAE_dCnt_{}'.format(i)]
                SDS['ThtcwinAE'][:,i-1]=SDS['ThtcwinAE_{}'.format(i)]
                del SDS['ThtcwinAE_{}'.format(i)]
                SDS['TSOinAE'][:,i-1]=SDS['TSOinAE_{}'.format(i)]
                del SDS['TSOinAE_{}'.format(i)]
                SDS['TSOoutAE'][:,i-1]=SDS['TSOoutAE_{}'.format(i)]
                del SDS['TSOoutAE_{}'.format(i)]
                SDS['NTCAE'][:,i-1]=SDS['NTCAE_{}'.format(i)]
                del SDS['NTCAE_{}'.format(i)]
                SDS['TegTCinAE'][:,i-1]=SDS['TegTCinAE_{}'.format(i)]
                del SDS['TegTCinAE_{}'.format(i)]
                SDS['TegTCoutAE'][:,i-1]=SDS['TegTCoutAE_{}'.format(i)]
                del SDS['TegTCoutAE_{}'.format(i)]
            #Group variables that are "per AE" and "per cylinder"
            SDS['ThtcwoutcylAE']=np.empty((int(len(SDS['pambairER'])),int(RDS['nAE']),int(np.max(RDS['ncAE']))))
            SDS['ThtcwoutcylAE'].fill(np.nan)
            SDS['TegEVoutcylAE']=np.empty((int(len(SDS['pambairER'])),int(RDS['nAE']),int(np.max(RDS['ncAE']))))
            SDS['TegEVoutcylAE'].fill(np.nan)
            for i in range(1,int(RDS['nAE'])+1):
                for j in range(1,int(RDS['ncAE'][i-1])+1):
                    SDS['ThtcwoutcylAE'][:,i-1,j-1]=SDS['ThtcwoutcylAE_{}_{}'.format(i,j)]
                    del SDS['ThtcwoutcylAE_{}_{}'.format(i,j)]
                    SDS['TegEVoutcylAE'][:,i-1,j-1]=SDS['TegEVoutcylAE_{}_{}'.format(i,j)]
                    del SDS['TegEVoutcylAE_{}_{}'.format(i,j)]
            #Find average values across AE cylinders
            SDS['ThtcwoutAE']=np.mean((SDS['ThtcwoutcylAE']),axis=2)
            SDS['TegEVoutAE']=np.mean((SDS['TegEVoutcylAE']),axis=2)
            #Group variables that are "per SG"
            if RDS['SG']=='Yes':
                SDS['RHSG_dCnt']=np.empty((int(len(SDS['pambairER'])),int(RDS['nSG'])))
                SDS['RHSG_dCnt'].fill(np.nan)
                SDS['PelSG_avg']=np.empty((int(len(SDS['pambairER'])),int(RDS['nSG'])))
                SDS['PelSG_avg'].fill(np.nan)
                SDS['DEelSG_dCnt']=np.empty((int(len(SDS['pambairER'])),int(RDS['nSG'])))
                SDS['DEelSG_dCnt'].fill(np.nan)
                for i in range(1,int(RDS['nSG'])+1):
                    SDS['RHSG_dCnt'][:,i-1]=SDS['RHSG_dCnt_{}'.format(i)]
                    del SDS['RHSG_dCnt_{}'.format(i)]
                    SDS['PelSG_avg'][:,i-1]=SDS['PelSG_avg_{}'.format(i)]
                    del SDS['PelSG_avg_{}'.format(i)]
                    SDS['DEelSG_dCnt'][:,i-1]=SDS['DEelSG_dCnt_{}'.format(i)]
                    del SDS['DEelSG_dCnt_{}'.format(i)]
                
           
            #Check if nFOGME>5 and discard these logs
            DS['FOMEChangeOvers']=np.zeros(np.shape(SDS['pambairER']))
            FOMEChangeOverscheck=np.argwhere(SDS['nFOGME']>5)            ##
            if np.size(FOMEChangeOverscheck)>0:
                for i in FOMEChangeOverscheck: 
                    SDS['nFOGME'][i]=5
                    SDS['LogValidity'][i]=0
                    DS['FOMEChangeOvers'][i]=1
                print('ME FO change overs > 5!')
                
                
                
            #Check if nFOGAE>5 and discard these logs 
            DS['FOAEChangeOvers']=np.zeros(np.shape(SDS['pambairER']))
            FOAEChangeOverscheck=np.argwhere(SDS['nFOGAE']>5)            ##
            if np.size(FOAEChangeOverscheck)>0:
                for i in FOAEChangeOverscheck: 
                    SDS['nFOGAE'][i]=5
                    SDS['LogValidity'][i]=0
                    DS['FOAEChangeOvers'][i]=1
                print('AE FO change overs > 5!')
            #Group variables that are "per ME FOG"
                
            FOGgME=[]
            VFOCgME=[]
            mFOCgME=[]
            TFOFMingME=[]
            vFOgME=[]
            rhoFOgME=[]
            LCVFOgME=[]
            SulFOgME=[]
                
            for j in range(1,int(np.max(SDS['nFOGME']))+1):
                FOGgME.append(SDS['FOGME_{}'.format(j)])
                del SDS['FOGME_{}'.format(j)]
                VFOCgME.append(SDS['VFOCME_dCnt_{}'.format(j)])
                del SDS['VFOCME_dCnt_{}'.format(j)]
                mFOCgME.append(SDS['mFOCME_dCnt_{}'.format(j)])
                del SDS['mFOCME_dCnt_{}'.format(j)]
                TFOFMingME.append(SDS['TFOFMinME_avg_{}'.format(j)])
                del SDS['TFOFMinME_avg_{}'.format(j)]
                vFOgME.append(SDS['vFOME_an_{}'.format(j)])
                del SDS['vFOME_an_{}'.format(j)]
                rhoFOgME.append(SDS['rhoFOME_an_{}'.format(j)])
                del SDS['rhoFOME_an_{}'.format(j)]
                LCVFOgME.append(SDS['LCVFOME_an_{}'.format(j)])
                del SDS['LCVFOME_an_{}'.format(j)]
                SulFOgME.append(SDS['SulFOME_an_{}'.format(j)])
                del SDS['SulFOME_an_{}'.format(j)]
            SDS['FOGgME']=np.array(FOGgME).T
            SDS['VFOCgME']=np.array(VFOCgME).T
            SDS['mFOCgME']=np.array(mFOCgME).T
            SDS['TFOFMingME']=np.array(TFOFMingME).T
            SDS['vFOgME']=np.array(vFOgME).T
            SDS['rhoFOgME']=np.array(rhoFOgME).T
            SDS['LCVFOgME']=np.array(LCVFOgME).T
            SDS['SulFOgME']=np.array(SulFOgME).T
            
            #Group variables that are "per AE FOG"
            FOGgAE=[]
            VFOCgAE=[]
            mFOCgAE=[]
            TFOFMingAE=[]
            vFOgAE=[]
            rhoFOgAE=[]
            LCVFOgAE=[]
            SulFOgAE=[]
                
            for j in range(1,int(np.max(SDS['nFOGAE']))+1):
                FOGgAE.append(SDS['FOGAE_{}'.format(j)])
                del SDS['FOGAE_{}'.format(j)]
                VFOCgAE.append(SDS['VFOCAE_dCnt_{}'.format(j)])
                del SDS['VFOCAE_dCnt_{}'.format(j)]
                mFOCgAE.append(SDS['mFOCAE_dCnt_{}'.format(j)])
                del SDS['mFOCAE_dCnt_{}'.format(j)]
                TFOFMingAE.append(SDS['TFOFMinAE_avg_{}'.format(j)])
                del SDS['TFOFMinAE_avg_{}'.format(j)]
                vFOgAE.append(SDS['vFOAE_an_{}'.format(j)])
                del SDS['vFOAE_an_{}'.format(j)]
                rhoFOgAE.append(SDS['rhoFOAE_an_{}'.format(j)])
                del SDS['rhoFOAE_an_{}'.format(j)]
                LCVFOgAE.append(SDS['LCVFOAE_an_{}'.format(j)])
                del SDS['LCVFOAE_an_{}'.format(j)]
                SulFOgAE.append(SDS['SulFOAE_an_{}'.format(j)])
                del SDS['SulFOAE_an_{}'.format(j)]
            SDS['FOGgAE']=np.array(FOGgAE).T
            SDS['VFOCgAE']=np.array(VFOCgAE).T
            SDS['mFOCgAE']=np.array(mFOCgAE).T
            SDS['TFOFMingAE']=np.array(TFOFMingAE).T
            SDS['vFOgAE']=np.array(vFOgAE).T
            SDS['rhoFOgAE']=np.array(rhoFOgAE).T
            SDS['LCVFOgAE']=np.array(LCVFOgAE).T
            SDS['SulFOgAE']=np.array(SulFOgAE).T
            #Group variables that are "per ME COG"
            COGgME=[]
            VCOCgME=[]
            rhoCOgME=[]
            BNCOgME=[]
            for j in range(1,int(np.max(SDS['nCOGME']))+1):
                COGgME.append(SDS['COGME_{}'.format(j)])
                del SDS['COGME_{}'.format(j)]
                VCOCgME.append(SDS['VCOCME_dCnt_{}'.format(j)])
                del SDS['VCOCME_dCnt_{}'.format(j)]
                rhoCOgME.append(SDS['rhoCOME_spec_{}'.format(j)])
                del SDS['rhoCOME_spec_{}'.format(j)]
                BNCOgME.append(SDS['BNCOME_spec_{}'.format(j)])
                del SDS['BNCOME_spec_{}'.format(j)] 
            SDS['COGgME']=np.array(COGgME).T
            SDS['VCOCgME']=np.array(VCOCgME).T
            SDS['rhoCOgME']=np.array(rhoCOgME).T
            SDS['BNCOgME']=np.array(BNCOgME).T
        
            
        # ME log variables grouping
        if 'ME' in LogType:
            #Group variables that are "per cylinder"
            pindcylME_avg=[]
            pmaxcylME_avg=[]
            pcompcylME_avg=[]
            ThtcwoutcylME_avg=[]
            TLOPISToutcylME_avg=[]
            TegEVoutcylME_avg=[]
            TLIWATcylME_avg=[]
            phiIgncylME_avg=[]
            phiEVOcorcylME_set=[]
            TLOMBRGcylME_avg=[]
            TLOCHBRGcylME_avg=[]
            FPIcylME_avg=[]
            VITcylME_avg=[]
            pcomppscavcylME_set=[]
            HLcylME_set=[]
            LLcylME_set=[]
            pmaxcylME_set=[]
            RunInPISTcylME=[]
            SCOCcylME_avg=[]
            SCOCcylME_set=[]
            HMIcylME_set=[]
            ACCcylME_set=[]
           
            for i in range(1,int(RDS['ncME'][Eidx-1])+1):
                if 'pindcylME_avg_1' in  SDS.keys():
                    pindcylME_avg.append(SDS['pindcylME_avg_{}'.format(i)])
                    # del SDS['pindcylME_avg_{}'.format(i)]
                if 'pmaxcylME_avg_1' in  SDS.keys():
                    pmaxcylME_avg.append(SDS['pmaxcylME_avg_{}'.format(i)])
                    # del SDS['pmaxcylME_avg_{}'.format(i)]
                if 'pcompcylME_avg_1' in  SDS.keys():
                    pcompcylME_avg.append(SDS['pcompcylME_avg_{}'.format(i)])
                    # del SDS['pcompcylME_avg_{}'.format(i)]
                if 'ThtcwoutcylME_avg_1' in  SDS.keys():
                    ThtcwoutcylME_avg.append(SDS['ThtcwoutcylME_avg_{}'.format(i)])
                    # del SDS['ThtcwoutcylME_avg_{}'.format(i)]
                if 'TLOPISToutcylME_avg_1' in  SDS.keys():
                    TLOPISToutcylME_avg.append(SDS['TLOPISToutcylME_avg_{}'.format(i)])
                    # del SDS['TLOPISToutcylME_avg_{}'.format(i)]
                if 'TegEVoutcylME_avg_1' in  SDS.keys():
                    TegEVoutcylME_avg.append(SDS['TegEVoutcylME_avg_{}'.format(i)])
                    # del SDS['TegEVoutcylME_avg_{}'.format(i)]
                if 'TLIWATcylME_avg_1' in  SDS.keys():
                    TLIWATcylME_avg.append(SDS['TLIWATcylME_avg_{}'.format(i)])
                    # del SDS['TLIWATcylME_avg_{}'.format(i)]
                if 'phiIgncylME_avg_1' in  SDS.keys():
                    phiIgncylME_avg.append(SDS['phiIgncylME_avg_{}'.format(i)])
                    # del SDS['phiIgncylME_avg_{}'.format(i)]
                if 'phiEVOcorcylME_set_1' in  SDS.keys():
                    phiEVOcorcylME_set.append(SDS['phiEVOcorcylME_set_{}'.format(i)])
                    # del SDS['phiEVOcorcylME_set_{}'.format(i)]
                if 'TLOMBRGcylME_avg_1' in  SDS.keys():
                    TLOMBRGcylME_avg.append(SDS['TLOMBRGcylME_avg_{}'.format(i)])
                    # del SDS['TLOMBRGcylME_avg_{}'.format(i)]
                if 'TLOCHBRGcylME_avg_1' in  SDS.keys():
                    TLOCHBRGcylME_avg.append(SDS['TLOCHBRGcylME_avg_{}'.format(i)])
                    # del SDS['TLOCHBRGcylME_avg_{}'.format(i)]
                if 'FPIcylME_avg_1' in  SDS.keys():
                    FPIcylME_avg.append(SDS['FPIcylME_avg_{}'.format(i)])
                    # del SDS['FPIcylME_avg_{}'.format(i)]
                if 'VITcylME_avg_1' in  SDS.keys():
                    VITcylME_avg.append(SDS['VITcylME_avg_{}'.format(i)])
                    #del SDS['VITcylME_avg_{}'.format(i)]
                if 'pcomppscavcylME_set_1' in  SDS.keys():
                    pcomppscavcylME_set.append(SDS['pcomppscavcylME_set_{}'.format(i)])
                    #del SDS['pcomppscavcylME_set_{}'.format(i)]
                if 'HLcylME_set_1' in  SDS.keys():
                    HLcylME_set.append(SDS['HLcylME_set_{}'.format(i)])
                    #del SDS['HLcylME_set_{}'.format(i)]
                if 'LLcylME_set_1' in  SDS.keys():
                    LLcylME_set.append(SDS['LLcylME_set_{}'.format(i)])
                    #del SDS['LLcylME_set_{}'.format(i)]
                if 'pmaxcylME_set_1' in  SDS.keys():
                    pmaxcylME_set.append(SDS['pmaxcylME_set_{}'.format(i)])
                    #del SDS['pmaxcylME_set_{}'.format(i)]
                if 'RunInPISTcylME_1' in  SDS.keys():
                    RunInPISTcylME.append(SDS['RunInPISTcylME_{}'.format(i)])
                    #del SDS['RunInPISTcylME_{}'.format(i)]
                if 'SCOCcylME_avg_1' in  SDS.keys():
                    SCOCcylME_avg.append(SDS['SCOCcylME_avg_{}'.format(i)])
                    #del SDS['SCOCcylME_avg_{}'.format(i)]
                if 'SCOCcylME_set_1' in  SDS.keys():
                    SCOCcylME_set.append(SDS['SCOCcylME_set_{}'.format(i)])
                    #del SDS['SCOCcylME_set_{}'.format(i)]
                if 'HMIcylME_set_1' in  SDS.keys():
                    HMIcylME_set.append(SDS['HMIcylME_set_{}'.format(i)])
                    #del SDS['HMIcylME_set_{}'.format(i)]
                if 'ACCcylME_set_1' in  SDS.keys():
                    ACCcylME_set.append(SDS['ACCcylME_set_{}'.format(i)])
                    #del SDS['ACCcylME_set_{}'.format(i)]
            
            
            SDS['pindcylME_avg']=np.array(pindcylME_avg).T
            SDS['pmaxcylME_avg']=np.array(pmaxcylME_avg).T
            SDS['pcompcylME_avg']=np.array(pcompcylME_avg).T
            SDS['ThtcwoutcylME_avg']=np.array(ThtcwoutcylME_avg).T
            SDS['TLOPISToutcylME_avg']=np.array(TLOPISToutcylME_avg).T
            SDS['TegEVoutcylME_avg']=np.array(TegEVoutcylME_avg).T
            SDS['TLIWATcylME_avg']=np.array(TLIWATcylME_avg).T
            SDS['phiIgncylME_avg']=np.array(phiIgncylME_avg).T
            SDS['phiEVOcorcylME_set']=np.array(phiEVOcorcylME_set).T
            SDS['TLOMBRGcylME_avg']=np.array(TLOMBRGcylME_avg).T
            SDS['TLOCHBRGcylME_avg']=np.array(TLOCHBRGcylME_avg).T
            SDS['FPIcylME_avg']=np.array(FPIcylME_avg).T
            SDS['VITcylME_avg']=np.array(VITcylME_avg).T
            SDS['pcomppscavcylME_set']=np.array(pcomppscavcylME_set).T
            SDS['HLcylME_set']=np.array(HLcylME_set).T
            SDS['LLcylME_set']=np.array(LLcylME_set).T
            SDS['pmaxcylME_set']=np.array(pmaxcylME_set).T
            SDS['RunInPISTcylME']=np.array(RunInPISTcylME).T
            SDS['SCOCcylME_avg']=np.array(SCOCcylME_avg).T
            SDS['SCOCcylME_set']=np.array(SCOCcylME_set).T
            SDS['HMIcylME_set']=np.array(HMIcylME_set).T
            SDS['ACCcylME_set']=np.array(ACCcylME_set).T
            
            #Find average values across cylinders
            if 'pindcylME_avg' in  SDS.keys():
                SDS['pindME_avg']=np.mean(SDS['pindcylME_avg'],axis=1)
            if 'pmaxcylME_avg' in  SDS.keys():
                SDS['pmaxME_avg']=np.mean(SDS['pmaxcylME_avg'],axis=1)
            if 'pcompcylME_avg' in  SDS.keys():
                SDS['pcompME_avg']=np.mean(SDS['pcompcylME_avg'],axis=1)
            if 'ThtcwoutcylME_avg' in  SDS.keys():
                SDS['ThtcwoutME_avg']=np.mean(SDS['ThtcwoutcylME_avg'],axis=1)
            if 'TLOPISToutcylME_avg' in  SDS.keys():
                SDS['TLOPISToutME_avg']=np.mean(SDS['TLOPISToutcylME_avg'],axis=1)
            if 'TegEVoutcylME_avg' in  SDS.keys():
                SDS['TegEVoutME_avg']=np.mean(SDS['TegEVoutcylME_avg'],axis=1)
            if 'TLIWATcylME_avg' in  SDS.keys():
                SDS['TLIWATME_avg']=np.mean(SDS['TLIWATcylME_avg'],axis=1)
            if 'phiIgncylME_avg' in  SDS.keys():
                SDS['phiIgnME_avg']=np.mean(SDS['phiIgncylME_avg'],axis=1)
            if 'phiEVOcorcylME_set' in  SDS.keys():
                SDS['phiEVOcorME_set']=np.mean(SDS['phiEVOcorcylME_set'],axis=1)
            if 'TLOMBRGcylME_avg' in  SDS.keys():
                if SDS['TLOMBRGcylME_avg'].ndim>1:
                    SDS['TLOMBRGME_avg']=np.mean(SDS['TLOMBRGcylME_avg'],axis=1)
            if 'TLOCHBRGcylME_avg' in  SDS.keys():
                if SDS['TLOCHBRGcylME_avg'].ndim>1:
                    SDS['TLOCHBRGME_avg']=np.mean(SDS['TLOCHBRGcylME_avg'],axis=1)
            if 'FPIcylME_avg' in  SDS.keys():
                SDS['FPIME_avg']=np.mean(SDS['FPIcylME_avg'],axis=1)
            if 'VITcylME_avg' in  SDS.keys():
                SDS['VITME_avg']=np.mean(SDS['VITcylME_avg'],axis=1)
            if 'pcomppscavcylME_set' in  SDS.keys():
                SDS['pcomppscavME_set']=np.mean(SDS['pcomppscavcylME_set'],axis=1)
            if 'HLcylME_set' in  SDS.keys():
                SDS['HLME_set']=np.mean(SDS['HLcylME_set'],axis=1)
            if 'LLcylME_set' in  SDS.keys():
                SDS['LLME_set']=np.mean(SDS['LLcylME_set'],axis=1)
            if 'pmaxcylME_set' in  SDS.keys():
                SDS['pmaxME_set']=np.mean(SDS['pmaxcylME_set'],axis=1)
            if 'RunInPISTcylME' in  SDS.keys():
                SDS['RunInPISTME']=np.mean(SDS['RunInPISTcylME'],axis=1)
            if 'SCOCcylME_avg' in  SDS.keys():
                SDS['SCOCME_avg']=np.mean(SDS['SCOCcylME_avg'],axis=1)
            if 'SCOCcylME_set' in  SDS.keys():
                SDS['SCOCME_set']=np.mean(SDS['SCOCcylME_set'],axis=1)
            if 'HMIcylME_set' in  SDS.keys():
                SDS['HMIME_set']=np.mean(SDS['HMIcylME_set'],axis=1)
            if 'ACCcylME_set' in  SDS.keys():
                SDS['ACCME_set']=np.mean(SDS['ACCcylME_set'],axis=1)
            
            
            #Group variables that are "per TC"
            NTCTCME_avg=[]
            dpairAFTCME_avg=[]
            dpairACTCME_avg=[]
            TairTCinTCME_avg=[]
            TairACinTCME_avg=[]
            TairACoutTCME_avg=[]
            TltcwACinTCME_avg=[]
            TltcwACoutTCME_avg=[]
            TegTCinTCME_avg=[]
            TegTCoutTCME_avg=[]
            pegTCoutTCME_avg=[]
            TcwTCbcoutTCME_avg=[]
            TclTCinTCME_avg=[]
            TclTCoutTCME_avg=[]
            TLOTCinTCME_avg=[]
            TLOTCoutTCME_avg=[]
            TCCOTCME_avg=[]
            for i in range(1,int(RDS['nTCME'][Eidx-1])+1):
                if 'NTCTCME_avg_1' in  SDS.keys():
                    NTCTCME_avg.append(SDS['NTCTCME_avg_{}'.format(i)])
                    #del SDS['NTCTCME_avg_{}'.format(i)]
                if 'dpairAFTCME_avg_1' in  SDS.keys():
                    dpairAFTCME_avg.append(SDS['dpairAFTCME_avg_{}'.format(i)])
                    #del SDS['dpairAFTCME_avg_{}'.format(i)]
                if 'dpairACTCME_avg_1' in  SDS.keys():
                    dpairACTCME_avg.append(SDS['dpairACTCME_avg_{}'.format(i)])
                    #del SDS['dpairACTCME_avg_{}'.format(i)]
                if 'TairTCinTCME_avg_1' in  SDS.keys():
                    TairTCinTCME_avg.append(SDS['TairTCinTCME_avg_{}'.format(i)])
                    #del SDS['TairTCinTCME_avg_{}'.format(i)]
                if 'TairACinTCME_avg_1' in  SDS.keys():
                    TairACinTCME_avg.append(SDS['TairACinTCME_avg_{}'.format(i)])
                    #del SDS['TairACinTCME_avg_{}'.format(i)]
                if 'TairACoutTCME_avg_1' in  SDS.keys():
                    TairACoutTCME_avg.append(SDS['TairACoutTCME_avg_{}'.format(i)])
                    #del SDS['TairACoutTCME_avg_{}'.format(i)]
                if 'TltcwACinTCME_avg_1' in  SDS.keys():
                    TltcwACinTCME_avg.append(SDS['TltcwACinTCME_avg_{}'.format(i)])
                    #del SDS['TltcwACinTCME_avg_{}'.format(i)]
                if 'TltcwACoutTCME_avg_1' in  SDS.keys():
                    TltcwACoutTCME_avg.append(SDS['TltcwACoutTCME_avg_{}'.format(i)])
                    #del SDS['TltcwACoutTCME_avg_{}'.format(i)]
                if 'TegTCinTCME_avg_1' in  SDS.keys():
                    TegTCinTCME_avg.append(SDS['TegTCinTCME_avg_{}'.format(i)])
                    #del SDS['TegTCinTCME_avg_{}'.format(i)]
                if 'TegTCoutTCME_avg_1' in  SDS.keys():
                    TegTCoutTCME_avg.append(SDS['TegTCoutTCME_avg_{}'.format(i)])
                    #del SDS['TegTCoutTCME_avg_{}'.format(i)]
                if 'pegTCoutTCME_avg_1' in  SDS.keys():
                    pegTCoutTCME_avg.append(SDS['pegTCoutTCME_avg_{}'.format(i)])
                    #del SDS['pegTCoutTCME_avg_{}'.format(i)]
                if 'TcwTCbcoutTCME_avg_1' in  SDS.keys():
                    TcwTCbcoutTCME_avg.append(SDS['TcwTCbcoutTCME_avg_{}'.format(i)])
                    #del SDS['TcwTCbcoutTCME_avg_{}'.format(i)]
                if 'TclTCinTCME_avg_1' in  SDS.keys():
                    TclTCinTCME_avg.append(SDS['TclTCinTCME_avg_{}'.format(i)])
                    #del SDS['TclTCinTCME_avg_{}'.format(i)]
                if 'TclTCoutTCME_avg_1' in  SDS.keys():
                    TclTCoutTCME_avg.append(SDS['TclTCoutTCME_avg_{}'.format(i)])
                    #del SDS['TclTCoutTCME_avg_{}'.format(i)]
                if 'TLOTCinTCME_avg_1' in  SDS.keys():
                    TLOTCinTCME_avg.append(SDS['TLOTCinTCME_avg_{}'.format(i)])
                    #del SDS['TLOTCinTCME_avg_{}'.format(i)]
                if 'TLOTCoutTCME_avg_1' in  SDS.keys():
                    TLOTCoutTCME_avg.append(SDS['TLOTCoutTCME_avg_{}'.format(i)])
                    #del SDS['TLOTCoutTCME_avg_{}'.format(i)]
                if 'TCCOTCME_avg_1' in  SDS.keys():
                    TCCOTCME_avg.append(SDS['TCCOTCME_avg_{}'.format(i)])
                    #del SDS['TCCOTCME_avg_{}'.format(i)]
                    
                    
                    
                    
            SDS['NTCTCME_avg']=np.array(NTCTCME_avg).T
            SDS['dpairAFTCME_avg']=np.array(dpairAFTCME_avg).T
            SDS['dpairACTCME_avg']=np.array(dpairACTCME_avg).T
            SDS['TairTCinTCME_avg']=np.array(TairTCinTCME_avg).T
            SDS['TairACinTCME_avg']=np.array(TairACinTCME_avg).T
            SDS['TairACoutTCME_avg']=np.array(TairACoutTCME_avg).T
            SDS['TltcwACinTCME_avg']=np.array(TltcwACinTCME_avg).T
            SDS['TltcwACoutTCME_avg']=np.array(TltcwACoutTCME_avg).T
            SDS['TegTCinTCME_avg']=np.array(TegTCinTCME_avg).T
            SDS['TegTCoutTCME_avg']=np.array(TegTCoutTCME_avg).T
            SDS['pegTCoutTCME_avg']=np.array(pegTCoutTCME_avg).T
            SDS['TcwTCbcoutTCME_avg']=np.array(TcwTCbcoutTCME_avg).T
            SDS['TclTCinTCME_avg']=np.array(TclTCinTCME_avg).T
            SDS['TclTCoutTCME_avg']=np.array(TclTCoutTCME_avg).T
            SDS['TLOTCinTCME_avg']=np.array(TLOTCinTCME_avg).T
            SDS['TLOTCoutTCME_avg']=np.array(TLOTCoutTCME_avg).T
            SDS['TCCOTCME_avg']=np.array(TCCOTCME_avg).T
            
            #Find average values across TCs
            if 'NTCTCME_avg' in  SDS.keys():
                SDS['NTCME_avg']=np.mean((SDS['NTCTCME_avg']),axis=1)
            if 'dpairAFTCME_avg' in  SDS.keys():
                SDS['dpairAFME_avg']=np.mean(SDS['dpairAFTCME_avg'],axis=1)
            if 'dpairACTCME_avg' in  SDS.keys():
                SDS['dpairACME_avg']=np.mean(SDS['dpairACTCME_avg'],axis=1)
            if 'TairTCinTCME_avg' in  SDS.keys():
                SDS['TairTCinME_avg']=np.mean(SDS['TairTCinTCME_avg'],axis=1)
            if 'TairACinTCME_avg' in  SDS.keys():
                SDS['TairACinME_avg']=np.mean(SDS['TairACinTCME_avg'],axis=1)
            if 'TairACoutTCME_avg' in  SDS.keys():
                SDS['TairACoutME_avg']=np.mean(SDS['TairACoutTCME_avg'],axis=1)
            if 'TltcwACinTCME_avg' in  SDS.keys():
                SDS['TltcwACinME_avg']=np.mean(SDS['TltcwACinTCME_avg'],axis=1)
            if 'TltcwACoutTCME_avg' in  SDS.keys():
                SDS['TltcwACoutME_avg']=np.mean(SDS['TltcwACoutTCME_avg'],axis=1)
            if 'TegTCinTCME_avg' in  SDS.keys():
                SDS['TegTCinME_avg']=np.mean(SDS['TegTCinTCME_avg'],axis=1)
            if 'TegTCoutTCME_avg' in  SDS.keys():
                SDS['TegTCoutME_avg']=np.mean(SDS['TegTCoutTCME_avg'],axis=1)
            if 'pegTCoutTCME_avg' in  SDS.keys():
                SDS['pegTCoutME_avg']=np.mean(SDS['pegTCoutTCME_avg'],axis=1)
            if 'TcwTCbcoutTCME_avg' in  SDS.keys():
                SDS['TcwTCbcoutME_avg']=np.mean(SDS['TcwTCbcoutTCME_avg'],axis=1)
            if 'TclTCinTCME_avg' in  SDS.keys():
                SDS['TclTCinME_avg']=np.mean(SDS['TclTCinTCME_avg'],axis=1)
            if 'TclTCoutTCME_avg' in  SDS.keys():
                SDS['TclTCoutME_avg']=np.mean(SDS['TclTCoutTCME_avg'],axis=1)
            if 'TLOTCinTCME_avg' in  SDS.keys():
                SDS['TLOTCinME_avg']=np.mean(SDS['TLOTCinTCME_avg'],axis=1)
            if 'TLOTCoutTCME_avg' in  SDS.keys():
                SDS['TLOTCoutME_avg']=np.mean(SDS['TLOTCoutTCME_avg'],axis=1)
            if 'TCCOTCME_avg' in  SDS.keys():
                SDS['TCCOME_avg']=np.mean(SDS['TCCOTCME_avg'],axis=1)
                
        #AE log variables grouping
        if LogType =="AE":
            #Group variables that are "per cylinder"
            pindcylAE_avg=[]
            pmaxcylAE_avg=[]
            pcompcylAE_avg=[]
            ThtcwoutcylAE_avg=[]
            TegEVoutcylAE_avg=[]
            FPIcylAE_avg=[]
            for i in range(1,int(RDS['ncAE'][Eidx-1])+1):
                if 'pindcylAE_avg_1' in  SDS.keys():
                    pindcylAE_avg.append(SDS['pindcylAE_avg_{}'.format(i)])
                    #del SDS['pindcylAE_avg_{}'.format(i)]
                if 'pmaxcylAE_avg_1' in  SDS.keys():
                    pmaxcylAE_avg.append(SDS['pmaxcylAE_avg_{}'.format(i)])
                    #del SDS['pmaxcylAE_avg_{}'.format(i)]
                if 'pcompcylAE_avg_1' in  SDS.keys():
                    pcompcylAE_avg.append(SDS['pcompcylAE_avg_{}'.format(i)])
                    #del SDS['pcompcylAE_avg_{}'.format(i)]
                if 'ThtcwoutcylAE_avg_1' in  SDS.keys():
                    ThtcwoutcylAE_avg.append(SDS['ThtcwoutcylAE_avg_{}'.format(i)])
                    #del SDS['ThtcwoutcylAE_avg_{}'.format(i)]
                if 'TegEVoutcylAE_avg_1' in  SDS.keys():
                    TegEVoutcylAE_avg.append(SDS['TegEVoutcylAE_avg_{}'.format(i)])
                if 'FPIcylAE_avg_1' in  SDS.keys():
                    FPIcylAE_avg.append(SDS['FPIcylAE_avg_{}'.format(i)])
                    #del SDS['FPIcylAE_avg_{}'.format(i)]
                
            SDS['pindcylAE_avg']=np.array(pindcylAE_avg).T
            SDS['pmaxcylAE_avg']=np.array(pmaxcylAE_avg).T
            SDS['pcompcylAE_avg']=np.array(pcompcylAE_avg).T
            SDS['ThtcwoutcylAE_avg']=np.array(ThtcwoutcylAE_avg).T
            SDS['TegEVoutcylAE_avg']=np.array(TegEVoutcylAE_avg).T
            SDS['FPIcylAE_avg']=np.array(FPIcylAE_avg).T
            
            
            #Find average values across cylinders
            if 'pindcylAE_avg' in  SDS.keys():
                SDS['pindAE_avg']=np.mean(SDS['pindcylAE_avg'],axis=1)
            if 'pmaxcylAE_avg' in  SDS.keys():
                SDS['pmaxAE_avg']=np.mean(SDS['pmaxcylAE_avg'],axis=1)
            if 'pcompcylAE_avg' in  SDS.keys():
                SDS['pcompAE_avg']=np.mean(SDS['pcompcylAE_avg'],axis=1)
            if 'ThtcwoutcylAE_avg' in  SDS.keys():
                SDS['ThtcwoutAE_avg']=np.mean(SDS['ThtcwoutcylAE_avg'],axis=1)
            if 'TegEVoutcylAE_avg' in  SDS.keys():
                SDS['TegEVoutAE_avg']=np.mean(SDS['TegEVoutcylAE_avg'],axis=1)
            if 'FPIcylAE_avg' in  SDS.keys():
                SDS['FPIAE_avg']=np.mean(SDS['FPIcylAE_avg'],axis=1)
            
            
            
            
            #Group variables that are "per TC"
            NTCTCAE_avg=[]
            dpairAFTCAE_avg=[]
            dpairACTCAE_avg=[]
            TairTCinTCAE_avg=[]
            TairACoutTCAE_avg=[]
            pegTCoutTCAE_avg=[]
            TegTCinTCAE_avg=[]
            TegTCoutTCAE_avg=[]
            pSOTCinTCAE_avg=[]
            for i in range(1,int(RDS['nTCAE'][Eidx-1])+1):
                if 'NTCTCAE_avg_1' in  SDS.keys():
                    NTCTCAE_avg.append(SDS['NTCTCAE_avg_{}'.format(i)])
                    #del SDS['NTCTCAE_avg_{}'.format(i)]
                if 'dpairAFTCAE_avg_1' in  SDS.keys():
                    dpairAFTCAE_avg.append(SDS['dpairAFTCAE_avg_{}'.format(i)])
                    #del SDS['dpairAFTCAE_avg_{}'.format(i)]
                if 'dpairACTCAE_avg_1' in  SDS.keys():
                    dpairACTCAE_avg.append(SDS['dpairACTCAE_avg_{}'.format(i)])
                    #del SDS['dpairACTCAE_avg_{}'.format(i)]
                if 'TairTCinTCAE_avg_1' in  SDS.keys():
                    TairTCinTCAE_avg.append(SDS['TairTCinTCAE_avg_{}'.format(i)])
                    #del SDS['TairTCinTCAE_avg_{}'.format(i)]
                
                if 'TairACoutTCAE_avg_1' in  SDS.keys():
                    TairACoutTCAE_avg.append(SDS['TairACoutTCAE_avg_{}'.format(i)])
                    #del SDS['TairACoutTCAE_avg_{}'.format(i)]
                if 'pegTCoutTCAE_avg_1' in  SDS.keys():
                    pegTCoutTCAE_avg.append(SDS['pegTCoutTCAE_avg_{}'.format(i)])
                    #del SDS['pegTCoutTCAE_avg_{}'.format(i)]
                if 'TegTCinTCAE_avg_1' in  SDS.keys():
                    TegTCinTCAE_avg.append(SDS['TegTCinTCAE_avg_{}'.format(i)])
                    #del SDS['TegTCinTCAE_avg_{}'.format(i)]
                if 'TegTCoutTCAE_avg_1' in  SDS.keys():
                    TegTCoutTCAE_avg.append(SDS['TegTCoutTCAE_avg_{}'.format(i)])
                    #del SDS['TegTCoutTCAE_avg_{}'.format(i)]
                if 'pSOTCinTCAE_avg_1' in  SDS.keys():
                    pSOTCinTCAE_avg.append(SDS['pSOTCinTCAE_avg_{}'.format(i)])
                    #del SDS['pSOTCinTCAE_avg_{}'.format(i)]
            SDS['NTCTCAE_avg']=np.array(NTCTCAE_avg).T
            SDS['dpairAFTCAE_avg']=np.array(dpairAFTCAE_avg).T
            SDS['dpairACTCAE_avg']=np.array(dpairACTCAE_avg).T
            SDS['TairTCinTCAE_avg']=np.array(TairTCinTCAE_avg).T
            SDS['TairACoutTCAE_avg']=np.array(TairACoutTCAE_avg).T
            SDS['pegTCoutTCAE_avg']=np.array(pegTCoutTCAE_avg).T
            SDS['TegTCinTCAE_avg']=np.array(TegTCinTCAE_avg).T
            SDS['TegTCoutTCAE_avg']=np.array(TegTCoutTCAE_avg).T
            SDS['pSOTCinTCAE_avg']=np.array(pSOTCinTCAE_avg).T
            #Find average values across TCs
            if 'NTCTCAE_avg' in  SDS.keys():
                SDS['NTCAE_avg']=np.mean((SDS['NTCTCAE_avg']),axis=1)
            if 'dpairAFTCAE_avg' in  SDS.keys():
                SDS['dpairAFAE_avg']=np.mean(SDS['dpairAFTCAE_avg'],axis=1)
            if 'dpairACTCAE_avg' in  SDS.keys():
                SDS['dpairACAE_avg']=np.mean(SDS['dpairACTCAE_avg'],axis=1)
            if 'TairTCinTCAE_avg' in  SDS.keys():
                SDS['TairTCinAE_avg']=np.mean(SDS['TairTCinTCAE_avg'],axis=1)
            if 'TairACoutTCAE_avg' in  SDS.keys():
                SDS['TairACoutAE_avg']=np.mean(SDS['TairACoutTCAE_avg'],axis=1)
            if 'pegTCoutTCAE_avg' in  SDS.keys():
                SDS['pegTCoutAE_avg']=np.mean(SDS['pegTCoutTCAE_avg'],axis=1)
            if 'TegTCinTCAE_avg' in  SDS.keys():
                SDS['TegTCinAE_avg']=np.mean(SDS['TegTCinTCAE_avg'],axis=1)
            if 'TegTCoutTCAE_avg' in  SDS.keys():
                SDS['TegTCoutAE_avg']=np.mean(SDS['TegTCoutTCAE_avg'],axis=1)
            if 'pSOTCinTCAE_avg' in  SDS.keys():
                SDS['pSOTCinAE_avg']=np.mean(SDS['pSOTCinTCAE_avg'],axis=1)
            
            
            
                
            
        
                    
                
                
                    
                    
        # SRQSTD['TotalTimeLapse'].append(time.time()-start_time)
        # SRQSTD['Remarks'].append("complete")        
        # return(SDS,DS,SRQSTD)  
    except Exception as err:
        exp="Error in GroupServiceData {} in line No. {} for IMO {} and LogID {}  ".format(err,sys.exc_info()[-1].tb_lineno,IMO,SDS['LogID'])
        logger.error(exp,exc_info=True)
        ExceptionHandler(LogType,exp)
        SRQSTD['TotalTimeLapse']=time.time()-start_time              
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return({},{},SRQST)
        
    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time               
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(SDS,DS,SRQST)             
            
            
            
        
        

            
        
        
                
                
                
        
        
         
     
                                                       

