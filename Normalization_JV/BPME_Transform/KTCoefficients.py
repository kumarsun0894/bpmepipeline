# -*- coding: utf-8 -*-

"""
Created on Mon Apr 13 12:04:41 2020

@author: akhil.surendran

% KTCoefficients
% Description:  This fumction returns the polynomial coefficients to be
%               used in the KT calculation as defiend in (Oosterveld & van
%               Oossanen, 1975).
%
% Input:        -
%
% Output:       polynomial coeffcicients Ckt, skt, tkt, ukt, vkt for KT
%               calculation
%
% Referemces:   - Oosterveld, M. W. C., & van Oossanen, P. (1975). Further
%                 computer-analyzed data of the Wageningen B-screw series.
%                 International shipbuilding progress, 22(251), 251-262.
%
% See also:     KQCoefficients, BSeries, BSeriesCor 

"""

import numpy as np
def KTCoefficients():
    K_T=np.array([[0.00880496,0,0,0,0]
        ,[-0.204554,1,0,0,0]
        ,[0.166351,0,1,0,0]
        ,[0.158114,0,2,0,0]
        ,[-0.147581,2,0,1,0]
		,[-0.481497,1,1,1,0]
		,[0.415437,0,2,1,0]
		,[0.0144043,0,0,0,1]
		,[-0.0530054,2,0,0,1]
		,[0.0143481,0,1,0,1]
		,[0.0606826,1,1,0,1]
		,[-0.0125894,0,0,1,1]
		,[0.0109689,1,0,1,1]
		,[-0.133698,0,3,0,0]
		,[0.00638407,0,6,0,0]
		,[-0.00132718,2,6,0,0]
		,[0.168496,3,0,1,0]
		,[-0.0507214,0,0,2,0]
		,[0.0854559,2,0,2,0]
		,[-0.0504475,3,0,2,0]
		,[0.010465,1,6,2,0]
		,[-0.00648272,2,6,2,0]
		,[-0.00841728,0,3,0,1]
		,[0.0168424,1,3,0,1]
		,[-0.00102296,3,3,0,1]
		,[-0.0317791,0,3,1,1]
		,[0.018604,1,0,2,1]
		,[-0.00410798,0,2,2,1]
		,[-0.000606848,0,0,0,2]
		,[-0.0049819,1,0,0,2]
		,[0.0025983,2,0,0,2]
		,[-0.000560528,3,0,0,2]
		,[-0.00163652,1,2,0,2]
		,[-0.000328787,1,6,0,2]
		,[0.000116502,2,6,0,2]
		,[0.000690904 ,0,0,1,2]
		,[0.00421749,0,3,1,2]
		,[0.0000565229,3,6,1,2]
		,[-0.00146564,0,3,2,2,]])
    Ckt=K_T[:,0].T.reshape(1,-1)
    skt=K_T[:,1].T.reshape(1,-1)
    tkt=K_T[:,2].T.reshape(1,-1)
    ukt=K_T[:,3].T.reshape(1,-1)
    vkt=K_T[:,4].T.reshape(1,-1)
    
    return (Ckt,skt,tkt,ukt,vkt)
# a,b,c,d,e=KTCoefficients()


   