# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 13:39:31 2020

@author: Subhin Antony

% STNormalization
% Description:  This function applies the STNormalizationSingleEngine
%               function for every engine.
%
% Input:        ST          [-]     raw shop test variable (matrix)
%               MCRST       [%]     raw shop test engine loads (matrix)
%
% Output:       STnorm      [-]     normalized shot test variable (matrix)
%               MCRSTnorm   [%]     normalized shop test engine loads (matrix)
%
% Note:         MCRST must be monotonically increasing and the ST values
%               should correspond to the MCRST order.
%
% See also:     STNormalizationSingleEngine, ReferenceCurves



"""
from BPME_Common import LastElement as le
import numpy as np
from BPME_Transform import STNormalizationSingleEngine as sg


def STNormalization(ST,MCRST):
    if ST.ndim==1:
        ST=ST.reshape(1,-1)
    if MCRST.ndim==1:
        MCRST=MCRST.reshape(1,-1)
    
    [nST,temp]=np.shape(ST)
    
    #Memory pre-allocation
    STnorm=np.empty(np.shape(MCRST))
    STnorm[:]=np.nan
    MCRSTnorm=np.empty(np.shape(MCRST))
    MCRSTnorm[:]=np.nan
    for i in range(0,nST):
        [STtemp,MCRSTtemp]=sg.STNormalizationSingleEngine(ST[i,:],MCRST[i,:])
        STnorm[i,0:len(STtemp)]=STtemp
        MCRSTnorm[i,0:len(MCRSTtemp)]=MCRSTtemp
        
    #Remove unnecessary NaNs
    MaxLast=int(np.max(le.LastElement(MCRSTnorm)))
    STnorm=STnorm[:,0:MaxLast+1]
    MCRSTnorm=MCRSTnorm[:,0:MaxLast+1]
    return(STnorm,MCRSTnorm)
        

