# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 14:17:48 2020

@author: Subhin Antony

% BSeries
% Description:  This fumction implements the popular Wagenigen B-Series
%               method as pulished in (Oosterveld & van Oossanen, 1975)
%
% Input:        J       [-]     advance coefficient (vector)
%               P_D     [-]     pitch ratio (scalar)
%               AeAo    [-]     expanded blade area ratio (scalar)
%               z       [-]     number of propeller blades (scalar)
%
% Output:       Kt      [-]     thrust coefficient (vector)
%               Kq      [-]     torque coefficient (vector)
%               etao    [-]     open-water propeller efficiency (vector)
%
% Referemces:   - Oosterveld, M. W. C., & van Oossanen, P. (1975). Further
%                 computer-analyzed data of the Wageningen B-screw series.
%                 International shipbuilding progress, 22(251), 251-262.
%
% See also:     BSeriesCor, KTCoefficients, KQCoefficients

"""

#This fumction implements the popular Wagenigen B-Series method as pulished in (Oosterveld & van Oossanen, 1975)
import numpy as np
from BPME_Transform import KTCoefficients as KTC
from BPME_Transform import KQCoefficients as KQC

def BSeries(J,P_D,AeAo,z):
    J=J.reshape(1,-1).T
    #Get Kt and Kq coefficients
    [Ckt,skt,tkt,ukt,vkt]=KTC.KTCoefficients()
    [Ckq,skq,tkq,ukq,vkq]=KQC.KQCoefficients()
    
    #Calculate Kt and Kq using the B-Series polynomials
    Kt=np.sum(Ckt*J**skt*P_D**tkt*AeAo**ukt*z**vkt,axis=1)
    Kq=np.sum(Ckq*J**skq*P_D**tkq*AeAo**ukq*z**vkq,axis=1)
    
    #Calculate open-water propeller efficiency
    etao=J/(2*np.pi)*Kt/Kq
    
    #Filter out of range values
    Kt[Kt<0]=np.nan
    Kq[Kq<0]=np.nan
    etao[np.logical_or(etao<0 ,etao>1)]=np.nan
    
    return (Kt,Kq,etao)