# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 12:04:41 2020

@author: akhil.surendran
"""
import numpy as np
def LCVEstimator(FOG):
    #Initialization
    LCV=np.empty(FOG.size)
    LCV[:] = np.nan

    #LCV estimation
    #RMA-1 RMB-2 RMD-3 RME-4 RMG-5 RMK-6 DMA-7 DMB-8 DMX-9 DMZ-10 LNG-11 LPG PROPANE-12 LPG BUTANE-13
    
    LCV[(FOG>=1) & (FOG<=3)]=41.2
    LCV[(FOG>=4) & (FOG<=6)]=40.2
    LCV[(FOG>=7) & (FOG<=10)]=42.7
    LCV[FOG==11]=48
    LCV[FOG==12]=46.3
    LCV[FOG==13]=45.7
    
    return LCV
      
