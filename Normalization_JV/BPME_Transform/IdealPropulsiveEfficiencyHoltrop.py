# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 12:04:41 2020

@author: akhil.surendran
"""
import numpy as np
def IdealPropulsiveEfficiencyHoltrop(U,ReferenceDataStruct):
    if np.isnan(np.all(ReferenceDataStruct['HullProfile'])):
        print("Holtrop data are not entered, thus etaDHid cannot be estimated!")
        etaDHid=np.empty(np.shape(U))
        etaDHid[:] = np.nan
        return
    etaDHid=0.7*np.ones(np.shape(U))
    return etaDHid

