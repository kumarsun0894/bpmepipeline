# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 17:32:00 2020

@author: Subhin Antony
"""
import numpy as np
def EffectivePower(PMEST,etaeff,PowerTypeMEST,nMEST,NoME):
    if PMEST.ndim==1:
        PMES=PMEST.reshape(1,-1)
    else: PMES=PMEST
    eta = np.empty((1,int(nMEST)))
    eta[:] = np.nan
    PeffMEST =  np.empty((np.shape(PMEST)))
    PeffMEST[:] = np.nan
    for i in range(0,int(nMEST)):
        NM=int(NoME[i]-1)
        eta[0,i]=etaeff[NM]
        
    
    #Check if ME shop test is given in terms of Effective, or Brake power and re-define it as Effective
    for i in range(0,int(nMEST)):
        if PowerTypeMEST[i]=='B':
            PeffMEST[i:]=PMES[i:]/eta[0,i] #in kW
            
        elif PowerTypeMEST[i]=='E':
            PeffMEST[i:]=PMES[i:]
            
        else:
            print('Please define the PowerType for shop test No'+str(i)+ 'and run again!')
            PeffMEST[i:]=np.empty(np.size(PMES[i]))
            PeffMEST[i:]=np.nan
    if PMEST.ndim==1:
        return(PeffMEST.ravel())
    else:
        return(PeffMEST)
        


#PMEST=np.array([[2825,5649,8476,10173,11298],[2825,5649,8476,10173,11298]])
#etaeff=np.array([0.990])
#PowerTypeMEST=np.array([["E"],["B"]])
#nMEST=2
#NoME=np.array([[1],[1]])

# PMEST=np.array([[6170,12340,18510,22212,24680,24680,27148],[6170,12340,18510,22212,24680,24680,27148]])
# etaeff = np.array([0.99])
# PowerTypeMEST=np.array(["E","B"])
# nMEST=2;
# NoME=np.array([1,1])


#PMEST=np.array([2825,5649,8476,10173,11298])
#
#etaeff=np.array([0.990])
#PowerTypeMEST=np.array(["B"])
#nMEST=1
#NoME=np.array([1])
       
# a=EffectivePower(PMEST,etaeff,PowerTypeMEST,nMEST,NoME)
#print(a)              
# if PMEST.ndim==1:
#         PMES=PMEST.reshape(1,-1)
# else: PMES=PMEST
# eta = np.empty((1,int(nMEST)))
# eta[:] = np.nan
# PeffMEST = np.empty((np.shape(PMES)))
# PeffMEST[:] = np.nan
# for i in range(0,int(nMEST)):
#         NM=int(NoME[i]-1)
#         eta[0,i]=etaeff[NM]
# for i in range(0,int(nMEST)):
#         if PowerTypeMEST[i]=='B':
#             PeffMEST[i,:]=PMES[i,:]/eta[0,i] #in kW
            
#         elif PowerTypeMEST[i]=='E':
#             PeffMEST[i,:]=PMES[i,:]
            
#         else:
#             print('Please define the PowerType for shop test No'+str(i)+ 'and run again!')
#             PeffMEST[i,:]=np.empty(np.size(PMES[i]))
#             PeffMEST[i,:]=np.nan
        
        
        
        