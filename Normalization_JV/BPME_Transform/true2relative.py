# -*- coding: utf-8 -*-

"""
Created on Sun Apr 12 10:37:30 2020

@author: Subhin Antony

% true2relative
% Description:  This function transfroms the true direction to relative.
%               This is applied for wave directions whenever the true is
%               provided rather than the relative one.
%
% Input:        head    [deg]   ship heading (vector)
%               psiT    [deg]   true direction (vector)
%
% Output:       psiR    [deg]   relative direction (vector)
%
% Note:         This function is not used to transform wind direction
%               because there, the ship's speed is also an important
%               factor. For wind direction transformation, the ISO15016 and
%               ISO 19030 approaches are used.
%
% See also:     ManipulateServiceData


"""

def true2relative(head,psiT):
    psiR=psiT-head
    if psiR.ndim!=1:
        psiR=psiR.ravel()
    

    for  i in range(0,len(psiT)):
      if psiR[i]<-180:
         psiR[i]=psiR[i]+360
      elif psiR[i]>180:
         psiR[i]=psiR[i]-360
    
    return psiR

