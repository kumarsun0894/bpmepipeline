# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 17:22:46 2020

@author: Subhin Antony
"""

def ClylinderConstants(Ck2,dPISTME,sPISTME,ModelME):
    #Estimate mean effective pressure based on MAN formula
    k2=Ck2*dPISTME**2*sPISTME
    k1=1
    return(k1,k2)