# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import sys
from ExceptionHandler import ExceptionHandler 
from BPME_Transform import CYLOBNEstimator as CBE
from BPME_Transform import CYLOrhoEstimator as CRE
from BPME_Common import nansumwrapper as nsw
import time 
import logging 
# create logger
logger = logging.getLogger('BPMEngine.2.02')

#Description:  This function treats multiple CYLO grades used within a noon log.
def ManipulateCYLOData(IMO,SDS,DS,LogType,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        if LogType in 'Noon':
            if np.any(SDS['nCOGME']>0):
                 
                #In case of multiple CYLO grades used within a log duration (change over),the data must be checked
                #Check if NaNs are properly distributed in ME CYLO properties arrays
                for k in range(0,len(SDS['nCOGME'])):
                    if SDS['nCOGME'][k]<np.max(SDS['nCOGME']):
                        for l in range(SDS['nCOGME'][k]+1,np.max(SDS['nCOGME'])):
                              #COG check
                            if ~pd.isnull(SDS['COGgME'][k,l]):
                                SDS['COGgME'][k,l]=np.nan
                                print('COG nan error!')
                              
                              #V check
                            if ~pd.isnull(SDS['VCOCgME'][k,l]):
                                SDS['VCOCgME'][k,l]=np.nan
                                print('VCOC nan error!')
                            
                              #rho check
                            if ~pd.isnull(SDS['rhoCOgME'][k,l]):
                                SDS['rhoCOgME'][k,l]=np.nan
                                print('rho nan error!')
                                 
                              #BN check
                            if ~pd.isnull(SDS['BNCOgME'][k,l]):
                                SDS['BNCOgME'][k,l]=np.nan
                                print('BNCO nan error!')
                
                          
                for k in range(0,len(SDS['nCOGME'])):
                    for l in range(0,SDS['nCOGME'][k]):
                        #COG estimation
                        if pd.isnull(SDS['COGgME'][k,l]):
                            #COGgME(k,l)=NaN;must define estimation rule
                            print('COG unavailable!')
                    
                        #V estimation
                        if pd.isnull(SDS['VCOCgME'][k,l]):
                            #VCOCgME(k,l)=NaN;must define estimation rule
                            print('VCOC unavailable!')
                        #rho estimation
                        if pd.isnull(SDS['rhoCOgME'][k,l]) and (pd.isnull(SDS['COGgME'][k,l])==False):
                            #SDS['rhoCOgME'][k,l]=CRE.CYLOrhoEstimator(SDS['COGgME'][k,l])#kg/m3
                            SDS['rhoCOgME'][k,l]=float('Nan')
                        #BN estimation
                        if pd.isnull(SDS['BNCOgME'][k,l]) and (pd.isnull(SDS['COGgME'][k,l])==False):
                            #SDS['BNCOgME'][k,l]=CBE.CYLOBNEstimator(SDS['COGgME'][k,l])
                            SDS['BNCOgME'][k,l]=float('Nan')
                    
                   
                #Change of unit [L]->[m3]
                SDS['VCOCgME']=SDS['VCOCgME']/1000
    
                #Calculate CYLO mass consumption
                SDS['mCOCgME']=SDS['VCOCgME']*SDS['rhoCOgME'] 
                      
             #Calculate the ME CYLO properties weighted average for the logs where nCOG>1
            if np.max(SDS['nCOGME'])>=1:
                SDS['mCOCME']=nsw.nansumwrapper(SDS['mCOCgME'],axis=1)
                SDS['VCOCME']=nsw.nansumwrapper(SDS['VCOCgME'],axis=1)
                #SDS['rhoCOME']=nsw.nansumwrapper(SDS['mCOCgME']*SDS['rhoCOgME'],axis=1)/nsw.nansumwrapper(SDS['mCOCgME'],axis=1)
                SDS['rhoCOME']=np.divide(nsw.nansumwrapper(SDS['mCOCgME']*SDS['rhoCOgME'],axis=1), nsw.nansumwrapper(SDS['mCOCgME'],axis=1), out=np.full_like(nsw.nansumwrapper(SDS['mCOCgME']*SDS['rhoCOgME'],axis=1),float('nan')), where=nsw.nansumwrapper(SDS['mCOCgME'],axis=1)!=0)
                #SDS['BNCOME']=nsw.nansumwrapper(SDS['mCOCgME']*SDS['BNCOgME'],axis=1)/nsw.nansumwrapper(SDS['mCOCgME'],axis=1)  
                SDS['BNCOME']=np.divide(nsw.nansumwrapper(SDS['mCOCgME']*SDS['BNCOgME'],axis=1), nsw.nansumwrapper(SDS['mCOCgME'],axis=1), out=np.full_like(nsw.nansumwrapper(SDS['mCOCgME']*SDS['BNCOgME'],axis=1),float('nan')), where=nsw.nansumwrapper(SDS['mCOCgME'],axis=1)!=0)
                        
             #This is to cover noon at port only logs. In that case there is not any COCME info.
            else:
                SDS['mCOCME']=np.empty(np.shape(SDS['nCOGME']))
                SDS['mCOCME'][:]=np.nan
                 
                SDS['VCOCME']=np.empty(np.shape(SDS['nCOGME']))
                SDS['VCOCME'][:] = np.nan
                 
                SDS['BNCOME']=np.empty(np.shape(SDS['nCOGME']))
                SDS['BNCOME'][:] =np.nan
                 
                SDS['rhoCOME']=np.empty(np.shape(SDS['nCOGME']))
                SDS['rhoCOME'][:] = np.nan
                   
            if np.any(SDS['nCOGME']>0):
                 
                #Remove per change-over info for ME
                del(SDS['mCOCgME'])
                del(SDS['VCOCgME'])
                del(SDS['rhoCOgME'])
                del(SDS['BNCOgME'])
             
             #Indicate logs with missing COC
            DS['MissingCOCME']=np.zeros(np.shape(SDS['mCOCME']))
            COCMEcheck=np.argwhere(pd.isnull(SDS['mCOCME']))
            if np.any(COCMEcheck):
                DS['MissingCOCME'][COCMEcheck]=1
                print('COC is missing!')
    except Exception as err:
        exp = "Error in ManipulateCYCLOData {} in line No. {} for IMO {}".format(err,sys.exc_info()[-1].tb_lineno,IMO)
        logger.error(exp,exc_info=True)
        ExceptionHandler(LogType,exp)
        SRQSTD['TotalTimeLapse']=time.time()-start_time              
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return({},{},SRQST)
    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time               
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(SDS,DS,SRQST) 
    
             
    