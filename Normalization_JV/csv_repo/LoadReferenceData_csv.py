# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 08:49:58 2020

@author: Subhin Antony
"""

# import os
# os.chdir('C:\\Users\\SubhinAntony\\Downloads\\Normalization_JV')
import numpy as np 
def LoadReferenceData(vobid):
    path = 'Ref_{}.m'.format(vobid)
    dct = {}
    num_int = ['IMONoVessel']
    num=['IMONoVessel','Loa','Lbp','B','D','Td','Tb','Ts','Ud',\
     'CapacityCargoVessel','DWT','nCHVessel','nCGVessel','Ds','Vs','Dd','Vd',\
         'Ad','Zad','nISBRG','etaS','etaB','rhoswh','TrialIndexMT','nProp','dProp',\
             'pitchProp','zProp','AeAoProp','TaConSTrial','TfConSTrial','VConSTrial',\
                 'TaSTrial','TfSTrial','VSTrial','nME','nMEST','nAE','nAEST','nBL','nFM',\
                     'nSPM','nSThruster','g','Zaid','kyy','ksiP','ksiN','ksiU','LCVMDOref',\
                         'LCVHFOref','Ck2','CorAllowance','MCRidAE','rhoswSTrial','RHmarginAE','SCOCminMANelME']
    stg=['NameVessel','NoonVerModel','HPVerModel','MEVerModel',\
              'HPLogConfig','MELogConfig','AELogConfig','NoonLogConfig','HullNoVessel',\
              'BuildYardVessel','YearBuildVessel','UltimateOwnerVessel','TypeVessel',\
              'TypeSTAWINDVessel','SuperStructureSTAWINDVessel','TypeCGVessel',\
              'BulbousBowVessel','Cstern','TransomSternVessel','AutoLoggingVessel',\
              'vesselsensd','vesselwp','DrawingURLVessel','PowerTypeMT','TypeProp',\
              'HullNoSTrial','PowerTypeSTrial','WeatherCorSTrial','SPM','SThruster',\
              'GCU','SG','RG','STurbine','GTurbine','PMotor','AEVerModel']
    str_list=['MakerME','BuilderME','ModelME','SerNoME','CategoryME','TypeME',\
                  'PistConfME','ModeME','DualFuelME','VEGBPME','LIWATME','IARME','MBTME',\
                  'CBTME','AVME','TVME','TCCOSME','MakerTCME','ModelTCME','TypeGovME',\
                  'ModelGovME','TypeLBME','MakerLBME','ModelLBME','CoolantJKTME',\
                  'LibcodeME','HullNoMEST','SerNoMEST','FuelModeMEST','PowerTypeMEST',\
                  'ModeMEST','MakerAE','PistConfAE','ModelAE','DualFuelAE','ECNTRAE',\
                  'LibcodeAE','HullNoAEST','FuelModeAEST','MakerBL','ModelBL','FluidFM',\
                  'EquipmentFM','LocationFM','TypeFM','MakerFM','ModelFM','InterfaceFM',\
                  'ECNTRSPM','LocationSThruster','LoadTypeAEST','RegLBME']
    arr=['BowSection','HullProfile','RudderProfile','TransomSection','Th',\
             'Dh','LCBh','TaMT','TfMT','VMT','U0MT','P0MT','N0MT','etaD0MT',\
             'UCorSTrial','PCorSTrial','NCorSTrial','etaeffME','ncME','dPISTME',\
             'sPISTME','PMCRME','PNCRME','NMCRME','NNCRME','nTCME','NmaxTCME',\
             'TmaxTCME','muTCME','dTCcompME','SCOCidME','NoME','LCVFOMEST','rhoFOMEST',\
             'MCRMEST','NMEST','PMEST','NTCMEST','FPIMEST','TFOPPinMEST','FOCMEST',\
             'SFOCMEST','SFOCisoMEST','TairTCinMEST','pambairMEST','TscavMEST',\
             'pscavMEST','pindMEST','peffMEST','pmaxMEST','pcompMEST','TegEVoutMEST',\
             'pegRMEST','TegTCinMEST','pegTCinMEST','TegTCoutMEST','pegTCoutMEST',\
             'TcwACinMEST','TcwACoutMEST','TairACinMEST','TairACoutMEST','dpairACMEST',\
             'dpairAFMEST','AuxBlowMEST','MCRNOxMEST','NOxisoMEST','ncAE','dPISTAE',\
             'sPISTAE','PengnomAE','PgennomAE','NnomAE','PFAE','nTCAE','NoAE',\
             'LCVFOAEST','rhoFOAEST','MCRAEST','NAEST','PengAEST','PgenAEST','NTCAEST',\
             'FPIAEST','FOCAEST','SFOCAEST','SFOCisoAEST','TairTCinAEST','pambairAEST',\
             'pmaxAEST','TegEVoutAEST','TegTCinAEST','TegTCoutAEST','TscavAEST',\
             'SteamCapacityBL','FaccuracyFM','PnomSThruster','F1iso','F2iso','Kiso',\
             'TCNomogram','UtrialMT', 'PtrialMT', 'NtrialMT', 'LCVGOMEST', 'TGOinMEST', \
             'pGOinMEST', 'GOCMEST', 'SGOCMEST', 'SGOCisoMEST', 'LCVGOAEST', 'TFOPPinAEST', \
             'TGOinAEST', 'pGOinAEST', 'GOCAEST', 'SGOCAEST', 'SGOCisoAEST', 'TcwACinAEST', \
             'TairACinAEST', 'pscavAEST','Cwph','Cmh','WSAh','dtunSThruster','etagenAEST',\
             'PgennomAAE','PFnomAE','SCOCbME','SCOCminME','SCOCmaxME','BNLBME','ACCLBME',\
             'TmCP','UCP','FOCMECP','FwiCP','SSNCP','LCVFOMECP','pcompAEST','TcwACoutAEST','dpairACAEST','dpairAFAEST']
             
    
    with open('input/'+path,'r') as f:
        for line in f:
            ar = line.rstrip(';\n').split('=')
            if ar[0] in stg:
                dct[ar[0]]=ar[1].rstrip('"').lstrip('"').replace('[','').replace(']','')
            elif ar[0] in num:
                dct[ar[0]]=float(ar[1].replace('[','').replace(']',''))
             
            elif ar[0] in str_list:
                if ar[1]=='[]':
                    dct[ar[0]]=np.array([])
                else:
                    dct[ar[0]]=np.array([i.rstrip('"').lstrip('"') for i in ar[1].replace('[','').replace(']','').split(",")])
            elif ar[0] in arr:
                ar_tmp=[i for i in ar[1].replace('[','').replace(']','').replace(';',',;,').split(",")]
                index=[i for i ,X in enumerate(ar_tmp) if X==';']
                arrr=[ float(i) for i in ar_tmp if i !=';' ]
                if len(index)>=1:
                    dct[ar[0]]=np.reshape(arrr,(int(len(index)+int(1)),index[0]))
                elif len(index)==0:
                    dct[ar[0]]=np.array(arrr)
    str = open('input/'+path,'r').read()               
    
    return(dct,str)    