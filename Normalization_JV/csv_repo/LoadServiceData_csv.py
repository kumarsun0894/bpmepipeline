# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 16:29:52 2020

@author: Subhin Antony
"""
import numpy as np
import  pandas as pd
pd.set_option('display.max_rows', 2000)
# import os
# os.chdir('C:\\Users\\SubhinAntony\\Downloads\\Normalization_JV')
def LoadServiceData(VesselObjectID,EngineIndex,LogType):
    inputfile='input/{}{}Log_{}.csv'.format(LogType,EngineIndex,VesselObjectID)
    #,nrows=30
    inputdata=pd.read_csv(inputfile,nrows=30)
    #inputdata['tstart']=pd.to_datetime(inputdata['DateStart_UTC'] + ' ' + inputdata['TimeStart_UTC'],format="%d/%m/%Y %H:%M:%S")#
    #inputdata['tend']=pd.to_datetime(inputdata['DateEnd_UTC'] + ' ' + inputdata['TimeEnd_UTC'],format="%d/%m/%Y %H:%M:%S")#
    inputdata['tstart']=pd.to_datetime(inputdata['DateStart_UTC']).dt.strftime('%d/%m/%Y')+ ' ' + inputdata['TimeStart_UTC']
    inputdata['tend']=pd.to_datetime(inputdata['DateEnd_UTC']).dt.strftime('%d/%m/%Y')+ ' ' + inputdata['TimeEnd_UTC']
    inputdata['tstart']=pd.to_datetime(inputdata['tstart'])
    inputdata['tend'] = pd.to_datetime(inputdata['tend'])
    # inputdata['tstart']=pd.to_datetime(inputdata['DateStart_UTC'] + ' ' + inputdata['TimeStart_UTC'],format="%d/%m/%Y %H:%M:%S")
    # inputdata['tend']=pd.to_datetime(inputdata['DateEnd_UTC'] + ' ' + inputdata['TimeEnd_UTC'],format="%d/%m/%Y %H:%M:%S")
    inputdata['Dt']=(inputdata.tend-inputdata.tstart).astype('timedelta64[s]')
    inputdata['Dt']=inputdata.Dt/3600
    inputdata['LogID']=inputdata['LogID'].astype('str')
    inputdata['LogID']=inputdata['LogID'].str.rstrip('_')
    # inputdata['LogID']=inputdata['LogID'].str.rstrip('_')
    # inputdata['DateStart_UTC']=pd.to_datetime(inputdata.DateStart_UTC)
    # inputdata['DateEnd_UTC']=pd.to_datetime(inputdata.DateEnd_UTC)
    # print(inputdata.DateStart_UTC.dt.hour-inputdata.DateEnd_UTC.dt.hour)
    # print(inputdata.dtypes)
    # coll=[]
    # for col in inputdata.columns:
    #     if inputdata[col].dtype == np.float64:
    #         coll.append(col)
        
    # inputdata=inputdata.T
    # ServiceDataStruct = dict(zip(inputdata.index, inputdata.values))
    # for ky in coll:
    #     ServiceDataStruct[ky]=ServiceDataStruct[ky].astype('float64')
    # ServiceDataStruct['LogID']=ServiceDataStruct['LogID'].astype('int64')
        
   
    return(inputdata)


# VesselObjectID=683546
# EngineIndex=''
# LogType='Noon'
# BPMEngineNoonVersion=1.22
# a=LoadServiceData(VesselObjectID,EngineIndex,LogType)
