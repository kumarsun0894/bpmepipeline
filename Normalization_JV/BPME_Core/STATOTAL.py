# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 17:20:34 2020

@author: SunKumar
@Delta:SunKumar

% STATOTAL
% Description:  This function implements all methods described in ISO19030
%               and ISO15016:2015 related to weather corrections.
%
% Input:        SDS (structure)
%               RDS (structure)
%               DS (structure)
%
% Output:       SDS (structure)
%               DS (structure)
%
% Note:         For more information, see:
%               "Weather Correction Methods v9.docx" 
%
% See also:     BPMEngineSingleVessel, SHALLOW, TEMP, DENS, STAWIND,
%               STAWAVE, ADMIRALTY, LOAD, PROPRPM

"""
import sys
import numpy as np
import pandas as pd
from ExceptionHandler import ExceptionHandler 
from BPME_Core import SHALLOW as S
from BPME_Core import TEMP as T
from BPME_Core import DENS as D
from BPME_Core import STAWIND as STwin
from BPME_Core import ADMIRALTY as A
from BPME_Core import LOAD as L
from BPME_Core import PROPRPM as PR
from BPME_Core import STAWAVE as STwav
import time
import logging 
# create logger
logger = logging.getLogger('BPMEngine.2.02')
def STATOTAL(IMO,LogType,SDS,RDS,DS,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        if "LogValidity" not in SDS.keys():
            SDS['LogValidity']=np.zeros(SDS['Tm'].shape)
            
        #Speed decrease due to shallow water in kn
        SDS['DU']=S.SHALLOW(SDS['STW'],
                            SDS['Tm'],
                            SDS['Amid'],
                            SDS['hsw'].reshape(1,-1).T,
                            RDS['B'],
                            RDS['g'])
        #Indicate shallow or missing water depth
        DS['WaterDepth']=np.zeros(SDS['Tm'].shape)
        WaterDepthcheck=np.flatnonzero(pd.isnull(SDS['DU']) & ~pd.isnull(SDS['STW']) & ~pd.isnull(SDS['Tm'])).reshape(1,-1).T
        if len(WaterDepthcheck)>0:
            SDS['LogValidity'][WaterDepthcheck]=0
            DS['WaterDepth'][WaterDepthcheck]=1
            print('Shallow or missing water depth!')
    
        #Resistance increase due to temperature in N
        if SDS['WSA'].ndim==1:
            SDS['WSA']=SDS['WSA'].reshape(1,-1).T
        SDS['DRt']=T.TEMP(SDS['STW'],
                                   SDS['WSA'],
                                   SDS['rhosw'],
                                   SDS['vsw'],
                                   RDS['Lbp'])
        
        #Power increase due to temperature in kW
        SDS['DPt']=SDS['DRt']*SDS['STW']*0.514444/SDS['etaDid']/1000
        
        #Resistance increase due to density and temperature in N
        SDS['DRrhow']=D.DENS(SDS['STW'],
                                        SDS['PDid'],
                                        SDS['etaDid'],
                                        SDS['rhosw'])
        
        #Power increase due to density and temperature in kW
        SDS['DPrhow']=SDS['DRrhow']*SDS['STW']*0.514444/SDS['etaDid']/1000
        
        #Resistance increase due to relative wind in N
        [SDS['DRwi'],Uwtref]=STwin.STAWIND( SDS['SOG'].ravel(),
                                        SDS['HEAD'].ravel(),
                                        SDS['Uwit'].ravel(),
                                        SDS['psiwit'].ravel(),
                                        SDS['Tair'].ravel(),
                                        SDS['pair'].ravel(),
                                        SDS['Tm'].ravel(),
                                        SDS['teu'].ravel(),
                                        RDS['Td'],
                                        RDS['Tb'],
                                        RDS['B'],
                                        RDS['Zaid'],
                                        RDS['Zad'],
                                        RDS['Ad'],
                                        RDS['TypeSTAWINDVessel'],
                                        RDS['SuperStructureSTAWINDVessel'])
        SDS['DRwi'] = SDS['DRwi'].reshape(1,-1).T
        
        #Indicate wind speed > 6Bft
        DS['HighWind']=np.zeros(SDS['Tm'].shape)
        HighWindcheck=np.flatnonzero(np.round(Uwtref.astype('float64')/0.514444)>27).reshape(1,-1).T
        if len(WaterDepthcheck)>0:
            SDS['LogValidity'][HighWindcheck]=0
            DS['HighWind'][HighWindcheck]=1
            print('Wind speed > 6Bft!')
    
        
        
        
        #Power increase due to relative wind in kW
        SDS['DPwi']=SDS['DRwi']*SDS['SOG']*0.514444/SDS['etaDid']/1000
    
         #Resistance increase due to waves in N
        [SDS['DRww'],OORwv]=STwav.STAWAVE( SDS['STW'],
                                        SDS['Tm'],
                                        SDS['Cb'],
                                        SDS['rhosw'],
                                        SDS['hsw'].reshape(1,-1).T,
                                        SDS['Hwv'].reshape(1,-1).T,
                                        SDS['Twv'].reshape(1,-1).T,
                                        SDS['psiwvr'],
                                        SDS['azbow'],
                                        RDS['Lbp'],
                                        RDS['B'],
                                        RDS['kyy'],
                                        RDS['g'])
        
        #Indicate wave range limitations
        DS['WaveMethodRange']=np.zeros(SDS['Tm'].shape)
        WaveMethodRangecheck=np.flatnonzero(~pd.isnull(OORwv)).reshape(1,-1).T
        if len(WaveMethodRangecheck)>0:
            SDS['LogValidity'][WaveMethodRangecheck]=0
            DS['WaveMethodRange'][WaveMethodRangecheck]=1
            print('Wave correction method out of range!')
    
         #Resistance increase due to swell in N
        [SDS['DRws'],OORsl]=STwav.STAWAVE( SDS['STW'],
                                        SDS['Tm'],
                                        SDS['Cb'],
                                        SDS['rhosw'],
                                        SDS['hsw'].reshape(1,-1).T,
                                        SDS['Hsl'].reshape(1,-1).T,
                                        SDS['Tsl'].reshape(1,-1).T,
                                        SDS['psislr'],
                                        SDS['azbow'],
                                        RDS['Lbp'],
                                        RDS['B'],
                                        RDS['kyy'],
                                        RDS['g'])
    
        #Indicate swell range limitations
        DS['SwellMethodRange']=np.zeros(SDS['Tm'].shape)
        SwellMethodRangecheck=np.flatnonzero(~pd.isnull(OORsl)).reshape(1,-1).T
        if len(SwellMethodRangecheck)>0:
            SDS['LogValidity'][SwellMethodRangecheck]=0;
            DS['SwellMethodRange'][SwellMethodRangecheck]=1
            print('Swell correction method out of range!')
    
    
        #Resistance increase due to waves and swell in N
        SDS['DRw']=SDS['DRww']+SDS['DRws']
        
        #Power increase due to waves and swell in kW
        SDS['DPw']=SDS['DRw']*SDS['STW']*0.514444/SDS['etaDid']/1000
    
        #Power increase due to displacement difference in kW
        SDS['DPv']=A.ADMIRALTY(SDS['PD'].reshape(1,-1).T,SDS['DISPV'],SDS['DISPVref'])
    
        #Resistance increase due to displacement difference in N
        # SDS['DRv']=SDS['DPv']*1000*SDS['etaDid']/SDS['STW']/0.514444
        SDS['DRv']=np.divide((SDS['DPv']*1000*SDS['etaDid']),SDS['STW'].astype('float64'),out= np.full_like((SDS['DPv']*1000*SDS['etaDid']),np.nan),where=SDS['STW']!=0)/0.514444
        #Total resistance increase due to environmental conditions in N
        SDS['DR']=SDS['DRt']+SDS['DRrhow']+SDS['DRwi']+SDS['DRw']+SDS['DRv']
    
        #Power correction due to load variation in kW
        SDS['DPksi']=L.LOAD(SDS['STW'],
                                     SDS['PD'].reshape(1,-1),
                                     SDS['DR'],
                                     SDS['etaDid'],
                                     RDS['ksiP'])
        #Total power correction in kW
        #Note:  Wave correction is based on both STAWAVE1 and STAWAVE2 method.
        #STAWAVE2 is more accurate than STAWAVE1 according to "Specialist
        #Committee on Performance of Ships in Service - Final Report and 
        #Recommendations to the 27th ITTC", chapters 8 and 9. A more 
        #accurate method is also described in this document and worth to
        #check it: "NMRI".
        SDS['DPD']=SDS['DPt']+SDS['DPrhow']+SDS['DPwi']+SDS['DPw']+SDS['DPksi']+SDS['DPv']
        
        #RPM correction due to load variation in RPM
        SDS['DNProp']=PR.PROPRPM(SDS['NProp'],
                                        SDS['STW'],
                                        SDS['PD'].reshape(1,-1).T,
                                        SDS['DR'],
                                        SDS['DU'],
                                        SDS['etaDid'],
                                        RDS['ksiP'],
                                        RDS['ksiN'],
                                        RDS['ksiU'])
       
        #Corrected results in kW/kn/RPM
        SDS['PDcor']=SDS['PD'].reshape(1,-1).T-SDS['DPD']
        #SDS['PDcor'][SDS['PDcor']<0]=np.nan
        SDS['Ucor']=SDS['STW']+SDS['DU']
        #SDS['Ucor'][SDS['Ucor']<0]=np.nan
        SDS['NPropcor']=SDS['NProp']-SDS['DNProp']
        #SDS['NPropcor'][SDS['NPropcor']<0]=np.nan
    
        #Indicate negative corrected speeds
        DS['NegativeUcor']=np.zeros(np.shape(SDS['Tm']))
        NegativeUcorcheck=np.argwhere((SDS['Ucor'].astype('float64').ravel())<=0).reshape(1,-1).T
        if len(NegativeUcorcheck)>0:
            SDS['Ucor'][NegativeUcorcheck]=0; #set it 0 to avoid complex numbers in calculations
            SDS['LogValidity'][NegativeUcorcheck]=0
            DS['NegativeUcor'][NegativeUcorcheck]=1
            print('Ucor<=0!')
   
        #Indicate negative corrected powers
        DS['NegativePDcor']=np.zeros(np.shape(SDS['Tm']))
        NegativePDcorcheck=np.argwhere((SDS['PDcor'].astype('float64').ravel())<=0).reshape(1,-1).T
        if len(NegativePDcorcheck)>0:
            SDS['PDcor'][NegativePDcorcheck]=0 #set it 0 to avoid complex numbers in calculations
            SDS['LogValidity'][NegativePDcorcheck]=0
            DS['NegativePDcor'][NegativePDcorcheck]=1
            print('PDcor<=0!')
   
        #Indicate negative corrected rpm
        DS['NegativeNPropcor']=np.zeros(np.shape(SDS['Tm']))
        NegativeNPropcorcheck=np.argwhere((SDS['NPropcor'].astype('float64').ravel())<=0).reshape(1,-1).T
        if len(NegativeNPropcorcheck)>0:
            SDS['NPropcor'][NegativeNPropcorcheck]=0#set it 0 to avoid complex numbers in calculations
            SDS['LogValidity'][NegativeNPropcorcheck]=0
            DS['NegativeNPropcor'][NegativeNPropcorcheck]=1
            print('NPropcor<=0!')
        # SRQST['TotalTimeLapse'].append(time.time()-start_time)
        # SRQST['Remarks'].append("complete")    
        # return [SDS,DS,SRQST]
    except Exception as err:
        exp = "Error in STATOTAL {} in line No. {} for IMO {}".format(err,sys.exc_info()[-1].tb_lineno,IMO)
        logger.error(exp,exc_info=True)
        ExceptionHandler(LogType,exp)
        SRQSTD['TotalTimeLapse']=time.time()-start_time              
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return({},{},SRQST)
        
    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time               
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(SDS,DS,SRQST)