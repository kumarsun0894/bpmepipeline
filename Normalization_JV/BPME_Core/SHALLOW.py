"""
Created on Mon Apr 20 11:18:22 2020

@author: AkhilSurendran

% SHALLOW
% Description:  This function calculates the speed correction due to
%               shallow water effects as per ISO15016, Annex G.
%
% Input:        STW     [kn]    ship speed through water (vector)
%               Tmid    [m]     draught at midships (vector)
%               B       [m]     ship breadth (scalar)
%               Amid    [m2]    midship section area under water (vector)
%               h       [m]     water depth (vector)
%               g       [m/s2]  acceleration of gravity (scalar)
%
% Output:       DU      [kn]    decrease of speed due to shallow water (vector)
%
% Limitations:  max([2*sqrt(B*Tmid),2*U^2/g]) <= h <= max([3*sqrt(B*Tmid),2.75*U^2/g])
%               Amid/h^2 >=0.05
%
% See also:     STATOTAL

"""
import numpy as np
def SHALLOW(STW,Tmid,Amid,hsw,B,g):
    
    #Convert ship speed through water from kn to m/s
    U=STW*0.514444
    #hsw=hsw.reshape(1,-1).T
    #By default DU=NaN covering the too shallow cases
    DU=np.empty(np.shape(Tmid))
    DU[:] = np.nan
    
    #Define deep water
    #isDeep=np.argwhere(hsw>=np.max([3*np.sqrt((B*Tmid).astype('float64')),2.75*U**2/g],axis=1))
    isDeep = np.flatnonzero(hsw>=np.max(np.maximum(3*np.sqrt((B*Tmid).astype('float64')),2.75*U**2/g),axis=1).reshape(1,-1).T).reshape(1,-1).T
    #define shallow water
    #isShallow=np.flatnonzero(np.logical_and(hsw>=np.max(np.maximum(2*np.sqrt((B*Tmid).astype('float64')),2*U**2/g),axis=1).reshape(1,-1).T,hsw<=np.max(np.maximum(3*np.sqrt((B*Tmid).astype('float64')),2.75*U**2/g),axis= 1).reshape(1,-1).T,Amid/hsw**2>=0.05)).reshape(1,-1).T
    isShallow=np.flatnonzero(np.logical_and(hsw>=np.max(np.maximum(2*np.sqrt((B*Tmid).astype('float64')),2*U**2/g),axis=1).reshape(1,-1).T,hsw<=np.max(np.maximum(3*np.sqrt((B*Tmid).astype('float64')),2.75*U**2/g),axis= 1).reshape(1,-1).T,np.divide(Amid.astype('float64'), hsw.astype('float64')**2, out=np.full_like(Amid.astype('float64'),float('nan')), where=hsw.astype('float64')**2!=0)>=0.05)).reshape(1,-1).T

    #isShallow=np.argwhere()
          
    if len(isDeep)>0 :
        DU[isDeep]=0
    if len(isShallow)>0:
         DU[isShallow]=U[isShallow]*(0.1242*(Amid[isShallow]/hsw[isShallow]**2-0.05)+1-np.sqrt(np.tanh(g*hsw[isShallow].astype('float64')/U[isShallow].astype('float64')**2)))/0.514444
    
    return DU     


