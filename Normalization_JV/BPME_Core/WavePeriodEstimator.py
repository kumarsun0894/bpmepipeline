# -*- coding: utf-8 -*-
from scipy.interpolate import pchip
import numpy as np
def WavePeriodEstimator(H13):
    #Correlation of wave height and period. Table from Lee and Bales, 1984
#    Hwmean=np.array([0 ,0.05,0.30,0.88,1.88,3.25,5.00,7.50,11.50,15.00])
#    Twmean=np.array([NaN NaN 6.3 7.5 8.8 9.7 12.4 15.0 16.4 20.0])
    
    Hwmean=np.array([0.30,0.88,1.88,3.25,5.00,7.50,11.50,15.00])
    Twmean=np.array([6.3,7.5,8.8,9.7,12.4,15.0,16.4,20.0])

    #Calculation of modal wave period (based on the mean significant wave height)
    pchipfn = pchip(Hwmean,Twmean)
    T13=pchipfn(H13)
    T13[H13==0]=0
    return T13
