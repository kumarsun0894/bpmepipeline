# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 20:57:48 2020

@author: Subhin Antony

% ADMIRALTY
% Description:  This method is described in ISO15016:2015, Annex H. If the
%               displacement of the ship at the S/P trial differs from the
%               required displacement within the limit of 2%, specified in
%               ISO15016:2015, Section 7.1, the following formula, based on
%               the Admiral-formula, shall be applied to the power values.
%
% Input:        PD      [kW]    measured delivered power (scalar)
%               V       [m3]    actual displacement (scalar)
%               Vref    [m3]    reference displacement (scalar)
%
% Output:       DPv     [kW]    power correction for difference in displacement (scalar)
%
% Note 1:       The 2% limit is for the Sea Trials scope. Under the ship
%               performance scope this is loosen to 5% as per ISO19030-2,
%               Section 4.5.
%
% Note 2:       The power correction for displacement is suitable in cases
%               where we strictly use as "reference curves" the model test
%               curves without creating a reference envelope. This
%               reference envelope is based on the interpolation between
%               model test data at specific draughts, extrapolation of such
%               curves for lower speed/power/RPM/propulsive efficiency that
%               are not provided in the model test data and piecewise
%               linear interpolation between two successive draughts. Thus,
%               we assume that we have known reference values for
%               power/speed/RPM/propulsive efficiency at any given
%               condition (within the extreme limits of the envelopes),
%               which means that the displacement correction for power is
%               irrelevant. This must be re-evaluated in the future when we
%               will decide if we follow the "reference envelope" or the
%               "reference curve" method. An idea is to first check if
%               |(V_ref-V_m)/V_ref |?2~5%; if true, then the reference
%               curve method is applied along with the correction; if
%               false, then the reference envelope is applied without any
%               correction.
%
% Limitations:  |(Vref-V)/V| <= 5%
%
% See also:     STATOTAL


"""

import numpy as np
def ADMIRALTY(PD,V,Vref):
    DPv=np.empty(np.shape(PD))
    DPv[:] = np.nan

    #Check if sea trials real and target displacements have less than 5%
    #acceptable difference according to ISO19030-2, paragraph 5.5
    isAcceptable=np.flatnonzero(100*abs(Vref-V)/V<=5).reshape(1,-1).T
    if len(isAcceptable)>0 :
        DPv[isAcceptable]=(-PD[isAcceptable])*((Vref[isAcceptable]/V[isAcceptable])**(2/3)-1)
    
    return DPv


