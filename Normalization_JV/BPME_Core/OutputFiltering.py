"""
Created on Mon Apr 20 11:18:22 2020

@author: Subhin Antony

% OutputFiltering
% Description:  This function checks proper correlation among the KPIs. If
%               correlation fails, then the corresponding log is considered
%               invalid.
%
% Input:        ODS (structure)
%               DS (structure)
%
% Output:       ODS (structure)
%               DS (structure)
%
% See also:     BPMEngineSingleVessel, HPOutputCalculation,

"""
import sys
import numpy as np
import pandas as pd
import time
import logging
from ExceptionHandler import ExceptionHandler
# create logger
logger = logging.getLogger('BPMEngine.2.02')

def OutputFiltering(IMO,SDS,RDS,ODS,DS,LogType,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        if LogType in 'Noon' or  LogType in 'HP'or LogType in 'Auto':
            if SDS['LogValidity'].ndim==1:
                SDS['LogValidity']=SDS['LogValidity'].reshape(1,-1).T
            #Check correlation of Speed Drop with Apparent Slip
            DS['SDASCorrelation']=np.zeros(ODS['SpeedDrop'].shape)
            SDAScheck=np.flatnonzero(np.abs(ODS['SpeedDrop']-ODS['ApparentSlip'])>RDS['CorAllowance']).reshape(1,-1).T
            if len(SDAScheck)>0:
                SDS['LogValidity'][SDAScheck]=0
                DS['SDASCorrelation'][SDAScheck]=1
                print('Large SD from AS deviation!')
            #Check correlation of Speed Drop with RPM deviation
            DS['SDdeltaNCorrelation']=np.zeros(ODS['SpeedDrop'].shape)
            SDdeltaNcheck=np.flatnonzero(np.abs(ODS['SpeedDrop']-ODS['deltaN'])>RDS['CorAllowance']).reshape(1,-1).T
            if len(SDdeltaNcheck)>0:
                SDS['LogValidity'][SDdeltaNcheck]=0
                # ODS['deltaN'][SDdeltaNcheck]=np.nan
                DS['SDdeltaNCorrelation'][SDdeltaNcheck]=1
                print('Large SD from dN deviation!')
            #Check correlation of RPM deviation with Apparent Slip
            DS['deltaNASCorrelation']=np.zeros(ODS['SpeedDrop'].shape)
            deltaNAScheck=np.flatnonzero(np.abs(ODS['deltaN']-ODS['ApparentSlip'])>RDS['CorAllowance']).reshape(1,-1).T
            if len(deltaNAScheck)>0:
                SDS['LogValidity'][deltaNAScheck]=0
                DS['deltaNASCorrelation'][deltaNAScheck]=1
                print('Large dN from AS deviation!')

            #Check correlation of PD1 deviation from FOC deviation
            DS['deltaPD1FOCCorrelation']=np.zeros(np.shape(ODS['SpeedDrop']))
            deltaPD1deltaFOCcheck=np.abs(ODS['deltaPD1']-ODS['deltaFOCME'])>RDS['CorAllowance']
            if np.any(deltaPD1deltaFOCcheck):
                SDS['LogValidity'][deltaPD1deltaFOCcheck]=0
                # ODS['deltaFOCME'][deltaPD1deltaFOCcheck]=np.nan
                DS['deltaPD1FOCCorrelation'][deltaPD1deltaFOCcheck]=1
                print('Large dP from dFOC deviation!')

            #Check correlation of Power deviation from Speed Drop
            DS['deltaPD1SDCorrelation']=np.zeros(np.shape(ODS['SpeedDrop']))
            deltaPD1SDcheck=np.flatnonzero(np.abs(ODS['deltaPD1']-((ODS['SpeedDrop']/100+1)**3-1)*100)>RDS['CorAllowance']).reshape(1,-1).T
            if len(deltaPD1SDcheck)>0:
                SDS['LogValidity'][deltaPD1SDcheck]=0
                # ODS['deltaPD1'][deltaPD1SDcheck]=np.nan
                DS['deltaPD1SDCorrelation'][deltaPD1SDcheck]=1
                print('Large dP from f(SD) deviation!')


            #Check if ME load is out of 10-100# MCR range
            #This may need to be moved to another function, e.g. MEOutputFiltering
            DS['MELoadRange']=np.zeros(np.shape(ODS['SpeedDrop']))
            MELoadRangecheck=np.flatnonzero(np.logical_or(np.nanmean(SDS['MCRME'],axis= 1)<10 , np.nanmean(SDS['MCRME'],axis = 1)>100)).reshape(1,-1).T #nanmean to accomodate multiple engines
            if len(MELoadRangecheck)>0:
                SDS['LogValidity'][MELoadRangecheck]=0
                DS['MELoadRange'][MELoadRangecheck]=1
                print('ME load is out of 10-100# MCR range!')


            #Check if Ucor is out of 10-90# MCR range
            DS['SpeedRange']=np.zeros(np.shape(ODS['SpeedDrop']))
            SpeedRangecheck=np.flatnonzero(np.logical_or(SDS['Ucor']<RDS['Uminref'] ,SDS['Ucor']>RDS['Umaxref'])).reshape(1,-1).T
            if len(SpeedRangecheck)>0:
                SDS['LogValidity'][SpeedRangecheck]=0
                DS['SpeedRange'][SpeedRangecheck]=1
                print('Speed is out of the range corresponding to 10-90# MCR!')

            #Find invalid reports
    #        KPINaNs = []
    #        KPINaNs.append(pd.isnull(ODS['SpeedDrop']))
    #        KPINaNs.append(pd.isnull(ODS['ApparentSlip']))
    #        KPINaNs.append(pd.isnull(ODS['deltaN']))
    #        KPINaNs.append(pd.isnull(ODS['deltaPD1']))
    #        KPINaNs.append(pd.isnull(ODS['deltaPD2']))
    #        KPINaNs.append(pd.isnull(ODS['deltaSFOCisoME']))
    #        KPINaNs.append(pd.isnull(ODS['deltaFOCME']))
    #        KPINaNs = np.array(KPINaNs).T

            #Indicate not calculated speed drop
            DS['MissingSD']=np.zeros(np.shape(SDS['Tm']))
            MissingSDcheck=np.flatnonzero(pd.isnull(ODS['SpeedDrop'])).reshape(1,-1).T
            if len(MissingSDcheck)>0:
                SDS['LogValidity'][MissingSDcheck]=0
                DS['MissingSD'][MissingSDcheck]=1
                print('Missing Speed Drop!')

            #Indicate not calculated Apparent Slip
            DS['MissingAS']=np.zeros(np.shape(SDS['Tm']))
            MissingAScheck=np.flatnonzero(pd.isnull(ODS['ApparentSlip'])).reshape(1,-1).T
            if len(MissingAScheck)>0:
                SDS['LogValidity'][MissingAScheck]=0
                DS['MissingAS'][MissingAScheck]=1
                print('Missing Apparent Slip!')

            #Indicate not calculated deltaN
            DS['MissingdeltaN']=np.zeros(np.shape(SDS['Tm']))
            MissingdeltaNcheck=np.flatnonzero(pd.isnull(ODS['deltaN'])).reshape(1,-1).T
            if len(MissingdeltaNcheck)>0:
                SDS['LogValidity'][MissingdeltaNcheck]=0
                DS['MissingdeltaN'][MissingdeltaNcheck]=1
                print('Missing deltaN!')

            #Indicate not calculated deltaPD1
            DS['MissingdeltaPD1']=np.zeros(np.shape(SDS['Tm']))
            MissingdeltaPD1check=np.flatnonzero(pd.isnull(ODS['deltaPD1'])).reshape(1,-1).T
            if len(MissingdeltaPD1check)>0:
                SDS['LogValidity'][MissingdeltaPD1check]=0
                DS['MissingdeltaPD1'][MissingdeltaPD1check]=1
                print('Missing deltaPD1!');

            #Indicate not calculated deltaPD2
            DS['MissingdeltaPD2']=np.zeros(np.shape(SDS['Tm']))
            MissingdeltaPD2check=np.flatnonzero(pd.isnull(ODS['deltaPD2']))
            if len(MissingdeltaPD2check)>0:
                 SDS['LogValidity'][MissingdeltaPD2check]=0
                 DS['MissingdeltaPD2'][MissingdeltaPD2check]=1
                 print('Missing deltaPD2!');

            #Indicate not calculated deltaSFOCisoME
            DS['MissingdeltaSFOCisoME']=np.zeros(np.shape(SDS['Tm']))
            MissingdeltaSFOCisoMEcheck=np.flatnonzero(pd.isnull(ODS['deltaSFOCisoME'])).reshape(1,-1).T
            if len(MissingdeltaSFOCisoMEcheck)>0:
                SDS['LogValidity'][MissingdeltaSFOCisoMEcheck]=0
                DS['MissingdeltaSFOCisoME'][MissingdeltaSFOCisoMEcheck]=1
                print('Missing deltaSFOCisoME!')

            #Indicate not calculated deltaFOCME
            DS['MissingdeltaFOCME']=np.zeros(np.shape(SDS['Tm']))
            MissingdeltaFOCMEcheck=np.flatnonzero(pd.isnull(ODS['deltaFOCME'])).reshape(1,-1).T
            if len(MissingdeltaFOCMEcheck)>0:
                SDS['LogValidity'][MissingdeltaFOCMEcheck]=0
                DS['MissingdeltaFOCME'][MissingdeltaFOCMEcheck]=1
                print('Missing deltaFOCME!')

            #Set a flag (1=valid, 0=invalid) for each report
            #ODS['LogValidity']=~np.any(KPINaNs==1,axis=2).T
            ODS['LogValidity']=SDS['LogValidity']


        if 'ME' in LogType:
            #this is dummy now. it must be defined properly
            ODS['LogValidityME']=np.ones(np.shape(SDS['pambairER']))
        if 'AE' in LogType:
            #this is dummy now. it must be defined properly
            ODS['LogValidityAE']=np.ones(np.shape(SDS['pambairER']))
        # SRQST['TotalTimeLapse'].append(time.time()-start_time)
        # SRQST['Remarks'].append("complete")

        # return [ODS,DS,SRQST]
        
        
        # # # Dignostic update --Dec 2020
        # if 'Noon' in LogType:
        #     #DS['MissingActualpropellermargin']=np.where(pd.isnull(((SDS['PeffME'] - ODS['PeffLightidME'])*100/ODS['PeffLightidME'])), 1, 0)
        #     DS['MissingAEU']=np.where(pd.isnull(ODS['AEU']), 1, 0)
        #     #DS['MissingdeltaSCOCME']=np.where(pd.isnull((SDS['SCOCME']-ODS['SCOCidME'])*100/ODS['SCOCidME']), 1, 0)
        #     DS['MissingdeltaSCOCME']=np.where(pd.isnull(np.divide((SDS['SCOCME']-ODS['SCOCidME'])*100, ODS['SCOCidME'], out=np.full_like((SDS['SCOCME']-ODS['SCOCidME'])*100,float('nan')), where=ODS['SCOCidME']!=0)), 1, 0)
        # if 'ME' in LogType:
        #     DS['MissingdeltaSFOCidME']=np.where(pd.isnull(ODS['SFOCisoidME']), 1, 0)
        #     #DS['MissingdeltaSFOCME']=np.where(pd.isnull(((SDS['SFOCisoME'] - ODS['SFOCisoidME'])*100/ODS['SFOCisoidME'])), 1, 0)
        #     DS['MissingdeltaSFOCME']=np.where(pd.isnull(np.divide((SDS['SFOCisoME'] - ODS['SFOCisoidME'])*100, ODS['SFOCisoidME'], out=np.full_like((SDS['SFOCisoME'] - ODS['SFOCisoidME'])*100,float('nan')), where=ODS['SFOCisoidME']!=0)), 1, 0)
        #     DS['MissingSFOCcorME']=np.where(pd.isnull(SDS['SFOCisoME']), 1, 0)
            
        #     #DS['MEinletpressure']=np.where(np.logical_and((abs((SDS['pscavabsisoME'] - ODS['pscavabsisoidME'])*100/ODS['pscavabsisoidME']) > 10),(abs((SDS['pcompabsisoME'] - ODS['pcompabsisoidME'])*100/ODS['pcompabsisoidME']) < 8)), 1, 0)
        #     MEinletpressure_logic=np.logical_and((abs(np.divide((SDS['pscavabsisoME'] - ODS['pscavabsisoidME'])*100, ODS['pscavabsisoidME'], out=np.full_like((SDS['pscavabsisoME'] - ODS['pscavabsisoidME'])*100,float('nan')), where=ODS['pscavabsisoidME']!=0)) > 10),(abs(np.divide((SDS['pcompabsisoME'] - ODS['pcompabsisoidME'])*100, ODS['pcompabsisoidME'], out=np.full_like((SDS['pcompabsisoME'] - ODS['pcompabsisoidME'])*100,float('nan')), where=ODS['pcompabsisoidME']!=0)) < 8)) or pd.isnull(abs(np.divide((SDS['pscavabsisoME'] - ODS['pscavabsisoidME'])*100, ODS['pscavabsisoidME'], out=np.full_like((SDS['pscavabsisoME'] - ODS['pscavabsisoidME'])*100,float('nan')), where=ODS['pscavabsisoidME']!=0)))or pd.isnull(abs(np.divide((SDS['pcompabsisoME'] - ODS['pcompabsisoidME'])*100, ODS['pcompabsisoidME'], out=np.full_like((SDS['pcompabsisoME'] - ODS['pcompabsisoidME'])*100,float('nan')), where=ODS['pcompabsisoidME']!=0)))
        #     DS['MEinletpressure']=np.where(MEinletpressure_logic, 1, 0)
        #     #DS['MissingActualpropellermargin']=np.where(pd.isnull(((SDS['PeffME'].ravel() - ODS['PeffLightidME'].ravel())*100/ODS['PeffLightidME'].ravel())), 1, 0)
        #     DS['MissingActualpropellermargin']=np.where(pd.isnull(np.divide((SDS['PeffME'].ravel() - ODS['PeffLightidME'].ravel())*100, ODS['PeffLightidME'].ravel(), out=np.full_like((SDS['PeffME'].ravel() - ODS['PeffLightidME'].ravel())*100,float('nan')), where=ODS['PeffLightidME'].ravel()!=0)), 1, 0)
        #     #new
        #     TegTCinME = SDS['TegTCinTCME'] # No diagnostics defined. This is TC wise for the ME.
        #     TegTCinidME = ODS['TegTCinidME'] # Corresponding Diagnostics --> MissingdeltaTegTCinME
        #     DS['MissingdeltaTegTCinME']=np.where(pd.isnull(TegTCinidME), 1, 0)
        #     #deltaTegTCin = (TegTCinME - TegTCinidME) * 100/TegTCinidME
        #     deltaTegTCin = np.divide((TegTCinME - TegTCinidME) * 100, TegTCinidME, out=np.full_like((TegTCinME - TegTCinidME) * 100,float('nan')), where=TegTCinidME!=0)
        #     DS['MissingdeltaTegTCinidME']=np.where(pd.isnull(deltaTegTCin), 1, 0)
            
        #     if SDS['TegTCoutTCME'].ndim==1:
        #         TegTCoutME = SDS['TegTCoutTCME'].reshape(1,-1).T # No diagnostics defined. (Only do a reshaping if its 1-D)
        #     else:
        #         TegTCoutME = SDS['TegTCoutTCME']
        #     TegTCoutidME = ODS['TegTCoutidME'] # Corresponding Diagnostics --> MissingdeltaTegTCoutME
        #     DS['MissingdeltaTegTCoutME']=np.where(pd.isnull(TegTCoutidME), 1, 0)
        #     #deltaTegTCout = (TegTCoutME - TegTCoutidME) * 100/TegTCoutidME
        #     deltaTegTCout = np.divide( (TegTCoutME - TegTCoutidME) * 100, TegTCoutidME, out=np.full_like( (TegTCoutME - TegTCoutidME) * 100,float('nan')), where=TegTCoutidME!=0)
        #     DS['MissingdeltaTegTCoutidME']=np.where(pd.isnull(deltaTegTCout), 1, 0)
        #     if SDS['FPIcylME'].ndim==1:
        #         FPIME = SDS['FPIcylME'].reshape(1,-1).T  # No diagnostics defined. (Only do a reshaping if its 1-D) This is cylinder wise FPI
        #     else:
        #         FPIME = SDS['FPIcylME']
        #     FPIidME = ODS['FPIidME'] # Corresponding Diagnostics --> MissingdeltaFPIidME
        #     DS['MissingdeltaFPIidME']=np.where(pd.isnull(FPIidME), 1, 0)
        #     #deltaFPIME = (FPIME - FPIidME) * 100/FPIidME # Corresponding Diagnostics --> MissingdeltaFPIME  
        #     deltaFPIME = np.divide((FPIME - FPIidME) * 100, FPIidME, out=np.full_like((FPIME - FPIidME) * 100,float('nan')), where=FPIidME!=0) # Corresponding Diagnostics --> MissingdeltaFPIME  
            
        #     DS['MissingdeltaFPIME']=np.where(pd.isnull(deltaFPIME), 1, 0)
        #     if SDS['pindcylME'].ndim==1:
        #         pindME = SDS['pindcylME'].reshape(1,-1).T  # Corresponding Diagnostics --> MissingpindcorME  (Only do a reshaping if its 1-D) Not corrected. Check if its defined in new iso corrections. This is cylinder wise pind 
        #         DS['MissingpindcorME']=np.where(pd.isnull(pindME), 1, 0)
        #     else:
        #         pindME = SDS['pindcylME']
        #         DS['MissingpindcorME']=np.where(pd.isnull(pindME), 1, 0)
        #     pindidME = ODS['pindidME'] # Corresponding Diagnostics --> MissingdeltapindidME
        #     DS['MissingdeltapindidME']=np.where(pd.isnull(pindidME), 1, 0)
        #     #deltapindME = (pindME - pindidME) * 100/pindidME # Corresponding Diagnostics --> MissingdeltapindME
        #     deltapindME = np.divide((pindME - pindidME) * 100, pindidME, out=np.full_like((pindME - pindidME) * 100,float('nan')), where=pindidME!=0)
        #     DS['MissingdeltapindME']=np.where(pd.isnull(deltapindME), 1, 0)
        #     if SDS['pmaxcylME'].ndim==1:
        #         pmaxabsME = SDS['pmaxcylME'].reshape(1,-1) # Corresponding Diagnostics --> MissingpmaxcorME  (Only do a reshaping if its 1-D) Not corrected. Check if its defined in new iso corrections.
        #         DS['MissingpmaxcorME']=np.where(pd.isnull(pmaxabsME), 1, 0)
        #     else:
        #         pmaxabsME = SDS['pmaxcylME'] # Corresponding Diagnostics --> MissingpmaxcorME  (Only do a reshaping if its 1-D) Not corrected. Check if its defined in new iso corrections.
        #         DS['MissingpmaxcorME']=np.where(pd.isnull(pmaxabsME), 1, 0)
        #     pmaxabsisoidME = ODS['pmaxabsisoidME'] # Corresponding Diagnostics --> MissingdeltapmaxidME
        #     DS['MissingdeltapmaxidME']=np.where(pd.isnull(pmaxabsisoidME), 1, 0)
        #     #deltapmaxME = (pmaxabsME - pmaxabsisoidME) * 100/pmaxabsisoidME # Corresponding Diagnostics --> MissingdeltapmaxME
        #     deltapmaxME = np.divide((pmaxabsME - pmaxabsisoidME) * 100, pmaxabsisoidME, out=np.full_like((pmaxabsME - pmaxabsisoidME) * 100,float('nan')), where=pmaxabsisoidME!=0) # Corresponding Diagnostics --> MissingdeltapmaxME
        #     DS['MissingdeltapmaxME']=np.where(pd.isnull(deltapmaxME), 1, 0)
        #     if SDS['pcompcylME'].ndim==1:
        #         pcompabsME = SDS['pcompcylME'].reshape(1,-1).T # Corresponding Diagnostics --> MissingpcompcorME  (Only do a reshaping if its 1-D) Not corrected. Check if its defined in new iso corrections.
        #         DS['MissingpcompcorME']=np.where(pd.isnull(pcompabsME), 1, 0)
        #     else:
        #         pcompabsME = SDS['pcompcylME'] # Corresponding Diagnostics --> MissingpcompcorME  (Only do a reshaping if its 1-D) Not corrected. Check if its defined in new iso corrections.
        #         DS['MissingpcompcorME']=np.where(pd.isnull(pcompabsME), 1, 0)
        #     pcompabsisoidME = ODS['pcompabsisoidME'] # Corresponding Diagnostics --> MissingdeltapcompidME
        #     DS['MissingdeltapcompidME']=np.where(pd.isnull(pcompabsisoidME), 1, 0)
        #     #deltapcompME = (pcompabsME - pcompabsisoidME) * 100/pcompabsisoidME # Corresponding Diagnostics --> MissingdeltapcompME
        #     deltapcompME = np.divide((pcompabsME - pcompabsisoidME) * 100, pcompabsisoidME, out=np.full_like((pcompabsME - pcompabsisoidME) * 100,float('nan')), where=pcompabsisoidME!=0) # Corresponding Diagnostics --> MissingdeltapcompME
        #     DS['MissingdeltapcompME']=np.where(pd.isnull(deltapcompME), 1, 0)
        #     if SDS['pmaxcylME'].ndim==1 and SDS['pcompcylME'].ndim==1:
        #         pmax_pcompME = SDS['pmaxcylME'].reshape(1,-1).T - SDS['pcompcylME'].reshape(1,-1).T # Corresponding Diagnostics --> MissingpmaxcorME_pcompcorME  (Only do a reshaping if its 1-D). This can be corrected by individually correcting each value using the new ISO corrections defined.
        #         DS['MissingpmaxcorME_pcompcorME']=np.where(pd.isnull(pmax_pcompME), 1, 0)
        #     else:
        #         pmax_pcompME = SDS['pmaxcylME'] - SDS['pcompcylME'] # Corresponding Diagnostics --> MissingpmaxcorME_pcompcorME  (Only do a reshaping if its 1-D). This can be corrected by individually correcting each value using the new ISO corrections defined.
        #         DS['MissingpmaxcorME_pcompcorME']=np.where(pd.isnull(pmax_pcompME), 1, 0)
            
        #     pmax_pcompisoidME = ODS['pmax_pcompisoidME'] # Corresponding Diagnostics --> MissingpmaxidME_pcompidME
        #     DS['MissingpmaxidME_pcompidME']=np.where(pd.isnull(pmax_pcompisoidME), 1, 0)
        #     #deltapmax_pcompME = (pmax_pcompME - pmax_pcompisoidME) * 100/pmax_pcompisoidME # Corresponding Diagnostics --> MissingpmaxME_pcompME. Here its assumed that this Diagnostics is corresponding to delta. 
        #     deltapmax_pcompME = np.divide((pmax_pcompME - pmax_pcompisoidME) * 100, pmax_pcompisoidME, out=np.full_like((pmax_pcompME - pmax_pcompisoidME) * 100,float('nan')), where=pmax_pcompisoidME!=0)
        #     DS['MissingpmaxME_pcompME']=np.where(pd.isnull(deltapmax_pcompME), 1, 0)
        #     if SDS['pcompcylME'].ndim==1 and SDS['pscavME'].ndim==1:
        #         pcomp_pscavME = SDS['pcompcylME'].reshape(1,-1).T/SDS['pscavME'].reshape(1,-1).T # Corresponding Diagnostics --> MissingpcompcorME_pscavcorME  (Only do a reshaping if its 1-D). This can be corrected by individually correcting each value using the new ISO corrections defined.
        #         DS['MissingpcompcorME_pscavcorME']=np.where(pd.isnull(pcomp_pscavME), 1, 0)
        #     else:
        #         pcomp_pscavME = SDS['pcompcylME']/SDS['pscavME'] # Corresponding Diagnostics --> MissingpcompcorME_pscavcorME  (Only do a reshaping if its 1-D). This can be corrected by individually correcting each value using the new ISO corrections defined.
        #         DS['MissingpcompcorME_pscavcorME']=np.where(pd.isnull(pcomp_pscavME), 1, 0)
        #     pcomp_pscavabsisoidME = ODS['pcomp_pscavabsisoidME'] # Corresponding Diagnostics --> MissingpcompidME_pscavidME
        #     DS['MissingpcompidME_pscavidME']=np.where(pd.isnull(pcomp_pscavabsisoidME), 1, 0)
        #     #deltapcomp_pscavME = (pcomp_pscavME - pcomp_pscavabsisoidME) * 100/pcomp_pscavabsisoidME # Corresponding Diagnostics --> MissingpcompME_pscavME. Here its assumed that this Diagnostics is corresponding to delta. 
        #     deltapcomp_pscavME = np.divide((pcomp_pscavME - pcomp_pscavabsisoidME) * 100, pcomp_pscavabsisoidME, out=np.full_like((pcomp_pscavME - pcomp_pscavabsisoidME) * 100,float('nan')), where=pcomp_pscavabsisoidME!=0)
        #     DS['MissingpcompME_pscavME']=np.where(pd.isnull(deltapcomp_pscavME), 1, 0)
        #     if SDS['dpairAFTCME'].ndim==1:
        #         dpairAFTCME = SDS['dpairAFTCME'].reshape(1,-1).T # Corresponding Diagnostics --> MissingpairAFcorME  (Only do a reshaping if its 1-D). Check the feasibility of correcting this value.
        #         DS['MissingpairAFcorME']=np.where(pd.isnull(dpairAFTCME), 1, 0)
        #     else:
        #         dpairAFTCME = SDS['dpairAFTCME'] # Corresponding Diagnostics --> MissingpairAFcorME  (Only do a reshaping if its 1-D). Check the feasibility of correcting this value.
        #         DS['MissingpairAFcorME']=np.where(pd.isnull(dpairAFTCME), 1, 0)
        #     dpairAFidME = ODS['dpairAFidME'] # Corresponding Diagnostics --> MissingdeltapairAFidME
        #     DS['MissingdeltapairAFidME']=np.where(pd.isnull(dpairAFidME), 1, 0)
            
        #     #deltapairAFME = (dpairAFTCME - dpairAFidME) * 100/dpairAFidME # Corresponding Diagnostics --> MissingdeltapairAFME. Here its assumed that this Diagnostics is corresponding to delta. 
        #     deltapairAFME = np.divide((dpairAFTCME - dpairAFidME) * 100, dpairAFidME, out=np.full_like((dpairAFTCME - dpairAFidME) * 100,float('nan')), where=dpairAFidME!=0)
        #     DS['MissingdeltapairAFME']=np.where(pd.isnull(deltapairAFME), 1, 0)
            
        #     DS['MEAFdeposits']=np.where(np.logical_or(deltapairAFME <-10,deltapairAFME>30),1,0)
            
        #     if SDS['dpairACTCME'].ndim==1:
        #         dpairACTCME = SDS['dpairACTCME'].reshape(1,-1).T # Corresponding Diagnostics --> MissingpairACcorME  (Only do a reshaping if its 1-D). Check the feasibility of correcting this value.
        #         DS['MissingpairACcorME']=np.where(pd.isnull(dpairACTCME), 1, 0)
        #     else:
        #         dpairACTCME = SDS['dpairACTCME']
        #         DS['MissingpairACcorME']=np.where(pd.isnull(dpairACTCME), 1, 0)
                
        #     dpairACidME = ODS['dpairACidME'] # Corresponding Diagnostics --> MissingdeltapairACidME
        #     DS['MissingdeltapairACidME']=np.where(pd.isnull(dpairACidME), 1, 0)
        #     #deltapairACME = (dpairACTCME - dpairACidME) * 100/dpairACidME # Corresponding Diagnostics --> MissingdeltapairACME. Here its assumed that this Diagnostics is corresponding to delta. 
        #     deltapairACME = np.divide((dpairACTCME - dpairACidME) * 100, dpairACidME, out=np.full_like((dpairACTCME - dpairACidME) * 100,float('nan')), where=dpairACidME!=0)
        #     DS['MissingdeltapairACME']=np.where(pd.isnull(deltapairACME), 1, 0)
        #     # if abs(deltapairACME)>10:
        #         # Trigger Diagnostics MEACpressuredrop = 1 # Shape will be same as deltapairACME
        #     DS['MEACpressuredrop']=np.where(abs(deltapairACME)>10, 1, 0)
            
        #     if SDS['dTcwACTCME'].ndim==1:
                
        #         dTcwACTCME = SDS['dTcwACTCME'].reshape(1,-1).T # Corresponding Diagnostics --> MissingTcwACcorME  (Only do a reshaping if its 1-D). Check the feasibility of correcting this value.
        #         DS['MissingTcwACcorME']=np.where(pd.isnull(dTcwACTCME), 1, 0)
        #     else:
        #         dTcwACTCME = SDS['dTcwACTCME']
        #         DS['MissingTcwACcorME']=np.where(pd.isnull(dTcwACTCME), 1, 0)
        #     dTcwACidME = ODS['dTcwACidME'] # Corresponding Diagnostics --> MissingdeltaTcwACidME
        #     DS['MissingdeltaTcwACidME']=np.where(pd.isnull(dTcwACidME), 1, 0)
        #     #deltaTcwACME = (dTcwACTCME - dTcwACidME) * 100/dTcwACidME # Corresponding Diagnostics --> MissingdeltaTcwACME. Here its assumed that this Diagnostics is corresponding to delta. 
        #     deltaTcwACME = np.divide((dTcwACTCME - dTcwACidME) * 100, dTcwACidME, out=np.full_like((dTcwACTCME - dTcwACidME) * 100,float('nan')), where=dTcwACidME!=0)
        #     DS['MissingdeltaTcwACME']=np.where(pd.isnull(deltaTcwACME), 1, 0)
        #     #if (SDS['TairACinTCME'] - SDS['TairACoutTCME'])/(SDS['TairACinTCME'] - SDS['TltcwACinTCME']) >= 0.98 or <=0.88
        #         # Trigger Diagnostics MEACToutlet = 1 # Shape will be same as TairACinTCME
        #     # MEACToutlet_logic=np.logical_or(((SDS['TairACinTCME'] - SDS['TairACoutTCME'])/(SDS['TairACinTCME'] - SDS['TltcwACinTCME'])) > 0.98,((SDS['TairACinTCME'] - SDS['TairACoutTCME'])/(SDS['TairACinTCME'] - SDS['TltcwACinTCME'])) < 0.88)
        #     MEACToutlet_logic = np.logical_or(np.divide((SDS['TairACinTCME'] - SDS['TairACoutTCME']), (SDS['TairACinTCME'] - SDS['TltcwACinTCME']), out=np.full_like((SDS['TairACinTCME'] - SDS['TairACoutTCME']),float('nan')), where=(SDS['TairACinTCME'] - SDS['TltcwACinTCME'])!=0) > 0.98,np.divide((SDS['TairACinTCME'] - SDS['TairACoutTCME']), (SDS['TairACinTCME'] - SDS['TltcwACinTCME']), out=np.full_like((SDS['TairACinTCME'] - SDS['TairACoutTCME']),float('nan')), where=(SDS['TairACinTCME'] - SDS['TltcwACinTCME'])!=0) < 0.88) or pd.isnull(np.divide((SDS['TairACinTCME'] - SDS['TairACoutTCME']), (SDS['TairACinTCME'] - SDS['TltcwACinTCME']), out=np.full_like((SDS['TairACinTCME'] - SDS['TairACoutTCME']),float('nan')), where=(SDS['TairACinTCME'] - SDS['TltcwACinTCME'])!=0)) or pd.isnull(np.divide((SDS['TairACinTCME'] - SDS['TairACoutTCME']), (SDS['TairACinTCME'] - SDS['TltcwACinTCME']), out=np.full_like((SDS['TairACinTCME'] - SDS['TairACoutTCME']),float('nan')), where=(SDS['TairACinTCME'] - SDS['TltcwACinTCME'])!=0))
        #     DS['MEACToutlet']=np.where(MEACToutlet_logic, 1, 0)   
        #     if SDS['dTaircwACTCME'].ndim==1:
        #         dTaircwACTCME = SDS['dTaircwACTCME'].reshape(1,-1).T # Corresponding Diagnostics --> MissingTaircwACcorME  (Only do a reshaping if its 1-D). Check the feasibility of correcting this value.
        #         DS['MissingTaircwACcorME']=np.where(pd.isnull(dTaircwACTCME), 1, 0)
        #     else:
        #         dTaircwACTCME = SDS['dTaircwACTCME']
        #         DS['MissingTaircwACcorME']=np.where(pd.isnull(dTaircwACTCME), 1, 0)
        #     dTaircwACidME = ODS['dTaircwACidME'] # Corresponding Diagnostics --> MissingdeltaTaircwACidME
        #     DS['MissingdeltaTaircwACidME']=np.where(pd.isnull(dTaircwACidME), 1, 0)
        #     #deltaTaircwACTCME  = (dTaircwACTCME - dTaircwACidME) * 100/dTaircwACidME # Corresponding Diagnostics --> MissingdeltaTaircwACME. Here its assumed that this Diagnostics is corresponding to delta. 
        #     deltaTaircwACTCME  =np.divide((dTaircwACTCME - dTaircwACidME) * 100, dTaircwACidME, out=np.full_like((dTaircwACTCME - dTaircwACidME) * 100,float('nan')), where=dTaircwACidME!=0)
        #     DS['MissingdeltaTaircwACME']=np.where(pd.isnull(deltaTaircwACTCME), 1, 0)   
        #     #if  SDS['TairACoutTCME'] == NaN
        #         # Trigger Diagnostics MissingTairACoutTCME_avg = 1 # shape will be same as SDS['TairACoutTCME']
        #     DS['MissingTairACoutTCME_avg']=np.where(pd.isnull(SDS['TairACoutTCME']), 1, 0)
             
        #     #if  SDS['TairACinTCME'] == NaN
        #         # Trigger Diagnostics MissingTltcwACinTCME = 1 # shape will be same as SDS['TairACoutTCME']
        #     DS['MissingTltcwACinTCME']=np.where(pd.isnull(SDS['TairACinTCME']), 1, 0)
            
        #     if SDS['etaTCcompcalcTCME'].ndim==1:
        #         etaTCcompcalcTCME = SDS['etaTCcompcalcTCME'].reshape(1,-1).T # Corresponding Diagnostics --> MissingetaTCcompcorME  (Only do a reshaping if its 1-D). Check the feasibility of correcting this value.
        #         DS['MissingetaTCcompcorME']=np.where(pd.isnull(etaTCcompcalcTCME), 1, 0)
        #     else:
        #         etaTCcompcalcTCME = SDS['etaTCcompcalcTCME'] # Corresponding Diagnostics --> MissingetaTCcompcorME  (Only do a reshaping if its 1-D). Check the feasibility of correcting this value.
        #         DS['MissingetaTCcompcorME']=np.where(pd.isnull(etaTCcompcalcTCME), 1, 0)
                
        #     etaTCcompidME = ODS['etaTCcompidME'] # Corresponding Diagnostics --> MissingdeltaetaTCcompidME
        #     DS['MissingdeltaetaTCcompidME']=np.where(pd.isnull(etaTCcompidME), 1, 0)
        #     #deltaetaTCcompME  = (etaTCcompcalcTCME - etaTCcompidME) * 100/etaTCcompidME # Corresponding Diagnostics --> MissingdeltaetaTCcompME. Here its assumed that this Diagnostics is corresponding to delta. 
        #     deltaetaTCcompME  = np.divide((etaTCcompcalcTCME - etaTCcompidME) * 100, etaTCcompidME, out=np.full_like((etaTCcompcalcTCME - etaTCcompidME) * 100,float('nan')), where=etaTCcompidME!=0)
        #     DS['MissingdeltaetaTCcompME']=np.where(pd.isnull(deltaetaTCcompME), 1, 0)
        #     etaTCturbcalcTCME = SDS['etaTCturbcalcTCME'] # Corresponding Diagnostics --> MissingetaTCturbcorME  (Only do a reshaping if its 1-D). Check the feasibility of correcting this value.
        #     DS['MissingetaTCturbcorME']=np.where(pd.isnull(etaTCturbcalcTCME), 1, 0)
        #     etaTCturbidME = ODS['etaTCturbidME'] # Corresponding Diagnostics --> MissingdeltaetaTCturbidME
        #     DS['MissingdeltaetaTCturbidME']=np.where(pd.isnull(etaTCturbidME), 1, 0)
        #     #deltaetaTCturbME  = (etaTCturbcalcTCME - etaTCturbidME) * 100/etaTCturbidME # Corresponding Diagnostics --> MissingdeltaetaTCturbME. Here its assumed that this Diagnostics is corresponding to delta. 
        #     deltaetaTCturbME  = np.divide((etaTCturbcalcTCME - etaTCturbidME) * 100, etaTCturbidME, out=np.full_like((etaTCturbcalcTCME - etaTCturbidME) * 100,float('nan')), where=etaTCturbidME!=0)
        #     DS['MissingdeltaetaTCturbME']=np.where(pd.isnull(deltaetaTCturbME), 1, 0)   
        
        # if 'AE' in LogType:
        #     SFOCisoAE = SDS['SFOCisoAE']
        #     DS['MissingSFOCcorAE']=np.where(pd.isnull(SFOCisoAE), 1, 0) 
        #     SFOCisoidAE = ODS['SFOCisoidAE']
        #     DS['MissingdeltaSFOCidAE']=np.where(pd.isnull(SFOCisoidAE), 1, 0) 
        #     #deltaSFOCisoAE = (SFOCisoAE - SFOCisoidAE) * 100/SFOCisoidAE
        #     deltaSFOCisoAE = np.divide((SFOCisoAE - SFOCisoidAE) * 100, SFOCisoidAE, out=np.full_like((SFOCisoAE - SFOCisoidAE) * 100,float('nan')), where=SFOCisoidAE!=0)
        #     DS['MissingdeltaSFOCAE']=np.where(pd.isnull(deltaSFOCisoAE), 1, 0) 
        #     NTCTCAE = SDS['NTCTCAE']
        #     DS['MissingNTCcorAE']=np.where(pd.isnull(NTCTCAE), 1, 0) 
        #     NTCidAE = ODS['NTCidAE']
        #     DS['MissingdeltaNTCidAE']=np.where(pd.isnull(NTCidAE), 1, 0) 
        #     #deltaNTCTCAE = (NTCTCAE - NTCidAE) * 100/NTCidAE
        #     deltaNTCTCAE = np.divide((NTCTCAE - NTCidAE) * 100, NTCidAE, out=np.full_like((NTCTCAE - NTCidAE) * 100,float('nan')), where=NTCidAE!=0)
        #     DS['MissingdeltaNTCAE']=np.where(pd.isnull(deltaNTCTCAE), 1, 0) 
        #     pscavAE = SDS['pscavAE']
        #     DS['MissingpscavcorAE']=np.where(pd.isnull(pscavAE), 1, 0) 
        #     pscavabsisoidAE = ODS['pscavabsisoidAE']
        #     DS['MissingdeltapscavidAE']=np.where(pd.isnull(pscavabsisoidAE), 1, 0) 
        #     #deltapscavAE = (pscavAE - pscavabsisoidAE) * 100/pscavabsisoidAE
        #     deltapscavAE =np.divide((pscavAE - pscavabsisoidAE) * 100, pscavabsisoidAE, out=np.full_like((pscavAE - pscavabsisoidAE) * 100,float('nan')), where=pscavabsisoidAE!=0)
        #     DS['MissingdeltapscavAE']=np.where(pd.isnull(deltapscavAE), 1, 0) 
        #     pmaxcylAE = SDS['pmaxcylAE']
        #     DS['MissingpmaxcorAE']=np.where(pd.isnull(pmaxcylAE), 1, 0) 
        #     pmaxabsisoidAE = ODS['pmaxabsisoidAE']
        #     DS['MissingdeltapmaxidAE']=np.where(pd.isnull(pmaxabsisoidAE), 1, 0) 
        #     #deltapmaxcylAE = (pmaxcylAE - pmaxabsisoidAE) * 100/pmaxabsisoidAE
        #     deltapmaxcylAE = np.divide((pmaxcylAE - pmaxabsisoidAE) * 100, pmaxabsisoidAE, out=np.full_like((pmaxcylAE - pmaxabsisoidAE) * 100,float('nan')), where=pmaxabsisoidAE!=0)
        #     DS['MissingdeltapmaxAE']=np.where(pd.isnull(deltapmaxcylAE), 1, 0) 
            
            
        # if LogType in 'Noon' or  LogType in 'ME':
            
        #     DS['MissingdeltapscavME']=np.where(pd.isnull(ODS['deltapscavabsisoME']), 1, 0)
        #     DS['MissingdeltapscavidME']=np.where(pd.isnull(ODS['pscavabsisoidME']), 1, 0)
        #     DS['MissingpscavcorME']=np.where(pd.isnull(SDS['pscavabsisoME']), 1, 0)
            
        #     #new
        #     NTCTCME = SDS['NTCTCME']
        #     DS['MissingNTCcorME']=np.where(pd.isnull(NTCTCME), 1, 0)
        #     if 'Noon' in LogType:
        #         NTCidME = ODS['NTCidME'][:,:,np.newaxis]
        #         DS['MissingdeltaNTCidME']=np.where(pd.isnull(NTCidME), 1, 0)
        #     else:
        #         NTCidME = ODS['NTCidME']
        #         DS['MissingdeltaNTCidME']=np.where(pd.isnull(NTCidME), 1, 0)
                
        #     # KPI DS based on the above.
        #     #deltaNTCTCME = (NTCTCME - NTCidME)*100/NTCidME
        #     deltaNTCTCME = np.divide((NTCTCME - NTCidME)*100, NTCidME, out=np.full_like((NTCTCME - NTCidME)*100,float('nan')), where=NTCidME!=0)
        #     DS['MissingdeltaNTCME']=np.where(pd.isnull(deltaNTCTCME), 1, 0)
        #     # TegEVoutcylME -- ME and Cyl specific data. 
        #     TegEVoutcylME = SDS['TegEVoutcylME'] # No Diagnostics for this parameter # This is not corrected needs correction using the new ISO Engine correction.
        #     if 'Noon' in LogType:
        #         TegEVoutisoidME = ODS['TegEVoutisoidME'][:,:,np.newaxis]
        #         DS['MissingdeltaTexhidME']=np.where(pd.isnull(TegEVoutisoidME), 1, 0)
        #     else:
        #         TegEVoutisoidME = ODS['TegEVoutisoidME']
        #         DS['MissingdeltaTexhidME']=np.where(pd.isnull(TegEVoutisoidME), 1, 0)
        #     # KPI DS based on the above.
        #     #deltaTegEVoutisoME_cylwise = (TegEVoutcylME - TegEVoutisoidME)*100/TegEVoutisoidME
        #     deltaTegEVoutisoME_cylwise =np.divide((TegEVoutcylME - TegEVoutisoidME)*100, TegEVoutisoidME, out=np.full_like((TegEVoutcylME - TegEVoutisoidME)*100,float('nan')), where=TegEVoutisoidME!=0)
        #     DS['MissingdeltaTexhME']=np.where(pd.isnull(deltaTegEVoutisoME_cylwise), 1, 0)
            
        # if LogType in 'Noon' or  LogType in 'AE':
        #     if 'Noon' in LogType:
        #         TegTCinAE = SDS['TegTCinAE'][:,:,np.newaxis]
        #         DS['MissingTegTCincorAE']=np.where(pd.isnull(TegTCinAE), 1, 0)
        #     else:
        #         TegTCinAE = SDS['TegTCinAE']
        #         DS['MissingTegTCincorAE']=np.where(pd.isnull(TegTCinAE), 1, 0)
        #     if 'Noon' in LogType:
        #         TegTCinisoidAE = ODS['TegTCinisoidAE'][:,:,np.newaxis]
        #         DS['MissingdeltaTegTCinidAE']=np.where(pd.isnull(TegTCinisoidAE), 1, 0)
        #     else:
        #         TegTCinisoidAE = ODS['TegTCinisoidAE']
        #         DS['MissingdeltaTegTCinidAE']=np.where(pd.isnull(TegTCinisoidAE), 1, 0)
        #     #deltaTegTCinisoAE = (TegTCinAE - TegTCinisoidAE)*100/TegTCinisoidAE
        #     deltaTegTCinisoAE = np.divide((TegTCinAE - TegTCinisoidAE)*100, TegTCinisoidAE, out=np.full_like((TegTCinAE - TegTCinisoidAE)*100,float('nan')), where=TegTCinisoidAE!=0)
        #     DS['MissingdeltaTegTCinAE']=np.where(pd.isnull(deltaTegTCinisoAE), 1, 0)
        #     if 'Noon' in LogType:
        #         TegTCoutAE = SDS['TegTCoutAE'][:,:,np.newaxis]
        #         DS['MissingTegTCoutcorAE']=np.where(pd.isnull(TegTCoutAE), 1, 0)
        #     else:
        #         TegTCoutAE = SDS['TegTCoutAE']
        #         DS['MissingTegTCoutcorAE']=np.where(pd.isnull(TegTCoutAE), 1, 0)
        #     if 'Noon' in LogType:
        #         TegTCoutisoidAE = ODS['TegTCoutisoidAE'][:,:,np.newaxis]
        #         DS['MissingdeltaTegTCoutidAE']=np.where(pd.isnull(TegTCoutisoidAE), 1, 0)
        #     else:
        #         TegTCoutisoidAE = ODS['TegTCoutisoidAE']
        #         DS['MissingdeltaTegTCoutidAE']=np.where(pd.isnull(TegTCoutisoidAE), 1, 0)
        #     #deltaTegTCoutisoAE = (TegTCoutAE - TegTCoutisoidAE)*100/TegTCoutisoidAE
        #     deltaTegTCoutisoAE = np.divide((TegTCoutAE - TegTCoutisoidAE)*100, TegTCoutisoidAE, out=np.full_like((TegTCoutAE - TegTCoutisoidAE)*100,float('nan')), where=TegTCoutisoidAE!=0)
        #     DS['MissingdeltaTegTCoutAE']=np.where(pd.isnull(deltaTegTCoutisoAE), 1, 0)
        #     TegEVoutcylAE = SDS['TegEVoutcylAE']
        #     DS['MissingTexhcorAE']=np.where(pd.isnull(TegEVoutcylAE), 1, 0)
        #     if 'Noon' in LogType:
        #         TegEVoutisoidAE = ODS['TegEVoutisoidAE'][:,:,np.newaxis]
        #         DS['MissingdeltaTexhidAE']=np.where(pd.isnull(TegEVoutisoidAE), 1, 0)
        #     else:
        #         TegEVoutisoidAE = ODS['TegEVoutisoidAE']
        #         DS['MissingdeltaTexhidAE']=np.where(pd.isnull(TegEVoutisoidAE), 1, 0)
        #     #deltaTegEVoutisoAE_cylwise = (TegEVoutcylAE - TegEVoutisoidAE)*100/TegEVoutisoidAE
        #     deltaTegEVoutisoAE_cylwise = np.divide((TegEVoutcylAE - TegEVoutisoidAE)*100, TegEVoutisoidAE, out=np.full_like((TegEVoutcylAE - TegEVoutisoidAE)*100,float('nan')), where=TegEVoutisoidAE!=0)
        #     DS['MissingdeltaTexhAE']=np.where(pd.isnull(deltaTegEVoutisoAE_cylwise), 1, 0)
            
            
        # if LogType in 'Noon' or  LogType in 'HP'or LogType in 'Auto':
        #     DS['MissingdeltaSFOCidME']=np.where(pd.isnull(ODS['SFOCisoidME']), 1, 0)
        #     #DS['MissingdeltaSFOCME']=np.where(pd.isnull(((SDS['SFOCisoME'] - ODS['SFOCisoidME'])*100/ODS['SFOCisoidME'])), 1, 0)
        #     DS['MissingdeltaSFOCME']=np.where(pd.isnull(np.divide((SDS['SFOCisoME'] - ODS['SFOCisoidME'])*100, ODS['SFOCisoidME'], out=np.full_like((SDS['SFOCisoME'] - ODS['SFOCisoidME'])*100,float('nan')), where=ODS['SFOCisoidME']!=0)), 1, 0)
        #     DS['MissingSFOCcorME']=np.where(pd.isnull(SDS['SFOCisoME']), 1, 0)
            
        #     if (SDS['PeffME'].ndim ==1) or (ODS['PeffLightidME'].ndim ==1) or (ODS['PeffLightidME'].ndim ==1):
        #         #DS['MissingActualpropellermargin']=np.where(pd.isnull(((SDS['PeffME'].ravel() - ODS['PeffLightidME'].ravel())*100/ODS['PeffLightidME'].ravel())), 1, 0)
        #         DS['MissingActualpropellermargin']=np.where(pd.isnull(np.divide((SDS['PeffME'].ravel() - ODS['PeffLightidME'].ravel())*100, ODS['PeffLightidME'].ravel(), out=np.full_like((SDS['PeffME'].ravel() - ODS['PeffLightidME'].ravel())*100,float('nan')), where=ODS['PeffLightidME'].ravel()!=0)), 1, 0)
        #     else:
        #         #DS['MissingActualpropellermargin']=np.where(pd.isnull(((SDS['PeffME'] - ODS['PeffLightidME'])*100/ODS['PeffLightidME'])), 1, 0)
        #         DS['MissingActualpropellermargin']=np.where(pd.isnull(np.divide((SDS['PeffME'] - ODS['PeffLightidME'])*100, ODS['PeffLightidME'], out=np.full_like((SDS['PeffME'] - ODS['PeffLightidME'])*100,float('nan')), where=ODS['PeffLightidME']!=0)), 1, 0)
                
           
            
            
        
            
        
    except Exception as err:
        exp = "Error in OutputFiltering {} in line No. {} for IMO {}".format(err,sys.exc_info()[-1].tb_lineno,IMO)
        logger.error(exp,exc_info=True)
        ExceptionHandler(LogType,exp)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)

        return({},{},SRQST)



    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(ODS,DS,SRQST)
