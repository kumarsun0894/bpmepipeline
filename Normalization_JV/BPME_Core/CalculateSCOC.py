# -*- coding: utf-8 -*-
import numpy as np
import sys
from BPME_Common import nansumwrapper as nsw
#Description:  This function calculates the SCOC for 2-stroke MEs.
import logging
# create logger
import time
from ExceptionHandler import ExceptionHandler
logger = logging.getLogger('BPMEngine.2.02')
def CalculateSCOC(IMO,SDS,LogType,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        if 'Noon' in LogType:
            # SDS['SCOCME']=1000*SDS['mCOCME']/nsw.nansumwrapper(SDS['PeffME']*SDS['RHME'],axis=1) #mean SCOC in [g/kWh] across all MEs
            # SDS['SCOCME']=1000*np.divide(SDS['mCOCME'], nsw.nansumwrapper(SDS['PeffME']*SDS['RHME'],axis=1), out=np.full_like(SDS['mCOCME'],float('nan')), where=nsw.nansumwrapper(SDS['PeffME']*SDS['RHME'],axis=1)!=0) #mean SCOC in [g/kWh] across all MEs
            SDS['SCOCME']=1000*np.divide(SDS['mCOCME'].astype('float64'), nsw.nansumwrapper(SDS['PeffME']*SDS['RHME'],axis=1).astype('float64'), out=np.full_like(SDS['mCOCME'].astype('float64'),float('nan')), where=nsw.nansumwrapper(SDS['PeffME']*SDS['RHME'],axis=1).astype('float64')!=0)    
    except Exception as err:
        exp = "Error in CalculateSCOC {} in line No. {} for IMO {}".format(err,sys.exc_info()[-1].tb_lineno,IMO)
        logger.error(exp,exc_info=True)
        ExceptionHandler(LogType,exp)
        SRQSTD['TotalTimeLapse']=time.time()-start_time              
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return({},SRQST)
        
    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time               
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(SDS,SRQST)
        
        
    
