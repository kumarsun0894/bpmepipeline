# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 20:57:48 2020

@author: Subhin Antony

% P_LI
% Description:  This function estimates the effective power based on the
%               indicated pressure according to MAN manuals.
%
% Input:        pind    [bar]   indicated pressure (vector)
%               N       [RPM]   engine speed (vector)
%               k1      [bar]   mean friction loss (scalar)
%               k2      [-]     cylinder constant as defined by MAN (scalar)
%               nc      [-]     number of cylinders (scalar)
%
% Output:       P       [kW]    estimated effective power (vector)
%
% See also:     PowerEstimation, P_SFOC, P_FPI, P_NTC

"""

def P_LI(pind,N,k1,k2,nc):
    peff=pind-k1       #mean effective pressure in [bar]
    Peff=nc*k2*N*peff  #effective power in [kW]
    
    return(Peff)