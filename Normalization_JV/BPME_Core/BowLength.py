# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 13:26:10 2020

@author: SubhinAntony

% BowLength
% Description:  This function calculates the distance of the bow to 95% of
%               maximum breadth on the waterline according to the
%               definition in ISO15016:2015, Annex D.1.
%
% Input:        Lbp     [m]     length between perpendiculars (scalar)
%
% Output:       Lbwl    [m]     estimated bow length on the waterline (scalar)
%
% Note 1:       Based on FPC's experience so far (29/11/2018) this variable
%               is hard to define based on drawings. Therefore, it is
%               decided to use an approximation by default as 25%Lbp.
%
% Note 2:       A beter approximation could be taking percentage of the
%               waterline length (Lwl) instead of percentage of the Lbp.
%               This cna be developed in the future if necessary.
%
% See also:     STAWAVE1

"""

def BowLength(Lbp):
    Lbwl=0.25*Lbp
    return(Lbwl)