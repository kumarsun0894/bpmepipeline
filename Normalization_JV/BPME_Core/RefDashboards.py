# -*- coding: utf-8 -*-
"""
Created on Tue May  5 10:17:11 2020

@author: SunKumar
@Delta:SunKumar

% RefDashboards
% Description:  This function is used to generate a series of reference
%               curves to be displayed in PAL.
%
% Input:        RDS
%
% Output:       RefDashStruct
%
% See also:     HPOutputExport

"""
import sys
import time
import logging

# create logger
logger = logging.getLogger('BPMEngine.2.02')
import numpy as np
import pandas as pd
import numpy.matlib
from BPME_Common import WaterLineLength as wll
from BPME_Common import WaterPlaneArea as wpa
from BPME_Common import BowSectionArea as bsa
from BPME_Common import TransomArea as tsa
from BPME_Common import RudderSurfaceArea as rsa
from BPME_Common import CenterOfBuoyancy as cob
from BPME_Common import MidshipSectionArea as msa
from BPME_Common import WettedSurfaceArea as wsa
from BPME_Common import SeawaterViscosity as vss
from BPME_Common import SeawaterDensity as dss
from BPME_Common import IdealDeliveredPowerHoltrop as idph
from BPME_Common import IdealDeliveredPower as idp
from BPME_Common import IdealFOC as idf
from BPME_Common import IdealRPMHoltrop as idrh
from BPME_Common import IdealRPM as idr
from BPME_Common import IdealSFOC as idsf
from BPME_Common import IdealST as idst
from BPME_Core import EngineLoadDiagram as eld
from BPME_Common import IdealSpeed as ids
from BPME_Core import CPCurvesNormalization as ccn
from ExceptionHandler import ExceptionHandler


def RefDashboards(records_df, start_time, SRQST):
    SRQSTD = {}
    SRQSTD['StepTimeLapse'] = time.time() - start_time
    SRQSTD['Stage'] = __name__
    rd_status = -1
    try:
        for i, v in records_df.iterrows():
            try:

                RDS = v['RDS']

                ###################################################################################
                # Define structure dimensions                                                      #
                ###################################################################################
                RDashS = {}
                # Define available reference draughts for dashboards

                tr1 = np.array([RDS['Tb'], RDS['Td'], RDS['Ts'], np.min(RDS['Tref']), np.max(RDS['Tref'])])
                tr2 = RDS['TmCP'].astype('float64')
                tr3 = np.array([np.arange(np.ceil(RDS['Tb'] / 0.5) * 0.5, RDS['Ts'], 0.5)]).ravel()
                tr4 = np.array([np.arange(np.ceil(np.min(RDS['Tref']) / 0.5) * 0.5, np.max(RDS['Tref']), 0.5)]).ravel()

                tr = np.round(np.concatenate((tr1, tr2, tr3, tr4)), 2)
                TrefTemp = np.sort(tr[~(np.triu(np.abs(tr[:, None] - tr) <= 0.001, 1)).any(0)])

                if RDS['UCP'].ndim == 1:
                    RDS['UCP'] = RDS['UCP'].reshape(1, -1)
                if RDS['FOCMECP'].ndim == 1:
                    RDS['FOCMECP'] = RDS['FOCMECP'].reshape(1, -1)

                #    tr = np.sort(np.round(np.concatenate((tr1,tr2,tr3)),2))
                #    TrefTemp=np.sort(tr[~(np.triu(np.abs(tr[:,None] - tr) <= 0.001 * np.max(np.abs(tr)),1)).any(0)])

                #     TrefTemp = np.array([7.45,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,13.9])

                # Define speed range that corresponds to 10-90% MCR for dashboards
                UrefTemp = np.arange(RDS['Uminref'], RDS['Umaxref'] + 1).T

                # Define CP speed vectors
                UCPTemp = RDS['UCP'].ravel()
                UCPTemp = UCPTemp[~pd.isnull(UCPTemp)]

                # #Define CP speed vectors
                # if RDS['FOCMECP'].ndim==2:
                #     RDS['FOCMECP']=RDS['FOCMECP'][:,~np.any(np.isnan(RDS['FOCMECP']), axis=0)]
                # if RDS['UCP'].ndim==2:
                #     RDS['UCP']=RDS['UCP'][:,~np.any(np.isnan(RDS['UCP']), axis=0)]
                # UCPTemp=RDS['UCP'].T.reshape((np.size(RDS['FOCMECP']),1),order='F')#matrix to vector
                # rem=np.flatnonzero(np.isnan(UCPTemp))
                # UCPTemp=np.delete(UCPTemp,rem,axis=0) #remove nans

                # Engine Layout
                [NLayoutMEtemp, PeffLayoutMEtemp] = eld.EngineLoadDiagram(np.mean(RDS['PMCRME']),
                                                                          np.mean(RDS['NMCRME']))

                if UrefTemp.ndim == 1:
                    UrefTemp = UrefTemp.reshape(1, -1).T
                #     if TrefTemp.ndim == 1:
                #         TrefTemp = TrefTemp.reshape(1,-1).T

                # Find lengths of all groups of data
                HPLength = (np.tile(UrefTemp, ((TrefTemp).size, 1))).size
                CPLength = len(UCPTemp)
                MELength = (np.tile(np.arange(10, 101, 5).T, (int(RDS['nME']), 1))).size
                AELength = (np.tile(np.arange(10, 101, 5).T, (int(RDS['nAE']), 1))).size
                LOLength = (NLayoutMEtemp).size

                StructHeight = np.max(np.array([HPLength, CPLength, MELength, AELength, LOLength]))
                # StructWidth=28; (5-HP, 3-CP, 2-LO, 6-AE, 12-ME)

                # Memory pre-allocation
                RDashS['UrefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['TrefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['PDrefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['FOCMErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['NproprefDash'] = np.full((StructHeight, 1), np.nan)

                RDashS['TCPDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['UCPDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['FOCMECPDash'] = np.full((StructHeight, 1), np.nan)

                RDashS['MCRMErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['NoMErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['SFOCisoMErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['NMErefDash'] = np.full((StructHeight, 1), np.nan)

                RDashS['NLightMErefDash'] = np.full((StructHeight, 1), np.nan)

                RDashS['PeffMErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['pindMErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['pscavabsisoMErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['pmaxabsisoMErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['pcompabsisoMErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['TegEVoutisoMErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['NTCMErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['NLayoutMErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['PeffLayoutMErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['MCRAErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['NoAErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['SFOCisoAErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['TegEVoutisoAErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['TegTCinisoAErefDash'] = np.full((StructHeight, 1), np.nan)
                RDashS['TegTCoutisoAErefDash'] = np.full((StructHeight, 1), np.nan)
                ###########################################################################
                # HP Curves                                               #
                ###########################################################################
                RDashS['UrefDash'][0:HPLength, 0] = np.ravel(np.tile(UrefTemp, ((TrefTemp).size, 1)), order='F')
                RDashS['TrefDash'][0:HPLength, 0] = np.ravel(
                    np.reshape(np.ravel(np.tile(TrefTemp, ((UrefTemp).size, 1)), order='F'),
                               (RDashS['UrefDash'][0:HPLength]).shape), order='F')

                # Define Holtrop reference power for the reference curves
                Ta = RDashS['TrefDash']

                Tf = RDashS['TrefDash']

                Tm = ((Ta + Tf) / 2).ravel()
                DISPV = idst.IdealST(RDS['Th'], RDS['Vh'], Tm, 0)
                STW = RDashS['UrefDash']

                [Lwl, xLwl0_5, _] = wll.WaterLineLength(Ta, Tf, RDS['Lbp'], RDS['HullProfile'].T)
                [_, Cwp] = wpa.WaterPlaneArea(Tm, Lwl, RDS['B'], RDS['Th'], RDS['Cwph'])
                [Ab, hb, _, _] = bsa.BowSectionArea(Tf, RDS['BowSection'].T, RDS['BulbousBowVessel'])
                Atr, _, _ = tsa.TransomArea(Ta, Tf, RDS['Lbp'], RDS['B'], RDS['HullProfile'].T, RDS['TransomSection'].T,
                                            RDS['TransomSternVessel'])
                Ar, _, _, _ = rsa.RudderSurfaceArea(Ta, RDS['Lbp'], RDS['RudderProfile'].T)
                [_, lcb] = cob.CenterOfBuoyancy(Tm, Lwl, xLwl0_5, RDS['Th'], RDS['LCBh'])
                Cb = DISPV / (Lwl * RDS['B'] * Tm)  # block coefficient in [-]
                [_, Cm] = msa.MidshipSectionArea(Tm, RDS['B'], RDS['Th'],
                                                 RDS['Cmh'])  # midship section area under water in [m2]
                WSA = wsa.WettedSurfaceArea(Tm, Lwl, DISPV, Cm, Cwp, Ab, RDS['Th'], RDS['WSAh'],
                                            RDS['B'])  # wetted surface area in [m2]

                vsw = vss.SeawaterViscosity(15 * np.ones(Tm.shape), 35.16504 * np.ones(Tm.shape))
                rhosw = dss.SeawaterDensity(15 * np.ones(Tm.shape), 35.16504 * np.ones(Tm.shape))

                if STW.ndim == 2:
                    STW = STW.ravel()
                if Tf.ndim == 2:
                    Tf = Tf.ravel()
                if WSA.ndim == 2:
                    WSA = WSA.ravel()
                if lcb.ndim == 2:
                    lcb = lcb.ravel()
                if Ar.ndim == 2:
                    Ar = Ar.ravel()
                if Ab.ndim == 2:
                    Ab = Ab.ravel()
                if hb.ndim == 2:
                    hb = hb.ravel()
                if Atr.ndim == 2:
                    Atr = Atr.ravel()

                PDHid = idph.IdealDeliveredPowerHoltrop(STW, Tm, Tf, Lwl, DISPV, WSA, Cb, Cm, Cwp, lcb, rhosw, vsw, Ar,
                                                        Ab, hb, Atr, RDS)

                ###########################################################################
                # Create reference vectors according to MariApps preferred format         #
                ###########################################################################

                # Delivered power
                RDashS['PDrefDash'] = np.round(
                    idp.IdealDeliveredPower(RDashS['UrefDash'], RDashS['TrefDash'], PDHid, RDS['Tref'], RDS['Pref'],
                                            RDS['Uref'], RDS['Tb'], RDS['Ts']))

                # Fuel Consumption
                # This is the total FOC for the whole vessel thus, we multiply for each engine
                # RDashS.FOCMErefDash=IdealFOC(RDashS.PDrefDash,RDS.FOCMEST,RDS.PeffMEST,RDS.LCVMEST,RDS.LCVHFOref,RDS.etaT,RDS.etaeff);

                # Conventional engines
                if len(np.where(RDS['DualFuelME'] == 'No')[0]) == len(RDS['DualFuelME']):
                    RDashS['FOCMErefDash'] = np.round(idf.IdealFOC(RDashS['PDrefDash'],
                                                                   RDS['FOCMEST'][0, :] * int(RDS['nME']),
                                                                   RDS['PeffMEST'][0, :] * int(RDS['nME']),
                                                                   RDS['LCVFOMEST'][0],
                                                                   RDS['LCVHFOref'],
                                                                   RDS['etaT'],
                                                                   RDS['etaeffME'][0]), 2)

                # DF engines
                elif len(np.where(RDS['DualFuelME'] == 'Yes')[0]) == len(RDS['DualFuelME']):

                    # Temporarily since only a single curve should be produced, we use only
                    # the gas mode. How to show more accurate info in UI is somtething that
                    # needs to be designed and developed.

                    # Pilot
                    POCMErefDash = np.round(idf.IdealFOC(RDashS['PDrefDash'].ravel(),
                                                         RDS['FOCMEST'][RDS['RepMEGO'], :] * int(RDS['nME']),
                                                         RDS['PeffMEST'][RDS['RepMEGO'], :] * int(RDS['nME']),
                                                         RDS['LCVFOMEST'][RDS['RepMEGO']],
                                                         RDS['LCVHFOref'],
                                                         RDS['etaT'],
                                                         RDS['etaeffME'][int(RDS['NoME'][RDS['RepMEGO']])]), 2)

                    # Gas
                    GOCMErefDash = np.round(idf.IdealFOC(RDashS['PDrefDash'].ravel(),
                                                         RDS['GOCMEST'][RDS['RepMEGO'], :] * int(RDS['nME']),
                                                         RDS['PeffMEST'][RDS['RepMEGO'], :] * int(RDS['nME']),
                                                         RDS['LCVGOMEST'][RDS['RepMEGO']],
                                                         RDS['LCVHFOref'],
                                                         RDS['etaT'],
                                                         RDS['etaeffME'][int(RDS['NoME'][RDS['RepMEGO']])]), 2)

                    RDashS['FOCMErefDash'] = np.round(GOCMErefDash + POCMErefDash, 2)

                # Propeller Speed
                # Here we must think if we need per propeller or a mean value is sufficient
                # NHid=IdealRPMHoltrop(RDashS.PDrefDash,RDS.NMEST,RDS.PeffMEST,RDS.etaT,RDS.etaeff,RDS.factorN2);
                NHid = idrh.IdealRPMHoltrop(RDashS['PDrefDash'],
                                            RDS['NMEST'][0, :],
                                            RDS['PeffMEST'][0, :] * int(RDS['nME']),  # for multiple engines
                                            RDS['etaT'],
                                            RDS['etaeffME'][0],
                                            RDS['factorN2'],
                                            RDS)
                RDashS['NproprefDash'] = np.round(idr.IdealRPM(RDashS['UrefDash'],
                                                               RDashS['TrefDash'], NHid, RDS['Tref'], RDS['Nref'],
                                                               RDS['Uref'], RDS['Tb'],
                                                               RDS['Ts']), 2)

                ##############################################################################
                # CP Curves                                                                  #
                ################################################################################

                # Removing rows containing NaN from UCP,FOCMECP and also removing NaN from TmCP and passing it to CPCurvesNormalization.
                # This is done under the assumption that we will get two rows of data for UCP and FOCMECP in all the cases filled with NaN atleast.

                RDS['UCP'] = RDS['UCP'][~np.isnan(RDS['UCP']).all(axis=1)]
                RDS['FOCMECP'] = RDS['FOCMECP'][~np.isnan(RDS['FOCMECP']).all(axis=1)]
                RDS['TmCP'] = RDS['TmCP'][~np.isnan(RDS['TmCP'])]

                # [RDashS['UCPDash'][0:CPLength,0],RDashS['FOCMECPDash'][0:CPLength,0],RDashS['TCPDash'][0:CPLength,0]]=ccn.CPCurvesNormalization(RDS)
                [UCPtempnorm, FOCMECPtempnorm, TCPtemnorm] = ccn.CPCurvesNormalization(RDS)
                RDashS['UCPDash'][0:CPLength, 0] = np.around(UCPtempnorm, 2)
                RDashS['FOCMECPDash'][0:CPLength, 0] = np.around(FOCMECPtempnorm, 2)
                RDashS['TCPDash'][0:CPLength, 0] = np.around(TCPtemnorm, 2)

                ##############################################################################
                # ME Curves                                                                  #
                ##############################################################################
                # Engine Load
                RDashS['MCRMErefDash'][0:MELength, 0] = np.ravel(np.tile(np.arange(10, 101, 5).T, (int(RDS['nME']), 1)))

                # Engine No
                inp = np.arange(1, RDS['nME'] + 1).T
                if inp.ndim == 1:
                    inp = inp.reshape(1, -1).T

                RDashS['NoMErefDash'][0:MELength, 0] = np.ravel(np.reshape(np.ravel(
                    np.tile(inp.reshape(1, -1), (int(RDashS['MCRMErefDash'][0:MELength].size / RDS['nME']), 1)),
                    order='F'), RDashS['MCRMErefDash'][0:MELength].shape))

                # Per Engine Reference Curves

                # Conventional engines
                SFOCMEtemp = []
                NMETemp = []
                PeffMETemp = []
                pindMETemp = []
                pscavabsisoMETemp = []
                pmaxabsisoMETemp = []
                pcompabsisoMETemp = []
                TegEVoutisoMETemp = []
                NTCMETemp = []
                SPOCMEtemp = []
                SGOCMEtemp = []
                x = np.arange(10, 101, 5)
                if x.ndim == 1:
                    x = x.reshape(1, -1).T

                if len(np.where(RDS['DualFuelME'] == 'No')[0]) == len(RDS['DualFuelME']):

                    for i in range(0, int(RDS['nME'])):
                        SFOCMEtemp.append(idsf.IdealSFOC(x, RDS['SFOCisocalcMEST'][i, :], RDS['MCRMEST'][i, :]))
                        NMETemp.append(idst.IdealST(RDS['MCRMEST'][i, :], RDS['NMEST'][i, :], x, 0))
                        PeffMETemp.append(idst.IdealST(RDS['MCRMEST'][i, :], RDS['PeffMEST'][i, :], x, 0))
                        pindMETemp.append(idst.IdealST(RDS['MCRMEST'][i, :], RDS['pindMEST'][i, :], x, 0))
                        pscavabsisoMETemp.append(
                            idst.IdealST(RDS['MCRMEST'][i, :], RDS['pscavabsisocalcMEST'][i, :], x, 0))
                        pmaxabsisoMETemp.append(
                            idst.IdealST(RDS['MCRMEST'][i, :], RDS['pmaxabsisocalcMEST'][i, :], x, 0))
                        pcompabsisoMETemp.append(
                            idst.IdealST(RDS['MCRMEST'][i, :], RDS['pcompabsisocalcMEST'][i, :], x, 0))
                        TegEVoutisoMETemp.append(
                            idst.IdealST(RDS['MCRMEST'][i, :], RDS['TegEVoutisocalcMEST'][i, :], x, 100))
                        NTCMETemp.append(
                            idst.IdealST(RDS['pscavabsisocalcMEST'][i, :], RDS['NTCMEST'][i, :], pscavabsisoMETemp[i],
                                         0))
                    SFOCMEtemp = np.array(SFOCMEtemp).T
                    NMETemp = np.array(NMETemp).T
                    PeffMETemp = np.array(PeffMETemp).T
                    pindMETemp = np.array(pindMETemp).T
                    pscavabsisoMETemp = np.array(pscavabsisoMETemp).T
                    pmaxabsisoMETemp = np.array(pmaxabsisoMETemp).T
                    pcompabsisoMETemp = np.array(pcompabsisoMETemp).T
                    TegEVoutisoMETemp = np.array(TegEVoutisoMETemp).T
                    NTCMETemp = np.array(NTCMETemp).T


                # Dual fuel engines
                elif len(np.where(RDS['DualFuelME'] == 'Yes')[0]) == len(RDS['DualFuelME']):
                    # Temporarily since only a single curve should be produced, we use only
                    # the gas mode. How to show more accurate info in UI is somtething that
                    # needs to be designed and developed.
                    checkval = np.where(RDS['FuelModeMEST'] == 'G')
                    checkval = np.array(checkval).T
                    checkval = checkval.ravel()

                    SGOCMEtemp = []

                    for i in checkval:
                        ii = int(RDS['NoME'][i]) - 1
                        SPOCMEtemp.insert(ii,
                                          idsf.IdealSFOC(x, RDS['SFOCisocalcMEST'][i], RDS['MCRMEST'][i, :]).ravel())
                        SGOCMEtemp.insert(ii, idsf.IdealSFOC(x, RDS['SGOCisocalcMEST'][i], RDS['MCRMEST'][i]).ravel())

                        NMETemp.insert(ii, idst.IdealST(RDS['MCRMEST'][i, :], RDS['NMEST'][i, :], x, 0))
                        PeffMETemp.insert(ii, idst.IdealST(RDS['MCRMEST'][i, :], RDS['PeffMEST'][i, :], x, 0))
                        pindMETemp.insert(ii, idst.IdealST(RDS['MCRMEST'][i, :], RDS['pindMEST'][i, :], x, 0))
                        pscavabsisoMETemp.insert(ii,
                                                 idst.IdealST(RDS['MCRMEST'][i, :], RDS['pscavabsisocalcMEST'][i, :], x,
                                                              0))
                        pmaxabsisoMETemp.insert(ii,
                                                idst.IdealST(RDS['MCRMEST'][i, :], RDS['pmaxabsisocalcMEST'][i, :], x,
                                                             0))
                        pcompabsisoMETemp.insert(ii,
                                                 idst.IdealST(RDS['MCRMEST'][i, :], RDS['pcompabsisocalcMEST'][i, :], x,
                                                              0))
                        TegEVoutisoMETemp.insert(ii,
                                                 idst.IdealST(RDS['MCRMEST'][i, :], RDS['TegEVoutisocalcMEST'][i, :], x,
                                                              100))
                        NTCMETemp.insert(ii, idst.IdealST(RDS['pscavabsisocalcMEST'][i, :], RDS['NTCMEST'][i, :],
                                                          pscavabsisoMETemp[ii], 0))

                    SPOCMEtemp = np.array(SPOCMEtemp).T
                    SGOCMEtemp = np.array(SGOCMEtemp).T
                    SFOCMEtemp = np.ravel((SPOCMEtemp + SGOCMEtemp), order='F')
                    # SFOCtemp = np.array(SFOCtemp).T
                    NMETemp = np.ravel(np.array(NMETemp).T, order='F')
                    PeffMETemp = np.ravel(np.array(PeffMETemp).T, order='F')
                    pindMETemp = np.ravel(np.array(pindMETemp).T, order='F')
                    pscavabsisoMETemp = np.ravel(np.array(pscavabsisoMETemp).T, order='F')
                    pmaxabsisoMETemp = np.ravel(np.array(pmaxabsisoMETemp).T, order='F')
                    pcompabsisoMETemp = np.ravel(np.array(pcompabsisoMETemp).T, order='F')
                    TegEVoutisoMETemp = np.ravel(np.array(TegEVoutisoMETemp).T, order='F')
                    NTCMETemp = np.ravel(np.array(NTCMETemp).T, order='F')

                RDashS['SFOCisoMErefDash'][0:MELength, 0] = np.ravel(
                    np.round(np.reshape(SFOCMEtemp.astype('float64'), (RDashS['MCRMErefDash'][0:MELength].shape)), 2))
                RDashS['NMErefDash'][0:MELength, 0] = np.ravel(
                    np.round(np.reshape(NMETemp, RDashS['MCRMErefDash'][0:MELength].shape), 2))
                RDashS['PeffMErefDash'][0:MELength, 0] = np.ravel(
                    np.round(np.reshape(PeffMETemp, RDashS['MCRMErefDash'][0:MELength].shape)))
                RDashS['pindMErefDash'][0:MELength, 0] = np.ravel(
                    np.round(np.reshape(pindMETemp, RDashS['MCRMErefDash'][0:MELength].shape), 2))
                RDashS['pscavabsisoMErefDash'][0:MELength, 0] = np.ravel(
                    np.round(np.reshape(pscavabsisoMETemp, RDashS['MCRMErefDash'][0:MELength].shape), 2))
                RDashS['pmaxabsisoMErefDash'][0:MELength, 0] = np.ravel(
                    np.round(np.reshape(pmaxabsisoMETemp, RDashS['MCRMErefDash'][0:MELength].shape), 1))
                RDashS['pcompabsisoMErefDash'][0:MELength, 0] = np.ravel(
                    np.round(np.reshape(pcompabsisoMETemp, RDashS['MCRMErefDash'][0:MELength].shape), 1))
                RDashS['TegEVoutisoMErefDash'][0:MELength, 0] = np.ravel(
                    np.round(np.reshape(TegEVoutisoMETemp, RDashS['MCRMErefDash'][0:MELength].shape)))
                RDashS['NTCMErefDash'][0:MELength, 0] = np.ravel(
                    np.round(np.reshape(NTCMETemp, RDashS['MCRMErefDash'][0:MELength].shape)))

                # Light Running Curve
                PDrefDash_at_min_Tref = RDashS['PDrefDash'][RDashS['TrefDash'] == np.nanmin(RDashS['TrefDash'])]
                NproprefDash_at_min_Tref = RDashS['NproprefDash'][RDashS['TrefDash'] == np.nanmin(RDashS['TrefDash'])]
                # RDashS.NLightMErefDash=round(IdealST(PDrefDash_at_Tb/(RDS.nME*RDS.etaT*RDS.etaeffME(1)),NproprefDash_at_Tb,RDashS.PeffMErefDash,0),2);
                if np.all(np.isnan(NproprefDash_at_min_Tref)):
                    RDashS['NLightMErefDash'] = np.empty(RDashS['PeffMErefDash'].shape)
                    RDashS['NLightMErefDash'][:] = np.nan

                else:
                    RDashS['NLightMErefDash'] = np.around(
                        ids.IdealSpeed(RDashS['PeffMErefDash'], np.ones(np.shape(RDashS['PeffMErefDash'])),
                                       np.full(np.shape(RDashS['PeffMErefDash']), np.nan), np.array([1]),
                                       PDrefDash_at_min_Tref.T / (RDS['nME'] * RDS['etaT'] * RDS['etaeffME'][0]),
                                       NproprefDash_at_min_Tref.T, 1, 1), 2)

                # SFOC Comment
                # currently there is a single source of FOC for every engine. this approach
                # do not let SFOC calculation per engine but per vessel. This must be improved
                # RDashS.SFOCisoMErefDash=IdealSFOC(RDashS.MCRMErefDash,RDS.SFOCisocalcMEST,RDS.MCRMEST);

                # Engine Layout Diagram

                RDashS['NLayoutMErefDash'][0:LOLength, 0] = np.round(NLayoutMEtemp, 2)
                RDashS['PeffLayoutMErefDash'][0:LOLength, 0] = np.round(PeffLayoutMEtemp)

                ###########################################################################
                # AE curves                                                               #
                ###########################################################################
                # Engine Load
                RDashS['MCRAErefDash'][0:AELength, 0] = np.ravel(np.tile(np.arange(10, 101, 5).T, (int(RDS['nAE']), 1)))

                # Engine No
                inp1 = np.arange(1, RDS['nAE'] + 1)
                #     if inp1.ndim ==1:
                #         inp1 = inp.reshape(1,-1).T
                RDashS['NoAErefDash'][0:AELength, 0] = np.ravel(np.reshape(
                    np.ravel(np.tile(inp1.T, (int(RDashS['MCRAErefDash'][0:AELength].size / RDS['nAE']), 1)),
                             order='F'), RDashS['MCRAErefDash'][0:AELength].shape))

                # memory pre-allocation
                SPOCAEtemp = []
                SGOCAEtemp = []
                SFOCAEtemp = np.full((x.size, int(RDS['nAE'])), np.nan)
                TegEVoutisoAEtemp = np.full((x.size, int(RDS['nAE'])), np.nan)
                TegTCinisoAEtemp = np.full((x.size, int(RDS['nAE'])), np.nan)
                TegTCoutisoAEtemp = np.full((x.size, int(RDS['nAE'])), np.nan)

                # Define curves only for modelled AEs
                if RDS['AEVerModel'] == 'Y':
                    # Conventional engines
                    if len(np.where(RDS['DualFuelAE'] == 'No')[0]) == len(RDS['DualFuelAE']):
                        for i in range(0, int(RDS['nAE'])):

                            # if(np.any(np.isnan(RDS['MCRAEST'][i,:]))):
                            #                    remcalc=np.flatnonzero(np.isnan(RDS['MCRAEST'][i,:]))
                            if (np.all(np.isnan(RDS['SFOCisocalcAEST'][i, :]))):
                                print("SFOCAETemp Cannot be Calculated")
                            elif ((np.isnan(RDS['SFOCisocalcAEST'][i, :]).sum()) < len(RDS['SFOCisocalcAEST'][i, :])):
                                remcalc = np.flatnonzero(np.isnan(RDS['SFOCisocalcAEST'][i, :]))
                                SFOCAEtemp[:, i] = np.ravel(
                                    idsf.IdealSFOC(x, np.delete(RDS['SFOCisocalcAEST'][i, :], remcalc),
                                                   np.delete(RDS['MCRAEST'][i, :], remcalc)), order='F')
                            else:
                                SFOCAEtemp[:, i] = np.ravel(
                                    idsf.IdealSFOC(x, RDS['SFOCisocalcAEST'][i, :], RDS['MCRAEST'][i, :]), order='F')
                            #                SFOCAEtemp[:,i]=np.ravel(idsf.IdealSFOC(x,RDS['SFOCisocalcAEST'][i,:],RDS['MCRAEST'][i,:]),order ='F')

                            TegEVoutisoAEtemp[:, i] = np.reshape(
                                idst.IdealST(RDS['MCRAEST'][i, :], RDS['TegEVoutisocalcAEST'][i, :], x, 100),
                                TegEVoutisoAEtemp[:, i].shape)
                            TegTCinisoAEtemp[:, i] = np.reshape(
                                idst.IdealST(RDS['MCRAEST'][i, :], RDS['TegTCinisocalcAEST'][i, :], x, 120),
                                TegTCinisoAEtemp[:, i].shape)
                            TegTCoutisoAEtemp[:, i] = np.reshape(
                                idst.IdealST(RDS['MCRAEST'][i, :], RDS['TegTCoutisocalcAEST'][i, :], x, 100),
                                TegTCoutisoAEtemp[:, i].shape)

                    # Dual fuel engines
                    elif len(np.where(RDS['DualFuelAE'] == 'Yes')[0]) == len(RDS['DualFuelAE']):
                        # Temporarily since only a single curve should be produced, we use only
                        # the gas mode. How to show more accurate info in UI is somtething that
                        # needs to be designed and developed.
                        checkvalAE = np.where(RDS['FuelModeMEST'] == 'G')
                        checkvalAE = np.array(checkval).T
                        checkvalAE = checkval.ravel()
                        for i in checkvalAE:
                            ii = int(RDS['NoAE'][i]) - 1
                            SPOCAEtemp.insert(ii, idsf.IdealSFOC(x, SGOCAEtemp, RDS['MCRAEST'][i, :]).ravel())
                            SGOCAEtemp.insert(ii,
                                              idsf.IdealSFOC(x, RDS['SGOCisocalcAEST'][i], RDS['MCRAEST'][i]).ravel())
                            TegEVoutisoAEtemp[:, ii] = idst.IdealST(RDS['MCRAEST'][i, :],
                                                                    RDS['TegEVoutisocalcAEST'][i, :], x, 100)
                            TegTCinisoAEtemp[:, ii] = idst.IdealST(RDS['MCRAEST'][i, :],
                                                                   RDS['TegTCinisocalcAEST'][i, :], x, 120)
                            TegTCoutisoAEtemp[:, ii] = idst.IdealST(RDS['MCRAEST'][i, :],
                                                                    RDS['TegTCoutisocalcAEST'][i, :], x, 100)
                        SFOCAEtemp = (SPOCAEtemp + SGOCAEtemp)

                RDashS['SFOCisoAErefDash'][0:AELength, 0] = np.ravel(
                    np.round(np.reshape(np.ravel(SFOCAEtemp, order='F'), (RDashS['MCRAErefDash'][0:AELength]).shape),
                             2))
                RDashS['TegEVoutisoAErefDash'][0:AELength, 0] = np.ravel(np.round(
                    np.reshape(np.ravel(TegEVoutisoAEtemp, order='F'), (RDashS['MCRAErefDash'][0:AELength]).shape)))
                RDashS['TegTCinisoAErefDash'][0:AELength, 0] = np.ravel(np.round(
                    np.reshape(np.ravel(TegTCinisoAEtemp, order='F'), (RDashS['MCRAErefDash'][0:AELength]).shape)))
                RDashS['TegTCoutisoAErefDash'][0:AELength, 0] = np.ravel(np.round(
                    np.reshape(np.ravel(TegTCoutisoAEtemp, order='F'), (RDashS['MCRAErefDash'][0:AELength]).shape)))

                # SFOC Comment
                # currently there is a single source of FOC for every engine. this approach
                # do not let SFOC calculation per engine but per vessel. This must be improved
                # RDashS.SFOCisoMErefDash=IdealSFOC(RDashS.MCRMErefDash,RDS.SFOCisocalcMEST,RDS.MCRMEST);

                #
                RDashS['UrefDash'] = np.ravel(RDashS['UrefDash'])
                RDashS['TrefDash'] = np.ravel(RDashS['TrefDash'])
                RDashS['PDrefDash'] = np.ravel(RDashS['PDrefDash'])
                RDashS['FOCMErefDash'] = np.ravel(RDashS['FOCMErefDash'])
                RDashS['NproprefDash'] = np.ravel(RDashS['NproprefDash'])
                RDashS['MCRMErefDash'] = np.ravel(RDashS['MCRMErefDash'])
                RDashS['NoMErefDash'] = np.ravel(RDashS['NoMErefDash'])
                RDashS['SFOCisoMErefDash'] = np.ravel(RDashS['SFOCisoMErefDash'])
                RDashS['NMErefDash'] = np.ravel(RDashS['NMErefDash'])
                RDashS['PeffMErefDash'] = np.ravel(RDashS['PeffMErefDash'])
                RDashS['pindMErefDash'] = np.ravel(RDashS['pindMErefDash'])
                RDashS['pscavabsisoMErefDash'] = np.ravel(RDashS['pscavabsisoMErefDash'])
                RDashS['pmaxabsisoMErefDash'] = np.ravel(RDashS['pmaxabsisoMErefDash'])
                RDashS['pcompabsisoMErefDash'] = np.ravel(RDashS['pcompabsisoMErefDash'])
                RDashS['TegEVoutisoMErefDash'] = np.ravel(RDashS['TegEVoutisoMErefDash'])
                RDashS['NTCMErefDash'] = np.ravel(RDashS['NTCMErefDash'])
                RDashS['NLayoutMErefDash'] = np.ravel(RDashS['NLayoutMErefDash'])
                RDashS['PeffLayoutMErefDash'] = np.ravel(RDashS['PeffLayoutMErefDash'])
                RDashS['MCRAErefDash'] = np.ravel(RDashS['MCRAErefDash'])
                RDashS['NoAErefDash'] = np.ravel(RDashS['NoAErefDash'])
                RDashS['SFOCisoAErefDash'] = np.ravel(RDashS['SFOCisoAErefDash'])
                RDashS['TegEVoutisoAErefDash'] = np.ravel(RDashS['TegEVoutisoAErefDash'])
                RDashS['TegTCinisoAErefDash'] = np.ravel(RDashS['TegTCinisoAErefDash'])
                RDashS['TegTCoutisoAErefDash'] = np.ravel(RDashS['TegTCoutisoAErefDash'])
                RDashS['IMO'] = np.empty(np.shape(RDashS['UrefDash']))
                RDashS['IMO'] = int(RDS['IMONoVessel'])

                RDashS['FOCMECPDash'] = np.ravel(RDashS['FOCMECPDash'])
                RDashS['NLightMErefDash'] = np.ravel(RDashS['NLightMErefDash'])
                RDashS['TCPDash'] = np.ravel(RDashS['TCPDash'])
                RDashS['UCPDash'] = np.ravel(RDashS['UCPDash'])

            except Exception as eri:
                # raise
                expi = "Reference DashBoard Error-{} in line No. {} for Vessel {}".format(eri, sys.exc_info()[
                    -1].tb_lineno, v.imo)
                logger.error(expi, exc_info=True)
                ExceptionHandler("Ref", expi)

                v['RefOut'] = "empty"
                rd_status = 0
                pass
            else:
                v['RefOut'] = RDashS
                pass
        # return records_df
    except Exception as err:
        # raise
        expo = "{} in line No. {}".format(err, sys.exc_info()[-1].tb_lineno)
        logger.error(expo, exc_info=True)
        ExceptionHandler("Ref", expo)

        SRQSTD['TotalTimeLapse'] = time.time() - start_time
        SRQSTD['Remarks'] = "failed"
        SRQST.append(SRQSTD)
        return ({}, SRQST)

    else:
        if rd_status == 0:
            SRQSTD['TotalTimeLapse'] = time.time() - start_time
            SRQSTD['Remarks'] = "failed"
            SRQST.append(SRQSTD)
            return ({}, SRQST)
        else:
            SRQSTD['TotalTimeLapse'] = time.time() - start_time
            SRQSTD['Remarks'] = "complete"
            SRQST.append(SRQSTD)
            records_df_filtered = records_df[records_df['RefOut'] != "empty"]
            return (records_df_filtered, SRQST)


