# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd

def IdealSCOC(MCRME,NME,SCOCME_set,BNCOME,SulFOME,CategoryME,MakerME,TypeLBME,ModelLBME,RegLBME,NMCRME,SCOCbME,SCOCminME,SCOCmaxME,BNLBME,ACCLBME,SCOCminMANelME):
    #Memory pre-allocation
    SCOCidME=np.empty(np.shape(MCRME))
    SCOCidME[:] = np.nan
    
    #If 4-stroke N/A
    if CategoryME=='4':
        return SCOCidME
    
    #Run for every log
    for i in range(0,len(MCRME)):
        SCOCidME[i]=IdealSCOCScalar(MCRME[i],NME[i],SCOCME_set[i],BNCOME[i],SulFOME[i],MakerME,TypeLBME,ModelLBME,RegLBME,NMCRME,SCOCbME,SCOCminME,SCOCmaxME,BNLBME,ACCLBME,SCOCminMANelME)
    
    
    return SCOCidME  


def IdealSCOCScalar(MCRME,NME,SCOCME_set,BNCOME,SulFOME,MakerME,TypeLBME,ModelLBME,RegLBME,NMCRME,SCOCbME,SCOCminME,SCOCmaxME,BNLBME,ACCLBME,SCOCminMANelME):
    #Value initialization
    SCOCidME=np.nan
    
    #Define the multiplication factors 
    A=NME/(NMCRME*MCRME/100)
    B=NMCRME/NME
    C=B
    if RegLBME=='MEP':
        C=A
    
   #Calculate ideal SCOC values
    
    #MAN
    if MakerME in 'MAN':
        if TypeLBME in 'M':
            if (MCRME>=25)and( MCRME<=100):
                SCOCidME=SCOCbME*C
            elif MCRME<25:
                SCOCidME=SCOCbME*B
        elif TypeLBME in 'E':
            if pd.isnull(BNLBME) or pd.isnull(ACCLBME):
                SCOCidME = SCOCminMANelME 
            else:
                SCOCidME=BNLBME/BNCOME*ACCLBME*SulFOME
                                             
    #Mitsubishi
    elif MakerME in 'Mitsubishi':
        if MCRME>=15 and MCRME<=100:
            if ~np.isnan(SCOCbME):
                SCOCidME=SCOCbME*A
            elif np.isnan(SCOCbME):
                SCOCidME=SCOCME_set
        
        elif MCRME<15:
            SCOCidME=SCOCmaxME
    
    #Wartsila
    elif MakerME in 'Wartsila' or MakerME in 'WinGD'or MakerME in 'Sulzer':
        
        SCOCidME=SCOCbME
        if ModelLBME in 'CLU3' and  MCRME<20:
            SCOCidME=SCOCbME*0.2/(MCRME/100)
        if pd.isnull(SCOCbME):
            SCOCidME=SCOCminME
            
        
    
    #Add safety checks and adjust SCOCid according to the valid range
    if SCOCidME>SCOCmaxME:
        SCOCidME=SCOCmaxME
    elif SCOCidME<SCOCminME:
        SCOCidME=SCOCminME
    
    return SCOCidME



