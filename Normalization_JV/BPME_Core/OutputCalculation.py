# -*- coding: utf-8 -*-

"""
Created on Thu Apr 30 11:32:45 2020

@author: SubhinAntony

% OutputCalculation
% Description:  This function calculates the all KPIs used for every BPME.
%
% Input:        SDS (structure)
%               RDS (structure)
%
% Output:       ODS (structure)
%
% See also:     BPMEngineMESingleVessel

"""
import sys
from ExceptionHandler import ExceptionHandler
from BPME_Common import IdealSFOC as isf
from BPME_Core import IdealSpeedHoltrop as ish
from BPME_Common import IdealSpeed as ids
from BPME_Common import IdealDeliveredPower as idp
from BPME_Common import IdealRPMHoltrop as irh
from BPME_Common import IdealRPM as idr
from BPME_Common import IdealFOC as idf
from BPME_Common import IdealST as IST
from BPME_Core import AEUtilization as aeu
from BPME_Core import IdealEffectiveLightPower as iel
from BPME_Core import WeatherCorrectionIndex as wci
from BPME_Core import IdealSCOC as isc
from BPME_Core import IdealSCOCSetCorr as iscsetcor
import numpy as np
import time
import logging 
logger = logging.getLogger('BPMEngine.2.02')
def OutputCalculation(SDS,RDS,DS,Eidx,IMO,LogType,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        ODS={}
        ODS['LogID']=SDS['LogID']
        IMO = int(RDS['IMONoVessel'])
        if len(str(Eidx))==0:
            Eidx=1
        if LogType  in 'Noon':
            SDS['SFOCMEActuals'] = SDS['SFOCME']
            if SDS['SFOCMEActuals'].ndim==2:
                SDS['SFOCMEActuals']=np.nanmean(SDS['SFOCMEActuals'].astype('float64'),axis=1)
            #2
            if 'mFOCAE' not in SDS:
                SDS['SFOCAEActuals'] = np.empty(SDS['SFOCMEActuals'].shape)
                SDS['SFOCAEActuals'] [:]= np.nan
            else:
                if SDS['mFOCAE'].ndim==1:
                    SDS['mFOCAE']=SDS['mFOCAE'].reshape(1,-1).T
                #SDS['SFOCAEActuals'] =1000*np.divide(SDS['mFOCAE'],(SDS['PeffAE']*SDS['RHAE']),out= np.full_like(SDS['mFOCAE'],float("nan")),where=(SDS['PeffAE']*SDS['RHAE'])!=0)   # Check if this can be calculated for LogType = Noon.
                SDS['SFOCAEActuals'] =1000*np.divide(SDS['mFOCAE'].astype('float64'),(SDS['PeffAE']*SDS['RHAE']),out= np.full_like((SDS['PeffAE']*SDS['RHAE']),float("nan")),where=(SDS['PeffAE']*SDS['RHAE'])!=0)
                if SDS['SFOCAEActuals'].ndim==2:
                    SDS['SFOCAEActuals']=np.nanmean(SDS['SFOCAEActuals'],axis=1,dtype = 'float64')
            #3
            TexhAE = SDS['TegEVoutcylAE']
            TexhAE_AE_Wise_avg = np.mean(TexhAE,2,keepdims = True) # For each AE 1 value which is the average of all cylinders of that AE
            SDS['DeltaTExhCylAE'] = np.abs((TexhAE - TexhAE_AE_Wise_avg)*100/TexhAE_AE_Wise_avg)
            #4
            TexhME = SDS['TegEVoutcylME']
            TexhME_ME_Wise_avg = np.mean(TexhME,2,keepdims = True) # For each ME 1 value which is the average of all cylinders of that ME
            SDS['DeltaTExhCylME'] = np.abs((TexhME - TexhME_ME_Wise_avg)*100/TexhME_ME_Wise_avg)
            #5
            NTCTCME = SDS['NTCTCME']
            NTCTCME_ME_Wise_avg = np.mean(NTCTCME,2,keepdims = True) # For each ME 1 value which is the average of all TCs of that ME
            SDS['DeltaNTCTCME'] = np.abs((NTCTCME - NTCTCME_ME_Wise_avg)*100/NTCTCME_ME_Wise_avg)
            #6
            SDS['SFOCMEActuals_eng'] = SDS['SFOCME']
            # if SFOCMEActuals_eng.shape[1]==1:
            #     SDS['SFOCMEActuals_1']=SFOCMEActuals_eng
            # elif SFOCMEActuals_eng.shape[1]>1:
            #     SFOCMEActuals_df=pd.DataFrame(SFOCMEActuals_eng)
            #     sin.rename(columns=lambda x:x+1).add_prefix('SFOCMEActuals'+'_').astype('float64')
            
            #7
            if 'mFOCAE' not in SDS:
                SDS['SFOCAEActuals_eng'] = np.empty(SDS['SFOCMEActuals'].shape)
                SDS['SFOCAEActuals_eng'] [:]= np.nan
            else:
                if SDS['mFOCAE'].ndim==1:
                    SDS['mFOCAE']=SDS['mFOCAE'].reshape(1,-1).T
                #SDS['SFOCAEActuals'] =1000*np.divide(SDS['mFOCAE'],(SDS['PeffAE']*SDS['RHAE']),out= np.full_like(SDS['mFOCAE'],float("nan")),where=(SDS['PeffAE']*SDS['RHAE'])!=0)   # Check if this can be calculated for LogType = Noon.
                SDS['SFOCAEActuals_eng'] =1000*np.divide(SDS['mFOCAE'].astype('float64'),(SDS['PeffAE']*SDS['RHAE']),out= np.full_like((SDS['PeffAE']*SDS['RHAE']),float("nan")),where=(SDS['PeffAE']*SDS['RHAE'])!=0)
            
            
        if LogType  in 'Noon' or LogType in 'HP' or LogType in 'ME' or LogType in 'Auto':  
            
            #Calculate ideal PeffLight
            ODS['PeffLightidME']=iel.IdealEffectiveLightPower(SDS['NME'],RDS)
            
            #Conventional engines
            if len(np.where(RDS['DualFuelME']=='No')[0])==len(RDS['DualFuelME']):  ####
                #Ideal (common) assuming ideal shop test data for all engines
                ODS['SFOCisoidME']=isf.IdealSFOC(np.mean(SDS['MCRME'],axis=1),\
                                                          RDS['SFOCisocalcMEST'][Eidx-1,:],\
                                                              RDS['MCRMEST'][Eidx-1,:])
        
                #Dual fuel engines
            elif 'Yes' in RDS['DualFuelME']:
                #Ideal (common) assuming ideal shop test data for all engines
                #Memory pre-allocation
                ODS['SFOCisoidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['SFOCisoidME'][:]=np.nan
                ODS['SPOCisoidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['SPOCisoidME'][:]=np.nan
                ODS['SGOCisoidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['SGOCisoidME'][:]=np.nan
                ODS['deltaSFOCisoME']=np.empty(np.shape(SDS['pambairER']))
                ODS['deltaSFOCisoME'][:]=np.nan
                
                # Fuel mode
                ODS['SFOCisoidME'][SDS['FOModeME'].ravel()]=isf.IdealSFOC(np.mean(SDS['MCRME'][SDS['FOModeME'].ravel(),:],axis=1),\
                                                                                       RDS['SFOCisocalcMEST'][RDS['RepMEFO'],:],\
                                                                                           RDS['MCRMEST'][RDS['RepMEFO'],:])
                #Gas mode
                #Pilot
                ODS['SPOCisoidME'][SDS['GOModeME'].ravel()]=isf.IdealSFOC(np.mean(SDS['MCRME'][SDS['GOModeME'].ravel(),:],axis=1),\
                                                                                       RDS['SFOCisocalcMEST'][RDS['RepMEGO'],:],\
                                                                                           RDS['MCRMEST'][RDS['RepMEGO'],:])
                #Gas 
                ODS['SGOCisoidME'][SDS['GOModeME'].ravel()]=isf.IdealSFOC(np.mean(SDS['MCRME'][SDS['GOModeME'].ravel(),:],axis=1),\
                                                                                       RDS['SGOCisocalcMEST'][RDS['RepMEGO'],:],\
                                                                                           RDS['MCRMEST'][RDS['RepMEGO'],:])
                #Embed SGOC+SPOC in SFOC vectors for a uniform terminology
                ODS['SFOCisoidME'][SDS['GOModeME'].ravel()]=ODS['SGOCisoidME'][SDS['GOModeME'].ravel()]+ODS['SPOCisoidME'][SDS['GOModeME'].ravel()]
    
            #Average percentage
            ODS['deltaSFOCisoME']=(SDS['SFOCisoME']-ODS['SFOCisoidME'])/ODS['SFOCisoidME']*100
        
       
        ##############################
        #NOON KPIS
        ##############################
        if LogType in 'Noon':
            #Calculate ideal SCOC
            ODS['SCOCidME']=isc.IdealSCOC(np.nanmean(SDS['MCRME'],axis= 1),
                                          np.nanmean(SDS['NME'],axis=1),
                                          np.nanmean(SDS['SCOCME_set'],axis=1),
                                          SDS['BNCOME'],
                                          SDS['SulFOME'],
                                          RDS['CategoryME'][0],
                                          RDS['MakerME'][0],
                                          RDS['TypeLBME'][0],
                                          RDS['ModelLBME'][0],
                                          RDS['RegLBME'][0],
                                          RDS['NMCRME'][0],
                                          RDS['SCOCbME'][0],
                                          RDS['SCOCminME'][0],
                                          RDS['SCOCmaxME'][0],
                                          RDS['BNLBME'][0],
                                          RDS['ACCLBME'][0],
                                          RDS['SCOCminMANelME'])
            ODS['SCOCcorME_set']=iscsetcor.IdealSCOC(np.nanmean(SDS['MCRME'],axis= 1),
                                          np.nanmean(SDS['NME'],axis=1),
                                          np.nanmean(SDS['SCOCME_set'],axis=1),
                                          SDS['BNCOME'],
                                          SDS['SulFOME'],
                                          RDS['CategoryME'][0],
                                          RDS['MakerME'][0],
                                          RDS['TypeLBME'][0],
                                          RDS['ModelLBME'][0],
                                          RDS['RegLBME'][0],
                                          RDS['NMCRME'][0],
                                          np.nanmean(SDS['SCOCME_set'],axis=1),
                                          RDS['SCOCminME'][0],
                                          RDS['SCOCmaxME'][0],
                                          RDS['BNLBME'][0],
                                          RDS['ACCLBME'][0])
            #Calculate AEU
            ODS['AEU']=aeu.AEUtilization(SDS['RHAE'],SDS['Dt'],SDS['DEengAE'],RDS['PengnomAE'],RDS['MCRidAE'],RDS['RHmarginAE'])
            #Memory pre-allocation
            ODS['TegEVoutisoidAE']=np.empty(np.shape(SDS['MCRAE']))
            ODS['TegEVoutisoidAE'][:] = np.nan
            
            ODS['TegTCinisoidAE']=np.empty(np.shape(SDS['MCRAE']))
            ODS['TegTCinisoidAE'][:] = np.nan
            
            ODS['TegTCoutisoidAE']=np.empty(np.shape(SDS['MCRAE']))
            ODS['TegTCoutisoidAE'][:] = np.nan
            
            ODS['deltaTegEVoutisoAE']=np.empty(np.shape(SDS['MCRAE']))
            ODS['deltaTegEVoutisoAE'][:] = np.nan
            
            ODS['deltaTegTCinisoAE']=np.empty(np.shape(SDS['MCRAE']))
            ODS['deltaTegTCinisoAE'][:] = np.nan
            
            ODS['deltaTegTCoutisoAE']=np.empty(np.shape(SDS['MCRAE']))
            ODS['deltaTegTCoutisoAE'][:] = np.nan
        
            if RDS['AEVerModel'] in 'Y':
                #Check if DF capability for AEs
                if len(np.where(RDS['DualFuelME']=='No')[0])==len(RDS['DualFuelME']):
                    #Calculate ideal values for all AEs
                    for i in range(0,int(RDS['nAE'])):
                        ODS['TegEVoutisoidAE'][:,i]=IST.IdealST(RDS['MCRAEST'][i,:],RDS['TegEVoutisocalcAEST'][i,:],SDS['MCRAE'][:,i],100)
                        ODS['TegTCinisoidAE'][:,i]=IST.IdealST(RDS['MCRAEST'][i,:],RDS['TegTCinisocalcAEST'][i,:],SDS['MCRAE'][:,i],120)
                        ODS['TegTCoutisoidAE'][:,i]=IST.IdealST(RDS['MCRAEST'][i,:],RDS['TegTCoutisocalcAEST'][i,:],SDS['MCRAE'][:,i],100)
                elif 'Yes' in RDS['DualFuelAE']:
                    pass
                #Since currently there is no option to select fuel mode in PO
                #UI, we define the gas mode as the default mode for DF vessels.
                #This is not optimal and may lead to incorrect calculation of
                #KPIs. This must be fixed.
                #The current approach for MEs is as follows:
                #---quote---
                #In case of nME>1 we assume that all engines are identical so we
                #get Engine No1 data and multiply by nME when needed or take the
                #mean value (see etaeff)
                #---unquote---
                #This is not sufficient for AEs which are possibly different so
                #this functionality is not available now for DF AEs
           
            #deltaTegEVoutisoAE
            ODS['deltaTegEVoutisoAE']=(SDS['TegEVoutisoAE']-ODS['TegEVoutisoidAE'])/ODS['TegEVoutisoidAE']*100
            #deltaTegTCinisoAE
            ODS['deltaTegTCinisoAE']=(SDS['TegTCinisoAE']-ODS['TegTCinisoidAE'])/ODS['TegTCinisoidAE']*100
            #deltaTegTCoutisoAE
            ODS['deltaTegTCoutisoAE']=(SDS['TegTCoutisoAE']-ODS['TegTCoutisoidAE'])/ODS['TegTCoutisoidAE']*100
        
        
        #####################################
        #HP/Noon/Auto KPIs
        ######################################
        if LogType in ['Noon','HP','Auto']:
            #2.Speed Drop (Deviation of Speed from Reference Corrected for Weather and Shallow Water Effects)
            UHid=ish.IdealSpeedHoltrop(SDS['PDcor'],\
                        SDS['Tm'],\
                        SDS['Tf'],\
                        SDS['Lwl'],\
                        SDS['DISPV'],\
                        SDS['WSA'],\
                        SDS['Cb'],\
                        SDS['Cm'],\
                        SDS['Cwp'],\
                        SDS['lcb'],\
                        SDS['rhosw'],\
                        SDS['vsw'],\
                        SDS['Ar'],\
                        SDS['Ab'],\
                        SDS['hb'],\
                        SDS['Atr'],\
                        RDS)
            ODS['Uid']=ids.IdealSpeed(SDS['PDcor'],\
                                        SDS['Tm'],\
                                        UHid,\
                                        RDS['Tref'],\
                                        RDS['Pref'],\
                                        RDS['Uref'],\
                                        RDS['Tb'],\
                                        RDS['Ts'])
#            ODS['SpeedDrop']=-(SDS['Ucor']-ODS['Uid'])/ODS['Uid']*100
            ODS['SpeedDrop']=np.divide(-(SDS['Ucor']-ODS['Uid']), ODS['Uid'], out=np.full_like(-(SDS['Ucor']-ODS['Uid']),float('nan')), where=ODS['Uid']!=0)*100         
            
            #3.Apparent Slip
#            ODS['ApparentSlip']=(1-(SDS['SOG']*30.8667)/(RDS['pitchProp']*SDS['NProp']))*100
            ODS['ApparentSlip']=(1-np.divide((SDS['SOG']*30.8667), (RDS['pitchProp']*SDS['NProp']), out=np.full_like((SDS['SOG']*30.8667),float('nan')), where=(RDS['pitchProp']*SDS['NProp'])!=0))*100
            
            #4.Deviation of Power(U) from Reference Corrected for Weather Effect
            PDid1=idp.IdealDeliveredPower(SDS['Ucor'].astype('float64'),\
                                  SDS['Tm'].astype('float64'),\
                                  SDS['PDHid'],\
                                  RDS['Tref'],\
                                  RDS['Pref'],\
                                  RDS['Uref'],\
                                  RDS['Tb'],\
                                  RDS['Ts'])
#            ODS['deltaPD1']=(SDS['PDcor']-PDid1)/PDid1*100
            ODS['deltaPD1']=np.divide((SDS['PDcor']-PDid1), PDid1, out=np.full_like((SDS['PDcor']-PDid1),float('nan')), where=PDid1!=0)*100.
            #5.Deviation of Power(N) from Reference Corrected for Weather Effect
            #this needs further investigation
            temp=np.empty(np.shape(SDS['NPropcor']))
            temp[:]=np.nan
            PDHid2=idp.IdealDeliveredPower(SDS['NPropcor'].astype('float64'),\
                                    np.ones(np.shape(SDS['NPropcor'])),\
                                    temp,\
                                    np.array([1.0]),\
                                    RDS['PeffMEST'][0,:]*RDS['nME']*RDS['etaT']*RDS['etaeffME'][0],\
                                    RDS['NMEST'][0,:]*1.05,\
                                    1.0,\
                                    1.0)
            PDid2=idp.IdealDeliveredPower(SDS['NPropcor'].astype('float64'),\
                                  SDS['Tm'].astype('float64'),\
                                  PDHid2,\
                                  RDS['Tref'],\
                                  RDS['Pref'],\
                                  RDS['Nref'],\
                                  RDS['Tb'],\
                                  RDS['Ts'])
#            ODS['deltaPD2']=(SDS['PDcor']-PDid2)/PDid2*100
            ODS['deltaPD2']=np.divide((SDS['PDcor']-PDid2), PDid2, out=np.full_like((SDS['PDcor']-PDid2),float('nan')), where=PDid2!=0)*100
            #6.Deviation of Propeller Rotational Speed from Reference Corrected for Weather and Shallow Water Effects
            NPropHid=irh.IdealRPMHoltrop(SDS['PDcor'],\
                                  RDS['NMEST'][0,:],\
                                  RDS['PeffMEST'][0,:]*RDS['nME'],\
                                  RDS['etaT'],\
                                  RDS['etaeffME'][0],\
                                  RDS['factorN2'],\
                                  RDS)
            NPropid=idr.IdealRPM(SDS['Ucor'].astype('float64'),\
                          SDS['Tm'].astype('float64'),\
                          NPropHid,\
                          RDS['Tref'],\
                          RDS['Nref'],\
                          RDS['Uref'],\
                          RDS['Tb'],\
                          RDS['Ts'])
            ODS['deltaN']=np.divide((SDS['NPropcor']-NPropid),(NPropid),out=np.full_like((SDS['NPropcor']-NPropid),np.nan),where = NPropid!=0 )*100
            #7.Deviation of FO Consumption from Reference Corrected for LCV and Weather Effect
            if len(np.where(RDS['DualFuelME']=='No')[0])==len(RDS['DualFuelME']):
                #In case of nME>1 we assume that all engines are identical so we get 
                #Engine No1 data and multiply by nME when needed or take the mean value (see etaeff)
                FOCidME=idf.IdealFOC( PDid1,\
                              RDS['FOCMEST'][0,:]*RDS['nME'],\
                              RDS['PeffMEST'][0,:]*RDS['nME'],\
                              RDS['LCVFOMEST'][0],\
                              RDS['LCVHFOref'],\
                              RDS['etaT'],\
                              RDS['etaeffME'][0])
                if FOCidME.ndim ==1:
                    FOCidME=FOCidME.reshape(1,-1).T
                if SDS['PD'].ndim==1:
                    SDS['PD']=SDS['PD'].reshape(1,-1).T
                
                    
                FOCid1ME=idf.IdealFOC(SDS['PD'],\
                              RDS['FOCMEST'][0,:]*RDS['nME'],\
                              RDS['PeffMEST'][0,:]*RDS['nME'],\
                              RDS['LCVFOMEST'][0],\
                              RDS['LCVHFOref'],\
                              RDS['etaT'],\
                              RDS['etaeffME'][0])
                
                FOCid2ME=idf.IdealFOC(SDS['PDcor'],\
                              RDS['FOCMEST'][0,:]*RDS['nME'],\
                              RDS['PeffMEST'][0,:]*RDS['nME'],\
                              RDS['LCVFOMEST'][0],\
                              RDS['LCVHFOref'],\
                              RDS['etaT'],\
                              RDS['etaeffME'][0])
                FOCidME=FOCidME.ravel()
                FOCid1ME=FOCid1ME.ravel()
                FOCid2ME=FOCid2ME.ravel()
                if FOCidME.ndim==1:
                    FOCidME=FOCidME.reshape(1,-1).T
                if FOCid1ME.ndim==1:
                    FOCid1ME=FOCid1ME.reshape(1,-1).T
                if FOCid2ME.ndim==1:
                    FOCid2ME=FOCid2ME.reshape(1,-1).T
                # ODS['deltaFOCME']=((SDS['FOCLCVcorME']-FOCid1ME)+(FOCid2ME-FOCidME))/FOCidME*100
                ODS['deltaFOCME']=np.divide(((SDS['FOCLCVcorME']-FOCid1ME)+(FOCid2ME-FOCidME)),FOCidME,out = np.full_like(((SDS['FOCLCVcorME']-FOCid1ME)+(FOCid2ME-FOCidME)),np.nan),where = FOCidME!=0)*100
            #DF engines
            elif len(np.where(RDS['DualFuelME']=='Yes')[0])==len(RDS['DualFuelME']):
                #Fuel mode
                FOCidME=idf.IdealFOC( PDid1,\
                              RDS['FOCMEST'][RDS['RepMEFO'],:]*RDS['nME'],\
                              RDS['PeffMEST'][RDS['RepMEFO'],:]*RDS['nME'],\
                              RDS['LCVFOMEST'][RDS['RepMEFO']],\
                              RDS['LCVHFOref'],\
                              RDS['etaT'],\
                              RDS['etaeffME'][int(RDS['NoME'][RDS['RepMEFO']])])
                FOCid1ME=idf.IdealFOC(SDS['PD'],\
                              RDS['FOCMEST'][RDS['RepMEFO'],:]*RDS['nME'],\
                              RDS['PeffMEST'][RDS['RepMEFO'],:]*RDS['nME'],\
                              RDS['LCVFOMEST'][RDS['RepMEFO']],\
                              RDS['LCVHFOref'],\
                              RDS['etaT'],\
                              RDS['etaeffME'][int(RDS['NoME'][RDS['RepMEFO']])])
                FOCid2ME=idf.IdealFOC(SDS['PDcor'].astype('float64'),\
                              RDS['FOCMEST'][RDS['RepMEFO'],:]*RDS['nME'],\
                              RDS['PeffMEST'][RDS['RepMEFO'],:]*RDS['nME'],\
                              RDS['LCVFOMEST'][RDS['RepMEFO']],\
                              RDS['LCVHFOref'],\
                              RDS['etaT'],\
                              RDS['etaeffME'][int(RDS['NoME'][RDS['RepMEFO']])])
                              #RDS['etaeffME'][RDS['NoME'][RDS[RepMEFO]]])
                FOCidME=FOCidME.ravel()
                FOCid1ME=FOCid1ME.ravel()
                FOCid2ME=FOCid2ME.ravel()
                if FOCidME.ndim==1:
                    FOCidME=FOCidME.reshape(1,-1).T
                if FOCid1ME.ndim==1:
                    FOCid1ME=FOCid1ME.reshape(1,-1).T
                if FOCid2ME.ndim==1:
                    FOCid2ME=FOCid2ME.reshape(1,-1).T
                #Gas mode
                #Pilot
                POCidME=idf.IdealFOC( PDid1,\
                              RDS['FOCMEST'][RDS['RepMEGO'],:]*RDS['nME'],\
                              RDS['PeffMEST'][RDS['RepMEGO'],:]*RDS['nME'],\
                              RDS['LCVFOMEST'][RDS['RepMEGO']],\
                              RDS['LCVHFOref'],\
                              RDS['etaT'],\
                              RDS['etaeffME'][int(RDS['NoME'][RDS['RepMEGO']])])
                POCid1ME=idf.IdealFOC(SDS['PD'],\
                              RDS['FOCMEST'][RDS['RepMEGO'],:]*RDS['nME'],\
                              RDS['PeffMEST'][RDS['RepMEGO'],:]*RDS['nME'],\
                              RDS['LCVFOMEST'][RDS['RepMEGO']],\
                              RDS['LCVHFOref'],\
                              RDS['etaT'],\
                              RDS['etaeffME'][int(RDS['NoME'][RDS['RepMEGO']])])
                POCid2ME=idf.IdealFOC(SDS['PDcor'],\
                              RDS['FOCMEST'][RDS['RepMEGO'],:]*RDS['nME'],\
                              RDS['PeffMEST'][RDS['RepMEGO'],:]*RDS['nME'],\
                              RDS['LCVFOMEST'][RDS['RepMEGO']],\
                              RDS['LCVHFOref'],\
                              RDS['etaT'],\
                              RDS['etaeffME'][int(RDS['NoME'][RDS['RepMEGO']])])
                #Gas
                GOCidME=idf.IdealFOC( PDid1,\
                              RDS['GOCMEST'][RDS['RepMEGO'],:]*RDS['nME'],\
                              RDS['PeffMEST'][RDS['RepMEGO'],:]*RDS['nME'],\
                              RDS['LCVGOMEST'][RDS['RepMEGO']],\
                              RDS['LCVHFOref'],\
                              RDS['etaT'],\
                              RDS['etaeffME'][int(RDS['NoME'][RDS['RepMEGO']])])
                GOCid1ME=idf.IdealFOC(SDS['PD'],\
                              RDS['GOCMEST'][RDS['RepMEGO'],:]*RDS['nME'],\
                              RDS['PeffMEST'][RDS['RepMEGO'],:]*RDS['nME'],\
                              RDS['LCVGOMEST'][RDS['RepMEGO']],\
                              RDS['LCVHFOref'],\
                              RDS['etaT'],\
                              RDS['etaeffME'][int(RDS['NoME'][RDS['RepMEGO']])])
                GOCid2ME=idf.IdealFOC(SDS['PDcor'],\
                              RDS['GOCMEST'][RDS['RepMEGO'],:]*RDS['nME'],\
                              RDS['PeffMEST'][RDS['RepMEGO'],:]*RDS['nME'],\
                              RDS['LCVGOMEST'][RDS['RepMEGO']],\
                              RDS['LCVHFOref'],\
                              RDS['etaT'],\
                              RDS['etaeffME'][int(RDS['NoME'][RDS['RepMEGO']])])
                #Memory pre-allocation
                ODS['deltaFOCME']=np.empty(np.shape(SDS['pambairER']))
                ODS['deltaFOCME'][:]=np.nan
                
                ODS['deltaGOCME']=np.empty(np.shape(SDS['pambairER']))
                ODS['deltaGOCME'][:]=np.nan                     
                #Average percentage (combining both modes of operation) 
                ODS['deltaFOCME'][SDS['FOModeME'].ravel()]=((SDS['FOCLCVcorME'][SDS['FOModeME'].ravel()]-\
                                                                    FOCid1ME[SDS['FOModeME'].ravel()])+\
                                                                   (FOCid2ME[SDS['FOModeME'].ravel()]-\
                                                                    FOCidME[SDS['FOModeME'].ravel()]))/\
                                                                    FOCidME[SDS['FOModeME'].ravel()]*100  
                ODS['deltaFOCME'][SDS['GOModeME'].ravel()]=((SDS['FOCLCVcorME'][SDS['GOModeME'].ravel()]-\
                                                                    FOCid1ME[SDS['GOModeME'].ravel()])+\
                                                                   (FOCid2ME[SDS['GOModeME'].ravel()]-\
                                                                    FOCidME[SDS['GOModeME'].ravel()]))/\
                                                                    FOCidME[SDS['GOModeME'].ravel()]*100
            #8.Weather Index  
            if LogType in ['Noon','HP']:
                WI=wci.WeatherCorrectionIndex(SDS,RDS)
                #For normalization purposes and proper UI display in PO page the
                #weather index is transformed as below. The original index will be
                #shown in another page (SPE and/or HP trend) at the begining of Q3 2020 
                WIrangenorm=100-np.abs(WI)
                if np.all(WIrangenorm<0):
                    WIrangenorm=np.array([0])
                #The WI must be further normalized wrt its severity in accordance
                #with the frequency and validity indices color codings. The
                #proposed coloring scale normalization is:
                #Green: 95-100# ->  70-100#
                #Amber: 90-95#  ->  40-70#
                #Red:   0-90#   ->  0-40#
                #The above mapping breaks the function into 2 regions: 0-90# and
                #90-100#. The implementation is shown below:
                WInorm=WIrangenorm
                WInorm[WInorm<90]=4/9*WInorm[WInorm<90]
                WInorm[WInorm>=90]=30/5*WInorm[WInorm>=90]-500;
                ODS['WeatherIndex']=WInorm
                if ODS['WeatherIndex'].ndim==1:
                    ODS['WeatherIndex']=ODS['WeatherIndex'].reshape(1,-1).T
            elif LogType =='Auto':
                ODS['WeatherIndex']=np.empty(np.shape(SDS['Ucor']))
                ODS['WeatherIndex'][:]=np.nan
                if ODS['WeatherIndex'].ndim==1:
                    ODS['WeatherIndex']=ODS['WeatherIndex'].reshape(1,-1).T
                    
                    
        
        
        ###################################################
        #ME/NOON KPI
        ###################################################       
        if 'Noon' in LogType or 'ME' in  LogType:
            if len(np.where(RDS['DualFuelME']=='No')[0])==len(RDS['DualFuelME']):
                ODS['pscavabsisoidME']=IST.IdealST(RDS['PeffMEST'][Eidx-1,:],RDS['pscavabsisocalcMEST'][Eidx-1,:],SDS['PeffME'],0)
                ODS['TegEVoutisoidME']=IST.IdealST(RDS['PeffMEST'][Eidx-1,:],RDS['TegEVoutisocalcMEST'][Eidx-1,:],SDS['PeffME'],100)
                ODS['NTCidME']=IST.IdealST(RDS['pscavabsisocalcMEST'][Eidx-1,:],RDS['NTCMEST'][Eidx-1,:],SDS['pscavabsisoME'],0)
                
                ODS['pscavabsisoidME']=ODS['pscavabsisoidME'].reshape(1,-1).T
                ODS['TegEVoutisoidME']=ODS['TegEVoutisoidME'].reshape(1,-1).T
                ODS['NTCidME']=ODS['NTCidME'].reshape(1,-1).T
            elif 'Yes' in RDS['DualFuelME']:
                #Since currently there is no option to select fuel mode in PO
                #UI, we define the gas mode as the default mode for DF vessels.
                #This is not optimal and may lead to incorrect calculation of
                #KPIs. This must be fixed.
                #In case of nME>1 we assume that all engines are identical so we
                #get Engine No1 data and multiply by nME when needed or take the
                #mean value (see etaeff)
                foidx=(RDS['FuelModeMEST']=='F') & (RDS['NoME']==Eidx)
                goidx=(RDS['FuelModeMEST']=='G') & (RDS['NoME']==Eidx)  
                
                ODS['pscavabsisoidME'] = np.empty(SDS['PeffME'].shape)
                ODS['pscavabsisoidME'][:] = np.nan
                ODS['TegEVoutisoidME'] = np.empty(SDS['PeffME'].shape)
                ODS['TegEVoutisoidME'][:]  = np.nan
                ODS['NTCidME']=np.empty(SDS['PeffME'].shape)
                ODS['NTCidME'][:]=np.nan
    
                ODS['pscavabsisoidME'][SDS['FOModeME'].ravel(),:]=IST.IdealST(RDS['PeffMEST'][foidx,:],RDS['pscavabsisocalcMEST'][foidx,:],SDS['PeffME'][SDS['FOModeME'].ravel(),:],0).reshape(np.shape( ODS['pscavabsisoidME'][SDS['FOModeME'].ravel(),:]))
                ODS['TegEVoutisoidME'][SDS['FOModeME'].ravel(),:]=IST.IdealST(RDS['PeffMEST'][foidx,:],RDS['TegEVoutisocalcMEST'][foidx,:],SDS['PeffME'][SDS['FOModeME'].ravel(),:],100).reshape(np.shape(ODS['TegEVoutisoidME'][SDS['FOModeME'].ravel(),:]))
                ODS['NTCidME'][SDS['FOModeME'].ravel(),:]=IST.IdealST(RDS['pscavabsisocalcMEST'][foidx,:],RDS['NTCMEST'][foidx,:],SDS['pscavabsisoME'][SDS['FOModeME'].ravel(),:],0).reshape(np.shape(ODS['NTCidME'][SDS['FOModeME'].ravel(),:]))
               
                ODS['pscavabsisoidME'][SDS['GOModeME'].ravel(),:]=IST.IdealST(RDS['PeffMEST'][goidx,:],RDS['pscavabsisocalcMEST'][goidx,:],SDS['PeffME'][SDS['GOModeME'].ravel(),:],0).reshape(np.shape(ODS['pscavabsisoidME'][SDS['GOModeME'].ravel(),:]))
                ODS['TegEVoutisoidME'][SDS['GOModeME'].ravel(),:]=IST.IdealST(RDS['PeffMEST'][goidx,:],RDS['TegEVoutisocalcMEST'][goidx,:],SDS['PeffME'][SDS['GOModeME'].ravel(),:],100).reshape(np.shape(ODS['TegEVoutisoidME'][SDS['GOModeME'].ravel(),:]))
                ODS['NTCidME'][SDS['GOModeME'].ravel(),:]=IST.IdealST(RDS['pscavabsisocalcMEST'][goidx,:],RDS['NTCMEST'][goidx,:],SDS['pscavabsisoME'][SDS['GOModeME'].ravel(),:],0).reshape(np.shape(ODS['NTCidME'][SDS['GOModeME'].ravel(),:]))
                #ODS['pscavabsisoidME']=np.array(ODS['pscavabsisoidME']).T
                #ODS['TegEVoutisoidME'] = np.array(ODS['TegEVoutisoidME']).T
                #ODS['NTCidME'] = np.array(ODS['NTCidME']).T
           
            #9.deltapscavabsisoME
            ODS['deltapscavabsisoME']=100*np.divide((SDS['pscavabsisoME']-ODS['pscavabsisoidME']),ODS['pscavabsisoidME'] ,out=np.full_like((SDS['pscavabsisoME']-ODS['pscavabsisoidME']),float("nan")),where=ODS['pscavabsisoidME']!=0)
            #ODS['deltapscavabsisoME']=(SDS['pscavabsisoME']-ODS['pscavabsisoidME'])/ODS['pscavabsisoidME']*100 
            #10.deltaTegEVoutisoME
            #ODS['deltaTegEVoutisoME']=(SDS['TegEVoutisoME']-ODS['TegEVoutisoidME'])/ODS['TegEVoutisoidME']*100
            ODS['deltaTegEVoutisoME']=100*np.divide((SDS['TegEVoutisoME']-ODS['TegEVoutisoidME']),ODS['TegEVoutisoidME'],out = np.full_like((SDS['TegEVoutisoME']-ODS['TegEVoutisoidME']),float("nan")),where=ODS['pscavabsisoidME']!=0)
            #11.deltaNTCME
            #ODS['deltaNTCME']=(SDS['NTCME']-ODS['NTCidME'])/ODS['NTCidME']*100
            ODS['deltaNTCME']=100*np.divide((SDS['NTCME']-ODS['NTCidME']),ODS['NTCidME'],out = np.full_like((SDS['NTCME']-ODS['NTCidME']),float("nan")),where=ODS['NTCidME']!=0)
            
        
        
        #########################################
        # ME KPIs 
        ##########################################
        if LogType=='ME':
            
            if RDS['DualFuelME'][Eidx-1]=='No':
                #12.Ideal(common) pmax_pcompisoME
                ODS['pmax_pcompisoidME']=IST.IdealST(RDS['MCRMEST'][Eidx-1,:],RDS['pmax_pcompisoMEST'][Eidx-1,:],SDS['MCRME'],0)
                ODS['pmax_pcompisoidME']= ODS['pmax_pcompisoidME'].reshape(1,-1).T  
            
                #13.Ideal (common) pcomp_pscavabsisoME
                ODS['pcomp_pscavabsisoidME']=IST.IdealST(RDS['MCRMEST'][Eidx-1,:],RDS['pcomp_pscavabsisoMEST'][Eidx-1,:],SDS['MCRME'],40)
                ODS['pcomp_pscavabsisoidME'] = ODS['pcomp_pscavabsisoidME'].reshape(1,-1).T
                
                #14.deltapindME
                ODS['pindidME']=IST.IdealST(RDS['MCRMEST'][Eidx-1,:],RDS['pindMEST'][Eidx-1,:],SDS['MCRME'],0)
                ODS['pindidME'] = ODS['pindidME'].reshape(1,-1).T
                
                #15.Ideal (common) NME
                ODS['NidME']= IST.IdealST(RDS['pindMEST'][Eidx-1,:],RDS['NMEST'][Eidx-1,:],SDS['pindME'],0)
                ODS['NidME']= ODS['NidME'].reshape(1,-1).T 
                        
                #16.Ideal (common) FPIME
                ODS['FPIidME']=IST.IdealST(RDS['pindMEST'][Eidx-1,:],RDS['FPIMEST'][Eidx-1,:],SDS['pindME'],0)
                ODS['FPIidME'] = ODS['FPIidME'].reshape(1,-1).T
                
                #17.Ideal (common) pmaxabsisoME
                ODS['pmaxabsisoidME']=IST.IdealST(RDS['pindMEST'][Eidx-1,:],RDS['pmaxabsisocalcMEST'][Eidx-1,:],SDS['pindME'],0)
                ODS['pmaxabsisoidME'] = ODS['pmaxabsisoidME'].reshape(1,-1).T
      
                #18.Ideal(common)pcompabsisoME
                ODS['pcompabsisoidME']=IST.IdealST(RDS['PeffMEST'][Eidx-1,:],RDS['pcompabsisocalcMEST'][Eidx-1,:],SDS['PeffME'],0)
                ODS['pcompabsisoidME'] =  ODS['pcompabsisoidME'].reshape(1,-1).T
    
                #19.Ideal (common) dpairAFME
                ODS['dpairAFidME']=IST.IdealST(RDS['pscavabsisocalcMEST'][Eidx-1,:],RDS['dpairAFMEST'][Eidx-1,:],SDS['pscavabsisoME'],0) 
                ODS['dpairAFidME'] = ODS['dpairAFidME'].reshape(1,-1).T
                
                #20.Ideal (common) TegTCinME
                ODS['TegTCinidME']=IST.IdealST(RDS['pscavabsisocalcMEST'][Eidx-1,:],RDS['TegTCinMEST'][Eidx-1,:],SDS['pscavabsisoME'],120) 
                ODS['TegTCinidME'] = ODS['TegTCinidME'].reshape(1,-1).T
                
                #21.Ideal (common) TegTCoutME
                ODS['TegTCoutidME']=IST.IdealST(RDS['pscavabsisocalcMEST'][Eidx-1,:],RDS['TegTCoutMEST'][Eidx-1,:],SDS['pscavabsisoME'],100) 
                ODS['TegTCoutidME']= ODS['TegTCoutidME'].reshape(1,-1).T
            
                #22.Ideal (common) dTcwACME
                ODS['dTcwACidME']=IST.IdealST(RDS['pscavabsisocalcMEST'][Eidx-1,:],RDS['dTcwACMEST'][Eidx-1,:],SDS['pscavabsisoME'],0) 
                ODS['dTcwACidME']=ODS['dTcwACidME'].reshape(1,-1).T
            
                #23.Ideal (common) dpairACME
                ODS['dpairACidME']=IST.IdealST(RDS['pscavabsisocalcMEST'][Eidx-1,:],RDS['dpairACMEST'][Eidx-1,:],SDS['pscavabsisoME'],0)
                ODS['dpairACidME'] = ODS['dpairACidME'].reshape(1,-1).T
                
                #24.deltaetaTCcompME
                ODS['etaTCcompidME']=IST.IdealST(RDS['pscavabsisocalcMEST'][Eidx-1,:],RDS['etaTCcompcalcMEST'][Eidx-1,:],SDS['pscavabsisoME'],0) 
                ODS['etaTCcompidME'] = ODS['etaTCcompidME'].reshape(1,-1).T
                
                #25.Ideal (common) etaTCturbME
                ODS['etaTCturbidME']=IST.IdealST(RDS['pscavabsisocalcMEST'][Eidx-1,:],RDS['etaTCturbcalcMEST'][Eidx-1,:],SDS['pscavabsisoME'],0) 
                ODS['etaTCturbidME']= ODS['etaTCturbidME'].reshape(1,-1).T
    
                #26.Ideal (common) dTaircwACME
                ODS['dTaircwACidME']=IST.IdealST(RDS['pscavabsisocalcMEST'][Eidx-1,:],RDS['dTaircwACMEST'][Eidx-1,:],SDS['pscavabsisoME'],0) 
                ODS['dTaircwACidME'] = ODS['dTaircwACidME'].reshape(1,-1).T
            
            elif RDS['DualFuelME'][Eidx-1]=='Yes':
                foidx=(RDS['FuelModeMEST']=='F') & (RDS['NoME']==Eidx)
                goidx=(RDS['FuelModeMEST']=='G') & (RDS['NoME']==Eidx)
                
                #Memory pre-allocation
                ODS['pmax_pcompisoidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['pmax_pcompisoidME'][:]=np.nan
                
                ODS['pcomp_pscavabsisoidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['pcomp_pscavabsisoidME'][:]=np.nan
                
                ODS['pindidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['pindidME'][:]=np.nan
                
                ODS['NidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['NidME'][:]=np.nan
                
                ODS['FPIidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['FPIidME'][:]=np.nan
                
                ODS['pmaxabsisoidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['pmaxabsisoidME'][:]=np.nan
                
                ODS['pcompabsisoidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['pcompabsisoidME'][:]=np.nan
                
                ODS['dpairAFidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['dpairAFidME'][:]=np.nan
                
                ODS['TegTCinidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['TegTCinidME'][:]=np.nan
                
                ODS['TegTCoutidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['TegTCoutidME'][:]=np.nan
                
                ODS['dTcwACidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['dTcwACidME'][:]=np.nan
                
                ODS['dpairACidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['dpairACidME'][:]=np.nan
                
                ODS['etaTCcompidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['etaTCcompidME'][:]=np.nan
                
                ODS['etaTCturbidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['etaTCturbidME'][:]=np.nan
                
                ODS['dTaircwACidME']=np.empty(np.shape(SDS['pambairER']))
                ODS['dTaircwACidME'][:]=np.nan
                
                #Fuel mode
                ODS['pmax_pcompisoidME'][SDS['FOModeME'],0]=IST.IdealST(RDS['MCRMEST'][foidx,:],RDS['pmax_pcompisoMEST'][foidx,:],SDS['MCRME'][SDS['FOModeME'],0],0).reshape(1,-1).T
                ODS['pcomp_pscavabsisoidME'][SDS['FOModeME'],0]=IST.IdealST(RDS['MCRMEST'][foidx,:],RDS['pcomp_pscavabsisoMEST'][foidx,:],SDS['MCRME'][SDS['FOModeME'],0],40).reshape(1,-1).T
                ODS['pindidME'][SDS['FOModeME'],0]=IST.IdealST(RDS['MCRMEST'][foidx,:],RDS['pindMEST'][foidx,:],SDS['MCRME'][SDS['FOModeME'],0],0).reshape(1,-1).T
                ODS['NidME'][SDS['FOModeME'],0]=IST.IdealST(RDS['pindMEST'][foidx,:],RDS['NMEST'][foidx,:],SDS['pindME'][SDS['FOModeME'],0],0).reshape(1,-1).T
                ODS['FPIidME'][SDS['FOModeME'],0]=IST.IdealST(RDS['pindMEST'][foidx,:],RDS['FPIMEST'][foidx,:],SDS['pindME'][SDS['FOModeME'],0],0).reshape(1,-1).T
                ODS['pmaxabsisoidME'][SDS['FOModeME'],0]=IST.IdealST(RDS['pindMEST'][foidx,:],RDS['pmaxabsisocalcMEST'][foidx,:],SDS['pindME'][SDS['FOModeME'],0],0).reshape(1,-1).T
                ODS['pcompabsisoidME'][SDS['FOModeME'],0]=IST.IdealST(RDS['PeffMEST'][foidx,:],RDS['pcompabsisocalcMEST'][foidx,:],SDS['PeffME'][SDS['FOModeME'],0],0).reshape(1,-1).T
                ODS['dpairAFidME'][SDS['FOModeME'],0]=IST.IdealST(RDS['pscavabsisocalcMEST'][foidx,:],RDS['dpairAFMEST'][foidx,:],SDS['pscavabsisoME'][SDS['FOModeME'],0],0).reshape(1,-1).T
                ODS['TegTCinidME'][SDS['FOModeME'],0]=IST.IdealST(RDS['pscavabsisocalcMEST'][foidx,:],RDS['TegTCinMEST'][foidx,:],SDS['pscavabsisoME'][SDS['FOModeME'],0],120).reshape(1,-1).T
                ODS['TegTCoutidME'][SDS['FOModeME'],0]=IST.IdealST(RDS['pscavabsisocalcMEST'][foidx,:],RDS['TegTCoutMEST'][foidx,:],SDS['pscavabsisoME'][SDS['FOModeME'],0],100).reshape(1,-1).T
                ODS['dTcwACidME'][SDS['FOModeME'],0]=IST.IdealST(RDS['pscavabsisocalcMEST'][foidx,:],RDS['dTcwACMEST'][foidx,:],SDS['pscavabsisoME'][SDS['FOModeME'],0],0).reshape(1,-1).T
                ODS['dpairACidME'][SDS['FOModeME'],0]=IST.IdealST(RDS['pscavabsisocalcMEST'][foidx,:],RDS['dpairACMEST'][foidx,:],SDS['pscavabsisoME'][SDS['FOModeME'],0],0).reshape(1,-1).T
                ODS['etaTCcompidME'][SDS['FOModeME'],0]=IST.IdealST(RDS['pscavabsisocalcMEST'][foidx,:],RDS['etaTCcompcalcMEST'][foidx,:],SDS['pscavabsisoME'][SDS['FOModeME'],0],0).reshape(1,-1).T
                ODS['etaTCturbidME'][SDS['FOModeME'],0]=IST.IdealST(RDS['pscavabsisocalcMEST'][foidx,:],RDS['etaTCturbcalcMEST'][foidx,:],SDS['pscavabsisoME'][SDS['FOModeME'],0],0).reshape(1,-1).T 
                ODS['dTaircwACidME'][SDS['FOModeME'],0]=IST.IdealST(RDS['pscavabsisocalcMEST'][foidx,:],RDS['dTaircwACMEST'][foidx,:],SDS['pscavabsisoME'][SDS['FOModeME'],0],0).reshape(1,-1).T 
            
                #Gas mode
                ODS['pmax_pcompisoidME'][SDS['GOModeME'],0]=IST.IdealST(RDS['MCRMEST'][goidx,:],RDS['pmax_pcompisoMEST'][goidx,:],SDS['MCRME'][SDS['GOModeME'],0],0).reshape(1,-1).T 
                ODS['pcomp_pscavabsisoidME'][SDS['GOModeME'],0]=IST.IdealST(RDS['MCRMEST'][goidx,:],RDS['pcomp_pscavabsisoMEST'][goidx,:],SDS['MCRME'][SDS['GOModeME'],0],40).reshape(1,-1).T 
                ODS['pindidME'][SDS['GOModeME'],0]=IST.IdealST(RDS['MCRMEST'][goidx,:],RDS['pindMEST'][goidx,:],SDS['MCRME'][SDS['GOModeME'],0],0).reshape(1,-1).T 
                ODS['NidME'][SDS['GOModeME'],0]=IST.IdealST(RDS['pindMEST'][goidx,:],RDS['NMEST'][goidx,:],SDS['pindME'][SDS['GOModeME'],0],0).reshape(1,-1).T 
                ODS['FPIidME'][SDS['GOModeME'],0]=IST.IdealST(RDS['pindMEST'][goidx,:],RDS['FPIMEST'][goidx,:],SDS['pindME'][SDS['GOModeME'],0],0).reshape(1,-1).T 
                ODS['pmaxabsisoidME'][SDS['GOModeME'],0]=IST.IdealST(RDS['pindMEST'][goidx,:],RDS['pmaxabsisocalcMEST'][goidx,:],SDS['pindME'][SDS['GOModeME'],0],0).reshape(1,-1).T 
                ODS['pcompabsisoidME'][SDS['GOModeME'],0]=IST.IdealST(RDS['PeffMEST'][goidx,:],RDS['pcompabsisocalcMEST'][goidx,:],SDS['PeffME'][SDS['GOModeME'],0],0).reshape(1,-1).T 
                ODS['dpairAFidME'][SDS['GOModeME'],0]=IST.IdealST(RDS['pscavabsisocalcMEST'][goidx,:],RDS['dpairAFMEST'][goidx,:],SDS['pscavabsisoME'][SDS['GOModeME'],0],0).reshape(1,-1).T  
                ODS['TegTCinidME'][SDS['GOModeME'],0]=IST.IdealST(RDS['pscavabsisocalcMEST'][goidx,:],RDS['TegTCinMEST'][goidx,:],SDS['pscavabsisoME'][SDS['GOModeME'],0],120).reshape(1,-1).T  
                ODS['TegTCoutidME'][SDS['GOModeME'],0]=IST.IdealST(RDS['pscavabsisocalcMEST'][goidx,:],RDS['TegTCoutMEST'][goidx,:],SDS['pscavabsisoME'][SDS['GOModeME'],0],100).reshape(1,-1).T  
                ODS['dTcwACidME'][SDS['GOModeME'],0]=IST.IdealST(RDS['pscavabsisocalcMEST'][goidx,:],RDS['dTcwACMEST'][goidx,:],SDS['pscavabsisoME'][SDS['GOModeME'],0],0).reshape(1,-1).T  
                ODS['dpairACidME'][SDS['GOModeME'],0]=IST.IdealST(RDS['pscavabsisocalcMEST'][goidx,:],RDS['dpairACMEST'][goidx,:],SDS['pscavabsisoME'][SDS['GOModeME'],0],0).reshape(1,-1).T  
                ODS['etaTCcompidME'][SDS['GOModeME'],0]=IST.IdealST(RDS['pscavabsisocalcMEST'][goidx,:],RDS['etaTCcompcalcMEST'][goidx,:],SDS['pscavabsisoME'][SDS['GOModeME'],0],0).reshape(1,-1).T  
                ODS['etaTCturbidME'][SDS['GOModeME'],0]=IST.IdealST(RDS['pscavabsisocalcMEST'][goidx,:],RDS['etaTCturbcalcMEST'][goidx,:],SDS['pscavabsisoME'][SDS['GOModeME'],0],0).reshape(1,-1).T  
                ODS['dTaircwACidME'][SDS['GOModeME'],0]=IST.IdealST(RDS['pscavabsisocalcMEST'][goidx,:],RDS['dTaircwACMEST'][goidx,:],SDS['pscavabsisoME'][SDS['GOModeME'],0],0).reshape(1,-1).T  
        
        ############################################       
        #AE KPIs
        ############################################
        if LogType in 'AE':
            
            if RDS['DualFuelAE'][Eidx-1]=='No':
                ODS['SFOCisoidAE']=isf.IdealSFOC(SDS['MCRAE'],RDS['SFOCisocalcAEST'][Eidx-1,:],RDS['MCRAEST'][Eidx-1,:])
                ODS['pscavabsisoidAE'] = IST.IdealST(RDS['MCRAEST'][Eidx-1,:],RDS['pscavabsisocalcAEST'][Eidx-1,:],SDS['MCRAE'],0)
                ODS['NTCidAE'] = IST.IdealST(RDS['MCRAEST'][Eidx-1,:],RDS['NTCAEST'][Eidx-1,:],SDS['MCRAE'],0)
                ODS['pmaxabsisoidAE'] = IST.IdealST(RDS['MCRAEST'][Eidx-1,:],RDS['pmaxabsisocalcAEST'][Eidx-1,:],SDS['MCRAE'],0)
                ODS['TegEVoutisoidAE'] = IST.IdealST(RDS['MCRAEST'][Eidx-1,:],RDS['TegEVoutisocalcAEST'][Eidx-1,:],SDS['MCRAE'],100)
                ODS['TegTCinidAE'] = IST.IdealST(RDS['MCRAEST'][Eidx-1,:],RDS['TegTCinAEST'][Eidx-1,:],SDS['MCRAE'],120) 
                ODS['TegTCoutidAE'] = IST.IdealST(RDS['MCRAEST'][Eidx-1,:],RDS['TegTCoutAEST'][Eidx-1,:],SDS['MCRAE'],100) 
            
            elif RDS['DualFuelAE'][Eidx]=='Yes':
                foidx=(RDS['FuelModeAEST']=='F') & (RDS['NoAE']==Eidx)
                goidx=(RDS['FuelModeAEST']=='G') & (RDS['NoAE']==Eidx)
                
                #Memory pre-allocation
                ODS['SFOCisoidAE'] = np.empty(np.shape(SDS['pambairER']))
                ODS['SFOCisoidAE'][:]= np.nan
                
                ODS['SPOCisoidAE'] = np.empty(np.shape(SDS['pambairER']))
                ODS['SPOCisoidAE'][:]= np.nan
                
                ODS['SGOCisoidAE'] = np.empty(np.shape(SDS['pambairER']))
                ODS['SGOCisoidAE'][:]= np.nan
                
                ODS['pscavabsisoidAE'] = np.empty(np.shape(SDS['pambairER']))
                ODS['pscavabsisoidAE'][:]= np.nan
                
                ODS['NTCidAE'] = np.empty(np.shape(SDS['pambairER']))
                ODS['NTCidAE'][:]= np.nan
                
                ODS['pmaxabsisoidAE'] = np.empty(np.shape(SDS['pambairER']))
                ODS['pmaxabsisoidAE'][:]= np.nan
                
                ODS['TegEVoutisoidAE'] = np.empty(np.shape(SDS['pambairER']))
                ODS['TegEVoutisoidAE'][:]= np.nan
                
                ODS['TegTCinidAE'] = np.empty(np.shape(SDS['pambairER']))
                ODS['TegTCinidAE'][:]= np.nan
                
                ODS['TegTCoutidAE'] = np.empty(np.shape(SDS['pambairER']))
                ODS['TegTCoutidAE'][:]= np.nan
                
                #Fuel mode
                ODS['SFOCisoidAE'][SDS['FOModeAE']]=isf.IdealSFOC(SDS['MCRAE'][SDS['FOModeAE']],RDS['SFOCisocalcAEST'][foidx,:],RDS['MCRAEST'][foidx,:])
                ODS['pscavabsisoidAE'][SDS['FOModeAE']]=IST.IdealST(RDS['MCRAEST'][foidx,:],RDS['pscavabsisocalcAEST'][foidx,:],SDS['MCRAE'][SDS['FOModeAE']],0)
                ODS['NTCidAE'][SDS['FOModeAE']]=IST.IdealST(RDS['MCRAEST'][foidx,:],RDS['NTCAEST'][foidx,:],SDS['MCRAE'][SDS['FOModeAE']],0)
                ODS['pmaxabsisoidAE'][SDS['FOModeAE']]=IST.IdealST(RDS['MCRAEST'][foidx,:],RDS['pmaxabsisocalcAEST'][foidx,:],SDS['MCRAE'][SDS['FOModeAE']],0)
                ODS['TegEVoutisoidAE'][SDS['FOModeAE']]=IST.IdealST(RDS['MCRAEST'][foidx,:],RDS['TegEVoutisocalcAEST'][foidx,:],SDS['MCRAE'][SDS['FOModeAE']],100)
                ODS['TegTCinidAE'][SDS['FOModeAE']]=IST.IdealST(RDS['MCRAEST'][foidx,:],RDS['TegTCinAEST'][foidx,:],SDS['MCRAE'][SDS['FOModeAE']],120) 
                ODS['TegTCoutidAE'][SDS['FOModeAE']]=IST.IdealST(RDS['MCRAEST'][foidx,:],RDS['TegTCoutAEST'][foidx,:],SDS['MCRAE'][SDS['FOModeAE']],100)
            
                #Gas mode
                #Pilot
                ODS['SPOCisoidAE'][SDS['GOModeAE']]=isf.IdealSFOC(SDS['MCRAE'][SDS['GOModeAE']],RDS['SFOCisocalcAEST'][goidx,:],RDS['MCRAEST'][goidx,:])
        
                #Gas                                                             
                ODS['SGOCisoidAE'][SDS['GOModeAE']]=isf.IdealSFOC(SDS['MCRAE'][SDS['GOModeAE']],RDS['SGOCisocalcAEST'][goidx,:],RDS['MCRAEST'][goidx,:])
        
                #Embed SGOC+SPOC in SFOC vectors for a uniform terminology
                ODS['SFOCisoidAE'][SDS['GOModAE']]=ODS['SGOCisoidAE'][SDS['GOModeAE']]+ODS['SPOCisoidAE'][SDS['GOModeAE']]
                ODS['pscavabsisoidAE'][SDS['GOModeAE']]=IST.IdealST(RDS['MCRAEST'][goidx,:],RDS['pscavabsisocalcAEST'][goidx,:],SDS['MCRAE'][SDS['GOModeAE']],0)
                ODS['NTCidAE'][SDS['GOModeAE']]=IST.IdealST(RDS['MCRAEST'][goidx,:],RDS['NTCAEST'][goidx,:],SDS['MCRAE'][SDS['GOModeAE']],0)
                ODS['pmaxabsisoidAE'][SDS['GOModeAE']]=IST.IdealST(RDS['MCRAEST'][goidx,:],RDS['pmaxabsisocalcAEST'][goidx,:],SDS['MCRAE'][SDS['GOModeAE']],0)
                ODS['TegEVoutisoidAE'][SDS['GOModeAE']]=IST.IdealST(RDS['MCRAEST'][goidx,:],RDS['TegEVoutisocalcAEST'][goidx,:],SDS['MCRAE'][SDS['GOModeAE']],100)
                ODS['TegTCinidAE'][SDS['GOModeAE']]=IST.IdealST(RDS['MCRAEST'][goidx,:],RDS['TegTCinAEST'][goidx,:],SDS['MCRAE'][SDS['GOModeAE']],120) 
                ODS['TegTCoutidAE'][SDS['GOModeAE']]=IST.IdealST(RDS['MCRAEST'][goidx,:],RDS['TegTCoutAEST'][goidx,:],SDS['MCRAE'][SDS['GOModeAE']],100)
        if LogType=='Noon':
            SDS['FOCidME']=FOCidME
            SDS['PDid1']=PDid1
        
        # SRQST['TotalTimeLapse'].append(time.time()-start_time)
        # SRQST['Remarks'].append("complete") 
        # return [ODS,DS,SRQST]
    except Exception as err:
        #raise
        exp = "Error in OutputCalcultion in line No. {} for IMO {} and LogType {} and LogID-{} Error-{}".format(sys.exc_info()[-1].tb_lineno,IMO,LogType,SDS['LogID'],err)
        logger.error(exp,exc_info=True)
        ExceptionHandler(LogType,exp)
        SRQSTD['TotalTimeLapse']=time.time()-start_time              
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        
        return({},{},SRQST)
        
    
        
    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time               
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(ODS,DS,SRQST)