# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 16:21:31 2020

@author: SubhinAntony

% STAWAVE1
% Description:  This function implements the STAWAVE1 method described in
%               ISO15016:2015, Annex D.1. It estimates the resistance
%               increase in head waves provided that heave and pitch are
%               small.
%
% Input:        Lbp     [m]     length between perpendiculars (scalar)
%               B       [m]     ship breadth (scalar)
%               rhow    [kg/m3] water density (scalar)
%               g       [m/s2]  acceleration of gravity in  (scalar)
%               Hw      [m]     significant wave height (scalar)
%               wdir    [deg]   wave direction (scalar)
%               azbow   [g]     vertical acceleration at bow (scalar)
%
% Output:       DRw     [N]     mean resistance increase due to waves (scalar)
%
% Limitations:  Hw      <=   2.25*sqrt(Lbp/100)    
%               azbow   <   0.05g (small heave and pitch)
%               -45°    <=  wdir <= 45°
%
% See also:     STAWAVE,STAWAVE2

"""
import math 
import numpy as np
from BPME_Core import BowLength as bl
def STAWAVE1(Lbp,B,rhow,g,Hw):

    Lbwl=bl.BowLength(Lbp)  #Bow length estimation in [m]
    DRw=1/16*rhow*g*Hw**2*B*math.sqrt(B/Lbwl)    #Calculation of resistance increase in [N]
    return(DRw)
  
        