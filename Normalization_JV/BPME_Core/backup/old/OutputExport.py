# -*- coding: utf-8 -*-
"""
Created on Fri May  8 16:56:41 2020

@author: SubhinAntony

% OutputExport
% Description:  This function generates 2 or 3 CSV files depending on the
%               BPME type:
%               a) KPIs and calculated variables
%               b) Diagnostics
%               c) Reference curves
%               The data in all CSV files are imported in PAL database for
%               visualization, etc.
%
% Input:        RDS (structure)
%               SDS (structure)
%               ODS (structure)
%               DS (structure)
%               VesselObjectID (scalar)
%               Eidx (scalar)
%               LogType (string)
%               BPMEngineVersion (scalar)
%
% Output:       N/A
%
% See also:     BPMEngineSingleVessel, OutputCalculation, PowerEstimation


"""
import pandas as pd
import numpy as np
import sys
 
import time
from ExceptionHandler import ExceptionHandler
import logging 
# create logger
logger = logging.getLogger('BPMEngine.2.02')
def OutputExport(IMO,SDS,RDS,ODS,DS,VesselObjectID,Eidx,LogType,BPMEngineVersion,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
#        # Create name and path for the output/referemce/diagnostics csv files
        # outputfile1="output/{}{}Output_{}.csv".format(LogType,Eidx,VesselObjectID)
        # outputfile2="output/{}{}RefOutput_{}.csv".format(LogType,Eidx,VesselObjectID)  ##not needed 
        # outputfile3="output/{}{}Diagnostics_{}.csv".format(LogType,Eidx,VesselObjectID)
        # outputfileEX="output/{}{}Output_{}EX.csv".format(LogType,Eidx,VesselObjectID)
        
        
        
        
        # Create BPM Engine version vector
        BPMENGINEVERSION=np.empty(np.shape(SDS['pambairER']))
        BPMENGINEVERSION[:]=float('nan')
        BPMENGINEVERSION[:]=BPMEngineVersion
        
        IMO=np.empty(np.shape(SDS['LogID']))
        IMO[:]=int(RDS['IMONoVessel']) # This should be taken from the input in while implementation.
        LogID=SDS['LogID']
        DateTimeStart=SDS['tstart']
        DateTimeEnd=SDS['tend']
        LogDurationHRS=SDS['Dt']
        
        DS['IMO']=IMO
        DS['LogID']=LogID
        DS['DateTimeStart']=DateTimeStart
        DS['DateTimeEnd']=DateTimeEnd
        DS['LogDurationHRS']=LogDurationHRS
        DS['BPMENGINEVERSION']=BPMENGINEVERSION
        if LogType in ['Noon']:
            FOCidME=SDS['FOCidME']
            FOCLCVcorME=SDS['FOCLCVcorME']
            SFOCisoidME=ODS['SFOCisoidME']
            if isinstance(SFOCisoidME, float):
                SFOCisoidME=np.array([SFOCisoidME])
                # SFOCisoidME=np.empty(LogID.shape)
                # SFOCisoidME[:]=np.nan
            else:
                sis=[]
                for sfme in SFOCisoidME.ravel():
                    if type(sfme)==np.ndarray:
                        sis.append(sfme[0])
                    else:
                        sis.append(sfme)
                SFOCisoidME=np.array(sis)
            SFOCisoME=SDS['SFOCisoME']
            PDid1=SDS['PDid1']
            PD=SDS['PD']
           
            MCRMEPD=SDS['MCRMEPD']
            MCRME=np.around(SDS['MCRME'].astype('float64'),1)
            PDcor=SDS['PDcor'].astype('float64')
            DS['Report_Type']=SDS['Report_Type']
            Report_Type=SDS['Report_Type']
            SFOCMEActuals_avg=SDS['SFOCMEActuals']
            SFOCMEActuals=SDS['SFOCMEActuals_eng']
            SFOCAEActuals_avg=SDS['SFOCAEActuals']
            SFOCAEActuals=SDS['SFOCAEActuals_eng']
            DeltaTExhCylAE=SDS['DeltaTExhCylAE']
            DeltaTExhCylME=SDS['DeltaTExhCylME']
            DeltaNTCTCME=SDS['DeltaNTCTCME']
#            
#            DS['Report_Type']=SDS['Report_Type']
#            Report_Type=SDS['Report_Type']
#            SFOCMEActuals=SDS['SFOCMEActuals']
#            SFOCAEActuals=SDS['SFOCAEActuals']
#            DeltaTExhCylAE=SDS['DeltaTExhCylAE']
#            DeltaTExhCylME=SDS['DeltaTExhCylME']
#            DeltaNTCTCME=SDS['DeltaNTCTCME']
        #Define Noon/HP/ME/Auto output symbols   
        if LogType in ['Noon','HP','Auto','ME']:
            deltaSFOCisoME=np.around(ODS['deltaSFOCisoME'].astype('float64'),2)
            PeffME=np.round(SDS['PeffME'].astype('float64'))
        
        # Define Noon output symbols  
        if LogType in 'Noon':
            if RDS['nME']==1:
                SCOCME_set_1=np.nanmean(SDS['SCOCME_set'],1)
                SCOCcorME_set_1=np.around(ODS['SCOCcorME_set'],2)
                
                SCOCME_1=np.around(SDS['SCOCME'].astype('float64'),2)
                SCOCidealME_1=np.around(ODS['SCOCidME'],2)#temporary symbol change
                
                deltaNTCME_1=np.around(ODS['deltaNTCME'].astype('float64'),1)
                deltaTegEVoutisoME_1=np.around(ODS['deltaTegEVoutisoME'].astype('float64'),1)
                deltapscavabsisoME_1=np.around(ODS['deltapscavabsisoME'].astype('float64'),1)
            else:
                SCOCME_set=np.nanmean(SDS['SCOCME_set'],1)
                SCOCcorME_set=np.around(ODS['SCOCcorME_set'],2)
                
                SCOCME=np.around(SDS['SCOCME'].astype('float64'),2)
                SCOCidealME=np.around(ODS['SCOCidME'],2)#temporary symbol change
                
                deltaNTCME=np.around(ODS['deltaNTCME'],1)
                deltaTegEVoutisoME=np.around(ODS['deltaTegEVoutisoME'].astype('float64'),1)
                deltapscavabsisoME=np.around(ODS['deltapscavabsisoME'].astype('float64'),1)
            
            MCRAE=np.around(SDS['MCRAE'].astype('float64'),1)
            AEU=np.around(ODS['AEU'].astype('float64'))
            deltaTegEVoutisoAE=np.around(ODS['deltaTegEVoutisoAE'].astype('float64'),1)
            deltaTegTCinisoAE=np.around(ODS['deltaTegTCinisoAE'].astype('float64'),1)
            deltaTegTCoutisoAE=np.around(ODS['deltaTegTCoutisoAE'].astype('float64'),1)
            
        # Define Noon/HP/Auto output symbols
        if LogType in ['Noon','HP','Auto']:
            if RDS['nME']==1:
                MCRME_1=np.around(SDS['MCRME'].astype('float64'),1)
                #PeffME_1=np.round(SDS['PeffME'])
                PeffME_1=np.round(SDS['PeffME'].astype('float64'))
                PeffLightidME_1=np.around(ODS['PeffLightidME'])
                NNME_1=np.around(SDS['NME'].astype('float64'),2)#this is a service variable and has a bad symbol to export; it must be improved...
            else:
                MCRME=np.around(SDS['MCRME'].astype('float64'),1)
                PeffME=np.around(SDS['PeffME'].astype('float64'))
                PeffLightidME=np.around(ODS['PeffLightidME'])
                NNME=np.around(SDS['NME'].astype('float64'),2)#this is a service variable and has a bad symbol to export; it must be improved...
                
                
            SpeedDrop=np.around(ODS['SpeedDrop'].astype('float64'),1)
            ApparentSlip=np.around(ODS['ApparentSlip'].astype('float64'),1)
            deltaPD1=np.around(ODS['deltaPD1'].astype('float64'),1)
            deltaPD2=np.around(ODS['deltaPD2'].astype('float64'),1)
            deltaN=np.around(ODS['deltaN'].astype('float64'),1)
            deltaFOCME=np.around(ODS['deltaFOCME'].astype('float64'),1)
            Ucor=np.around(SDS['Ucor'].astype('float64'),1)
            NPropcor=np.around(SDS['NPropcor'].astype('float64'),1)
            WeatherIndex=np.around(ODS['WeatherIndex'].astype('float64'),1)
            LogValidity=ODS['LogValidity']
        # Define Noon/ME output symbols 
    #    if LogType in ['Noon','ME']:
    #        deltaNTCME=np.around(ODS['deltaNTCME'].astype('float64'),1)
    #        deltaTegEVoutisoME=np.around(ODS['deltaTegEVoutisoME'].astype('float64'),1)
    #        deltapscavabsisoME=np.around(ODS['deltapscavabsisoME'].astype('float64'),1)
    #    # Define ME output symbols
        if LogType =='ME':
            ME=np.empty(np.shape(SDS['LogID']))
            ME[:]=int(Eidx)
            #General
            MCRME=np.around(SDS['MCRME'].astype('float64'),1)
            PeffME=np.around(SDS['PeffME'].astype('float64'))
            PeffLightidME=np.around(ODS['PeffLightidME'])
            SFOCisoME=np.around(SDS['SFOCisoME'].astype('float64'),2)
            pscavabsisoME=np.around(SDS['pscavabsisoME'].astype('float64'),2)
            TegEVoutisoME=np.around(SDS['TegEVoutisoME'].astype('float64'))
            NTCisoME=np.around(SDS['NTCME'].astype('float64')) #this must be upgraded with the iso correction
            NNME=np.around(SDS['NME'].astype('float64'),2) #this is a service variable and has a bad symbol to export it must be improved
            FPIisoME=np.around(SDS['FPIME'].astype('float64'),1) #this must be upgraded with the iso correction
            pindME=np.around(SDS['pindME'].astype('float64'),2)
            pmaxabsisoME=np.around(SDS['pmaxabsisoME'].astype('float64'),1)
            pcompabsisoME=np.around(SDS['pcompabsisoME'].astype('float64'),1)
            pmax_pcompisoME=np.around(SDS['pmax_pcompisoME'].astype('float64'),1)
            pcomp_pscavabsisoME=np.around(SDS['pcomp_pscavabsisoME'].astype('float64'),1)
            dpairACME=np.around(SDS['dpairACME'].astype('float64'),1)
            dpairAFME=np.around(SDS['dpairAFME'].astype('float64'),1)
            TegTCinisoME=np.around(SDS['TegTCinME'].astype('float64')) #this must be upgraded with the iso correction
            TegTCoutisoME=np.around(SDS['TegTCoutME'].astype('float64')) #this must be upgraded with the iso correction
            dTcwACME=np.around(SDS['dTcwACME'].astype('float64'))
            dTaircwACME=np.around(SDS['dTaircwACME'].astype('float64'))
            etaTCcompME=np.around(SDS['etaTCcompcalcME'].astype('float64'),1)
            etaTCturbME=np.around(SDS['etaTCturbcalcME'].astype('float64'),1)
            
            #Ideal
            if isinstance(ODS['SFOCisoidME'],np.ndarray):
                SFOCisoidME=np.around(ODS['SFOCisoidME'].astype('float64'),2)
            else:
                SFOCisoidME=np.around(ODS['SFOCisoidME'],2) 
            pscavabsisoidME=np.around(ODS['pscavabsisoidME'].astype('float64'),2)
            TegEVoutisoidME=np.around(ODS['TegEVoutisoidME'].astype('float64'))
            NTCisoidME=np.around(ODS['NTCidME'].astype('float64')) #this must be upgraded with the iso correction
            NidME=np.around(ODS['NidME'].astype('float64'))
            FPIisoidME=np.around(ODS['FPIidME'].astype('float64'),1) #this must be upgraded with the iso correction
            pindidME=np.around(ODS['pindidME'].astype('float64'),2)
            pmaxabsisoidME=np.around(ODS['pmaxabsisoidME'].astype('float64'),1)
            pcompabsisoidME=np.around(ODS['pcompabsisoidME'].astype('float64'),1)
            pmax_pcompisoidME=np.around(ODS['pmax_pcompisoidME'].astype('float64'),1)
            pcomp_pscavabsisoidME=np.around(ODS['pcomp_pscavabsisoidME'].astype('float64'),1)
            dpairACidME=np.around(ODS['dpairACidME'].astype('float64'),1)
            dpairAFidME=np.around(ODS['dpairAFidME'].astype('float64'),1)
            TegTCinisoidME=np.around(ODS['TegTCinidME'].astype('float64')) #this must be upgraded with the iso correction
            TegTCoutisoidME=np.around(ODS['TegTCoutidME'].astype('float64')) #this must be upgraded with the iso correction
            dTcwACidME=np.around(ODS['dTcwACidME'].astype('float64'))
            dTaircwACidME=np.around(ODS['dTaircwACidME'].astype('float64'))
            etaTCcompidME=np.around(ODS['etaTCcompidME'].astype('float64'),1)
            etaTCturbidME=np.around(ODS['etaTCturbidME'].astype('float64'),1)
            
            #Per cyl
            TegEVoutisocylME=np.around(SDS['TegEVoutisocylME'].astype('float64'))
            FPIisocylME=np.around(SDS['FPIcylME'].astype('float64'),1) #this must be upgraded with the iso correction
            pmaxabsisocylME=np.around(SDS['pmaxabsisocylME'].astype('float64'),1)
            pcompabsisocylME=np.around(SDS['pcompabsisocylME'].astype('float64'),1)
            
            #Per TC
            if np.all(RDS['nTCME']==1):
                NTCisoTCME_1=np.around(SDS['NTCTCME'].astype('float64')) #this must be upgraded with the iso correction
                dpairACTCME_1=np.around(SDS['dpairACTCME'],1);
                dpairAFTCME_1=np.around(SDS['dpairAFTCME'],1);
                TegTCinisoTCME_1=np.around(SDS['TegTCinTCME'].astype('float64')); #this must be upgraded with the iso correction
                TegTCoutisoTCME_1=np.around(SDS['TegTCoutTCME'].astype('float64')); #this must be upgraded with the iso correction
                dTcwACTCME_1=np.around(SDS['dTcwACTCME'].astype('float64'));
                dTaircwACTCME_1=np.around(SDS['dTaircwACTCME'].astype('float64'));
                etaTCcompTCME_1=np.around(SDS['etaTCcompcalcTCME'].astype('float64'),1);
                etaTCturbTCME_1=np.around(SDS['etaTCturbcalcTCME'].astype('float64'),1);
            else:
                NTCisoTCME=np.around(SDS['NTCTCME'].astype('float64')) #this must be upgraded with the iso correction
                dpairACTCME=np.around(SDS['dpairACTCME'].astype('float64'),1)
                dpairAFTCME=np.around(SDS['dpairAFTCME'].astype('float64'),1)
                TegTCinisoTCME=np.around(SDS['TegTCinTCME'].astype('float64')) #this must be upgraded with the iso correction
                TegTCoutisoTCME=np.around(SDS['TegTCoutTCME'].astype('float64')) #this must be upgraded with the iso correction
                dTcwACTCME=np.around(SDS['dTcwACTCME'].astype('float64'))
                dTaircwACTCME=np.around(SDS['dTaircwACTCME'].astype('float64'))
                etaTCcompTCME=np.around(SDS['etaTCcompcalcTCME'].astype('float64'),1)
                etaTCturbTCME=np.around(SDS['etaTCturbcalcTCME'].astype('float64'),1)
            
            #Validity   
            LogValidityME=ODS['LogValidityME'].astype('float64')
        
    
        #Define AE output symbols                                                
        if LogType in 'AE':
            AE=np.empty(np.shape(SDS['LogID']))
            AE[:]=int(Eidx)
        
            #General
            MCRAE=np.around(SDS['MCRAE'].astype('float64'),1);
            SFOCisoAE=np.around(SDS['SFOCisoAE'].astype('float64'),2);
            pscavabsisoAE=np.around(SDS['pscavabsisoAE'].astype('float64'),2);
            TegEVoutisoAE=np.around(SDS['TegEVoutisoAE'].astype('float64'));
            NTCisoAE=np.around(SDS['NTCAE'].astype('float64')); #this must be upgraded with the iso correction
            pmaxabsisoAE=np.around(SDS['pmaxabsisoAE'].astype('float64'),1);
            TegTCinisoAE=np.around(SDS['TegTCinAE'].astype('float64')); #this must be upgraded with the iso correction
            TegTCoutisoAE=np.around(SDS['TegTCoutAE'].astype('float64')); #this must be upgraded with the iso correction
            
            #Ideal
            SFOCisoidAE=np.around(ODS['SFOCisoidAE'].astype('float64'),2);
            pscavabsisoidAE=np.around(ODS['pscavabsisoidAE'].astype('float64'),2);
            TegEVoutisoidAE=np.around(ODS['TegEVoutisoidAE'].astype('float64'));
            NTCisoidAE=np.around(ODS['NTCidAE'].astype('float64')); #this must be upgraded with the iso correction
            pmaxabsisoidAE=np.around(ODS['pmaxabsisoidAE'].astype('float64'),1);
            TegTCinisoidAE=np.around(ODS['TegTCinidAE'].astype('float64')); #this must be upgraded with the iso correction
            TegTCoutisoidAE=np.around(ODS['TegTCoutidAE']); #this must be upgraded with the iso correction
            
            #Per cyl
            TegEVoutisocylAE=np.around(SDS['TegEVoutisocylAE'].astype('float64'));
            pmaxabsisocylAE=np.around(SDS['pmaxabsisocylAE'].astype('float64'),1);
            
            #Per TC
            if np.all(RDS['nTCAE'])==1:
                NTCisoTCAE_1=np.around(SDS['NTCTCAE'].astype('float64')) #this must be upgraded with the iso correction
                TegTCinisoTCAE_1=np.around(SDS['TegTCinTCAE'].astype('float64')); #this must be upgraded with the iso correction
                TegTCoutisoTCAE_1=np.around(SDS['TegTCoutTCAE'].astype('float64')); #this must be upgraded with the iso correction
            else:
                NTCisoTCAE=np.around(SDS['NTCTCAE'].astype('float64')) #this must be upgraded with the iso correction
                TegTCinisoTCAE=np.around(SDS['TegTCinTCAE'].astype('float64')) #this must be upgraded with the iso correction
                TegTCoutisoTCAE=np.around(SDS['TegTCoutTCAE'].astype('float64')) #this must be upgraded with the iso correction
            
            
            #Validity
            LogValidityAE=ODS['LogValidityAE'];
    
        # Noon Exports 
        if LogType =='Noon':
            OutputTableEXT=dict(LogID=LogID.ravel(),
                                 IMO=IMO.ravel().astype(int),
                                 DateTimeStart=DateTimeStart.ravel(),
                                 DateTimeEnd=DateTimeEnd.ravel(),
                                 LogDurationHRS=LogDurationHRS.ravel(),
                                 Report_Type=Report_Type.ravel(),
                                 SFOCMEActuals_avg=SFOCMEActuals_avg.ravel(),
                                 SFOCMEActuals=pd.DataFrame(SFOCMEActuals).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                 SFOCAEActuals_avg=SFOCAEActuals_avg.ravel(),
                                 SFOCAEActuals=pd.DataFrame(SFOCAEActuals).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                 MCRME=pd.DataFrame(MCRME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                 Ucor=Ucor.ravel(),
                                 PDcor=PDcor.ravel(),
                                 NPropcor = NPropcor.ravel(),
                                 MCRMEPD=pd.DataFrame(MCRMEPD).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                 FOCidME=FOCidME.ravel(),
                                FOCLCVcorME=FOCLCVcorME.ravel(),
                                SFOCisoidME=SFOCisoidME.ravel(),
                                SFOCisoME=SFOCisoME.ravel(),
                                PDid1=PDid1.ravel(),
                                PD=PD.ravel()
                                    # DeltaTExhCylAE=DeltaTExhCylAE,
                                    # DeltaTExhCylME=DeltaTExhCylME,
                                    # DeltaNTCTCME=DeltaNTCTCME
                                    )
            OutputTableEXT=pd.DataFrame.from_dict(OutputTableEXT)
            cll=['SFOCMEActuals','SFOCAEActuals','MCRME','MCRMEPD']
            for iii in cll  :
                if iii in OutputTableEXT.columns:
                    OutputTableEXTD=OutputTableEXT[iii].str.split(",",expand=True).rename(columns=lambda x:x+1).add_prefix(iii+'_').astype('float64')
                    OutputTableEXT = pd.concat([OutputTableEXT, OutputTableEXTD], axis = 1)
                    OutputTableEXT.drop([iii], axis = 1, inplace = True) 
 
            cl_nm=[]
            ote_list=['DeltaTExhCylAE','DeltaTExhCylME','DeltaNTCTCME']
            k=0
            
            for cl_ot in ote_list:
                l=0
                
                sh=eval(cl_ot).shape[1]
                for ar in np.array_split(eval(cl_ot).transpose(1,0,2),sh):
                    if k==0:
                        ote_df=pd.DataFrame(pd.DataFrame(np.mean(ar,axis=0)).apply(lambda x: ','.join(x.astype(str)),axis=1))
                        k=k+1
                        l=l+1
                        cl_nm.append(cl_ot+'_'+str(l))
                    else:
                        ote_dft=pd.DataFrame(pd.DataFrame(np.mean(ar,axis=0)).apply(lambda x: ','.join(x.astype(str)),axis=1))
                        ote_df=pd.concat([ote_df, ote_dft], axis = 1)
                        l=l+1
                        cl_nm.append(cl_ot+'_'+str(l))
            ote_df.columns=cl_nm
            
            # cll=['DeltaTExhCylAE','DeltaTExhCylME','DeltaNTCTCME']
            for iii in cl_nm  :
                if iii in ote_df.columns:
                    ote_dfD=ote_df[iii].str.split(",",expand=True).rename(columns=lambda x:x+1).add_prefix(iii+'_').astype('float64')
                    ote_df = pd.concat([ote_df, ote_dfD], axis = 1)
                    ote_df.drop([iii], axis = 1, inplace = True)
                
            OutputTableEXT = pd.concat([OutputTableEXT, ote_df], axis = 1)
            # OutputTableEXT.to_csv(outputfileEX, index=False)
            OutputTableEXT.rename(columns = {'SFOCAEActuals_avg':'SFOCAEActuals','SFOCMEActuals_avg':'SFOCMEActuals'}, inplace = True)
            ExOutput=OutputTableEXT
                        
                    
            if RDS['nME']==1:
                OutputTable=dict(LogID=LogID.ravel(),
                                 IMO=IMO.ravel().astype(int),
                                 
                                 DateTimeStart=DateTimeStart.ravel(),
                                 DateTimeEnd=DateTimeEnd.ravel(),
                                 LogDurationHRS=LogDurationHRS.ravel(),
                                 Report_Type=Report_Type.ravel(),
                                 SpeedDrop=SpeedDrop.ravel(),
                                  ApparentSlip= ApparentSlip.ravel(),
                                  deltaPD1 = deltaPD1.ravel(),
                                  deltaPD2 = deltaPD2.ravel(),
                                  deltaN = deltaN.ravel(),
                                  deltaFOCME = deltaFOCME.ravel(),
                                  deltaSFOCisoME= deltaSFOCisoME.ravel(),
                                  deltaNTCME_1 = deltaNTCME_1.ravel(),
                                  deltaTegEVoutisoME_1 = deltaTegEVoutisoME_1.ravel(),
                                  deltapscavabsisoME_1 = deltapscavabsisoME_1.ravel(),
                                  Ucor = Ucor.ravel(),
                                  NPropcor = NPropcor.ravel() ,
                                  MCRME_1 = MCRME_1.ravel(),
                                  PeffME_1=PeffME_1.ravel(),
                                  PeffLightidME_1=PeffLightidME_1.ravel(),
                                  NNME_1=NNME_1.ravel(),
                                  SCOCcorME_set_1=SCOCcorME_set_1.ravel(),
                                  SCOCME_set_1=SCOCME_set_1.ravel(),
                                  SCOCME_1=SCOCME_1.ravel(),
                                  SCOCidealME_1=SCOCidealME_1.ravel(),
                                  AEU = AEU.ravel(),
                                  MCRAE = pd.DataFrame(MCRAE).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  deltaTegEVoutisoAE =pd.DataFrame(deltaTegEVoutisoAE).apply(lambda x: ','.join(x.astype(str)),axis=1) ,
                                  deltaTegTCinisoAE = pd.DataFrame(deltaTegTCinisoAE).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  deltaTegTCoutisoAE = pd.DataFrame(deltaTegTCoutisoAE).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  WeatherIndex = WeatherIndex.ravel(),
                                  LogValidity = LogValidity.ravel(),
                                  BPMENGINEVERSION = BPMENGINEVERSION.ravel())
            
            #for multiuple MEs
            else:    
                OutputTable=dict(LogID=LogID.ravel(),
                                 IMO=IMO.ravel().astype(int),
                                 
                                 DateTimeStart=DateTimeStart.ravel(),
                                 DateTimeEnd=DateTimeEnd.ravel(),
                                 LogDurationHRS=LogDurationHRS.ravel(),
                                 Report_Type=Report_Type.ravel(),
                                  SpeedDrop=SpeedDrop.ravel(),
                                  ApparentSlip=ApparentSlip.ravel(),
                                  deltaPD1=deltaPD1.ravel(),
                                  deltaPD2=deltaPD2.ravel(),
                                  deltaN=deltaN.ravel(),
                                  deltaFOCME=deltaFOCME.ravel(),
                                  deltaSFOCisoME=deltaSFOCisoME.ravel(),
                                  deltaNTCME=pd.DataFrame(deltaNTCME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  deltaTegEVoutisoME=pd.DataFrame(deltaTegEVoutisoME).apply(lambda x: ','.join(x.astype(str)),axis=1) ,
                                  deltapscavabsisoME=pd.DataFrame(deltapscavabsisoME).apply(lambda x: ','.join(x.astype(str)),axis=1) ,
                                  Ucor=Ucor.ravel(),
                                  NPropcor=NPropcor.ravel(),
                                  MCRME=pd.DataFrame(MCRME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  PeffME=pd.DataFrame(PeffME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  
                                  PeffLightidME=pd.DataFrame(PeffLightidME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  NNME=pd.DataFrame(NNME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  
                                  SCOCcorME_set=pd.DataFrame(SCOCcorME_set).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  SCOCME_set=pd.DataFrame(SCOCME_set).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  
                                  SCOCME=pd.DataFrame(SCOCME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  SCOCidealME=pd.DataFrame(SCOCidealME).apply(lambda x: ','.join(x.astype(str)),axis=1),#temporary symbol change
                                  
                                  MCRAE = pd.DataFrame(MCRAE).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  AEU = AEU.ravel(),
                                  deltaTegEVoutisoAE = pd.DataFrame(deltaTegEVoutisoAE).apply(lambda x: ','.join(x.astype(str)),axis=1) ,
                                  deltaTegTCinisoAE = pd.DataFrame(deltaTegTCinisoAE).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  deltaTegTCoutisoAE = pd.DataFrame(deltaTegTCoutisoAE).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  WeatherIndex=WeatherIndex.ravel(),
                                  LogValidity=LogValidity.ravel(),
                                  BPMENGINEVERSION=BPMENGINEVERSION.ravel())
            OutputTable=pd.DataFrame.from_dict(OutputTable) 
            cll=['deltaNTCME','deltaTegEVoutisoME','deltapscavabsisoME','MCRME','MCRAE','deltaTegEVoutisoAE','deltaTegTCinisoAE','deltaTegTCoutisoAE','PeffME','PeffLightidME','NNME','SCOCcorME_set','SCOCME_set','SCOCME','SCOCidealME']
            for iii in cll  :
                if iii in OutputTable.columns:
                    OutputTableD=OutputTable[iii].str.split(",",expand=True).rename(columns=lambda x:x+1).add_prefix(iii+'_').astype('float64')
                    OutputTable = pd.concat([OutputTable, OutputTableD], axis = 1)
                    OutputTable.drop([iii], axis = 1, inplace = True)
            
            
            # OutputTable.to_csv(outputfile1, index=False)
            Output=OutputTable
            
            
            #Generate reference curves csv
            #RefDashboards(RDS)
            #RefDashTable=rfd.RefDashboards(RDS)
            # RefDashTable['IMO']=np.empty(np.shape(RefDashTable['UrefDash']))
            # RefDashTable['IMO']=int(RDS['IMONoVessel'])
            #RefDashTable=pd.DataFrame.from_dict(RefDashTable)
            # RefDashTable.to_csv(outputfile2, index=False)
            # RefOutput=''
            #Generate diagnostics csv
            # LogType=SDS.Report_Type;
            # DateTimeEnd=SDS.tend;
            DiagnosticsTable=DS
            DiagnosticsTable['LogValidity'] = LogValidity
            Kyy=['MissingPengAE','MissingPSME','PeffMEDiscrepancy','PeffestMEUsage','MissingPelAE']
            
            for ky in DiagnosticsTable.keys():
                if (ky in DiagnosticsTable.keys()) and not(ky in Kyy):
                    DiagnosticsTable[ky]=DiagnosticsTable[ky].ravel()
                if ky in Kyy:
                    DiagnosticsTable[ky]=pd.DataFrame(DiagnosticsTable[ky]).apply(lambda x: ','.join(x.astype(str)),axis=1)
                    DiagnosticsTable[ky]=DiagnosticsTable[ky].ravel()
                    
            DiagnosticsTable=pd.DataFrame.from_dict(DiagnosticsTable)
            cll=['MissingPengAE','MissingPSME','PeffMEDiscrepancy','PeffestMEUsage','MissingPelAE']
            for iii in cll  :
                if iii in DiagnosticsTable.columns:
                    DiagnosticsTableD=DiagnosticsTable[iii].str.split(",",expand=True).rename(columns=lambda x:x+1).add_prefix(iii+'_')
                    DiagnosticsTable = pd.concat([DiagnosticsTable, DiagnosticsTableD], axis = 1)
                    DiagnosticsTable.drop([iii], axis = 1, inplace = True)
            # DiagnosticsTable.to_csv(outputfile3, index=False)
            if RDS['nME']==1:
                DiagnosticsTable.rename(columns = {'MissingPSME_1':'MissingPSME', 'PeffMEDiscrepancy_1':'PeffMEDiscrepancy',
                                  'PeffestMEUsage_1':'PeffestMEUsage'}, inplace = True)
            Diagnostics=DiagnosticsTable
        
        # HP Exports  
        elif LogType in ['HP']:
            #Generate Output csv
            if RDS['nME']==1:
                OutputTable=dict(LogID = LogID.ravel(),
                                 IMO=IMO.ravel().astype(int),
                                 
                                 DateTimeStart=DateTimeStart.ravel(),
                                 DateTimeEnd=DateTimeEnd.ravel(),
                                 LogDurationHRS=LogDurationHRS.ravel(),
                                 SpeedDrop = SpeedDrop.ravel(),
                                 ApparentSlip = ApparentSlip.ravel(),
                                 deltaPD1 = deltaPD1.ravel(),
                                 deltaPD2 = deltaPD2.ravel(),
                                 deltaN = deltaN.ravel(),
                                 deltaFOCME =deltaFOCME.ravel() ,
                                 deltaSFOCisoME = deltaSFOCisoME.ravel(),
                                 Ucor = Ucor.ravel(),
                                 NPropcor = NPropcor.ravel(),
                                 MCRME_1 = MCRME_1.ravel(),
                                 
                                 PeffME_1=PeffME_1.ravel(),
                                 PeffLightidME_1=PeffLightidME_1.ravel(),
                                 NNME_1= NNME_1.ravel(),
                                 
                                 WeatherIndex = WeatherIndex.ravel(),
                                 LogValidity = LogValidity.ravel(),
                                 BPMENGINEVERSION = BPMENGINEVERSION.ravel())
            else:     
                OutputTable=dict(LogID=LogID.ravel(),
                                 IMO=IMO.ravel().astype(int),
                                 
                                 DateTimeStart=DateTimeStart.ravel(),
                                 DateTimeEnd=DateTimeEnd.ravel(),
                                 LogDurationHRS=LogDurationHRS.ravel(),
                                  SpeedDrop=SpeedDrop.ravel(),
                                  ApparentSlip=ApparentSlip.ravel(),
                                  deltaPD1=deltaPD1.ravel(),
                                  deltaPD2=deltaPD2.ravel(),
                                  deltaN=deltaN.ravel(),
                                  deltaFOCME=deltaFOCME.ravel(),
                                  deltaSFOCisoME=deltaSFOCisoME.ravel(),
                                  Ucor=Ucor.ravel(),
                                  NPropcor=NPropcor.ravel(),
                                  MCRME=pd.DataFrame(MCRME).apply(lambda x: ','.join(x.astype(str)),axis=1) ,
                                   
                                  PeffME=pd.DataFrame(PeffME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  PeffLightidME=pd.DataFrame(PeffLightidME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  NNME= pd.DataFrame(NNME).apply(lambda x: ','.join(x.astype(str)),axis=1),
#                                  
                                  WeatherIndex=WeatherIndex.ravel(),
                                  LogValidity=LogValidity.ravel(),
                                  BPMENGINEVERSION=BPMENGINEVERSION.ravel())
            OutputTable=pd.DataFrame.from_dict(OutputTable)
            
            cll=['MCRME','PeffME','PeffLightidME','NNME']
            for iii in cll  :
                if iii in OutputTable.columns:
                    OutputTableD=OutputTable[iii].str.split(",",expand=True).rename(columns=lambda x:x+1).add_prefix(iii+'_').astype('float64')
                    OutputTable = pd.concat([OutputTable, OutputTableD], axis = 1)
                    OutputTable.drop([iii], axis = 1, inplace = True)
            # OutputTable.to_csv(outputfile1, index=False)
            Output=OutputTable
            #Generate reference curves csv
            #RefDashboards(RDS)
            # RefDashTable=rfd.RefDashboards(RDS)
            # RefDashTable=pd.DataFrame.from_dict(RefDashTable)
            # RefDashTable.to_csv(outputfile2, index=False)
            # RefOutput=''
            #Generate diagnostics csv
            DiagnosticsTable=DS
            DiagnosticsTable['LogValidity'] = LogValidity
            Kyy=["MissingPSME","PeffMEDiscrepancy","PeffestMEUsage"]
            
            for ky in DiagnosticsTable.keys():
                if (ky in DiagnosticsTable.keys()) and not(ky in Kyy):
                    DiagnosticsTable[ky]=DiagnosticsTable[ky].ravel()
                if ky in Kyy:
                    DiagnosticsTable[ky]=pd.DataFrame(DiagnosticsTable[ky]).apply(lambda x: ','.join(x.astype(str)),axis=1)
                    DiagnosticsTable[ky]=DiagnosticsTable[ky].ravel()
                    
            DiagnosticsTable=pd.DataFrame.from_dict(DiagnosticsTable)
            cll=["MissingPSME","PeffMEDiscrepancy","PeffestMEUsage"]
            for iii in cll  :
                if iii in DiagnosticsTable.columns:
                    DiagnosticsTableD=DiagnosticsTable[iii].str.split(",",expand=True).rename(columns=lambda x:x+1).add_prefix(iii+'_')
                    DiagnosticsTable = pd.concat([DiagnosticsTable, DiagnosticsTableD], axis = 1)
                    DiagnosticsTable.drop([iii], axis = 1, inplace = True)
            # DiagnosticsTable.to_csv(outputfile3, index=False)
            if RDS['nME']==1:
                DiagnosticsTable.rename(columns = {'MissingPSME_1':'MissingPSME', 'PeffMEDiscrepancy_1':'PeffMEDiscrepancy',
                                  'PeffestMEUsage_1':'PeffestMEUsage'}, inplace = True)
            Diagnostics=DiagnosticsTable
            ExOutput=''
        
        #Auto Export
        elif LogType=='Auto':
            if RDS['nME']==1:
                OutputTable=dict(LogID=LogID.ravel(),
                                 IMO=IMO.ravel().astype(int),
                                 
                                 DateTimeStart=DateTimeStart.ravel(),
                                 DateTimeEnd=DateTimeEnd.ravel(),
                                 LogDurationHRS=LogDurationHRS.ravel(),
                                 SpeedDrop= SpeedDrop.ravel(),
                                 ApparentSlip = ApparentSlip.ravel(),
                                 deltaPD1=deltaPD1.ravel(),
                                 deltaPD2=deltaPD2.ravel(),
                                 deltaN=deltaN.ravel(),
                                 deltaFOCME = deltaFOCME.ravel(),
                                 deltaSFOCisoME = deltaSFOCisoME.ravel(),
                                 Ucor = Ucor.ravel(),
                                 NPropcor=NPropcor.ravel(),
                                 MCRME_1 = MCRME_1.ravel(),
                                 PeffME_1=PeffME_1.ravel(),
                                 PeffLightidME_1=PeffLightidME_1.ravel(),
                                 NNME_1=NNME_1.ravel(),
                                 LogValidity =LogValidity.ravel() ,
                                 BPMENGINEVERSION =BPMENGINEVERSION.ravel() )
            else:
                OutputTable=dict(LogID=LogID.ravel(),
                                 IMO=IMO.ravel().astype(int),
                                 
                                 DateTimeStart=DateTimeStart.ravel(),
                                 DateTimeEnd=DateTimeEnd.ravel(),
                                 LogDurationHRS=LogDurationHRS.ravel(),
                                 SpeedDrop = SpeedDrop.ravel(),
                                 ApparentSlip = ApparentSlip.ravel(),
                                 deltaPD1 = deltaPD1.ravel(),
                                 deltaPD2 = deltaPD2.ravel(),
                                 deltaN =deltaN.ravel() ,
                                 deltaFOCME= deltaFOCME.ravel(),
                                 deltaSFOCisoME = deltaSFOCisoME.ravel(),
                                 Ucor = Ucor.ravel(),
                                 NPropcor = NPropcor.ravel(),
                                 MCRME = pd.DataFrame(MCRME).apply(lambda x: ','.join(x.astype(str)),axis=1) ,
                                 PeffME=pd.DataFrame(PeffME).apply(lambda x: ','.join(x.astype(str)),axis=1) ,
                                 PeffLightidME=pd.DataFrame(PeffLightidME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                 NNME=pd.DataFrame(NNME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                 LogValidity = LogValidity.ravel(),
                                 BPMENGINEVERSION = BPMENGINEVERSION.ravel())
            OutputTable=pd.DataFrame.from_dict(OutputTable)
            cll=["MCRME","PeffME","PeffLightidME","NNME"]
            for iii in cll  :
                if iii in OutputTable.columns:
                    OutputTableD=OutputTable[iii].str.split(",",expand=True).rename(columns=lambda x:x+1).add_prefix(iii+'_').astype('float64')
                    OutputTable = pd.concat([OutputTable, OutputTableD], axis = 1)
                    OutputTable.drop([iii], axis = 1, inplace = True)
            # OutputTable.to_csv(outputfile1, index=False)
            Output=OutputTable
            
            
            #Generate reference curves csv
            #RefDashboards(RDS)
            # RefDashTable=rfd.RefDashboards(RDS)
            # RefDashTable=pd.DataFrame.from_dict(RefDashTable)
            # RefDashTable.to_csv(outputfile2, index=False)
            # RefOutput=''
            #Generate diagnostics csv
            DiagnosticsTable=DS
            DiagnosticsTable['LogValidity'] = LogValidity
            # for ky in DiagnosticsTable.keys():
            #     DiagnosticsTable[ky]=DiagnosticsTable[ky].ravel()
            # DiagnosticsTable=pd.DataFrame.from_dict(DiagnosticsTable)
            # # DiagnosticsTable.to_csv(outputfile3, index=False)
            Kyy=['MissingPengAE','MissingPSME','PeffMEDiscrepancy','PeffestMEUsage','MissingPelAE']
            
            for ky in DiagnosticsTable.keys():
                if (ky in DiagnosticsTable.keys()) and not(ky in Kyy):
                    DiagnosticsTable[ky]=DiagnosticsTable[ky].ravel()
                if ky in Kyy:
                    DiagnosticsTable[ky]=pd.DataFrame(DiagnosticsTable[ky]).apply(lambda x: ','.join(x.astype(str)),axis=1)
                    DiagnosticsTable[ky]=DiagnosticsTable[ky].ravel()
                    
            DiagnosticsTable=pd.DataFrame.from_dict(DiagnosticsTable)
            cll=['MissingPengAE','MissingPSME','PeffMEDiscrepancy','PeffestMEUsage','MissingPelAE']
            for iii in cll  :
                if iii in DiagnosticsTable.columns:
                    DiagnosticsTableD=DiagnosticsTable[iii].str.split(",",expand=True).rename(columns=lambda x:x+1).add_prefix(iii+'_')
                    DiagnosticsTable = pd.concat([DiagnosticsTable, DiagnosticsTableD], axis = 1)
                    DiagnosticsTable.drop([iii], axis = 1, inplace = True)
            # DiagnosticsTable.to_csv(outputfile3, index=False)
            if RDS['nME']==1:
                DiagnosticsTable.rename(columns = {'MissingPSME_1':'MissingPSME', 'PeffMEDiscrepancy_1':'PeffMEDiscrepancy',
                                  'PeffestMEUsage_1':'PeffestMEUsage'}, inplace = True)
            Diagnostics=DiagnosticsTable
            ExOutput=''
                
        elif LogType=='ME':
            #Generate Output csv
            if np.all(RDS['nTCME']==1):
                OutputTable=dict(LogID=LogID.ravel(),
                                IMO=IMO.ravel().astype(int),
                                ME=ME.ravel().astype(int),
                                
                                 DateTimeStart=DateTimeStart.ravel(),
                                 DateTimeEnd=DateTimeEnd.ravel(),
                                 LogDurationHRS=LogDurationHRS.ravel(),
                                  MCRME=MCRME.ravel(),
                                  PeffME=PeffME.ravel(),
                                  PeffLightidME=PeffLightidME.ravel(),
                                  SFOCisoME=SFOCisoME.ravel(),
                                  pscavabsisoME=pscavabsisoME.ravel(),
                                  TegEVoutisoME=TegEVoutisoME.ravel(),
                                  NTCisoME=NTCisoME.ravel(),
                                  NNME=NNME.ravel(),
                                  FPIisoME=FPIisoME.ravel(),
                                  pindME=pindME.ravel(),
                                  pmaxabsisoME=pmaxabsisoME.ravel(),
                                  pcompabsisoME=pcompabsisoME.ravel(),
                                  pmax_pcompisoME=pmax_pcompisoME.ravel(),
                                  dpairACME=dpairACME.ravel(),
                                  dpairAFME=dpairAFME.ravel(),
                                  TegTCinisoME=TegTCinisoME.ravel(),
                                  TegTCoutisoME=TegTCoutisoME.ravel(),
                                  dTcwACME=dTcwACME.ravel(),
                                  dTaircwACME=dTaircwACME.ravel(),
                                  etaTCcompME=etaTCcompME.ravel(),
                                  etaTCturbME=etaTCturbME.ravel(),
                                  SFOCisoidME=SFOCisoidME.ravel(),
                                  pscavabsisoidME=pscavabsisoidME.ravel(),
                                  TegEVoutisoidME=TegEVoutisoidME.ravel(),
                                  NTCisoidME=NTCisoidME.ravel(),
                                  NidME=NidME.ravel(),
                                  FPIisoidME=FPIisoidME.ravel(),
                                  pindidME=pindidME.ravel(),
                                  pmaxabsisoidME=pmaxabsisoidME.ravel(),
                                  pcompabsisoidME=pcompabsisoidME.ravel(),
                                  pmax_pcompisoidME=pmax_pcompisoidME.ravel(),
                                  pcomp_pscavabsisoME=pcomp_pscavabsisoME.ravel(),
                                  pcomp_pscavabsisoidME=pcomp_pscavabsisoidME.ravel(),
                                  dpairACidME=dpairACidME.ravel(),
                                  dpairAFidME=dpairAFidME.ravel(),
                                  TegTCinisoidME=TegTCinisoidME.ravel(),
                                  TegTCoutisoidME=TegTCoutisoidME.ravel(),
                                  dTcwACidME=dTcwACidME.ravel(),
                                  dTaircwACidME=dTaircwACidME.ravel(),
                                  etaTCcompidME=etaTCcompidME.ravel(),
                                  etaTCturbidME=etaTCturbidME.ravel(),
                                  TegEVoutisocylME=pd.DataFrame(TegEVoutisocylME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  FPIisocylME=pd.DataFrame(FPIisocylME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  pmaxabsisocylME=pd.DataFrame(pmaxabsisocylME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  pcompabsisocylME=pd.DataFrame(pcompabsisocylME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  NTCisoTCME_1=pd.DataFrame(NTCisoTCME_1).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  dpairACTCME_1=pd.DataFrame(dpairACTCME_1).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  dpairAFTCME_1=pd.DataFrame(dpairAFTCME_1).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  TegTCinisoTCME_1=pd.DataFrame(TegTCinisoTCME_1).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  TegTCoutisoTCME_1=pd.DataFrame(TegTCoutisoTCME_1).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  dTcwACTCME_1=pd.DataFrame(dTcwACTCME_1).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  dTaircwACTCME_1=pd.DataFrame(dTaircwACTCME_1).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  etaTCcompTCME_1=pd.DataFrame(etaTCcompTCME_1).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  etaTCturbTCME_1=pd.DataFrame(etaTCturbTCME_1).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  LogValidityME=LogValidityME.ravel(),
                                  BPMENGINEVERSION=BPMENGINEVERSION.ravel())
            else:
                OutputTable=dict(LogID=LogID.ravel(),
                                 IMO=IMO.ravel().astype(int),
                                 ME=ME.ravel().astype(int),
                                 DateTimeStart=DateTimeStart.ravel(),
                                 DateTimeEnd=DateTimeEnd.ravel(),
                                 LogDurationHRS=LogDurationHRS.ravel(),
                                  MCRME=MCRME.ravel(),
                                  PeffME=PeffME.ravel(),
                                  PeffLightidME=PeffLightidME.ravel(),
                                  SFOCisoME=SFOCisoME.ravel(),
                                  pscavabsisoME=pscavabsisoME.ravel(),
                                  TegEVoutisoME=TegEVoutisoME.ravel(),
                                  NTCisoME=NTCisoME.ravel(),
                                  NNME=NNME.ravel(),
                                  FPIisoME=FPIisoME.ravel(),
                                  pindME=pindME.ravel(),
                                  pmaxabsisoME=pmaxabsisoME.ravel(),
                                  pcompabsisoME=pcompabsisoME.ravel(),
                                  pmax_pcompisoME=pmax_pcompisoME.ravel(),
                                  dpairACME=dpairACME.ravel(),
                                  dpairAFME=dpairAFME.ravel(),
                                  TegTCinisoME=TegTCinisoME.ravel(),
                                  TegTCoutisoME=TegTCoutisoME.ravel(),
                                  dTcwACME=dTcwACME.ravel(),
                                  dTaircwACME=dTaircwACME.ravel(),
                                  etaTCcompME=etaTCcompME.ravel(),
                                  etaTCturbME=etaTCturbME.ravel(),
                                  SFOCisoidME=SFOCisoidME.ravel(),
                                  pscavabsisoidME=pscavabsisoidME.ravel(),
                                  TegEVoutisoidME=TegEVoutisoidME.ravel(),
                                  NTCisoidME=NTCisoidME.ravel(),
                                  NidME=NidME.ravel(),
                                  FPIisoidME=FPIisoidME.ravel(),
                                  pindidME=pindidME.ravel(),
                                  pmaxabsisoidME=pmaxabsisoidME.ravel(),
                                  pcompabsisoidME=pcompabsisoidME.ravel(),
                                  pmax_pcompisoidME=pmax_pcompisoidME.ravel(),
                                  pcomp_pscavabsisoME=pcomp_pscavabsisoME.ravel(),
                                  pcomp_pscavabsisoidME=pcomp_pscavabsisoidME.ravel(),
                                  dpairACidME=dpairACidME.ravel(),
                                  dpairAFidME=dpairAFidME.ravel(),
                                  TegTCinisoidME=TegTCinisoidME.ravel(),
                                  TegTCoutisoidME=TegTCoutisoidME.ravel(),
                                  dTcwACidME=dTcwACidME.ravel(),
                                  dTaircwACidME=dTaircwACidME.ravel(),
                                  etaTCcompidME=etaTCcompidME.ravel(),
                                  etaTCturbidME=etaTCturbidME.ravel(),
                                  TegEVoutisocylME=pd.DataFrame(TegEVoutisocylME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  FPIisocylME=pd.DataFrame(FPIisocylME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  pmaxabsisocylME=pd.DataFrame(pmaxabsisocylME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  pcompabsisocylME=pd.DataFrame(pcompabsisocylME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  NTCisoTCME=pd.DataFrame(NTCisoTCME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  dpairACTCME=pd.DataFrame(dpairACTCME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  dpairAFTCME=pd.DataFrame(dpairAFTCME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  TegTCinisoTCME=pd.DataFrame(TegTCinisoTCME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  TegTCoutisoTCME=pd.DataFrame(TegTCoutisoTCME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  dTcwACTCME=pd.DataFrame(dTcwACTCME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  dTaircwACTCME=pd.DataFrame(dTaircwACTCME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  etaTCcompTCME=pd.DataFrame(etaTCcompTCME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  etaTCturbTCME=pd.DataFrame(etaTCturbTCME).apply(lambda x: ','.join(x.astype(str)),axis=1),
                                  LogValidityME=LogValidityME.ravel(),
                                  BPMENGINEVERSION=BPMENGINEVERSION.ravel())
            #OutputTable=pd.DataFrame.from_dict(OutputTable)
            OutputTable=pd.DataFrame.from_dict(OutputTable,orient='index')
            OutputTable = OutputTable.transpose()
            cll=["TegEVoutisocylME","FPIisocylME","pmaxabsisocylME","pcompabsisocylME","NTCisoTCME","dpairACTCME","dpairAFTCME","TegTCinisoTCME","TegTCoutisoTCME","dTcwACTCME","dTaircwACTCME","etaTCcompTCME","etaTCturbTCME",]
            for iii in cll  :
                if iii in OutputTable.columns:
                    OutputTableD=OutputTable[iii].str.split(",",expand=True).rename(columns=lambda x:x+1).add_prefix(iii+'_').astype('float64')
                    OutputTable = pd.concat([OutputTable, OutputTableD], axis = 1)
                    OutputTable.drop([iii], axis = 1, inplace = True)
            # OutputTable.to_csv(outputfile1, index=False)
            Output=OutputTable
            # cll=['TegEVoutisocylME','FPIisocylME','pmaxabsisocylME','pcompabsisocylME','NTCisoTCME','dpairACTCME','dpairAFTCME','TegTCinisoTCME','TegTCoutisoTCME','dTcwACTCME','dTaircwACTCME','etaTCcompTCME','etaTCturbTCME']
            # for iii in cll:
            #     OutputTableD=OutputTable[iii].str.split(",",expand=True).rename(columns=lambda x:x+1).add_prefix(iii+'_')
            #     OutputTable = pd.concat([OutputTable, OutputTableD], axis = 1)
            #     OutputTable.drop([iii], axis = 1, inplace = True)
                
                
                
            # OutputTable.to_csv(outputfile1, index=False)
            #RefDashboards(RDS)
            # RefDashTable=rfd.RefDashboards(RDS)
            # RefDashTable=pd.DataFrame.from_dict(RefDashTable)
            # RefDashTable.to_csv(outputfile2, index=False)
            RefOutput=''
            DiagnosticsTable=DS
            DiagnosticsTable['LogValidityME'] = LogValidityME
            DiagnosticsTable['ME'] = ME.ravel().astype(int)
            for ky in DiagnosticsTable.keys():
                DiagnosticsTable[ky]=DiagnosticsTable[ky].ravel()
            DiagnosticsTable=pd.DataFrame.from_dict(DiagnosticsTable)
            # DiagnosticsTable.to_csv(outputfile3, index=False)
            Diagnostics=DiagnosticsTable
            ExOutput=''
        
        elif LogType=='AE':
            #Generate Output csv
            if np.all(RDS['nTCAE'])==1:
                OutputTable=dict(LogID=LogID.ravel(),
                        IMO=IMO.ravel().astype(int),
                        AE=AE.ravel().astype(int),
                        DateTimeStart=DateTimeStart.ravel(),
                        DateTimeEnd=DateTimeEnd.ravel(),
                          LogDurationHRS=LogDurationHRS.ravel(),
                          MCRAE=MCRAE.ravel(),
                          SFOCisoAE=SFOCisoAE.ravel(),
                          pscavabsisoAE=pscavabsisoAE.ravel(),
                          TegEVoutisoAE=TegEVoutisoAE.ravel(),
                          NTCisoAE=NTCisoAE.ravel(),
                          pmaxabsisoAE=pmaxabsisoAE.ravel(),
                          TegTCinisoAE = TegTCinisoAE.ravel(),
                          TegTCoutisoAE = TegTCoutisoAE.ravel(),
                          SFOCisoidAE = SFOCisoidAE.ravel(),
                          pscavabsisoidAE=pscavabsisoidAE.ravel(),   
                          TegEVoutisoidAE=TegEVoutisoidAE.ravel(),
                          NTCisoidAE=NTCisoidAE.ravel(),
                          pmaxabsisoidAE=pmaxabsisoidAE.ravel(),
                          TegTCinisoidAE=TegTCinisoidAE.ravel(),
                          TegTCoutisoidAE=TegTCoutisoidAE.ravel(),
                          TegEVoutisocylAE=pd.DataFrame(TegEVoutisocylAE).apply(lambda x: ','.join(x.astype(str)),axis=1),
                          pmaxabsisocylAE=pd.DataFrame(pmaxabsisocylAE).apply(lambda x: ','.join(x.astype(str)),axis=1),
                          NTCisoTCAE_1=pd.DataFrame(NTCisoTCAE_1).apply(lambda x: ','.join(x.astype(str)),axis=1),
                          TegTCinisoTCAE_1=pd.DataFrame(TegTCinisoTCAE_1).apply(lambda x: ','.join(x.astype(str)),axis=1),
                          TegTCoutisoTCAE_1=pd.DataFrame(TegTCoutisoTCAE_1).apply(lambda x: ','.join(x.astype(str)),axis=1),
                          LogValidityAE=LogValidityAE.ravel(),
                          BPMENGINEVERSION=BPMENGINEVERSION.ravel())
            else:
                OutputTable=dict(LogID=LogID.ravel(), 
                                 IMO=IMO.ravel().astype(int),
                                 AE=AE.ravel().astype(int),
                                 DateTimeStart=DateTimeStart.ravel(),
                                 DateTimeEnd=DateTimeEnd.ravel(),
                                 LogDurationHRS=LogDurationHRS.ravel(),
                          MCRAE=MCRAE.ravel(),
                          SFOCisoAE=SFOCisoAE.ravel(),
                          pscavabsisoAE=pscavabsisoAE.ravel(),
                          TegEVoutisoAE=TegEVoutisoAE.ravel(),
                          NTCisoAE=NTCisoAE.ravel(),
                          pmaxabsisoAE=pmaxabsisoAE.ravel(),
                          TegTCinisoAE = TegTCinisoAE.ravel(),
                          TegTCoutisoAE = TegTCoutisoAE.ravel(),
                          SFOCisoidAE = SFOCisoidAE.ravel(),
                          pscavabsisoidAE=pscavabsisoidAE.ravel(),   
                          TegEVoutisoidAE=TegEVoutisoidAE.ravel(),
                          NTCisoidAE=NTCisoidME.ravel(),
                          pmaxabsisoidAE=pmaxabsisoidAE.ravel(),
                          TegTCinisoidAE=TegTCinisoidAE.ravel(),
                          TegTCoutisoidAE=TegTCoutisoidAE.ravel(),
                          TegEVoutisocylAE=pd.DataFrame(TegEVoutisocylAE).apply(lambda x: ','.join(x.astype(str)),axis=1),
                          pmaxabsisocylAE=pd.DataFrame(pmaxabsisocylAE).apply(lambda x: ','.join(x.astype(str)),axis=1),
                          NTCisoTCAE=pd.DataFrame(NTCisoTCAE).apply(lambda x: ','.join(x.astype(str)),axis=1),
                          TegTCinisoTCAE=pd.DataFrame(TegTCinisoTCAE).apply(lambda x: ','.join(x.astype(str)),axis=1),
                          TegTCoutisoTCAE=pd.DataFrame(TegTCoutisoTCAE).apply(lambda x: ','.join(x.astype(str)),axis=1),
                          LogValidityAE=LogValidityAE.ravel(),
                          BPMENGINEVERSION=BPMENGINEVERSION.ravel())
            OutputTable=pd.DataFrame.from_dict(OutputTable)
            cll=['TegEVoutisocylAE','pmaxabsisocylAE','NTCisoTCAE','TegTCinisoTCAE','TegTCoutisoTCAE']
            for iii in cll  :
                if iii in OutputTable.columns:
                    OutputTableD=OutputTable[iii].str.split(",",expand=True).rename(columns=lambda x:x+1).add_prefix(iii+'_').astype('float64')
                    OutputTable = pd.concat([OutputTable, OutputTableD], axis = 1)
                    OutputTable.drop([iii], axis = 1, inplace = True)
            # OutputTable.to_csv(outputfile1, index=False)
            Output=OutputTable
            # cll=['TegEVoutisocylME','FPIisocylME','pmaxabsisocylME','pcompabsisocylME','NTCisoTCME','dpairACTCME','dpairAFTCME','TegTCinisoTCME','TegTCoutisoTCME','dTcwACTCME','dTaircwACTCME','etaTCcompTCME','etaTCturbTCME']
            # for iii in cll:
            #     OutputTableD=OutputTable[iii].str.split(",",expand=True).rename(columns=lambda x:x+1).add_prefix(iii+'_')
            #     OutputTable = pd.concat([OutputTable, OutputTableD], axis = 1)
            #     OutputTable.drop([iii], axis = 1, inplace = True)
                
                
                
            # OutputTable.to_csv(outputfile1, index=False)
            #RefDashboards(RDS)
            # RefDashTable=rfd.RefDashboards(RDS)
            # RefDashTable=pd.DataFrame.from_dict(RefDashTable)
            # RefDashTable.to_csv(outputfile2, index=False)
            RefOutput=''
            DiagnosticsTable=DS
            DiagnosticsTable['LogValidityAE'] = LogValidityAE
            DiagnosticsTable['AE'] = AE.ravel().astype(int)
            for ky in DiagnosticsTable.keys():
                DiagnosticsTable[ky]=DiagnosticsTable[ky].ravel()
            DiagnosticsTable=pd.DataFrame.from_dict(DiagnosticsTable)
            # DiagnosticsTable.to_csv(outputfile3, index=False)
            Diagnostics=DiagnosticsTable
            ExOutput=''
    except Exception as err:
        exp = "Error in OutputExport {} in line No. {} for IMO {}".format(err,sys.exc_info()[-1].tb_lineno,IMO)
        logger.error(exp,exc_info=True)
        ExceptionHandler(LogType,exp)
        SRQSTD['TotalTimeLapse']=time.time()-start_time              
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return('','','',SRQST)
        
    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time               
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        Diagnostics.IMO = Diagnostics.IMO.astype(int)
        return(Output,Diagnostics,ExOutput,SRQST)
        
       
        
        
   
    
        
    
    
