# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 23:23:03 2020

@author: SubhinAntony

% STAWAVE2
% Description:  This function implements the STAWAVE2 method described in
%               ISO15016:2015, Annex D.2. An empirical method has been
%               developed to approximate the transfer function of the mean
%               resistance increase in regular head waves by using the main
%               parameters such as ship dimensions and speed. An empirical
%               transfer function, Rwave, covers both the mean resistance
%               increase due to wave reflection Rawrl and the motion
%               induced resistance Rawml.
%
% Input:        STW     [kn]    ship speed through water (scalar)
%               Lbp     [m]     length between perpendiculars (scalar)
%               B       [m]     ship breadth (scalar)
%               Tmid    [m]     draught at midships (scalar)
%               Cb      [-]     block coefficient (scalar)
%               kyy     [-]     non-dimensional lateral radius of gyration (scalar)
%               rhow    [kg/m3] water density in full scale (scalar)
%               g       [m/s2]  acceleration of gravity (scalar)
%               h       [m]     water depth (scalar)
%               Hw      [m]     significant wave height (scalar)
%               Tw      [s]     wave period (scalar)
%               wdir    [deg]   wave direction (scalar)
%
% Output:       DRw     [N]     mean resistance increase due to waves (scalar)
%
% Limitations:  Lbp >  75m
%               4.0 <  Lbp/B  < 9.0
%               2.2 <  B/Tmid < 9.0
%               0.1 <  Fr     < 0.3
%               0.5 <  Cb     < 0.9
%               -45 <= wdir   <= 45
%
% See also:     STAWAVE1

"""
import math
import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.special import iv as besseli
from scipy.special import kv as besselk
from BPME_Core import WavePeriodEstimator as wpe
def WaveNumber(w,h,g):
    #Initial value
    x0=0.0001
    
    #Solving dispersion equation using Newton-Rapshon
    c=(w**2*h)/g
    con=0
    x=[]
    k=[]
    for i in range(0,np.size(w)):
        y=[]
        y.append(x0)
        x.append(y)
        y.append(x[con]-((c-(x[i][con]*np.tanh(x[i][con])))/((-x[i][con]*(1/np.cosh(x[i][con]))** 2)-np.tanh(x[i][con]))))
        del x[-1]
        x.append(y)
        while np.abs(x[i][con+1]-x[i][con])>0.00001 and con<len(x[0]):
            y.append(x[i][con+1]-((c-(x[i][con+1]*np.tanh(x[i][con+1])))/((-x[i][con+1]*(1/np.cosh(x[i][con+1]))** 2)-np.tanh(x[i][con+1]))))
            del x[-1]
            x.append(y)
            con=con+1
        k.append(x[i][con]/h)
    return(np.array(k).T)
   
def PiersonMoskowitz(w,H13,T01):
    #Calculate Afw
    Afw=173*H13**2/T01**4
    
    #Calculate Bfw
    Bfw=691/T01**4
    
    #Calculate Seta
    Seta=math.exp(np.divide(-Bfw,w**4,out=np.full_like(Bfw,float('NaN')),where=w!=0))*np.divide(Afw,w**5,out=np.full_like(Afw,float('NaN')),where=w!=0)
    return(Seta)

    
def RWAVE(w,U,Lbp,B,Tmid,Cb,kyy,rhow,g,h,zetaA):
    # Preliminary calculations
    #Calculate wavenumber in [rad/m]
    #For deep water this can be approximated by k=w^2/g
    k=lambda w: WaveNumber(w,h,g)
    
    #Calculate Froude number in [-]
    Fn=U/math.sqrt(g*Lbp)
    # Calculate transfer function for the the motion induced resistance
    #Calculate wbar in [-]
    wbar=lambda w:(math.sqrt(Lbp/g)*kyy**(1/3)/(1.17*Fn**(-0.143)))*w
    
    #Calculate a1 in [-]
    a1=60.3*Cb**1.34
    
    #Calculate b1 and d1 in [-]
    if wbar(w)<1:
      b1=11
      d1=14
    else:
      b1=-8.5
      d1=-566*(Lbp/B)**(-2.66)
   
    
    #Calculate raw in [-]
    raw=lambda w:wbar(w)**b1*math.exp(b1/d1*(1-wbar(w)**d1))*a1*Fn**(1.5)*math.exp(-3.5*Fn)
    
    #Calculate Rawml in [N]
    Rawml=lambda w:4*rhow*g*zetaA**2*B**2/Lbp*raw(w)
    # Calculate transfer function for the mean resistance increase due to wave reflection
    #Calculate f1 in [-]
    f1=0.692*(U/math.sqrt(Tmid*g))**0.769+1.81*Cb**6.95
    
    #Calculate a1w in [-]
    a1w=lambda w:(np.pi**2*(besseli(1,(1.5*k(w)*Tmid).astype('float64')))**2)/(np.pi**2*(besseli(1,(1.5*k(w)*Tmid).astype('float64')))**2+(besselk(1,(1.5*k(w)*Tmid).astype('float64')))**2)*f1 
#    a1w=lambda w:(np.pi**2*(besseli(1,1.5*(k(w)*Tmid).astype('float64'))**2))/(np.pi**2*(besseli(1,1.5*k(w)*Tmid))**2+(besselk(1,1.5*k(w)*Tmid))**2)*f1
    
    #Calculate Rawrl in [N]
    Rawrl=lambda w:(0.5*rhow*g*zetaA**2*B)*a1w(w)
    # Calculate empirical transfer function Rwave %
    Rwave=Rawml(w)+Rawrl(w)
    return(Rwave)


    
    
def STAWAVE2(STW,Lbp,B,Tmid,Cb,kyy,rhow,g,h,Hw,Tw):
    #Convert ship speed through water from kn to m/s
    U=STW*0.514444
    
    #Depth check
    if h<=0:
        DRw=np.nan
        print('Water depth error!')
        return(DRw)
    
    # Wave period check 
    if  Tw<=0 or np.isnan(Tw):
        Tw=wpe.WavePeriodEstimator(Hw);
#            #Table from Lee and Bales, 1984
#        Hwmean=np.array([0, 0.05, 0.30, 0.88, 1.88, 3.25, 5.00, 7.50, 11.50, 15.00])
#        Twmean=np.array([0, 1, 6.3,  7.5,  8.8,  9.7,  12.4, 15.0, 16.4,  20.0]) #the first 2 were NaNs but we put 0 and 1 for proper usage of spline function
#        spln=InterpolatedUnivariateSpline(Hwmean,Twmean)
#        Tw=spln(Hw)
            
        
    
    zetaA=Hw/2
    wmax=math.ceil(2*np.pi/Tw*5)
    w=np.arange(0,wmax+0.1,0.1)
    
    Seta=np.zeros(np.shape(w))
    Rwave=np.zeros(np.shape(w))
    
    for i in range(0,len(w)):
        Seta[i]=PiersonMoskowitz(w[i],Hw,Tw)
        Rwave[i]=RWAVE(w[i],U,Lbp,B,Tmid,Cb,kyy,rhow,g,h,zetaA)
    

    I=Rwave*Seta/zetaA**2
    I[np.isnan(I.astype('float64'))]=0
    DRw=2*np.trapz(I,w)
        
    return(DRw)    

# j=298

# STW = ServiceDataStruct['STW'][j][0]
# Tm = ServiceDataStruct['Tm'][j][0]
# Cb = ServiceDataStruct['Cb'][j][0]
# rhosw = ServiceDataStruct['rhosw'][j][0]
# hsw = ServiceDataStruct['hsw'].reshape(1,-1).T[j][0]
# Hwv = ServiceDataStruct['Hwv'].reshape(1,-1).T[j][0]
# Twv = ServiceDataStruct['Twv'].reshape(1,-1).T[j][0]
# psiwvr = ServiceDataStruct['psiwvr'][j][0]
# azbow = ServiceDataStruct['azbow'][j][0]
# Lbp = ReferenceDataStruct['Lbp']
# B = ReferenceDataStruct['B']
# kyy = ReferenceDataStruct['kyy']
# g = ReferenceDataStruct['g']
# DRww = STAWAVE2(STW,Lbp,B,Tm,Cb,kyy,rhosw,g,hsw,Hwv,Twv,psiwvr)
# rhow = rhosw
# Tmid = Tm
# h = hsw
# Hw = Hwv
# Tw = Twv
# wdir = psiwvr



























