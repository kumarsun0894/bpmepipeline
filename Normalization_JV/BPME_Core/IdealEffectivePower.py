# -*- coding: utf-8 -*-
import numpy as np
from BPME_Common.pchipwrap import pchipwrap as pchip
#from scipy.interpolate import pchip
from BPME_Common import IdealSFOC as isf
def IdealEffectivePower (FOCid,FOCMEST,PeffMEST,LCVFOMEST,LCVref):
    '''
    Calculate MCR as the maximum value of the shop test vector
    %This may not be 100% correct in some exceptional case. Currently there is
    %a validation in ModelCreator for this. However, for a more robust result,
    %we may add PMCRME as an additional input argument in this function.
    '''
    PMCRME=np.max(PeffMEST)
    #Calculate the LCV corrected SFOC from the ME shop test 
    SFOCLCVcorMEST=1000*FOCMEST/PeffMEST*LCVFOMEST/LCVref
    #Calculate the ME load vector as the MCR percentage from the shop test
    MCRMEST=PeffMEST/PMCRME*100
    #Dummy engine load in %
    mmm=np.arange(10,101,5).reshape(1,-1).T
    #Dummy effective power in kW
    Peffmmm=mmm*PMCRME/100
    #Dummy ideal SFOC in g/kwh
    SFOCmmm=isf.IdealSFOC(mmm,SFOCLCVcorMEST,MCRMEST)
    #Dummy ideal FOC in MT/day
    FOCmmm=24*SFOCmmm*Peffmmm/10**6
    #Ideal effective power in kW given the ideal FOC
    Peffidx=pchip(FOCmmm.ravel(),Peffmmm.ravel())
    Peffid = Peffidx(FOCid.ravel())
    return(Peffid)
    
    
    
    
    