# -*- coding: utf-8 -*-
"""
Created on Fri May  8 16:56:41 2020

@author: SubhinAntony

% AEUtilization
% Description:  This function calculates the auxiliary engines utilization
%               (AEU) based on the formula:
%
%               AEU = TotDEAE / TotDEidAE
%
%               Where:  - TotDEAE is the total energy produced (or energy
%                         demand) on board during log in kWh. This is the
%                         sum of energies produced by all operating AEs at
%                         the engine side.                         
%
%                       - TotDEidAE is the ideal total energy in kWh. This
%                         is the energy that can be produced by the
%                         operating AEs given their nominal (engine) power,
%                         their hours of operation (running hours) within
%                         each log and based on the MCRidAE which is the
%                         load at the statistical minimum of the AE SFOC
%                         curve, e.g. at 80%.
%
% Exceptions:   In order to ensure that the AEU is taking into account some
%               physical limitations, several controls are performed which
%               may change the AEU calculation to 100%. For details see the
%               flow control within the algorithm. 
%
% Input:        RHAE        [hr]    running hours during log for each AE (vector per AE)
%               Dt          [hr]    log duration (vector)
%               DEengAE     [kWh]   energy produced during log for each AE (engine side) (vector per AE)
%               PengnomAE   [kW]    nominal engine power (MCR) for each AE (vector)
%               MCRidAE     [%]     ideal load (engine side), e.g. 80% (scalar)
%               RHmarginAE  [hr]    acceptable time for parallel AE usage (scalar)
%
% Output:       AEU         [%]    auxiliary engine utilization (vector)
%
% See also:     OutputCalculation



"""
import numpy as np
from BPME_Common import nansumwrapper as nsw

def AEUtilization(RHAE,Dt,DEengAE,PengnomAE,MCRidAE,RHmarginAE):

   #Calculate total AE running hours
    TotRHAE=nsw.nansumwrapper(RHAE,axis=1)

   #Calculate total AE energy production in kWh (engine side)
    TotDEAE=nsw.nansumwrapper(DEengAE,axis=1)

   #Memeory pre-allocation
    TotDEidAE=np.empty(Dt.shape)
    TotDEidAE[:] = np.nan
    TotDEidmaxAE=np.empty(Dt.shape)
    TotDEidmaxAE[:] = np.nan
    TotDEid_1_lessAE=np.empty(Dt.shape)
    TotDEid_1_lessAE[:]= np.nan
    AEU=np.empty(Dt.shape)
    AEU[:]=np.nan
    #Calculate AEU for each log
    for i in range(0,len(Dt)):
    # 1.Main calculation #    
        #Find which AEs are in operation for each log (operational index: OI)
        AEOI=np.argwhere((~np.isnan(RHAE[i,:])) & (RHAE[i,:]>0))
        
        #Check if running hours are empty or log duration is less than 1hr
        if len(AEOI)==0 or Dt[i]<=1:
            continue
        #generate diagnostic and continue to next log

        #Define ideal energy given the operating engines and their RHs
        TotDEidAE[i]=np.sum(MCRidAE/100*PengnomAE[0][AEOI]*RHAE[i,AEOI])
    
        #Find AEU
        AEU[i]=TotDEAE[i]/TotDEidAE[i]*100
    
        #Find the maxinmun ideal energy based on the largest of the operating AEs for each log
        TotDEidmaxAE[i]=MCRidAE/100*Dt[i]*np.max(PengnomAE[0][AEOI])
    
        #Find the "one-less" ideal energy
        TotDEid_1_lessAE[i]=MCRidAE/100*Dt[i]*(np.sum(PengnomAE[0][AEOI])-min(PengnomAE[0][AEOI]))
    
        #Check if AEs where used only in series (not parallel) and AEU is less than 100#
        if (TotRHAE[i]<=Dt[i]+RHmarginAE) and (AEU[i]<=100):
            AEU[i]=100
        
        #AEs are used in parallel
        elif TotRHAE[i]>Dt[i]+RHmarginAE:
            #Check if the optimum number of AEs is utilized and AEU is less than 100#
            if (TotDEAE[i]>TotDEidmaxAE[i] ) and  (TotDEAE[i]>TotDEid_1_lessAE[i]) and (AEU[i]<=100):
                AEU[i]=100
    return AEU
        