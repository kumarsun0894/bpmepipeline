# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 15:29:16 2020

@author: Subhin Antony
@Delta: SunKumar

% STAWAVE
% Description:  This function calls either STAWAVE1 or STAWAVE2 based on
%               the following rule:
%               - If azbow < 0.05, use STAWAVE1
%               - If azbow >= 0.05, use STAWAVE2
%
% Input:        STW     [kn]    ship speed through water (vector)
%               Tm      [m]     mean draught (vector)
%               Cb      [-]     block coefficient (vector)
%               rhosw   [kg/m3] water density in full scale (vector)
%               hsw     [m]     water depth (vector)
%               Hwv     [m]     wave height (vector)
%               Twv     [s]     wave period (vector)
%               psiwvr  [deg]   relative wave direction (vector)
%               azbow   [g]     vertical acceleration at bow (vector)
%               Lbp     [m]     length between perpendiculars (scalar)
%               B       [m]     ship breadth (scalar)
%               kyy     [-]     lateral radius of gyration (scalar)
%               g       [m/s2]  gravity acceleration (scalar)
%
% Output:       DRw     [N]     mean resistance increase due to waves (vector)
%
% See also:     STAWAVE1, STAWAVE2, STATOTAL

"""
import numpy as np
from BPME_Core import STAWAVE1 as sw1
from BPME_Core import STAWAVE2 as sw2
def STAWAVE(STW,Tm,Cb,rhosw,hsw,Hwv,Twv,psiwvr,azbow,Lbp,B,kyy,g):
    #Memory preallocation
    # DRw=np.empty(Tm.shape)
    # DRw[:]=np.nan
    DRw = np.full(Tm.shape,np.nan)
    OOR = np.full(Tm.shape,np.nan) #out of method range

    ########################################################
    #Method selection rule application
    ########################################################
    #After several case studies, we observed that:
    #1.Eventhough STAWAVE2 is more accurate than STAWAVE1, it is sensitive and
        #needs accurate input for the wave period. The latter is not always
        #available with high accuracy and the estimators may not depict the reality.

   
    #2.For swell the STAWAVE2 and period estimator seem more appropriate.
    
    #3.When a weather provider feeds the methods, STAWAVE2 is preferable.
    
    #4.When manual data feed the methods, STAWAVE1 is preferable as more stable.

    #5.STAWAVE1 has an application range, but it seems stable even out of this
        #range. In view of this, until we conclude to the weather data policy
        #(wether we ll use only from a provider and never from crew), the STAWAVE1
        #will be the only method for weather correction.
    
    for j in range(0,int(len(Tm))):
        if (np.abs(psiwvr[j][0])>45 or Hwv[j]<=0 or np.isnan(Hwv[j].astype('float64'))):
            DRw[j][0]=0
  
        #elif azbow[j][0]<0.05 and Hwv[j][0]<=2.25*np.sqrt(Lbp/100)
        else:
            DRw[j][0]=sw1.STAWAVE1(Lbp,B,rhosw[j][0],g,Hwv[j][0])
        
        #elif Lbp>75 and Lbp/B>4 and Lbp/B<9 and B/Tm[j]>2.2 and B/Tm[j]<9 and STW[j]*0.514444/np.sqrt(g*Lbp)>0.1 and STW[j]*0.514444/np.sqrt(g*Lbp)<0.3 and Cb[j]>0.5 and Cb[j]<0.9:
            #Resistance/Power increase due to waves/swell using STAWAVE2 method in N/kW
            #DRw[j][0]=sw2.STAWAVE2(STW[j][0],Lbp,B,Tm[j][0],Cb[j][0],kyy,rhosw[j][0],g,hsw[j][0],Hwv[j][0],Twv[j][0])
            
        #else:
            #DRw[j][0] = float('NaN')
            #OOR[j][0] = 1
    return(DRw,OOR)