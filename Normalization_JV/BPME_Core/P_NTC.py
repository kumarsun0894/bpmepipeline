# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 20:44:51 2020

@author: Subhin Antony

% P_NTC
% Description:  This function is used to estimate the brake power using the
%               turbocharger method as formulated based on MAN plate 70627
%               and the respective application.
%
% Input:        NTC     [RPM]   TC speed (vector)
%               Tscav   [°C]    scavenge air temperature (vector)
%               pamb    [mbar]  ambient pressure (vector)
%               NTCST   [RPM]   shop test TC speed (vector)
%               TscavST [°C]    shop test scavenge air temperature (vector)
%               pambST  [mbar]  shop test ambient pressure (vector)
%               PST     [kW]    shop test effective power (vector)
%               c       [-]     TC nomogram coeffcients as extracted from plate 70627 (matrix)
%
% Output:       P       [kW]    estimated effective power (vector)
%
% See also:     PowerEstimation, P_SFOC, P_FPI, P_LI


"""
import numpy as np
import pandas as pd
#from scipy.interpolate import pchip
from BPME_Common.pchipwrap import pchipwrap as pchip
#from scipy.interpolate import CubicSpline
def P_NTC(NTC,Tscav,pamb,NTCST,TscavST,pambST,PST,c):
    #Train the Pcor-NTCcor model
    NTCcor_train=c[0,0]*NTCST*TscavST+c[0,1]*NTCST
    Pcor_train=c[1,0]*PST*pambST+c[1,1]*PST
    
    #sort
    dataset = pd.DataFrame({'NTCcor_train': NTCcor_train, 'Pcor_train': Pcor_train}, columns=['NTCcor_train', 'Pcor_train'])
    dataset=dataset.sort_values('NTCcor_train')
    
    #Estimate power on Pcor-NTCcor model based on input NTC and Tscav
    NTCcor=c[0,0]*NTC*Tscav+c[0,1]*NTC
    NTCcor=NTCcor.astype('float64')
    pc=pchip(np.array(dataset['NTCcor_train']).astype('float64'),np.array(dataset['Pcor_train']).astype('float64'))
    Pcor=pc(NTCcor)
    
    
    #Estimate power after correction for pamb
    P=Pcor/(c[1,0]*pamb+c[1,1])
    
    #Check if NTC is within the shop tested range
    # NTC=NTC.ravel()
    
    NTCcheck=NTC<(np.min(NTCST))
    if np.sum(NTCcheck)>0:
        P[NTCcheck]=np.nan
        print('P_NTC error: NTC too low for the method to be applied!')
        
    return(P)