# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 23:23:03 2020

@author: AkhilSurendran

% DENS
% Description:  This function implements part of the method described in
%               ISO15016:2015, Annex E. The formula below estimates the
%               resistance increase due to the effects of water density.
%
% Input:        STW     [kn]    ship speed through water (vector)
%               PDid    [kW]    ideal delivered power (vector)
%               etaDid  [-]     ideal propulsive efficiency (vector)
%               rhow    [kg/m3] water density (vector)
%
% Output:       DRrhow  [N]     resistance increase due to water density (vector)
%
% Standards:    Temperature of the seawater is 15°C and density is 1026kg/m3.
%
% Note1:        According to ITTC 7.5-02-01-03: In general, the water
%               properties are a function of temperature, pressure and
%               absolute salinity. In this procedure, data are provided at
%               standard pressure of 0.101325 MPa. Assuming standard
%               absolute salinities as Sfwa=0g/kg (fresh water) and
%               Sswa=35.16504g/kg (sea water), the rest of the properties
%               (e.g.density, viscosity, etc.) are only functions of
%               temperature.
%
% Note2:        Discrepancy was observed on the power correction for
%               density calculated by FPC code and STAIMO, i.e. for
%               a measured shaft power of 8483kW the power correction for
%               density was calculated as:
%
%                           FPC code    STAIMO code   Difference
%                           [kW]/[%]    [kW]/[%]      [kW] / [%]
%               ------------------------------------------------
%               dPdensity   4.9/0.06    14.9/0.18     10/0.12
%
%               This discrepancy was due to the effect of the salinity.
%               Thus, the above assumption to use the standard absolute
%               salinity of Sswa=35.16504g/kg for sea water and assume the
%               rest of the properties as functions ONLY of temperature
%               introduces an error. However, the error is relatively small
%               (0.12% for this case study) and we ll keep this assumption
%               for our early stage developments.
%
% See also:     STATOTAL, TEMP, DensityStandardSeawater

"""
import numpy as np
from BPME_Common import DensityStandardSeawater as DSSW

def DENS(STW,PDid,etaDid,rhow):
    #Convert ship speed through water from kn to m/s
    U=STW*0.514444
    
    #Reference density calculation
    rhowref=DSSW.DensityStandardSeawater(15)
    
    #Total resistance calculation in N
    # Rtref=etaDid*PDid*1e3/U
    Rtref=np.divide((etaDid*PDid*1e3),U.astype('float64') ,out = np.full_like((etaDid*PDid*1e3),np.nan),where = U!=0)
    #Calculation of resistance increase due to deviation of water temperature and density
    DRrhow=Rtref*(rhow/rhowref-1)
    
    return DRrhow
         
     