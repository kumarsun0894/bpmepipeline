# -*- coding: utf-8 -*-
'''
 This function provides statistical values for the wave height
 and wave period based on the World Meteorological Organization (WMO) sea state code.
 Here, we use data provided by "Lee, W. T., & Bales, S. L. (1984, January).
 Environmental data for design of marine vehicles. In Ship Structure Symposium, '84".
 According to StormGeo:
 ----------------------
 The main issue with the Douglas Scale is the lack of quantified values
 for the sea and swell. Over the years, there have been attempts to equate
 the descriptions used in the Douglas Scales to precise wave heights, but
 according to the World Meteorological Organization (WMO), there is no
 internationally recognized definition or official status for any of the
 Douglas Scales.

 In the absence of specific wording in the charter party stating
 otherwise, it has always been StormGeo’s policy to examine sea waves
 (wind-generated) and swell separately in keeping with the original
 Douglas tables.

 With this latest arbitration award from London, there is evidence for
 consensus that a 2 meter swell height is indeed the preferable
 understanding for Douglas Sea State 3.

 Sources:
 https://en.wikipedia.org/wiki/Douglas_sea_scale
 https://www.stormgeo.com/solutions/shipping/fleet-performance/articles/douglas-sea-state-3-london-arbitration-award/

 Note by Nicholas Kouvaras:
 --------------------------
 The vague definition for DSS refers to double code notation, i.e. a code
 for wind waves and a code for swell. There is no definition available for
 single code notation. In the lack of a clear definition for a single
 notation for the DSS as used in the charterparties, FPC decided to
 consider the significant wave height as the resultant of the wind wave
 and swell height, i.e. H13=sqrt(Hwv^2+Hsw^2). Even though the 
 DSSwv-to-Hwv is clearly defined in WMO, the DSSsw-to-Hsw is quite vague.

 To implement the use of significant wave height approach (as described
 above) and to avoid using the vague swell length observation (which can
 be derived from the also vague swell period) there is a need for a 1-by-1
 mapping of the DSSsw-to-Hsw. FPC defined the table below (see columns
 minsw' and maxsw') based on the vague definitions by WMO (shown in
 columns minsw and maxsw).

 FPC's Interpretation for the Swell Douglass Sea Scale
 -----------------------------------------------------
 DSSsw minsw maxsw minsw' maxsw'
 -     m     m     m      m
 0     0     0     0      0
 1     0	  2     0      1
 2     0	  2     1      2
 3     2	  4     2      2.5
 4     2 	  4     2.5    3
 5     2	  4     3      4
 6     4	  -     4      5
 7     4	  -     5      6
 8     4	  -     6      7
 9     -	  -     7      8

 Based on the definitions above the final table is being constituted which
 forms the basis for a single notation for the DSS using the significant
 wave height as input.
 
 Single Code Douglass Sea Scale for Significant Wave Height (by FPC)
 -------------------------------------------------------------------
 DSS min  max
 -   m    m
 0	  0    0
 1	  0    1
 2	  1	   2.1
 3	  2.1  2.8
 4	  2.8  3.9
 5	  3.9  5.7
 6	  5.7  7.8
 7	  7.8  10.8
 8	  10.8 15.7
 9	  15.7 -

 It is suggested to check the performance of this algorithm and tune it
 later if needed. '''
 
 
import numpy as np
from BPME_Core import WavePeriodEstimator as wpe

def SeaState(DSS):

    
    #Wind waves
    Hwvmin=np.array([0,0,0.1,0.5,1.25,2.5,4,6,9,14]).reshape(1,-1).T
    Hwvmax=np.array([0,0.1,0.5,1.25,2.5,4,6,9,14,np.nan]).reshape(1,-1).T
    Hwvmean=np.nanmean(np.concatenate((Hwvmin,Hwvmax),axis=1),axis= 1)
    Hwv=Hwvmean[int(DSS)]
    Twv=wpe.WavePeriodEstimator(Hwv)
    
    #Swell
    Hslmin=np.array([0,0,1,2,2.5,3,4,5,6,7]).reshape(1,-1).T
    Hslmax=np.array([0,1,2,2.5,3,4,5,6,7,8]).reshape(1,-1).T
    Hslmean=np.nanmean(np.concatenate((Hslmin,Hslmax),axis=1),axis=1)
    Hsl=Hslmean[int(DSS)]
    Tsl=wpe.WavePeriodEstimator(Hsl)

    #Significant wave height
    H13min=np.sqrt(Hwvmin**2+Hslmin**2)
    H13max=np.sqrt(Hwvmax**2+Hslmax**2)
    H13mean=np.nanmean(np.concatenate((H13min,H13max),axis=1),axis=1)
    H13=H13mean[int(DSS)]
    T13=wpe.WavePeriodEstimator(H13)     

    return [Hwv,Twv,Hsl,Tsl,H13,T13]    