# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 17:20:34 2020

@author: SunKumar
@Delta:SunKumar

% STAWIND
% Description:  This function implements the STAWIND method described in
%               ISO15016:2015, Annex C and ISO19030-2, Annexes E and G.
%
% Input:        SOG     [kn]    ship speed over ground (scalar)
%               head    [deg]   ship heading (scalar)
%               UWIT    [kn]    true wind speed at anemometer height (scalar)
%               psiwt   [deg]   true wind direction at anemometer height (scalar)
%               ta      [°C]    atmospheric air temperature (scalar)
%               pa      [mbar]  atmospheric air pressure (scalar)
%               Tm      [m]     mean draught (scalar)
%               TEU     [-]     existence of conatiners on main deck (string)
%               Td      [m]     design draught (scalar)
%               Tb      [m]     mean reference ballast draught (scalar)
%               B       [m]     ship breadth (scalar)
%               Zaid    [m]     reference anemometer height (scalar)
%               Zad     [m]     anemometer height at design draught (scalar)
%               Ad      [m2]    design transverse projected area (scalar)
%               VT      [-]     vessel type (string)
%               SS      [-]     superstructure type (string)
%
% Output:       DRa     [N]     mean resistance increase due to wind (scalar)
%
% See also:     STATOTAL, STAWAVE1, STAWAVE2

"""

import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline
#from BPME_Repository import BPMEngineSingleVessel as bps
from BPME_Common import TEUint2nom as T2N 

def LoadingCondition(Tm,Td,Tb):
	Tav=(Td+Tb)/2
	Tcrd=(Td+Tav)/2
	Tcrb=(Tb+Tav)/2
	
	#print (Tm,Td,Tb)
   
	if Tm>Tcrd:
		LC='L'
		#print (LC)
	elif (Tm<=Tcrd and Tm>=Tcrb):
		LC='A'
		#print (abc)
	elif Tm<Tcrb:
		LC='B'
		#print (abc)
	else:
		LC = float('nan')
		print("Loading Condition Error!")
# 	CXLC)
	return LC 


def AirDensity(ta,pa):
	R=287.058
	roa=100*pa/(R*(ta+273.15))
	return roa
def CxCombined(psiwrref):
	Cxt =-6.661E-07*psiwrref**3+1.735E-04*psiwrref**2-2.831E-04*psiwrref-8.521E-01
	return Cxt

def CxSTAWIND(psiwrref,VT,SS,LC,TEU):
	#print (LC,VT,SS,TEU)
	#sys.exit()
	#tanker conventional bow - ballast loading - normal superstructure - 280k DWT
	STApsi1=np.array([0, 9.8, 19.7, 29.7, 39.6, 49.7, 59.9, 69.7, 79.7, 89.7, 99.8, 109.8, 119.8, 129.7, 139.5, 149.5, 159.6, 169.5, 180])
	STACx1=np.array([-0.871, -0.768, -0.629, -0.467, -0.327, -0.212, -0.135, -0.075, -0.049, -0.005, 0.069, 0.161, 0.276, 0.355, 0.453, 0.540,	0.590,	0.638,	0.615])
	
	#tanker conventional bow - laden loading - normal superstructure - 280k DWT
	STApsi2=np.array([0,	9.9,	19.9,	29.8,	39.9,	49.2,	59.8,	69.8,	79.7,	90.0,	99.6,	109.8,	119.8,	129.8,	139.6,	149.5,	159.7,	169.6,	180])
	STACx2=np.array([-0.956,	-0.935,	-0.857,	-0.735,	-0.623,	-0.503,	-0.346,	-0.176,	-0.080,	0.038,	0.126,	0.209,	0.276,	0.391,	0.513,	0.647,	0.733,	0.772,	0.760])
	
	#tanker cylindrical bow - ballast loading - normal superstructure - 280k DWT
	STApsi3=np.array([0,	9.9,	19.7,	29.8,	39.9,	49.8,	59.8,	69.6,	79.7,	89.8,	99.8,	109.7,	119.8,	129.6,	139.7,	149.5,	159.6,	169.6,	180])
	STACx3=np.array([-0.871,	-0.767,	-0.625,	-0.456,	-0.273,	-0.004,	0.305,	0.460,	0.468,	0.279,	0.169,	0.165,	0.286,	0.361,	0.455,	0.554,	0.599,	0.643,	0.624])
	
	#LNG carrier - average loading - prismatic integrated deck - 125k-m3
	STApsi4=np.array([0,	9.8,	19.8,	29.8,	39.7,	50.0,	59.7,	70.0,	79.7,	89.9,	99.7,	109.9,	120.0,	129.6,	139.8,	149.6,	159.9,	169.6,	180])
	STACx4=np.array([-1.027,	-0.988,	-0.938,	-0.828,	-0.671,	-0.472,	-0.310,	-0.153,	-0.043,	0.037,	0.102,	0.228,	0.406,	0.562,	0.682,	0.787,	0.873,	0.910,	0.903])
	
	#LNG carrier - average loading - prismatic extended deck - 138k-m3
	STApsi5=np.array([0,	7.4,	14.8,	29.9,	45.2,	59.8,	74.6,	90.0,	104.7,	120.0,	134.7,	149.6,	164.7,	180])
	STACx5=np.array([-0.808,	-0.838,	-0.792,	-0.932,	-0.556,	-0.278,	-0.118,	-0.203,	0.004,	0.191,	0.798,	1.068,	1.034,	0.591])
	
	#LNG carrier - average loading - spherical - 125k-m3
	STApsi6=np.array([0,	7.3,	14.8,	29.9,	45.1,	59.9,	74.6,	90.0,	104.8,	120.0,	134.5,	149.6,	164.7,	180])
	STACx6=np.array([-1.121,	-0.972,	-0.789,	-0.705,	-0.556,	-0.374,	-0.125,	0.298,	0.570,	0.824,	0.604,	0.788,	0.952,	0.744])
	
	#container ship - ballast loading - with lashing bridges - 6800TEU
	STApsi7=np.array([0,	7.6,	15.1,	22.6,	29.9,	44.9,	59.9,	74.8,	90.0,	104.5,	119.5,	134.9,	149.6,	164.4,	180])
	STACx7=np.array([-0.975,	-0.965,	-1.061,	-1.042,	-0.903,	-0.802,	-0.655,	-0.459,	-0.252,	0.116,	0.494,	0.897,	1.032,	1.053,	0.876])
	
	#container ship - ballast loading - without lashing bridges - 6800TEU
	STApsi8=np.array([0,	7.4,	14.9,	22.6,	29.9,	44.8,	59.7,	74.8,	89.8,	104.7,	119.4,	134.4,	149.4,	164.5,	180])
	STACx8=np.array([-0.866,	-0.816,	-0.895,	-0.869,	-0.708,	-0.684,	-0.614,	-0.458,	-0.293,	0.025,	0.408,	0.795,	0.965,	0.989,	0.859])
	
	#container ship - laden loading - with containers - 6800TEU
	STApsi9=np.array([0,	7.5,	15.0,	22.5,	29.8,	44.9,	59.9,	74.9,	89.9,	104.6,	119.5,	134.5,	149.4,	164.5,	180])
	STACx9=np.array([-0.671,	-0.733,	-0.759,	-0.726,	-0.666,	-0.404,	-0.253,	-0.219,	-0.271,	-0.060,	0.347,	0.672,	0.882,	0.833,	0.645])
	
	#container ship - laden loading - without containers, with lashing bridges - 6800TEU
	STApsi10=np.array([0,	7.7,	15.1,	22.4,	29.8,	45.1,	59.8,	74.9,	89.8,	104.3,	119.6,	134.6,	149.4,	164.5,	180])
	STACx10=np.array([-1.018,	-1.064,	-1.122,	-1.126,	-1.124,	-0.960,	-0.743,	-0.451,	-0.127,	0.261,	0.752,	1.071,	1.163,	1.163,	0.924])
	
	#car carrier - average loading - normal superstructure - Autosky
	STApsi11=np.array([0,	7.2,	14.8,	29.9,	44.9,	59.8,	74.6,	89.7,	104.6,	119.5,	134.3,	149.5,	164.3,	180])
	STACx11=np.array([-0.532,	-0.578,	-0.498,	-0.406,	-0.210,	-0.070,	-0.043,	0.122,	-0.281,	0.171,	0.679,	0.895,	0.864,	0.756])
	
	#ferry/cruise ship - average loading - normal superstructure
	STApsi12=np.array([0,	7.4,	15.1,	29.9,	44.9,	49.7,	60.0,	75.0,	85.2,	90.9,	94.8,	100.0,	110.1,	120.1,	140.1,	154.9,	160.3,	170.1,	180])
	STACx12=np.array([-0.697,	-0.729,	-0.732,	-0.698,	-0.353,	-0.243,	-0.268,	-0.002,	0.188,	0.026,	-0.049,	-0.055,	0.090,	0.227,	0.556,	0.832,	0.797,	0.720,	0.654])
	
	#general cargo ship - average loading - normal superstructure
	STApsi13=np.array([0,	9.8,	19.9,	29.8,	39.8,	49.9,	59.9,	69.8,	79.7,	89.6,	99.7,	109.6,	119.6,	139.7,	149.5,	159.4,	169.5,	180])
	STACx13=np.array([-0.592,	-0.870,	-0.999,	-0.996,	-0.880,	-0.847,	-0.661,	-0.426,	-0.274,	-0.092,	0.086,	0.474,	0.848,	1.390,	1.470,	1.341,	0.915,	0.817])
	
	###########################
	# tanker conventional bow #
	###########################
	#if strcmp(VT,'TK')
	if VT == 'TK':
		if LC == 'B':
			spl = InterpolatedUnivariateSpline(STApsi1,STACx1)
			Cx0 = spl(0)
			spl1 = InterpolatedUnivariateSpline(STApsi1,STACx1)
			Cx = spl1(psiwrref)
		
		elif LC =='A':
			spl = InterpolatedUnivariateSpline(STApsi1,STACx1)
			spl1 = InterpolatedUnivariateSpline(STApsi2,STACx2)
			Cx0 = np.mean([spl(0),spl1(0)])
			spl2 = InterpolatedUnivariateSpline(STApsi1,STACx1)
			spl3 = InterpolatedUnivariateSpline(STApsi2,STACx2)
			Cx = np.mean([spl2(psiwrref),spl3(psiwrref)],axis=0)
		elif LC == 'L':
			spl =InterpolatedUnivariateSpline(STApsi2,STACx2)
			Cx0 = spl(0)
			spl1 = InterpolatedUnivariateSpline(STApsi2,STACx2)
			Cx = spl1(psiwrref)
		else:
			Cx0 = float('NaN')
			Cx = float('NaN')
			print("Cx Error!")
		
	 
	elif VT =='BC':
		if SS == 'N':
			if LC == 'B':
				spl = InterpolatedUnivariateSpline(STApsi1,STACx1)
				Cx0 = spl(0)
				spl1 = InterpolatedUnivariateSpline(STApsi1,STACx1)
				Cx = spl1(psiwrref)
		
			elif LC =='A':
				spl = InterpolatedUnivariateSpline(STApsi1,STACx1)
				spl1 = InterpolatedUnivariateSpline(STApsi2,STACx2)
				Cx0 = np.mean([spl(0),spl1(0)],axis=0)
				spl2 = InterpolatedUnivariateSpline(STApsi1,STACx1)
				spl3 = InterpolatedUnivariateSpline(STApsi2,STACx2)
				Cx = np.mean([spl2(psiwrref),spl3(psiwrref)],axis=0)
			elif LC == 'L':
				spl = InterpolatedUnivariateSpline(STApsi2,STACx2)
				Cx0 = spl(0)
				spl1 = InterpolatedUnivariateSpline(STApsi2,STACx2)
				Cx = spl1(psiwrref)
			else:
				Cx0=float('NaN')
				Cx = float('NaN')
				print("Cx Error")
		elif SS == 'CRA':
			spl = InterpolatedUnivariateSpline(STApsi13,STACx13)
			Cx0 = spl(0)
			spl1 = InterpolatedUnivariateSpline(STApsi13,STACx13)
			Cx = spl1(psiwrref)
		else:
			Cx0=float('NaN')
			Cx = float('NaN')
			print("Cx Error")



	elif VT == 'TKCYL':
		if LC == 'B':
			spl = InterpolatedUnivariateSpline(STApsi3,STACx3)
			Cx0 = spl(0)
			spl1 = InterpolatedUnivariateSpline(STApsi3,STACx3)
			Cx = spl1(psiwrref)
		elif LC == 'A':
			spl = InterpolatedUnivariateSpline(STApsi1,STACx1)
			spl1 = InterpolatedUnivariateSpline(STApsi2,STACx2)
			Cx0 = np.mean([spl(0),spl1(0)],axis=0)
			spl2 = InterpolatedUnivariateSpline(STApsi3,STACx3)
			spl3 = InterpolatedUnivariateSpline(STApsi2,STACx2)
			Cx = np.mean([spl2(psiwrref),spl3(psiwrref)],axis=0)
		elif LC == 'L':
			spl = InterpolatedUnivariateSpline(STApsi2,STACx2)
			Cx0 = spl(0)
			spl1 = InterpolatedUnivariateSpline(STApsi2,STACx2)
			Cx = spl1(psiwrref)
		else:
			Cx0 = float('NaN')
			Cx = float('NaN')
			print("Cx error!")
			

	elif VT == 'BCCYL':
		if LC =='B':
			spl = InterpolatedUnivariateSpline(STApsi3,STACx3)
			Cx0 = spl(0)
			spl1 = InterpolatedUnivariateSpline(STApsi3,STACx3)
			Cx = spl1(psiwrref)
		elif LC == 'A':
			spl = InterpolatedUnivariateSpline(STApsi3,STACx3)
			spl1 = InterpolatedUnivariateSpline(STApsi2,STACx2)
			Cx0 = np.mean([spl(0),spl1(0)],axis=0)
			spl2 = InterpolatedUnivariateSpline(STApsi3,STACx3)
			spl3 = InterpolatedUnivariateSpline(STApsi2,STACx2)
			Cx = np.mean([spl2(psiwrref),spl3(psiwrref)],axis=0)
		elif LC =='L':
			spl = InterpolatedUnivariateSpline(STApsi2,STACx2)
			Cx0 = spl(0)
			spl1 = InterpolatedUnivariateSpline(STApsi2,STACx2)
			Cx = spl1(psiwrref)
		else:
			Cx0 = float('NaN')
			Cx = float('NaN')
			print("Cx Error!")


	elif VT =='GC':
		if SS == 'PID':
			spl = InterpolatedUnivariateSpline(STApsi4,STACx4)
			Cx0 = spl(0)
			spl1 = InterpolatedUnivariateSpline(STApsi4,STACx4)
			Cx = spl1(psiwrref)
		elif SS == 'PED':
			spl = InterpolatedUnivariateSpline(STApsi5,STACx5)
			Cx0 = spl(0)
			spl1 = InterpolatedUnivariateSpline(STApsi5,STACx5)
			Cx = spl1(psiwrref)
		elif SS == 'SPH':
			spl = InterpolatedUnivariateSpline(STApsi6,STACx6)
			Cx0 = spl(0)
			spl1 = InterpolatedUnivariateSpline(STApsi6,STACx6)
			Cx = spl1(psiwrref)
		else:
			Cx0 = float('NaN')
			Cx = float('NaN')
			print("Cx Error!")


	elif VT == 'CC':
		if TEU == 'NO':
			if SS == 'LB':
				if LC =='B':
					spl = InterpolatedUnivariateSpline(STApsi7,STACx7)
					Cx0 = spl(0)
					spl1 = InterpolatedUnivariateSpline(STApsi7,STACx7)
					Cx = spl1(psiwrref)
				elif LC == 'A':
					spl = InterpolatedUnivariateSpline(STApsi7,STACx7)
					spl1 = InterpolatedUnivariateSpline(STApsi10,STACx10)
					Cx0 = np.mean([spl(0),spl1(0)],axis=0)
					spl2 = InterpolatedUnivariateSpline(STApsi7,STACx7)
					spl3 = InterpolatedUnivariateSpline(STApsi10,STACx10)
					Cx = np.mean([spl2(psiwrref),spl3(psiwrref)],axis=0)
				elif LC == 'L':
					spl = InterpolatedUnivariateSpline(STApsi10,STACx10)
					Cx0 = spl(0)
					spl1 = InterpolatedUnivariateSpline(STApsi10,STACx10)
					Cx = spl1(psiwrref)
				else:
					Cx0 = float('NaN')
					Cx = float('NaN')
					print("Cx Error!")

			elif SS == 'N':
				if LC == 'B':
					spl = InterpolatedUnivariateSpline(STApsi8,STACx8)
					Cx0 = spl(0)
					spl1 = InterpolatedUnivariateSpline(STApsi8,STACx8)
					Cx = spl1(psiwrref)
				elif LC == 'A':
					spl = InterpolatedUnivariateSpline(STApsi8,STACx8)
					spl1 = InterpolatedUnivariateSpline(STApsi2,STACx2)
					Cx0 = np.mean([spl(0),spl1(0)],axis=0)
					spl2 = InterpolatedUnivariateSpline(STApsi8,STACx8)
					spl3 = InterpolatedUnivariateSpline(STApsi2,STACx2)
					Cx = np.mean([spl2(psiwrref),spl3(psiwrref)],axis=0)
				elif LC == 'L':
					spl = InterpolatedUnivariateSpline(STApsi2,STACx2)
					Cx0 = spl(0)
					spl1 = InterpolatedUnivariateSpline(STApsi2,STACx2)
					Cx = spl1(psiwrref)
				else:
					Cx0 = float('NaN')
					Cx = float('NaN')
					print("Cx Error!")
									
			elif SS == 'CRA':
				if LC == 'B':
					spl = InterpolatedUnivariateSpline(STApsi8,STACx8)
					spl1 = InterpolatedUnivariateSpline(STApsi13,STACx13)
					Cx0 = np.mean([spl(0),spl1(0)],axis=0)
					spl2 = InterpolatedUnivariateSpline(STApsi8,STACx8)
					spl3 = InterpolatedUnivariateSpline(STApsi13,STACx13)
					Cx = np.mean([spl(psiwrref),spl1(psiwrref)],axis=0)
				elif LC == 'A':
					spl = InterpolatedUnivariateSpline(STApsi8,STACx8)
					spl1 = InterpolatedUnivariateSpline(STApsi2,STACx2)
					spl2 = InterpolatedUnivariateSpline(STApsi13,STACx13)
					Cx0 = np.mean([spl(0),spl1(0),spl2(0)],axis=0)
					spl3 = InterpolatedUnivariateSpline(STApsi8,STACx8)
					spl4 = InterpolatedUnivariateSpline(STApsi2,STACx2)
					spl5 = InterpolatedUnivariateSpline(STApsi13,STACx13)
					Cx = np.mean([spl3(psiwrref),spl4(psiwrref),spl5(psiwrref)],axis=0)
				elif LC == 'L':
					spl = InterpolatedUnivariateSpline(STApsi2,STACx2)
					spl1 = InterpolatedUnivariateSpline(STApsi13,STACx13)
					Cx0 = np.mean([spl(0),spl1(0)],axis=0)
					spl2 = InterpolatedUnivariateSpline(STApsi2,STACx2)
					spl3 = InterpolatedUnivariateSpline(STApsi13,STACx13)
					Cx = np.mean([spl2(psiwrref),spl3(psiwrref)],axis=0)
				else:
					Cx0 = float('NaN')
					Cx = float('NaN')
					print("Cx Error!")
			else:
				Cx0 = float('NaN')
				Cx = float('NaN')
				print("Cx Error!")



		elif TEU == 'FULL':
			spl = InterpolatedUnivariateSpline(STApsi9,STACx9)
			Cx0 = spl(0)
			spl1 = InterpolatedUnivariateSpline(STApsi9,STACx9)
			Cx = spl1(psiwrref)
		elif TEU == 'A':
			if SS == 'LB':
				if LC == 'B':
					spl = InterpolatedUnivariateSpline(STApsi7,STACx7)
					spl1 = InterpolatedUnivariateSpline(STApsi9,STACx9)
					Cx0 = np.mean([spl(0),spl1(0)],axis=0)
					spl2 = InterpolatedUnivariateSpline(STApsi7,STACx7)
					spl3 = InterpolatedUnivariateSpline(STApsi9,STACx9)
					Cx = np.mean([spl2(psiwrref),spl3(psiwrref)],axis=0)
				elif LC == 'A':
					spl = InterpolatedUnivariateSpline(STApsi7,STACx7)
					spl1 = InterpolatedUnivariateSpline(STApsi10,STACx10)
					spl2 = InterpolatedUnivariateSpline(STApsi9,STACx9)
					Cx0 = np.mean([spl(0),spl1(0),spl2(0)],axis=0)
					spl3 = InterpolatedUnivariateSpline(STApsi7,STACx7)
					spl4 = InterpolatedUnivariateSpline(STApsi10,STACx10)
					spl5 = InterpolatedUnivariateSpline(STApsi9,STACx9)
					Cx = np.mean([spl3(psiwrref),spl4(psiwrref),spl5(psiwrref)],axis=0)
				elif LC == 'L':
					spl = InterpolatedUnivariateSpline(STApsi10,STACx10)
					spl1 = InterpolatedUnivariateSpline(STApsi9,STACx9)
					Cx0 = np.mean([spl(0),spl1(0)],axis=0)
					spl2 = InterpolatedUnivariateSpline(STApsi10,STACx10)
					spl3 = InterpolatedUnivariateSpline(STApsi9,STACx9)
					Cx = np.mean([spl2(psiwrref),spl3(psiwrref)],axis=0)
# 					print("Cx0",Cx0)
# 					print("Cx",Cx)
				else:
					Cx0 = float('NaN')
					Cx = float('NaN')
					print("Cx Error!")


			elif SS == 'N':
				spl = InterpolatedUnivariateSpline(STApsi8,STACx8)
				spl1 = InterpolatedUnivariateSpline(STApsi9,STACx9)
				Cx0=  np.mean([spl(0),spl1(0)],axis=0)
				spl2 = InterpolatedUnivariateSpline(STApsi8,STACx8)
				spl3 = InterpolatedUnivariateSpline(STApsi9,STACx9)
				Cx =  np.mean([spl2(psiwrref),spl3(psiwrref)],axis=0)
			elif SS == 'CRA':
				spl = InterpolatedUnivariateSpline(STApsi13,STACx13)
				Cx0 = spl(0)
				spl1 = InterpolatedUnivariateSpline(STApsi13,STACx13)
				Cx = spl1(psiwrref)
			else:
				Cx0 = float('NaN')
				Cx = float('NaN')
				print("Cx Error!")
		else:
			Cx0 = float('NaN')
			Cx = float('NaN')
			print("Cx Error!")


	elif VT == 'CAR':
		spl = InterpolatedUnivariateSpline(STApsi11,STACx11)
		Cx0 = spl(0)
		spl1 = InterpolatedUnivariateSpline(STApsi11,STACx11)
		Cx = spl1(psiwrref)
	
	elif VT == 'FB':
		spl = InterpolatedUnivariateSpline(STApsi12,STACx12)
		Cx0 = spl(0)
		spl1 = InterpolatedUnivariateSpline(STApsi12,STACx12)
		Cx = spl1(psiwrref)
	
	elif VT == 'GEN':
		spl = InterpolatedUnivariateSpline(STApsi13,STACx13)
		Cx0 = spl(0)
		spl1 = InterpolatedUnivariateSpline(STApsi13,STACx13)
		Cx = spl1(psiwrref)
	else:
		Cx0=CxCombined(0)
		Cx=CxCombined(psiwrref)

		
		
	return Cx,Cx0

def STAWIND(SOG,head,UWIT,psiwt,ta,pa,Tm,TEU,Td,Tb,B,Zaid,Zad,Ad,VT,SS):
	Ug=SOG*0.514444
	Uwt=UWIT*0.514444
	DRa = np.full(SOG.shape,np.nan)
	#Geometric calculations
	dT=Td-Tm
	A=Ad+dT*B

	#Anemometer heights correction to current loading conditions
	#As per ISO 19030-2:2016 we assume negligible effect of trim
	Za=Zad+dT
	Zaref=(Ad*(Zaid+dT)+0.5*B*dT**2)/A

	#Calculation of true wind speed at reference anemometer height in m/s
	Uwtref=Uwt*(Zaref/Za)**(1/7)
	#c1=np.array(Uwt <= 14)
	c1 = np.array(np.round(Uwtref.astype('float64')/0.514444)<=27)
	c2 = np.array(np.isnan(Uwt.astype('float64'))== False)#AC
	c3 = np.array(np.isnan(Ug.astype('float64'))== False)#AC
	isValid =np.array(c1 & c2 & c3).ravel()
# 	if isValid.ndim == 1:
# 		isValid = isValid.reshape(-1,1)
# 	print("isValid",isValid.shape)
	
	isValid = np.asarray(np.nonzero(isValid)).ravel()
# 	print("isValid",isValid)
	psiwrref=np.full(SOG.shape,np.nan);   
	LC = np.full(SOG.shape,np.nan).astype(np.str)  
	Cx=np.full(SOG.shape,np.nan)#AC
	Cx0=np.full(SOG.shape,np.nan)#AC
	Caa0=np.full(SOG.shape,np.nan)#AC
	Caa=np.full(SOG.shape,np.nan)#AC
	rhoa=np.full(SOG.shape,np.nan)#AC
#	dT=np.full(SOG.shape,np.nan)#AC
#	A=np.full(SOG.shape,np.nan)#AC
#	Za=np.full(SOG.shape,np.nan)#AC
#	Zaref=np.full(SOG.shape,np.nan)#AC
	Uwrref=np.full(SOG.shape,np.nan)#AC
#	Uwtref=np.full(SOG.shape,np.nan)#AC
	for i in isValid:
		
# 		print(i)
		
		rhoa[i]=AirDensity(ta[i],pa[i])

		
#		dT[i]=Td-Tm[i]

		
#		A[i]=Ad+dT[i]*B

		
#		Za[i]=Zad+dT[i]
		
#		Zaref[i]=(Ad*(Zaid+dT[i])+0.5*B*(dT[i]**2))/A[i]

		
#		Uwtref[i]=Uwt[i]*(Zaref[i]/Za[i])**(1/7)
# 		print("ui",Uwtref[i])

		
		Uwrref[i]=np.sqrt(Uwtref[i]**2+Ug[i]**2+2*Uwtref[i]*Ug[i]*np.cos((np.pi/180)*(psiwt[i]-head[i])))
# 		print("xxx",Uwrref[i])

	
	 
	#Calculation of relative wind direction at reference height
		
		
			
				
# 			print("i",i)
# 		print("Ug",Ug[i])
# 		print("Uwtref",Uwtref[i])
# 		print("cos",np.cos(psiwt[i]-head[i]))
# 		print("TT",Ug[i]+Uwtref[i]*np.cos(psiwt[i]-head[i]))
		if (Ug[i]+Uwtref[i]*np.cos((np.pi/180)*(psiwt[i]-head[i])))>=0:
			psiwrref[i]=(180/np.pi)*np.arctan((Uwtref[i]*np.sin((np.pi/180)*(psiwt[i]-head[i])))/(Ug[i]+Uwtref[i]*np.cos((np.pi/180)*(psiwt[i]-head[i]))))
		elif (Ug[i]+Uwtref[i]*np.cos((np.pi/180)*(psiwt[i]-head[i])))<0:
			psiwrref[i]=(180/np.pi)*np.arctan((Uwtref[i]*np.sin((np.pi/180)*(psiwt[i]-head[i])))/(Ug[i]+Uwtref[i]*np.cos((np.pi/180)*(psiwt[i]-head[i]))))+180 #AC
		

		if psiwrref[i]<-180:
			psiwrref[i]=psiwrref[i]+360
		elif psiwrref[i]>180:
			psiwrref[i]=psiwrref[i]-360
	
		
# 		print("Tm[i]",Tm[i])
# 		print("Td",Td)
# 		print("Tb",Tb)
		
		LC[i]=LoadingCondition(Tm[i],Td,Tb)

		
		[Cx[i],Cx0[i]]=CxSTAWIND(np.abs(psiwrref[i]),VT,SS,LC[i],TEU[i])

		Caa0[i]=-Cx0[i]
		Caa[i]=-Cx[i]

		DRa[i]=0.5*rhoa[i]*Caa[i]*A[i]*Uwrref[i]**2-0.5*rhoa[i]*Caa0[i]*A[i]*Ug[i]**2
	
	return (DRa,Uwtref)


# if __name__ =='__main__':
# 	
# 	ServiceDataStruct['teu'] = T2N.TEUint2nom(ServiceDataStruct['TEUonDeck'])
# 	SOG  = ServiceDataStruct['SOG']
# 	head = ServiceDataStruct['HEAD']
# 	UWIT = ServiceDataStruct['Uwit']
# 	psiwt = ServiceDataStruct['psiwit']
# 	ta = ServiceDataStruct['Tair']
# 	pa = ServiceDataStruct['pair']
# 	Tm = ServiceDataStruct['Tm']
# 	TEU= ServiceDataStruct['teu']
# 	Td = ReferenceDataStruct['Td']
# 	Tb =ReferenceDataStruct['Tb']
# 	B  =ReferenceDataStruct['B']
# 	Zaid = ReferenceDataStruct['Zaid']
# 	Zad =ReferenceDataStruct['Zad']
# 	Ad=ReferenceDataStruct['Ad']
# 	VT =  ReferenceDataStruct['TypeSTAWINDVessel']
# 	SS = ReferenceDataStruct['SuperStructureSTAWINDVessel']
# 	ServiceDataStruct['DRa'] = STAWIND(SOG,head,UWIT,psiwt,ta,pa,Tm,TEU,Td,Tb,B,Zaid,Zad,Ad,VT,SS)
	
# ServiceDataStruct_df = pd.DataFrame.from_dict(ServiceDataStruct, orient='index')
# ServiceDataStruct_dfT = np.transpose(ServiceDataStruct_df)

#ServiceDataStruct_dfT.to_csv("STAWIND.csv")



 
