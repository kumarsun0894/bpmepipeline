# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 17:32:18 2020

@author: SubhinAntony

% LOAD
% Description:  This function calculates the power correction due to load
%               variation according to ISO15016, Section 12.2.3.
%
% Input:        STW     [kn]    ship's speed through the water (vector)
%               PD      [kW]    measured delivered power (vector)
%               DR      [N]     resistance increase due to envir. conditions (vector)
%               etaDid  [-]     ideal propulsive efficiency (vector)
%               ksiP    [-]     propulsive efficiency overload factor(scalar)
%
% Output:       DPksi   [kW]    power correction due to load variation (scalar)
%
% See also:     STATOTAL, PROPRPM

"""
import numpy as np

def LOAD(STW,PD,DR,etaDid,ksiP):
    if ksiP==0:
         DPksi=np.zeros(np.shape(STW))
    else:
          U=STW*0.514444     #ship speed through water in [m/s]
          PDwatt=PD*1e3
          PDksi=np.empty(np.shape(PD))
          PDksi[:]
          DPksi=np.empty(np.shap(PD))
          DPksi[:] = np.nan
          for i in range(0,len(STW)):
              if PDwatt[i]-DR[i]*U[i]/etaDid[i]>=0:
                  #Calculate the delivered power under ideal conditions in [W]
                  PDksi[i]=0.5*(PDwatt[i]-DR[i]*U[i]/etaDid[i]+np.sqrt((PDwatt[i]-DR[i]*U[i]/etaDid[i])**2+4*PDwatt[i]*DR[i]*U[i]/etaDid[i]*ksiP))
                  
                  #Calculation of power increase in [kW]
                  DPksi[i]=-PDwatt[i]*ksiP*DR[i]*U[i]/etaDid[i]/PDksi[i]/1000
              else:
                  print('Load variation error!')
                  DPksi[i]=0
    
    return DPksi
                  
                 
                  
                  