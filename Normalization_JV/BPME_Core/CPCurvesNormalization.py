# -*- coding: utf-8 -*-
import numpy as np
import time
from BPME_Core import CPcriteria as cpc
from BPME_Core import IdealEffectivePower as iep
from BPME_Core import STATOTAL as stat
from BPME_Core import SeaState as ss
from BPME_Common import SeawaterDensity as swd
from BPME_Common import IdealST as ist
from BPME_Common import WaterLineLength as wll
from BPME_Common import WaterPlaneArea as wpa
from BPME_Common import BowSectionArea as bsa
from BPME_Common import MidshipSectionArea as msa
from BPME_Common import WettedSurfaceArea as wsa
from BPME_Common import SeawaterViscosity as swv
from BPME_Common import IdealRPMHoltrop as irh
from BPME_Common import IdealRPM as irp
from BPME_Common import IdealFOC as ifoc
from BPME_Transform import IdealPropulsiveEfficiencyHoltrop as iph
from BPME_Transform import IdealPropulsiveEfficiency as ipe
from BPME_Transform import Beaufort2Knots as b2k

def CPCurvesNormalization(RDS):

    #Passing CPcriteria
    [cr,_]=cpc.CPcriteria(RDS['TmCP'],RDS['UCP'],RDS['FOCMECP'],RDS['FwiCP'],RDS['SSNCP'],RDS['LCVFOMECP'])
    if cr==0:
        u=np.nan
        fcpnorm=np.nan
        t=np.nan
        return [u,fcpnorm,t]
    
    #Define CP speed, draught and FOC vectors
    # if RDS['UCP'].ndim==2:
    #     RDS['UCP']=RDS['UCP'][:,~np.any(np.isnan(RDS['UCP']), axis=0)]
    # if RDS['TmCP'].ndim==2:
    #     RDS['TmCP']=RDS['TmCP'][:,~np.any(np.isnan(RDS['TmCP']), axis=0)]
    # if RDS['FOCMECP'].ndim==2:
    #     RDS['FOCMECP']=RDS['FOCMECP'][:,~np.any(np.isnan(RDS['FOCMECP']), axis=0)]    
    u=RDS['UCP'].T.reshape((np.size(RDS['UCP']),1),order='F') #matrix to vector
    t=np.tile(RDS['TmCP'],(len(RDS['UCP'][0]),1)).reshape(np.shape(u),order='F') #matrix to vector
    fest=RDS['FOCMECP'].T.reshape((np.size(RDS['FOCMECP']),1),order='F')#matrix to vector
    #rm =np.flatnonzero(np.isnan(u))
    #t=np.delete(t,rm,axis=0) #emove nans
    #u= np.delete(u,rm,axis=0) #remove nans
#    t=t.reshape(1,-1).T
#    u=u.reshape(1,-1).T
    #rm2= np.flatnonzero(np.isnan(fest))
    #fest=np.delete(fest,rm2,axis=0)#remove nan
    
    #Default data (can be defined possibly in PAL PM Constants)
    ###########################################################
    TEUcp="FULL"     #containers on main deck as per charter party description (NO, A, FULL)
    Tswcp=15        #sea water temperature [°C]
    Saswcp=35.16504 #sea water salinity [g/kg]
    hswcp=1000      #sea water depth [m]
    Taircp=15       #atmospheric air temperature [°C]
    paircp=1013     #atmospheric air pressure [mbar]
    rhoswcp=swd.SeawaterDensity(Tswcp,Saswcp)
    
    #Estimate fuel mode for DF engines  
    ############################################################
    
    #This may need to be added as input forin PAL PM CP editor or the BPME to calculate and export both modes by default.
    
    #Conventional engines
    if np.all( 'No' in RDS['DualFuelME']):
        FuelMode='F'              
    #DF engines
    elif np.any('Yes' in RDS['DualFuelME']):
        FuelMode='G'
     
    
    
    #Calculate displacement in [m3]
    Vcp=ist.IdealST(RDS['Th'],RDS['Vh'],RDS['TmCP'],0)
    DISPV=np.tile(Vcp,(len(RDS['UCP'][0]),1)).reshape(u.shape,order='F')
    
    #Remove NaNs from vectors 
    rm =np.flatnonzero(np.isnan(u))
    t=np.delete(t,rm,axis=0) #emove nans
    u= np.delete(u,rm,axis=0) #remove nans
    DISPV = np.delete(DISPV,rm,axis=0) #removing corresponding element from DISPV where UCP == NaN.
    t=t.reshape(1,-1).T
    u=u.reshape(1,-1).T
    DISPV=DISPV.reshape(1,-1).T
    
    rm2= np.flatnonzero(np.isnan(fest))
    fest=np.delete(fest,rm2,axis=0)
    
    
    #Trasnsform weather conditions from DSS and Bft to BPME terms 
    [Hwvcp,Twvcp,Hslcp,Tslcp,H13cp,T13cp]=ss.SeaState(RDS['SSNCP'])
    Uwitcp=b2k.Beaufort2Knots(RDS['FwiCP']*np.ones(np.shape(u)),t,RDS['Td'],RDS['B'],RDS['Zaid'],RDS['Zad'],RDS['Ad'])
    
    #Calculate hydrostatics data needed for the WSA estimation 
    [Lwl,_,_]=wll.WaterLineLength(t,t,RDS['Lbp'],RDS['HullProfile'].T)
    
    if Lwl.ndim==1:
        Lwl=Lwl.reshape(1,-1).T
    
    [_,Cwp]=wpa.WaterPlaneArea(t,Lwl,RDS['B'],RDS['Th'],RDS['Cwph'])
    [Ab,_,_,_]=bsa.BowSectionArea(t,RDS['BowSection'].T,RDS['BulbousBowVessel'])
    
    if Ab.ndim==1:
        Ab=Ab.reshape(1,-1).T
    
    [Amid,Cm]=msa.MidshipSectionArea(t,RDS['B'],RDS['Th'],RDS['Cmh'])
    
    #Calculate wetted surface area in [m2]   
    Scp=wsa.WettedSurfaceArea(t,Lwl,DISPV,Cm,Cwp,Ab,RDS['Th'],RDS['WSAh'],RDS['B'])
    
    if Scp.ndim==1:
        Scp = Scp.reshape(1,-1).T
    #Create the input structure for the STATOTAL function - Part 1
    CPDS = {}
    CPDS['STW']=u
    CPDS['SOG']=u
    CPDS['Tm']=t
    CPDS['Amid']=Amid
    CPDS['hsw']=hswcp*np.ones(np.shape(u))
    CPDS['rhosw']=rhoswcp*np.ones(np.shape(u))
    CPDS['DISPV']=DISPV
    CPDS['WSA']=Scp
    CPDS['vsw']=swv.SeawaterViscosity(Tswcp,Saswcp)*np.ones(np.shape(u))
    CPDS['etaDHid']=iph.IdealPropulsiveEfficiencyHoltrop(u,RDS)
    CPDS['etaDid']=ipe.IdealPropulsiveEfficiency(u,t,CPDS['etaDHid'],RDS['Tref'],RDS['etaDref'],RDS['Uref'],RDS['Tb'],RDS['Ts'])
    
    #Calculate delivered power from CP FOC 
    ##################################################################

    #Additional clarification and developments needed to treat DF engines properly
    
    #Conventional engines
    if np.all('No' in RDS['DualFuelME'] ):
        #For multiple engines we assume identical shop test data
        CPDS['PDid']=iep.IdealEffectivePower(fest,RDS['FOCMEST'][0,:]*RDS['nME'],RDS['PeffMEST'][0,:]*RDS['nME'],RDS['LCVFOMEST'][0],RDS['LCVFOMECP'])*RDS['etaT']*RDS['etaeffME'][0]
        if CPDS['PDid'].ndim==1:
            CPDS['PDid']=CPDS['PDid'].reshape(1,-1).T
    #DF Engine
    elif np.any('Yes' in RDS['DualFuelME']):
        
        #FUEL MODE
        PDidFM=iep.IdealEffectivePower(fest,RDS['FOCMEST'][RDS['RepMEFO'],:]*RDS['nME'],RDS['PeffMEST'][RDS['RepMEFO'],:]*RDS['nME'],RDS['LCVFOMEST'][RDS['RepMEFO']],RDS['LCVFOMECP'])*RDS['etaT']*RDS['etaeffME'][int(RDS['NoME'][RDS['RepMEFO']])]
        if PDidFM.ndim==1:
            PDidFM=PDidFM.reshape(1,-1).T
        #Gas mode
        ######################################
        #Pilot
        #PDidPGM=IdealEffectivePower(fest,RDS.FOCMEST(RDS.RepMEGO,:)*RDS.nME,RDS.PeffMEST(RDS.RepMEGO,:)*RDS.nME,RDS.LCVFOMEST(RDS.RepMEGO),RDS.LCVFOMECP)*RDS.etaT*RDS.etaeffME(RDS.NoME(RDS.RepMEGO))                                      
        
        #GAS
        PDidGGM=iep.IdealEffectivePower(fest,RDS['GOCMEST'][RDS['RepMEGO'],:]*RDS['nME'],RDS['PeffMEST'][RDS['RepMEGO'],:]*RDS['nME'],RDS['LCVGOMEST'][RDS['RepMEGO']],RDS['LCVFOMECP'])*RDS['etaT']*RDS['etaeffME'][int(RDS['NoME'][RDS['RepMEGO']])]
        #PDidGM=PDidPGM+PDidGGM
        if PDidGGM.ndim==1:
            PDidGGM= PDidGGM.reshape(1,-1).T
         
        if FuelMode in 'G':
            CPDS['PDid']=PDidGGM
        elif FuelMode in 'F':
            CPDS['PDid']=PDidFM
        else:
            raise Exception('Please define fuel mode and run again!')
     
    #Create the input structure for the STATOTAL function - Part 2  
    IMO=int(RDS['IMONoVessel'])
    CPDS['PD']=CPDS['PDid']
    CPDS['DISPVref']=CPDS['DISPV']
    CPDS['NPropH']=irh.IdealRPMHoltrop(CPDS['PD'],RDS['NMEST'][0,:],RDS['PeffMEST'][0,:]*RDS['nME'],RDS['etaT'],RDS['etaeffME'][0],RDS['factorN2'],RDS)
    CPDS['NProp']=irp.IdealRPM(u,t,CPDS['NPropH'],RDS['Tref'],RDS['Nref'],RDS['Uref'],RDS['Tb'],RDS['Ts'])              
    CPDS['Uwit']=Uwitcp
    CPDS['Tair']=Taircp*np.ones(np.shape(u))
    CPDS['pair']=paircp*np.ones(np.shape(u))
    CPDS['teu']=np.tile(TEUcp,np.shape(u))    
    CPDS['Cb']=CPDS['DISPV']/(RDS['Lbp']*RDS['B']*CPDS['Tm'])
    CPDS['Hwv']=Hwvcp*np.ones(np.shape(u))
    CPDS['Twv']=Twvcp*np.ones(np.shape(u))
    CPDS['Hsl']=Hslcp*np.ones(np.shape(u))
    CPDS['Tsl']=Tslcp*np.ones(np.shape(u))

    CPDS['HEAD']=np.zeros(np.shape(u))
    CPDS['psiwit']=np.zeros(np.shape(u))
    CPDS['psiwvr']=np.zeros(np.shape(u))
    CPDS['psislr']=np.zeros(np.shape(u))
    CPDS['azbow']=np.zeros(np.shape(u))

    #Call STATOTAL to get power correction
    LogType="ref"
    [CPDS,_,_]=stat.STATOTAL(IMO,LogType,CPDS,RDS,{},time.time(),[])

    #Calculate normalized power in kW 
    #The added power from Bft and DSS are being substracted here to get a normalized power curve to ideal weather conditions
    pnorm=CPDS['PD']-CPDS['DPD']
    
    #Calculate normalized FOC in MT/day 
    ##################################################
    #Additional clarification and developments needed to treat DF engines properly
    
    #Conventional engines
    if np.all(RDS['DualFuelME']=='No'):
        #For multiple engines we assume identical shop test data
        fcpnorm=ifoc.IdealFOC(pnorm,RDS['FOCMEST'][0,:]*RDS['nME'],RDS['PeffMEST'][0,:]*RDS['nME'],RDS['LCVFOMEST'][0],RDS['LCVFOMECP'],RDS['etaT'],RDS['etaeffME'][0])
    elif np.any(RDS['DualFuelME']=='Yes'):
        #Fuel Mode
        fFMcp=ifoc.IdealFOC(pnorm,RDS['FOCMEST'][RDS['RepMEFO'],:]*RDS['nME'],RDS['PeffMEST'][RDS['RepMEFO'],:]*RDS['nME'],RDS['LCVFOMEST'][RDS['RepMEFO']],RDS['LCVFOMECP'],RDS['etaT'],RDS['etaeffME'][int(RDS['NoME'][RDS['RepMEFO']])])
        
        #Gas Mode
        ##############################################
        
        #pilot
        POCGMcp=ifoc.IdealFOC(pnorm,RDS['FOCMEST'][RDS['RepMEGO'],:]*RDS['nME'],RDS['PeffMEST'][RDS['RepMEGO'],:]*RDS['nME'],RDS['LCVFOMEST'][RDS['RepMEGO']],RDS['LCVFOMECP'],RDS['etaT'],RDS['etaeffME'][int(RDS['NoME'][RDS['RepMEGO']])])
        
        #Gas
        GOCGMcp=ifoc.IdealFOC(pnorm,RDS['GOCMEST'][RDS['RepMEGO'],:]*RDS['nME'],RDS['PeffMEST'][RDS['RepMEGO'],:]*RDS['nME'],RDS['LCVGOMEST'][RDS['RepMEGO']],RDS['LCVFOMECP'],RDS['etaT'],RDS['etaeffME'][int(RDS['NoME'][RDS['RepMEGO']])])

        fGMcp=POCGMcp+GOCGMcp
        
        if FuelMode in 'G':
            fcpnorm=fGMcp
        elif FuelMode in 'F':
            fcpnorm=fFMcp
        else:
            raise Exception('Please define fuel mode and run again!')
    
    return u.ravel(),fcpnorm.ravel(),t.ravel()
        
    
        

                           
    
    

    
    

