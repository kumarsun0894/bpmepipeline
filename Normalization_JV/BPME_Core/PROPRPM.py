# -*- coding: utf-8 -*-

"""
Created on Thu Apr 30 11:32:45 2020

@author: SubhinAntony

% PROPRPM
% Description:  This function calculates the propeller RPM correction
%               according to ISO15016, Annex J.
%
% Input:        N       [RPM]   measured propeller speed (vector)
%               STW     [kn]    ship's speed through water (vector)
%               PD      [kW]    measured delivered power (vector)
%               DR      [N]     resistance increase due to envir. conditions (vector)
%               DU      [kn]    decrease of speed due to shallow water (vector)
%               etaDid  [-]     ideal propulsive efficiency (vector)
%               ksiP    [-]     propulsive efficiency overload factor (scalar)
%               ksiN    [-]     RPM overload factor (scalar)
%               ksiU    [-]     speed overload factor (scalar)
%
% Output:       DN      [RPM]   propeller speed correction (vector)
%
% See also:     STATOTAL


"""
import numpy as np

def PROPRPM(N,STW,PD,DR,DU,etaDid,ksiP,ksiN,ksiU):
    U=STW*0.514444     #ship speed through water in [m/s]
    DUms=DU*0.514444   #speed correction in [m/s]
    PDwatt=PD*1e3      #measured shaft power in [W]
    Nsec=N/60          #propeller speed in [s-1]

   
    #Calculate the delivered power under ideal conditions in [W]
    PDksi=0.5*(PDwatt-DR*U/etaDid+np.sqrt(((PDwatt-DR*U/etaDid)**2+4*PDwatt*DR*U/etaDid*ksiP).astype('float64')))
    #0.5*(PDwatt-DR.*U./etaDid+sqrt((PDwatt-DR.*U./etaDid).^2+4*PDwatt.*DR.*U./etaDid*ksiP))
    # PDksi[PDksi==0] = np.nan
#    D1 = (ksiN*(PDwatt-PDksi)/PDksi).astype('float64')
    D1 = np.divide((ksiN*(PDwatt-PDksi)), PDksi.astype('float64'), out=np.full_like((ksiN*(PDwatt-PDksi)),0), where=PDksi!=0)
    # D1[D1==0] = np.nan
    # D1 = D1/PDksi
    # U[U==0] = np.nan
#    D2 = ((ksiU*DUms)/U).astype('float64')
    D2 = np.divide((ksiU*DUms),U.astype('float64'),out=np.full_like((ksiU*DUms),0),where=U!=0)
    # D2[D2==0] =np.nan
    # D2 = D2/U
    # D3 = U
    # D3[D3==0] = np.nan
    #Calculate propeller speed correction in [RPM]
    DN=Nsec*(1-(1/(D1+D2+1)))*60
    
        
    return DN   