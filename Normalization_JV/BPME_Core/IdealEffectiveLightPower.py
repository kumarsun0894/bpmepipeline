# -*- coding: utf-8 -*-
import numpy as np
from BPME_Common import IdealST as Ist
from BPME_Common import WaterLineLength as Wll
from BPME_Common import WaterPlaneArea as Wpa
from BPME_Common import BowSectionArea as Bsa
from BPME_Common import TransomArea as Tra
from BPME_Common import RudderSurfaceArea as Rsa
from BPME_Common import CenterOfBuoyancy as Cob
from BPME_Common import MidshipSectionArea as Msa
from BPME_Common import WettedSurfaceArea as Wsa
from BPME_Common import SeawaterViscosity as Swv
from BPME_Common import SeawaterDensity as Swd
from BPME_Common import IdealDeliveredPowerHoltrop as Idph
from BPME_Common import IdealDeliveredPower as Idp
from BPME_Common import IdealRPMHoltrop as irh
from BPME_Common import IdealRPM as Ir


def IdealEffectiveLightPower(NME,RDS):
    #Define speed vector
    U=np.arange(RDS['Uminref'],RDS['Umaxref']+1).reshape(1,-1).T

#    Define Holtrop reference power for the reference curves
    Ta=RDS['Tb']*np.ones(np.shape(U))
    Tf=RDS['Tb']*np.ones(np.shape(U))
    Ta=Ta.reshape(1,-1).T
    Tf=Tf.reshape(1,-1).T
    Tm=(Ta+Tf)/2
    DISPV=Ist.IdealST(RDS['Th'],RDS['Vh'],Tm,0)
    DISPV=DISPV.reshape(1,-1).T
    [Lwl,xLwl0_5,temp]=Wll.WaterLineLength(Ta,Tf,RDS['Lbp'],RDS['HullProfile'].T) 
    
    Lwl=Lwl.reshape(1,-1).T
    xLwl0_5 = xLwl0_5.reshape(1,-1).T
    
    [_,Cwp]=Wpa.WaterPlaneArea(Tm,Lwl,RDS['B'],RDS['Th'],RDS['Cwph'])
    
    Cwp = Cwp.reshape(1,-1).T
    
    [Ab,hb,temp1,temp2]=Bsa.BowSectionArea(Tf,RDS['BowSection'].T,RDS['BulbousBowVessel'])
    
    Ab=Ab.reshape(1,-1).T
    hb=hb.reshape(1,-1).T
    
    Atr,_,_=Tra.TransomArea(Ta,Tf,RDS['Lbp'],RDS['B'],RDS['HullProfile'].T,RDS['TransomSection'].T,RDS['TransomSternVessel'])
   
    Atr=Atr.reshape(1,-1).T
    
    Ar,_,_,_=Rsa.RudderSurfaceArea(Ta,RDS['Lbp'],RDS['RudderProfile'].T)
    
    
    
    [_,lcb]=Cob.CenterOfBuoyancy(Tm,Lwl,xLwl0_5,RDS['Th'],RDS['LCBh'])
    Cb=DISPV/(Lwl*RDS['B']*Tm) #block coefficient in [-]
    [_,Cm]=Msa.MidshipSectionArea(Tm,RDS['B'],RDS['Th'],RDS['Cmh'])#midship section area under water in [m2]
    
    Cm=Cm.reshape(1,-1).T
    
    WSA=Wsa.WettedSurfaceArea(Tm,Lwl,DISPV,Cm,Cwp,Ab,RDS['Th'],RDS['WSAh'],RDS['B']) #wetted surface area in [m2]
    vsw=Swv.SeawaterViscosity(15*np.ones(np.shape(Tm)),35.16504*np.ones(np.shape(Tm)))
    rhosw=Swd.SeawaterDensity(15*np.ones(np.shape(Tm)),35.16504*np.ones(np.shape(Tm)))
    
    PDHid=Idph.IdealDeliveredPowerHoltrop(U.ravel(),Tm.ravel(),Tf.ravel(),Lwl.ravel(),DISPV.ravel(),WSA.ravel(),Cb.ravel(),Cm.ravel(),Cwp.ravel(),lcb.ravel(),rhosw.ravel(),vsw.ravel(),Ar.ravel(),Ab.ravel(),hb.ravel(),Atr.ravel(),RDS)
    
    PDHid=PDHid.reshape(1,-1).T
    #Delivered power
    PDLightRef=Idp.IdealDeliveredPower(U,Tm,PDHid,RDS['Tref'],RDS['Pref'],RDS['Uref'],RDS['Tb'],RDS['Ts'])
    
    PDLightRef= PDLightRef.reshape(1,-1).T
    #Effective power
    PeffLight=PDLightRef/(RDS['nME']*RDS['etaT']*RDS['etaeffME'][0])

    #Propeller speed
    NHid=irh.IdealRPMHoltrop(PDLightRef,RDS['NMEST'][0,:],RDS['PeffMEST'][0,:]*RDS['nME'],RDS['etaT'],RDS['etaeffME'][0],RDS['factorN2'],RDS)
    NLightRef=Ir.IdealRPM(U,Tm,NHid,RDS['Tref'],RDS['Nref'],RDS['Uref'],RDS['Tb'],RDS['Ts'])
    
    #Light Running Curve
    PeffLightidME=Ist.IdealST(NLightRef,PeffLight,NME,0)
    
    return PeffLightidME