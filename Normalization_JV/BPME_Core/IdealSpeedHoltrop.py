# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 17:32:18 2020

@author: SunKumar

"""
import numpy as np
from scipy.interpolate import pchip
#from scipy.interpolate import InterpolatedUnivariateSpline 
from BPME_Common import IdealDeliveredPowerHoltrop as idph


def IdealSpeedHoltrop(Pcor,Tm,Tf,Lwl,V,S,Cb,Cm,Cwp,lcb,rhosw,vsw,Ar,Ab,hb,Atr,ReferenceDataStruct):
    Pcor,Tm,Tf,Lwl,V,S,Cb,Cm,Cwp,lcb,rhosw,vsw,Ar,Ab,hb,Atr=Pcor.astype('float64'),Tm.astype('float64'),Tf.astype('float64'),Lwl.astype('float64'),V.astype('float64'),S.astype('float64'),Cb.astype('float64'),Cm.astype('float64'),Cwp.astype('float64'),lcb.astype('float64'),rhosw.astype('float64'),vsw.astype('float64'),Ar.astype('float64'),Ab.astype('float64'),hb.astype('float64'),Atr.astype('float64')
    

    #If Holtrop data are not entered, skip this function
    if np.isnan(ReferenceDataStruct['HullProfile']).all() ==True:
        print("Holtrop data are not entered, thus UHid cannot be estimated!")
        UHid = np.full(Tm.shape,np.nan)
        return(UHid)
    
    
    UU=np.arange(ReferenceDataStruct['Uminref'],ReferenceDataStruct['Umaxref'] + .01,1).T
    
    
    #This for loop must be eliminated to improve code performance
    PDHUU = []
    UHid = []
    
    for i in range(0,len(Tm)):
        PDH=idph.IdealDeliveredPowerHoltrop(UU,Tm[i]*np.ones(UU.shape),
                                                Tf[i]*np.ones(UU.shape),
                                                Lwl[i]*np.ones(UU.shape),
                                                V[i]*np.ones(UU.shape),
                                                S[i]*np.ones(UU.shape),
                                                Cb[i]*np.ones(UU.shape),
                                                Cm[i]*np.ones(UU.shape),
                                                Cwp[i]*np.ones(UU.shape),
                                                lcb[i]*np.ones(UU.shape),
                                                rhosw[i]*np.ones(UU.shape),
                                                vsw[i]*np.ones(UU.shape),
                                                Ar[i]*np.ones(UU.shape),
                                                Ab[i]*np.ones(UU.shape),
                                                hb[i]*np.ones(UU.shape),
                                                Atr[i]*np.ones(UU.shape),
                                                ReferenceDataStruct)
        
#         PDHUU = np.array(PDHUU).T.astype(float)
#         print(PDHUU.shape)
#         if np.iscomplex(PDH).any() == False:
             
#              PDH = PDH.astype(float)
#         else:
#              UHid.append(float('NaN'))

        
        # print('PDH')
        # print(PDH)
        # print(type(PDH))
        if np.isnan(PDH.astype('float64')).all()==False:
            UH=pchip(PDH.astype('float64'),UU.astype('float64'))
            UHid.append(UH(Pcor[i].astype('float64'))[0])
        else:
            UHid.append(float('NaN'))
    
    UHid = np.array(UHid).reshape(1,-1).T
    return UHid
# if __name__ =="__main__":
#      Pcor=ServiceDataStruct['PSME'].astype(float)
#      Tm=ServiceDataStruct['Tm'].astype(float)
#      Tf=ServiceDataStruct['Tf'].astype(float)
#      Lwl=ServiceDataStruct['Lwl'].astype(float)
#      V=ServiceDataStruct['DISPV'].astype(float)
#      S=ServiceDataStruct['WSA'].astype(float)
#      Cb=ServiceDataStruct['Cb'].astype(float)
#      Cm=ServiceDataStruct['Cm'].astype(float)
#      Cwp=ServiceDataStruct['Cwp'].astype(float)
#      lcb=ServiceDataStruct['lcb'].astype(float)
#      rhosw=ServiceDataStruct['rhosw'].astype(float)
#      vsw=ServiceDataStruct['vsw'].astype(float)
#      Ar=ServiceDataStruct['Ar'].astype(float)
#      Ab=ServiceDataStruct['Ab'].astype(float)
#      hb=ServiceDataStruct['hb'].astype(float)
#      Atr=ServiceDataStruct['Atr'].astype(float)
#      ReferenceDataStruct=ReferenceDataStruct
#      IdealSpeedHoltrop = IdealSpeedHoltrop(Pcor.astype(float),Tm,Tf,Lwl,V,S,Cb,Cm,Cwp,lcb,rhosw,vsw,Ar,Ab,hb,Atr,ReferenceDataStruct)
#     
