"""
Created on Mon Apr 20 11:18:22 2020

@author: AkhilSurendran

% WeatherCorrectionIndex
% Description:  This function calculates the difference of the resultant
%               added power due to weather effects as reported by crew vs
%               weather provider. The result is normalized for the measured
%               delivered power and is provided as a percentage diffrence
%               with positive and negative values. Signs explanation:
%               -Positive means that the reported weather effect showed 
%               better than the weather provider performance
%               -Negative means that the reported weather effect showed
%               worse than the weather provider performance
%
% Input:        
%
% Output:       
%

%
% See also: 

"""
from BPME_Core import TEMP as T
from BPME_Core import DENS as D
from BPME_Core import STAWIND as wi
from BPME_Core import STAWAVE as wa
from BPME_Core import LOAD as L
import numpy as np

def WeatherCorrectionIndex(SDS,RDS):
    #Resistance/power increase due to temperature in N/kW
    DRt_wp=T.TEMP(SDS['STW'],SDS['WSA'],SDS['rhosw_wp'],SDS['vsw_wp'],RDS['Lbp'])
    DPt_wp=DRt_wp*SDS['STW']*0.514444/SDS['etaDid']/1000

    #Resistance/power increase due to density and temperature in N/kW
    DRrhow_wp=D.DENS(SDS['STW'],SDS['PDid'],SDS['etaDid'],SDS['rhosw_wp'])
    DPrhow_wp=DRrhow_wp*SDS['STW']*0.514444/SDS['etaDid']/1000

    #Resistance/power increase due to relative wind in N/kW
    DRwi_wp,temp=wi.STAWIND(SDS['SOG'].ravel(),SDS['HEAD'].ravel(),SDS['Uwit_wp'].ravel(),SDS['psiwit_wp'].ravel(),SDS['Tair_wp'].ravel(),SDS['pair_wp'].ravel(),SDS['Tm'].ravel(),SDS['teu'].ravel(),RDS['Td'],RDS['Tb'],RDS['B'],RDS['Zaid'],RDS['Zaid'],RDS['Ad'],RDS['TypeSTAWINDVessel'],RDS['SuperStructureSTAWINDVessel'])
    if DRwi_wp.ndim==1:
        DRwi_wp=DRwi_wp.reshape(1,-1).T
    DPwi_wp=DRwi_wp*SDS['SOG']*0.514444/SDS['etaDid']/1000

    #Resistance/power increase due to waves/swell in N/kW
    DRww_wp,temp=wa.STAWAVE(SDS['STW'],SDS['Tm'],SDS['Cb'],SDS['rhosw_wp'],SDS['hsw'].reshape(1,-1).T,SDS['Hwv_wp'].reshape(1,-1).T,SDS['Twv_wp'].reshape(1,-1).T,SDS['psiwvr_wp'],SDS['azbow'],RDS['Lbp'],RDS['B'],RDS['kyy'],RDS['g'])
    DRws_wp,temp=wa.STAWAVE(SDS['STW'],SDS['Tm'],SDS['Cb'],SDS['rhosw_wp'],SDS['hsw'].reshape(1,-1).T,SDS['Hsl_wp'].reshape(1,-1).T,SDS['Tsl_wp'].reshape(1,-1).T,SDS['psislr_wp'],SDS['azbow'],RDS['Lbp'],RDS['B'],RDS['kyy'],RDS['g'])
    DRw_wp=DRww_wp+DRws_wp
    DPw_wp=DRw_wp*SDS['STW']*0.514444/SDS['etaDid']/1000

    DR_wp=DRt_wp+DRrhow_wp+DRwi_wp+DRw_wp
    DPksi_wp=L.LOAD(SDS['STW'],SDS['PD'],DR_wp,SDS['etaDid'],RDS['ksiP'])
    DPD_wp=DPt_wp+DPrhow_wp+DPwi_wp+DPw_wp+DPksi_wp
    if SDS['PD'].ndim==1:
        SDS['PD']=SDS['PD'].reshape(1,-1).T
    #WI=(SDS['DPD']-DPD_wp)/SDS['PD']*100
    WI=np.divide((SDS['DPD'].astype(float)-DPD_wp.astype(float)), SDS['PD'].astype(float), out=np.full_like((SDS['DPD']-DPD_wp),float('nan')), where=SDS['PD']!=0)*100
    
    return WI