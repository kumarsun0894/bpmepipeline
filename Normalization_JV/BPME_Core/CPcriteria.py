# -*- coding: utf-8 -*-
import numpy as np

def CPcriteria(TmCP,UCP,FOCMECP,FwiCP,SSNCP,LCVFOMECP):
    #####################################################
    # Variable                          Units	Min	Max	
    # -------------------------------------------------
    # Agreed Draught                    m       2	25
    # Agreed Wind                       Bft     0	8
    # Agreed Sea State                  DSS     0	6
    # Agreed Speed                      kn      1	35
    # Agreed Fuel Oil Consumption       MT/24h	0.5	300
    # Agreed FO Reference LCV           Mj/kg	35	80
    ##################################################### 
    #Initialize
    cr=1
    cause=np.nan
    
    #Check FwiCP
    if len(FwiCP)!=1 or np.isnan(FwiCP) or np.floor(FwiCP)!=FwiCP or FwiCP<0 or FwiCP>8 :
        cr=0;
        cause='FwiCP';
        return [cr,cause]
    
    #Check SSNCP
    if len(SSNCP)!=1 or np.isnan(SSNCP) or np.floor(SSNCP)!=SSNCP or SSNCP<0 or SSNCP>6:
        cr=0;
        cause='SSNCP'
        return [cr,cause]
    
    #Check TmCP
    if len(TmCP)<1 or len(TmCP)>2 or np.any(np.isnan(TmCP)) or np.any((TmCP<2)|(TmCP>25)):
        cr=0;
        cause='TmCP'
        return [cr,cause]
    
    #Check size compatibility
    [nUrow,nUcol]=np.shape(UCP);
    [nFOCrow,nFOCcol]=np.shape(FOCMECP);
    if nUrow!=len(TmCP) or nFOCrow!=len(TmCP) or nUcol!=nFOCcol:
        cr=0;
        cause='VectorsSize'       
        return [cr,cause]
    
    #Check UCP
    if np.any(np.all(np.isnan(UCP.T))) or np.any(np.amin(UCP,axis=1)<0) or np.any(np.amax(UCP,axis=1)>35):
        cr=0;
        cause='UCP';
        return [cr,cause]
    
    #Check FOCMECP
    if np.any(np.all(np.isnan(FOCMECP.T))) or np.any(np.amin(FOCMECP,axis=1)<0.5) or np.any(np.amax(FOCMECP,axis=1)>300):
        cr=0;
        cause='FOCMECP'
        return [cr,cause]
    
    return [cr,cause]





    