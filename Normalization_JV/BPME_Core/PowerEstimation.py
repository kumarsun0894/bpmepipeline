# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 11:18:22 2020

@author: Subhin Antony

% PowerEstimation
% Description:  This function is called by BPMEngineSingleVessel and
%               performs the following steps:
%               -Apply 4 power estimation methods (SFOC, fuel index, TC
%                speed, load indicator)
%               -Check if shaft power meter is used
%               -The power is then taken from the best available method
%                based on the following priority: 1)shaft power meter,
%                2)load indicator, 3)TC speed, 4)fuel index and 5)SFOC
%
% Input:        SDS
%               RDS
%               DS
                Eidx        
%               LogType
%
% Output:       SDS
%               DS
%
% See also:     P_SFOC, P_FPI, P_NTC, P_LI, BPMEngineSingleVessel

"""
from BPME_Common import nansumwrapper as nsw
import sys
import numpy as np
import pandas as pd
from BPME_Core import P_SFOC as ps
from BPME_Core import P_FPI as pf
from BPME_Core import P_NTC as pn
from BPME_Core import P_LI as pl
from ExceptionHandler import ExceptionHandler
from BPME_Core import IdealGeneratorEfficiency as ige
from BPME_Common import nansumwrapper as nsw
import time
import logging 
# create logger
logger = logging.getLogger('BPMEngine.2.02')

def PowerEstimation(IMO,SDS,RDS,DS,Eidx,LogType,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        if LogType in ['Noon','HP','ME','Auto']:
            if len(str(Eidx))==0:
                Eidx=1
            if LogType in ['Noon','HP','Auto']:
                nME=int(RDS['nME'])
            elif LogType in 'ME':
                nME=1
        #ME effective power estimation methods implementation
            if SDS['LCVFOME'].ndim==1:
                SDS['LCVFOME']=SDS['LCVFOME'].reshape(1,-1).T
            if SDS['FPIME'].ndim==1:
                SDS['FPIME']=SDS['FPIME'].reshape(1,-1).T
            if SDS['NME'].ndim==1:
                SDS['NME']=SDS['NME'].reshape(1,-1).T
            if SDS['TFOPPinME'].ndim==1:
                SDS['TFOPPinME']=SDS['TFOPPinME'].reshape(1,-1).T
            if SDS['NTCME'].ndim==1:
                SDS['NTCME']=SDS['NTCME'].reshape(1,-1).T
            if SDS['TscavME'].ndim==1:
                SDS['TscavME']=SDS['TscavME'].reshape(1,-1).T
            if SDS['pambairER'].ndim==1:
                SDS['pambairER']=SDS['pambairER'].reshape(1,-1).T
            if SDS['pindME'].ndim==1:
                SDS['pindME']=SDS['pindME'].reshape(1,-1).T
            if SDS['rhoFOME'].ndim==1:
                SDS['rhoFOME']=SDS['rhoFOME'].reshape(1,-1).T
            if SDS['mFOCME'].ndim==1:
                SDS['mFOCME']=SDS['mFOCME'].reshape(1,-1).T
            
            if np.sum(np.char.count(RDS['DualFuelME'],'No'))==len(RDS['DualFuelME']):
                SDS['Peff1ME']=ps.P_SFOC(np.divide(SDS['mFOCME'].astype('float64'), nsw.nansumwrapper(SDS['RHME'],axis=1).reshape(1,-1).T.astype('float64'), out=np.full_like(SDS['mFOCME'].astype('float64'),float('nan')), where=nsw.nansumwrapper(SDS['RHME'],axis=1).reshape(1,-1).T.astype('float64')!=0),\
                                                   SDS['LCVFOME'],\
                                                       RDS['PeffMEST'][Eidx-1,:],\
                                                           RDS['SFOCcalcMEST'][Eidx-1,:],\
                                                               RDS['LCVFOMEST'][Eidx-1])
            
                # SDS['Peff1ME']=ps.P_SFOC(SDS['mFOCME']/nsw.nansumwrapper(SDS['RHME'],axis=1).reshape(1,-1).T,\
                #                                    SDS['LCVFOME'],\
                #                                        RDS['PeffMEST'][Eidx-1,:],\
                #                                            RDS['SFOCcalcMEST'][Eidx-1,:],\
                #                                                RDS['LCVFOMEST'][Eidx-1])
                SDS['Peff2ME']=pf.P_FPI(SDS['FPIME'],\
                                                  SDS['NME'],\
                                                      SDS['TFOPPinME'],\
                                                          SDS['rhoFOME'],\
                                                              SDS['LCVFOME'],\
                                                                  RDS['FPIMEST'][Eidx-1,:],\
                                                                      RDS['peffMEST'][Eidx-1,:],\
                                                                          RDS['TFOPPinMEST'][Eidx-1,:],\
                                                                              RDS['rhoFOMEST'][Eidx-1],\
                                                                                  RDS['LCVFOMEST'][Eidx-1],\
                                                                                      RDS['k2'][Eidx-1],\
                                                                                          RDS['ncME'][Eidx-1])
                SDS['Peff3ME']=pn.P_NTC(SDS['NTCME'],\
                                               SDS['TscavME'],\
                                                   SDS['pambairER'],\
                                                       RDS['NTCMEST'][Eidx-1,:],\
                                                           RDS['TscavMEST'][Eidx-1,:],\
                                                               RDS['pambairMEST'][Eidx-1,:],\
                                                                   RDS['PeffMEST'][Eidx-1,:],\
                                                                       RDS['TCNomogram'])
                SDS['Peff4ME']=pl.P_LI(SDS['pindME'],\
                                              SDS['NME'],\
                                                  RDS['k1'],\
                                                      RDS['k2'][Eidx-1],\
                                                          RDS['ncME'][Eidx-1])
                SDS['Peff1ME']=SDS['Peff1ME'].reshape(int(RDS['nME']),-1).T
                SDS['Peff2ME']=SDS['Peff2ME'].reshape(int(RDS['nME']),-1).T
                SDS['Peff3ME']=SDS['Peff3ME'].reshape(int(RDS['nME']),-1).T
                SDS['Peff4ME']=SDS['Peff4ME'].reshape(int(RDS['nME']),-1).T
            
            elif 'Yes' in RDS['DualFuelME']:
            #All methods above must be improved to accomodate multiple and DF engines
                SDS['Peff1ME']=np.empty((int(len(SDS['pambairER'])),nME))
                SDS['Peff1ME'][:]=np.nan
                SDS['Peff2ME']=np.empty((int(len(SDS['pambairER'])),nME))
                SDS['Peff2ME'][:]=np.nan
                SDS['Peff3ME']=np.empty((int(len(SDS['pambairER'])),nME))
                SDS['Peff3ME'][:]=np.nan
                SDS['Peff4ME']=np.empty((int(len(SDS['pambairER'])),nME))
                SDS['Peff4ME'][:]=np.nan
            
            #ME shaft power calculation in [kW]
            for i in range(0,nME):        
                for j in range(0,int(len(SDS['pambairER']))):
                    if pd.isnull(SDS['PSME'][j,i]):
                        if not (pd.isnull(SDS['DESME'][j,i])):
                            SDS['PSME'][j,i]=SDS['DESME'][j,i]/SDS['RHME'][j,i]
                        elif not (pd.isnull(SDS['Peff4ME'][j,i])):
                            SDS['PSME'][j,i]=SDS['Peff4ME'][j,i]*RDS['etaB']*RDS['etaeffME'][i]
                        elif not(pd.isnull(SDS['Peff3ME'][j,i])):
                            SDS['PSME'][j,i]=SDS['Peff3ME'][j,i]*RDS['etaB']*RDS['etaeffME'][i]
                        elif not (pd.isnull(SDS['Peff2ME'][j,i])):
                            SDS['PSME'][j,i]=SDS['Peff2ME'][j,i]*RDS['etaB']*RDS['etaeffME'][i]
                        elif not(pd.isnull(SDS['Peff1ME'][j,i])):
                            SDS['PSME'][j,i]=SDS['Peff1ME'][j,i]*RDS['etaB']*RDS['etaeffME'][i]
                        else:
                            SDS['PSME'][j,i]=np.nan
            #SDS['PSME'][SDS['PSME']<0]=np.nan
            #Indicate logs with missing PSME
            DS['MissingPSME']=np.zeros(SDS['PSME'].shape)
            PSMEcheck=pd.isnull(SDS['PSME']) | (SDS['PSME']<0)   ###
            if len(PSMEcheck)>0:
                SDS['PSME'][PSMEcheck]=np.nan
                DS['MissingPSME'][PSMEcheck]=1
                print('Power estimation error!')
            #Indicate logs with a discrepancy between PeffestME and PeffME >1#
            DS['PeffMEDiscrepancy']=np.zeros((len(SDS['pambairER']),nME))
            if SDS['PeffestME'].ndim==1:
                SDS['PeffestME']= SDS['PeffestME'].reshape(1,-1).T
#            PeffMEDiscrepancycheck=np.flatnonzero(abs((SDS['PSME']-SDS['PeffestME']*RDS['etaB']*RDS['etaeffME'][Eidx-1])/SDS['PSME'])>0.01).reshape(1,-1).T
            #PeffMEDiscrepancycheck=np.flatnonzero(abs(np.divide((SDS['PSME']-SDS['PeffestME']*RDS['etaB']*RDS['etaeffME'][Eidx-1]).astype('float64'),SDS['PSME'].astype('float64'),out=np.full_like((SDS['PSME']-SDS['PeffestME']*RDS['etaB']*RDS['etaeffME'][Eidx-1]).astype('float64'),float('nan')),where=SDS['PSME'].astype('float64')!=0))>0.01).reshape(1,-1).T
            if nME==1:
                #PeffMEDiscrepancycheck=np.flatnonzero(abs((SDS['PSME']-SDS['PeffestME']*RDS['etaB']*RDS['etaeffME'][Eidx-1])/SDS['PSME'])>0.01).reshape(1,-1).T
                PeffMEDiscrepancycheck=np.flatnonzero(abs(np.divide((SDS['PSME']-SDS['PeffestME']*RDS['etaB']*RDS['etaeffME'][Eidx-1]), SDS['PSME'], out=np.full_like((SDS['PSME']-SDS['PeffestME']*RDS['etaB']*RDS['etaeffME'][Eidx-1]),float('nan')), where=SDS['PSME']!=0))>0.01).reshape(1,-1).T
            else:
                PeffMEDiscrepancycheck=(abs((SDS['PSME']-SDS['PeffestME']*RDS['etaB']*RDS['etaeffME'][Eidx-1])/SDS['PSME'])>0.01)
            if len(PeffMEDiscrepancycheck)>0:
                #SDS.LogValidity(PeffMEDiscrepancycheck)=0
                DS['PeffMEDiscrepancy'][PeffMEDiscrepancycheck]=1
                print('Estimated effective power by crew vs calculated discrepancy > 1#!')
            
            #Use PeffestME if PSME=NaN
            DS['PeffestMEUsage']=np.zeros((len(SDS['pambairER']),nME))
            PeffestMEUsagecheck=np.flatnonzero(pd.isnull(SDS['PSME']) &( ~pd.isnull(SDS['PeffestME'])))
            if len(PeffestMEUsagecheck)>0:
                #SDS.LogValidity(PeffestMEUsagecheck)=0
                SDS['PSME'][PeffestMEUsagecheck]=SDS['PeffestME'][PeffestMEUsagecheck]*RDS['etaB']*RDS['etaeffME'][Eidx-1]
                DS['PeffestMEUsage'][PeffestMEUsagecheck]=1
                print('Estimated effective power by crew is used!')
            
            if 'ME' in LogType:
                #Check discrepancy between reported Pind and calculated from pind
                DS['PindDiscrepancy']=np.zeros(SDS['pambairER'].shape)
                SDS['PindcalcME']=RDS['ncME'][Eidx-1]*RDS['k2'][Eidx-1]*SDS['NME']*SDS['pindME']
                if SDS['PindME'].ndim==1:
                    SDS['PindME']=SDS['PindME'].reshape(1,-1).T
                PindDiscrepancycheck=np.flatnonzero(abs(np.divide((SDS['PindME']-SDS['PindcalcME']),SDS['PindcalcME'],out=np.full_like((SDS['PindME']-SDS['PindcalcME']),float("nan")),where=SDS['PindcalcME']!=0))>0.01)
                if len(PindDiscrepancycheck)>0:
                    #SDS.LogValidity(PindDiscrepancycheck)=0
                    DS['PindDiscrepancy'][PindDiscrepancycheck]=1
                    print('Indicated power reported by crew and calculated are different!')
    
                #Use PindME if PSME=NaN
                DS['PindUsage']=np.zeros(SDS['pambairER'].shape)
                PindUsagecheck=np.flatnonzero(pd.isnull(SDS['PSME']) &(~pd.isnull(SDS['PindME'])))
                
                if len(PindUsagecheck)>0:
                    #SDS.LogValidity(Peffestcheck)=0
                    SDS['PSME'][PindUsagecheck]=(SDS['PindME'][PindUsagecheck]-RDS['ncME'][Eidx-1]*RDS['k2'][Eidx-1]*SDS['NME'][PindUsagecheck])*RDS['etaB']*RDS['etaeffME'][Eidx-1]
                    DS['PindUsage'][PindUsagecheck]=1
                    print('Indicated power by crew is used!')
            
            #For now we assume simple propulsion chain, where the relationship between
            #PD, PS, PB and Peff are handled with the respective efficiencies.
            #This is not the case for complex propulsion chain, when multiple engines,
            #reduction gears, shaft generators and CPP exist.
            #All the above complexities must be considered in the formulas in order to
            #calculate properly the corresponding powers.
    
            #For multiple engines we add all PSME accross engines (nansum)
            SDS['PD']=nsw.nansumwrapper(SDS['PSME'],axis=1)*RDS['etaS']               #average delivered power during log in [kW]
            SDS['PeffME']=SDS['PSME']/RDS['etaB']/RDS['etaeffME'][Eidx-1]   #average break power during log in [kW]
            SDS['MCRME']=SDS['PeffME']/RDS['PMCRME'][Eidx-1]*100   #average MCR percentage during log in [%]
        
            if 'ME' in LogType:
                #Estimate mean effective pressure based on MAN formula
                #SDS['peffestME']=SDS['PeffME']/(RDS['k2'][Eidx-1]*SDS['NME']*RDS['ncME'][Eidx-1]) #in bar
                SDS['peffestME'] = np.divide(SDS['PeffME'],(RDS['k2'][Eidx-1]*SDS['NME'].astype('float64')*RDS['ncME'][Eidx-1]), out= np.full_like(SDS['PeffME'],float("nan")),where=(RDS['k2'][Eidx-1]*SDS['NME']*RDS['ncME'][Eidx-1])!=0 )
                #Estimate mean indicated pressure based on MAN formula
                SDS['pindestME']=SDS['peffestME']+RDS['k1'] #in bar
    
                #Indicate logs with missing pind
                DS['Missingpind']=np.zeros(SDS['pambairER'].shape)
                Missingpindcheck=np.flatnonzero(np.isnan(SDS['pindME']))
                if len(Missingpindcheck)>0:
                    DS['Missingpind'][Missingpindcheck]=1
                    SDS['pindME'][Missingpindcheck]=SDS['pindestME'][Missingpindcheck]
                    print('Indicated pressure is missing!')
        
        if 'Noon'in LogType or 'AE' in LogType:
    
#            if LogType in 'Noon':
#                nAE=int(RDS['nAE'])
#                AEidx2=np.arange(0,RDS['nAE']).astype('int')
#            elif LogType in 'AE':
#                nAE=int(1)
#                AEidx2=Eidx-1
#           for i in range(0,nAE):
            if LogType in 'Noon':
                AEidx=np.arange(0,int(RDS['nAE']))
            elif LogType in 'AE':
                AEidx=Eidx-1
            if SDS['PelAE'].ndim==1:
                SDS['PelAE']=SDS['PelAE'].reshape(1,-1).T
            if SDS['DEelAE'].ndim==1:
                SDS['DEelAE']=SDS['DEelAE'].reshape(1,-1).T
                
            
            SDS['PelAE'][pd.isnull(SDS['PelAE'])]=SDS['DEelAE'][pd.isnull(SDS['PelAE'])]/SDS['RHAE'][pd.isnull(SDS['PelAE'])]    
       
            #Indicate logs with missing or negative PengAE
            DS['MissingPelAE']=np.zeros(SDS['PelAE'].shape)
            PelAEcheck=pd.isnull(SDS['PelAE']) | (SDS['PelAE']<0)
            
            if np.any(PelAEcheck):
                SDS['PelAE'][PelAEcheck]=np.nan
                DS['MissingPelAE'][PelAEcheck]=1
                print('AE Electric Power estimation error!')
            
            #######################################################################
            # Going forward we can add the equivalent diagnostics and estimations
            # used for MEs. Currently this is on hold until FPC conlcude how to
            # treat the variable PengAE_avg.
            # -Indicate logs with a discrepancy between PeffestME and PeffME >1#
            # -Use PeffestME if PSME=NaN
            #######################################################################
   
            SDS['MCRgenAE']=100*np.divide(SDS['PelAE'],RDS['PgennomRAE'][0][AEidx],out=np.full_like(SDS['PelAE'],float("nan")),where=RDS['PgennomRAE'][0][AEidx]!=0)
            
            SDS['etagenAE']=ige.IdealGeneratorEfficiency(SDS['MCRgenAE'],RDS['MCRgenAEST'][AEidx,:],RDS['etagenAEST'][AEidx,:])
            
            #This is an estimated power factor
            #It must be changed to 0.8 or to an entry from the vessel
            SDS['PFAE']=1#ones(length(SDS.pambairER),nAE);
            SDS['etasysAE']=SDS['etagenAE']*SDS['PFAE']
            
            SDS['PeffAE']=np.divide(SDS['PelAE'],SDS['etasysAE'],out=np.full_like(SDS['PelAE'],float("nan")),where=SDS['etasysAE']!=0)
            if (RDS['PengnomAE'].ndim==1) and (SDS['PeffAE'].ndim==2):
                RDS['PengnomAE']=RDS['PengnomAE'].reshape(1,-1)
                SDS['MCRAE']=SDS['PeffAE']/RDS['PengnomAE'][0,AEidx]*100
                RDS['PengnomAE']=RDS['PengnomAE'].ravel()
            else:
                SDS['MCRAE']=SDS['PeffAE']/RDS['PengnomAE'][0,AEidx]*100
            SDS['DEengAE']=SDS['PeffAE']*SDS['RHAE']    
            # if AEidx2.ndim==1:
            #     AEidx2=AEidx2.reshape(1,-1)
        if 'Noon'in LogType :
            #1
            SDS['MCRMEPD']=SDS['PD']/RDS['PMCRME'][Eidx-1]*100    
        
    
        # SRQST['TotalTimeLapse'].append(time.time()-start_time)
        # SRQST['Remarks'].append("complete")
        # return(SDS,DS,SRQST)  
    except Exception as err:
        exp = "Error in PowerEstimation {} in line No. {} for IMO {}".format(err,sys.exc_info()[-1].tb_lineno,IMO)
        logger.error(exp,exc_info=True)
        ExceptionHandler(LogType,exp)
        SRQSTD['TotalTimeLapse']=time.time()-start_time              
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return({},{},SRQST)
        
    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time               
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(SDS,DS,SRQST)

    
    
    
    
 
