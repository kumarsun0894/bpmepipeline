# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 20:26:11 2020

@author: Subhin Antony

% P_FPI
% Description:  This function is used to estimate the effective power using
%               the fuel pump index method as formulated based on MAN plate
%               70627 and the respective application. To achieve better
%               results, the fuel index measurement is corrected for FO
%               temperature, density and LCV.
%
% Input:        FPI     [-]     fuel index (vector)
%               N       [RPM]   speed (vector)
%               TFO     [°C]    FO temperature at fuel pump inlet (vector)
%               rhoFO   [kg/m3] FO density (vector)
%               LCV     [Mj/kg] FO LCV (vector)
%               FPIST   [-]     shop test fuel index (vector)
%               MEPST   [bar]   shop test mean effective pressure (vector)
%               TFOST   [°C]    shop test FO temperature at fuel pump inlet (vector)
%               rhoST   [kg/m3] shop test FO density (scalar)
%               LCVST   [Mj/kg] shop test FO LCV (scalar)
%               k2      [-]     cylinder constant as defined by MAN (scalar)
%               nc      [-]     number of cylinders (scalar)
%
% Output:       P       [kW]    estimated effective power (vector)
%
% Note:         In the future for more accurate results we may introduce
%               the fuel index offset as an additional input as this is
%               frequently reffered by Wartsila.
%
% See also:     PowerEstimation, P_SFOC, P_NTC, P_LI

"""
import numpy as np
from BPME_Common import VolumeCorrectionFactor as vcf
#from scipy.interpolate import pchip
from BPME_Common.pchipwrap import pchipwrap as pchip
#from scipy.interpolate import InterpolatedUnivariateSpline
def P_FPI(FPI,N,TFO,rhoFO,LCV,FPIST,MEPST,TFOST,rhoFOST,LCVST,k2,nc):
    #Correct shop test fuel pump index for density and temperature
    rhoFOSTcor=(rhoFOST-1.1)*vcf.VolumeCorrectionFactor(rhoFOST,TFOST) #FO density for temperature in [kg/m3] (new correction)
    FPISTcor=FPIST*rhoFOSTcor/1000*LCVST/42.7
    
    #Correct current fuel pump index for current fuel density, temperature and LCV
    rhoFOcor=(rhoFO-1.1)*vcf.VolumeCorrectionFactor(rhoFO,TFO) #FO density for temperature in [kg/m3] (new correction)
    FPIcor=FPI*rhoFOcor/1000*LCV/42.7
    FPIcor=FPIcor.astype('float64')
    #Estimate MEP on FPIcor-MEP model
    pc=pchip(FPISTcor,MEPST)
    MEP=pc(FPIcor)
   
    
    #Estimate effective power
    P=k2*nc*N*MEP
    # FPI=FPI.ravel()
    #Check if FPI is within the shop tested range
    FPIcheck=FPI<(np.min(FPIST))
    if np.sum(FPIcheck)>0:
        P[FPIcheck]=np.nan
        print('P_FPI error: FPI too low for the method to be applied!')
    return(P)
    


