"""
Created on Mon Apr 20 11:18:22 2020

@author: Subhin Antony

% TEMP
% Description:  This function implements part of the method described in
%               ISO15016:2015, Annex E. The formula below estimates the
%               resistance increase due to the effects of water
%               temperature as it is expressed in terms of water density
%               and viscosity change.
%
% Input:        STW     [kn]    ship speed through water (vector)
%               S       [m2]    actual wetted surface (vector)
%               rhow    [kg/m3] water density (vector)
%               vw      [m2/s]  sea water viscosity (vector)   
%               Los     [m]     overall submerged length (approximated by Lbp) (scalar)
%
% Output:       DRt     [N]     resistance increase due to water temperature (vector)
%
% Standards:    Temperature of the seawater is 15°C and density is 1026kg/m3.
%
% Note:         According to ITTC 7.5-02-01-03: In general, the water
%               properties are a function of temperature, pressure and
%               absolute salinity. In this procedure, data are provided at
%               standard pressure of 0.101325 MPa. Assuming standard
%               absolute salinities as Sfwa=0g/kg (fresh water) and
%               Sswa=35.16504g/kg (sea water), the rest of the properties
%               (e.g.density, viscosity, etc.) are only functions of
%               temperature.
%
% See also:     STATOTAL, DENS, DensityStandardSeawater, ViscosityStandardSeawater

"""
import numpy as np
from BPME_Common import ViscosityStandardSeawater as VSSW

def TEMP(STW,S,rhow,vw,Los):
#    if STW.ndim ==1:
#       STW = STW.reshape(1,-1).T
#    if S.ndim ==1:
#       S = S.reshape(1,-1).T
#    if rhow.ndim ==1:
#       rhow = rhow.reshape(1,-1).T
#    if vw.ndim ==1:
#       vw = vw.reshape(1,-1).T  
    #Convert ship speed through water from kn to m/s
    U=STW*0.514444
    
    #Ideal kinematic viscosity calculation
    vwid=VSSW.ViscosityStandardSeawater(15)
    
    #Reynolds number calculation
    Rn=U*Los/vw
    Rnid=U*Los/vwid
    
    #Frictional resistance coefficient calculation based on the 1957 ITTC line
    Cf=0.075/(np.log10(Rn.astype('float64'))-2)**2
    Cfid=0.075/(np.log10(Rnid.astype('float64'))-2)**2
    
    #Frictional resistance calculation
    Rf=1/2*rhow*S*U**2*Cf
    
    #Calculation of resistance increase due to deviation of water temperature
    DRt=np.empty(np.shape(STW))
    DRt[:] = np.nan
    for i in range(0,len(STW)):
        if np.isreal(Rf[i])[0]:
            DRt[i][0]=Rf[i][0]*(1-Cfid[i][0]/Cf[i][0])
    
    return DRt
    