# -*- coding: utf-8 -*-

"""
Created on Sun Apr 26 23:23:03 2020

@author: AkhilSurendran

% CalculateSFOC
% Description:  This function calculates the ME/AE SFOC, SFOC ISO corrected
%               and FOCLCVcorME and appends them on SDS. It works both for
%               conventional as well as dual fuel engines.
%
% Input:        SDS     Service data (structure)
%               RDS     Reference data (structure)
%               DS      Diagnostics (structure)
%
% Output:       SDS     Service data (structure)
%               DS      Diagnostics (structure)
%
% See also:     BPMEngineSingleVessel, ManipulateServiceData,
%               HPOutputFiltering, RefDashboards


"""
import numpy as np
import sys
from ExceptionHandler import ExceptionHandler
from BPME_Common import SFOCiso
from BPME_Common import nansumwrapper as nsw
import time 
import logging 
# create logger
logger = logging.getLogger('BPMEngine.2.02')
def CalculateSFOC(IMO,SDS,RDS,LogType,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
   
    
        DFM = RDS['DualFuelME']
        if LogType in 'Noon'or LogType in 'HP' or LogType in 'ME' or LogType in 'Auto':
    
            if(np.size(np.where(DFM == 'No')) == np.size(DFM)):
                if SDS['mFOCME'].ndim==1:
                    SDS['mFOCME']=SDS['mFOCME'].reshape(1,-1).T
                SDS['SFOCME']=np.divide(1000*SDS['mFOCME'],(nsw.nansumwrapper(SDS['PeffME']*SDS['RHME'],axis=1).reshape(1,-1).T).astype('float64'),out = np.full_like(1000*SDS['mFOCME'],float("nan")),where =(nsw.nansumwrapper(SDS['PeffME']*SDS['RHME'],axis=1).reshape(1,-1).T)!=0)
                SDS['SFOCisoME']=SFOCiso.SFOCiso(SDS['SFOCME'],
                                            SDS['LCVFOME'],
                                            RDS['LCVMDOref'],
                                            np.nanmean(SDS['TltcwACinME'],axis=1).reshape(1,-1).T,# %nanmean is added bdue to great number of data gaps...
                                            SDS['TairTCinME'],
                                            SDS['pambairER'].reshape(1,-1).T)
                #dz1=np.divide(24*SDS['mFOCME'],(1000*np.nanmean(SDS['RHME'],axis= 1).reshape(1,-1).T),out=np.full_like(SDS['mFOCME'],float("nan")),where = (1000*np.nanmean(SDS['RHME'],axis= 1).reshape(1,-1).T)!=0)
                dz1=np.divide(24*SDS['mFOCME'],(1000*np.nanmean(SDS['RHME'],axis= 1).astype(float).reshape(1,-1).T),out=np.full_like(SDS['mFOCME'],float("nan")),where = (1000*np.nanmean(SDS['RHME'],axis= 1).reshape(1,-1).T)!=0)
                dz2=np.divide(SDS['LCVFOME'],RDS['LCVHFOref'],out= np.full_like(SDS['LCVFOME'],float("nan")),where= RDS['LCVHFOref']!=0)
    
                SDS['FOCLCVcorME']=dz1*dz2
                                            
                                              
            elif (len(DFM[DFM == 'Yes']) >0):
                SDS['SFOCME'] = np.empty(np.shape(SDS['pambairER']))
                SDS['SFOCME'][:]=np.nan
                SDS['SFOCisoME']=np.empty(np.shape(SDS['pambairER']))
                SDS['SFOCisoME'][:]= np.nan
                SDS['FOCLCVcorME']=np.empty(np.shape(SDS['pambairER']))
                SDS['FOCLCVcorME'][:] = np.nan
                SDS['SPOCME']=np.empty(np.shape(SDS['pambairER']))
                SDS['SPOCME'][:]= np.nan
                SDS['SPOCisoME']=np.empty(np.shape(SDS['pambairER']))
                SDS['SPOCisoME'][:] = np.nan
                SDS['POCLCVcorME']=np.empty(np.shape(SDS['pambairER']))
                SDS['POCLCVcorME'][:] = np.nan 
                SDS['SGOCME']=np.empty(np.shape(SDS['pambairER']))
                SDS['SGOCME'][:] = np.nan
                SDS['SGOCisoME']=np.empty(np.shape(SDS['pambairER']))
                SDS['SGOCisoME'][:] = np.nan
                SDS['GOCLCVcorME']=np.empty(np.shape(SDS['pambairER']))
                SDS['GOCLCVcorME'][:] = np.nan                            
       
                ############################## FUEL MODE #############################################
        
                SDS['SFOCME'][SDS['FOModeME'],0]=1000*SDS['mFOCME'][SDS['FOModeME'],0]/nsw.nansumwrapper(SDS['PeffME'][SDS['FOModeME'].ravel(),:]*SDS['RHME'][SDS['FOModeME'].ravel(),:],axis=1).reshape(1,-1).T   #mean SFOC in [g/kWh]
                #1000*SDS['mFOCME'][SDS['FOMode'],0]/((np.sum(SDS['PeffME'][SDS['FOMode'].ravel(),:],axis=1).reshape(1,-1).T)*SDS['Dt'][SDS['FOMode'],0])  #mean SFOC in [g/kWh]
                                                                    
                                                      
                SDS['SFOCisoME'][SDS['FOModeME'],0]=SFOCiso.SFOCiso(SDS['SFOCME'][SDS['FOModeME'],0],
                                                                      SDS['LCVFOME'][SDS['FOModeME'],0],
                                                                      RDS['LCVMDOref'],
                                                                      (np.nanmean(SDS['TltcwACinME'][SDS['FOModeME'].ravel(),:],axis=1)).reshape(1,-1).T,# %nanmean is added bdue to great number of data gaps...
                                                                      SDS['TairTCinME'][SDS['FOModeME'],0],
                                                                      SDS['pambairER'][SDS['FOModeME'],0])
    
                SDS['FOCLCVcorME'][SDS['FOModeME'],0]=24*SDS['mFOCME'][SDS['FOModeME'],0]/(1000*np.nanmean(SDS['RHME'][SDS['FOModeME'],0],axis=1).reshape(1,-1).T)*SDS['LCVFOME'][SDS['FOModeME'],0]/RDS['LCVHFOref'] 
       
    
                ############################# GAS MODE ###################################################                                                                 
                                                                
                ############Pilot###############
                SDS['SPOCME'][SDS['GOModeME'],0]=1000*SDS['mFOCME'][SDS['GOModeME'],0]/(nsw.nansumwrapper(SDS['PeffME'][SDS['GOModeME'].ravel(),:]*SDS['RHME'][SDS['GOModeME'].ravel(),:],axis=1).reshape(1,-1).T)
                                                                      
                SDS['SPOCisoME'][SDS['GOModeME'],0]=SFOCiso.SFOCiso(SDS['SPOCME'][SDS['GOModeME'],0],
                                                                      SDS['LCVFOME'][SDS['GOModeME'],0],
                                                                      RDS['LCVMDOref'],
                                                                      np.nanmean(SDS['TltcwACinME'][SDS['GOModeME'].ravel(),:],axis=1).reshape(1,-1).T,# %nanmean is added bdue to great number of data gaps...
                                                                      SDS['TairTCinME'][SDS['GOModeME'],0],
                                                                      SDS['pambairER'][SDS['GOModeME'],0])
                                                                 
                SDS['POCLCVcorME'][SDS['GOModeME'],0] = 24*SDS['mFOCME'][SDS['GOModeME'],0]/(1000*np.nanmean(SDS['RHME'][SDS['GOModeME'],0],axis=1).reshape(1,-1).T)*SDS['LCVFOME'][SDS['GOModeME'],0]/RDS['LCVHFOref']
             
            
                ############Pilot###############
                #SDS['LCVGOME']=50*np.ones(np.shape(SDS['pambairER']))
                
                SDS['SGOCME'][SDS['GOModeME'],0]=1000*SDS['mGOCME'][SDS['GOModeME'],0]/(nsw.nansumwrapper(SDS['PeffME'][SDS['GOModeME'].ravel(),:]*SDS['RHME'][SDS['GOModeME'].ravel(),:],axis=1).reshape(1,-1).T)
                                                                          #mean SPOC in [g/kWh]
                if SDS['LCVGOME'].ndim==1:
                    SDS['LCVGOME']= SDS['LCVGOME'].reshape(1,-1).T                                                         
                
                SDS['SGOCisoME'][SDS['GOModeME'],0]=SFOCiso.SFOCiso(SDS['SGOCME'][SDS['GOModeME'],0],
                                                                      SDS['LCVGOME'][SDS['GOModeME'],0],
                                                                      RDS['LCVMDOref'],
                                                                      np.nanmean(SDS['TltcwACinME'][SDS['GOModeME'].ravel(),:],axis=1).reshape(1,-1).T,#nanmean is added bdue to great number of data gaps...
                                                                      SDS['TairTCinME'][SDS['GOModeME'],0],
                                                                      SDS['pambairER'][SDS['GOModeME'],0])
                SDS['GOCLCVcorME'][SDS['GOModeME'],0]=24*SDS['mGOCME'][SDS['GOModeME'],0]/(1000*np.nanmean(SDS['RHME'][SDS['GOModeME'],0],axis=1).reshape(1,-1).T)*SDS['LCVGOME'][SDS['GOModeME'],0]/RDS['LCVHFOref'] #GOC in [MT/day]
                                         
                                                           
                #Embed SGOC+SPOC in SFOC vectors for a uniform terminology
                SDS['SFOCME'][SDS['GOModeME'],0]=SDS['SGOCME'][SDS['GOModeME'],0]+SDS['SPOCME'][SDS['GOModeME'],0]
                SDS['SFOCisoME'][SDS['GOModeME'],0]=SDS['SGOCisoME'][SDS['GOModeME'],0]+SDS['SPOCisoME'][SDS['GOModeME'],0]
                SDS['FOCLCVcorME'][SDS['GOModeME'],0]=SDS['GOCLCVcorME'][SDS['GOModeME'],0]+SDS['POCLCVcorME'][SDS['GOModeME'],0]
        
        elif LogType in 'AE':
            if SDS['LCVFOAE'].ndim==1:
                SDS['LCVFOAE'] = SDS['LCVFOAE'].reshape(1,-1).T
            if SDS['LCVGOAE'].ndim==1:
                SDS['LCVGOAE'] = SDS['LCVGOAE'].reshape(1,-1).T
            #Conventional engines
            if (np.size(np.where(DFM == 'No')) == np.size(DFM)):
                SDS['SFOCAE']=1000*SDS['mFOCAE'].reshape(1,-1).T/(SDS['PeffAE']*SDS['RHAE'])   #mean SFOC in [g/kWh]
                SDS['SFOCisoAE']=SFOCiso.SFOCiso(SDS['SFOCAE'],SDS['LCVFOAE'],RDS['LCVMDOref'],SDS['TltcwACinAE'],SDS['TairTCinAE'],SDS['pambairER'])
            
            #Dual fuel engines
            elif (len(DFM[DFM == 'Yes']) >0):
            
                #Memory pre-allocation
                SDS['SFOCAE']=np.empty(SDS['pambairER'].shape)
                SDS['SFOCisoAE']=np.empty(SDS['pambairER'].shape)
                SDS['SPOCAE']=np.empty(SDS['pambairER'].shape)
                SDS['SPOCisoAE']=np.empty(SDS['pambairER'].shape)
                SDS['SGOCAE']=np.empty(SDS['pambairER'].shape)
                SDS['SGOCisoAE']=np.empty(SDS['pambairER'].shape)
    
                #Fuel mode
                SDS['SFOCAE'][SDS['FOModeAE'],0]=1000*SDS['mFOCAE'][SDS['FOModeAE'],0]/(SDS['PeffAE'][SDS['FOModeAE'],0]*SDS['RHAE'][SDS['FOModeAE'],0])
                SDS['SFOCisoAE'][SDS['FOModeAE'],0]=SFOCiso.SFOCiso(SDS['SFOCAE'][SDS['FOModeAE'],0],
                                                                    SDS['LCVFOAE'][SDS['FOModeAE'],0],
                                                                    RDS['LCVMDOref'],
                                                                    (np.nanmean(SDS['TltcwACinAE'][SDS['FOModeAE'].ravel(),:],axis=1)).reshape(1,-1).T,
                                                                    SDS['TairTCinAE'][SDS['FOModeAE'],0],
                                                                    SDS['pambairER'][SDS['FOModeAE'],0])
    
                #Gas mode (pilot: FO)
                SDS['SPOCAE'][SDS['GOModeAE'],0]=1000*SDS['mFOCAE'][SDS['GOModeAE'],0]/(SDS['PeffAE'][SDS['GOModeAE'],0]*SDS['RHAE'][SDS['GOModeAE'],0])
                SDS['SPOCisoAE'][SDS['GOModeAE'],0]=SFOCiso.SFOCiso(SDS['SPOCAE'][SDS['GOModeAE'],0],
                                                                    SDS['LCVFOAE'][SDS['GOModeAE'],0],
                                                                    RDS['LCVMDOref'],
                                                                    (np.nanmean(SDS['TltcwACinAE'][SDS['GOModeAE'].ravel(),:],axis=1)).reshape(1,-1).T,
                                                                    SDS['TairTCinAE'][SDS['GOModeAE'],0],SDS['pambairER'][SDS['GOModeAE'],0])
    
                #Gas mode (main fuel: Gas)
                SDS['SGOCAE'][SDS['GOModeAE'],0]=1000*SDS['mGOCAE'][SDS['GOModeAE'],0]/(SDS['PeffAE'][SDS['GOModeAE'],0]*SDS['RHAE'][SDS['GOModeAE'],0])
                SDS['SGOCisoAE'][SDS['GOModeAE'],0]=SFOCiso.SFOCiso(SDS['SGOCAE'][SDS['GOModeAE'],0],
                                                                    SDS['LCVGOAE'][SDS['GOModeAE'],0],
                                                                    RDS['LCVMDOref'],
                                                                    (np.nanmean(SDS['TltcwACinAE'][SDS['GOModeAE'].ravel(),:],axis=1)).reshape(1,-1).T,
                                                                    SDS['TairTCinAE'][SDS['GOModeAE'],0],
                                                                    SDS['pambairER'][SDS['GOModeAE'],0])
    
                #Embed SGOC+SPOC in SFOC vectors for a uniform terminology
                SDS['SFOCAE'][SDS['GOModeAE'],0]=SDS['SGOCAE'][SDS['GOModeAE'],0]+SDS['SPOCAE'][SDS['GOModeAE'],0]
                SDS['SFOCisoAE'][SDS['GOModeAE'],0]=SDS['SGOCisoAE'][SDS['GOModeAE'],0]+SDS['SPOCisoAE'][SDS['GOModeAE'],0]
        # SRQST['TotalTimeLapse'].append(time.time()-start_time)
        # SRQST['Remarks'].append("complete")
        # return (SDS,SRQST)
    except Exception as err:
        exp = "Error in CalculateSFOC {} in line No. {} for IMO {}".format(err,sys.exc_info()[-1].tb_lineno,IMO)
        logger.error(exp,exc_info=True)
        ExceptionHandler(LogType,exp)
        SRQSTD['TotalTimeLapse']=time.time()-start_time              
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return({},SRQST)
        
    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time               
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(SDS,SRQST)