# -*- coding: utf-8 -*-
import numpy as np
#import scipy.interpolate as ipchip
from BPME_Common.pchipwrap import pchipwrap as pchip

def IdealGeneratorEfficiency(mcrgen,MCRgenAEST,etagenAEST):
    if MCRgenAEST.ndim==1:
        MCRgenAEST=MCRgenAEST.reshape(1,-1)
    if etagenAEST.ndim==1:
        etagenAEST=etagenAEST.reshape(1,-1)
    
    [imax,jmax]=np.shape(mcrgen)
    etagenid=np.empty(np.shape(mcrgen))
    etagenid[:] = np.nan
    
    for i in range(0,imax):
        for j in range(0,jmax):
            if mcrgen[i,j]<MCRgenAEST[j,0]:
                etagenid[i,j]=etagenAEST[j,0]
            elif (mcrgen[i,j]>=MCRgenAEST[j,0]) and (mcrgen[i,j]<=MCRgenAEST[j,-1]):
                pchipp=pchip(MCRgenAEST[j,:],etagenAEST[j,:])
                etagenid[i,j] = pchipp(mcrgen[i,j])
                # pchip=ipchip.PchipInterpolator(MCRgenAEST[j,:],etagenAEST[j,:])
                # etagenid[i,j]=pchip(mcrgen[i,j])
            elif mcrgen[i,j]>=MCRgenAEST[j,-1]:
                etagenid[i,j]=etagenAEST[j,-1]    
            else:
                print('IdealGeneratorEfficiency error!') #diagnostic
     
    etagenid[(etagenid>1) | (etagenid<0.7)]=np.nan
    
    return etagenid