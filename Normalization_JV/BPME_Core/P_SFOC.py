# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 20:13:56 2020

@author: Subhin Antony

% P_SFOC
% Description:  This function is used to estimate the effective power based
%               on the SFOC reference curve corrected for LCV.
%
% Input:        Mfoc    [kg/h]  FO consumption (vector)
%               LCV     [Mj/kg] FO LCV (vector)
%               PST     [kW]    shop test effective power (vector)
%               SFOCST  [g/kWh] shop test SFOC (w.o. ISO correction) (vector)
%               LCVST   [Mj/kg] shop test FO LCV (scalar)
%
% Output:       P       [kW]    estimated effective power (vector)
%
% See also:     PowerEstimation, P_FPI, P_NTC, P_LI

"""
import numpy as np
#from scipy.interpolate import pchip
from BPME_Common.pchipwrap import pchipwrap as pchip

def P_SFOC(Mfoc,LCV,PST,SFOCST,LCVST):
    #Create MfocST vector
    MfocST=PST*SFOCST/1000*LCVST/42.7
    
    #Correct current mass FO consumption for current LCV
    Mcor=Mfoc*LCV/42.7
    Mcor=Mcor.astype('float64')
    #Estimate power
    pc=pchip(MfocST,PST)
    P=pc(Mcor)
    
    
    #Check if Mcor is within the shop tested range
    Mcheck=np.array(Mcor<np.min(MfocST))
    if np.sum(Mcheck)>0:
        P[Mcheck]=np.nan
        print('P_SFOC error: Mfoc too low for the method to be applied!')
        
    return(P)




