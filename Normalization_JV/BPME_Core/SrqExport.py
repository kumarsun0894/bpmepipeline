# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 14:26:57 2020

@author: SubhinAntony
"""
import numpy as np
from datetime import datetime
def SrqExport(start_time,SRQ,SRQST):
    # IMO=np.empty(np.shape(SDS['LogID']))
    # IMO[:]=int(RDS['IMONoVessel']) # This should be taken from the input in while implementation.
    # LogID=SDS['LogID']
    # DateTimeStart=SDS['tstart']
    # DateTimeEnd=SDS['tend']
    # SRQ['ID']=111
    SRQ['SRQStatus']='Completed'
    SRQ['SRQCompletedOn']=datetime.now()
    SRQ['SRQCompletedOn']=SRQ['SRQCompletedOn'].strftime("%d/%m/%Y %H:%M:%S")
    return(SRQ,SRQST)