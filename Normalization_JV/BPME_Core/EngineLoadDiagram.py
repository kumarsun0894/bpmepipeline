# -*- coding: utf-8 -*-
"""
Created on Tue May  5 18:39:43 2020

@author: SunKumar

% EngineLoadDiagram
% Description:  This function generates the engine's layout diagram
%               according to MAN guide (add reference here).
%
% Input:        PMCR    [kW]    Engine's power at MCR (scalar)
%               NMCR    [RPM]   Engine's speed at MCR (scalar)
%
% Output:       x       [RPM]   Layout diagram x-axis (vector)
%               y       [kW]]   Layout diagram y-axis (vector)
%
% See also:     RefDashboards


"""

import numpy as np
def EngineLoadDiagram(PMCR,NMCR):
	#Find point B
	Pb=0.967*PMCR
	Nb=0.967*NMCR

	#Create segment No4
	C4=Pb/Nb**2
	x4= np.linspace(0,Nb,10)
	y4=C4*x4**2

	#Create the whole diagram
	x= np.round(np.append(x4,[NMCR,NMCR*1.05,NMCR*1.05]),2)
	
	y=np.round(np.append(y4,[PMCR,PMCR,0]),2)
	
	
	return x,y
