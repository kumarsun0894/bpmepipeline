# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 21:16:17 2020

@author: Sun Kumar
"""


import os
import configparser
import logging
from pathlib import Path,PureWindowsPath

logger = logging.getLogger('BPMEngine.2.02')

try:
    # get absolute path of currently running file
    dirname, filename = os.path.split(Path(PureWindowsPath(os.path.abspath(__file__))))


    log_dir=os.path.abspath(Path(PureWindowsPath(os.path.join(dirname, '..'))))

    # change directory to the parent directory to read 'config' file
    os.chdir(Path(PureWindowsPath(log_dir)))

    config = configparser.ConfigParser()

    # Read config file that sets configurable parameters for the application
    config.read('config.ini')







    #Set all MariApps API variables and url's
    mariBaseUrl = config['Mariapps_API']['baseURL']
    mariHeaders = config['Mariapps_API']['headers']
    
    mariclientCrt = config['Mariapps_API']['clientCrt']
    mariclientKey = config['Mariapps_API']['clientKey']
    maripendingUrl = config['Mariapps_API']['pendingUrl']
    mariGetRef = config['Mariapps_API']['mariappsGetReference']
    mariGetNoon = config['Mariapps_API']['getNoon']
    mariGetHP = config['Mariapps_API']['getHP']
    mariGetME = config['Mariapps_API']['getME']
    mariGetAE = config['Mariapps_API']['getAE']
    mariGetAuto = config['Mariapps_API']['getAuto']

    fromDate = config['Mariapps_API']['fromDate']
    toDate = config['Mariapps_API']['toDate']
    mariNoonOut=config['Mariapps_API']['mariNoonOut']
    mariHPOut=config['Mariapps_API']['mariHPOut']
    mariMEOut=config['Mariapps_API']['mariMEOut']
    mariAEOut=config['Mariapps_API']['mariAEOut']
    mariRefOut=config['Mariapps_API']['mariRefOut']
    UnprocessedOnly=config['Mariapps_API']['UnprocessedOnly']
    
    mariautopendingUrl = config['Mariapps_API']['AutopendingUrl']
    mariAutoOut=config['Mariapps_API']['mariAutoOut']
    

    #Set all Navidium API variables and url's
    AuthUser=config['Navidium_API']['AuthUser']
    AuthPW=config['Navidium_API']['AuthPW']
    
    naviBaseUrl=config['Navidium_API']['baseURL']
    NaviHeaders = config['Navidium_API']['Bulkheaders']
    getReference = config['Navidium_API']['getReference']
    saveReference=config['Navidium_API']['saveReference']
    saveNoon=config['Navidium_API']['saveNoon']
    saveNoonKPI=config['Navidium_API']['saveNoonKPI']
    saveAutoKPI=config['Navidium_API']['saveAutoKPI']
    saveHP=config['Navidium_API']['saveHP']
    saveHPKPI=config['Navidium_API']['saveHPKPI']
    saveME=config['Navidium_API']['saveME']
    saveAE=config['Navidium_API']['saveAE']
    saveMEKPI=config['Navidium_API']['saveMEKPI']
    saveAEKPI=config['Navidium_API']['saveAEKPI']
    getAuto=config['Navidium_API']['getAuto']
    AutoFromDate = config['Navidium_API']['AutoFromDate']
    AutoToDate = config['Navidium_API']['AutoToDate']
    saveSRQurl = config['Navidium_API']['SaveSRQurl']
    pendingAuto = config['Navidium_API']['pendingAuto']
    
    saveAuto=config['Navidium_API']['saveAuto']
    melog = config['Navidium_API']['melog']
    noonlog = config['Navidium_API']['noonlog']
    hplog = config['Navidium_API']['hplog']
    autolog = config['Navidium_API']['autolog']
    

    #BULK
    SaveHistoricalSRQHeader=config['Navidium_API']['SaveHistoricalSRQHeader']
    GetDataSyncInProgressHSRQ=config['Navidium_API']['GetDataSyncInProgressHSRQ']
    SaveHSRQLogs=config['Navidium_API']['SaveHSRQLogs']
    UpdateHistoricalSRQLogStatus=config['Navidium_API']['UpdateHistoricalSRQLogStatus']
    UpdateBulkSRQHeaderStatus=config['Navidium_API']['UpdateBulkSRQHeaderStatus']


    #Set Vessel Filter
    vessel=config['DEFAULT']['vessel_filter']
    auto_vessel=config['DEFAULT']['auto_filter']
    DataPath=config['DEFAULT']['DataPath']
    em_list=config['DEFAULT']['em_list']
    cc_list=config['DEFAULT']['cc_list']
    mail_sender=config['DEFAULT']['mail_sender']
    sender_pw=config['DEFAULT']['sender_pw']

    os.chdir(Path(PureWindowsPath(dirname)))




except Exception:

    logger.error("Error occured in Configuration", exc_info=True,stack_info =True)
