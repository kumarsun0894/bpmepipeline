# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 10:07:15 2020

@author: SubhinAntony
"""

import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import os.path
import pandas as pd

def SendEmail(email_recipient,df_test,email_subject,attachment_txt = '',attachment_zip = ''):

    email_sender = 'testsubhin@outlook.com'

    # msg = MIMEMultipart()
    # msg['From'] = email_sender
    # msg['To'] = email_recipient
    # msg['Subject'] = email_subject

    # msg.attach(MIMEText(email_message, 'plain'))
    #message = MIMEMultipart("alternative")
    message = MIMEMultipart()
    message["Subject"] = email_subject
    message["From"] = email_sender
    message["To"] = email_recipient
    
    # Create the plain-text and HTML version of your message
    

    
    
    html = """\
    <html>
      <head></head>
      <body>
      <p>Hi,</p>
      <p>Please find below, list of vessel(s) causing error(s) from HP BPM Engine:</p>
        {0}
     <p>Best Regards,</p>
      </body>
    </html>
    """.format(df_test.to_html())
    
    # Turn these into plain/html MIMEText objects
    # part1 = MIMEText(textone, "plain")
    part2 = MIMEText(html, "html")
    # part3 = MIMEText(texttwo, "plain")
    
    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    # message.attach(part1)
    message.attach(part2)
    # message.attach(part3)

    if attachment_txt != '':
        filename = os.path.basename(attachment_txt)
        attachment = open(attachment_txt, "rb")
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(attachment.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition',
                        "attachment; filename= %s" % filename)
        message.attach(part)
    if attachment_zip != '':
        filename = os.path.basename(attachment_zip)
        attachment = open(attachment_zip, "rb")
        partz = MIMEBase('application', 'octet-stream')
        partz.set_payload(attachment.read())
        encoders.encode_base64(partz)
        partz.add_header('Content-Disposition',
                        "attachment; filename= %s" % filename)
        message.attach(partz)

    try:
        server = smtplib.SMTP('smtp.office365.com', 587)
        server.ehlo()
        server.starttls()
        server.login('testsubhin@outlook.com', 'subhin1996')
        text = message.as_string()
        server.sendmail(email_sender, email_recipient, text)
        print('email sent')
        server.quit()
    except:
        print("SMPT server connection error")
    return True
if __name__=='__main__':
    csvv=pd.read_csv('er.csv',index_col=0)
    em_list=['subhin.antony@navidium.com','sun.kumar@navidium.com','aravind.chandrasekhran@navidium.com']
    for em in em_list:
        SendEmail(em,csvv,'Error in BPM Engine','C:\\Users\\SubhinAntony\\Videos\\Mail\\email\\1230\\BPMEngineNoonErrorLog.txt','C:\\Users\\SubhinAntony\\Videos\\Mail\\email\\1230\\InputFile.zip')
        print('sent')