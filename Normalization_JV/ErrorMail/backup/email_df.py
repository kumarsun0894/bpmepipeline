# -*- coding: utf-8 -*-
"""
Created on Fri Nov 13 10:39:34 2020

@author: SubhinAntony
"""
import pandas as pd
from pathlib import Path
import os
import io
import shutil

import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import os.path

def SendEmail(email_sender,pw,email_recipient,cc_recipient,df_test,email_subject,Logtype,attachment_txt = '',attachment_zip = ''):

    #email_sender = 'testsubhin@outlook.com'
    #cc_recipient=['subhin07@gmail.com']

    
    message = MIMEMultipart()
    message["Subject"] = email_subject
    message["From"] = email_sender
    message["To"] =", ".join(email_recipient)
    message["Cc"] =", ".join(cc_recipient)
    df_test=df_test.drop_duplicates()
    
    
    # Create the plain-text and HTML version of your message
    

    
    
    html = """\
    <html>
      <head></head>
      <body>
      <p>Hi,</p>
      <p>Please find below, list of vessel(s) causing error(s) from {0} BPM Engine:</p>
        {1}
     <p>Best Regards,</p>
      </body>
    </html>
    """.format(Logtype,df_test.to_html())
    
    # Turn these into plain/html MIMEText objects
    # part1 = MIMEText(textone, "plain")
    part2 = MIMEText(html, "html")
    # part3 = MIMEText(texttwo, "plain")
    
    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    # message.attach(part1)
    message.attach(part2)
    # message.attach(part3)

    if attachment_txt != '':
        filename = os.path.basename(attachment_txt)
        attachment = open(attachment_txt, "rb")
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(attachment.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition',
                        "attachment; filename= %s" % filename)
        message.attach(part)
    if attachment_zip != '':
        filename = os.path.basename(attachment_zip)
        attachment = open(attachment_zip, "rb")
        partz = MIMEBase('application', 'octet-stream')
        partz.set_payload(attachment.read())
        encoders.encode_base64(partz)
        partz.add_header('Content-Disposition',
                        "attachment; filename= %s" % filename)
        message.attach(partz)

    try:
        server = smtplib.SMTP('smtp.office365.com', 587)
        server.ehlo()
        server.starttls()
        server.login(email_sender, pw)
        text = message.as_string()
        server.sendmail(email_sender, (email_recipient + cc_recipient), text)
        print('email sent')
        server.quit()
        return(int(1))
    except:
        print("SMPT server connection error")
        return(int(0))

def email_df(main_fld,csv_dr,em_list,cc_list,mail_sender,sender_pw,noonlog,hplog,melog):
    ref_st=-1
    noon_st=-1
    hp_st=-1
    me_st=-1
    noondf_send=pd.DataFrame(columns = ['Object ID','Vessel'])
    hpdf_send=pd.DataFrame(columns = ['Object ID','Vessel'])
    medf_send=pd.DataFrame(columns = ['Object ID','Vessel'])
    vsl_path=csv_dr+"\VesselDetails.csv"
    noon_infile=Path(main_fld+"\\Noon\\InputFile")
    hp_infile=Path(main_fld+"\\HP\\InputFile")
    me_infile=Path(main_fld+"\\ME\\InputFile")
    # load csv dir
    vsl_csv=pd.read_csv(vsl_path)
    
    # check and load ref_df
    refdfOutput= main_fld+r"\Refdata\rfdf.csv"
    if Path(refdfOutput).is_file():
        rf_df=pd.read_csv(refdfOutput)
        if rf_df.shape[0]>0:
            ref_st=0
            rf_df = rf_df.drop_duplicates()
            rf_df.reset_index(drop=True,inplace=True)
            rf_mrg=pd.merge(rf_df, vsl_csv, on='imo')
            rf_mrg_send=rf_mrg[['Object ID','Vessel']]
            noondf_send=noondf_send.append(rf_mrg_send, ignore_index=True)
            hpdf_send=hpdf_send.append(rf_mrg_send, ignore_index=True)
            medf_send=medf_send.append(rf_mrg_send, ignore_index=True)
            if not os.path.exists(noon_infile):
                os.makedirs(noon_infile)
            if not os.path.exists(hp_infile):
                os.makedirs(hp_infile)
            if not os.path.exists(me_infile):
                os.makedirs(me_infile)
            for index, row in rf_mrg.iterrows():
                ref_noon=Path(str(noon_infile)+"\Ref_{}.m".format(row['Object ID']))
                mynoon = open(ref_noon,'w')
                mynoon.write(row['ref'])
                mynoon.close()
                ref_hp=Path(str(hp_infile)+"\Ref_{}.m".format(row['Object ID']))
                myhp = open(ref_hp,'w')
                myhp.write(row['ref'])
                myhp.close()
                ref_me=Path(str(me_infile)+"\Ref_{}.m".format(row['Object ID']))
                myme = open(ref_me,'w')
                myme.write(row['ref'])
                myme.close()
    noondfOutput= main_fld+r"\Noon\noondf.csv"
    if Path(noondfOutput).is_file():
        noon_df=pd.read_csv(noondfOutput)
        if noon_df.shape[0]>0:
            noon_st=0
            noon_df = noon_df.drop_duplicates()
            noon_df.reset_index(drop=True,inplace=True)
            noon_mrg=pd.merge(noon_df, vsl_csv, on='imo')
            noon_mrg_send=noon_mrg[['Object ID','Vessel']]
            noondf_send=noondf_send.append(noon_mrg_send, ignore_index=True)
            if not os.path.exists(noon_infile):
                os.makedirs(noon_infile)
            for index, row in noon_mrg.iterrows():
                ref_noon=Path(str(noon_infile)+"\Ref_{}.m".format(row['Object ID']))
                mynoon = open(ref_noon,'w')
                mynoon.write(row['ref'])
                mynoon.close()
                inputdata=pd.read_csv(io.StringIO(row['ser']))
                sr_noon=Path(str(noon_infile)+"\\NoonLog_{}.csv".format(row['Object ID']))
                inputdata.to_csv(sr_noon)
            
            
    hpdfOutput= main_fld+r"\HP\hpdf.csv"
    if Path(hpdfOutput).is_file():
        hp_df=pd.read_csv(hpdfOutput)
        if hp_df.shape[0]>0:
            hp_st=0
            hp_df = hp_df.drop_duplicates()
            hp_df.reset_index(drop=True,inplace=True)
            hp_mrg=pd.merge(hp_df, vsl_csv, on='imo')
            hp_mrg_send=hp_mrg[['Object ID','Vessel']]
            hpdf_send=noondf_send.append(hp_mrg_send, ignore_index=True)
            if not os.path.exists(hp_infile):
                os.makedirs(hp_infile)
            for index, row in hp_mrg.iterrows():
                ref_hp=Path(str(hp_infile)+"\Ref_{}.m".format(row['Object ID']))
                myhp = open(ref_hp,'w')
                myhp.write(row['ref'])
                myhp.close()
                inputdata=pd.read_csv(io.StringIO(row['ser']))
                sr_hp=Path(str(hp_infile)+"\\HPLog_{}.csv".format(row['Object ID']))
                inputdata.to_csv(sr_hp)
            
    medfOutput= main_fld+r"\ME\medf.csv"
    if Path(medfOutput).is_file():
        me_df=pd.read_csv(medfOutput)
        if me_df.shape[0]>0:
            me_st=0
            me_df = me_df.drop_duplicates()
            me_df.reset_index(drop=True,inplace=True)
            me_mrg=pd.merge(me_df, vsl_csv, on='imo')
            me_mrg_send=me_mrg[['Object ID','Vessel']]
            medf_send=noondf_send.append(me_mrg_send, ignore_index=True)
            if not os.path.exists(me_infile):
                os.makedirs(me_infile)
            for index, row in me_mrg.iterrows():
                ref_me=Path(str(me_infile)+"\Ref_{}.m".format(row['Object ID']))
                myme = open(ref_me,'w')
                myme.write(row['ref'])
                myme.close()
                inputdata=pd.read_csv(io.StringIO(row['ser']))
                sr_me=Path(str(me_infile)+"\\MELog_{}.csv".format(row['Object ID']))
                inputdata.to_csv(sr_me)
    if (ref_st==0) or (noon_st==0):
        if os.path.exists(noon_infile):
            shutil.make_archive(noon_infile, 'zip', noon_infile)
        infl_zip=Path(main_fld+"\\Noon\\InputFile.zip")
        tx_file=Path(csv_dr+"\\"+noonlog)
        if not(os.path.exists(infl_zip)):
            infl_zip=''
        if not(os.path.exists(tx_file)):
            tx_file=''
        
        noondf_send.index += 1
        

        cnt=SendEmail(mail_sender,sender_pw,em_list,cc_list,noondf_send,'Error in NoonBPM Engine',"Noon",infl_zip,tx_file)
            
        if cnt==int(1):
            print('remove folder')
            if Path(refdfOutput).is_file():
                os.remove(refdfOutput)
            
            shutil.rmtree(Path(main_fld+"\\Noon"))
            if tx_file.is_file():
                open(tx_file, 'w').close()
    if (ref_st==0) or (hp_st==0):
        if os.path.exists(hp_infile):
            shutil.make_archive(hp_infile, 'zip', hp_infile)
        infl_zip=Path(main_fld+"\\HP\\InputFile.zip")
        tx_file=Path(csv_dr+"\\"+hplog)
        if not(os.path.exists(infl_zip)):
            infl_zip=''
        if not(os.path.exists(tx_file)):
            tx_file=''
        
        hpdf_send.index += 1
        

        cnt=SendEmail(mail_sender,sender_pw,em_list,cc_list,hpdf_send,'Error in HPBPM Engine',"HP",infl_zip,tx_file)
            
        if cnt==int(1):
            # print('remove folder')
            if Path(refdfOutput).is_file():
                os.remove(refdfOutput)
            
            shutil.rmtree(Path(main_fld+"\\HP"))
            if tx_file.is_file():
                open(tx_file, 'w').close()
    if (ref_st==0) or (me_st==0):
        if os.path.exists(me_infile):
            shutil.make_archive(me_infile, 'zip', me_infile)
        infl_zip=Path(main_fld+"\\ME\\InputFile.zip")
        tx_file=Path(csv_dr+"\\"+melog)
        if not(os.path.exists(infl_zip)):
            infl_zip=''
        if not(os.path.exists(tx_file)):
            tx_file=''
        
        medf_send.index += 1
        

        cnt=SendEmail(mail_sender,sender_pw,em_list,cc_list,medf_send,'Error in MEBPM Engine',"ME",infl_zip,tx_file)
            
        if cnt==int(1):
            print('remove folder')
            if Path(refdfOutput).is_file():
                os.remove(refdfOutput)
            
            shutil.rmtree(Path(main_fld+"\\ME"))
            if tx_file.is_file():
                open(tx_file, 'w').close()
                
                
            
        
        
            
        
    
    
    