# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 10:08:19 2020

@author: SubhinAntony
"""
#22-7-20
import json
from csv_repo import LoadServiceData_csv as lsd
from csv_repo import ReferenceCurves_csv as rc
from csv_repo import LoadReferenceData_csv as lrd
from csv_repo import RefDashboards as rfd
from BPMEngineSingleVessel import BPMEngineSingleVessel
import time
import pandas as pd
import io
import numpy as np

start_time = time.time()
shiplist=pd.read_csv('error.csv')

# ls1=shiplist.index.to_list()

#shiplist = pd.DataFrame(shiplist, index =[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49]) 
#shiplist = pd.DataFrame(shiplist, index =[1]) 
#ls2=shiplist.index.to_list()
#shiplist.to_csv('error22.csv')


###f= open("sample_01_sep_2020.txt","w+")
#shiplist=pd.read_csv('ext.csv')
#ls1=shiplist.index.to_list()

#ls2=shiplist.index.to_list()
#shiplist.to_csv('complex.csv')

SRQ={}
SRQST=[]


BPMEngineVersion = 2.02
mgk_list=[]
fn_list=[]
xinc=[]
comp=[]
python_error_list=[]
completed=[]
for key, value in shiplist.iterrows():
    error_index_python=int(value[0])-1
    error_index_matlab=int(value[0])
    print('-------------------wow------wow------{}'.format(int(value[0])-1))
    try:
    
        VesselObjectID=value[1]
        LogType=value[2]
        if LogType=='ME'or LogType=='AE':
            EngineIndex=int(value[3])
        else:
            EngineIndex=value[3]
        if pd.isnull(value[3]):
            EngineIndex=''
            
        print("find_ship")
        VesselObjectID=int(VesselObjectID)
        print(VesselObjectID,LogType,EngineIndex)
        outputfile1="csv_out/{}{}Output_{}.csv".format(LogType,EngineIndex,VesselObjectID)
        outputfile2="csv_out/RefOutput_{}.csv".format(VesselObjectID)  ##not needed 
        outputfile3="csv_out/{}{}Diagnostics_{}.csv".format(LogType,EngineIndex,VesselObjectID)
        outputfileEX="csv_out/{}{}Output_{}EX.csv".format(LogType,EngineIndex,VesselObjectID)
        # Load reference data for the selected vessel
        RDSraw,file = lrd.LoadReferenceData(int(VesselObjectID))
        RDS = rc.ReferenceCurves(RDSraw, LogType,start_time,SRQST)
        RefDashTable=rfd.RefDashboards(RDS)
        RefDashTable=pd.DataFrame.from_dict(RefDashTable)
        RefDashTable.to_csv(outputfile2, index=False)
        SDSraw = lsd.LoadServiceData(VesselObjectID, EngineIndex, LogType)
        ###f.write('\r\n '+str(LogType))
        ###f.write('\r\n...............................................')
        print(LogType)
        # if LogType in ['Auto','Noon','HP','ME','AE']:
        #     ServiceDataRaw=SDSraw.to_csv()
        #     RefOutput=RefDashTable.to_csv()
        #     if LogType in ['Auto','Noon','HP']:
        #         injson={'IMO':int(VesselObjectID),'LogData':ServiceDataRaw}
        #         injson =str(injson)
        #     if LogType in ['ME']:
        #         injson={'IMO':int(VesselObjectID),'LogData':ServiceDataRaw,'MENumber':int(EngineIndex)}
        #         injson =str(injson)
        #     if LogType in ['AE']:
        #         injson={'IMO':int(VesselObjectID),'LogData':ServiceDataRaw,'AENumber':int(EngineIndex)}
        #         injson =str(injson)
        #     refjson={'IMO':int(VesselObjectID),'ReferenceInput':file,'ReferenceCurve':RefOutput}
        #     refjson =str(refjson)
        #     f.write("\r\n Input \r\n")
        #     f.write(injson)
        #     f.write("\r\n Reference \r\n")
        #     f.write(refjson)
            
        Output,Diagnostics,ExOutput,srqlist=BPMEngineSingleVessel(LogType,EngineIndex,BPMEngineVersion,RDS,SDSraw,SRQ,start_time,SRQST)
        # if LogType =='Auto':
            
        #     ojson={'IMO':int(VesselObjectID),'Output':Output,'Diagnostics':Diagnostics}
        #     ojson =str(ojson)
             
        #     f.write("\r\n output \r\n")
        #     f.write(ojson)
        # if LogType =='Noon':
            
        #     ojson={'IMO':int(VesselObjectID),'Output':Output,'Diagnostics':Diagnostics,'Extentions':ExOutput}
        #     ojson =str(ojson)
             
        #     f.write("\r\n output \r\n")
        #     f.write(ojson)
        # if LogType =='HP':
            
        #     ojson={'IMO':int(VesselObjectID),'Output':Output,'Diagnostics':Diagnostics}
        #     ojson =str(ojson)
             
        #     f.write("\r\n output \r\n")
        #     f.write(ojson)
        # if LogType =='ME':
            
        #     ojson={'IMO':int(VesselObjectID),'Output':Output,'Diagnostics':Diagnostics,'MENumber':int(EngineIndex)}
        #     ojson =str(ojson)
             
        #     f.write("\r\n output \r\n")
        #     f.write(ojson)
        # if LogType =='AE':
            
        #     ojson={'IMO':int(VesselObjectID),'Output':Output,'Diagnostics':Diagnostics,'AENumber':int(EngineIndex)}
        #     ojson =str(ojson)
             
        #     f.write("\r\n output \r\n")
        #     f.write(ojson)
        # if LogType =='Noon':
        #     nooninputjson={'IMO':int(imo),'LogData':SDSraw}
            
        Outputpd=pd.read_csv(io.StringIO(Output))
        Outputpd.to_csv(outputfile1, index=False)
        Diagnosticspd=pd.read_csv(io.StringIO(Diagnostics))
        Diagnosticspd.to_csv(outputfile3, index=False)
        if LogType =='Noon':
            ExOutputpd=pd.read_csv(io.StringIO(ExOutput))
            ExOutputpd.to_csv(outputfileEX, index=False)
        print("done bro ")
    
    except FileNotFoundError:
        fn_list.append(error_index_python)
    except Exception as err:
        raise
        if str(err) =='(m>k) failed for hidden m: fpcurf0:m=3':
            mgk_list.append(error_index_python)
        elif str(err) =='`x` must be strictly increasing sequence.':
            xinc.append(error_index_python)
        elif str(err) =="can't convert complex to float":
            comp.append(error_index_python)
        
        else:
            python_error_list.append(error_index_python)
    else:
        completed.append(error_index_python)
        print(python_error_list)
        print(mgk_list)
        print(xinc)
        print(comp)
        print(fn_list)
# f.close()
        
        
    
        
        
