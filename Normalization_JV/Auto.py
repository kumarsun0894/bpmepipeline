# -*- coding: utf-8 -*-
"""
Created on Wed Sep  2 20:59:35 2020

@author: SubhinAntony
"""
from LogSetup import setup_logging
setup_logging()
import logging
from BPMEngineSingleVessel import BPMEngineSingleVessel
from BPME_Repository.PendingAuto import PendingAuto
from BPME_Repository.AutoInputApi import AutoInputApi
from BPME_Repository.AutoOutputApi import AutoOutputApi
from BPME_Repository.AutoRefApi import AutoRefApi
from BPME_Repository.LoadReferenceData import LoadReferenceData
from BPME_Repository.LoadServiceData import LoadServiceData
from BPME_Transform.ReferenceCurves import ReferenceCurves
from BPME_Core.RefDashboards import RefDashboards
from BPME_Repository.isComplexPropulsion import isComplexPropulsion
from config import naviBaseUrl,pendingAuto,getReference,getAuto,AutoFromDate,AutoToDate,saveAutoKPI
import pandas as pd
import time
import logging
import numpy as np
from config import AuthUser,AuthPW
# create logger
logger = logging.getLogger('BPMEngine.2.02')
import warnings
warnings.filterwarnings("ignore")
import datetime
def AutoBPME():
    start_time = time.time()
    BPMEngineVersion='2.02'
    

    SRQ={}
    SRQ['BPMEVersion']=BPMEngineVersion
    SRQST1=[]
    #'imo', 'logtype','rawref','RDSraw','RDS','RefOut'
    #Invoking Prelimenary ApI for  fetching IMO and LogType
    AutoRecords,SRQST1 = PendingAuto(naviBaseUrl,pendingAuto,start_time,SRQST1,AuthUser,AuthPW)
    # split by imo
    AutoRecordData =AutoRecords.groupby(AutoRecords.imo)

    for rd in AutoRecordData:
        SRQST = SRQST1.copy()
        AutoRecordsDf=rd[1]
        #Invoking NDL Reference ApI for all IMOs

        AutoRecordsDf,SRQST  = AutoRefApi(naviBaseUrl,getReference,AutoRecordsDf,start_time,SRQST,AuthUser,AuthPW)
        #Loading Reference Data and Extracting NME from Reference Data for all IMOs
        AutoRecordsDf,SRQST = LoadReferenceData(AutoRecordsDf,start_time,SRQST)
        # Check if complex propulsion chain exist and go to next vessel
        AutoRecordsDf_filter = AutoRecordsDf.drop_duplicates(subset = ["imo"])
        AutoRecordsDf_filter.reset_index(drop=True,inplace=True)

        icp,SRQST=isComplexPropulsion(AutoRecordsDf_filter['RDSraw'][0],start_time,SRQST)
        if icp:
            print('Complex propulsion chain!')
        # Define reference curves
        AutoRecordsDf_filter,SRQST= ReferenceCurves(AutoRecordsDf_filter,start_time,SRQST)
        #RefDashboards(RDS)
        AutoRecordsDf_filter,SRQST=RefDashboards(AutoRecordsDf_filter,start_time,SRQST)

        # save reference output ???


        #records_df=records_df.iloc[0]
        for i , records_df in AutoRecordsDf_filter.iterrows():
            try:
                #Processing for Noon Service Data
                if records_df['logtype'] =='Auto':
                    a=1/0
                    SRQSTN=SRQST.copy()
                    SRQN=SRQ.copy()
                    SRQN['logtype']='Auto'
                    #Calling Mariapps API for Fetching Noon Service Data
                    ServiceDataRaw,SRQSTN = AutoInputApi(naviBaseUrl,getAuto,AutoFromDate,AutoToDate,records_df,start_time,SRQST,AuthUser,AuthPW)
                    #Loading Service Data into Dataframe For processing
                    ServiceData,SRQSTN = LoadServiceData(ServiceDataRaw,records_df['logtype'],start_time,SRQSTN,records_df['imo'])
                    print("Total Number of Noon LogID to process-{}".format(len(ServiceData)))
                    if len(ServiceData)!=0:
                        ServiceData =ServiceData.groupby(ServiceData.LogID)
                        #Passing Each LogID(row) into BPMEngineSingleVessel
                        for data in ServiceData:
                            SRQSTNSD=SRQSTN.copy()

                            #Calling BPMEngine Single Vessel
                            AutoOutput,AutoDiagnostics,AutoOutputExt,srqlist=BPMEngineSingleVessel(records_df['logtype'],records_df['EngineIndex'],BPMEngineVersion,records_df['RDS'],data[1],SRQN,start_time,SRQSTNSD)
                            #Saving Noon Output into Navidium Database by Calling Navidim API
                            SRQSTND=AutoOutputApi(naviBaseUrl,saveAutoKPI,records_df['imo'],AutoOutput,AutoDiagnostics,AutoOutputExt,start_time,AuthUser,AuthPW)
                            srqlist['SRQST'].append(SRQSTND)

                            # #Saving Service Queue Request by Calling Navidium's Service Queue Request API
                            # SaveSRQApi(naviBaseUrl,saveSRQurl,srqlist,start_time)

                    else:
                        print("No Noon Log to process for imo {}".format(records_df['imo']))

            except:
                logger.error("Error in Auto for imo {} and logtype {}".format(records_df['imo'],records_df['logtype']),exc_info=True,stack_info =True)
                pass


if __name__ == '__main__':
    AutoBPME()


