# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 11:14:52 2020
@author: Sun Kumar
"""
#check
from LogSetup import setup_logging
setup_logging()
import logging
logger = logging.getLogger('BPMEngine.2.02')
#directory =os.path.split(Path(PureWindowsPath(os.path.abspath(__file__))))[0]
#os.chdir(Path(PureWindowsPath(directory)))


import io
import warnings
warnings.filterwarnings("ignore")
import ast
import time
#         from LogSetup import setup_logging
#         setup_logging()

from BPME_Repository.PendingReports import PendingReports
from BPME_Repository.PALRefApi import PALRefApi
from BPME_Repository.LoadReferenceData import LoadReferenceData
from BPME_Repository.isComplexPropulsion import isComplexPropulsion
from BPME_Transform.ReferenceCurves import ReferenceCurves
from BPME_Repository.VesselFilter import VesselFilter
from BPME_Core.RefDashboards import RefDashboards
from BPME_Repository.LoadServiceData import LoadServiceData
from BPME_Repository.PALNoonApi import PALNoonApi
from BPME_Repository.PALHPApi import PALHPApi
from BPME_Repository.PALMEApi import PALMEApi
from BPME_Repository.PALAEApi import PALAEApi
from BPME_Repository.NDLRefApi import NDLRefApi
from BPME_Repository.SaveNoonInput import SaveNoonInput
from BPME_Repository.SaveHPInput import SaveHPInput
from BPME_Repository.SaveMEInput import SaveMEInput
from BPME_Repository.SaveAEInput import SaveAEInput
from BPME_Repository.NoonOutputApi import NoonOutputApi
from BPME_Repository.HPOutputApi import HPOutputApi
from BPME_Repository.MEOutputApi import MEOutputApi
from BPME_Repository.AEOutputApi import AEOutputApi
from BPME_Repository.SaveRefApI import SaveRefApI
from BPME_Repository.ReferenceCompare import ReferenceCompare
from BPMEngineSingleVessel import BPMEngineSingleVessel
from BPME_Repository.SaveSRQApi import SaveSRQApi
from config import mariHeaders,mariclientKey,mariclientCrt,mariBaseUrl
from config import maripendingUrl,mariGetRef,mariGetNoon,mariGetHP,mariGetME,mariGetAE
from config import naviBaseUrl,getReference,saveNoon,saveNoonKPI,saveHP,saveHPKPI
from config import saveSRQurl,saveME,saveMEKPI,saveAE,saveAEKPI
from config import saveReference,UnprocessedOnly
from config import vessel,fromDate,toDate,mariNoonOut,mariHPOut,mariMEOut,mariRefOut,mariAEOut
from BPME_Repository.PALNoonOutput import PALNoonOutput
from BPME_Repository.PALHPOutput import PALHPOutput
from BPME_Repository.PALMEOutput import PALMEOutput
from BPME_Repository.PALAEOutput import PALAEOutput
from BPME_Repository.PALRefOutput import PALRefOutput
from config import DataPath
from config import mail_sender,sender_pw,em_list,cc_list
from ErrorMail.mkdirr import mkdirr
from ErrorMail.email_df import email_df
from BPMEAuto import BPMEAutoBPME
from config import AuthUser,AuthPW
from config import  melog,noonlog,hplog

import pandas as pd
from pathlib import Path

try:

    start_time = time.time()
    BPMEngineVersion='2.02'
    headers = ast.literal_eval(mariHeaders)
    er_file,main_fld=mkdirr(DataPath)
    # em_list=ast.literal_eval(em_list)
    # email_df(main_fld, DataPath,em_list)
    
    refdfOutput= main_fld+r"\Refdata\rfdf.csv"
    if Path(refdfOutput).is_file():
        rf_df=pd.read_csv(refdfOutput)
    else:
        rf_df=pd.DataFrame(columns = ['imo','ref','ser'])
    rf_df.to_csv(refdfOutput, index=False)

    SRQ={}
    SRQ['BPMEVersion']=BPMEngineVersion
    SRQST1=[]

    #Invoking Prelimenary ApI for  fetching IMO and LogType
    records_df1,SRQST1 = PendingReports(mariBaseUrl,maripendingUrl,headers,mariclientCrt,mariclientKey,start_time,SRQST1)
    #records_df1=records_df1[records_df1['logtype']=="ME"]
    # import pandas as pd
    # records_df1 = pd.DataFrame(columns=['imo', 'logtype','rawref','naviRef','RDSraw','RDS','RefOut','nME','EngineIndex','nAE','AEEngineIndex'])
    # records_df1  = records_df1.append({'imo': int(9172129) ,'logtype': 'Noon'}, ignore_index=True)
    # records_df1  = records_df1.append({'imo': int(9665683) ,'logtype': 'HP'}, ignore_index=True)
    # records_df1  = records_df1.append({'imo': int(9665683) ,'logtype': 'ME','EngineIndex': int(1)}, ignore_index=True)
    #Applying Vessel Filter
    if vessel == '':
        pass
    else:
        records_df1 = VesselFilter(records_df1,vessel)



    #Invoking all ServiceData APIs for each row of records_df
    RecordData =records_df1.groupby(records_df1.imo)
    #for i,v in records_df.iterrows():
    for rd in RecordData:
        SRQST = SRQST1.copy()
        records_df=rd[1]
        ref_imo = records_df.imo.unique()
        try:
            

            #Invoking PAL Reference ApI for all IMOs
            records_df,SRQST  = PALRefApi(mariBaseUrl,mariGetRef,headers,mariclientCrt,mariclientKey,records_df,start_time,SRQST)
            #Loading Reference Data and Extracting NME from Reference Data for all IMOs
            records_df,SRQST = LoadReferenceData(records_df,start_time,SRQST)
            # Check if complex propulsion chain exist and go to next vessel
            records_df_filter = records_df.drop_duplicates(subset = ["imo"])
            records_df_filter.reset_index(drop=True,inplace=True)
            if isinstance(records_df, pd.DataFrame):
                ref=records_df_filter['rawref'][0]
            else:
                ref='empty'
    
            icp,SRQST=isComplexPropulsion(records_df_filter['RDSraw'][0],start_time,SRQST)
            if icp:
                print('Complex propulsion chain!')
            # Define reference curves
            records_df_filter,SRQST= ReferenceCurves(records_df_filter,start_time,SRQST)
            #RefDashboards(RDS)
            records_df_filter,SRQST=RefDashboards(records_df_filter,start_time,SRQST)
            #Invoking NDL Reference ApI for List of IMOspip
            records_df_filter,SRQST = NDLRefApi(naviBaseUrl,getReference,records_df_filter,start_time,SRQST,AuthUser,AuthPW) #incase of error use mariapps_reference only
            #Reference Comparison
            if SRQST[-1]['Remarks'] =='complete':
                mismatched_imo,SRQST = ReferenceCompare(records_df_filter,start_time,SRQST)
                #Saving Reference Output in Navidium Server/input also in case of change
                SRQST=SaveRefApI(naviBaseUrl,saveReference,mismatched_imo,records_df_filter,start_time,SRQST,AuthUser,AuthPW)
            elif SRQST[-1]['Remarks'] =='failed':
                mismatched_imo = records_df_filter.imo.tolist()
                #Saving Reference Output in Navidium Server/input also in case of change
                SRQST=SaveRefApI(naviBaseUrl,saveReference,mismatched_imo,records_df_filter,start_time,SRQST,AuthUser,AuthPW)
    
    #         else:
    #             SRQST=SaveRefApI(naviBaseUrl,saveReference,mismatched_imo,records_df_filter,start_time,SRQST)
            #Passing Data to MariApps
            SRQST = PALRefOutput(mariBaseUrl,mariRefOut,headers,mariclientCrt,mariclientKey,records_df_filter,start_time,SRQST)
            if not(ref=="empty"):
                ref_stat=-1
                for stat in SRQST:
                    if stat['Stage'] in ['BPME_Transform.ReferenceCurves','BPME_Core.RefDashboards']:
                        if stat['Remarks']=="failed":
                            ref_stat=0
            if not(ref=="empty") and (ref_stat==0):
                rf_df  = rf_df.append({'imo': int(ref_imo) ,'ref': ref}, ignore_index=True)
                refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                rf_df.to_csv(refdfOutput, index=False)
            records_df1 = records_df.merge(records_df_filter,on='imo',how='right')
            records_df2 = records_df1.drop(['logtype_y','nME_y','EngineIndex_y','nAE_y','AEEngineIndex_y','naviRef_x','RDS_x','RefOut_x','rawref_x','RDSraw_x'],axis=1)
            records_df2.rename(columns = {'logtype_x':'logtype', 'nME_x':'nME','nAE_x':'nAE','AEEngineIndex_x':'AEEngineIndex',
                                      'RDS_y':'RDS','RefOut_y':'RefOut','EngineIndex_x':'EngineIndex','rawref_y':'rawref','naviRef_y':'naviRef','RDSraw_y':'RDSraw'}, inplace = True)
    
            #records_df=records_df.iloc[0]
            for i , records_df in records_df2.iterrows():
                try:
                    #Processing for Noon Service Data
                    if records_df['logtype'] =='Noon':
                        noondfOutput= main_fld+r"\Noon\noondf.csv"
                        if Path(noondfOutput).is_file():
                            noon_df=pd.read_csv(noondfOutput)
                        else:
                            noon_df=pd.DataFrame(columns = ['imo','ref','ser'])
                        noon_df.to_csv(noondfOutput, index=False)
                        noonimo=records_df['imo']
                        noonref=records_df['rawref']
                        SRQSTN=SRQST.copy()
                        SRQN=SRQ.copy()
                        SRQN['logtype']='Noon'
                        #Calling Mariapps API for Fetching Noon Service Data
                        ServiceDataRaw,SRQSTN = PALNoonApi(mariBaseUrl,mariGetNoon,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,start_time,SRQSTN)
                        noonser=ServiceDataRaw
                        #Loading Service Data into Dataframe For processing
                        ServiceData,SRQSTN = LoadServiceData(ServiceDataRaw,records_df['logtype'],start_time,SRQSTN,records_df['imo'])
                        print("Total Number of Noon LogID to process-{}".format(len(ServiceData)))
                        if len(ServiceData)!=0:
                            ServiceData =ServiceData.groupby(ServiceData.LogID)
                            #Passing Each LogID(row) into BPMEngineSingleVessel
                            for data in ServiceData:
                                SRQSTNSD=SRQSTN.copy()
                                #Saving Noon Input into Navidium Database by Calling Navidium API
                                SRQSTNSD=SaveNoonInput(naviBaseUrl,saveNoon,records_df['imo'],data[1],start_time,SRQSTNSD,AuthUser,AuthPW)
                                #Calling BPMEngine Single Vessel
                                NoonOutput,NoonDiagnostics,NoonOutputExt,srqlist=BPMEngineSingleVessel(records_df['logtype'],records_df['EngineIndex'],BPMEngineVersion,records_df['RDS'],data[1],SRQN,start_time,SRQSTNSD)
                                #Saving Noon Output into Navidium Database by Calling Navidim API
                                SRQSTND=NoonOutputApi(naviBaseUrl,saveNoonKPI,records_df['imo'],NoonOutput,NoonDiagnostics,NoonOutputExt,start_time,AuthUser,AuthPW)
                                srqlist['SRQST'].append(SRQSTND)
                                #Passing Noon Api to Mariapps
                                NoonOutput=pd.read_csv(io.StringIO(NoonOutput))
                                for colm in NoonOutput.columns:
                                
                                    if colm in ['SCOCME_set_1','SCOCME_set_2','SCOCME_set_3','SCOCME_set_4','SCOCME_set_5','SCOCME_set_6','SCOCME_set_7']:
                                        del NoonOutput[colm]
                                NoonOutput=NoonOutput.to_csv(index = False)
                                
                                srqlist['SRQST'].append(PALNoonOutput(mariBaseUrl,mariNoonOut,headers,mariclientCrt,mariclientKey,NoonOutput,NoonDiagnostics,start_time))
                                #Saving Service Queue Request by Calling Navidium's Service Queue Request API
                                SaveSRQApi(naviBaseUrl,saveSRQurl,srqlist,start_time,AuthUser,AuthPW)
                                if records_df['logtype']=='Noon':
                                    if not(noonser==''):
                                        noon_stat=-1
                                        for stat in srqlist['SRQST']:
                                            if stat['Stage'] in ['BPMEngineSingleVessel']:
                                                if stat['Remarks']=="failed":
                                                    noon_stat=0
                                    if not(noonser=='') and (noon_stat==0):
                                        noon_df=pd.read_csv(noondfOutput)
                                        noon_df  = noon_df.append({'imo': int(noonimo) ,'ref': noonref,'ser': noonser}, ignore_index=True)
                                        # refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                                        noon_df.to_csv(noondfOutput, index=False)
    
                        else:
                            print("No Noon Log to process for imo {}".format(records_df['imo']))
                    #Processing for HP Service Data
                    elif records_df['logtype'] =='HP':
                        hpdfOutput= main_fld+r"\HP\hpdf.csv"
                        if Path(hpdfOutput).is_file():
                            hp_df=pd.read_csv(hpdfOutput)
                        else:
                            hp_df=pd.DataFrame(columns = ['imo','ref','ser'])
                        hp_df.to_csv(hpdfOutput, index=False)
                        hpimo=records_df['imo']
                        hpref=records_df['rawref']
                        SRQSTH=SRQST.copy()
                        SRQH=SRQ.copy()
                        SRQH['logtype']='HP'
                        #Calling Mariapps API for Fetching HP Service Data
                        ServiceDataRaw,SRQSTH = PALHPApi(mariBaseUrl,mariGetHP,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,start_time,SRQSTH)
                        hpser=ServiceDataRaw
                        #Loading Service Data into Dataframe For processing
                        ServiceData,SRQSTH = LoadServiceData(ServiceDataRaw,records_df['logtype'],start_time,SRQSTH,records_df['imo'])
                        print("Total Number of HP LogID to process-{}".format(len(ServiceData)))
                        if len(ServiceData)!=0:
                            ServiceData =ServiceData.groupby(ServiceData.LogID)
                            #Passing Each LogID(row) into BPMEngineSingleVessel
                            for data in ServiceData:
                                SRQSTHSD=SRQSTH.copy()
                                #Saving HP Input into Navidium Database by Calling Navidium API
                                SRQSTHSD=SaveHPInput(naviBaseUrl,saveHP,records_df['imo'],data[1],start_time,SRQSTHSD,AuthUser,AuthPW)
                                #Calling BPMEngine Single Vessel
                                HPOutput,HPDiagnostics,HPOutputExt,srqlist=BPMEngineSingleVessel(records_df['logtype'],records_df['EngineIndex'],BPMEngineVersion,records_df['RDS'],data[1],SRQH,start_time,SRQSTHSD)
                                #Saving HP Output into Navidium Database by Calling Navidim API
                                SRQSTHD=HPOutputApi(naviBaseUrl,saveHPKPI,records_df['imo'],HPOutput,HPDiagnostics,start_time,AuthUser,AuthPW)
                                srqlist['SRQST'].append(SRQSTHD)
                                #Passing HP Output to Mariapps
                                srqlist['SRQST'].append(PALHPOutput(mariBaseUrl,mariHPOut,headers,mariclientCrt,mariclientKey,HPOutput,HPDiagnostics,start_time))
                                #Saving Service Queue Request by Calling Navidium's Service Queue Request API
                                SaveSRQApi(naviBaseUrl,saveSRQurl,srqlist,start_time,AuthUser,AuthPW)
                                if records_df['logtype']=='HP':
                                    if not(hpser==''):
                                        hp_stat=-1
                                        for stat in srqlist['SRQST']:
                                            if stat['Stage'] in ['BPMEngineSingleVessel']:
                                                if stat['Remarks']=="failed":
                                                    hp_stat=0
                                    if not(hpser=='') and (hp_stat==0):
                                        hp_df=pd.read_csv(hpdfOutput)
                                        hp_df  = hp_df.append({'imo': int(hpimo) ,'ref': hpref,'ser': hpser}, ignore_index=True)
                                        # refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                                        hp_df.to_csv(hpdfOutput, index=False)
                        else:
                            print("No HP Log to process for imo {}".format(records_df['imo']))
    
                    # #Processing for ME Service Data
                    elif records_df['logtype'] =='ME':
                        medfOutput= main_fld+r"\ME\medf.csv"
                        if Path(medfOutput).is_file():
                            me_df=pd.read_csv(medfOutput)
                        else:
                            me_df=pd.DataFrame(columns = ['imo','ref','ser'])
                        me_df.to_csv(medfOutput, index=False)
                        meimo=records_df['imo']
                        meref=records_df['rawref']
                        SRQSTM=SRQST.copy()
                        SRQM=SRQ.copy()
                        SRQM['logtype']='ME'
                        for EngineNo in range(0,int(records_df['nME'])):
                            #Calling Mariapps API for Fetching ME Service Data
                            ServiceDataRaw,SRQSTM = PALMEApi(mariBaseUrl,mariGetME,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,EngineNo+1,start_time,SRQSTM)
                            meser=ServiceDataRaw
                            #Loading Service Data into Dataframe For processing
                            ServiceData,SRQSTM = LoadServiceData(ServiceDataRaw,records_df['logtype'],start_time,SRQSTM,records_df['imo'])
                            print("Total Number of ME LogID to process-{}".format(len(ServiceData)))
                            if len(ServiceData)!=0:
                                ServiceData =ServiceData.groupby(ServiceData.LogID)
                                #Passing Each LogID(row) into BPMEngineSingleVessel
                                for data in ServiceData:
                                    SRQSTMSD=SRQSTM.copy()
                                    #Saving ME Input into Navidium Database by Calling Navidium API
                                    SRQSTMSD=SaveMEInput(naviBaseUrl,saveME,records_df['imo'],data[1],EngineNo+1,start_time,SRQSTMSD,AuthUser,AuthPW)
                                    #Calling BPMEngine Single Vessel
                                    MEOutput,MEDiagnostics,MEOutputExt,srqlist=BPMEngineSingleVessel(records_df['logtype'],EngineNo+1,BPMEngineVersion,records_df['RDS'],data[1],SRQM,start_time,SRQSTMSD)
                                    #Saving ME Output into Navidium Database by Calling Navidim API
                                    SRQSTMD=MEOutputApi(naviBaseUrl,saveMEKPI,records_df['imo'],MEOutput,MEDiagnostics,EngineNo+1,start_time,AuthUser,AuthPW)
                                    srqlist['SRQST'].append(SRQSTMD)
                                    #Pass ME output to PAL
                                    srqlist['SRQST'].append(PALMEOutput(mariBaseUrl,mariMEOut,headers,mariclientCrt,mariclientKey,EngineNo+1,MEOutput,MEDiagnostics,start_time))
                                    #Saving Service Queue Request by Calling Navidium's Service Queue Request API
                                    SaveSRQApi(naviBaseUrl,saveSRQurl,srqlist,start_time,AuthUser,AuthPW)
                                    if records_df['logtype']=='ME':
                                        if not(meser==''):
                                            me_stat=-1
                                            for stat in srqlist['SRQST']:
                                                if stat['Stage'] in ['BPMEngineSingleVessel']:
                                                    if stat['Remarks']=="failed":
                                                        me_stat=0
                                        if not(meser=='') and (me_stat==0):
                                            me_df=pd.read_csv(medfOutput)
                                            me_df  = me_df.append({'imo': int(meimo) ,'ref': meref,'ser': meser}, ignore_index=True)
                                            # refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                                            me_df.to_csv(medfOutput, index=False)
                            else:
                                print("No ME Log to process for imo {}".format(records_df['imo']))
    
                    # elif records_df['logtype'] =='AE':
                    #     SRQSTA=SRQST.copy()
                    #     SRQA=SRQ.copy()
                    #     SRQA['logtype']='AE'
                    #     for EngineNo in range(0,int(records_df['nAE'])):
                    #         #Calling Mariapps API for Fetching AE Service Data
                    #         ServiceDataRaw,SRQSTA = PALAEApi(mariBaseUrl,mariGetAE,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,EngineNo+1,start_time,SRQSTA)
                    #         #Loading Service Data into Dataframe For processing
                    #         ServiceData,SRQSTA = LoadServiceData(ServiceDataRaw,records_df['logtype'],start_time,SRQSTA,records_df['imo'])
                    #         print("Total Number of AE LogID to process-{}".format(len(ServiceData)))
                    #         if len(ServiceData)!=0:
                    #             ServiceData =ServiceData.groupby(ServiceData.LogID)
                    #             #Passing Each LogID(row) into BPMEngineSingleVessel
                    #             for data in ServiceData:
                    #                 SRQSTASD=SRQSTA.copy()
                    #                 #Saving AE Input into Navidium Database by Calling Navidium API
                    #                 SRQSTASD=SaveAEInput(naviBaseUrl,saveAE,records_df['imo'],data[1],EngineNo+1,start_time,SRQSTASD)
                    #                 #Calling BPMEngine Single Vessel
                    #                 AEOutput,AEDiagnostics,AEOutputExt,srqlist=BPMEngineSingleVessel(records_df['logtype'],EngineNo+1,BPMEngineVersion,records_df['RDS'],data[1],SRQA,start_time,SRQSTASD)
                    #                 #Saving AE Output into Navidium Database by Calling Navidim API
                    #                 SRQSTAD=AEOutputApi(naviBaseUrl,saveAEKPI,records_df['imo'],AEOutput,AEDiagnostics,EngineNo+1,start_time)
                    #                 srqlist['SRQST'].append(SRQSTAD)
                    #                 #Pass AE output to PAL
                    #                 srqlist['SRQST'].append(PALAEOutput(mariBaseUrl,mariAEOut,headers,mariclientCrt,mariclientKey,EngineNo+1,AEOutput,AEDiagnostics,start_time))
                    #                 #Saving Service Queue Request by Calling Navidium's Service Queue Request API
                    #                 SaveSRQApi(naviBaseUrl,saveSRQurl,srqlist,start_time)
                    #         else:
                    #             print("No AE Log to process for imo {}".format(records_df['imo']))
    
    
    
    
    
    
    
                except:
                    
                    logger.error("Error in BPMEngine for imo {} and logtype {}".format(records_df['imo'],records_df['logtype']),exc_info=True)
                    if records_df['logtype']=='Noon':
                        if not(noonser==''):
                            noon_stat=-1
                            for stat in srqlist['SRQST']:
                                if stat['Stage'] in ['BPMEngineSingleVessel']:
                                    if stat['Remarks']=="failed":
                                        noon_stat=0
                        if not(noonser=='') and (noon_stat==0):
                            noon_df=pd.read_csv(noondfOutput)
                            noon_df  = noon_df.append({'imo': int(noonimo) ,'ref': noonref,'ser': noonser}, ignore_index=True)
                            # refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                            noon_df.to_csv(noondfOutput, index=False)
                    if records_df['logtype']=='HP':
                        if not(hpser==''):
                            hp_stat=-1
                            for stat in srqlist['SRQST']:
                                if stat['Stage'] in ['BPMEngineSingleVessel']:
                                    if stat['Remarks']=="failed":
                                        hp_stat=0
                        if not(hpser=='') and (hp_stat==0):
                            hp_df=pd.read_csv(hpdfOutput)
                            hp_df  = hp_df.append({'imo': int(hpimo) ,'ref': hpref,'ser': hpser}, ignore_index=True)
                            # refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                            hp_df.to_csv(hpdfOutput, index=False)
                    if records_df['logtype']=='ME':
                        if not(meser==''):
                            me_stat=-1
                            for stat in srqlist['SRQST']:
                                if stat['Stage'] in ['BPMEngineSingleVessel']:
                                    if stat['Remarks']=="failed":
                                        me_stat=0
                        if not(meser=='') and (me_stat==0):
                            me_df=pd.read_csv(medfOutput)
                            me_df  = me_df.append({'imo': int(meimo) ,'ref': meref,'ser': meser}, ignore_index=True)
                            # refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                            me_df.to_csv(medfOutput, index=False)
                    pass

        except Exception as er:
            #logger.error("Error in BPMEngine {} ".format(er),exc_info=True,stack_info =True)
            logger.error("Error in BPMEngine {} for imo-{} ".format(er,ref_imo),exc_info=True)
            if not(ref=="empty"):
                ref_stat=-1
                for stat in SRQST:
                    if stat['Stage'] in ['BPME_Transform.ReferenceCurves','BPME_Core.RefDashboards']:
                        if stat['Remarks']=="failed":
                            ref_stat=0
            if not(ref=="empty") and (ref_stat==0):
                rf_df=pd.read_csv(refdfOutput)
                rf_df  = rf_df.append({'imo': int(ref_imo) ,'ref': ref}, ignore_index=True)
                # refdfOutput= main_fld+r"\Refdata\rfdf.csv"
                rf_df.to_csv(refdfOutput, index=False)
            pass
    #BPMEAutoBPME()
except Exception as err:
    logger.error("Error in BPMEngine {} ".format(err),exc_info=True)

else:
    try:
        
        em_list=ast.literal_eval(em_list)
        cc_list=ast.literal_eval(cc_list)
        email_df(main_fld, DataPath,em_list,cc_list,mail_sender,sender_pw,noonlog,hplog,melog)
    except Exception as err:
        logger.error("Error in sending mail {} ".format(err),exc_info=True)
        print('email failed')
    else:
        print("BPM Engine 2.02  Execution Completed Successfully")


