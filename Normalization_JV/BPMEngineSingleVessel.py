# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 10:03:26 2020

@author: SubhinAntony
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 14:49:48 2020

@author: Subhin Antony
"""
import sys
from BPME_Transform import GroupServiceData as gsd
from BPME_Transform import ManipulateFuelData as mfd
from BPME_Transform import ManipulateCYLOData as mcyl
from BPME_Transform import HandleServiceSymbols as hss
from BPME_Transform import MissingServiceDataEstimator as msde
from BPME_Transform import ManipulateServiceData as msd
from BPME_Core import PowerEstimation as pe
from BPME_Core import CalculateSFOC as csfoc
from BPME_Core import CalculateSCOC as cscoc
from BPME_Core import STATOTAL as st
from BPME_Core import OutputCalculation as opc
from BPME_Core import OutputFiltering as opf
from BPME_Core import OutputExport as ope
from DiagnosticsDependancy import DiagnosticsDependancy as dd

from BPME_Repository import dataframetodic as dtd
from BPME_Repository import SrqDataFrame as sdf
import pandas as pd
import time
import logging
import numpy as np
# create logger
logger = logging.getLogger('BPMEngine.2.02')

import datetime

def BPMEngineSingleVessel(LogType,EngineIndex,BPMEngineVersion,RDS,ServiceData,SRQ,start_time,SRQSTTemp):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__


    if pd.isnull(EngineIndex):
        EngineIndex = ''
    SRQST=SRQSTTemp.copy()
    SDSraw,SRQST = dtd.dataframetodic(ServiceData, start_time, SRQST)
    # SRQ update
    SRQ['SRQStatus']='InProgress'
    SRQ['SRQStartedOn']=datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f%Z")
    SRQ['SRQStartedOn']=SRQ['SRQStartedOn']   #.strftime("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")  #"%d/%m/%Y %H:%M:%S"
    IMO=int(RDS['IMONoVessel'])
    LogID=SDSraw['LogID']
    DateTimeStart=SDSraw['tstart']
    DateTimeEnd=SDSraw['tend']
    SRQ['IMO']=IMO
    SRQ['LogId']=int(LogID[0])
    SRQ['LogDurationStart']=DateTimeStart[0].strftime("%Y-%m-%dT%H:%M:%S.%f%Z")
    SRQ['LogDurationEnd']=DateTimeEnd[0].strftime("%Y-%m-%dT%H:%M:%S.%f%Z")

    try:

        # Group service data
        SDSraw, DS,SRQST = gsd.GroupServiceData(IMO,SDSraw, RDS, EngineIndex, LogType ,start_time,SRQST)
        
        

        # Manipulate fuel data
        SDSraw, DS,SRQST = mfd.ManipulateFuelData(IMO,SDSraw, RDS, DS, LogType,start_time,SRQST)
        #Manipulate CYLO data
        [SDSraw,DS,SRQST]=mcyl.ManipulateCYLOData(IMO,SDSraw,DS,LogType,start_time,SRQST)


        # Handle service symbols
        SDS,SRQST = hss.HandleServiceSymbols(IMO,SDSraw,LogType,start_time,SRQST)

        # Estimate missing service data
        SDS,SRQST = msde.MissingServiceDataEstimator(IMO,LogType,SDS, RDS,start_time,SRQST)

        # Manipulate service data
        [SDS, DS,SRQST] = msd.ManipulateServiceData(IMO,SDS, RDS, DS, EngineIndex, LogType,start_time,SRQST)

        # power destination
        [SDS, DS,SRQST] = pe.PowerEstimation(IMO,SDS, RDS, DS, EngineIndex, LogType,start_time,SRQST)

        # CalculateSFOC
        SDS,SRQST = csfoc.CalculateSFOC(IMO,SDS, RDS, LogType,start_time,SRQST)
        #SCOC calculation
        SDS,SRQST=cscoc.CalculateSCOC(IMO,SDS,LogType,start_time,SRQST)

        if 'HP' in LogType or 'Noon' in LogType or 'Auto' in LogType:
            [SDS, DS,SRQST] = st.STATOTAL(IMO,LogType,SDS, RDS, DS,start_time,SRQST)

        [ODS, DS,SRQST] = opc.OutputCalculation(SDS, RDS, DS, EngineIndex,IMO,LogType,start_time,SRQST)
        
        
        

        # Filter output data
        [ODS, DS,SRQST] = opf.OutputFiltering(IMO,SDS, RDS, ODS, DS, LogType,start_time,SRQST)
        #ODS=dd.DiagnosticsDependancy(DS, ODS,LogType)

        # Export KPIs, selected corrected variables and diagnostics
        Output,Diagnostics,ExOutput,SRQST=ope.OutputExport(IMO,SDS, RDS, ODS, DS, IMO,EngineIndex, LogType, BPMEngineVersion,start_time,SRQST)


        if LogType=='Noon':
            Output=Output.replace([np.inf, -np.inf], np.nan)
            Diagnostics=Diagnostics.replace([np.inf, -np.inf], np.nan)
            ExOutput=ExOutput.replace([np.inf, -np.inf], np.nan)
            FinalOutput=Output.to_csv(index = False)
            FinalDiagnostics=Diagnostics.to_csv(index = False)
            FinalExOutput=ExOutput.to_csv(index = False)
        if LogType in ['HP','ME','Auto','AE']:
            Output=Output.replace([np.inf, -np.inf], np.nan)
            Diagnostics=Diagnostics.replace([np.inf, -np.inf], np.nan)
            FinalOutput=Output.to_csv(index = False)
            FinalDiagnostics=Diagnostics.to_csv(index = False)
            FinalExOutput=''
        print("--- %s seconds ---" % (time.time() - start_time))
        # return(FinalOutput,FinalDiagnostics,FinalExOutput,SRQPerLog)
    except Exception as err:
        
        logger.error("Error in BPMEngineSingleVessel in line No. {} for imo {} and logtype {} and LogID-{}  Error-{}".format(sys.exc_info()[-1].tb_lineno,IMO,LogType,LogID,err),exc_info=True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        SRQPerLog=sdf.SrqDataFrame(SRQ,SRQST,"failed")
        return('','','',SRQPerLog)

    else:

        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        SRQPerLog=sdf.SrqDataFrame(SRQ,SRQST,"Completed")
        return(FinalOutput,FinalDiagnostics,FinalExOutput,SRQPerLog)

