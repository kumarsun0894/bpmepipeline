# -*- coding: utf-8 -*-
"""
Created on Wed Sep  2 20:59:35 2020

@author: SubhinAntony
"""
import ast
from BPME_Repository.NDLRefApi import NDLRefApi
from BPMEngineSingleVessel import BPMEngineSingleVessel
from BPME_Repository.PendingAuto import PendingAuto
from BPME_Repository.VesselFilter import VesselFilter
from BPME_Repository.PALPendingAuto import PALPendingAuto
from BPME_Repository.PALRefApi import PALRefApi
from BPME_Repository.SaveRefApI import SaveRefApI
from BPME_Repository.SaveSRQApi import SaveSRQApi
from BPME_Repository.ReferenceCompareAuto import ReferenceCompareAuto
from BPME_Repository.AutoInputApi import AutoInputApi
from BPME_Repository.AutoOutputApi import AutoOutputApi
from BPME_Repository.AutoRefApi import AutoRefApi
from BPME_Repository.LoadReferenceData import LoadReferenceData
from BPME_Repository.LoadServiceData import LoadServiceData
from BPME_Transform.ReferenceCurves import ReferenceCurves
from BPME_Core.RefDashboards import RefDashboards
from BPME_Repository.isComplexPropulsion import isComplexPropulsion
from BPME_Repository.PALRefOutput import PALRefOutput
from BPME_Repository.PALAutoApi import PALAutoApi
from BPME_Repository.PALAutoOutput import PALAutoOutput
from BPME_Repository.SaveAutoInput import SaveAutoInput
from config import mariHeaders,mariclientKey,mariclientCrt,mariBaseUrl
from config import auto_vessel
from config import naviBaseUrl,pendingAuto,getReference,getAuto,AutoFromDate,AutoToDate,saveAutoKPI,saveSRQurl,mariGetAuto,saveAuto
from config import vessel,fromDate,toDate,UnprocessedOnly,mariAutoOut
from config import mariautopendingUrl,mariGetRef,mariRefOut
from config import saveReference
from config import AuthUser,AuthPW
import pandas as pd
import time
import logging
import numpy as np
# create logger
logger = logging.getLogger('BPMEngine.2.02')
import warnings
warnings.filterwarnings("ignore")
import datetime
def BPMEAutoBPME():
    try:
        start_time = time.time()
        BPMEngineVersion='2.02'
        headers = ast.literal_eval(mariHeaders)
    
        SRQ={}
        SRQ['BPMEVersion']=BPMEngineVersion
        SRQST1=[]
        #'imo', 'logtype','rawref','RDSraw','RDS','RefOut'
        #Invoking Prelimenary ApI for  fetching IMO and LogType
        # Pending auto from PAL
        AutoRecords,SRQST1 = PALPendingAuto(mariBaseUrl,mariautopendingUrl,headers,mariclientCrt,mariclientKey,start_time,SRQST1)
        #AutoRecords,SRQST1 = PendingAuto(naviBaseUrl,pendingAuto,start_time,SRQST1)
        #Applying Vessel Filter
        if auto_vessel == '':
            pass
        else:
            AutoRecords = VesselFilter(AutoRecords,auto_vessel)
        # split by imo
        AutoRecordData =AutoRecords.groupby(AutoRecords.imo)
    
        for rd in AutoRecordData:
            SRQST = SRQST1.copy()
            AutoRecordsDf=rd[1]
            ref_imo = AutoRecordsDf.imo.unique()
            try:
                #Invoking PAL Reference ApI for all IMOs
                AutoRecordsDf,SRQST  = PALRefApi(mariBaseUrl,mariGetRef,headers,mariclientCrt,mariclientKey,AutoRecordsDf,start_time,SRQST)
            
                #Invoking NDL Reference ApI for all IMOs
        
                #AutoRecordsDf,SRQST  = AutoRefApi(naviBaseUrl,getReference,AutoRecordsDf,start_time,SRQST)
                #Loading Reference Data and Extracting NME from Reference Data for all IMOs
                AutoRecordsDf,SRQST = LoadReferenceData(AutoRecordsDf,start_time,SRQST)
                # Check if complex propulsion chain exist and go to next vessel
                AutoRecordsDf_filter = AutoRecordsDf.drop_duplicates(subset = ["imo"])
                AutoRecordsDf_filter.reset_index(drop=True,inplace=True)
        
                icp,SRQST=isComplexPropulsion(AutoRecordsDf_filter['RDSraw'][0],start_time,SRQST)
                if icp:
                    print('Complex propulsion chain!')
                # Define reference curves
                AutoRecordsDf_filter,SRQST= ReferenceCurves(AutoRecordsDf_filter,start_time,SRQST)
                #RefDashboards(RDS)
                AutoRecordsDf_filter,SRQST=RefDashboards(AutoRecordsDf_filter,start_time,SRQST)
                
                #Invoking NDL Reference ApI for List of IMOspip
                AutoRecordsDf_filter,SRQST = NDLRefApi(naviBaseUrl,getReference,AutoRecordsDf_filter,start_time,SRQST,AuthUser,AuthPW)#incase of error use mariapps_reference only
        
                #Reference Comparison
                if SRQST[-1]['Remarks'] =='complete':
                    mismatched_imo,SRQST = ReferenceCompareAuto(AutoRecordsDf_filter,start_time,SRQST)
                    #Saving Reference Output in Navidium Server/input also in case of change
                    SRQST=SaveRefApI(naviBaseUrl,saveReference,mismatched_imo,AutoRecordsDf_filter,start_time,SRQST,AuthUser,AuthPW)
                elif SRQST[-1]['Remarks'] =='failed':
                    mismatched_imo = AutoRecordsDf_filter.imo.tolist()
                    #Saving Reference Output in Navidium Server/input also in case of change
                    SRQST=SaveRefApI(naviBaseUrl,saveReference,mismatched_imo,AutoRecordsDf_filter,start_time,SRQST,AuthUser,AuthPW)
                    
                SRQST = PALRefOutput(mariBaseUrl,mariRefOut,headers,mariclientCrt,mariclientKey,AutoRecordsDf_filter,start_time,SRQST)
        
        
                #records_df=records_df.iloc[0]
                for i , records_df in AutoRecordsDf_filter.iterrows():
                    try:
                        #Processing for Noon Service Data
                        if records_df['logtype'] =='Auto':
                            SRQSTN=SRQST.copy()
                            SRQN=SRQ.copy()
                            SRQN['logtype']='Auto'
                            #Calling Mariapps API for Fetching Auto Service Data
                            ServiceDataRaw,SRQSTN = PALAutoApi(mariBaseUrl,mariGetAuto,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,start_time,SRQSTN)
                            #Loading Service Data into Dataframe For processing
                            ServiceData,SRQSTN = LoadServiceData(ServiceDataRaw,records_df['logtype'],start_time,SRQSTN,records_df['imo'])
                            print("Total Number of Auto LogID to process-{}".format(len(ServiceData)))
                            if len(ServiceData)!=0:
                                ServiceData =ServiceData.groupby(ServiceData.LogID)
                                #Passing Each LogID(row) into BPMEngineSingleVessel
                                for data in ServiceData:
                                    SRQSTNSD=SRQSTN.copy()
                                    SRQSTNSD=SaveAutoInput(naviBaseUrl,saveAuto,records_df['imo'],data[1],start_time,SRQSTNSD,AuthUser,AuthPW)
        
                                    #Calling BPMEngine Single Vessel
                                    AutoOutput,AutoDiagnostics,AutoOutputExt,srqlist=BPMEngineSingleVessel(records_df['logtype'],records_df['EngineIndex'],BPMEngineVersion,records_df['RDS'],data[1],SRQN,start_time,SRQSTNSD)
                                    #Saving Noon Output into Navidium Database by Calling Navidim API
                                    SRQSTND=AutoOutputApi(naviBaseUrl,saveAutoKPI,records_df['imo'],AutoOutput,AutoDiagnostics,AutoOutputExt,start_time,AuthUser,AuthPW)
                                    srqlist['SRQST'].append(SRQSTND)
                                    
                                    srqlist['SRQST'].append(SRQSTND)
                                    #Passing Noon Api to Mariapps
                                    srqlist['SRQST'].append(PALAutoOutput(mariBaseUrl,mariAutoOut,headers,mariclientCrt,mariclientKey,AutoOutput,AutoDiagnostics,start_time))
                                    #Saving Service Queue Request by Calling Navidium's Service Queue Request API
                                    SaveSRQApi(naviBaseUrl,saveSRQurl,srqlist,start_time)
        
                                    
        
                            else:
                                print("No Noon Log to process for imo {}".format(records_df['imo']))
        
                    except:
                        logger.error("Error in Auto for imo {} and logtype {}".format(records_df['imo'],records_df['logtype']),exc_info=True,stack_info =True)
                        pass
            except Exception as er:
                #logger.error("Error in BPMEngine {} ".format(er),exc_info=True,stack_info =True)
                logger.error("Error in BPMEngine {} for imo-{} ".format(er,ref_imo),exc_info=True,stack_info =True)
                pass
    except Exception as err:
        logger.error("Error in BPMEAuto {} ".format(err),exc_info=True,stack_info =True)

    else:
        print("BPMAuto 2.02  Execution Completed Successfully")


if __name__ == '__main__':
    BPMEAutoBPME()


