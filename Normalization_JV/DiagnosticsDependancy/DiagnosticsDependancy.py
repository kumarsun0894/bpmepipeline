# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 10:07:03 2020

@author: SubhinAntony
"""
# importing the module 
import numpy as np
import json
import ast
def Merge(dict1, dict2):
    res = {**dict1, **dict2}
    return res
def DiagnosticsDependancy(DS,ODS,LogType):
    val=float('Nan')
     
      
    # reading the data from the file 
    with open('C:\\Users\\SubhinAntony\\Music\\BPME2.02(9)\\BPME2.02\\Error_file\\KeyValidation.txt') as f: 
        data = f.read() 
      
    temp={}
    DSdic=DS      
    # reconstructing the data as a dictionary 
    masterjs = json.loads(data) 
    if LogType  in 'Noon':
        js=masterjs['NoonLog']
    if LogType  in 'HP':
        js=masterjs['HPLog']
    if LogType  in 'ME':
        js=masterjs['MELog']
    if LogType  in 'AE':
        js=masterjs['AELog']
    
    for ky in js.keys():
        if set(js[ky]).issubset(DSdic.keys()):
            
            print("keys are present") 
            sum_ar = 0
            for i in js[ky]:
                if DSdic[i].ndim==2:
                    sum_ar = sum_ar + DSdic[i].sum(axis=1).ravel()
                elif DSdic[i].ndim==1:
                    sum_ar = sum_ar + DSdic[i].ravel()
                elif DSdic[i].ndim==3:
                    sum_ar = sum_ar + DSdic[i].sum(axis=(1, 2)).ravel()
                else:
                    sum_ar = sum_ar + DSdic[i].ravel()
                
                
            sum_ar[sum_ar==0]=-1
            sum_ar[sum_ar>0]=0
            sum_ar[sum_ar==-1]=1
            temp[ky]=sum_ar
        else: 
            print("keys are not present") 
    val_sum=0
    for j in temp.keys():
        val_sum = val_sum + temp[j]
    val=(val_sum/len(temp.keys()))*100
    
    if LogType  in 'Noon':
        ODS['NoonLogValidity']=val
    if LogType  in 'HP':
        ODS['HPLogValidity']=val
    if LogType  in 'ME':
        ODS['MELogValidity']=val
    if LogType  in 'AE':
        ODS['AutoLogValidity']=val
    ODS=Merge(ODS, temp)  
  
    return(ODS)
      
    
    
    
    
if __name__=='__main__':
    DiagnosticsDependancy()
    