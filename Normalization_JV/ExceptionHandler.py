# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 16:24:53 2020

@author: SUN
"""
import logging


def ExceptionHandler(logtype, message):
    if logtype == 'Noon':
        log1 = logging.getLogger('BPMEngine.2.02_Noon')
        log1.error(message)
    elif logtype == 'HP':
        log2 = logging.getLogger('BPMEngine.2.02_HP')
        log2.error(message)
    elif logtype == 'ME':
        log3 = logging.getLogger('BPMEngine.2.02_ME')
        log3.error(message)
    elif logtype == 'Auto':
        log5 = logging.getLogger('BPMEngine.2.02_Auto')
        log5.error(message)
    elif logtype == 'Ref':
        log4 = logging.getLogger('BPMEngine.2.02_Ref')
        log4.error(message)
    return
