# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 10:58:49 2020

@author: SubhinAntony
"""

import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')
import requests
from requests.auth import HTTPBasicAuth

def NDLUpdateHistoricalSRQLogStatus(naviBaseUrl,HistoricalSRQLogStatus,records_df,start_time,SRQST,LogSta,AuthUser,AuthPW):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=time.time()-start_time
    SRQSTD['Stage']=__name__
    try:

        naviref=naviBaseUrl+HistoricalSRQLogStatus
        Logstatus={}
        Logstatus['IMO']=int(records_df['imo'])
        Logstatus['GroupID']=records_df['groupID']
        Logstatus['LogType']=records_df['logtype']
        Logstatus['LogStatus']=LogSta




        r=requests.post(naviref,json=Logstatus,auth=HTTPBasicAuth(AuthUser, AuthPW))
        if r.status_code==200:
            print('UpdateHistoricalSRQLogStatus Sucessfull')
        elif r.status_code!=200:
            logger.error("UpdateHistoricalSRQLogStatus API Connection Error {}".format(r.status_code), exc_info=True,stack_info =True)
            raise Exception

        # return records_df
    except Exception as err:
        logger.error("Error in UpdateHistoricalSRQLogStatus {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return(SRQST)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(SRQST)
