# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 20:27:27 2020

@author: SUN
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 12:45:12 2020

@author: Sun Kumar
"""

#Function for Fetching pending IMOs,logtype
import requests
import pandas as pd
import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')

def PendingReports_BulkAuto(mariBaseUrl,mariautopendingUrl,headers,clientCrt,clientKey,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:
        records_df = pd.DataFrame(columns=['imo', 'logtype','rawref','naviRef','RDSraw','RDS','RefOut','nME','EngineIndex','nAE','AEEngineIndex','LogCount','groupID'])
        pendurl = mariBaseUrl+mariautopendingUrl
        response = requests.get(pendurl, headers=headers, cert=(clientCrt, clientKey))
        if response.status_code == 200:
            pending_reports = response.json()
            #records = {}

            #########################

            for i in pending_reports:
                if (i['autoLogCount']>0) and (i['autoModelled']== True):
                    records_df=records_df.append({'imo': i["imo"],'logtype': 'Auto','LogCount':i['autoLogCount']}, ignore_index=True)



        elif response.status_code != 200:
            logger.error("{} {} for URL-{}".format(response.text,response.status_code,pendurl), exc_info=True,stack_info =True)
            raise Exception


        # return records_df
    except Exception as err:
        logger.error("Error in Fetching Pending Record {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return({},SRQST)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        if records_df.empty == False:
            return(records_df,SRQST)
        else:
            

            logger.error("No Pending Records to Perform Operation Further")
            return({},SRQST)
