# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 16:51:42 2020

@author: Sun Kumar
"""
import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')
import requests
from requests.auth import HTTPBasicAuth
def NDLSaveBulkLogInput(naviBaseUrl,SaveBulkLog,records_df,ServiceDataRaw,EngInx,start_time,SRQST,AuthUser,AuthPW):
	SRQSTD={}
	SRQSTD['StepTimeLapse']=(time.time()-start_time)
	SRQSTD['Stage']=__name__
	try:
		LogCount=int(len(ServiceDataRaw))
		ServiceDataRaw = ServiceDataRaw.to_csv(index = False)
		savenoonurl=naviBaseUrl+SaveBulkLog
		jsonlist=[]
		if records_df['logtype']=='Noon' or records_df['logtype']=='HP' or records_df['logtype']=='Auto' :
			jsonl={'IMO':int(records_df['imo']),'GroupId':int(records_df['groupID']),'LogType':records_df['logtype'],'BPMEVersion':"2.02",'InputLog':ServiceDataRaw,'LogCount':LogCount}
		else:
			jsonl={'IMO':int(records_df['imo']),'GroupId':int(records_df['groupID']),'EngineIndex':EngInx,'LogType':records_df['logtype'],'BPMEVersion':"2.02",'InputLog':ServiceDataRaw,'LogCount':LogCount}
		jsonlist.append(jsonl)
		r=requests.post(savenoonurl,json=jsonlist,auth=HTTPBasicAuth(AuthUser, AuthPW))
		if r.status_code==200:
			print("Bulk Input ServiceData Successfully Dumped into Database")
		elif r.status_code!=200:
			logger.error("SaveNoonInput Error {} {}".format(r.text,r.status_code), exc_info=True)
			raise Exception

	except Exception as err:

		logger.error("Error in NDLSaveBulkLogInput for IMO-{} and Logtype-{} {}".format(records_df['imo'],records_df['logtype'],err), exc_info=True)
		SRQSTD['TotalTimeLapse']=time.time()-start_time
		SRQSTD['Remarks']="failed"
		SRQST.append(SRQSTD)
		return(SRQST)

	else:
		SRQSTD['TotalTimeLapse']=time.time()-start_time
		SRQSTD['Remarks']="complete"
		SRQST.append(SRQSTD)
		return(SRQST)
