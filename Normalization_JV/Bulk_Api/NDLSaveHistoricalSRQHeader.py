# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 13:25:01 2020

@author: SubhinAntony
"""
import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')
import requests
from requests.auth import HTTPBasicAuth
def NDLSaveHistoricalSRQHeader(naviBaseUrl,NaviHeaders,SaveHistoricalHeader,records_df,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=time.time()-start_time
    SRQSTD['Stage']=__name__
    try:

        naviref=naviBaseUrl+SaveHistoricalHeader
        tempdf=records_df[['imo','logtype','LogCount']]
        # group by imo
        HList=[]


        gimo =tempdf.groupby(tempdf.imo)
        for k in gimo:
            i=k[1]
            HDic={}
            HLtype={}
            HDic['IMO']=int(i['imo'].unique()[0])
            HLtype['Reference']="true"
            if "Noon" in i['logtype'].astype('str').tolist():
                HLtype['Noon']="true"

                LogDf=i[i['logtype']=='Noon']
                HLtype['NoonCount']=int(LogDf['LogCount'])
            elif "Noon" not in i['logtype'].astype('str').tolist():
                HLtype['Noon']="false"
                HLtype['NoonCount']=int(0)
                
            if "Auto" in i['logtype'].astype('str').tolist():
                HLtype['Auto']="true"

                LogDf=i[i['logtype']=='Auto']
                HLtype['AutoCount']=int(LogDf['LogCount'])
            elif "Auto" not in i['logtype'].astype('str').tolist():
                HLtype['Auto']="false"
                HLtype['AutoCount']=int(0)


            if "HP" in i['logtype'].astype('str').tolist():
                HLtype['HP']="true"

                LogDf=i[i['logtype']=='HP']
                HLtype['HPCount']=int(LogDf['LogCount'])
            elif "HP" not in i['logtype'].astype('str').tolist():
                HLtype['HP']="false"
                HLtype['HPCount']=int(0)
            if "ME" in i['logtype'].astype('str').tolist():
                HLtype['ME']="true"

                LogDf=i[i['logtype']=='ME']
                HLtype['MECount']=int(LogDf['LogCount'])
            elif "ME" not in i['logtype'].astype('str').tolist():
                HLtype['ME']="false"
                HLtype['MECount']=int(0)
            # if "AE" in i['logtype'].astype('str').tolist():
            #     HLtype['AE']="true"

            #     LogDf=i[i['logtype']=='AE']
            #     HLtype['AECount']=int(LogDf['LogCount'])
            # elif "AE" not in i['logtype'].astype('str').tolist():
            #     HLtype['AE']="false"
            #     HLtype['AECount']=int(0)
            HDic['LogTypes']=str(HLtype).replace("'","")
            HList.append(HDic)
        r=requests.post(naviref,headers=NaviHeaders,json=HList)
        if r.status_code==200:
            print('NDLSaveHistoricalSRQHeader successful')
        elif r.status_code!=200:
            logger.error("NDLSaveHistoricalSRQHeader API Connection Error {}".format(r.status_code), exc_info=True,stack_info =True)
            raise Exception

        # return records_df
    except Exception as err:
        logger.error("Error in NDLSaveHistoricalSRQHeader {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return(SRQST)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(SRQST)


