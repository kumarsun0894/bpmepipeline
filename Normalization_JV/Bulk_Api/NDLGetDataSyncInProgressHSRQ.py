# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 15:59:15 2020

@author: SubhinAntony
"""
import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')
import requests
import pandas as pd
import ast
from requests.auth import HTTPBasicAuth
def NDLGetDataSyncInProgressHSRQ(naviBaseUrl,GetSyncInProgressHSRQ,start_time,SRQST,AuthUser,AuthPW):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=time.time()-start_time
    SRQSTD['Stage']=__name__
    try:

        naviref=naviBaseUrl+GetSyncInProgressHSRQ


        r=requests.get(naviref,auth=HTTPBasicAuth(AuthUser,AuthPW))
        if r.status_code==200:
            navirefout = r.json()
            navirefout = navirefout['result']
            tempdf=pd.DataFrame(columns=['imo', 'logtype','rawref','naviRef','RDSraw','RDS','RefOut','nME','EngineIndex','nAE','AEEngineIndex','LogCount','groupID'])
            for d in navirefout:

                d1=ast.literal_eval(d['logTypes'].replace("Reference","'Reference'").replace("Noon","'Noon'").replace("'Noon'Count","'NoonCount'").replace("Auto","'Auto'").replace("'Auto'Count","'AutoCount'").replace("HP","'HP'").replace("'HP'Count","'HPCount'").replace("ME","'ME'").replace("'ME'Count","'MECount'").replace("true","'true'").replace("false","'false'"))
                #if d1['NoonCount']>0:
                if "Noon" in d1:
                    if d1['Noon']=="true":
                        AppendDic={}
                        AppendDic['imo']=str(d['imo'])
                        AppendDic['groupID']=d['groupID']
                        AppendDic['logtype']='Noon'
                        AppendDic['LogCount']=d1['NoonCount']
                        tempdf  = tempdf.append(AppendDic, ignore_index=True)
                if "Auto" in d1:
                    if d1['Auto']=="true":
                        AppendDic={}
                        AppendDic['imo']=str(d['imo'])
                        AppendDic['groupID']=d['groupID']
                        AppendDic['logtype']='Auto'
                        AppendDic['LogCount']=d1['AutoCount']
                        tempdf  = tempdf.append(AppendDic, ignore_index=True)
                
                # if d1['HPCount']>0:
                if "HP" in d1:
                    if d1['HP']=="true":
                        AppendDic={}
                        AppendDic['imo']=str(d['imo'])
                        AppendDic['groupID']=d['groupID']
                        AppendDic['logtype']='HP'
                        AppendDic['LogCount']=d1['HPCount']
                        tempdf  = tempdf.append(AppendDic, ignore_index=True)
                #if d1['MECount']>0:
                if "ME"in d1:
                    if d1['ME']=="true":
                        AppendDic={}
                        AppendDic['imo']=str(d['imo'])
                        AppendDic['groupID']=d['groupID']
                        AppendDic['logtype']='ME'
                        AppendDic['LogCount']=d1['MECount']
                        tempdf  = tempdf.append(AppendDic, ignore_index=True)
                # if d1['AECount']>0:
                #     AppendDic['logtype']='AE'
                #     AppendDic['LogCount']=d1['AECount']



        elif r.status_code!=200:
            logger.error("Navidium GetDataSyncInProgressHSRQ API Connection Error {} with  Response Text {}".format(r.status_code,r.text), exc_info=True,stack_info =True)
            raise Exception

        # return records_df
    except Exception as err:
        logger.error("Error in GetDataSyncInProgressHSRQ {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return({},SRQST)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(tempdf,SRQST)
