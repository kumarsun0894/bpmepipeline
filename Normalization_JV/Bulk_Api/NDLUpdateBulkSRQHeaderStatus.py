# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 12:12:24 2020

@author: SubhinAntony
"""


import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')
import requests
from requests.auth import HTTPBasicAuth

def NDLUpdateBulkSRQHeaderStatus(naviBaseUrl,BulkSRQHeaderStatus,ref_imo,grp_id,start_time,SRQST,Status,AuthUser,AuthPW):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=time.time()-start_time
    SRQSTD['Stage']=__name__
    try:

        naviref=naviBaseUrl+BulkSRQHeaderStatus
        BulkHeader={}
        BulkHeader['IMO']=int(ref_imo)
        BulkHeader['GroupID']=int(grp_id)
        BulkHeader['status']=Status



        r=requests.post(naviref,json=BulkHeader,auth=HTTPBasicAuth(AuthUser, AuthPW))
        if r.status_code==200:
            print('UpdateBulkSRQHeaderStatus successful')
        elif r.status_code!=200:
            logger.error("UpdateBulkSRQHeaderStatus  API Connection Error {}".format(r.status_code), exc_info=True,stack_info =True)
            raise Exception

        # return records_df
    except Exception as err:
        logger.error("Error in UpdateBulkSRQHeaderStatus {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        return(SRQST)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        return(SRQST)
