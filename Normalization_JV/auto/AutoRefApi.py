# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 12:46:18 2020

@author: Subhin Antony
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 15:34:58 2020

@author: Sun Kumar
"""

import requests
import time
import logging
logger = logging.getLogger('BPMEngine.2.02')



def AutoRefApi(naviBaseUrl,getReference,records_df,start_time,SRQST):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=time.time()-start_time
    SRQSTD['Stage']=__name__
    try:
        imoList = records_df['imo'].astype(int).to_list()
        naviref=naviBaseUrl+getReference
        time.sleep(10)
        r=requests.get(naviref,json=imoList)
        if r.status_code==200:
            navirefout = r.json()
            navirefout = navirefout['result']['referenceData']
            for i in range(0,len(navirefout)):
                for x,v in records_df.iterrows():
                    if int(v['imo']) ==navirefout[i]['imo']:
                        v['rawref'] = navirefout[i]['data']
        elif r.status_code!=200:
            logger.error("Navidium Ref API Connection Error {}".format(r.status_code), exc_info=True,stack_info =True)
            raise Exception


    except Exception:
        logger.error("NDL Ref API call Error", exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"
        SRQST.append(SRQSTD)
        raise Exception("Internal server Error")
        return({},SRQST)
    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"
        SRQST.append(SRQSTD)
        if records_df.empty == False:
            return(records_df,SRQST)
        else:
            logger.error("No Pending Records to Perform Operation Further")
            return({},SRQST)
        # return(records_df_filtered,SRQST)





