# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 22:58:05 2020

@author: Subhin Antony
"""
import time
import logging
# create logger
logger = logging.getLogger('BPMEngine.2.02')
import requests

def AutoOutputApi(naviBaseUrl,saveAutoKPI,imo,AutoOutput,AutoDiagnostics,AutoOutputExt,start_time):
    SRQSTD={}
    SRQSTD['StepTimeLapse']=(time.time()-start_time)
    SRQSTD['Stage']=__name__
    try:

        noonout=naviBaseUrl+saveAutoKPI
        r=requests.post(noonout,json={'IMO':int(imo),'Output':AutoOutput,'Diagnostics':AutoDiagnostics,'Extentions':AutoOutputExt})
        if r.status_code==200:
            print("Auto Ouput Successfully Saved in Database")
        elif r.status_code!=200:
            logger.error("AutoOutputApi Connection Error {}".format(r.status_code), exc_info=True,stack_info =True)
            raise Exception
    except Exception as err:

        logger.error("Error in AutoOutputApi {}".format(err), exc_info=True,stack_info =True)
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="failed"

        return(SRQSTD)

    else:
        SRQSTD['TotalTimeLapse']=time.time()-start_time
        SRQSTD['Remarks']="complete"

        return(SRQSTD)
