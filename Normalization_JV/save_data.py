# -*- coding: utf-8 -*-
"""
Created on Tue Sep 22 15:54:13 2020

@author: SubhinAntony
"""
from config import mariHeaders,mariclientKey,mariclientCrt,mariBaseUrl
from config import maripendingUrl,mariGetRef,mariGetNoon,mariGetHP,mariGetME,mariGetAE
from config import naviBaseUrl,getReference,saveNoon,saveNoonKPI,saveHP,saveHPKPI
from config import saveSRQurl,saveME,saveMEKPI,saveAE,saveAEKPI
from config import saveReference,UnprocessedOnly
from config import vessel,fromDate,toDate,mariNoonOut,mariHPOut,mariMEOut,mariRefOut,mariAEOut,mariautopendingUrl,mariGetAuto
import pandas as pd
from BPME_Repository.PALPendingAuto import PALPendingAuto
from BPME_Repository.PALAutoApi import PALAutoApi
from BPME_Repository.PendingReports import PendingReports
from BPME_Repository.PALNoonApi import PALNoonApi
from BPME_Repository.PALHPApi import PALHPApi
from BPME_Repository.PALMEApi import PALMEApi
from BPME_Repository.PALAEApi import PALAEApi
import ast
import io
headers = ast.literal_eval(mariHeaders)
import requests
import time
start_time = time.time()
#records_df1,SRQST1 = PendingReports(mariBaseUrl,maripendingUrl,headers,mariclientCrt,mariclientKey,start_time,[])
# records_df1 =PALPendingAuto(mariBaseUrl,mariautopendingUrl,headers,mariclientCrt,mariclientKey,start_time,[])
# records_df1=records_df1[0]
#imolist = []
#records_df = records_df[records_df['imo'].isin(imolist)]
ref_path_Space='C:\\Users\SubhinAntony\\Music\BPME2.02(12)\\BPME2.02\\Normalization_JV\\csv_data\\ref\\'
ref_path='C:\\Users\SubhinAntony\\Music\BPME2.02(12)\\BPME2.02\\Normalization_JV\\csv_data\\'
ser_path='C:\\Users\\SubhinAntony\\Music\\BPME2.02(12)\\BPME2.02\\Normalization_JV\\csv_data\\'

records_df1 = pd.DataFrame(columns=['imo', 'logtype','rawref','naviRef','RDSraw','RDS','RefOut','nME','EngineIndex','nAE','AEEngineIndex'])
for i in [9439864,9439852,9439840,9394519,9439838]:
    records_df1  = records_df1.append({'imo': int(i) ,'logtype': 'Noon'}, ignore_index=True)

# for i in [9467677,9469182,9473468,9476599,9479888,9481958,9484546,9484558,9486568,9486582,9486594,9490674,9494242,9498119,9500821,9503225,9508732,9509906,9512111,9522972,9525821,9531909,9532513,9532525,9532537,9532549,9532604,9532616,9532628,9535149,9535163,9535187,9535204,9539834,9542465,9542489,9546813,9551777,9552800,9560376,9561370,9565742,9567996,9568495,9568500,9568512,9576753,9576765,9576777,9576789,9577604]:
#     records_df1  = records_df1.append({'imo': int(i) ,'logtype': 'HP'}, ignore_index=True)

# for i in [9804904,9810056,9811000,9812121,9825427,9825439,9832121,9836359,9841938,9841940,9842712,9842724,9844540,9844576,9844588,9851804,9851816,9852523,9883376]:
#     records_df1  = records_df1.append({'imo': int(i) ,'logtype': 'ME','EngineIndex': int(1)}, ignore_index=True)
# for i in [9709025,9709037,9760770,9762261,9762273,9825427]:
#     records_df1  = records_df1.append({'imo': int(i) ,'logtype': 'ME','EngineIndex': int(2)}, ignore_index=True)
    

#records_df1  = records_df1.append({'imo': int(9709025) ,'logtype': 'Auto'}, ignore_index=True)
#records_df1  = records_df1.append({'imo': int(9328493) ,'logtype': 'Noon'}, ignore_index=True)
#records_df1  = records_df1.append({'imo': int(9760770) ,'logtype': 'HP'}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9709025) ,'logtype': 'ME','EngineIndex': int(1)}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9709025) ,'logtype': 'ME','EngineIndex': int(2)}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9220809) ,'logtype': 'Noon'}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9250153) ,'logtype': 'Noon'}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9254680) ,'logtype': 'Noon'}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9265756) ,'logtype': 'Noon'}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9266229) ,'logtype': 'Noon'}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9275036) ,'logtype': 'Noon'}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9275050) ,'logtype': 'Noon'}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9287998) ,'logtype': 'Noon'}, ignore_index=True)
# # carl 9665683
# records_df1  = records_df1.append({'imo': int(9697246) ,'logtype': 'HP'}, ignore_index=True)
#records_df1  = records_df1.append({'imo': int(9697234) ,'logtype': 'HP'}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9697234) ,'logtype': 'Noon'}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9697246) ,'logtype': 'Noon'}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9665683) ,'logtype': 'HP'}, ignore_index=True)
#records_df1  = records_df1.append({'imo': int(9618862) ,'logtype': 'ME','EngineIndex': int(1)}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9665683) ,'logtype': 'AE','EngineIndex': int(1)}, ignore_index=True)
#max 9676711
# records_df1  = records_df1.append({'imo': int(9676711) ,'logtype': 'Noon'}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9676711) ,'logtype': 'HP'}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9760770) ,'logtype': 'ME','EngineIndex': int(1)}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9760770) ,'logtype': 'ME','EngineIndex': int(2)}, ignore_index=True)

# marvel 9760770

#records_df1  = records_df1.append({'imo': int(9760770) ,'logtype': 'Noon'}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9760770) ,'logtype': 'HP'}, ignore_index=True)
#records_df1  = records_df1.append({'imo': int(9760770) ,'logtype': 'ME','EngineIndex': int(1)}, ignore_index=True)
# records_df1  = records_df1.append({'imo': int(9760770) ,'logtype': 'AE','EngineIndex': int(1)}, ignore_index=True)


RecordData =records_df1.groupby(records_df1.imo)
for rd in RecordData:
    records_df=rd[1]
   
    imoList = records_df.imo.unique().tolist()
    for imo in imoList:
        refurl = mariBaseUrl+mariGetRef+'?'+'IMO={}'.format(imo)
        r = requests.get(refurl, headers=headers, cert=(mariclientCrt,mariclientKey))
        if r.status_code == 200:
            for  i,x in records_df.iterrows():
                if x['imo'] == imo:
                    path=ref_path+'Ref_{}.m'.format(x['imo'])
                    f = open(path, "w")
                    f.write(r.text)
                    f.close()
                    fh = open(path, "r")
                    lines = fh.readlines()
                    fh.close()
                    keep = []
                    for line in lines:
                        if not line.isspace():
                            keep.append(line)
                    newpath=ref_path_Space+'Ref_{}.m'.format(x['imo'])
                    fh = open(newpath, "w")
                    fh.write("".join(keep))
                    # should also work instead of joining the list:
                    # fh.writelines(keep)
                    fh.close()

    records_df2=records_df
    
    for i , records_df in records_df2.iterrows():
        if records_df['logtype'] =='Noon':
            ServiceDataRaw,SRQSTN = PALNoonApi(mariBaseUrl,mariGetNoon,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,start_time,[])
            inputdata=pd.read_csv(io.StringIO(ServiceDataRaw))
            
            
            path=ser_path+'NoonLog_{}.csv'.format(records_df['imo'])
            inputdata.to_csv(path)
        if records_df['logtype'] =='HP':
            ServiceDataRaw,SRQSTN = PALHPApi(mariBaseUrl,mariGetHP,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,start_time,[])
            inputdata=pd.read_csv(io.StringIO(ServiceDataRaw))
            path=ser_path+'HPLog_{}.csv'.format(records_df['imo'])
            inputdata.to_csv(path)
        if records_df['logtype'] =='ME':
            ServiceDataRaw,SRQSTN = PALMEApi(mariBaseUrl,mariGetME,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,int(1),start_time,[])
            inputdata=pd.read_csv(io.StringIO(ServiceDataRaw))
            path=ser_path+'ME{}Log_{}.csv'.format(records_df['EngineIndex'],records_df['imo'])
            inputdata.to_csv(path)
        if records_df['logtype'] =='AE':
            ServiceDataRaw,SRQSTN = PALAEApi(mariBaseUrl,mariGetAE,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,int(1),start_time,[])
            inputdata=pd.read_csv(io.StringIO(ServiceDataRaw))
            path=ser_path+'AELog_{}.csv'.format(records_df['imo'])
            inputdata.to_csv(path)
        if records_df['logtype'] =='Auto':
            ServiceDataRaw,SRQSTN = PALAutoApi(mariBaseUrl,mariGetAuto,fromDate,toDate,UnprocessedOnly,headers,mariclientCrt,mariclientKey,records_df,start_time,[])
            inputdata=pd.read_csv(io.StringIO(ServiceDataRaw))
            #inputdata = inputdata[inputdata['COGME_1'].notna()]
            
            path=ser_path+'AutoLog_{}.csv'.format(records_df['imo'])
            inputdata.to_csv(path)
            
        



    